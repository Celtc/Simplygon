﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Editor
{
    /// <summary>
    /// Set defines.
    /// </summary>
    [DefaultExecutionOrder(int.MinValue)]
    internal static class EnsureDefine
    {
        //HAVE_LINQ;HAVE_XLINQ;HAVE_XML_DOCUMENT;NETSTANDARD2_0
        private static readonly string[] DEFINES = new [] { "HAVE_LINQ", "HAVE_XLINQ", "HAVE_XML_DOCUMENT", "NETSTANDARD2_0" };

        /// <summary>
        /// Assures the scripting define symbol.
        /// </summary>
        [InitializeOnLoadMethod]
        private static void AssureScriptingDefineSymbol()
        {
            foreach (var define in DEFINES)
            {
                SetDefineSymbol(define);
            }
        }

        /// <summary>
        /// Sets the define symbol.
        /// </summary>
        /// <param name="buildTarget">The build target.</param>
        /// <param name="define">The define.</param>
        /// <returns></returns>
        private static bool SetDefineSymbol(string define)
        {
            var result = false;
            var currentTarget = EditorUserBuildSettings.selectedBuildTargetGroup;
            if (currentTarget != BuildTargetGroup.Unknown)
            {
                SetDefineSymbol(currentTarget, define);
                result = true;
            }
            return result;
        }

        /// <summary>
        /// Sets the define symbol.
        /// </summary>
        /// <param name="buildTarget">The build target.</param>
        /// <param name="define">The define.</param>
        /// <returns></returns>
        private static void SetDefineSymbol(BuildTargetGroup buildTarget, string define)
        {
            var definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTarget).Trim();
            var defines = definesString.Split(';');
            if (defines.Contains(define) == false)
            {
                if (definesString.EndsWith(";", StringComparison.InvariantCulture) == false)
                {
                    definesString += ";";
                }
                definesString += define;
                PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTarget, definesString);
            }
        }
    }
}
