﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.Utils.DispatcherAction
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.Unity.Utils
{
  internal class DispatcherAction
  {
    public Action Action { get; private set; }

    public Action Callback { get; private set; }

    public Dispatcher CallbackDispatcher { get; private set; }

    public DispatcherAction(Action action)
    {
      this.Action = action;
      this.Callback = (Action) null;
      this.CallbackDispatcher = (Dispatcher) null;
    }

    public DispatcherAction(Action action, Action callback)
    {
      this.Action = action;
      this.Callback = callback;
      this.CallbackDispatcher = (Dispatcher) null;
    }

    public DispatcherAction(Action action, Action callback, Dispatcher callbackDispatcher)
    {
      this.Action = action;
      this.Callback = callback;
      this.CallbackDispatcher = callbackDispatcher;
    }

    public void Invoke()
    {
      this.Action();
      if (this.Callback == null)
        return;
      if (this.CallbackDispatcher != null)
        this.CallbackDispatcher.BeginInvoke(new DispatcherAction(this.Callback));
      else
        this.Callback();
    }
  }
}
