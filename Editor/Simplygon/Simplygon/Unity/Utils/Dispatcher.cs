﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.Utils.Dispatcher
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.Collections.Generic;

namespace Simplygon.Unity.Utils
{
  internal class Dispatcher
  {
    private List<DispatcherAction> actionsToInvoke = new List<DispatcherAction>();
    private List<DispatcherAction> pendingActions = new List<DispatcherAction>();
    private object actionListLock = new object();

    public void BeginInvoke(DispatcherAction dispatcherAction)
    {
      lock (this.actionListLock)
        this.pendingActions.Add(dispatcherAction);
    }

    public void Clear()
    {
      lock (this.actionListLock)
      {
        this.actionsToInvoke.Clear();
        this.pendingActions.Clear();
      }
    }

    public void Update()
    {
      lock (this.actionListLock)
      {
        this.actionsToInvoke.AddRange((IEnumerable<DispatcherAction>) this.pendingActions);
        this.pendingActions.Clear();
      }
      for (int index = 0; index < this.actionsToInvoke.Count; ++index)
      {
        try
        {
          if (this.actionsToInvoke[index] != null)
            this.actionsToInvoke[index].Invoke();
        }
        catch (Exception ex)
        {
        }
      }
      this.actionsToInvoke.Clear();
    }
  }
}
