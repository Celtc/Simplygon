﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.Utils.BackgroundWorker
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.Globalization;
using System.Threading;
using UnityEngine;

namespace Simplygon.Unity.Utils
{
  internal class BackgroundWorker
  {
    private Thread thread;

    public bool Running { get; private set; }

    public Dispatcher Dispatcher { get; private set; }

    public BackgroundWorker()
    {
      this.Dispatcher = new Dispatcher();
    }

    public void Start()
    {
      try
      {
        this.Running = true;
        if (this.thread != null)
          return;
        this.thread = new Thread(new ThreadStart(this.Run));
        this.thread.Start();
      }
      catch (Exception ex)
      {
        Debug.LogWarning((object) string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to start background worker ({0}\n{1})", (object) ex.Message, (object) ex.StackTrace));
        throw;
      }
    }

    public void Stop()
    {
      try
      {
        this.Running = false;
        this.thread = (Thread) null;
        if (this.Dispatcher == null)
          return;
        this.Dispatcher.Clear();
      }
      catch (Exception ex)
      {
        Debug.LogWarning((object) string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to stop background worker ({0}\n{1})", (object) ex.Message, (object) ex.StackTrace));
        throw;
      }
    }

    private void Run()
    {
      while (this.Running)
      {
        try
        {
          if (this.Dispatcher != null)
            this.Dispatcher.Update();
          else
            this.Stop();
          Thread.Sleep(100);
        }
        catch (Exception ex)
        {
          Debug.LogWarning((object) string.Format(CultureInfo.InvariantCulture, "Simplygon: Background worker execution failed ({0}\n{1})", (object) ex.Message, (object) ex.StackTrace));
          throw;
        }
      }
    }
  }
}
