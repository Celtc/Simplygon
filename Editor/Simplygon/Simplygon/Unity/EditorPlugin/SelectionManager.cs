﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.SelectionManager
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Unity.EditorPlugin.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
  public class SelectionManager
  {
    private List<string> lastSelection = new List<string>();
    private List<string> currentSelection = new List<string>();
    private volatile bool isHandlingSelection;

    public string Name { get; private set; }

    public List<PrefabEx> SelectedPrefabs { get; private set; }

    public int SkinnedCharacterCount { get; private set; }

    public string[] DefaultedShaderNamesInCurrentSelection { get; private set; }

    public SelectionManager()
    {
      this.SelectedPrefabs = new List<PrefabEx>();
      this.DefaultedShaderNamesInCurrentSelection = new string[0];
    }

    public void Clear()
    {
      this.Name = string.Empty;
      this.SelectedPrefabs.Clear();
      this.SkinnedCharacterCount = 0;
    }

    public void Update()
    {
      if (this.isHandlingSelection)
        return;
      this.isHandlingSelection = true;
      foreach (var @object in Selection.objects)
      {
        GameObject gameObject = @object as GameObject;
        if (gameObject != null)
          this.currentSelection.Add(gameObject.name);
      }
      if (this.currentSelection.Count != this.lastSelection.Count || this.currentSelection.Except<string>((IEnumerable<string>) this.lastSelection).Any<string>() || this.lastSelection.Except<string>((IEnumerable<string>) this.currentSelection).Any<string>())
      {
        this.isHandlingSelection = false;
        this.OnSelectionChange();
      }
      this.currentSelection.Clear();
      this.isHandlingSelection = false;
    }

    public void OnSelectionChange()
    {
      if (this.isHandlingSelection)
        return;
      try
      {
        this.isHandlingSelection = true;
        List<GameObject> gameObjectList = new List<GameObject>();
        foreach (var @object in Selection.objects)
        {
          GameObject gameObject = @object as GameObject;
          if (gameObject != null)
            gameObjectList.Add(gameObject);
        }
        this.lastSelection = gameObjectList.Select<GameObject, string>((Func<GameObject, string>) (selection => selection.name)).ToList<string>();
        this.SelectedPrefabs = PrefabUtilityEx.GetPrefabsForSelection(gameObjectList);
        this.SkinnedCharacterCount = 0;
        foreach (PrefabEx selectedPrefab in this.SelectedPrefabs)
        {
          if (selectedPrefab.BoneCount > 0)
            ++this.SkinnedCharacterCount;
        }
        this.Name = this.SelectedPrefabs.Count <= 0 ? string.Empty : string.Join("_", this.SelectedPrefabs.Select<PrefabEx, string>((Func<PrefabEx, string>) (p => p.Prefab.name)).Distinct<string>().ToArray<string>());
        this.DefaultedShaderNamesInCurrentSelection = gameObjectList.SelectMany<GameObject, Renderer>((Func<GameObject, IEnumerable<Renderer>>) (selection => (IEnumerable<Renderer>) selection.GetComponentsInChildren<Renderer>())).SelectMany<Renderer, Material>((Func<Renderer, IEnumerable<Material>>) (renderer => (IEnumerable<Material>) renderer.sharedMaterials)).Where<Material>((Func<Material, bool>) (material => null != material)).Select<Material, string>((Func<Material, string>) (material => JobUtils.GetShaderLookupName(material.shader.name))).Distinct<string>().Where<string>((Func<string, bool>) (shaderLookupName => !SharedData.Instance.ShaderSettings.Keys.Contains<string>(shaderLookupName))).ToArray<string>();
      }
      catch (Exception ex)
      {
        UIUtility.LogException("Selection failed", ex);
      }
      finally
      {
        this.isHandlingSelection = false;
      }
    }
  }
}
