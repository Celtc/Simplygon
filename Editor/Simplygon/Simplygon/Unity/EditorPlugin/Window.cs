﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.Window
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Unity.EditorPlugin.Jobs;
using UnityEditor;
using UnityEngine;
using System.Globalization;

namespace Simplygon.Unity.EditorPlugin
{
    public class Window : EditorWindow
    {
        public SimplygonScreen SimplygonScreen
        {
            get; private set;
        }

        public LoginScreen LoginScreen
        {
            get; private set;
        }

        public bool ForceRepaint
        {
            get; set;
        }

        public Window()
        {
            this.LoginScreen = new LoginScreen(this);
            this.SimplygonScreen = new SimplygonScreen();
            SharedData.Instance.SimplygonWindow = this;
            this.minSize = new Vector2(300f, 400f);
            this.autoRepaintOnSceneChange = true;
        }

        [MenuItem("Window/Simplygon/Simplygon")]
        private static void Init()
        {
            EditorWindow.GetWindow(typeof(Window), false, "Simplygon");
        }

        public void OnEnable()
        {
            SharedData.Instance.Initialize();
            this.LoginScreen.Initialize();
            this.SimplygonScreen.Initialize();
        }

        internal static void CreateLODGroup(UnityCloudJob job)
        {
            int length = job.LODCount();
            if (length <= 0)
                return;
            GameObject go = new GameObject();
            go.name = string.Format(CultureInfo.InvariantCulture, "{0}_LODGroup", (object)job.CloudJob.Name);
            LODGroup lodGroup = (LODGroup)go.AddComponent(typeof(LODGroup));
            lodGroup.hideFlags = HideFlags.HideAndDontSave;
            string path = string.Format(CultureInfo.InvariantCulture, "Assets/LODs/{0}/{1}.prefab", (object)job.AssetDirectory, (object)go.name);
            LOD[] lods = new LOD[length];
            int index1 = 0;
            float screenRelativeTransitionHeight = 1f;
            for (int index2 = 0; index2 < length; ++index2)
            {
                Object original = AssetDatabase.LoadAssetAtPath(string.Format(CultureInfo.InvariantCulture, "Assets/LODs/{0}/{1}_LOD{2}.prefab", (object)job.AssetDirectory, (object)job.CloudJob.Name, (object)(index2 + 1)), typeof(GameObject));
                if (original != (Object)null)
                {
                    GameObject gameObject = Object.Instantiate(original) as GameObject;
                    gameObject.name = original.name;
                    gameObject.transform.parent = go.gameObject.transform;
                    Renderer[] componentsInChildren = gameObject.GetComponentsInChildren<Renderer>();
                    screenRelativeTransitionHeight /= 2f;
                    lods[index1] = new LOD(screenRelativeTransitionHeight, componentsInChildren);
                }
                ++index1;
            }
            lodGroup.SetLODs(lods);
            lodGroup.RecalculateBounds();
            PrefabUtility.CreatePrefab(path, go, ReplacePrefabOptions.Default);
            Object.DestroyImmediate((Object)go);
        }

        public void OnDestroy()
        {
            SharedData.Instance.GeneralManager.Release();
        }

        public void OnSelectionChange()
        {
            SharedData.Instance.SelectionManager.OnSelectionChange();
            this.ForceRepaint = true;
        }

        public void Update()
        {
            if (Application.isPlaying)
                return;
            SharedData.Instance.UnityDispatcher.Update();
            SharedData.Instance.SelectionManager.Update();
            if (SharedData.Instance.GeneralManager.LoginState == LoginState.LoginSuccess)
                this.SimplygonScreen.Update();
            else
                this.LoginScreen.Update();
            if (!this.ForceRepaint)
                return;
            this.Repaint();
            this.ForceRepaint = false;
        }

        private void OnInspectorUpdate()
        {
            if (SharedData.Instance.GeneralManager.LoginState == LoginState.LoginSuccess)
                this.SimplygonScreen.OnInspectorUpdate();
            else
                this.LoginScreen.OnInspectorUpdate();
            if (SharedData.Instance.GeneralManager.JobManager != null && SharedData.Instance.GeneralManager.JobManager.ProcessingJobCount > 0)
                SharedData.Instance.WorkingIcon.Update();
            else
                SharedData.Instance.WorkingIcon.Reset();
            this.Repaint();
        }

        private void OnLostFocus()
        {
            EditorUtility.ClearProgressBar();
        }

        private void OnGUI()
        {
            if (SharedData.Instance.GeneralManager.LoginState == LoginState.LoginSuccess)
                this.SimplygonScreen.Show();
            else
                this.LoginScreen.Show();
            if (SharedData.Instance.GameObjectsToExport)
                EditorUtility.DisplayProgressBar("Exporting assets to Simplygon Cloud Package", "Please do not enter play mode until export has completed.", SharedData.Instance.ExportProgress);
            else if (SharedData.Instance.GameObjectsToImport)
                EditorUtility.DisplayProgressBar("Importing assets from Simplygon Cloud Package", "Please do not enter play mode until import has completed.", SharedData.Instance.ImportProgress);
            else if (SharedData.Instance.GameObjectsToUpload)
                EditorUtility.DisplayProgressBar("Uploading assets to Simplygon Cloud Package", "Please do not enter play mode until upload has completed.", SharedData.Instance.ExportProgress);
            else if (SharedData.Instance.GameObjectsToDownload)
                EditorUtility.DisplayProgressBar("Downloading assets from Simplygon Cloud Package", "Please do not enter play mode until download has completed.", SharedData.Instance.ImportProgress);
            else
                EditorUtility.ClearProgressBar();
            if (SharedData.Instance.ClientInfo != null && SharedData.Instance.ClientInfo.UnityInfo != null && (SharedData.Instance.ClientInfo.UnityInfo.MajorVersion > Simplygon.Unity.EditorPlugin.Properties.Settings.Default.MajorVersion || SharedData.Instance.ClientInfo.UnityInfo.MajorVersion == Simplygon.Unity.EditorPlugin.Properties.Settings.Default.MajorVersion && SharedData.Instance.ClientInfo.UnityInfo.MinorVersion > Simplygon.Unity.EditorPlugin.Properties.Settings.Default.MinorVersion || SharedData.Instance.ClientInfo.UnityInfo.MajorVersion == Simplygon.Unity.EditorPlugin.Properties.Settings.Default.MajorVersion && SharedData.Instance.ClientInfo.UnityInfo.MinorVersion == Simplygon.Unity.EditorPlugin.Properties.Settings.Default.MinorVersion && SharedData.Instance.ClientInfo.UnityInfo.BuildVersion > Simplygon.Unity.EditorPlugin.Properties.Settings.Default.BuildVersion))
            {
                EditorGUILayout.HelpBox(string.Format(CultureInfo.InvariantCulture, "Version {0}.{1}.{2} of the Simplygon plug-in is available.\nPlease download it from the Asset Store.", (object)SharedData.Instance.ClientInfo.UnityInfo.MajorVersion, (object)SharedData.Instance.ClientInfo.UnityInfo.MinorVersion, (object)SharedData.Instance.ClientInfo.UnityInfo.BuildVersion), MessageType.Info);
                if (Event.current.type == EventType.MouseDown && GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition))
                    Application.OpenURL("https://www.assetstore.unity3d.com/#/content/10144");
            }
            EditorGUILayout.HelpBox(string.Format(CultureInfo.InvariantCulture, "Version: {0}", (object)Simplygon.Unity.EditorPlugin.Properties.Settings.Default.Version), MessageType.None);
        }
    }
}
