﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.Jobs.JobUtils
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Users\nicolas_ezcurra\Desktop\Simplygon.dll

using Ionic.Zip;
using Ionic.Zlib;
using Simplygon.Cloud.Unity;
using Simplygon.Scene.Graph;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin.Jobs
{
    public class JobUtils
    {
        public static readonly string SimplygonDefaultTextureShaderChannelProperties = "SimplygonTextureShaderFallback";
        public static readonly string SimplygonDefaultColorShaderChannelProperties = "SimplygonColorShaderFallback";
        public static readonly string SimplygonDefaultShaderChannelProperties = "SimplygonShaderFallback";
        private static readonly string DefaultShaderPathPrefix = "Legacy Shaders/";

        internal static void PrintSceneHierarchy(Simplygon.Scene.Scene scene, Node topNode)
        {
            Debug.Log((object)string.Format(CultureInfo.InvariantCulture, "JobUtils::LogSceneHierarchy(): Parent: {0} ({1})", (object)topNode.Name, (object)topNode.Id));
            for (int index = 0; index < topNode.Children.Count; ++index)
            {
                Node node = scene.Nodes[topNode.Children[index]];
                Debug.Log((object)string.Format(CultureInfo.InvariantCulture, "JobUtils::LogSceneHierarchy(): \tChild: {0} ({1})", (object)node.Name, (object)node.Id));
            }
            for (int index = 0; index < topNode.Children.Count; ++index)
            {
                Node node = scene.Nodes[topNode.Children[index]];
                JobUtils.PrintSceneHierarchy(scene, node);
            }
        }

        public static void ZipDirectory(string inputPath, string outputPath, string outputFileName, Action<int, string> saveProgressHandler = null)
        {
            using (ZipFile zipFile = new ZipFile(Path.Combine(outputPath, outputFileName)))
            {
                zipFile.BufferSize = 1000000;
                zipFile.CodecBufferSize = 1000000;
                zipFile.CompressionLevel = Ionic.Zlib.CompressionLevel.BestSpeed;
                zipFile.AddDirectory(inputPath);
                if (saveProgressHandler != null)
                    zipFile.SaveProgress += (EventHandler<SaveProgressEventArgs>)((s, e) => saveProgressHandler(e.TotalBytesToTransfer == 0L ? 0 : (int)((double)e.BytesTransferred / (double)e.TotalBytesToTransfer * 100.0), e.CurrentEntry != null ? e.CurrentEntry.FileName : string.Empty));
                zipFile.Save();
            }
        }

        public static void UnZipToDirectory(string inputPath, string outputPath, string inputFileName, Action<int, string> extractProgressHandler = null)
        {
            try
            {
                using (ZipFile zipFile = ZipFile.Read(Path.Combine(inputPath, inputFileName)))
                {
                    if (extractProgressHandler != null)
                        zipFile.ExtractProgress += (EventHandler<ExtractProgressEventArgs>)((s, e) => extractProgressHandler(e.TotalBytesToTransfer == 0L ? 0 : (int)((double)e.BytesTransferred / (double)e.TotalBytesToTransfer * 100.0), e.CurrentEntry != null ? e.CurrentEntry.FileName : string.Empty));
                    foreach (ZipEntry zipEntry in zipFile)
                        zipEntry.Extract(inputPath, ExtractExistingFileAction.OverwriteSilently);
                }
            }
            catch (Exception ex)
            {
                Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Failed to decompress job resources in {0} to {1} ({2}\n{3})", (object)Path.Combine(inputPath, inputFileName), (object)outputPath, (object)ex.Message, (object)ex.StackTrace));
            }
        }

        internal static List<ShadingChannelProperties> GetDefaultShadingChannelProperties(Shader shader)
        {
            bool flag1 = false;
            bool flag2 = false;
            for (int propertyIdx = 0; propertyIdx < ShaderUtil.GetPropertyCount(shader); ++propertyIdx)
            {
                flag1 = ShaderUtil.GetPropertyName(shader, propertyIdx) == "_MainTex";
                flag2 = ShaderUtil.GetPropertyName(shader, propertyIdx) == "_Color";
                if (flag1)
                    break;
            }
            return new List<ShadingChannelProperties>((IEnumerable<ShadingChannelProperties>)JobUtils.GetDefaultShaderSettings()[flag1 ? JobUtils.SimplygonDefaultTextureShaderChannelProperties : (flag2 ? JobUtils.SimplygonDefaultColorShaderChannelProperties : JobUtils.SimplygonDefaultShaderChannelProperties)]);
        }

        public static string GetShaderLookupName(string shaderName)
        {
            string str = shaderName;
            if (shaderName == "Standard" || shaderName == "Standard (Specular setup)")
            {
                int result1 = 5;
                int result2 = 0;
                if (!int.TryParse(Application.unityVersion.Substring(0, 1), out result1))
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to parse Unity major version, will use default {0}.", (object)result1));
                if (!int.TryParse(Application.unityVersion.Substring(2, 1), out result2))
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to parse Unity minor version, will use default {0}.", (object)result2));
                if (result1 >= 5 && result2 >= 1)
                    str = string.Format(CultureInfo.InvariantCulture, "{0}{1}", (object)shaderName, (object)"5.1OrLater");
            }
            return str;
        }

        internal static Dictionary<string, List<ShadingChannelProperties>> GetDefaultShaderSettings()
        {
            Dictionary<string, List<ShadingChannelProperties>> dictionary1 = new Dictionary<string, List<ShadingChannelProperties>>();
            dictionary1.Add(JobUtils.SimplygonDefaultTextureShaderChannelProperties, new List<ShadingChannelProperties>()
      {
        new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.Diffuse, new List<BasicShadingNodeProperties>()
        {
          new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
        })
      });
            dictionary1.Add(JobUtils.SimplygonDefaultColorShaderChannelProperties, new List<ShadingChannelProperties>()
      {
        new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.Diffuse, new List<BasicShadingNodeProperties>()
        {
          new BasicShadingNodeProperties("_Color", "DiffuseColor", 0, BasicShadingNodeType.Color, "TexCoords0")
        })
      });
            dictionary1.Add(JobUtils.SimplygonDefaultShaderChannelProperties, new List<ShadingChannelProperties>()
      {
        new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.Diffuse, new List<BasicShadingNodeProperties>())
      });
            Dictionary<string, List<ShadingChannelProperties>> dictionary2 = dictionary1;
            string key1 = "Standard";
            List<ShadingChannelProperties> channelPropertiesList1 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList2 = channelPropertiesList1;
            string diffuse1 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes1 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes1.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes1.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes1.Add(new BasicShadingNodeProperties("_MetallicGlossMap", "MetallicGlossMap", 32, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes1.Add(new BasicShadingNodeProperties("_Mode", "Mode", 2, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes1.Add(new BasicShadingNodeProperties("_Cutoff", "Cutoff", 3, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes1.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.9 0.9 0.9 0.9"), "0_9Bound", 4, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes1.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1.9 1.9 1.9 1.9"), "1_9Bound", 25, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes1.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("2.9 2.9 2.9 2.9"), "2_9Bound", 6, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes1.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("3.9 3.9 3.9 3.9"), "3_9Bound", 7, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes1.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1 1 1 1"), "FullColorFullAlpha", 8, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes1.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.2 0.2 0.2 1"), "DefaultSpecularColor", 31, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes1 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(9, new int[2]
        {
          4,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(10, new int[2]
        {
          2,
          4
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(11, new int[2]
        {
          25,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          10,
          11
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(12, new int[2]
        {
          7,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          2,
          6
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(19, new int[2]
        {
          12,
          13
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(26, new int[2]
        {
          2,
          25
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(27, new int[2]
        {
          6,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(28, new int[2]
        {
          26,
          27
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(29, new int[2]
        {
          19,
          28
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(14, new int[2]
        {
          0,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(34, new int[4]
        {
          32,
          32,
          32,
          32
        }, new int[4]{ 0, 0, 0, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(30, new int[3]
        {
          8,
          31,
          34
        }, (int[]) null, ModifierShadingNodeType.Interpolate),
        new ModifierShadingNodeProperties(33, new int[2]
        {
          14,
          30
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(15, new int[4]
        {
          33,
          33,
          33,
          8
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          15,
          9
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          29,
          14
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(20, new int[2]
        {
          14,
          3
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(21, new int[4]
        {
          14,
          14,
          14,
          20
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(22, new int[2]
        {
          18,
          21
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(23, new int[2]
        {
          16,
          17
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(24, new int[2]
        {
          23,
          22
        }, (int[]) null, ModifierShadingNodeType.Add)
      };
            ShadingChannelProperties channelProperties1 = new ShadingChannelProperties(diffuse1, basicPropertyNodes1, modifierPropertyNodes1);
            channelPropertiesList2.Add(channelProperties1);
            List<ShadingChannelProperties> channelPropertiesList3 = channelPropertiesList1;
            string specular1 = Simplygon.Unity.EditorPlugin.Constants.Specular;
            List<BasicShadingNodeProperties> basicPropertyNodes2 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes2.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes2.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes2.Add(new BasicShadingNodeProperties("_Mode", "Mode", 2, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes2.Add(new BasicShadingNodeProperties("_Cutoff", "Cutoff", 3, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes2.Add(new BasicShadingNodeProperties("_MetallicGlossMap", "MetallicGlossMap", 4, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes2.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.2 0.2 0.2 1"), "DefaultSpecularColor", 5, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes2.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.9 0.9 0.9 0.9"), "0_9Bound", 6, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes2.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1.9 1.9 1.9 1.9"), "1_9Bound", 7, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes2.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("2.9 2.9 2.9 2.9"), "2_9Bound", 8, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes2.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("3.9 3.9 3.9 3.9"), "3_9Bound", 9, BasicShadingNodeType.Vector, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes2 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(10, new int[2]
        {
          6,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(11, new int[2]
        {
          9,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(12, new int[2]
        {
          2,
          8
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          11,
          12
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(14, new int[2]
        {
          10,
          13
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(15, new int[2]
        {
          2,
          6
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          7,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          15,
          16
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(24, new int[2]
        {
          2,
          7
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(25, new int[2]
        {
          8,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(26, new int[2]
        {
          24,
          25
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          0,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(35, new int[4]
        {
          4,
          4,
          4,
          4
        }, new int[4]{ 0, 0, 0, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(19, new int[3]
        {
          5,
          18,
          35
        }, (int[]) null, ModifierShadingNodeType.Interpolate),
        new ModifierShadingNodeProperties(20, new int[4]
        {
          19,
          19,
          19,
          4
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(21, new int[4]
        {
          18,
          18,
          18,
          18
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(22, new int[2]
        {
          21,
          3
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(23, new int[2]
        {
          22,
          20
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(27, new int[2]
        {
          14,
          20
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(32, new int[2]
        {
          23,
          17
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(29, new int[2]
        {
          20,
          21
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(34, new int[4]
        {
          29,
          29,
          29,
          4
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(33, new int[2]
        {
          34,
          26
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(30, new int[2]
        {
          27,
          32
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(31, new int[2]
        {
          30,
          33
        }, (int[]) null, ModifierShadingNodeType.Add)
      };
            ShadingChannelProperties channelProperties2 = new ShadingChannelProperties(specular1, basicPropertyNodes2, modifierPropertyNodes2);
            channelPropertiesList3.Add(channelProperties2);
            channelPropertiesList1.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.Normals, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_BumpMap", "NormalsTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            List<ShadingChannelProperties> channelPropertiesList4 = channelPropertiesList1;
            string opacity1 = Simplygon.Unity.EditorPlugin.Constants.Opacity;
            List<BasicShadingNodeProperties> basicPropertyNodes3 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes3.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes3.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes3.Add(new BasicShadingNodeProperties("_Mode", "Mode", 2, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes3.Add(new BasicShadingNodeProperties("_Cutoff", "Cutoff", 3, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes3.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.9 0.9 0.9 0.9"), "0_9Bound", 4, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes3.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1.9 1.9 1.9 1.9"), "1_9Bound", 25, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes3.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("2.9 2.9 2.9 2.9"), "2_9Bound", 6, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes3.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("3.9 3.9 3.9 3.9"), "3_9Bound", 7, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes3.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1 1 1 1"), "FullColorFullAlpha", 8, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes3 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(9, new int[2]
        {
          4,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(10, new int[2]
        {
          2,
          4
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(11, new int[2]
        {
          25,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          10,
          11
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(12, new int[2]
        {
          7,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          2,
          6
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(19, new int[2]
        {
          12,
          13
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(26, new int[2]
        {
          2,
          25
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(27, new int[2]
        {
          6,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(28, new int[2]
        {
          26,
          27
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(29, new int[2]
        {
          19,
          28
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(14, new int[2]
        {
          0,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(15, new int[4]
        {
          14,
          14,
          14,
          8
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          15,
          9
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          29,
          14
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(20, new int[2]
        {
          14,
          3
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(21, new int[4]
        {
          14,
          14,
          14,
          20
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(22, new int[2]
        {
          18,
          21
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(23, new int[2]
        {
          16,
          17
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(24, new int[2]
        {
          23,
          22
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(30, new int[4]
        {
          24,
          24,
          24,
          24
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties3 = new ShadingChannelProperties(opacity1, basicPropertyNodes3, modifierPropertyNodes3);
            channelPropertiesList4.Add(channelProperties3);
            List<ShadingChannelProperties> channelPropertiesList5 = channelPropertiesList1;
            string occlusion1 = Simplygon.Unity.EditorPlugin.Constants.Occlusion;
            List<BasicShadingNodeProperties> basicPropertyNodes4 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes4.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1 1 1 1"), "WhiteFullAlphaColor", 0, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes4.Add(new BasicShadingNodeProperties("_OcclusionMap", "OcclusionTexture", 1, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes4.Add(new BasicShadingNodeProperties("_OcclusionStrength", "OcclusionStrength", 2, BasicShadingNodeType.Float, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes4 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(3, new int[3]
        {
          0,
          1,
          2
        }, (int[]) null, ModifierShadingNodeType.Interpolate)
      };
            ShadingChannelProperties channelProperties4 = new ShadingChannelProperties(occlusion1, basicPropertyNodes4, modifierPropertyNodes4);
            channelPropertiesList5.Add(channelProperties4);
            channelPropertiesList1.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.Parallax, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_ParallaxMap", "ParallaxTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            channelPropertiesList1.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.DetailMask, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_DetailMask", "DetailMaskTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            List<ShadingChannelProperties> channelPropertiesList6 = channelPropertiesList1;
            string emission1 = Simplygon.Unity.EditorPlugin.Constants.Emission;
            List<BasicShadingNodeProperties> basicPropertyNodes5 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes5.Add(new BasicShadingNodeProperties("_EmissionMap", "EmissionTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes5.Add(new BasicShadingNodeProperties("_EmissionColorUI", "EmissionColorUI", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes5.Add(new BasicShadingNodeProperties("_EmissionScaleUI", "_EmissionScaleUI", 2, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes5.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 3, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes5.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 4, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes5.Add(new BasicShadingNodeProperties("_Mode", "Mode", 5, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes5.Add(new BasicShadingNodeProperties("_Cutoff", "Cutoff", 6, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes5.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.9 0.9 0.9 0.9"), "0_9Bound", 7, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes5.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1.9 1.9 1.9 1.9"), "1_9Bound", 8, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes5.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("2.9 2.9 2.9 2.9"), "2_9Bound", 9, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes5.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("3.9 3.9 3.9 3.9"), "3_9Bound", 10, BasicShadingNodeType.Vector, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes5 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(12, new int[2]
        {
          7,
          5
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          8,
          5
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(14, new int[2]
        {
          5,
          7
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(15, new int[2]
        {
          13,
          14
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          9,
          5
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          5,
          8
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          16,
          17
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(19, new int[2]
        {
          10,
          5
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(20, new int[2]
        {
          5,
          9
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(21, new int[2]
        {
          19,
          20
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(22, new int[2]
        {
          0,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(23, new int[2]
        {
          22,
          2
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(24, new int[2]
        {
          12,
          23
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(25, new int[2]
        {
          21,
          23
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(35, new int[2]
        {
          24,
          25
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(26, new int[2]
        {
          3,
          4
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(27, new int[4]
        {
          26,
          26,
          26,
          26
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(28, new int[2]
        {
          27,
          6
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(29, new int[2]
        {
          23,
          28
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(30, new int[2]
        {
          15,
          29
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(31, new int[2]
        {
          23,
          27
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(32, new int[2]
        {
          31,
          18
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(33, new int[2]
        {
          35,
          30
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(34, new int[2]
        {
          33,
          32
        }, (int[]) null, ModifierShadingNodeType.Add)
      };
            ShadingChannelProperties channelProperties5 = new ShadingChannelProperties(emission1, basicPropertyNodes5, modifierPropertyNodes5);
            channelPropertiesList6.Add(channelProperties5);
            channelPropertiesList1.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.SecondaryDiffuse, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_DetailAlbedoMap", "DetailDiffuseTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            channelPropertiesList1.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.SecondaryNormals, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_DetailNormalMap", "DetailNormalsTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            List<ShadingChannelProperties> channelPropertiesList7 = channelPropertiesList1;
            dictionary2.Add(key1, channelPropertiesList7);
            Dictionary<string, List<ShadingChannelProperties>> dictionary3 = dictionary1;
            string key2 = "Standard5.1OrLater";
            List<ShadingChannelProperties> channelPropertiesList8 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList9 = channelPropertiesList8;
            string diffuse2 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes6 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes6.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes6.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes6.Add(new BasicShadingNodeProperties("_MetallicGlossMap", "MetallicGlossMap", 32, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes6.Add(new BasicShadingNodeProperties("_Mode", "Mode", 2, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes6.Add(new BasicShadingNodeProperties("_Cutoff", "Cutoff", 3, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes6.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.9 0.9 0.9 0.9"), "0_9Bound", 4, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes6.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1.9 1.9 1.9 1.9"), "1_9Bound", 25, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes6.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("2.9 2.9 2.9 2.9"), "2_9Bound", 6, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes6.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("3.9 3.9 3.9 3.9"), "3_9Bound", 7, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes6.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1 1 1 1"), "FullColorFullAlpha", 8, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes6.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.2 0.2 0.2 1"), "DefaultSpecularColor", 31, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes6 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(9, new int[2]
        {
          4,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(10, new int[2]
        {
          2,
          4
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(11, new int[2]
        {
          25,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          10,
          11
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(12, new int[2]
        {
          7,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          2,
          6
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(19, new int[2]
        {
          12,
          13
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(26, new int[2]
        {
          2,
          25
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(27, new int[2]
        {
          6,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(28, new int[2]
        {
          26,
          27
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(29, new int[2]
        {
          19,
          28
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(14, new int[2]
        {
          0,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(34, new int[4]
        {
          32,
          32,
          32,
          32
        }, new int[4]{ 0, 0, 0, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(30, new int[3]
        {
          8,
          31,
          34
        }, (int[]) null, ModifierShadingNodeType.Interpolate),
        new ModifierShadingNodeProperties(33, new int[2]
        {
          14,
          30
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(15, new int[4]
        {
          33,
          33,
          33,
          8
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          15,
          9
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          29,
          14
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(20, new int[2]
        {
          14,
          3
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(21, new int[4]
        {
          14,
          14,
          14,
          20
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(22, new int[2]
        {
          18,
          21
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(23, new int[2]
        {
          16,
          17
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(24, new int[2]
        {
          23,
          22
        }, (int[]) null, ModifierShadingNodeType.Add)
      };
            ShadingChannelProperties channelProperties6 = new ShadingChannelProperties(diffuse2, basicPropertyNodes6, modifierPropertyNodes6);
            channelPropertiesList9.Add(channelProperties6);
            List<ShadingChannelProperties> channelPropertiesList10 = channelPropertiesList8;
            string specular2 = Simplygon.Unity.EditorPlugin.Constants.Specular;
            List<BasicShadingNodeProperties> basicPropertyNodes7 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes7.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes7.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes7.Add(new BasicShadingNodeProperties("_Mode", "Mode", 2, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes7.Add(new BasicShadingNodeProperties("_Cutoff", "Cutoff", 3, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes7.Add(new BasicShadingNodeProperties("_MetallicGlossMap", "MetallicGlossMap", 4, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes7.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.2 0.2 0.2 1"), "DefaultSpecularColor", 5, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes7.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.9 0.9 0.9 0.9"), "0_9Bound", 6, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes7.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1.9 1.9 1.9 1.9"), "1_9Bound", 7, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes7.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("2.9 2.9 2.9 2.9"), "2_9Bound", 8, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes7.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("3.9 3.9 3.9 3.9"), "3_9Bound", 9, BasicShadingNodeType.Vector, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes7 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(10, new int[2]
        {
          6,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(11, new int[2]
        {
          9,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(12, new int[2]
        {
          2,
          8
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          11,
          12
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(14, new int[2]
        {
          10,
          13
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(15, new int[2]
        {
          2,
          6
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          7,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          15,
          16
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(24, new int[2]
        {
          2,
          7
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(25, new int[2]
        {
          8,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(26, new int[2]
        {
          24,
          25
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          0,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(35, new int[4]
        {
          4,
          4,
          4,
          4
        }, new int[4]{ 0, 0, 0, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(19, new int[3]
        {
          5,
          18,
          35
        }, (int[]) null, ModifierShadingNodeType.Interpolate),
        new ModifierShadingNodeProperties(20, new int[4]
        {
          19,
          19,
          19,
          4
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(21, new int[4]
        {
          18,
          18,
          18,
          18
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(22, new int[2]
        {
          21,
          3
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(23, new int[2]
        {
          22,
          20
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(27, new int[2]
        {
          14,
          20
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(32, new int[2]
        {
          23,
          17
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(29, new int[2]
        {
          20,
          21
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(34, new int[4]
        {
          29,
          29,
          29,
          4
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(33, new int[2]
        {
          34,
          26
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(30, new int[2]
        {
          27,
          32
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(31, new int[2]
        {
          30,
          33
        }, (int[]) null, ModifierShadingNodeType.Add)
      };
            ShadingChannelProperties channelProperties7 = new ShadingChannelProperties(specular2, basicPropertyNodes7, modifierPropertyNodes7);
            channelPropertiesList10.Add(channelProperties7);
            channelPropertiesList8.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.Normals, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_BumpMap", "NormalsTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            List<ShadingChannelProperties> channelPropertiesList11 = channelPropertiesList8;
            string opacity2 = Simplygon.Unity.EditorPlugin.Constants.Opacity;
            List<BasicShadingNodeProperties> basicPropertyNodes8 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes8.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes8.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes8.Add(new BasicShadingNodeProperties("_Mode", "Mode", 2, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes8.Add(new BasicShadingNodeProperties("_Cutoff", "Cutoff", 3, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes8.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.9 0.9 0.9 0.9"), "0_9Bound", 4, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes8.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1.9 1.9 1.9 1.9"), "1_9Bound", 25, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes8.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("2.9 2.9 2.9 2.9"), "2_9Bound", 6, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes8.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("3.9 3.9 3.9 3.9"), "3_9Bound", 7, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes8.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1 1 1 1"), "FullColorFullAlpha", 8, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes8 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(9, new int[2]
        {
          4,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(10, new int[2]
        {
          2,
          4
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(11, new int[2]
        {
          25,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          10,
          11
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(12, new int[2]
        {
          7,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          2,
          6
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(19, new int[2]
        {
          12,
          13
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(26, new int[2]
        {
          2,
          25
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(27, new int[2]
        {
          6,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(28, new int[2]
        {
          26,
          27
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(29, new int[2]
        {
          19,
          28
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(14, new int[2]
        {
          0,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(15, new int[4]
        {
          14,
          14,
          14,
          8
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          15,
          9
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          29,
          14
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(20, new int[2]
        {
          14,
          3
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(21, new int[4]
        {
          14,
          14,
          14,
          20
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(22, new int[2]
        {
          18,
          21
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(23, new int[2]
        {
          16,
          17
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(24, new int[2]
        {
          23,
          22
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(30, new int[4]
        {
          24,
          24,
          24,
          24
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties8 = new ShadingChannelProperties(opacity2, basicPropertyNodes8, modifierPropertyNodes8);
            channelPropertiesList11.Add(channelProperties8);
            List<ShadingChannelProperties> channelPropertiesList12 = channelPropertiesList8;
            string occlusion2 = Simplygon.Unity.EditorPlugin.Constants.Occlusion;
            List<BasicShadingNodeProperties> basicPropertyNodes9 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes9.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1 1 1 1"), "WhiteFullAlphaColor", 0, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes9.Add(new BasicShadingNodeProperties("_OcclusionMap", "OcclusionTexture", 1, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes9.Add(new BasicShadingNodeProperties("_OcclusionStrength", "OcclusionStrength", 2, BasicShadingNodeType.Float, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes9 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(3, new int[3]
        {
          0,
          1,
          2
        }, (int[]) null, ModifierShadingNodeType.Interpolate)
      };
            ShadingChannelProperties channelProperties9 = new ShadingChannelProperties(occlusion2, basicPropertyNodes9, modifierPropertyNodes9);
            channelPropertiesList12.Add(channelProperties9);
            channelPropertiesList8.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.Parallax, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_ParallaxMap", "ParallaxTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            channelPropertiesList8.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.DetailMask, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_DetailMask", "DetailMaskTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            List<ShadingChannelProperties> channelPropertiesList13 = channelPropertiesList8;
            string emission2 = Simplygon.Unity.EditorPlugin.Constants.Emission;
            List<BasicShadingNodeProperties> basicPropertyNodes10 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes10.Add(new BasicShadingNodeProperties("_EmissionMap", "EmissionTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes10.Add(new BasicShadingNodeProperties("_EmissionColor", "EmissionColorUI", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes10.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 3, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes10.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 4, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes10.Add(new BasicShadingNodeProperties("_Mode", "Mode", 5, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes10.Add(new BasicShadingNodeProperties("_Cutoff", "Cutoff", 6, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes10.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.9 0.9 0.9 0.9"), "0_9Bound", 7, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes10.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1.9 1.9 1.9 1.9"), "1_9Bound", 8, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes10.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("2.9 2.9 2.9 2.9"), "2_9Bound", 9, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes10.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("3.9 3.9 3.9 3.9"), "3_9Bound", 10, BasicShadingNodeType.Vector, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes10 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(12, new int[2]
        {
          7,
          5
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          8,
          5
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(14, new int[2]
        {
          5,
          7
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(15, new int[2]
        {
          13,
          14
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          9,
          5
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          5,
          8
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          16,
          17
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(19, new int[2]
        {
          10,
          5
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(20, new int[2]
        {
          5,
          9
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(21, new int[2]
        {
          19,
          20
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(22, new int[2]
        {
          0,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(24, new int[2]
        {
          12,
          22
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(25, new int[2]
        {
          21,
          22
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(35, new int[2]
        {
          24,
          25
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(26, new int[2]
        {
          3,
          4
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(27, new int[4]
        {
          26,
          26,
          26,
          26
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(28, new int[2]
        {
          27,
          6
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(29, new int[2]
        {
          22,
          28
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(30, new int[2]
        {
          15,
          29
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(31, new int[2]
        {
          22,
          27
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(32, new int[2]
        {
          31,
          18
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(33, new int[2]
        {
          35,
          30
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(34, new int[2]
        {
          33,
          32
        }, (int[]) null, ModifierShadingNodeType.Add)
      };
            ShadingChannelProperties channelProperties10 = new ShadingChannelProperties(emission2, basicPropertyNodes10, modifierPropertyNodes10);
            channelPropertiesList13.Add(channelProperties10);
            channelPropertiesList8.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.SecondaryDiffuse, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_DetailAlbedoMap", "DetailDiffuseTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            channelPropertiesList8.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.SecondaryNormals, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_DetailNormalMap", "DetailNormalsTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            List<ShadingChannelProperties> channelPropertiesList14 = channelPropertiesList8;
            dictionary3.Add(key2, channelPropertiesList14);
            Dictionary<string, List<ShadingChannelProperties>> dictionary4 = dictionary1;
            string key3 = "Standard (Specular setup)";
            List<ShadingChannelProperties> channelPropertiesList15 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList16 = channelPropertiesList15;
            string diffuse3 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes11 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes11.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes11.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes11.Add(new BasicShadingNodeProperties("_Mode", "Mode", 2, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes11.Add(new BasicShadingNodeProperties("_Cutoff", "Cutoff", 3, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes11.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.9 0.9 0.9 0.9"), "0_9Bound", 4, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes11.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1.9 1.9 1.9 1.9"), "1_9Bound", 25, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes11.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("2.9 2.9 2.9 2.9"), "2_9Bound", 6, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes11.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("3.9 3.9 3.9 3.9"), "3_9Bound", 7, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes11.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1 1 1 1"), "FullColorFullAlpha", 8, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes11 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(9, new int[2]
        {
          4,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(10, new int[2]
        {
          2,
          4
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(11, new int[2]
        {
          25,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          10,
          11
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(12, new int[2]
        {
          7,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          2,
          6
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(19, new int[2]
        {
          12,
          13
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(26, new int[2]
        {
          2,
          25
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(27, new int[2]
        {
          6,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(28, new int[2]
        {
          26,
          27
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(29, new int[2]
        {
          19,
          28
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(14, new int[2]
        {
          0,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(15, new int[4]
        {
          14,
          14,
          14,
          8
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          15,
          9
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          29,
          14
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(20, new int[2]
        {
          14,
          3
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(21, new int[4]
        {
          14,
          14,
          14,
          20
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(22, new int[2]
        {
          18,
          21
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(23, new int[2]
        {
          16,
          17
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(24, new int[2]
        {
          23,
          22
        }, (int[]) null, ModifierShadingNodeType.Add)
      };
            ShadingChannelProperties channelProperties11 = new ShadingChannelProperties(diffuse3, basicPropertyNodes11, modifierPropertyNodes11);
            channelPropertiesList16.Add(channelProperties11);
            List<ShadingChannelProperties> channelPropertiesList17 = channelPropertiesList15;
            string specular3 = Simplygon.Unity.EditorPlugin.Constants.Specular;
            List<BasicShadingNodeProperties> basicPropertyNodes12 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes12.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes12.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes12.Add(new BasicShadingNodeProperties("_Mode", "Mode", 2, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes12.Add(new BasicShadingNodeProperties("_Cutoff", "Cutoff", 3, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes12.Add(new BasicShadingNodeProperties("_SpecGlossMap", "SpecularTexture", 4, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes12.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.9 0.9 0.9 0.9"), "0_9Bound", 6, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes12.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1.9 1.9 1.9 1.9"), "1_9Bound", 7, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes12.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("2.9 2.9 2.9 2.9"), "2_9Bound", 8, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes12.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("3.9 3.9 3.9 3.9"), "3_9Bound", 9, BasicShadingNodeType.Vector, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes12 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(10, new int[2]
        {
          6,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(11, new int[2]
        {
          9,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(12, new int[2]
        {
          2,
          8
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          11,
          12
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(14, new int[2]
        {
          10,
          13
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(15, new int[2]
        {
          2,
          6
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          7,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          15,
          16
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(24, new int[2]
        {
          2,
          7
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(25, new int[2]
        {
          8,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(26, new int[2]
        {
          24,
          25
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          0,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(21, new int[4]
        {
          18,
          18,
          18,
          18
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(22, new int[2]
        {
          21,
          3
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(23, new int[2]
        {
          22,
          4
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(27, new int[2]
        {
          14,
          4
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(32, new int[2]
        {
          23,
          17
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(29, new int[2]
        {
          4,
          21
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(34, new int[4]
        {
          29,
          29,
          29,
          4
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(33, new int[2]
        {
          34,
          26
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(30, new int[2]
        {
          27,
          32
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(31, new int[2]
        {
          30,
          33
        }, (int[]) null, ModifierShadingNodeType.Add)
      };
            ShadingChannelProperties channelProperties12 = new ShadingChannelProperties(specular3, basicPropertyNodes12, modifierPropertyNodes12);
            channelPropertiesList17.Add(channelProperties12);
            channelPropertiesList15.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.Normals, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_BumpMap", "NormalsTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            List<ShadingChannelProperties> channelPropertiesList18 = channelPropertiesList15;
            string opacity3 = Simplygon.Unity.EditorPlugin.Constants.Opacity;
            List<BasicShadingNodeProperties> basicPropertyNodes13 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes13.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes13.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes13.Add(new BasicShadingNodeProperties("_Mode", "Mode", 2, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes13.Add(new BasicShadingNodeProperties("_Cutoff", "Cutoff", 3, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes13.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.9 0.9 0.9 0.9"), "0_9Bound", 4, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes13.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1.9 1.9 1.9 1.9"), "1_9Bound", 25, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes13.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("2.9 2.9 2.9 2.9"), "2_9Bound", 6, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes13.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("3.9 3.9 3.9 3.9"), "3_9Bound", 7, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes13.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1 1 1 1"), "FullColorFullAlpha", 8, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes13 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(9, new int[2]
        {
          4,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(10, new int[2]
        {
          2,
          4
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(11, new int[2]
        {
          25,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          10,
          11
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(12, new int[2]
        {
          7,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          2,
          6
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(19, new int[2]
        {
          12,
          13
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(26, new int[2]
        {
          2,
          25
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(27, new int[2]
        {
          6,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(28, new int[2]
        {
          26,
          27
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(29, new int[2]
        {
          19,
          28
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(14, new int[2]
        {
          0,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(15, new int[4]
        {
          14,
          14,
          14,
          8
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          15,
          9
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          29,
          14
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(20, new int[2]
        {
          14,
          3
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(21, new int[4]
        {
          14,
          14,
          14,
          20
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(22, new int[2]
        {
          18,
          21
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(23, new int[2]
        {
          16,
          17
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(24, new int[2]
        {
          23,
          22
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(30, new int[4]
        {
          24,
          24,
          24,
          24
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties13 = new ShadingChannelProperties(opacity3, basicPropertyNodes13, modifierPropertyNodes13);
            channelPropertiesList18.Add(channelProperties13);
            List<ShadingChannelProperties> channelPropertiesList19 = channelPropertiesList15;
            string occlusion3 = Simplygon.Unity.EditorPlugin.Constants.Occlusion;
            List<BasicShadingNodeProperties> basicPropertyNodes14 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes14.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1 1 1 1"), "WhiteFullAlphaColor", 0, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes14.Add(new BasicShadingNodeProperties("_OcclusionMap", "OcclusionTexture", 1, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes14.Add(new BasicShadingNodeProperties("_OcclusionStrength", "OcclusionStrength", 2, BasicShadingNodeType.Float, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes14 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(3, new int[3]
        {
          0,
          1,
          2
        }, (int[]) null, ModifierShadingNodeType.Interpolate)
      };
            ShadingChannelProperties channelProperties14 = new ShadingChannelProperties(occlusion3, basicPropertyNodes14, modifierPropertyNodes14);
            channelPropertiesList19.Add(channelProperties14);
            channelPropertiesList15.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.Parallax, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_ParallaxMap", "ParallaxTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            channelPropertiesList15.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.DetailMask, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_DetailMask", "DetailMaskTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            List<ShadingChannelProperties> channelPropertiesList20 = channelPropertiesList15;
            string emission3 = Simplygon.Unity.EditorPlugin.Constants.Emission;
            List<BasicShadingNodeProperties> basicPropertyNodes15 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes15.Add(new BasicShadingNodeProperties("_EmissionMap", "EmissionTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes15.Add(new BasicShadingNodeProperties("_EmissionColorUI", "EmissionColorUI", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes15.Add(new BasicShadingNodeProperties("_EmissionScaleUI", "_EmissionScaleUI", 2, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes15.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 3, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes15.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 4, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes15.Add(new BasicShadingNodeProperties("_Mode", "Mode", 5, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes15.Add(new BasicShadingNodeProperties("_Cutoff", "Cutoff", 6, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes15.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.9 0.9 0.9 0.9"), "0_9Bound", 7, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes15.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1.9 1.9 1.9 1.9"), "1_9Bound", 8, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes15.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("2.9 2.9 2.9 2.9"), "2_9Bound", 9, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes15.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("3.9 3.9 3.9 3.9"), "3_9Bound", 10, BasicShadingNodeType.Vector, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes15 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(12, new int[2]
        {
          7,
          5
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          8,
          5
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(14, new int[2]
        {
          5,
          7
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(15, new int[2]
        {
          13,
          14
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          9,
          5
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          5,
          8
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          16,
          17
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(19, new int[2]
        {
          10,
          5
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(20, new int[2]
        {
          5,
          9
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(21, new int[2]
        {
          19,
          20
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(22, new int[2]
        {
          0,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(23, new int[2]
        {
          22,
          2
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(24, new int[2]
        {
          12,
          23
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(25, new int[2]
        {
          21,
          23
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(35, new int[2]
        {
          24,
          25
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(26, new int[2]
        {
          3,
          4
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(27, new int[4]
        {
          26,
          26,
          26,
          26
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(28, new int[2]
        {
          27,
          6
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(29, new int[2]
        {
          23,
          28
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(30, new int[2]
        {
          15,
          29
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(31, new int[2]
        {
          23,
          27
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(32, new int[2]
        {
          31,
          18
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(33, new int[2]
        {
          35,
          30
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(34, new int[2]
        {
          33,
          32
        }, (int[]) null, ModifierShadingNodeType.Add)
      };
            ShadingChannelProperties channelProperties15 = new ShadingChannelProperties(emission3, basicPropertyNodes15, modifierPropertyNodes15);
            channelPropertiesList20.Add(channelProperties15);
            channelPropertiesList15.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.SecondaryDiffuse, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_DetailAlbedoMap", "DetailDiffuseTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            channelPropertiesList15.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.SecondaryNormals, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_DetailNormalMap", "DetailNormalsTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            List<ShadingChannelProperties> channelPropertiesList21 = channelPropertiesList15;
            dictionary4.Add(key3, channelPropertiesList21);
            Dictionary<string, List<ShadingChannelProperties>> dictionary5 = dictionary1;
            string key4 = "Standard (Specular setup)5.1OrLater";
            List<ShadingChannelProperties> channelPropertiesList22 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList23 = channelPropertiesList22;
            string diffuse4 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes16 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes16.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes16.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes16.Add(new BasicShadingNodeProperties("_Mode", "Mode", 2, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes16.Add(new BasicShadingNodeProperties("_Cutoff", "Cutoff", 3, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes16.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.9 0.9 0.9 0.9"), "0_9Bound", 4, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes16.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1.9 1.9 1.9 1.9"), "1_9Bound", 25, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes16.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("2.9 2.9 2.9 2.9"), "2_9Bound", 6, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes16.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("3.9 3.9 3.9 3.9"), "3_9Bound", 7, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes16.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1 1 1 1"), "FullColorFullAlpha", 8, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes16 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(9, new int[2]
        {
          4,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(10, new int[2]
        {
          2,
          4
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(11, new int[2]
        {
          25,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          10,
          11
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(12, new int[2]
        {
          7,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          2,
          6
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(19, new int[2]
        {
          12,
          13
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(26, new int[2]
        {
          2,
          25
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(27, new int[2]
        {
          6,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(28, new int[2]
        {
          26,
          27
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(29, new int[2]
        {
          19,
          28
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(14, new int[2]
        {
          0,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(15, new int[4]
        {
          14,
          14,
          14,
          8
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          15,
          9
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          29,
          14
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(20, new int[2]
        {
          14,
          3
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(21, new int[4]
        {
          14,
          14,
          14,
          20
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(22, new int[2]
        {
          18,
          21
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(23, new int[2]
        {
          16,
          17
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(24, new int[2]
        {
          23,
          22
        }, (int[]) null, ModifierShadingNodeType.Add)
      };
            ShadingChannelProperties channelProperties16 = new ShadingChannelProperties(diffuse4, basicPropertyNodes16, modifierPropertyNodes16);
            channelPropertiesList23.Add(channelProperties16);
            List<ShadingChannelProperties> channelPropertiesList24 = channelPropertiesList22;
            string specular4 = Simplygon.Unity.EditorPlugin.Constants.Specular;
            List<BasicShadingNodeProperties> basicPropertyNodes17 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes17.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes17.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes17.Add(new BasicShadingNodeProperties("_Mode", "Mode", 2, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes17.Add(new BasicShadingNodeProperties("_Cutoff", "Cutoff", 3, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes17.Add(new BasicShadingNodeProperties("_SpecGlossMap", "SpecularTexture", 4, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes17.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.9 0.9 0.9 0.9"), "0_9Bound", 6, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes17.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1.9 1.9 1.9 1.9"), "1_9Bound", 7, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes17.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("2.9 2.9 2.9 2.9"), "2_9Bound", 8, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes17.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("3.9 3.9 3.9 3.9"), "3_9Bound", 9, BasicShadingNodeType.Vector, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes17 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(10, new int[2]
        {
          6,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(11, new int[2]
        {
          9,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(12, new int[2]
        {
          2,
          8
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          11,
          12
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(14, new int[2]
        {
          10,
          13
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(15, new int[2]
        {
          2,
          6
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          7,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          15,
          16
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(24, new int[2]
        {
          2,
          7
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(25, new int[2]
        {
          8,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(26, new int[2]
        {
          24,
          25
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          0,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(21, new int[4]
        {
          18,
          18,
          18,
          18
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(22, new int[2]
        {
          21,
          3
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(23, new int[2]
        {
          22,
          4
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(27, new int[2]
        {
          14,
          4
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(32, new int[2]
        {
          23,
          17
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(29, new int[2]
        {
          4,
          21
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(34, new int[4]
        {
          29,
          29,
          29,
          4
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(33, new int[2]
        {
          34,
          26
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(30, new int[2]
        {
          27,
          32
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(31, new int[2]
        {
          30,
          33
        }, (int[]) null, ModifierShadingNodeType.Add)
      };
            ShadingChannelProperties channelProperties17 = new ShadingChannelProperties(specular4, basicPropertyNodes17, modifierPropertyNodes17);
            channelPropertiesList24.Add(channelProperties17);
            channelPropertiesList22.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.Normals, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_BumpMap", "NormalsTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            List<ShadingChannelProperties> channelPropertiesList25 = channelPropertiesList22;
            string opacity4 = Simplygon.Unity.EditorPlugin.Constants.Opacity;
            List<BasicShadingNodeProperties> basicPropertyNodes18 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes18.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes18.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes18.Add(new BasicShadingNodeProperties("_Mode", "Mode", 2, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes18.Add(new BasicShadingNodeProperties("_Cutoff", "Cutoff", 3, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes18.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.9 0.9 0.9 0.9"), "0_9Bound", 4, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes18.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1.9 1.9 1.9 1.9"), "1_9Bound", 25, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes18.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("2.9 2.9 2.9 2.9"), "2_9Bound", 6, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes18.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("3.9 3.9 3.9 3.9"), "3_9Bound", 7, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes18.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1 1 1 1"), "FullColorFullAlpha", 8, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes18 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(9, new int[2]
        {
          4,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(10, new int[2]
        {
          2,
          4
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(11, new int[2]
        {
          25,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          10,
          11
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(12, new int[2]
        {
          7,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          2,
          6
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(19, new int[2]
        {
          12,
          13
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(26, new int[2]
        {
          2,
          25
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(27, new int[2]
        {
          6,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(28, new int[2]
        {
          26,
          27
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(29, new int[2]
        {
          19,
          28
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(14, new int[2]
        {
          0,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(15, new int[4]
        {
          14,
          14,
          14,
          8
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          15,
          9
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          29,
          14
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(20, new int[2]
        {
          14,
          3
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(21, new int[4]
        {
          14,
          14,
          14,
          20
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(22, new int[2]
        {
          18,
          21
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(23, new int[2]
        {
          16,
          17
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(24, new int[2]
        {
          23,
          22
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(30, new int[4]
        {
          24,
          24,
          24,
          24
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties18 = new ShadingChannelProperties(opacity4, basicPropertyNodes18, modifierPropertyNodes18);
            channelPropertiesList25.Add(channelProperties18);
            List<ShadingChannelProperties> channelPropertiesList26 = channelPropertiesList22;
            string occlusion4 = Simplygon.Unity.EditorPlugin.Constants.Occlusion;
            List<BasicShadingNodeProperties> basicPropertyNodes19 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes19.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1 1 1 1"), "WhiteFullAlphaColor", 0, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes19.Add(new BasicShadingNodeProperties("_OcclusionMap", "OcclusionTexture", 1, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes19.Add(new BasicShadingNodeProperties("_OcclusionStrength", "OcclusionStrength", 2, BasicShadingNodeType.Float, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes19 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(3, new int[3]
        {
          0,
          1,
          2
        }, (int[]) null, ModifierShadingNodeType.Interpolate)
      };
            ShadingChannelProperties channelProperties19 = new ShadingChannelProperties(occlusion4, basicPropertyNodes19, modifierPropertyNodes19);
            channelPropertiesList26.Add(channelProperties19);
            channelPropertiesList22.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.Parallax, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_ParallaxMap", "ParallaxTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            channelPropertiesList22.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.DetailMask, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_DetailMask", "DetailMaskTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            List<ShadingChannelProperties> channelPropertiesList27 = channelPropertiesList22;
            string emission4 = Simplygon.Unity.EditorPlugin.Constants.Emission;
            List<BasicShadingNodeProperties> basicPropertyNodes20 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes20.Add(new BasicShadingNodeProperties("_EmissionMap", "EmissionTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes20.Add(new BasicShadingNodeProperties("_EmissionColor", "EmissionColorUI", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes20.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 3, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes20.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 4, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes20.Add(new BasicShadingNodeProperties("_Mode", "Mode", 5, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes20.Add(new BasicShadingNodeProperties("_Cutoff", "Cutoff", 6, BasicShadingNodeType.Float, "TexCoords0"));
            basicPropertyNodes20.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0.9 0.9 0.9 0.9"), "0_9Bound", 7, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes20.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1.9 1.9 1.9 1.9"), "1_9Bound", 8, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes20.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("2.9 2.9 2.9 2.9"), "2_9Bound", 9, BasicShadingNodeType.Vector, "TexCoords0"));
            basicPropertyNodes20.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("3.9 3.9 3.9 3.9"), "3_9Bound", 10, BasicShadingNodeType.Vector, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes20 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(12, new int[2]
        {
          7,
          5
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          8,
          5
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(14, new int[2]
        {
          5,
          7
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(15, new int[2]
        {
          13,
          14
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          9,
          5
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          5,
          8
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          16,
          17
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(19, new int[2]
        {
          10,
          5
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(20, new int[2]
        {
          5,
          9
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(21, new int[2]
        {
          19,
          20
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(22, new int[2]
        {
          0,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(24, new int[2]
        {
          12,
          22
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(25, new int[2]
        {
          21,
          22
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(35, new int[2]
        {
          24,
          25
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(26, new int[2]
        {
          3,
          4
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(27, new int[4]
        {
          26,
          26,
          26,
          26
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(28, new int[2]
        {
          27,
          6
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(29, new int[2]
        {
          22,
          28
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(30, new int[2]
        {
          15,
          29
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(31, new int[2]
        {
          22,
          27
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(32, new int[2]
        {
          31,
          18
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(33, new int[2]
        {
          35,
          30
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(34, new int[2]
        {
          33,
          32
        }, (int[]) null, ModifierShadingNodeType.Add)
      };
            ShadingChannelProperties channelProperties20 = new ShadingChannelProperties(emission4, basicPropertyNodes20, modifierPropertyNodes20);
            channelPropertiesList27.Add(channelProperties20);
            channelPropertiesList22.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.SecondaryDiffuse, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_DetailAlbedoMap", "DetailDiffuseTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            channelPropertiesList22.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.SecondaryNormals, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_DetailNormalMap", "DetailNormalsTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            List<ShadingChannelProperties> channelPropertiesList28 = channelPropertiesList22;
            dictionary5.Add(key4, channelPropertiesList28);
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Bumped Diffuse", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Bumped Specular", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            Dictionary<string, List<ShadingChannelProperties>> dictionary6 = dictionary1;
            string key5 = string.Format(CultureInfo.InvariantCulture, "{0}Decal", (object)JobUtils.DefaultShaderPathPrefix);
            List<ShadingChannelProperties> channelPropertiesList29 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList30 = channelPropertiesList29;
            string diffuse5 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes21 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes21.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("1 1 1 1"), "FullColorAndAlpha", 0, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes21.Add(new BasicShadingNodeProperties("_DecalTex", "DiffuseDecalTexture", 1, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes21.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 2, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes21.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 3, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes21 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(4, new int[4]
        {
          1,
          1,
          1,
          -1
        }, new int[4]{ 0, 1, 2, -1 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(5, new int[4]
        {
          1,
          1,
          1,
          -1
        }, new int[4]{ 3, 3, 3, -1 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(6, new int[2]
        {
          5,
          4
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(7, new int[2]
        {
          0,
          5
        }, (int[]) null, ModifierShadingNodeType.Subtract),
        new ModifierShadingNodeProperties(8, new int[2]
        {
          7,
          2
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(9, new int[2]
        {
          8,
          6
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(10, new int[2]
        {
          9,
          3
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(11, new int[4]
        {
          10,
          10,
          10,
          0
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties21 = new ShadingChannelProperties(diffuse5, basicPropertyNodes21, modifierPropertyNodes21);
            channelPropertiesList30.Add(channelProperties21);
            List<ShadingChannelProperties> channelPropertiesList31 = channelPropertiesList29;
            dictionary6.Add(key5, channelPropertiesList31);
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Diffuse", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties()
      });
            Dictionary<string, List<ShadingChannelProperties>> dictionary7 = dictionary1;
            string key6 = string.Format(CultureInfo.InvariantCulture, "{0}Diffuse Detail", (object)JobUtils.DefaultShaderPathPrefix);
            List<ShadingChannelProperties> channelPropertiesList32 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList33 = channelPropertiesList32;
            string diffuse6 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes22 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes22.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes22.Add(new BasicShadingNodeProperties("_Detail", "DiffuseDetailTexture", 1, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes22.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 2, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes22.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0 0 0 1"), "FullAlpha", 3, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes22 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(4, new int[2]
        {
          1,
          0
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(5, new int[2]
        {
          4,
          2
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(6, new int[4]
        {
          5,
          5,
          5,
          3
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties22 = new ShadingChannelProperties(diffuse6, basicPropertyNodes22, modifierPropertyNodes22);
            channelPropertiesList33.Add(channelProperties22);
            List<ShadingChannelProperties> channelPropertiesList34 = channelPropertiesList32;
            dictionary7.Add(key6, channelPropertiesList34);
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Diffuse Fast", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Parallax Diffuse", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Parallax Specular", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Specular", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}VertexLit", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Bumped Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Bumped Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Bumped Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Bumped Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            Dictionary<string, List<ShadingChannelProperties>> dictionary8 = dictionary1;
            string key7 = "Marmoset/Mobile/Bumped Specular IBL Fast";
            List<ShadingChannelProperties> channelPropertiesList35 = new List<ShadingChannelProperties>();
            channelPropertiesList35.Add(JobUtils.GetStandardDiffuseShadingChannelProperties());
            List<ShadingChannelProperties> channelPropertiesList36 = channelPropertiesList35;
            string specular5 = Simplygon.Unity.EditorPlugin.Constants.Specular;
            List<BasicShadingNodeProperties> basicPropertyNodes23 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes23.Add(new BasicShadingNodeProperties("_MainTex", "SpecularTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes23.Add(new BasicShadingNodeProperties("_SpecColor", "SpecularColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes23 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(2, new int[4], new int[4]
        {
          3,
          3,
          3,
          3
        }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(3, new int[2]
        {
          2,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply)
      };
            ShadingChannelProperties channelProperties23 = new ShadingChannelProperties(specular5, basicPropertyNodes23, modifierPropertyNodes23);
            channelPropertiesList36.Add(channelProperties23);
            channelPropertiesList35.Add(JobUtils.GetStandardNormalsShadingChannelProperties());
            List<ShadingChannelProperties> channelPropertiesList37 = channelPropertiesList35;
            dictionary8.Add(key7, channelPropertiesList37);
            dictionary1.Add("Marmoset/Mobile/Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Occlusion/Bumped Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetMarmosetOcclusionStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Occlusion/Bumped Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetMarmosetOcclusionStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetOcclusionStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            Dictionary<string, List<ShadingChannelProperties>> dictionary9 = dictionary1;
            string key8 = "Marmoset/Mobile/Occlusion/Bumped Specular IBL Fast";
            List<ShadingChannelProperties> channelPropertiesList38 = new List<ShadingChannelProperties>();
            channelPropertiesList38.Add(JobUtils.GetMarmosetOcclusionStandardDiffuseShadingChannelProperties());
            List<ShadingChannelProperties> channelPropertiesList39 = channelPropertiesList38;
            string specular6 = Simplygon.Unity.EditorPlugin.Constants.Specular;
            List<BasicShadingNodeProperties> basicPropertyNodes24 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes24.Add(new BasicShadingNodeProperties("_MainTex", "SpecularTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes24.Add(new BasicShadingNodeProperties("_OccTex", "SpecularOcclusionTexture", 1, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes24.Add(new BasicShadingNodeProperties("_SpecColor", "SpecularColor", 2, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes24 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(3, new int[4], new int[4]
        {
          3,
          3,
          3,
          3
        }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(4, new int[4]
        {
          1,
          1,
          1,
          1
        }, new int[4]{ 1, 1, 1, 1 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(5, new int[2]
        {
          4,
          3
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(6, new int[2]
        {
          5,
          2
        }, (int[]) null, ModifierShadingNodeType.Multiply)
      };
            ShadingChannelProperties channelProperties24 = new ShadingChannelProperties(specular6, basicPropertyNodes24, modifierPropertyNodes24);
            channelPropertiesList39.Add(channelProperties24);
            channelPropertiesList38.Add(JobUtils.GetStandardNormalsShadingChannelProperties());
            List<ShadingChannelProperties> channelPropertiesList40 = channelPropertiesList38;
            dictionary9.Add(key8, channelPropertiesList40);
            dictionary1.Add("Marmoset/Mobile/Occlusion/Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetMarmosetOcclusionStandardDiffuseShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Occlusion/Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetMarmosetOcclusionStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetOcclusionStandardSpecularShadingChannelProperties()
      });
            Dictionary<string, List<ShadingChannelProperties>> dictionary10 = dictionary1;
            string key9 = "Marmoset/Mobile/Occlusion/Specular IBL Fast";
            List<ShadingChannelProperties> channelPropertiesList41 = new List<ShadingChannelProperties>();
            channelPropertiesList41.Add(JobUtils.GetMarmosetOcclusionStandardDiffuseShadingChannelProperties());
            List<ShadingChannelProperties> channelPropertiesList42 = channelPropertiesList41;
            string specular7 = Simplygon.Unity.EditorPlugin.Constants.Specular;
            List<BasicShadingNodeProperties> basicPropertyNodes25 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes25.Add(new BasicShadingNodeProperties("_MainTex", "SpecularTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes25.Add(new BasicShadingNodeProperties("_OccTex", "SpecularOcclusionTexture", 1, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes25.Add(new BasicShadingNodeProperties("_SpecColor", "SpecularColor", 2, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes25 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(3, new int[4], new int[4]
        {
          3,
          3,
          3,
          3
        }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(4, new int[4]
        {
          1,
          1,
          1,
          1
        }, new int[4]{ 1, 1, 1, 1 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(5, new int[2]
        {
          4,
          3
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(6, new int[2]
        {
          5,
          2
        }, (int[]) null, ModifierShadingNodeType.Multiply)
      };
            ShadingChannelProperties channelProperties25 = new ShadingChannelProperties(specular7, basicPropertyNodes25, modifierPropertyNodes25);
            channelPropertiesList42.Add(channelProperties25);
            List<ShadingChannelProperties> channelPropertiesList43 = channelPropertiesList41;
            dictionary10.Add(key9, channelPropertiesList43);
            dictionary1.Add("Marmoset/Mobile/Self-Illumin/Bumped Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Self-Illumin/Bumped Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Self-Illumin/Bumped Specular IBL Fast", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Self-Illumin/Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Self-Illumin/Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Specular IBL Fast", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties()
      });
            Dictionary<string, List<ShadingChannelProperties>> dictionary11 = dictionary1;
            string key10 = "Marmoset/Mobile/Terrain/Terrain Diffuse IBL";
            List<ShadingChannelProperties> channelPropertiesList44 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList45 = channelPropertiesList44;
            string diffuse7 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes26 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes26.Add(new BasicShadingNodeProperties("_Control", "DiffuseControlTexture", 1, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes26.Add(new BasicShadingNodeProperties("_Splat0", "DiffuseSplatTexture0", 2, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes26.Add(new BasicShadingNodeProperties("_Splat1", "DiffuseSplatTexture1", 3, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes26.Add(new BasicShadingNodeProperties("_Splat2", "DiffuseSplatTexture2", 4, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes26.Add(new BasicShadingNodeProperties("_Splat3", "DiffuseSplatTexture3", 5, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes26.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 6, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes26.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0 0 0 1"), "FullAlpha", 7, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes26 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(8, new int[4]
        {
          1,
          1,
          1,
          1
        }, new int[4], ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(9, new int[2]
        {
          8,
          2
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(10, new int[4]
        {
          1,
          1,
          1,
          1
        }, new int[4]{ 1, 1, 1, 1 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(11, new int[2]
        {
          10,
          3
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(12, new int[4]
        {
          1,
          1,
          1,
          1
        }, new int[4]{ 2, 2, 2, 2 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          12,
          4
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(14, new int[4]
        {
          1,
          1,
          1,
          1
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(15, new int[2]
        {
          14,
          5
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          9,
          11
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          13,
          15
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          17,
          16
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(19, new int[2]
        {
          18,
          6
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(20, new int[4]
        {
          19,
          19,
          19,
          7
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties26 = new ShadingChannelProperties(diffuse7, basicPropertyNodes26, modifierPropertyNodes26);
            channelPropertiesList45.Add(channelProperties26);
            channelPropertiesList44.Add(JobUtils.GetStandardNormalsShadingChannelProperties());
            List<ShadingChannelProperties> channelPropertiesList46 = channelPropertiesList44;
            dictionary11.Add(key10, channelPropertiesList46);
            Dictionary<string, List<ShadingChannelProperties>> dictionary12 = dictionary1;
            string key11 = "Marmoset/Mobile/Terrain/Terrain Specular IBL";
            List<ShadingChannelProperties> channelPropertiesList47 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList48 = channelPropertiesList47;
            string diffuse8 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes27 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes27.Add(new BasicShadingNodeProperties("_Control", "DiffuseControlTexture", 1, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes27.Add(new BasicShadingNodeProperties("_Splat0", "DiffuseSplatTexture0", 2, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes27.Add(new BasicShadingNodeProperties("_Splat1", "DiffuseSplatTexture1", 3, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes27.Add(new BasicShadingNodeProperties("_Splat2", "DiffuseSplatTexture2", 4, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes27.Add(new BasicShadingNodeProperties("_Splat3", "DiffuseSplatTexture3", 5, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes27.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 6, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes27.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0 0 0 1"), "FullAlpha", 7, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes27 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(8, new int[4]
        {
          1,
          1,
          1,
          1
        }, new int[4], ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(9, new int[2]
        {
          8,
          2
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(10, new int[4]
        {
          1,
          1,
          1,
          1
        }, new int[4]{ 1, 1, 1, 1 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(11, new int[2]
        {
          10,
          3
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(12, new int[4]
        {
          1,
          1,
          1,
          1
        }, new int[4]{ 2, 2, 2, 2 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          12,
          4
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(14, new int[4]
        {
          1,
          1,
          1,
          1
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(15, new int[2]
        {
          14,
          5
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          9,
          11
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(17, new int[2]
        {
          13,
          15
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(18, new int[2]
        {
          17,
          16
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(19, new int[2]
        {
          18,
          6
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(20, new int[4]
        {
          19,
          19,
          19,
          7
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties27 = new ShadingChannelProperties(diffuse8, basicPropertyNodes27, modifierPropertyNodes27);
            channelPropertiesList48.Add(channelProperties27);
            List<ShadingChannelProperties> channelPropertiesList49 = channelPropertiesList47;
            string specular8 = Simplygon.Unity.EditorPlugin.Constants.Specular;
            List<BasicShadingNodeProperties> basicPropertyNodes28 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes28.Add(new BasicShadingNodeProperties("_Control", "SpecularControlTexture", 1, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes28.Add(new BasicShadingNodeProperties("_Splat0", "SpecularSplatTexture0", 2, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes28.Add(new BasicShadingNodeProperties("_Splat1", "SpecularSplatTexture1", 3, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes28.Add(new BasicShadingNodeProperties("_Splat2", "SpecularSplatTexture2", 4, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes28.Add(new BasicShadingNodeProperties("_Splat3", "SpecularSplatTexture3", 5, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes28.Add(new BasicShadingNodeProperties("_SpecColor", "SpecularColor", 6, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes28 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(8, new int[4]
        {
          1,
          1,
          1,
          1
        }, new int[4], ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(9, new int[4]
        {
          2,
          2,
          2,
          2
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(10, new int[2]
        {
          9,
          8
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(11, new int[4]
        {
          1,
          1,
          1,
          1
        }, new int[4]{ 1, 1, 1, 1 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(12, new int[4]
        {
          3,
          3,
          3,
          3
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(13, new int[2]
        {
          12,
          11
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(14, new int[4]
        {
          1,
          1,
          1,
          1
        }, new int[4]{ 2, 2, 2, 2 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(15, new int[4]
        {
          4,
          4,
          4,
          4
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(16, new int[2]
        {
          15,
          14
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(17, new int[4]
        {
          1,
          1,
          1,
          1
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(18, new int[4]
        {
          5,
          5,
          5,
          5
        }, new int[4]{ 3, 3, 3, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(19, new int[2]
        {
          18,
          17
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(20, new int[2]
        {
          10,
          13
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(21, new int[2]
        {
          20,
          16
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(22, new int[2]
        {
          21,
          19
        }, (int[]) null, ModifierShadingNodeType.Add),
        new ModifierShadingNodeProperties(23, new int[2]
        {
          22,
          6
        }, (int[]) null, ModifierShadingNodeType.Multiply)
      };
            ShadingChannelProperties channelProperties28 = new ShadingChannelProperties(specular8, basicPropertyNodes28, modifierPropertyNodes28);
            channelPropertiesList49.Add(channelProperties28);
            channelPropertiesList47.Add(JobUtils.GetStandardNormalsShadingChannelProperties());
            List<ShadingChannelProperties> channelPropertiesList50 = channelPropertiesList47;
            dictionary12.Add(key11, channelPropertiesList50);
            dictionary1.Add("Marmoset/Mobile/Transparent/Bumped Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Transparent/Bumped Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Transparent/Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Transparent/Simple Glass/Bumped Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            Dictionary<string, List<ShadingChannelProperties>> dictionary13 = dictionary1;
            string key12 = "Marmoset/Mobile/Transparent/Simple Glass/Specular Glow IBL";
            List<ShadingChannelProperties> channelPropertiesList51 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList52 = channelPropertiesList51;
            string diffuse9 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes29 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes29.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes29.Add(new BasicShadingNodeProperties("_Illum", "DiffuseGlowTexture", 1, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes29.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 2, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes29.Add(new BasicShadingNodeProperties("_GlowColor", "DiffuseGlowColor", 3, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes29.Add(new BasicShadingNodeProperties("_GlowStrength", "DiffuseGlowColor", 4, BasicShadingNodeType.Float, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes29 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(5, new int[2]
        {
          1,
          4
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(6, new int[2]
        {
          5,
          3
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(7, new int[2]
        {
          2,
          0
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(8, new int[2]
        {
          7,
          6
        }, (int[]) null, ModifierShadingNodeType.Add)
      };
            ShadingChannelProperties channelProperties29 = new ShadingChannelProperties(diffuse9, basicPropertyNodes29, modifierPropertyNodes29);
            channelPropertiesList52.Add(channelProperties29);
            channelPropertiesList51.Add(JobUtils.GetMarmosetStandardSpecularShadingChannelProperties());
            channelPropertiesList51.Add(JobUtils.GetStandardOpacityShadingChannelProperties());
            List<ShadingChannelProperties> channelPropertiesList53 = channelPropertiesList51;
            dictionary13.Add(key12, channelPropertiesList53);
            dictionary1.Add("Marmoset/Mobile/Transparent/Simple Glass/Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Transparent/Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Vertex Color/Bumped Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Vertex Color/Bumped Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Vertex Color/Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetMarmosetOcclusionStandardDiffuseShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Vertex Color/Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetMarmosetOcclusionStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetOcclusionStandardSpecularShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Vertex Occlusion/Bumped Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Vertex Occlusion/Bumped Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Vertex Occlusion/Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Mobile/Vertex Occlusion/Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Occlusion/Bumped Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetMarmosetOcclusionStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Occlusion/Bumped Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetMarmosetOcclusionStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetOcclusionStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Occlusion/Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetMarmosetOcclusionStandardDiffuseShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Occlusion/Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetMarmosetOcclusionStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetOcclusionStandardSpecularShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Self-Illumin/Bumped Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Self-Illumin/Bumped Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Self-Illumin/Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Self-Illumin/Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Transparent/Bumped Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Transparent/Bumped Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Transparent/Cutout/Bumped Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentCutoutShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Transparent/Cutout/Bumped Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentCutoutShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Transparent/Cutout/Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentCutoutShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Transparent/Cutout/Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentCutoutShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Transparent/Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Transparent/Simple Glass/Bumped Specular Glow IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Transparent/Simple Glass/Bumped Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Transparent/Simple Glass/Specular Glow IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Transparent/Simple Glass/Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Transparent/Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Vertex Color/Bumped Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Vertex Color/Bumped Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Vertex Color/Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Vertex Color/Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Vertex Lit/Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Vertex Lit/Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Vertex Lit/Specular Occ IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetMarmosetOcclusionStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetOcclusionStandardSpecularShadingChannelProperties("_MainTex")
      });
            dictionary1.Add("Marmoset/Vertex Lit/Specular Vertex Occ IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Vertex Occlusion/Bumped Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Vertex Occlusion/Bumped Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Vertex Occlusion/Diffuse IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties()
      });
            dictionary1.Add("Marmoset/Vertex Occlusion/Specular IBL", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties()
      });
            Dictionary<string, List<ShadingChannelProperties>> dictionary14 = dictionary1;
            string key13 = "Mobile/Bumped Diffuse";
            List<ShadingChannelProperties> channelPropertiesList54 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList55 = channelPropertiesList54;
            string diffuse10 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes30 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes30.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes30.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0 0 0 1"), "FullAlpha", 1, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes30 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(2, new int[4]
        {
          0,
          0,
          0,
          1
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties30 = new ShadingChannelProperties(diffuse10, basicPropertyNodes30, modifierPropertyNodes30);
            channelPropertiesList55.Add(channelProperties30);
            channelPropertiesList54.Add(JobUtils.GetStandardNormalsShadingChannelProperties());
            List<ShadingChannelProperties> channelPropertiesList56 = channelPropertiesList54;
            dictionary14.Add(key13, channelPropertiesList56);
            Dictionary<string, List<ShadingChannelProperties>> dictionary15 = dictionary1;
            string key14 = "Mobile/Bumped Specular";
            List<ShadingChannelProperties> channelPropertiesList57 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList58 = channelPropertiesList57;
            string diffuse11 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes31 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes31.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes31.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0 0 0 1"), "FullAlpha", 1, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes31 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(2, new int[4]
        {
          0,
          0,
          0,
          1
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties31 = new ShadingChannelProperties(diffuse11, basicPropertyNodes31, modifierPropertyNodes31);
            channelPropertiesList58.Add(channelProperties31);
            List<ShadingChannelProperties> channelPropertiesList59 = channelPropertiesList57;
            string specular9 = Simplygon.Unity.EditorPlugin.Constants.Specular;
            List<BasicShadingNodeProperties> basicPropertyNodes32 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes32.Add(new BasicShadingNodeProperties("_MainTex", "SpecularTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes32 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(1, new int[4], new int[4]
        {
          3,
          3,
          3,
          3
        }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties32 = new ShadingChannelProperties(specular9, basicPropertyNodes32, modifierPropertyNodes32);
            channelPropertiesList59.Add(channelProperties32);
            channelPropertiesList57.Add(JobUtils.GetStandardNormalsShadingChannelProperties());
            List<ShadingChannelProperties> channelPropertiesList60 = channelPropertiesList57;
            dictionary15.Add(key14, channelPropertiesList60);
            Dictionary<string, List<ShadingChannelProperties>> dictionary16 = dictionary1;
            string key15 = "Mobile/Bumped Specular (1 Directional Light)";
            List<ShadingChannelProperties> channelPropertiesList61 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList62 = channelPropertiesList61;
            string diffuse12 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes33 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes33.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes33.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0 0 0 1"), "FullAlpha", 1, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes33 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(2, new int[4]
        {
          0,
          0,
          0,
          1
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties33 = new ShadingChannelProperties(diffuse12, basicPropertyNodes33, modifierPropertyNodes33);
            channelPropertiesList62.Add(channelProperties33);
            List<ShadingChannelProperties> channelPropertiesList63 = channelPropertiesList61;
            string specular10 = Simplygon.Unity.EditorPlugin.Constants.Specular;
            List<BasicShadingNodeProperties> basicPropertyNodes34 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes34.Add(new BasicShadingNodeProperties("_MainTex", "SpecularTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes34 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(1, new int[4], new int[4]
        {
          3,
          3,
          3,
          3
        }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties34 = new ShadingChannelProperties(specular10, basicPropertyNodes34, modifierPropertyNodes34);
            channelPropertiesList63.Add(channelProperties34);
            channelPropertiesList61.Add(JobUtils.GetStandardNormalsShadingChannelProperties());
            List<ShadingChannelProperties> channelPropertiesList64 = channelPropertiesList61;
            dictionary16.Add(key15, channelPropertiesList64);
            Dictionary<string, List<ShadingChannelProperties>> dictionary17 = dictionary1;
            string key16 = "Mobile/Diffuse";
            List<ShadingChannelProperties> channelPropertiesList65 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList66 = channelPropertiesList65;
            string diffuse13 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes35 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes35.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes35.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0 0 0 1"), "FullAlpha", 1, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes35 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(2, new int[4]
        {
          0,
          0,
          0,
          1
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties35 = new ShadingChannelProperties(diffuse13, basicPropertyNodes35, modifierPropertyNodes35);
            channelPropertiesList66.Add(channelProperties35);
            List<ShadingChannelProperties> channelPropertiesList67 = channelPropertiesList65;
            dictionary17.Add(key16, channelPropertiesList67);
            Dictionary<string, List<ShadingChannelProperties>> dictionary18 = dictionary1;
            string key17 = "Mobile/VertexLit";
            List<ShadingChannelProperties> channelPropertiesList68 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList69 = channelPropertiesList68;
            string diffuse14 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes36 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes36.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes36.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0 0 0 1"), "FullAlpha", 1, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes36 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(2, new int[4]
        {
          0,
          0,
          0,
          1
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties36 = new ShadingChannelProperties(diffuse14, basicPropertyNodes36, modifierPropertyNodes36);
            channelPropertiesList69.Add(channelProperties36);
            List<ShadingChannelProperties> channelPropertiesList70 = channelPropertiesList68;
            dictionary18.Add(key17, channelPropertiesList70);
            Dictionary<string, List<ShadingChannelProperties>> dictionary19 = dictionary1;
            string key18 = "Mobile/VertexLit (Only Directional Lights)";
            List<ShadingChannelProperties> channelPropertiesList71 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList72 = channelPropertiesList71;
            string diffuse15 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes37 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes37.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes37.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0 0 0 1"), "FullAlpha", 1, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes37 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(2, new int[4]
        {
          0,
          0,
          0,
          1
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties37 = new ShadingChannelProperties(diffuse15, basicPropertyNodes37, modifierPropertyNodes37);
            channelPropertiesList72.Add(channelProperties37);
            List<ShadingChannelProperties> channelPropertiesList73 = channelPropertiesList71;
            dictionary19.Add(key18, channelPropertiesList73);
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Reflective/Bumped Diffuse", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Reflective/Bumped Specular", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Reflective/Parallax Specular", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Reflective/Specular", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Self-Illumin/Bumped Diffuse", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Self-Illumin/Bumped Specular", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Self-Illumin/Diffuse", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Self-Illumin/Parallax Diffuse", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Self-Illumin/Parallax Specular", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Self-Illumin/Specular", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Self-Illumin/VertexLit", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties()
      });
            dictionary1.Add("Simplygon/Bumped Specular", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties("_SpecMap"),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add("Simplygon/Specular", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties("_SpecMap")
      });
            dictionary1.Add("Simplygon/Transparent Bumped Diffuse", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add("Simplygon/Transparent Bumped Specular", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties("_SpecMap"),
        JobUtils.GetStandardNormalsShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            Dictionary<string, List<ShadingChannelProperties>> dictionary20 = dictionary1;
            string key19 = "Simplygon/Transparent Bumped Specular Detail";
            List<ShadingChannelProperties> channelPropertiesList74 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList75 = channelPropertiesList74;
            string diffuse16 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes38 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes38.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes38.Add(new BasicShadingNodeProperties("_Detail", "DiffDetailTexture", 1, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes38.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 2, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes38 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(3, new int[2]
        {
          1,
          0
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(4, new int[2]
        {
          3,
          2
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(5, new int[4]
        {
          4,
          4,
          4,
          0
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties38 = new ShadingChannelProperties(diffuse16, basicPropertyNodes38, modifierPropertyNodes38);
            channelPropertiesList75.Add(channelProperties38);
            channelPropertiesList74.Add(JobUtils.GetMarmosetStandardSpecularShadingChannelProperties("_SpecMap"));
            channelPropertiesList74.Add(JobUtils.GetStandardNormalsShadingChannelProperties());
            channelPropertiesList74.Add(JobUtils.GetStandardOpacityShadingChannelProperties());
            List<ShadingChannelProperties> channelPropertiesList76 = channelPropertiesList74;
            dictionary20.Add(key19, channelPropertiesList76);
            dictionary1.Add("Simplygon/Transparent Diffuse", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add("Simplygon/Transparent Specular", new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetMarmosetStandardSpecularShadingChannelProperties("_SpecMap"),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Transparent/Bumped Diffuse", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Transparent/Bumped Specular", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Transparent/Cutout/Bumped Diffuse", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentCutoutShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Transparent/Cutout/Bumped Specular", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentCutoutShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Transparent/Cutout/Diffuse", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentCutoutShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Transparent/Cutout/DiffuseDoubleside", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentCutoutShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Transparent/Cutout/Soft Edge Unlit", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentCutoutShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Transparent/Cutout/Specular", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentCutoutShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Transparent/Cutout/VertexLit", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentCutoutShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Transparent/Diffuse", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Transparent/Parallax Diffuse", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Transparent/Parallax Specular", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardNormalsShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Transparent/Specular", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetStandardSpecularShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            dictionary1.Add(string.Format(CultureInfo.InvariantCulture, "{0}Transparent/VertexLit", (object)JobUtils.DefaultShaderPathPrefix), new List<ShadingChannelProperties>()
      {
        JobUtils.GetStandardDiffuseTransparentShadingChannelProperties(),
        JobUtils.GetStandardOpacityShadingChannelProperties()
      });
            Dictionary<string, List<ShadingChannelProperties>> dictionary21 = dictionary1;
            string key20 = "Unlit/Texture";
            List<ShadingChannelProperties> channelPropertiesList77 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList78 = channelPropertiesList77;
            string diffuse17 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes39 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes39.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes39.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0 0 0 1"), "FullAlpha", 1, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes39 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(2, new int[4]
        {
          0,
          0,
          0,
          1
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties39 = new ShadingChannelProperties(diffuse17, basicPropertyNodes39, modifierPropertyNodes39);
            channelPropertiesList78.Add(channelProperties39);
            List<ShadingChannelProperties> channelPropertiesList79 = channelPropertiesList77;
            dictionary21.Add(key20, channelPropertiesList79);
            Dictionary<string, List<ShadingChannelProperties>> dictionary22 = dictionary1;
            string key21 = "Unlit/Transparent";
            List<ShadingChannelProperties> channelPropertiesList80 = new List<ShadingChannelProperties>();
            channelPropertiesList80.Add(new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.Diffuse, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_MainTex", "DiffuseTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      }));
            List<ShadingChannelProperties> channelPropertiesList81 = channelPropertiesList80;
            string opacity5 = Simplygon.Unity.EditorPlugin.Constants.Opacity;
            List<BasicShadingNodeProperties> basicPropertyNodes40 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes40.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes40 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(1, new int[4]
        {
          0,
          0,
          0,
          -1
        }, new int[4]{ 3, 3, 3, -1 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties40 = new ShadingChannelProperties(opacity5, basicPropertyNodes40, modifierPropertyNodes40);
            channelPropertiesList81.Add(channelProperties40);
            List<ShadingChannelProperties> channelPropertiesList82 = channelPropertiesList80;
            dictionary22.Add(key21, channelPropertiesList82);
            Dictionary<string, List<ShadingChannelProperties>> dictionary23 = dictionary1;
            string key22 = "Unlit/Transparent Cutout";
            List<ShadingChannelProperties> channelPropertiesList83 = new List<ShadingChannelProperties>();
            List<ShadingChannelProperties> channelPropertiesList84 = channelPropertiesList83;
            string diffuse18 = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes41 = new List<BasicShadingNodeProperties>();
            basicPropertyNodes41.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes41.Add(new BasicShadingNodeProperties("_Cutoff", "OpacityCutoff", 1, BasicShadingNodeType.Float, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes41 = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(2, new int[2]
        {
          0,
          1
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(3, new int[4]
        {
          0,
          0,
          0,
          2
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle)
      };
            ShadingChannelProperties channelProperties41 = new ShadingChannelProperties(diffuse18, basicPropertyNodes41, modifierPropertyNodes41);
            channelPropertiesList84.Add(channelProperties41);
            List<ShadingChannelProperties> channelPropertiesList85 = channelPropertiesList83;
            dictionary23.Add(key22, channelPropertiesList85);
            return dictionary1;
        }

        private static ShadingChannelProperties GetStandardDiffuseShadingChannelProperties()
        {
            string diffuse = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes = new List<BasicShadingNodeProperties>();
            basicPropertyNodes.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0 0 0 1"), "FullAlpha", 2, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(3, new int[2]
        {
          1,
          0
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(4, new int[4]
        {
          3,
          3,
          3,
          2
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle)
      };
            return new ShadingChannelProperties(diffuse, basicPropertyNodes, modifierPropertyNodes);
        }

        private static ShadingChannelProperties GetStandardDiffuseTransparentShadingChannelProperties()
        {
            string diffuse = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes = new List<BasicShadingNodeProperties>();
            basicPropertyNodes.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(2, new int[2]
        {
          1,
          0
        }, (int[]) null, ModifierShadingNodeType.Multiply)
      };
            return new ShadingChannelProperties(diffuse, basicPropertyNodes, modifierPropertyNodes);
        }

        private static ShadingChannelProperties GetStandardDiffuseTransparentCutoutShadingChannelProperties()
        {
            string diffuse = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes = new List<BasicShadingNodeProperties>();
            basicPropertyNodes.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseMainTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes.Add(new BasicShadingNodeProperties("_Cutoff", "OpacityCutoff", 2, BasicShadingNodeType.Float, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(3, new int[2]
        {
          1,
          0
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(4, new int[2]
        {
          0,
          2
        }, (int[]) null, ModifierShadingNodeType.Step),
        new ModifierShadingNodeProperties(5, new int[4]
        {
          3,
          3,
          3,
          4
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle)
      };
            return new ShadingChannelProperties(diffuse, basicPropertyNodes, modifierPropertyNodes);
        }

        private static ShadingChannelProperties GetStandardSpecularShadingChannelProperties()
        {
            string specular = Simplygon.Unity.EditorPlugin.Constants.Specular;
            List<BasicShadingNodeProperties> basicPropertyNodes = new List<BasicShadingNodeProperties>();
            basicPropertyNodes.Add(new BasicShadingNodeProperties("_MainTex", "SpecularTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes.Add(new BasicShadingNodeProperties("_SpecColor", "SpecularColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(2, new int[4]
        {
          0,
          0,
          0,
          -1
        }, new int[4]{ 3, 3, 3, -1 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(3, new int[2]
        {
          2,
          1
        }, (int[]) null, ModifierShadingNodeType.Multiply)
      };
            return new ShadingChannelProperties(specular, basicPropertyNodes, modifierPropertyNodes);
        }

        private static ShadingChannelProperties GetStandardNormalsShadingChannelProperties()
        {
            return new ShadingChannelProperties(Simplygon.Unity.EditorPlugin.Constants.Normals, new List<BasicShadingNodeProperties>()
      {
        new BasicShadingNodeProperties("_BumpMap", "NormalsTexture", 0, BasicShadingNodeType.Texture, "TexCoords0")
      });
        }

        private static ShadingChannelProperties GetStandardOpacityShadingChannelProperties()
        {
            string opacity = Simplygon.Unity.EditorPlugin.Constants.Opacity;
            List<BasicShadingNodeProperties> basicPropertyNodes = new List<BasicShadingNodeProperties>();
            basicPropertyNodes.Add(new BasicShadingNodeProperties("_MainTex", "OpacityTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes.Add(new BasicShadingNodeProperties("_Color", "DiffuseColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(2, new int[4]
        {
          0,
          0,
          0,
          -1
        }, new int[4]{ 3, 3, 3, -1 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(3, new int[4]
        {
          1,
          1,
          1,
          -1
        }, new int[4]{ 3, 3, 3, -1 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(4, new int[2]
        {
          2,
          3
        }, (int[]) null, ModifierShadingNodeType.Multiply)
      };
            return new ShadingChannelProperties(opacity, basicPropertyNodes, modifierPropertyNodes);
        }

        private static ShadingChannelProperties GetMarmosetOcclusionStandardDiffuseShadingChannelProperties()
        {
            string diffuse = Simplygon.Unity.EditorPlugin.Constants.Diffuse;
            List<BasicShadingNodeProperties> basicPropertyNodes = new List<BasicShadingNodeProperties>();
            basicPropertyNodes.Add(new BasicShadingNodeProperties("_MainTex", "DiffuseTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes.Add(new BasicShadingNodeProperties("_OccTex", "DiffuseOcclusionTexture", 1, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes.Add(new BasicShadingNodeProperties("_Color", "DiffuseMainColor", 2, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0 0 0 1"), "FullAlpha", 3, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(4, new int[4]
        {
          1,
          1,
          1,
          -1
        }, new int[4]{ 0, 0, 0, -1 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(5, new int[2]
        {
          4,
          0
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(6, new int[2]
        {
          5,
          2
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(7, new int[4]
        {
          6,
          6,
          6,
          3
        }, new int[4]{ 0, 1, 2, 3 }, ModifierShadingNodeType.Swizzle)
      };
            return new ShadingChannelProperties(diffuse, basicPropertyNodes, modifierPropertyNodes);
        }

        private static ShadingChannelProperties GetMarmosetStandardSpecularShadingChannelProperties()
        {
            return JobUtils.GetMarmosetStandardSpecularShadingChannelProperties("_SpecTex");
        }

        private static ShadingChannelProperties GetMarmosetStandardSpecularShadingChannelProperties(string specularTextureName)
        {
            string specular = Simplygon.Unity.EditorPlugin.Constants.Specular;
            List<BasicShadingNodeProperties> basicPropertyNodes = new List<BasicShadingNodeProperties>();
            basicPropertyNodes.Add(new BasicShadingNodeProperties(specularTextureName, "SpecularTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes.Add(new BasicShadingNodeProperties("_SpecColor", "SpecularColor", 1, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(2, new int[2]
        {
          1,
          0
        }, (int[]) null, ModifierShadingNodeType.Multiply)
      };
            return new ShadingChannelProperties(specular, basicPropertyNodes, modifierPropertyNodes);
        }

        private static ShadingChannelProperties GetMarmosetOcclusionStandardSpecularShadingChannelProperties()
        {
            return JobUtils.GetMarmosetOcclusionStandardSpecularShadingChannelProperties("_SpecTex");
        }

        private static ShadingChannelProperties GetMarmosetOcclusionStandardSpecularShadingChannelProperties(string specularTextureName)
        {
            string specular = Simplygon.Unity.EditorPlugin.Constants.Specular;
            List<BasicShadingNodeProperties> basicPropertyNodes = new List<BasicShadingNodeProperties>();
            basicPropertyNodes.Add(new BasicShadingNodeProperties(specularTextureName, "SpecularTexture", 0, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes.Add(new BasicShadingNodeProperties("_OccTex", "SpecularOcclusionTexture", 1, BasicShadingNodeType.Texture, "TexCoords0"));
            basicPropertyNodes.Add(new BasicShadingNodeProperties("_SpecColor", "SpecularColor", 2, BasicShadingNodeType.Color, "TexCoords0"));
            basicPropertyNodes.Add(new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName("0 0 0 1"), "FullAlpha", 3, BasicShadingNodeType.Color, "TexCoords0"));
            List<ModifierShadingNodeProperties> modifierPropertyNodes = new List<ModifierShadingNodeProperties>()
      {
        new ModifierShadingNodeProperties(4, new int[4]
        {
          1,
          1,
          1,
          3
        }, new int[4]{ 1, 1, 1, 3 }, ModifierShadingNodeType.Swizzle),
        new ModifierShadingNodeProperties(5, new int[2]
        {
          4,
          0
        }, (int[]) null, ModifierShadingNodeType.Multiply),
        new ModifierShadingNodeProperties(6, new int[2]
        {
          5,
          2
        }, (int[]) null, ModifierShadingNodeType.Multiply)
      };
            return new ShadingChannelProperties(specular, basicPropertyNodes, modifierPropertyNodes);
        }
    }
}
