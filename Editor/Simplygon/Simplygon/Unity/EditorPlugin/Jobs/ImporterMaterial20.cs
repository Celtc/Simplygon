﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.Jobs.ImporterMaterial20
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Unity;
using Simplygon.Scene.Asset;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Globalization;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin.Jobs
{
    public class ImporterMaterial20
    {
        internal readonly Dictionary<string, string> DefaultShaderPropertyNames = new Dictionary<string, string>()
        {
          {
            Simplygon.Unity.EditorPlugin.Constants.Diffuse,
            "_MainTex"
          },
          {
            Simplygon.Unity.EditorPlugin.Constants.Specular,
            "_SpecMap"
          },
          {
            Simplygon.Unity.EditorPlugin.Constants.Normals,
            "_BumpMap"
          }
        };

        internal readonly Dictionary<string, string> MarmosetShaderPropertyNames = new Dictionary<string, string>()
        {
          {
            Simplygon.Unity.EditorPlugin.Constants.Diffuse,
            "_MainTex"
          },
          {
            Simplygon.Unity.EditorPlugin.Constants.Specular,
            "_SpecTex"
          },
          {
            Simplygon.Unity.EditorPlugin.Constants.Normals,
            "_BumpMap"
          }
        };

        internal readonly Dictionary<string, string> Unity5StandardSpecularSetupShaderPropertyNames = new Dictionary<string, string>()
        {
          {
            Simplygon.Unity.EditorPlugin.Constants.Diffuse,
            "_MainTex"
          },
          {
            Simplygon.Unity.EditorPlugin.Constants.Specular,
            "_SpecGlossMap"
          },
          {
            Simplygon.Unity.EditorPlugin.Constants.Normals,
            "_BumpMap"
          },
          {
            Simplygon.Unity.EditorPlugin.Constants.Parallax,
            "_ParallaxMap"
          },
          {
            Simplygon.Unity.EditorPlugin.Constants.Occlusion,
            "_OcclusionMap"
          },
          {
            Simplygon.Unity.EditorPlugin.Constants.Emission,
            "_EmissionMap"
          },
          {
            Simplygon.Unity.EditorPlugin.Constants.DetailMask,
            "_DetailMask"
          },
          {
            Simplygon.Unity.EditorPlugin.Constants.SecondaryDiffuse,
            "_DetailAlbedoMap"
          },
          {
            Simplygon.Unity.EditorPlugin.Constants.SecondaryNormals,
            "_DetailNormalMap"
          }
        };

        internal readonly Dictionary<string, string> UnityHDRPLitPropertyNames = new Dictionary<string, string>()
        {
          {
            Simplygon.Unity.EditorPlugin.Constants.Diffuse,
            "_BaseColorMap"
          },
          {
            "Normal",
            "_NormalMap"
          },
          {
            "MaskMap",
            "_MaskMap"
          },
          {
            "BentNormal",
            "_BentNormalMap"
          },
          {
            "Emissive",
            "_EmissiveColorMap"
          },
          {
            "Coat",
            "_CoatMaskMap"
          },
          {
            "Height",
            "_HeightMap"
          },
        };


        private readonly Dictionary<string, string> DefaultResultShaderSet = new Dictionary<string, string>()
        {
          {
            string.Format(CultureInfo.InvariantCulture, "{0}{1}{2}", (object) Simplygon.Unity.EditorPlugin.Constants.Diffuse, (object) Simplygon.Unity.EditorPlugin.Constants.Normals, (object) Simplygon.Unity.EditorPlugin.Constants.Specular),
            "Simplygon/Bumped Specular"
          },
          {
            string.Format(CultureInfo.InvariantCulture, "{0}{1}{2}{3}", (object) Simplygon.Unity.EditorPlugin.Constants.Diffuse, (object) Simplygon.Unity.EditorPlugin.Constants.Normals, (object) Simplygon.Unity.EditorPlugin.Constants.Opacity, (object) Simplygon.Unity.EditorPlugin.Constants.Specular),
            "Simplygon/Transparent Bumped Specular"
          },
          {
            string.Format(CultureInfo.InvariantCulture, "{0}{1}", (object) Simplygon.Unity.EditorPlugin.Constants.Diffuse, (object) Simplygon.Unity.EditorPlugin.Constants.Normals),
            "Legacy Shaders/Bumped Diffuse"
          },
          {
            string.Format(CultureInfo.InvariantCulture, "{0}{1}{2}", (object) Simplygon.Unity.EditorPlugin.Constants.Diffuse, (object) Simplygon.Unity.EditorPlugin.Constants.Normals, (object) Simplygon.Unity.EditorPlugin.Constants.Opacity),
            "Simplygon/Transparent Bumped Diffuse"
          },
          {
            Simplygon.Unity.EditorPlugin.Constants.Diffuse,
            "Diffuse"
          },
          {
            string.Format(CultureInfo.InvariantCulture, "{0}{1}", (object) Simplygon.Unity.EditorPlugin.Constants.Diffuse, (object) Simplygon.Unity.EditorPlugin.Constants.Specular),
            "Simplygon/Specular"
          },
          {
            string.Format(CultureInfo.InvariantCulture, "{0}{1}", (object) Simplygon.Unity.EditorPlugin.Constants.Diffuse, (object) Simplygon.Unity.EditorPlugin.Constants.Opacity),
            "Simplygon/Transparent Diffuse"
          },
          {
            string.Format(CultureInfo.InvariantCulture, "{0}{1}{2}", (object) Simplygon.Unity.EditorPlugin.Constants.Diffuse, (object) Simplygon.Unity.EditorPlugin.Constants.Opacity, (object) Simplygon.Unity.EditorPlugin.Constants.Specular),
            "Simplygon/Transparent Specular"
          }
        };

        private readonly Dictionary<string, string> MarmosetResultShaderSet = new Dictionary<string, string>()
        {
          {
            string.Format(CultureInfo.InvariantCulture, "{0}{1}{2}", (object) Simplygon.Unity.EditorPlugin.Constants.Diffuse, (object) Simplygon.Unity.EditorPlugin.Constants.Normals, (object) Simplygon.Unity.EditorPlugin.Constants.Specular),
            "Marmoset/Bumped Specular IBL"
          },
          {
            string.Format(CultureInfo.InvariantCulture, "{0}{1}{2}{3}", (object) Simplygon.Unity.EditorPlugin.Constants.Diffuse, (object) Simplygon.Unity.EditorPlugin.Constants.Normals, (object) Simplygon.Unity.EditorPlugin.Constants.Opacity, (object) Simplygon.Unity.EditorPlugin.Constants.Specular),
            "Marmoset/Bumped Specular IBL"
          },
          {
            string.Format(CultureInfo.InvariantCulture, "{0}{1}", (object) Simplygon.Unity.EditorPlugin.Constants.Diffuse, (object) Simplygon.Unity.EditorPlugin.Constants.Normals),
            "Marmoset/Bumped Diffuse IBL"
          },
          {
            string.Format(CultureInfo.InvariantCulture, "{0}{1}{2}", (object) Simplygon.Unity.EditorPlugin.Constants.Diffuse, (object) Simplygon.Unity.EditorPlugin.Constants.Normals, (object) Simplygon.Unity.EditorPlugin.Constants.Opacity),
            "Marmoset/Bumped Diffuse IBL"
          },
          {
            Simplygon.Unity.EditorPlugin.Constants.Diffuse,
            "Marmoset/Diffuse IBL"
          },
          {
            string.Format(CultureInfo.InvariantCulture, "{0}{1}", (object) Simplygon.Unity.EditorPlugin.Constants.Diffuse, (object) Simplygon.Unity.EditorPlugin.Constants.Specular),
            "Marmoset/Specular IBL"
          },
          {
            string.Format(CultureInfo.InvariantCulture, "{0}{1}", (object) Simplygon.Unity.EditorPlugin.Constants.Diffuse, (object) Simplygon.Unity.EditorPlugin.Constants.Opacity),
            "Marmoset/Diffuse IBL"
          },
          {
            string.Format(CultureInfo.InvariantCulture, "{0}{1}{2}", (object) Simplygon.Unity.EditorPlugin.Constants.Diffuse, (object) Simplygon.Unity.EditorPlugin.Constants.Opacity, (object) Simplygon.Unity.EditorPlugin.Constants.Specular),
            "Marmoset/Specular IBL"
          }
        };

        private Dictionary<Guid, UnityEngine.Material> materials = new Dictionary<Guid, UnityEngine.Material>();
        private Dictionary<Guid, UnityEngine.Texture> textures = new Dictionary<Guid, UnityEngine.Texture>();
        private List<GameObject> tempMaterialHolders = new List<GameObject>();
        private readonly Dictionary<BasicShadingNodeType, Action<string, UnityEngine.Material, MaterialChannel>> ShaderPropertySetters;
        private Simplygon.Scene.Scene scene;

        private UnityCloudJob CurrentJob
        {
            get; set;
        }

        public ImporterMaterial20()
        {
            this.ShaderPropertySetters = new Dictionary<BasicShadingNodeType, Action<string, UnityEngine.Material, MaterialChannel>>()
      {
        {
          BasicShadingNodeType.Texture,
          new Action<string, UnityEngine.Material, MaterialChannel>(this.SetShaderTextureProperty)
        },
        {
          BasicShadingNodeType.Color,
          new Action<string, UnityEngine.Material, MaterialChannel>(this.SetShaderColorProperty)
        }
      };
        }

        internal void Import(Simplygon.Scene.Scene scene, UnityCloudJob job)
        {
            this.scene = scene;
            this.CurrentJob = job;
            this.textures.Clear();

            foreach (KeyValuePair<Guid, Scene.Asset.Texture> texture1 in scene.Textures)
            {
                var simplygonTexture = texture1.Value;

                if (!scene.Materials.Values.ToList().Exists(material => material.MaterialChannels.ToList().Exists(channel =>
                  {
                      return channel.ChannelName == Constants.Opacity ?
                          channel.Textures.ToList().Exists(textureDescriptor => textureDescriptor.Texture == simplygonTexture.Id) :
                          false;
                  })))
                {

                    bool isNormalsTexture = scene.Materials.Values.ToList<Simplygon.Scene.Asset.Material>().Exists((Predicate<Simplygon.Scene.Asset.Material>)(material => material.MaterialChannels.ToList<MaterialChannel>().Exists((Predicate<MaterialChannel>)(channel =>
                  {
                      if (channel.ChannelName == Simplygon.Unity.EditorPlugin.Constants.Normals || channel.ChannelName == Simplygon.Unity.EditorPlugin.Constants.SecondaryNormals)
                          return channel.Textures.ToList<MaterialChannelTextureDescriptor>().Exists((Predicate<MaterialChannelTextureDescriptor>)(textureDescriptor => textureDescriptor.Texture == simplygonTexture.Id));
                      return false;
                  }))));
                    bool flag1 = scene.Materials.Values.ToList<Simplygon.Scene.Asset.Material>().Exists((Predicate<Simplygon.Scene.Asset.Material>)(material => material.MaterialChannels.ToList<MaterialChannel>().Exists((Predicate<MaterialChannel>)(channel =>
                  {
                      if (channel.ChannelName == Simplygon.Unity.EditorPlugin.Constants.Emission)
                          return channel.Textures.ToList<MaterialChannelTextureDescriptor>().Exists((Predicate<MaterialChannelTextureDescriptor>)(textureDescriptor => textureDescriptor.Texture == simplygonTexture.Id));
                      return false;
                  }))));
                    bool flag2 = scene.Materials.Values.ToList<Simplygon.Scene.Asset.Material>().Exists((Predicate<Simplygon.Scene.Asset.Material>)(material => material.MaterialChannels.ToList<MaterialChannel>().Exists((Predicate<MaterialChannel>)(channel =>
                  {
                      if (channel.ChannelName == Simplygon.Unity.EditorPlugin.Constants.Specular)
                          return channel.Textures.ToList<MaterialChannelTextureDescriptor>().Exists((Predicate<MaterialChannelTextureDescriptor>)(textureDescriptor => textureDescriptor.Texture == simplygonTexture.Id));
                      return false;
                  }))));
                    bool hdrpMaskMap = scene.Materials.Values.ToList<Simplygon.Scene.Asset.Material>().Exists((Predicate<Simplygon.Scene.Asset.Material>)(material => material.MaterialChannels.ToList<MaterialChannel>().Exists((Predicate<MaterialChannel>)(channel =>
                    {
                        if (channel.ChannelName == "MaskMap")
                            return channel.Textures.ToList<MaterialChannelTextureDescriptor>().Exists((Predicate<MaterialChannelTextureDescriptor>)(textureDescriptor => textureDescriptor.Texture == simplygonTexture.Id));
                        return false;
                    }))));
                    bool hdrpNormal = scene.Materials.Values.ToList<Simplygon.Scene.Asset.Material>().Exists((Predicate<Simplygon.Scene.Asset.Material>)(material => material.MaterialChannels.ToList<MaterialChannel>().Exists((Predicate<MaterialChannel>)(channel =>
                    {
                        if (channel.ChannelName == "Normal")
                            return channel.Textures.ToList<MaterialChannelTextureDescriptor>().Exists((Predicate<MaterialChannelTextureDescriptor>)(textureDescriptor => textureDescriptor.Texture == simplygonTexture.Id));
                        return false;
                    }))));
                    bool hdrpBentNormal = scene.Materials.Values.ToList<Simplygon.Scene.Asset.Material>().Exists((Predicate<Simplygon.Scene.Asset.Material>)(material => material.MaterialChannels.ToList<MaterialChannel>().Exists((Predicate<MaterialChannel>)(channel =>
                    {
                        if (channel.ChannelName == "BentNormal")
                            return channel.Textures.ToList<MaterialChannelTextureDescriptor>().Exists((Predicate<MaterialChannelTextureDescriptor>)(textureDescriptor => textureDescriptor.Texture == simplygonTexture.Id));
                        return false;
                    }))));
                    isNormalsTexture |= hdrpNormal;
                    bool alphaIsTransparency = !(isNormalsTexture | flag1 | flag2 | hdrpNormal | hdrpBentNormal | hdrpMaskMap);
                    UnityEngine.Texture texture2 = this.CreateTexture(scene, simplygonTexture, isNormalsTexture, alphaIsTransparency);
                    this.textures.Add(simplygonTexture.Id, texture2);
                }
            }
        }

        public void SaveMaterialsToAssetDatabase()
        {
            foreach (UnityEngine.Material material in this.materials.Values)
            {
                Path.Combine(this.CurrentJob.AssetFullMaterialDirectory, string.Format(CultureInfo.InvariantCulture, "{0}.mat", (object)material.name));
                string path = string.Format(CultureInfo.InvariantCulture, "Assets/LODs/{0}/{1}.mat", (object)this.CurrentJob.AssetMaterialDirectory, (object)material.name);
                AssetDatabase.CreateAsset(material, path);
            }
        }

        internal UnityEngine.Material CreateMaterial(Simplygon.Scene.Asset.Material simplygonMaterial, UnityEngine.Material originalMaterial, int lodIndex, List<UnityEngine.Material> otherMaterials)
        {
            //
            if (this.materials.ContainsKey(simplygonMaterial.Id))
            {
                return this.materials[simplygonMaterial.Id];
            }

            bool createdMaterial = false;
            try
            {
                bool noSourceMaterial = originalMaterial == null;
                if (noSourceMaterial)
                {
                    createdMaterial = true;
                    originalMaterial = new UnityEngine.Material(Shader.Find("Standard (Specular setup)"));
                }

                var gameObject = new GameObject
                {
                    hideFlags = HideFlags.HideAndDontSave
                };

                // Create material
                var meshRenderer = gameObject.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
                var material = new UnityEngine.Material(originalMaterial);
                material.CopyPropertiesFromMaterial(originalMaterial);
                material.name = string.Format(CultureInfo.InvariantCulture, "{0}_LOD{1}", (object)simplygonMaterial.Name, (object)lodIndex);

                // Set material
                meshRenderer.sharedMaterial = material;

                this.tempMaterialHolders.Add(gameObject);
                var shaderName = string.Empty;
                var shaderPropertyNames = this.DefaultShaderPropertyNames;

                var isStandardMaterial = false;
                var isHDRPMaterial = false;

                // Standard shader
                if (originalMaterial.shader.name == "Standard" ||
                    originalMaterial.shader.name == "Standard (Specular setup)")
                {
                    // Flag
                    isStandardMaterial = true;

                    // Shader names
                    shaderName = "Standard (Specular setup)";
                    shaderPropertyNames = this.Unity5StandardSpecularSetupShaderPropertyNames;

                    // Base
                    if (!material.HasProperty("_Mode") ||
                        material.GetFloat("_Mode") == 0.0)
                    {
                        var func = (Func<UnityEngine.Material, bool>)(otherMaterial =>
                        {
                            return otherMaterial.HasProperty("_Mode") ?
                                otherMaterial.GetFloat("_Mode") != 0.0 :
                                false;
                        });
                        if (!otherMaterials.Any(func))
                        {
                            goto label_9;
                        }
                    }

                    // Set properties
                    material.SetFloat("_Mode", 3f);
                    material.SetInt("_SrcBlend", 1);
                    material.SetInt("_DstBlend", 10);
                    material.SetInt("_ZWrite", 0);
                    material.DisableKeyword("_ALPHATEST_ON");
                    material.DisableKeyword("_ALPHABLEND_ON");
                    material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                    material.renderQueue = 3000;

                label_9:
                    if (material.HasProperty("_OcclusionStrength"))
                    {
                        material.SetFloat("_OcclusionStrength", 1f);
                    }
                    if (material.HasProperty("_EmissionColor"))
                    {
                        material.SetColor("_EmissionColor", Color.black);
                    }
                    if (material.HasProperty("_EmissionColorUI"))
                    {
                        material.SetColor("_EmissionColorUI", Color.black);
                    }
                    material.DisableKeyword("_METALLICGLOSSMAP");
                    material.EnableKeyword("_NORMALMAP");

                    var channelByName = simplygonMaterial.GetChannelByName("Specular");
                    if (channelByName != null &&
                        channelByName.Textures.Count > 0)
                    {
                        material.EnableKeyword("_SPECGLOSSMAP");
                    }
                }

                // HDRP Lit
                else if (originalMaterial.shader.name == "HDRP/Lit")
                {
                    // Flag
                    isHDRPMaterial = true;

                    // Shader names
                    shaderName = "HDRP/Lit";
                    shaderPropertyNames = this.UnityHDRPLitPropertyNames;

                    // Set properties
                    if (material.HasProperty("_BaseColor"))
                    {
                        material.SetColor("_BaseColor", Color.white);
                    }
                    if (material.HasProperty("_Metallic"))
                    {
                        material.SetFloat("_Metallic", 1f);
                    }
                    if (material.HasProperty("_SmoothnessRemapMin"))
                    {
                        material.SetFloat("_SmoothnessRemapMin", 0f);
                    }
                    if (material.HasProperty("_SmoothnessRemapMax"))
                    {
                        material.SetFloat("_SmoothnessRemapMax", 1f);
                    }
                    if (material.HasProperty("_AORemapMin"))
                    {
                        material.SetFloat("_AORemapMin", 0f);
                    }
                    if (material.HasProperty("_AORemapMax"))
                    {
                        material.SetFloat("_AORemapMax", 1f);
                    }
                    if (material.HasProperty("_NormalScale"))
                    {
                        material.SetFloat("_NormalScale", 1f);
                    }
                }

                // Other shader
                else
                {
                    var dictionary = this.DefaultResultShaderSet;
                    if (null != Shader.Find("Marmoset/Diffuse IBL"))
                    {
                        dictionary = this.MarmosetResultShaderSet;
                        shaderPropertyNames = this.MarmosetShaderPropertyNames;
                    }
                    var stringList = new List<string>();
                    var concatenatedChannelNames = string.Empty;
                    foreach (MaterialChannel materialChannel in simplygonMaterial.MaterialChannels)
                    {
                        if (!noSourceMaterial || !(materialChannel.ChannelName == Constants.Opacity))
                        {
                            stringList.Add(materialChannel.ChannelName);
                        }
                    }
                    stringList.Sort();
                    stringList.ForEach((Action<string>)(channelName => concatenatedChannelNames += channelName));
                    if (!dictionary.TryGetValue(concatenatedChannelNames, out shaderName))
                    {
                        shaderName = "Legacy Shaders/Bumped Diffuse";
                    }
                }

                // Setup material
                var shader = Shader.Find(shaderName);
                if (null != shader)
                {
                    material.shader = shader;
                    foreach (MaterialChannel materialChannel in simplygonMaterial.MaterialChannels)
                    {
                        if (materialChannel.Textures.Count > 0)
                        {
                            // For every texture
                            if (materialChannel.Textures.Count > 0 && 
                                shaderPropertyNames.TryGetValue(materialChannel.ChannelName, out string texProperty) &&                                  
                                textures.TryGetValue(materialChannel.Textures.ElementAt(0).Texture, out UnityEngine.Texture texture))
                            {
                                // Set texture to property
                                material.SetTexture(texProperty, texture);

                                // Scale
                                material.SetTextureScale(texProperty, new Vector2(1f, 1f));

                                // Color / Tint
                                if (!isHDRPMaterial &&
                                    material.HasProperty("_Color"))
                                {
                                    material.SetColor("_Color", Color.white);
                                }

                                // Spec color
                                if (!isStandardMaterial &&
                                    !isHDRPMaterial &&
                                    material.HasProperty("_SpecColor"))
                                {
                                    material.SetColor("_SpecColor", Color.white);
                                }

                                // Glossiness
                                if (isStandardMaterial &&
                                    material.HasProperty("_Glossiness"))
                                {
                                    material.SetFloat("_Glossiness", 1f);
                                }

                                // Glossiness scale
                                if (isStandardMaterial &&
                                    material.HasProperty("_GlossMapScale"))
                                {
                                    material.SetFloat("_GlossMapScale", 1f);
                                }

                                // Emission
                                if (!isHDRPMaterial && 
                                    "_EmissionMap" == texProperty)
                                {
                                    material.SetColor("_EmissionColor", Color.white);
                                    material.EnableKeyword("_EMISSION");
                                }

                                // Smoothness channel
                                if (material.HasProperty("_SmoothnessTextureChannel"))
                                {
                                    material.SetFloat("_SmoothnessTextureChannel", 0.0f);
                                }

                                // Keywords
                                if ("_DetailAlbedoMap" == texProperty || 
                                    "_DetailNormalMap" == texProperty)
                                    material.EnableKeyword("_DETAIL_MULX2");

                                if ("_ParallaxMap" == texProperty)
                                    material.EnableKeyword("_PARALLAXMAP");
                            }
                        }
                        else
                            Debug.Log((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: No texture found in material channel '{0}'.", (object)materialChannel.ChannelName));
                    }
                }
                else
                    Debug.Log((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unable to find shader '{0}'.", (object)shaderName));
                this.materials.Add(simplygonMaterial.Id, material);
                this.tempMaterialHolders.ForEach((Action<GameObject>)(holder => UnityEngine.Object.DestroyImmediate(holder)));
                this.tempMaterialHolders.Clear();
                return material;
            }
            catch (Exception ex)
            {
                Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to create material {0} ({1} {2})", (object)simplygonMaterial.Name, (object)ex.Message, (object)ex.StackTrace));
            }
            finally
            {
                if (createdMaterial)
                    UnityEngine.Object.DestroyImmediate(originalMaterial);
            }
            return (UnityEngine.Material)null;
        }

        private UnityEngine.Texture CreateTexture(Simplygon.Scene.Scene scene, Simplygon.Scene.Asset.Texture simplygonTexture, bool isNormalsTexture, bool alphaIsTransparency)
        {
            if (Application.platform == RuntimePlatform.OSXEditor)
                simplygonTexture.Path = simplygonTexture.Path.Replace('\\', '/');
            string destFileName = Path.Combine(this.CurrentJob.AssetFullMaterialDirectory, Path.GetFileName(simplygonTexture.Path));
            string str1 = string.Format(CultureInfo.InvariantCulture, "Assets/LODs/{0}/{1}", (object)this.CurrentJob.AssetMaterialDirectory, (object)Path.GetFileName(simplygonTexture.Path));
            string str2 = Path.Combine(scene.SceneDirectory, simplygonTexture.Path);
            if (!File.Exists(str2))
                return (UnityEngine.Texture)null;
            File.Copy(str2, destFileName, true);
            AssetDatabase.Refresh();
            TextureImporter atPath = AssetImporter.GetAtPath(str1) as TextureImporter;
            if (atPath != null)
            {
                atPath.textureType = !isNormalsTexture ? TextureImporterType.Default : TextureImporterType.NormalMap;
                atPath.alphaIsTransparency = alphaIsTransparency;
            }
            else
                Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Texture importer from path {0} unexpectedly null.", (object)str1));
            AssetDatabase.ImportAsset(str1, ImportAssetOptions.ForceUpdate);
            return AssetDatabase.LoadAssetAtPath(str1, typeof(UnityEngine.Texture)) as UnityEngine.Texture;
        }

        private void SetShaderTextureProperty(string shaderPropertyName, UnityEngine.Material material, MaterialChannel simplygonMaterialChannel)
        {
            if (simplygonMaterialChannel.Textures.Count > 0)
            {
                UnityEngine.Texture texture;
                if (!this.textures.TryGetValue(simplygonMaterialChannel.Textures.ElementAt<MaterialChannelTextureDescriptor>(0).Texture, out texture))
                    return;
                material.SetTexture(shaderPropertyName, texture);
            }
            else
                Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: No texture found in material channel '{0}'.", (object)simplygonMaterialChannel.ChannelName));
        }

        private void SetShaderColorProperty(string shaderPropertyName, UnityEngine.Material material, MaterialChannel simplygonMaterialChannel)
        {
            Simplygon.Math.Color color1 = simplygonMaterialChannel.Color;
            UnityEngine.Color color2 = new UnityEngine.Color(color1.R, color1.G, color1.B, color1.A);
            material.SetColor(shaderPropertyName, color2);
        }
    }
}
