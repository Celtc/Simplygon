﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.Jobs.UnityCloudJob
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Yoda.Client;
using Simplygon.Cloud.Yoda.IntegrationClient;
using System.IO;

namespace Simplygon.Unity.EditorPlugin.Jobs
{
    internal class UnityCloudJob
    {
        public CloudJob CloudJob
        {
            get; protected set;
        }

        public string AssetDirectory
        {
            get; set;
        }

        public string AssetMaterialDirectory
        {
            get; set;
        }

        public string AssetFullDirectory
        {
            get; set;
        }

        public string AssetFullMaterialDirectory
        {
            get; set;
        }

        public bool PendingPrefabValid
        {
            get; set;
        }

        public bool PendingPrefabAvailable
        {
            get; set;
        }

        public UnityCloudJob(CloudJob cloudJob)
        {
            this.CloudJob = cloudJob;
            if (!Directory.Exists(SharedData.Instance.UniqueTempPathInProject))
                Directory.CreateDirectory(SharedData.Instance.UniqueTempPathInProject);
            this.PendingPrefabValid = false;
            this.PendingPrefabAvailable = false;
        }

        public int LODCount()
        {
            int result = -1;
            if (this.CloudJob.SPL != null)
            {
                result = (this.CloudJob.SPL as Simplygon.SPL.v80.SPL).LODCount();
            }
            else
            {
                this.PendingPrefabValid = false;
                Job23 job = this.CloudJob.StateHandler.Client.GetJob(this.CloudJob.MappingId);
                if (job != null)
                    int.TryParse(job.ProgressLODCount, out result);
            }
            return result;
        }

        public void Update()
        {
            if (this.CloudJob.JobCustomData == null)
                return;
            this.PendingPrefabValid = AssetDatabaseEx.IsPendingLODValid(this.CloudJob.JobCustomData.UnityPendingLODFolderName, this.LODCount());
            this.PendingPrefabAvailable = AssetDatabaseEx.IsPendingLODAvailable(this.CloudJob.JobCustomData.UnityPendingLODFolderName);
        }
    }
}
