﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.Jobs.JobManager20
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json;
using Simplygon.Cloud.Yoda.Client;
using Simplygon.Cloud.Yoda.IntegrationClient;
using Simplygon.Hash;
using Simplygon.Scene.Common.Interfaces;
using Simplygon.Scene.Common.Progress;
using Simplygon.Scene.Common.WorkDirectory;
using Simplygon.Scene.FileFormat;
using Simplygon.Scene.FileFormat.SSF;
using Simplygon.SPL;
using Simplygon.Unity.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Globalization;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin.Jobs
{
    public class JobManager20
    {
        private object jobsLock = new object();
        private object deletedJobsLock = new object();
        private List<UnityCloudJob> deletedJobs = new List<UnityCloudJob>();
        private WorkDirectoryInfo workDirectory;

        internal List<UnityCloudJob> Jobs
        {
            get; private set;
        }

        public int ProcessingJobCount
        {
            get
            {
                lock (this.jobsLock)
                    return this.Jobs.Count<UnityCloudJob>((Func<UnityCloudJob, bool>)(job =>
                   {
                       if (!job.CloudJob.StateHandler.IsJobInFinalState)
                           return !job.CloudJob.StateHandler.IsJobBeingDeleted;
                       return false;
                   }));
            }
        }

        public int JobCount
        {
            get
            {
                lock (this.jobsLock)
                    return this.Jobs.Count;
            }
        }

        public JobManager20()
        {
            this.Jobs = new List<UnityCloudJob>();
            this.workDirectory = new WorkDirectoryInfo(SharedData.Instance.UniqueTempPathInProject);
        }

        internal void GMThread_CreateJob(string name, List<PrefabEx> prefabs, bool isCascadedLOD, Dispatcher gmDispatcher, Action onCompleted)
        {
            string temporaryPendingLODFolderId = string.Format(CultureInfo.InvariantCulture, "Tmp_{0}", (object)Guid.NewGuid().ToString());
            try
            {
                Simplygon.Scene.Scene scene = (Simplygon.Scene.Scene)null;
                string assetId = string.Empty;
                SharedData.Instance.UnityDispatcher.BeginInvoke(new DispatcherAction((Action)(() =>
               {
                   try
                   {
                       scene = this.UnityThread_ExportJob(temporaryPendingLODFolderId, name, prefabs);
                       assetId = SharedData.Instance.UseCachedAsset ? PrefabEx.ComputeHash(prefabs, scene) : PrefabEx.GetUniqueHash();
                   }
                   catch (Exception ex)
                   {
                       Debug.LogError((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: JobManager failed to export job ({0}\n{1})", (object)ex.Message, (object)ex.StackTrace));
                       onCompleted();
                   }
               }), (Action)(() =>
       {
           if (scene == null)
           {
               onCompleted();
           }
           else
           {
               CloudJob job = JobFactory.Instance.CreateJob(scene, assetId, (ISPL)SettingsUtility.CreateSPLFromCurrentNodes(isCascadedLOD), this.workDirectory, "Initialize");
               job.Name = name;
               job.CreatedOn = DateTime.UtcNow;
               UnityCloudJob unityCloudJob = new UnityCloudJob(job);
               unityCloudJob.CloudJob.StateHandler.OnJobInitialized += (EventHandler<JobStateChangedEventargs>)((s, e) =>
       {
           if (unityCloudJob.CloudJob.JobCustomData == null)
               unityCloudJob.CloudJob.JobCustomData = new CloudJob.CustomData();
           unityCloudJob.CloudJob.JobCustomData.UnityPendingLODFolderName = SHA256.ComputeString(assetId + unityCloudJob.CloudJob.StateHandler.CurrentSPLId);
           unityCloudJob.CloudJob.JobCustomData.ApplicationName = "Unity";
           unityCloudJob.CloudJob.JobCustomData.ApplicationVersion = Assembly.GetAssembly(typeof(JobManager20)).GetName().Version.ToString();
           SharedData.Instance.UnityDispatcher.BeginInvoke(new DispatcherAction((Action)(() => AssetDatabaseEx.MovePendingLODFolderPrefabs(temporaryPendingLODFolderId, unityCloudJob.CloudJob.JobCustomData.UnityPendingLODFolderName))));
       });
               unityCloudJob.CloudJob.StateHandler.OnJobDeleted += (EventHandler<JobStateChangedEventargs>)((s, e) => this.OnJobDeleted(e.Job));
               unityCloudJob.CloudJob.StateHandler.OnStateChanged += (EventHandler)((s, e) =>
       {
           SharedData.Instance.UnityDispatcher.BeginInvoke(new DispatcherAction((Action)(() => SharedData.Instance.SimplygonWindow.ForceRepaint = true)));
           unityCloudJob.Update();
       });
               lock (this.jobsLock)
                   this.Jobs.Add(unityCloudJob);
               unityCloudJob.CloudJob.StateHandler.Run(false);
               onCompleted();
           }
       }), gmDispatcher));
            }
            catch (Exception ex)
            {
                Debug.LogError((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: JobManager failed to create job ({0}\n{1})", (object)ex.Message, (object)ex.StackTrace));
                AssetDatabaseEx.DeletePendingLODFolder(temporaryPendingLODFolderId);
                onCompleted();
            }
        }

        public void RemoveDeletedJobs()
        {
            lock (this.deletedJobsLock)
            {
                foreach (UnityCloudJob deletedJob in this.deletedJobs)
                {
                    if (!string.IsNullOrEmpty(deletedJob.CloudJob.JobCustomData.UnityPendingLODFolderName))
                        AssetDatabaseEx.DeletePendingLODFolder(deletedJob.CloudJob.JobCustomData.UnityPendingLODFolderName);
                    lock (this.jobsLock)
                        this.Jobs.Remove(deletedJob);
                }
                this.deletedJobs.Clear();
            }
        }

        internal void OnJobDeleted(CloudJob job)
        {
            lock (this.deletedJobsLock)
            {
                UnityCloudJob unityCloudJob1 = this.Jobs.Find((Predicate<UnityCloudJob>)(unityCloudJob => unityCloudJob.CloudJob == job));
                if (unityCloudJob1 == null)
                    return;
                this.deletedJobs.Add(unityCloudJob1);
            }
        }

        private Simplygon.Scene.Scene UnityThread_ExportJob(string temporaryPendingLODFolderId, string name, List<PrefabEx> prefabs)
        {
            SharedData.Instance.SimplygonWindow.ForceRepaint = true;
            GameObject gameObject1 = (GameObject)null;
            try
            {
                gameObject1 = PrefabUtilityEx.CreateCloneForGameObjects(name, prefabs);
                AssetDatabaseEx.CreatePendingLODFolder(temporaryPendingLODFolderId);
                for (int index = 0; index < SharedData.Instance.LODChain.Count; ++index)
                    PrefabUtilityEx.SavePrefab(PrefabUtilityEx.GetPendingPrefabPath(temporaryPendingLODFolderId, name, index + 1), gameObject1);
                Exporter exporter = new Exporter();
                this.CreateDirectory(this.workDirectory.TexturesPath);
                GameObject gameObject2 = gameObject1;
                WorkDirectoryInfo workDirectory = this.workDirectory;
                return exporter.Export(gameObject2, workDirectory);
            }
            catch (Exception ex)
            {
                Debug.Log((object)string.Format(CultureInfo.InvariantCulture, "Simplygon exception - ExportJob: {0}", (object)ex.Message));
                AssetDatabaseEx.DeletePendingLODFolder(temporaryPendingLODFolderId);
                throw;
            }
            finally
            {
                UnityEngine.Object.DestroyImmediate(gameObject1);
            }
        }

        internal void UnityThread_ImportJob(YodaClient23 simplygonCloudClient, UnityCloudJob job, List<string> lodFilePaths, Action onCompleted)
        {
            try
            {
                this.CreateDirectory(this.workDirectory.TexturesPath);
                new Importer20(this.workDirectory).Import(job, lodFilePaths);
                if (!false)
                {
                    try
                    {
                        Window.CreateLODGroup(job);
                    }
                    catch (Exception ex)
                    {
                        Debug.Log((object)string.Format(CultureInfo.InvariantCulture, "Simplygon error - Failed to create LODGroup for {0}", (object)job.CloudJob.Name));
                    }
                }
                onCompleted();
                if (!job.CloudJob.StateHandler.IsJobCompleted)
                    return;
                simplygonCloudClient.DeleteJob(job.CloudJob.MappingId);
            }
            catch (Exception ex)
            {
                Debug.Log((object)string.Format(CultureInfo.InvariantCulture, "Simplygon exception - ImportJob: {0}", (object)ex.Message));
            }
            finally
            {
                SharedData.Instance.GeneralManager.UpdateAccount();
            }
        }

        internal void GMThread_LoadJobs(YodaClient23 simplygonCloudClient)
        {
            try
            {
                List<Job23> activeJobs = simplygonCloudClient.GetActiveJobs();
                lock (this.jobsLock)
                {
                    this.CreateDirectory(this.workDirectory.TexturesPath);
                    foreach (Job23 job23 in activeJobs)
                    {
                        CloudJob job = JobFactory.Instance.CreateJob(this.workDirectory, job23.JobId, job23.Status);
                        bool flag1 = false;
                        try
                        {
                            job.JobCustomData = JsonConvert.DeserializeObject<CloudJob.CustomData>(job23.CustomData);
                            flag1 = true;
                        }
                        catch (Exception ex)
                        {
                            Debug.Log((object)string.Format(CultureInfo.InvariantCulture, "Simplygon exception - LoadJobs - Deserialize custom data: {0}", (object)ex.Message));
                            simplygonCloudClient.DeleteJob(job23.JobId);
                        }
                        bool flag2 = false;
                        bool flag3 = false;
                        if (job.JobCustomData != null)
                        {
                            flag2 = !string.IsNullOrEmpty(job.JobCustomData.ApplicationName) && job.JobCustomData.ApplicationName.ToLower() == "unity";
                            flag3 = AssetDatabaseEx.IsPendingLODAvailable(job.JobCustomData.UnityPendingLODFolderName);
                        }
                        if (flag1 & flag2 & flag3)
                        {
                            job.Name = job23.Name;
                            DateTime result;
                            job.CreatedOn = DateTime.TryParse(job23.Created, out result) ? result : DateTime.MinValue;
                            UnityCloudJob unityCloudJob = new UnityCloudJob(job);
                            unityCloudJob.CloudJob.StateHandler.OnJobDeleted += (EventHandler<JobStateChangedEventargs>)((s, e) => this.OnJobDeleted(e.Job));
                            unityCloudJob.CloudJob.StateHandler.OnStateChanged += (EventHandler)((s, e) =>
                           {
                               SharedData.Instance.UnityDispatcher.BeginInvoke(new DispatcherAction((Action)(() => SharedData.Instance.SimplygonWindow.ForceRepaint = true)));
                               unityCloudJob.Update();
                           });
                            this.Jobs.Add(unityCloudJob);
                            unityCloudJob.CloudJob.StateHandler.Run(false);
                        }
                    }
                }
                foreach (string pendingLoD in AssetDatabaseEx.GetPendingLODs())
                {
                    string pendingLOD = pendingLoD;
                    try
                    {
                        if (!activeJobs.Exists((Predicate<Job23>)(job =>
                       {
                           if (string.IsNullOrEmpty(job.CustomData))
                               return false;
                           return pendingLOD == JsonConvert.DeserializeObject<CloudJob.CustomData>(job.CustomData).UnityPendingLODFolderName;
                       })))
                            SharedData.Instance.UnityDispatcher.BeginInvoke(new DispatcherAction((Action)(() => AssetDatabaseEx.DeletePendingLODFolder(pendingLOD))));
                    }
                    catch (Exception ex)
                    {
                        SharedData.Instance.UnityDispatcher.BeginInvoke(new DispatcherAction((Action)(() => AssetDatabaseEx.DeletePendingLODFolder(pendingLOD))));
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log((object)string.Format(CultureInfo.InvariantCulture, "Simplygon exception - LoadJobs: {0}", (object)ex.Message));
            }
        }

        internal void GMThread_UpdateUserSettings(YodaClient23 simplygonCloudClient, Dispatcher gmDispatcher)
        {
        }

        internal void WriteSceneToWorkDirectory(Simplygon.Scene.Scene scene, string outputFilePath)
        {
            SSFWriterFactory.GetWriter(WriterVersion.SSFWriterV20).Write(scene, outputFilePath, true, false, (SceneProgress)null, (IWorkDirectoryInfo)null);
        }

        private void CreateDirectory(string path)
        {
            try
            {
                if (Directory.Exists(path))
                    return;
                Directory.CreateDirectory(path);
            }
            catch (Exception ex)
            {
                Debug.LogError((object)string.Format(CultureInfo.InvariantCulture, "Failed to create texture directory {0} ({1})", (object)path, (object)ex.Message));
                throw;
            }
        }
    }
}
