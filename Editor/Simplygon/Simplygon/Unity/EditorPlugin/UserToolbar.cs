﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.UserToolbar
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.Globalization;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
  public class UserToolbar
  {
    private Vector2 scrollPositon = Vector2.zero;

    public void Update()
    {
    }

    public void OnInspectorUpdate()
    {
    }

    public void Show()
    {
      this.scrollPositon = EditorGUILayout.BeginScrollView(this.scrollPositon);
      EditorGUILayout.LabelField("User", SharedData.Instance.Account.Name, new GUILayoutOption[0]);
      EditorGUILayout.LabelField("Subscription", SharedData.Instance.Account.ProcessSubscription, new GUILayoutOption[0]);
      float processingTimePerMonth = SharedData.Instance.Account.ProcessSubscriptionRestrictions.ProcessingTimePerMonth;
      float num1 = (double) SharedData.Instance.Account.ProcesssingTimeUsedThisMonth > (double) processingTimePerMonth ? processingTimePerMonth : SharedData.Instance.Account.ProcesssingTimeUsedThisMonth;
      float num2 = num1 / processingTimePerMonth;
      if ((double) num2 > 0.5)
      {
        EditorGUI.ProgressBar(EditorGUILayout.BeginVertical(), num2, string.Format(CultureInfo.InvariantCulture, "Processing time used this month {0:0.0}h/{1:0.0}h", (object) (float) ((double) num1 / 3600.0), (object) (float) ((double) processingTimePerMonth / 3600.0)));
        GUILayout.Space(16f);
        EditorGUILayout.EndVertical();
      }
      if (GUILayout.Button("Log out"))
        SharedData.Instance.GeneralManager.Logout();
      EditorGUILayout.EndScrollView();
    }

    private void BuyCreditsGUI()
    {
      EditorGUILayout.BeginVertical();
      EditorGUILayout.LabelField("Buy Credits as you go", EditorStyles.boldLabel, new GUILayoutOption[0]);
      GUILayout.Label("Read more about how the credit system works here");
      GUILayout.Label("simplygon.com/unity", SharedData.Instance.LinkLabel, new GUILayoutOption[0]);
      EditorGUILayout.EndVertical();
      Rect lastRect = GUILayoutUtility.GetLastRect();
      EditorGUIUtility.AddCursorRect(lastRect, MouseCursor.Link);
      if (Event.current.type == EventType.MouseDown && lastRect.Contains(Event.current.mousePosition))
        Application.OpenURL("http://www.simplygon.com/unity");
      this.SeparatorGUI();
      EditorGUILayout.BeginHorizontal();
      EditorGUILayout.BeginVertical();
      EditorGUILayout.LabelField("Bronze pack", EditorStyles.boldLabel, new GUILayoutOption[0]);
      GUILayout.Label("100 Simplygon credits for $29");
      GUILayout.Label("Value $100");
      GUILayout.Label("\n\n");
      EditorGUILayout.EndVertical();
      if (GUILayout.Button((Texture) ResourcesEx.LoadBronzeCoin(), new GUILayoutOption[2]
      {
        GUILayout.Width(140f),
        GUILayout.Height(140f)
      }))
        Application.OpenURL("https://www.assetstore.unity3d.com/#/content/14927");
      EditorGUILayout.EndHorizontal();
      this.SeparatorGUI();
      EditorGUILayout.BeginHorizontal();
      EditorGUILayout.BeginVertical();
      EditorGUILayout.LabelField("Silver pack", EditorStyles.boldLabel, new GUILayoutOption[0]);
      GUILayout.Label("300 Simplygon credits for $79");
      GUILayout.Label("Value $300");
      GUILayout.Label("\n\n");
      EditorGUILayout.EndVertical();
      if (GUILayout.Button((Texture) ResourcesEx.LoadSilverCoin(), new GUILayoutOption[2]
      {
        GUILayout.Width(140f),
        GUILayout.Height(140f)
      }))
        Application.OpenURL("https://www.assetstore.unity3d.com/#/content/14929");
      EditorGUILayout.EndHorizontal();
      this.SeparatorGUI();
      EditorGUILayout.BeginHorizontal();
      EditorGUILayout.BeginVertical();
      EditorGUILayout.LabelField("Gold pack", EditorStyles.boldLabel, new GUILayoutOption[0]);
      GUILayout.Label("1,500 Simplygon credits for $349");
      GUILayout.Label("Value $1,500");
      GUILayout.Label("\n\n");
      EditorGUILayout.EndVertical();
      if (GUILayout.Button((Texture) ResourcesEx.LoadGoldCoin(), new GUILayoutOption[2]
      {
        GUILayout.Width(140f),
        GUILayout.Height(140f)
      }))
        Application.OpenURL("https://www.assetstore.unity3d.com/#/content/14930");
      EditorGUILayout.EndHorizontal();
    }

    private void SeparatorGUI()
    {
      GUILayout.Box("", new GUILayoutOption[2]
      {
        GUILayout.ExpandWidth(true),
        GUILayout.Height(1f)
      });
    }
  }
}
