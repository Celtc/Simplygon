﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.NodeWindowStep
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class NodeWindowStep : NodeWindow
    {
        public NodeWindowStep(int nodeRef)
          : base(nodeRef)
        {
            this.Title = "Step";
            this.AddInputSlot(SlotComponents.RGBA);
            this.AddInputSlot(SlotComponents.RGBA);
            List<SlotComponents> list = ((IEnumerable<SlotComponents>)Enum.GetValues(typeof(SlotComponents))).ToList<SlotComponents>();
            list.Remove(SlotComponents.RGBA);
            list.Insert(0, SlotComponents.RGBA);
            list.Remove(SlotComponents.RGB);
            list.Insert(1, SlotComponents.RGB);
            foreach (SlotComponents slotComponents in list)
                this.AddOutputSlot(slotComponents);
            this.HelpText = "The Step node pointwise evaluates to a one vector (1,1,1,1) if the color value in the topmost input is larger than that of the bottommost input, otherwise the point is evaluated to a zero vector (0,0,0,0). An unconnected RGBA input will produce a zero vector (black color).";
        }

        public override void DrawContents(Rect parent)
        {
            base.DrawContents(parent);
            this.SetWindowWithinBounds(parent);
        }

        internal override void Save(List<ModifierShadingNodeProperties> modifierShadingNodePropertiesCollection)
        {
            this.GenericSave(modifierShadingNodePropertiesCollection, ModifierShadingNodeType.Step, new int[2]
            {
        -1,
        -1
            }, new int[2] { -1, -1 });
        }
    }
}
