﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.JobsToolbar
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Unity.EditorPlugin.Jobs;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
  public class JobsToolbar
  {
    private readonly GUIContent AutomaticAssetDownloadGUIContent = new GUIContent("Download Assets Automatically", "If set, assets will be automatically downloaded as soon as they have been processed instead of requiring a separate 'Download' action.");
    private readonly GUIContent RemoveJobGUIContent = new GUIContent("X", "Remove job");
    private Vector2 scrollPosition = Vector2.zero;
    private List<JobsToolbar.UIJob> jobsBeingShown = new List<JobsToolbar.UIJob>();
    private List<JobsToolbar.UIJob> jobsToRemove = new List<JobsToolbar.UIJob>();
    private DateTime lastImportTimeStamp = DateTime.MinValue;
    private bool automaticAssetDownload;

    internal JobsToolbar()
    {
      this.automaticAssetDownload = SharedData.Instance.Settings.GetDownloadAssetsAutomatically();
    }

    public void Update()
    {
      try
      {
        if (Application.isPlaying)
          return;
        SharedData.Instance.GeneralManager.JobManager.RemoveDeletedJobs();
        this.jobsToRemove.ForEach((Action<JobsToolbar.UIJob>) (jobToRemove => this.jobsBeingShown.Remove(jobToRemove)));
        this.jobsToRemove.Clear();
        List<UnityCloudJob> jobsToShow = SharedData.Instance.GeneralManager.JobManager.Jobs.FindAll((Predicate<UnityCloudJob>) (job =>
        {
          if (!job.CloudJob.StateHandler.IsJobBeingDeleted)
            return !job.CloudJob.StateHandler.IsJobDeleted;
          return false;
        }));
        this.jobsBeingShown.RemoveAll((Predicate<JobsToolbar.UIJob>) (job => !jobsToShow.Contains(job.UnityCloudJob)));
        jobsToShow.ForEach((Action<UnityCloudJob>) (jobToShow =>
        {
          if (this.jobsBeingShown.Any<JobsToolbar.UIJob>((Func<JobsToolbar.UIJob, bool>) (job => job.UnityCloudJob == jobToShow)))
            return;
          this.jobsBeingShown.Add(new JobsToolbar.UIJob(jobToShow));
        }));
        DateTime now = DateTime.Now;
        foreach (JobsToolbar.UIJob uiJob in this.jobsBeingShown)
        {
          uiJob.Update();
          if (uiJob.IsAwaitingDownloadAccept && SharedData.Instance.Settings.GetDownloadAssetsAutomatically())
            uiJob.UnityCloudJob.CloudJob.AcceptDownload();
          else if (uiJob.IsAwaitingImportAccept && !SharedData.Instance.GameObjectsToExport && !SharedData.Instance.GameObjectsToImport && (now - this.lastImportTimeStamp).TotalSeconds >= 2.0)
          {
            uiJob.UnityCloudJob.CloudJob.AcceptImport();
            this.lastImportTimeStamp = DateTime.Now;
          }
        }
        SharedData.Instance.GameObjectsToExport = this.jobsBeingShown.Any<JobsToolbar.UIJob>((Func<JobsToolbar.UIJob, bool>) (job => job.UnityCloudJob.CloudJob.StateHandler.IsJobBeingCreated));
        SharedData.Instance.GameObjectsToUpload = this.jobsBeingShown.Any<JobsToolbar.UIJob>((Func<JobsToolbar.UIJob, bool>) (job => job.UnityCloudJob.CloudJob.StateHandler.IsJobBeingUploaded));
        SharedData.Instance.GameObjectsToImport = this.jobsBeingShown.Any<JobsToolbar.UIJob>((Func<JobsToolbar.UIJob, bool>) (job => job.UnityCloudJob.CloudJob.StateHandler.IsJobBeingImported));
        SharedData.Instance.GameObjectsToDownload = this.jobsBeingShown.Any<JobsToolbar.UIJob>((Func<JobsToolbar.UIJob, bool>) (job => job.UnityCloudJob.CloudJob.StateHandler.IsJobBeingDownloaded));
        if (SharedData.Instance.GameObjectsToExport || SharedData.Instance.GameObjectsToUpload)
          SharedData.Instance.ExportProgress = (float) ((double) this.jobsBeingShown.Where<JobsToolbar.UIJob>((Func<JobsToolbar.UIJob, bool>) (job =>
          {
            if (!job.UnityCloudJob.CloudJob.StateHandler.IsJobBeingCreated)
              return job.UnityCloudJob.CloudJob.StateHandler.IsJobBeingUploaded;
            return true;
          })).Select<JobsToolbar.UIJob, int>((Func<JobsToolbar.UIJob, int>) (job => job.UnityCloudJob.CloudJob.TotalProgress)).Min() * 0.0500000007450581 + 0.00999999977648258);
        if (!SharedData.Instance.GameObjectsToImport && !SharedData.Instance.GameObjectsToDownload)
          return;
        SharedData.Instance.ImportProgress = (float) this.jobsBeingShown.Where<JobsToolbar.UIJob>((Func<JobsToolbar.UIJob, bool>) (job =>
        {
          if (!job.UnityCloudJob.CloudJob.StateHandler.IsJobBeingDownloaded)
            return job.UnityCloudJob.CloudJob.StateHandler.IsJobBeingImported;
          return true;
        })).Select<JobsToolbar.UIJob, int>((Func<JobsToolbar.UIJob, int>) (job => job.UnityCloudJob.CloudJob.TotalProgress)).Min();
      }
      catch (Exception ex)
      {
        UIUtility.LogException("JobsToolbar::Update() failed", ex);
      }
    }

    public void OnInspectorUpdate()
    {
    }

    public void Show()
    {
      this.scrollPosition = EditorGUILayout.BeginScrollView(this.scrollPosition);
      float labelWidth = EditorGUIUtility.labelWidth;
      EditorGUIUtility.labelWidth = 200f;
      bool flag = EditorGUILayout.Toggle(this.AutomaticAssetDownloadGUIContent, this.automaticAssetDownload, new GUILayoutOption[0]);
      if (flag != this.automaticAssetDownload)
      {
        this.automaticAssetDownload = flag;
        SharedData.Instance.Settings.SetDownloadAssetsAutomatically(this.automaticAssetDownload);
      }
      EditorGUIUtility.labelWidth = labelWidth;
      this.SeparatorGUI();
      EditorGUILayout.Space();
      if (this.jobsBeingShown.Count == 0)
      {
        EditorGUILayout.HelpBox("No jobs in queue for processing", MessageType.Info);
      }
      else
      {
        foreach (JobsToolbar.UIJob job in this.jobsBeingShown)
        {
          EditorGUILayout.BeginHorizontal();
          job.Foldout = EditorGUILayout.Foldout(job.Foldout, job.Name, SharedData.Instance.FoldOutStyle);
          if (GUILayout.Button(this.RemoveJobGUIContent, new GUILayoutOption[2]
          {
            GUILayout.Height(24f),
            GUILayout.Width(24f)
          }))
          {
            job.UnityCloudJob.CloudJob.StateHandler.RequestJobDeletion();
            if (!this.jobsToRemove.Contains(job))
              this.jobsToRemove.Add(job);
          }
          EditorGUILayout.EndHorizontal();
          if (job.Foldout)
          {
            EditorGUILayout.LabelField("Status", job.Status, new GUILayoutOption[0]);
            if (!job.IsInFinalState)
              this.ProgressBarGUI(job);
            if (job.IsAwaitingImportAccept)
            {
              if (!job.PendingPrefabValid)
                EditorGUILayout.LabelField(string.Format(CultureInfo.InvariantCulture, "Prefab for pending LOD not found. Please submit job again."));
            }
            else if (job.IsAwaitingDownloadAccept)
            {
              if (GUILayout.Button("Download"))
                job.UnityCloudJob.CloudJob.AcceptDownload();
            }
            else if (job.IsFailed)
              this.UploadFailedGUI(job);
          }
          this.SeparatorGUI();
        }
      }
      EditorGUILayout.EndScrollView();
    }

    private void UploadFailedGUI(JobsToolbar.UIJob job)
    {
      EditorGUILayout.LabelField(string.Format(CultureInfo.InvariantCulture, "Job failed. Please submit job again."));
    }

    private void ProgressBarGUI(JobsToolbar.UIJob job)
    {
      EditorGUI.ProgressBar(EditorGUILayout.BeginVertical(), (float) job.TaskProgress * 0.01f, string.Format(CultureInfo.InvariantCulture, "{0} %", (object) job.TaskProgress));
      GUILayout.Space(16f);
      EditorGUILayout.EndVertical();
      EditorGUI.ProgressBar(EditorGUILayout.BeginVertical(), (float) job.TotalProgress * 0.01f, string.Format(CultureInfo.InvariantCulture, "{0} %", (object) job.TotalProgress));
      GUILayout.Space(16f);
      EditorGUILayout.EndVertical();
    }

    private void SeparatorGUI()
    {
      GUILayout.Box("", new GUILayoutOption[2]
      {
        GUILayout.ExpandWidth(true),
        GUILayout.Height(1f)
      });
    }

    private class UIJob
    {
      public UnityCloudJob UnityCloudJob { get; protected set; }

      public bool PendingPrefabValid { get; protected set; }

      public bool IsAwaitingDownloadAccept { get; protected set; }

      public bool IsAwaitingImportAccept { get; protected set; }

      public bool IsInFinalState { get; protected set; }

      public bool IsFailed { get; protected set; }

      public string Status { get; protected set; }

      public string Name { get; protected set; }

      public int TaskProgress { get; protected set; }

      public int TotalProgress { get; protected set; }

      public bool Foldout { get; set; }

      public UIJob(UnityCloudJob unityCloudJob)
      {
        this.UnityCloudJob = unityCloudJob;
        this.Foldout = true;
      }

      public void Update()
      {
        this.IsAwaitingDownloadAccept = this.UnityCloudJob.CloudJob.StateHandler.IsJobAwaitingDownloadAccept;
        this.IsAwaitingImportAccept = this.UnityCloudJob.CloudJob.StateHandler.IsJobAwaitingImportAccept;
        this.IsInFinalState = this.UnityCloudJob.CloudJob.StateHandler.IsJobInFinalState;
        this.IsFailed = this.UnityCloudJob.CloudJob.StateHandler.IsJobFailed;
        this.Status = this.UnityCloudJob.CloudJob.Message;
        this.Name = this.UnityCloudJob.CloudJob.Name;
        this.TaskProgress = this.UnityCloudJob.CloudJob.TaskProgress;
        this.TotalProgress = this.UnityCloudJob.CloudJob.TotalProgress;
        if (!this.IsAwaitingImportAccept)
          return;
        this.UnityCloudJob.Update();
        this.PendingPrefabValid = this.UnityCloudJob.PendingPrefabValid;
      }
    }
  }
}
