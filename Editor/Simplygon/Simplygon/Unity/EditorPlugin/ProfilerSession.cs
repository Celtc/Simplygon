﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.ProfilerSession
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.Unity.EditorPlugin
{
  public class ProfilerSession
  {
    public string Name { get; set; }

    public DateTime TimeStamp { get; set; }

    public TimeSpan ElapsedTime { get; set; }

    public ProfilerSession(string name)
    {
      this.Name = name;
    }

    public void Begin()
    {
      this.TimeStamp = DateTime.UtcNow;
    }

    public void End()
    {
      this.ElapsedTime = DateTime.UtcNow - this.TimeStamp;
    }
  }
}
