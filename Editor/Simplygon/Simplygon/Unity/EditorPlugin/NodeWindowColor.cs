﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.NodeWindowColor
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class NodeWindowColor : NodeWindow
    {
        private Color currentColor;

        public NodeWindowColor(int nodeRef, Color currentColor)
          : base(nodeRef)
        {
            this.currentColor = currentColor;
            this.Title = "Color";
            this.slotsUiOffset = new Vector2(0.0f, 40f);
            List<SlotComponents> list = ((IEnumerable<SlotComponents>)Enum.GetValues(typeof(SlotComponents))).ToList<SlotComponents>();
            list.Remove(SlotComponents.RGBA);
            list.Insert(0, SlotComponents.RGBA);
            list.Remove(SlotComponents.RGB);
            list.Insert(1, SlotComponents.RGB);
            foreach (SlotComponents slotComponents in list)
                this.AddOutputSlot(slotComponents);
            this.HelpText = "The Color node forwards an arbitrary color, selectable via the color picker within the node window.";
        }

        public override void DrawContents(Rect parent)
        {
            this.currentColor = EditorGUILayout.ColorField(this.currentColor, GUILayout.ExpandWidth(true));
            base.DrawContents(parent);
            GUILayout.Space(5f);
            this.SetWindowWithinBounds(parent);
        }

        internal override void Save(List<BasicShadingNodeProperties> basicShadingNodePropertiesCollection)
        {
            BasicShadingNodeProperties shadingNodeProperties = new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName(string.Format(CultureInfo.InvariantCulture, "{0} {1} {2} {3}", (object)this.currentColor.r, (object)this.currentColor.g, (object)this.currentColor.b, (object)this.currentColor.a)), string.Format(CultureInfo.InvariantCulture, "{0}Node{1}", (object)this.Title.Replace(" ", string.Empty), (object)this.NodeRef), this.NodeRef, BasicShadingNodeType.Color, "TexCoords0");
            basicShadingNodePropertiesCollection.Add(shadingNodeProperties);
        }
    }
}
