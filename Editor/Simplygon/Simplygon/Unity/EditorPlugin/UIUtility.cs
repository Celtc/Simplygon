﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.UIUtility
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Processor;
using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class UIUtility
    {
        private static readonly int MaxJobNameLength = 20;
        private static GUIStyle quickTextCenteredStyle = (GUIStyle)null;
        private static GUIStyle quickTextCenteredBoldStyle = (GUIStyle)null;
        private static volatile bool processingEnabled = true;
        private static GUIContent TargetAssetsGUIContent = new GUIContent("Selected Assets", "These assets will be uploaded to the Simplygon cloud service for processing.");

        public static GUIStyle QuickTextCenteredStyle
        {
            get
            {
                if (UIUtility.quickTextCenteredStyle == null)
                {
                    UIUtility.quickTextCenteredStyle = new GUIStyle();
                    UIUtility.quickTextCenteredStyle.fontSize = 12;
                    UIUtility.quickTextCenteredStyle.normal.textColor = EditorGUIUtility.isProSkin ? Color.white : Color.black;
                    UIUtility.quickTextCenteredStyle.alignment = TextAnchor.MiddleCenter;
                    UIUtility.quickTextCenteredStyle.wordWrap = true;
                }
                return UIUtility.quickTextCenteredStyle;
            }
        }

        public static GUIStyle QuickTextCenteredBoldStyle
        {
            get
            {
                if (UIUtility.quickTextCenteredBoldStyle == null)
                {
                    UIUtility.quickTextCenteredBoldStyle = new GUIStyle();
                    UIUtility.quickTextCenteredBoldStyle.fontSize = 12;
                    UIUtility.quickTextCenteredBoldStyle.fontStyle = FontStyle.Bold;
                    UIUtility.quickTextCenteredBoldStyle.normal.textColor = EditorGUIUtility.isProSkin ? Color.white : Color.black;
                    UIUtility.quickTextCenteredBoldStyle.alignment = TextAnchor.MiddleCenter;
                    UIUtility.quickTextCenteredBoldStyle.wordWrap = true;
                }
                return UIUtility.quickTextCenteredBoldStyle;
            }
        }

        public static void SeparatorGUI()
        {
            GUILayout.Box("", new GUILayoutOption[2]
            {
        GUILayout.ExpandWidth(true),
        GUILayout.Height(1f)
            });
        }

        public static void SeparatorGUI(string label, float startInset, float endInset)
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(startInset);
            GUILayout.FlexibleSpace();
            GUILayout.Label(label, EditorStyles.miniBoldLabel, new GUILayoutOption[0]);
            GUILayout.FlexibleSpace();
            GUILayout.Space(endInset);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(startInset);
            GUILayout.Box("", new GUILayoutOption[2]
            {
        GUILayout.ExpandWidth(true),
        GUILayout.Height(1f)
            });
            GUILayout.Space(endInset);
            EditorGUILayout.EndHorizontal();
        }

        public static void TargetAssetsGUI(ref bool showFoldOutAssets)
        {
            showFoldOutAssets = EditorGUILayout.Foldout(showFoldOutAssets, UIUtility.TargetAssetsGUIContent, SharedData.Instance.FoldOutStyle);
            if (!showFoldOutAssets)
                return;
            ++EditorGUI.indentLevel;
            if (SharedData.Instance.SelectionManager.SelectedPrefabs.Count > 0)
            {
                foreach (PrefabEx selectedPrefab in SharedData.Instance.SelectionManager.SelectedPrefabs)
                {
                    if (null != selectedPrefab.Prefab)
                    {
                        if (selectedPrefab.BoneCount > 0)
                            EditorGUILayout.LabelField(string.Format(CultureInfo.InvariantCulture, "{0} - Triangles ({1}) Bones({2})", (object)selectedPrefab.Prefab.name, (object)selectedPrefab.TriangleCount, (object)selectedPrefab.BoneCount));
                        else
                            EditorGUILayout.LabelField(string.Format(CultureInfo.InvariantCulture, "{0} - Triangles ({1})", (object)selectedPrefab.Prefab.name, (object)selectedPrefab.TriangleCount));
                    }
                }
            }
            else
                EditorGUILayout.LabelField("<No asset selected>");
            --EditorGUI.indentLevel;
        }

        public static void ProcessGUI()
        {
            if (SharedData.Instance.SelectionManager.DefaultedShaderNamesInCurrentSelection.Length != 0)
            {
                bool flag = SharedData.Instance.SelectionManager.DefaultedShaderNamesInCurrentSelection.Length > 1;
                EditorGUILayout.HelpBox(string.Format(CultureInfo.InvariantCulture, "No configured Simplygon shading {0} found for {1} {2}. \n\nPlease consider editing the shading {0} for {3} {1} in order to achieve better shader specific results than the default shading {0} when doing MaterialLOD or AggregateLOD.", flag ? (object)"networks" : (object)"network", flag ? (object)"shaders" : (object)"shader", (object)string.Join(", ", SharedData.Instance.SelectionManager.DefaultedShaderNamesInCurrentSelection), flag ? (object)"these" : (object)"this"), MessageType.Info);
            }
            if (SharedData.Instance.SelectionManager.SkinnedCharacterCount == 1 && (SettingsUtility.IsProxyLODEnabled() || SettingsUtility.IsAggregateLODEnabled()))
            {
                EditorGUILayout.HelpBox("Please make sure that the skinned asset has been exported in bind pose", MessageType.Info);
                EditorGUILayout.Space();
            }
            bool flag1 = false;
            if (SharedData.Instance.SelectionManager.SkinnedCharacterCount > 1)
                EditorGUILayout.HelpBox("Only one skinned character per job is supported", MessageType.Warning);
            else if (SharedData.Instance.SelectionManager.SkinnedCharacterCount == 1 && SharedData.Instance.SelectionManager.SelectedPrefabs.Count > 1)
                EditorGUILayout.HelpBox("Selection contains both skinned and non-skinned assets", MessageType.Warning);
            else if (!SharedData.Instance.GameObjectsToExport && !SharedData.Instance.GameObjectsToImport)
            {
                flag1 = true;
                EditorGUILayout.LabelField("Click me to Process", UIUtility.QuickTextCenteredBoldStyle, new GUILayoutOption[0]);
            }
            if (!(GUILayout.Button(flag1 ? (Texture)ResourcesEx.LoadProcessIcon() : (Texture)ResourcesEx.LoadProcessOffIcon()) & flag1) || !UIUtility.processingEnabled || SharedData.Instance.SelectionManager.SelectedPrefabs.Count == 0)
                return;
            List<string> processNodesOfType1 = SettingsUtility.GetSelectedProcessNodesOfType(typeof(ReductionProcessor));
            List<string> processNodesOfType2 = SettingsUtility.GetSelectedProcessNodesOfType(typeof(RemeshingProcessor));
            List<string> processNodesOfType3 = SettingsUtility.GetSelectedProcessNodesOfType(typeof(AggregationProcessor));
            if (!SharedData.Instance.Account.ProcessSubscriptionRestrictions.MeshLOD.Enabled && processNodesOfType1.Count > 0)
                EditorUtility.DisplayDialog("MeshLOD Disabled", string.Format(CultureInfo.InvariantCulture, "Please select a different processing type for LOD{0} #{1}.", processNodesOfType1.Count > 1 ? (object)"s" : (object)string.Empty, (object)string.Join(", #", processNodesOfType1.ToArray())), "OK");
            else if (!SharedData.Instance.Account.ProcessSubscriptionRestrictions.ProxyLOD.Enabled && processNodesOfType2.Count > 0)
                EditorUtility.DisplayDialog("ProxyLOD Disabled", string.Format(CultureInfo.InvariantCulture, "Please select a different processing type for LOD{0} #{1}.", processNodesOfType2.Count > 1 ? (object)"s" : (object)string.Empty, (object)string.Join(", #", processNodesOfType2.ToArray())), "OK");
            else if (!SharedData.Instance.Account.ProcessSubscriptionRestrictions.AggregateLOD.Enabled && processNodesOfType3.Count > 0)
            {
                EditorUtility.DisplayDialog("AggregateLOD Disabled", string.Format(CultureInfo.InvariantCulture, "Please select a different processing type for LOD{0} #{1}.", processNodesOfType3.Count > 1 ? (object)"s" : (object)string.Empty, (object)string.Join(", #", processNodesOfType3.ToArray())), "OK");
            }
            else
            {
                UIUtility.processingEnabled = false;
                int simultaneousJobs = SharedData.Instance.Account.ProcessSubscriptionRestrictions.SimultaneousJobs;
                int num = simultaneousJobs == -1 ? 100 : simultaneousJobs;
                if (SharedData.Instance.GeneralManager.JobManager.ProcessingJobCount < num)
                {
                    string name = SharedData.Instance.SelectionManager.Name;
                    if (name.Length > UIUtility.MaxJobNameLength)
                        name = name.Substring(0, UIUtility.MaxJobNameLength);
                    SharedData.Instance.GeneralManager.CreateJob(name, "myPriority", SharedData.Instance.SelectionManager.SelectedPrefabs, (Action)(() => UIUtility.processingEnabled = true));
                }
                else
                {
                    UIUtility.processingEnabled = true;
                    EditorUtility.DisplayDialog("Active Jobs Limit Reached", string.Format(CultureInfo.InvariantCulture, "Please wait for an ongoing job to be completed. The '{0}' subscription permits no more than {1} active job(s).", (object)SharedData.Instance.Account.ProcessSubscription, (object)num), "OK");
                }
            }
        }

        public static void LogException(string message, Exception ex)
        {
            Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: {0} ({1}\n{2}", (object)message, (object)ex.Message, (object)ex.StackTrace));
        }
    }
}
