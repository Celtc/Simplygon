﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.ResourcesEx
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class ResourcesEx
    {
        private const string SimplygonInstallationPath = @"Assets/Simplygon/Editor/Simplygon/";

        private static Texture2D bronzeCoin;
        private static Texture2D silverCoin;
        private static Texture2D goldCoin;
        private static Texture2D offModeIcon;
        private static Texture2D freeModeIcon;
        private static Texture2D payModeIcon;
        private static Texture2D payModeOffIcon;
        private static Texture2D createlodIcon_dark;
        private static Texture2D createlodIcon_light;
        private static Texture2D jobsIcon;
        private static Texture2D userIcon_dark;
        private static Texture2D userIcon_light;
        private static Texture2D debugIcon;
        private static Texture2D settingsIcon_dark;
        private static Texture2D settingsIcon_light;
        private static Texture2D createLODIconQuickStart_dark;
        private static Texture2D createLODIconQuickStart_light;
        private static Texture2D createLODIconAdvanced_dark;
        private static Texture2D createLODIconAdvanced_light;
        private static Texture2D processIcon;
        private static Texture2D processOffIcon;
        private static Texture2D processIcon_hover;
        private static Texture2D node_resize_handle;
        private static Texture2D node_bezier_texture;
        private static bool bronzeCoinDirty;
        private static bool silverCoinDirty;
        private static bool goldCoinDirty;
        private static bool offModeIconDirty;
        private static bool freeModeIconDirty;
        private static bool payModeIconDirty;
        private static bool payModeOffIconDirty;
        private static bool createlodIcon_darkDirty;
        private static bool createlodIcon_lightDirty;
        private static bool jobsIconDirty;
        private static bool userIcon_darkDirty;
        private static bool userIcon_lightDirty;
        private static bool debugIconDirty;
        private static bool settingsIcon_darkDirty;
        private static bool settingsIcon_lightDirty;
        private static bool createLODIconQuickStart_darkDirty;
        private static bool createLODIconQuickStart_lightDirty;
        private static bool createLODIconAdvanced_darkDirty;
        private static bool createLODIconAdvanced_lightDirty;
        private static bool processIcon_dirty;
        private static bool processOffIcon_dirty;
        private static bool processIcon_hover_dirty;
        private static bool node_resize_handle_dirty;
        private static bool node_bezier_texture_dirty;

        public static void IsDirty()
        {
            ResourcesEx.bronzeCoinDirty = true;
            ResourcesEx.silverCoinDirty = true;
            ResourcesEx.goldCoinDirty = true;
            ResourcesEx.offModeIconDirty = true;
            ResourcesEx.freeModeIconDirty = true;
            ResourcesEx.payModeIconDirty = true;
            ResourcesEx.payModeOffIconDirty = true;
            ResourcesEx.createlodIcon_darkDirty = true;
            ResourcesEx.createlodIcon_lightDirty = true;
            ResourcesEx.jobsIconDirty = true;
            ResourcesEx.userIcon_darkDirty = true;
            ResourcesEx.userIcon_lightDirty = true;
            ResourcesEx.debugIconDirty = true;
            ResourcesEx.settingsIcon_darkDirty = true;
            ResourcesEx.settingsIcon_lightDirty = true;
            ResourcesEx.createLODIconQuickStart_darkDirty = true;
            ResourcesEx.createLODIconQuickStart_lightDirty = true;
            ResourcesEx.createLODIconAdvanced_darkDirty = true;
            ResourcesEx.createLODIconAdvanced_lightDirty = true;
            ResourcesEx.processIcon_dirty = true;
            ResourcesEx.processOffIcon_dirty = true;
            ResourcesEx.processIcon_hover_dirty = true;
            ResourcesEx.node_resize_handle_dirty = true;
            ResourcesEx.node_bezier_texture_dirty = true;
        }

        public static Texture2D LoadSimplygonSymbolLogo()
        {
            return ResourcesEx.LoadDllResource("Simplygon_symbol_yellow_transparent", 96, 96);
        }

        public static Texture2D LoadSimplygonLogo()
        {
            return ResourcesEx.LoadDllResource("Simplygon_horisontal_yellow_transparent", 256, 64);
        }

        public static Texture2D LoadLoadingIcon(int index)
        {
            return ResourcesEx.LoadDllResource(string.Format(CultureInfo.InvariantCulture, "LoadIcon{0}", (object)index), 128, 128);
        }

        public static Texture2D LoadWorkingDarkIcon(int index)
        {
            return ResourcesEx.LoadDllResource(string.Format(CultureInfo.InvariantCulture, "managejobs_dark_{0}_v2", (object)index), 128, 64);
        }

        public static Texture2D LoadWorkingLightIcon(int index)
        {
            return ResourcesEx.LoadDllResource(string.Format(CultureInfo.InvariantCulture, "managejobs_light_{0}_v2", (object)index), 128, 64);
        }

        public static Texture2D LoadOffModeIcon()
        {
            if (ResourcesEx.offModeIcon == null || ResourcesEx.offModeIconDirty)
            {
                ResourcesEx.offModeIcon = ResourcesEx.LoadDllResource("offmode", 256, 128);
                ResourcesEx.offModeIconDirty = false;
            }
            return ResourcesEx.offModeIcon;
        }

        public static Texture2D LoadPayModeIcon()
        {
            if (ResourcesEx.payModeIcon == null || ResourcesEx.payModeIconDirty)
            {
                ResourcesEx.payModeIcon = ResourcesEx.LoadDllResource("paymode", 256, 128);
                ResourcesEx.payModeIconDirty = false;
            }
            return ResourcesEx.payModeIcon;
        }

        public static Texture2D LoadPayModeOffIcon()
        {
            if (ResourcesEx.payModeOffIcon == null || ResourcesEx.payModeOffIconDirty)
            {
                ResourcesEx.payModeOffIcon = ResourcesEx.LoadDllResource("paymode_off", 256, 128);
                ResourcesEx.payModeOffIconDirty = false;
            }
            return ResourcesEx.payModeOffIcon;
        }

        public static Texture2D LoadFreeModeIcon()
        {
            if (ResourcesEx.freeModeIcon == null || ResourcesEx.freeModeIconDirty)
            {
                ResourcesEx.freeModeIcon = ResourcesEx.LoadDllResource("freemode", 256, 128);
                ResourcesEx.freeModeIconDirty = false;
            }
            return ResourcesEx.freeModeIcon;
        }

        public static Texture2D LoadCreateLODIcon()
        {
            if (GUI.skin.name == "DarkSkin")
                return ResourcesEx.LoadCreateLODDarkIcon();
            return ResourcesEx.LoadCreateLODLightIcon();
        }

        private static Texture2D LoadCreateLODDarkIcon()
        {
            if (ResourcesEx.createlodIcon_dark == null || ResourcesEx.createlodIcon_darkDirty)
            {
                ResourcesEx.createlodIcon_dark = ResourcesEx.LoadDllResource("createlod_dark", 128, 64);
                ResourcesEx.createlodIcon_darkDirty = false;
            }
            return ResourcesEx.createlodIcon_dark;
        }

        private static Texture2D LoadCreateLODLightIcon()
        {
            if (ResourcesEx.createlodIcon_light == null || ResourcesEx.createlodIcon_lightDirty)
            {
                ResourcesEx.createlodIcon_light = ResourcesEx.LoadDllResource("createlod_light", 128, 64);
                ResourcesEx.createlodIcon_lightDirty = false;
            }
            return ResourcesEx.createlodIcon_light;
        }

        public static Texture2D LoadJobsIcon()
        {
            if (ResourcesEx.jobsIcon == null || ResourcesEx.jobsIconDirty)
            {
                ResourcesEx.jobsIcon = ResourcesEx.LoadDllResource("jobs", 128, 64);
                ResourcesEx.jobsIconDirty = false;
            }
            return ResourcesEx.jobsIcon;
        }

        public static Texture2D LoadUserIcon()
        {
            if (GUI.skin.name == "DarkSkin")
                return ResourcesEx.LoadUserDarkIcon();
            return ResourcesEx.LoadUserLightIcon();
        }

        public static Texture2D LoadUserDarkIcon()
        {
            if (ResourcesEx.userIcon_dark == null || ResourcesEx.userIcon_darkDirty)
            {
                ResourcesEx.userIcon_dark = ResourcesEx.LoadDllResource("user_dark_v2", 128, 64);
                ResourcesEx.userIcon_darkDirty = false;
            }
            return ResourcesEx.userIcon_dark;
        }

        public static Texture2D LoadUserLightIcon()
        {
            if (ResourcesEx.userIcon_light == null || ResourcesEx.userIcon_lightDirty)
            {
                ResourcesEx.userIcon_light = ResourcesEx.LoadDllResource("user_light_v2", 128, 64);
                ResourcesEx.userIcon_lightDirty = false;
            }
            return ResourcesEx.userIcon_light;
        }

        public static Texture2D LoadBronzeCoin()
        {
            if (ResourcesEx.bronzeCoin == null || ResourcesEx.bronzeCoinDirty)
            {
                ResourcesEx.bronzeCoin = ResourcesEx.LoadDllResource("bronze", 128, 128);
                ResourcesEx.bronzeCoinDirty = false;
            }
            return ResourcesEx.bronzeCoin;
        }

        public static Texture2D LoadSilverCoin()
        {
            if (ResourcesEx.silverCoin == null || ResourcesEx.silverCoinDirty)
            {
                ResourcesEx.silverCoin = ResourcesEx.LoadDllResource("silver", 128, 128);
                ResourcesEx.silverCoinDirty = false;
            }
            return ResourcesEx.silverCoin;
        }

        public static Texture2D LoadGoldCoin()
        {
            if (ResourcesEx.goldCoin == null || ResourcesEx.goldCoinDirty)
            {
                ResourcesEx.goldCoin = ResourcesEx.LoadDllResource("gold", 128, 128);
                ResourcesEx.goldCoinDirty = false;
            }
            return ResourcesEx.goldCoin;
        }

        public static Texture2D LoadDebugIcon()
        {
            if (ResourcesEx.debugIcon == null || ResourcesEx.debugIconDirty)
            {
                ResourcesEx.debugIcon = ResourcesEx.LoadDllResource("debug", 64, 64);
                ResourcesEx.debugIconDirty = false;
            }
            return ResourcesEx.debugIcon;
        }

        public static Texture2D LoadSettingsIcon()
        {
            if (EditorGUIUtility.isProSkin)
                return ResourcesEx.LoadSettingsIconLight();
            return ResourcesEx.LoadSettingsIconDark();
        }

        public static Texture2D LoadSettingsIconDark()
        {
            if (ResourcesEx.settingsIcon_dark == null || ResourcesEx.settingsIcon_darkDirty)
            {
                ResourcesEx.settingsIcon_dark = ResourcesEx.LoadDllResource("settings_dark_v2", 128, 128);
                ResourcesEx.settingsIcon_darkDirty = false;
            }
            return ResourcesEx.settingsIcon_dark;
        }

        public static Texture2D LoadSettingsIconLight()
        {
            if (ResourcesEx.settingsIcon_light == null || ResourcesEx.settingsIcon_lightDirty)
            {
                ResourcesEx.settingsIcon_light = ResourcesEx.LoadDllResource("settings_light_v2", 128, 128);
                ResourcesEx.settingsIcon_lightDirty = false;
            }
            return ResourcesEx.settingsIcon_light;
        }

        public static Texture2D LoadCreateLODQuickStartIcon()
        {
            if (EditorGUIUtility.isProSkin)
                return ResourcesEx.LoadCreateLODQuickStartIconDark();
            return ResourcesEx.LoadCreateLODQuickStartIconLight();
        }

        public static Texture2D LoadCreateLODQuickStartIconDark()
        {
            if (ResourcesEx.createLODIconQuickStart_dark == null || ResourcesEx.createLODIconQuickStart_darkDirty)
            {
                ResourcesEx.createLODIconQuickStart_dark = ResourcesEx.LoadDllResource("createlod_quick_start_v2", 128, 64);
                ResourcesEx.createLODIconQuickStart_darkDirty = false;
            }
            return ResourcesEx.createLODIconQuickStart_dark;
        }

        public static Texture2D LoadCreateLODQuickStartIconLight()
        {
            if (ResourcesEx.createLODIconQuickStart_light == null || ResourcesEx.createLODIconQuickStart_lightDirty)
            {
                ResourcesEx.createLODIconQuickStart_light = ResourcesEx.LoadDllResource("createlod_quick_start_light_v2", 128, 64);
                ResourcesEx.createLODIconQuickStart_lightDirty = false;
            }
            return ResourcesEx.createLODIconQuickStart_light;
        }

        public static Texture2D LoadCreateLODAdvancedIcon()
        {
            if (EditorGUIUtility.isProSkin)
                return ResourcesEx.LoadCreateLODAdvancedIconDark();
            return ResourcesEx.LoadCreateLODAdvancedIconLight();
        }

        public static Texture2D LoadCreateLODAdvancedIconDark()
        {
            if (ResourcesEx.createLODIconAdvanced_dark == null || ResourcesEx.createLODIconAdvanced_darkDirty)
            {
                ResourcesEx.createLODIconAdvanced_dark = ResourcesEx.LoadDllResource("createlod_advanced_v2", 128, 64);
                ResourcesEx.createLODIconAdvanced_darkDirty = false;
            }
            return ResourcesEx.createLODIconAdvanced_dark;
        }

        public static Texture2D LoadCreateLODAdvancedIconLight()
        {
            if (ResourcesEx.createLODIconAdvanced_light == null || ResourcesEx.createLODIconAdvanced_lightDirty)
            {
                ResourcesEx.createLODIconAdvanced_light = ResourcesEx.LoadDllResource("createlod_advanced_light_v2", 128, 64);
                ResourcesEx.createLODIconAdvanced_lightDirty = false;
            }
            return ResourcesEx.createLODIconAdvanced_light;
        }

        public static Texture2D LoadProcessIcon()
        {
            if (ResourcesEx.processIcon == null || ResourcesEx.processIcon_dirty)
            {
                ResourcesEx.processIcon = ResourcesEx.LoadDllResource("process", 256, 128);
                ResourcesEx.processIcon_dirty = false;
            }
            return ResourcesEx.processIcon;
        }

        public static Texture2D LoadProcessOffIcon()
        {
            if (ResourcesEx.processOffIcon == null || ResourcesEx.processOffIcon_dirty)
            {
                ResourcesEx.processOffIcon = ResourcesEx.LoadDllResource("process_off", 256, 128);
                ResourcesEx.processOffIcon_dirty = false;
            }
            return ResourcesEx.processOffIcon;
        }

        public static Texture2D LoadProcessIconHover()
        {
            if (ResourcesEx.processIcon_hover == null || ResourcesEx.processIcon_hover_dirty)
            {
                ResourcesEx.processIcon_hover = ResourcesEx.LoadDllResource("process_hover", 256, 128);
                ResourcesEx.processIcon_hover_dirty = false;
            }
            return ResourcesEx.processIcon_hover;
        }

        public static Texture2D LoadNodeResizeHandle()
        {
            if (ResourcesEx.node_resize_handle == null || ResourcesEx.node_resize_handle_dirty)
            {
                ResourcesEx.node_resize_handle = ResourcesEx.LoadDllResource("node_resize_handle", 20, 20);
                ResourcesEx.node_resize_handle_dirty = false;
            }
            return ResourcesEx.node_resize_handle;
        }

        public static Texture2D LoadNodeBezierTexture()
        {
            if (ResourcesEx.node_bezier_texture == null || ResourcesEx.node_bezier_texture_dirty)
            {
                ResourcesEx.node_bezier_texture = ResourcesEx.LoadDllResource("node_bezier_texture", 1, 5);
                ResourcesEx.node_bezier_texture_dirty = false;
            }
            return ResourcesEx.node_bezier_texture;
        }

        private static Texture2D LoadDllResource(string resourceName, int width, int height)
        {
            return (Texture2D)AssetDatabase.LoadAssetAtPath(SimplygonInstallationPath + "Simplygon/Unity/EditorPlugin/Resources/" + resourceName + ".png", typeof(Texture2D));

            //Texture2D texture2D = new Texture2D(width, height, TextureFormat.ARGB32, true, true);
            //Stream manifestResourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Simplygon.Unity.EditorPlugin.Resources." + resourceName + ".png");
            //if (manifestResourceStream != null)
            //{
            //    texture2D.LoadImage(ResourcesEx.ReadToEnd(manifestResourceStream));
            //    manifestResourceStream.Close();
            //    return texture2D;
            //}
            //Debug.LogError((object)string.Format(CultureInfo.InvariantCulture, "Failed to load {0} resource", (object)resourceName));
            //return (Texture2D)null;
        }

        private static byte[] ReadToEnd(Stream stream)
        {
            long position = stream.Position;
            stream.Position = 0L;
            try
            {
                byte[] buffer = new byte[4096];
                int length = 0;
                int num1;
                while ((num1 = stream.Read(buffer, length, buffer.Length - length)) > 0)
                {
                    length += num1;
                    if (length == buffer.Length)
                    {
                        int num2 = stream.ReadByte();
                        if (num2 != -1)
                        {
                            byte[] numArray = new byte[buffer.Length * 2];
                            Buffer.BlockCopy((Array)buffer, 0, (Array)numArray, 0, buffer.Length);
                            Buffer.SetByte((Array)numArray, length, (byte)num2);
                            buffer = numArray;
                            ++length;
                        }
                    }
                }
                byte[] numArray1 = buffer;
                if (buffer.Length != length)
                {
                    numArray1 = new byte[length];
                    Buffer.BlockCopy((Array)buffer, 0, (Array)numArray1, 0, length);
                }
                return numArray1;
            }
            finally
            {
                stream.Position = position;
            }
        }
    }
}
