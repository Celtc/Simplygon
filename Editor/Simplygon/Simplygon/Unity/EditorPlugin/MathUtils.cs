﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.MathUtils
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
  internal static class MathUtils
  {
    public static Matrix4x4 ToMatrix4x4(this Transform transform)
    {
      return Matrix4x4.TRS(transform.localPosition, transform.localRotation, transform.localScale);
    }

    public static void FromMatrix4x4(this Transform transform, Matrix4x4 matrix)
    {
      transform.localScale = matrix.GetScale();
      transform.rotation = matrix.GetRotation();
      transform.position = matrix.GetPosition();
    }

    public static Quaternion GetRotation(this Matrix4x4 matrix)
    {
      float num1 = Mathf.Sqrt(1f + matrix.m00 + matrix.m11 + matrix.m22) / 2f;
      float num2 = 4f * num1;
      double num3 = ((double) matrix.m21 - (double) matrix.m12) / (double) num2;
      float num4 = (matrix.m02 - matrix.m20) / num2;
      float num5 = (matrix.m10 - matrix.m01) / num2;
      double num6 = (double) num4;
      double num7 = (double) num5;
      double num8 = (double) num1;
      return new Quaternion((float) num3, (float) num6, (float) num7, (float) num8);
    }

    public static Vector3 GetPosition(this Matrix4x4 matrix)
    {
      double m03 = (double) matrix.m03;
      float m13 = matrix.m13;
      float m23 = matrix.m23;
      double num1 = (double) m13;
      double num2 = (double) m23;
      return new Vector3((float) m03, (float) num1, (float) num2);
    }

    public static Vector3 GetScale(this Matrix4x4 m)
    {
      double num1 = (double) Mathf.Sqrt((float) ((double) m.m00 * (double) m.m00 + (double) m.m01 * (double) m.m01 + (double) m.m02 * (double) m.m02));
      float num2 = Mathf.Sqrt((float) ((double) m.m10 * (double) m.m10 + (double) m.m11 * (double) m.m11 + (double) m.m12 * (double) m.m12));
      float num3 = Mathf.Sqrt((float) ((double) m.m20 * (double) m.m20 + (double) m.m21 * (double) m.m21 + (double) m.m22 * (double) m.m22));
      double num4 = (double) num2;
      double num5 = (double) num3;
      return new Vector3((float) num1, (float) num4, (float) num5);
    }
  }
}
