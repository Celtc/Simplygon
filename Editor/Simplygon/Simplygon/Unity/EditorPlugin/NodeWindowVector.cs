﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.NodeWindowVector
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Unity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class NodeWindowVector : NodeWindow
    {
        private readonly float VectorFieldWidth = 65f;
        private Vector4 colorVector;
        private static GUIStyle floatFieldStyle;

        public NodeWindowVector(int nodeRef, Vector4 colorVector)
          : base(nodeRef)
        {
            this.colorVector = colorVector;
            this.Title = "Vector";
            this.slotsUiOffset = new Vector2(0.0f, 22f);
            List<SlotComponents> list = ((IEnumerable<SlotComponents>)Enum.GetValues(typeof(SlotComponents))).ToList<SlotComponents>();
            list.Remove(SlotComponents.RGBA);
            list.Insert(0, SlotComponents.RGBA);
            list.Remove(SlotComponents.RGB);
            list.Insert(1, SlotComponents.RGB);
            foreach (SlotComponents slotComponents in list)
                this.AddOutputSlot(slotComponents);
            this.HelpText = "The Vector node forwards the color component values as set within the node window.";
            NodeWindowVector.floatFieldStyle = new GUIStyle(EditorStyles.miniTextField);
            NodeWindowVector.floatFieldStyle.margin = new RectOffset(0, 0, 0, 0);
        }

        public override void DrawContents(Rect parent)
        {
            base.DrawContents(parent);
            this.SetWindowWithinBounds(parent);
        }

        internal override void Save(List<BasicShadingNodeProperties> basicShadingNodePropertiesCollection)
        {
            BasicShadingNodeProperties shadingNodeProperties = new BasicShadingNodeProperties(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName(string.Format(CultureInfo.InvariantCulture, "{0} {1} {2} {3}", (object)this.colorVector.x, (object)this.colorVector.y, (object)this.colorVector.z, (object)this.colorVector.w)), string.Format(CultureInfo.InvariantCulture, "{0}Node{1}", (object)this.Title.Replace(" ", string.Empty), (object)this.NodeRef), this.NodeRef, BasicShadingNodeType.Vector, "TexCoords0");
            basicShadingNodePropertiesCollection.Add(shadingNodeProperties);
        }

        protected override void DrawFirstInsetContent()
        {
            Color backgroundColor = GUI.backgroundColor;
            GUILayout.BeginVertical();
            GUILayout.Space(36f);
            GUI.backgroundColor = Color.red;
            this.colorVector.x = EditorGUILayout.FloatField(this.colorVector.x, NodeWindowVector.floatFieldStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(this.VectorFieldWidth)
            });
            GUI.backgroundColor = Color.green;
            this.colorVector.y = EditorGUILayout.FloatField(this.colorVector.y, NodeWindowVector.floatFieldStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(this.VectorFieldWidth)
            });
            GUI.backgroundColor = Color.blue;
            this.colorVector.z = EditorGUILayout.FloatField(this.colorVector.z, NodeWindowVector.floatFieldStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(this.VectorFieldWidth)
            });
            GUI.backgroundColor = backgroundColor;
            this.colorVector.w = EditorGUILayout.FloatField(this.colorVector.w, NodeWindowVector.floatFieldStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(this.VectorFieldWidth)
            });
            GUILayout.EndVertical();
        }
    }
}
