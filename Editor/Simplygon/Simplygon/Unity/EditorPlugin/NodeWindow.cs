﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.NodeWindow
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public abstract class NodeWindow
    {
        protected Vector2 interSlotsUiOffset = new Vector2(0.0f, 16f);
        protected Vector2 slotsUiOffset = new Vector2(0.0f, 20f);
        private static int NextWindowId;
        private GUIStyle outputNodeLabelStyle;
        private GUIStyle inputNodeLabelStyle;

        public int NodeRef
        {
            get; set;
        }

        public List<SlotInput> InputSlots
        {
            get; protected set;
        }

        public List<SlotOutput> OutputSlots
        {
            get; protected set;
        }

        public bool IsExitNode
        {
            get; set;
        }

        public bool ShowHelp
        {
            get; set;
        }

        public string HelpText
        {
            get; protected set;
        }

        public Rect Window
        {
            get; set;
        }

        public int WindowId
        {
            get; private set;
        }

        public string Title
        {
            get; protected set;
        }

        public Vector2 MinSize
        {
            get; protected set;
        }

        public Vector2 MaxSize
        {
            get; protected set;
        }

        public bool IsCloseable
        {
            get; protected set;
        }

        public bool IsResizable
        {
            get; protected set;
        }

        public bool IsDraggable
        {
            get; protected set;
        }

        public NodeWindow(int nodeRef)
        {
            this.NodeRef = nodeRef;
            this.InputSlots = new List<SlotInput>();
            this.OutputSlots = new List<SlotOutput>();
            this.IsExitNode = false;
            this.ShowHelp = false;
            Rect currentWindow = SharedData.Instance.NodeEditorWindow.GetCurrentWindow();
            this.MinSize = new Vector2(110f, 120f);
            this.MaxSize = new Vector2(600f, 600f);
            this.Window = new Rect((float)((double)currentWindow.width - (double)this.MinSize.x - 160.0), 90f, this.MinSize.x, this.MinSize.y);
            this.WindowId = ++NodeWindow.NextWindowId;
            this.IsCloseable = true;
            this.IsResizable = false;
            this.IsDraggable = true;
            this.outputNodeLabelStyle = new GUIStyle(EditorStyles.miniLabel);
            this.outputNodeLabelStyle.alignment = TextAnchor.UpperRight;
            this.inputNodeLabelStyle = new GUIStyle(EditorStyles.miniLabel);
            this.inputNodeLabelStyle.alignment = TextAnchor.UpperLeft;
        }

        public void Update()
        {
        }

        public void Close()
        {
            if (!this.IsCloseable)
                return;
            this.InputSlots.ForEach((Action<SlotInput>)(slot => slot.Disconnect()));
            this.OutputSlots.ForEach((Action<SlotOutput>)(slot => slot.Disconnect()));
        }

        public virtual void DrawContents(Rect parent)
        {
            Color backgroundColor = GUI.backgroundColor;
            GUILayout.BeginHorizontal();
            this.DrawFirstInsetContent();
            GUILayout.BeginVertical();
            foreach (Slot inputSlot in this.InputSlots)
            {
                GUILayout.Label(inputSlot.Components.ToString(), this.inputNodeLabelStyle, new GUILayoutOption[0]);
                GUILayout.Space(this.interSlotsUiOffset.y - 18f);
            }
            GUILayout.EndVertical();
            GUILayout.BeginVertical();
            foreach (Slot outputSlot in this.OutputSlots)
            {
                GUILayout.Label(outputSlot.Components.ToString(), this.outputNodeLabelStyle, new GUILayoutOption[0]);
                GUILayout.Space(this.interSlotsUiOffset.y - 18f);
            }
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            int num = this.ShowHelp ? 1 : 0;
        }

        public void SetInputSlotsEnabled(bool enable, params SlotComponents[] slotComponents)
        {
            this.InputSlots.FindAll((Predicate<SlotInput>)(slot => ((IEnumerable<SlotComponents>)slotComponents).Contains<SlotComponents>(slot.Components))).ForEach((Action<SlotInput>)(slot => slot.IsEnabled = enable));
        }

        internal virtual void Save(List<BasicShadingNodeProperties> basicNodePropertiesCollection)
        {
        }

        internal virtual void Save(List<ModifierShadingNodeProperties> modifierNodePropertiesCollection)
        {
        }

        public void SetWindowWithinBounds(Rect parent)
        {
            if ((double)parent.xMin + (double)this.Window.xMax + 160.0 > (double)parent.xMax)
                this.Window = new Rect((float)((double)parent.xMax - (double)parent.xMin - (double)this.Window.width - 160.0), this.Window.yMin, this.Window.width, this.Window.height);
            Rect window;
            if ((double)this.Window.xMin < 20.0)
            {
                double num = 20.0;
                double yMin = (double)this.Window.yMin;
                double width = (double)this.Window.width;
                window = this.Window;
                double height = (double)window.height;
                this.Window = new Rect((float)num, (float)yMin, (float)width, (float)height);
            }
            double yMin1 = (double)parent.yMin;
            window = this.Window;
            double yMax = (double)window.yMax;
            if (yMin1 + yMax + (this.IsExitNode ? 35.0 : 20.0) > (double)parent.yMax)
            {
                window = this.Window;
                double xMin = (double)window.xMin;
                double num1 = (double)parent.yMax - (double)parent.yMin;
                window = this.Window;
                double height1 = (double)window.height;
                double num2 = num1 - height1 - (this.IsExitNode ? 35.0 : 20.0);
                window = this.Window;
                double width = (double)window.width;
                window = this.Window;
                double height2 = (double)window.height;
                this.Window = new Rect((float)xMin, (float)num2, (float)width, (float)height2);
            }
            window = this.Window;
            if ((double)window.yMin >= 90.0)
                return;
            window = this.Window;
            double xMin1 = (double)window.xMin;
            double num3 = 90.0;
            window = this.Window;
            double width1 = (double)window.width;
            window = this.Window;
            double height3 = (double)window.height;
            this.Window = new Rect((float)xMin1, (float)num3, (float)width1, (float)height3);
        }

        protected virtual void DrawFirstInsetContent()
        {
        }

        protected void AddInputSlot(SlotComponents slotComponents)
        {
            this.InputSlots.Add(new SlotInput(this.InputSlots.Count, slotComponents, this, this.interSlotsUiOffset, this.slotsUiOffset));
        }

        protected void AddOutputSlot(SlotComponents slotComponents)
        {
            this.OutputSlots.Add(new SlotOutput(this.OutputSlots.Count, slotComponents, this, this.interSlotsUiOffset, this.slotsUiOffset));
        }

        internal void GenericSave(List<ModifierShadingNodeProperties> modifierShadingNodePropertiesCollection, ModifierShadingNodeType nodeType, int[] inputNodes, int[] nodeData)
        {
            if (this.InputSlots.Count == inputNodes.Length)
            {
                for (int index = 0; index < this.InputSlots.Count; ++index)
                {
                    int connectedNodeRef;
                    SlotComponents connectedNodeOuputSlotComponents;
                    if (this.InputSlots[index].GetConnectedNodeInfo(out connectedNodeRef, out connectedNodeOuputSlotComponents))
                        inputNodes[index] = connectedNodeRef;
                    if (SlotComponents.RGBA != connectedNodeOuputSlotComponents && SlotComponents.RGB != connectedNodeOuputSlotComponents)
                        nodeData[index] = (int)connectedNodeOuputSlotComponents;
                }
                ModifierShadingNodeProperties shadingNodeProperties = new ModifierShadingNodeProperties(this.NodeRef, inputNodes, nodeData, nodeType);
                modifierShadingNodePropertiesCollection.Add(shadingNodeProperties);
            }
            else
                Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected number of input slots ({0}) for node '{1}'.", (object)this.InputSlots.Count, (object)nodeType));
        }
    }
}
