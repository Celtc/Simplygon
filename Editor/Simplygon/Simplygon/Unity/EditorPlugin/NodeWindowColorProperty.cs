﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.NodeWindowColorProperty
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Unity;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class NodeWindowColorProperty : NodeWindowShaderProperty
    {
        public NodeWindowColorProperty(int nodeRef, int selectedShaderPropertyIndex, string[] shaderColorProperties)
          : base(nodeRef, selectedShaderPropertyIndex, shaderColorProperties, Vector2.zero)
        {
            this.Title = "Color Property";
            this.HelpText = "The Color Property node forwards an arbitrary shader color property of the currently selected shader. The property is selectable via the drop-down within the node window.";
        }

        internal override BasicShadingNodeType GetShadingNodeType()
        {
            return BasicShadingNodeType.Color;
        }
    }
}
