﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.NodeWindowShaderProperty
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public abstract class NodeWindowShaderProperty : NodeWindow
    {
        private int selectedShaderPropertyIndex;
        private string[] shaderTextureProperties;

        public string TexCoords
        {
            get; protected set;
        }

        public NodeWindowShaderProperty(int nodeRef, int selectedShaderPropertyIndex, string[] shaderTextureProperties, Vector2 slotsUiOffset)
          : base(nodeRef)
        {
            this.TexCoords = "TexCoords0";
            this.selectedShaderPropertyIndex = selectedShaderPropertyIndex;
            this.shaderTextureProperties = shaderTextureProperties;
            this.slotsUiOffset = Vector2.zero == slotsUiOffset ? new Vector2(0.0f, 40f) : slotsUiOffset;
            List<SlotComponents> list = ((IEnumerable<SlotComponents>)Enum.GetValues(typeof(SlotComponents))).ToList<SlotComponents>();
            list.Remove(SlotComponents.RGBA);
            list.Insert(0, SlotComponents.RGBA);
            list.Remove(SlotComponents.RGB);
            list.Insert(1, SlotComponents.RGB);
            foreach (SlotComponents slotComponents in list)
                this.AddOutputSlot(slotComponents);
        }

        public override void DrawContents(Rect parent)
        {
            Color backgroundColor = GUI.backgroundColor;
            GUI.backgroundColor = (Color)new Color32((byte)60, (byte)160, byte.MaxValue, byte.MaxValue);
            this.selectedShaderPropertyIndex = EditorGUILayout.Popup(this.selectedShaderPropertyIndex, this.shaderTextureProperties);
            GUI.backgroundColor = backgroundColor;
            this.DrawInnerContents();
            base.DrawContents(parent);
            GUILayout.Space(5f);
            this.SetWindowWithinBounds(parent);
        }

        internal override void Save(List<BasicShadingNodeProperties> basicShadingNodePropertiesCollection)
        {
            string shaderTextureProperty = this.shaderTextureProperties[this.selectedShaderPropertyIndex];
            string nodeName = string.Format(CultureInfo.InvariantCulture, "{0}{1}{2}", (object)this.Title.Replace(" ", string.Empty), (object)this.NodeRef, (object)shaderTextureProperty);
            BasicShadingNodeType shadingNodeType = this.GetShadingNodeType();
            BasicShadingNodeProperties shadingNodeProperties = new BasicShadingNodeProperties(shaderTextureProperty, nodeName, this.NodeRef, shadingNodeType, this.TexCoords);
            basicShadingNodePropertiesCollection.Add(shadingNodeProperties);
        }

        protected virtual void DrawInnerContents()
        {
        }

        internal abstract BasicShadingNodeType GetShadingNodeType();
    }
}
