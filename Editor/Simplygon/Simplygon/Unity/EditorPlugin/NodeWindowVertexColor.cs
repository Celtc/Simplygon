﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.NodeWindowVertexColor
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class NodeWindowVertexColor : NodeWindow
    {
        public NodeWindowVertexColor(int nodeRef)
          : base(nodeRef)
        {
            this.Title = "Vertex Color";
            List<SlotComponents> list = ((IEnumerable<SlotComponents>)Enum.GetValues(typeof(SlotComponents))).ToList<SlotComponents>();
            list.Remove(SlotComponents.RGBA);
            list.Insert(0, SlotComponents.RGBA);
            list.Remove(SlotComponents.RGB);
            list.Insert(1, SlotComponents.RGB);
            foreach (SlotComponents slotComponents in list)
                this.AddOutputSlot(slotComponents);
            this.HelpText = "The Vertex Color node forwards all vertex colors (mesh.colors).";
        }

        public override void DrawContents(Rect parent)
        {
            base.DrawContents(parent);
            this.SetWindowWithinBounds(parent);
        }

        internal override void Save(List<BasicShadingNodeProperties> basicShadingNodePropertiesCollection)
        {
            string nodeName = string.Format(CultureInfo.InvariantCulture, "{0}Node{1}", (object)this.Title.Replace(" ", string.Empty), (object)this.NodeRef);
            BasicShadingNodeProperties shadingNodeProperties = new BasicShadingNodeProperties(string.Empty, nodeName, this.NodeRef, BasicShadingNodeType.VertexColor, "TexCoords0");
            basicShadingNodePropertiesCollection.Add(shadingNodeProperties);
        }
    }
}
