﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.ProcessToolbar
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Base;
using Simplygon.SPL.v80.Enum;
using Simplygon.SPL.v80.MaterialCaster;
using Simplygon.SPL.v80.Node;
using Simplygon.SPL.v80.Processor;
using Simplygon.SPL.v80.Settings;
using System;
using System.Globalization;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class ProcessToolbar
    {
        private readonly int MaxLODCount = 9;
        private List<int> texSizeDefaultValues = new List<int>()
    {
      32,
      64,
      128,
      256,
      512,
      1024,
      2048,
      4096,
      8192
    };
        private Vector2 scrollPosition = Vector2.zero;
        private Vector2 freeModeInfoScrollPosition = Vector2.zero;
        private bool showFoldOutAssets = true;
        private bool showFoldOutLODChainSettings = true;
        private bool showFoldOutLODList = true;
        private bool[] showFoldOutLOD = new bool[10];
        private bool[] showFoldOutFeatureQuality = new bool[10];
        private bool[] showFoldOutMeshPreProcessing = new bool[10];
        private bool[] showFoldOutMeshPostProcessing = new bool[10];
        private bool[] showFoldOutProxyPostProcessing = new bool[10];
        private bool[] showFoldOutMaterialLOD = new bool[10];
        private bool[] showFoldOutBoneLOD = new bool[10];
        private bool showHelp = true;
        private readonly GUIContent GUIContentReset = new GUIContent("Reset", string.Empty);
        private readonly GUIContent GUIContentResetWithHelp = new GUIContent("Reset", "Reset Simplygon settings to default");
        private readonly GUIContent GUIContentLoad = new GUIContent("Load", string.Empty);
        private readonly GUIContent GUIContentLoadWithHelp = new GUIContent("Load", "Load Simplygon settings file");
        private readonly GUIContent GUIContentSave = new GUIContent("Save", string.Empty);
        private readonly GUIContent GUIContentSaveWithHelp = new GUIContent("Save", "Save Simplygon settings file");
        private readonly GUIContent GUIContentShowHelp = new GUIContent("Show Help Tooltips", string.Empty);
        private readonly GUIContent GUIContentShowHelpWithHelp = new GUIContent("Show Help Tooltips", "Show Help Tooltips");
        private readonly GUIContent GUIContentCascadedLODChain = new GUIContent("Cascaded LOD Chain", string.Empty);
        private readonly GUIContent GUIContentCascadedLODChainWithHelp = new GUIContent("Cascaded LOD Chain", "If set, LODs are based on previous LODs in the chain and not on the original mesh.");
        private readonly GUIContent GUIContentNumberOfLODs = new GUIContent("#LODs", string.Empty);
        private readonly GUIContent GUIContentNumberOfLODsWithHelp = new GUIContent("#LODs", "The number of automatic LODs to be created from the base model.");
        private readonly GUIContent GUIContentUseCachedAsset = new GUIContent("Use Cached Asset", string.Empty);
        private readonly GUIContent GUIContentUseCachedAssetWithHelp = new GUIContent("Use Cached Asset", "If set, the process and/or upload phase may get automatically skipped if the asset already has been processed using the current settings and/or already has been uploaded. This option may be unchecked to force a re-upload for cases where the hashing algorithm fails to detect any per-asset changes, leading to an undesired processing result as a cached asset is processed instead of the intended asset.");
        private readonly GUIContent GUIContentReductionTargets = new GUIContent("Target Type", string.Empty);
        private readonly GUIContent GUIContentReductionTargetsWithHelp = new GUIContent("Target Type", "Set the reduction metric type. 'On-Screen Size' will reduce the geometry until any reduction will result in an error larger than one pixel when viewed at the specified size on-screen. 'Triangle Percent' will reduce until the number of triangles are reached.");
        private readonly GUIContent GUIContentTriangleRatio = new GUIContent("Number Triangles %", string.Empty);
        private readonly GUIContent GUIContentTriangleRatioWithHelp = new GUIContent("Number Triangles %", "The number of triangles for the MeshLOD, measured as a percentage of the No triangles in the base model. For example if the base model has 10.000 triangles, setting Number Triangle to 50% will generate a MeshLOD with 5.000 triangles.");
        private readonly GUIContent GUIContentOnScreenSize = new GUIContent("On-Screen Size (pixels)", string.Empty);
        private readonly GUIContent GUIContentOnScreenSizeWithHelp = new GUIContent("On-Screen Size (pixels)", "The size, in pixels, of the model when it rendered on the screen, for a given camera angle and distance. The size is measured as the diagonal pixel-size of the bounding box.");
        private readonly GUIContent GUIContentRepositionVertices = new GUIContent("Reposition Vertices", string.Empty);
        private readonly GUIContent GUIContentRepositionVerticesWithHelp = new GUIContent("Reposition Vertices", "If enabled, allow vertices to be repositioned to improve mesh quality.");
        private readonly GUIContent GUIContentRepairInvalidNormals = new GUIContent("Repair Invalid Normals", string.Empty);
        private readonly GUIContent GUIContentRepairInvalidNormalsWithHelp = new GUIContent("Repair Invalid Normals", "If enabled, normals will be repaired.");
        private readonly GUIContent GUIContentReplaceNormals = new GUIContent("Recalculate Normals", string.Empty);
        private readonly GUIContent GUIContentReplaceNormalsWithHelp = new GUIContent("Recalculate Normals", "If enabled, the original normals are discarded and replaced by new normals.");
        private readonly GUIContent GUIContentHardAngleCutoff = new GUIContent("Hard Angle Cutoff", string.Empty);
        private readonly GUIContent GUIContentHardAngleCutoffWithHelp = new GUIContent("Hard Angle Cutoff", "The Hard Angle Cutoff value specifies an angle in degrees at which the normals are considered hard.");
        private readonly GUIContent GUIContentFeatureImportance = new GUIContent("Feature Importance", string.Empty);
        private readonly GUIContent GUIContentFeatureImportanceWithHelp = new GUIContent("Feature Importance", "Settings, used to specify the importance of different features to preserve during mesh LOD creation. Note that lowering each feature preservation quality will lead to higher triangle reduction at a certain LOD switch size, at the cost of poorer LOD quality, and vice versa.");
        private readonly GUIContent GUIContentVertexColorImportance = new GUIContent("Vertex Color", string.Empty);
        private readonly GUIContent GUIContentVertexColorImportanceWithHelp = new GUIContent("Vertex Color", "Settings, used to specify the importance of different features to preserve during mesh LOD creation. Note that lowering each feature preservation quality will lead to higher triangle reduction at a certain LOD switch size, at the cost of poorer LOD quality, and vice versa.");
        private readonly GUIContent GUIContentGeometryImportance = new GUIContent("Silhouette", string.Empty);
        private readonly GUIContent GUIContentGeometryImportanceWithHelp = new GUIContent("Silhouette", "Settings, used to specify the importance of different features to preserve during mesh LOD creation. Note that lowering each feature preservation quality will lead to higher triangle reduction at a certain LOD switch size, at the cost of poorer LOD quality, and vice versa.");
        private readonly GUIContent GUIContentMaterialImportance = new GUIContent("Texture Stretch", string.Empty);
        private readonly GUIContent GUIContentMaterialImportanceWithHelp = new GUIContent("Texture Stretch", "Settings, used to specify the importance of different features to preserve during mesh LOD creation. Note that lowering each feature preservation quality will lead to higher triangle reduction at a certain LOD switch size, at the cost of poorer LOD quality, and vice versa.");
        private readonly GUIContent GUIContentShadingImportance = new GUIContent("Normal", string.Empty);
        private readonly GUIContent GUIContentShadingImportanceWithHelp = new GUIContent("Normal", "Settings, used to specify the importance of different features to preserve during mesh LOD creation. Note that lowering each feature preservation quality will lead to higher triangle reduction at a certain LOD switch size, at the cost of poorer LOD quality, and vice versa.");
        private readonly GUIContent GUIContentSkinningImportance = new GUIContent("Animation", string.Empty);
        private readonly GUIContent GUIContentSkinningImportanceWithHelp = new GUIContent("Animation", "Settings, used to specify the importance of different features to preserve during mesh LOD creation. Note that lowering each feature preservation quality will lead to higher triangle reduction at a certain LOD switch size, at the cost of poorer LOD quality, and vice versa.");
        private readonly GUIContent GUIContentWeldDist = new GUIContent("Welding Threshold", string.Empty);
        private readonly GUIContent GUIContentWeldDistWithHelp = new GUIContent("Welding Threshold", "All the mesh vertices at a distance this threshold will be merged together. Usually this value is set to zero, in order to weld all vertices that happen to be duplicates. In some cases however increasing the value might repair poorly made models.");
        private readonly GUIContent GUIContentMaterialLOD = new GUIContent("MaterialLOD", string.Empty);
        private readonly GUIContent GUIContentMaterialLODWithHelp = new GUIContent("MaterialLOD", "This feature enables the possibility to totally recreate the LOD textures, by making a new UVmap and Rebake new Maps for a mesh LOD. This feature sometimes be useful for lowering the triangle count of a LOD, since recreating UV and maps can be made much more efficiently on a small LOD, while still keeping the LOD quality. The feature can also be useful in cases where the base model has several maps that can be rebaked into one map leading to reduced draw calls. Note however that the cost of material LODs is that you need additional texture space for your Material LOD, and that you need to switch texture as well as mesh when switching to this LOD in runtime.");
        private readonly GUIContent GUIContentMappingImageSettingsWidth = new GUIContent("Width", string.Empty);
        private readonly GUIContent GUIContentMappingImageSettingsWidthWithHelp = new GUIContent("Width", "Sets the width of the textures.");
        private readonly GUIContent GUIContentMappingImageSettingsHeight = new GUIContent("Height", string.Empty);
        private readonly GUIContent GUIContentMappingImageSettingsHeightWithHelp = new GUIContent("Height", "Sets the height of the textures.");
        private readonly GUIContent GUIContentSamplingQuality = new GUIContent("Sampling Quality", string.Empty);
        private readonly GUIContent GUIContentSamplingQualityWithHelp = new GUIContent("Sampling Quality", "Rebaking of new maps is done by sampling the old maps, this setting defines the sampling quality. The higher the quality, the longer the processing time.");
        private readonly GUIContent GUIContentFlipBackfacingNormals = new GUIContent("Flip Backfacing Normals", string.Empty);
        private readonly GUIContent GUIContentFlipBackfacingNormalsWithHelp = new GUIContent("Flip Backfacing Normals", "If set, then normals in the source geometry that point into the triangles of the processed geometry will be flipped to point outwards. This should only be used on geometries with known normal errors, as the result may be inferior on geometries with well-behaved normals.");
        private readonly GUIContent GUIContentGenerateTangentSpaceNormals = new GUIContent("Tangent Space Normals", string.Empty);
        private readonly GUIContent GUIContentGenerateTangentSpaceNormalsWithHelp = new GUIContent("Tangent Space Normals", "If set, generate a normal map with tangent space normals. If not set, the normals in the map will be world-space.");
        private readonly GUIContent GUIContentBoneLODEnabled = new GUIContent("BoneLOD", string.Empty);
        private readonly GUIContent GUIContentBoneLODEnabledWithHelp = new GUIContent("BoneLOD", "This feature enables the possibility to automatically reduce the number of bones accordingly to several input parameters.");
        private readonly GUIContent GUIContentBoneLODDisabled = new GUIContent("BoneLOD is DISABLED", string.Empty);
        private readonly GUIContent GUIContentBoneLODDisabledWithHelp = new GUIContent("BoneLOD is DISABLED", "This feature enables the possibility to automatically reduce the number of bones accordingly to several input parameters.");
        private readonly GUIContent GUIContentBoneReductionTargets = new GUIContent("Target Type", string.Empty);
        private readonly GUIContent GUIContentBoneReductionTargetsWithHelp = new GUIContent("Target Type", "Set the reduction metric type. 'On-Screen Size' will reduce the bones until any reduction will result in an error larger than one pixel when viewed at the specified size on-screen. 'Number of Percent' will reduce until the number of bones are reached.");
        private readonly GUIContent GUIContentBoneRatio = new GUIContent("Bones (%)", string.Empty);
        private readonly GUIContent GUIContentBoneRatioWithHelp = new GUIContent("Bones (%)", "The number of bones for the BoneLOD, measured as a percentage of the No bones in the original model. For example if the base model has 100 bones, setting Number of Bones to 25% will generate a BoneLOD with 25 bones.");
        private readonly List<ProcessToolbar.UIProcessingType> ProcessingTypes = new List<ProcessToolbar.UIProcessingType>()
    {
      new ProcessToolbar.UIProcessingType(new GUIContent("MeshLOD", "MeshLODs are created by removing edges and vertices from the original mesh, while preserving critical features and all relevant original vertex data. Mesh LODs are often used for creating a set of AutoLODs for each prop and character in a game, in order to reduce triangle count."), 0, typeof (ReductionProcessor)),
      new ProcessToolbar.UIProcessingType(new GUIContent("ProxyLOD", "ProxyLODs are made by generating one new mesh (proxy mesh) and one new set of textures (proxy maps). The new mesh and texture resemble a copy of the base model, which looks similar to the base model at the defined LOD switch onscreen size. Proxy LODs are often used to replace groups of static objects with many materials and meshes, at a distance in a game scene. Switching to a Proxy LOD thus significantly reduces both the number of draw calls and triangle counts, specially in large outdoor scenes."), 1, typeof (RemeshingProcessor)),
      new ProcessToolbar.UIProcessingType(new GUIContent("AggregateLOD", ""), 2, typeof (AggregationProcessor))
    };
        private int[] texSize_Values;
        private GUIContent[] validSamplingQualities;
        private List<string> validSamplingQualitiesList;
        private GUIContent[] TexSize_Names;
        private bool showAdvancedSettings;

        public ProcessToolbar()
        {
            SharedData.Instance.PropertyChanged += (PropertyChangedEventHandler)((s, e) =>
           {
               if (!("Account" == e.PropertyName))
                   return;
               this.UpdateRestricitions();
           });
            this.showHelp = SharedData.Instance.Settings.GetShowHelp();
            this.showFoldOutLOD[0] = true;
            this.UpdateRestricitions();
            this.ResetSettings();
            this.showFoldOutAssets = true;
            this.showFoldOutLODChainSettings = true;
            this.showFoldOutLODList = true;
        }

        public void UpdateRestricitions()
        {
            int maxOutputTextureSize = -1;
            if (SharedData.Instance.Account != null)
                maxOutputTextureSize = SharedData.Instance.Account.ProcessSubscriptionRestrictions.MaterialLOD.MaxOutputTextureSize;
            this.texSize_Values = -1 == maxOutputTextureSize ? this.texSizeDefaultValues.ToArray() : this.texSizeDefaultValues.FindAll((Predicate<int>)(value => value <= maxOutputTextureSize)).ToArray();
            this.TexSize_Names = new GUIContent[this.texSize_Values.Length];
            for (int index = 0; index < this.texSize_Values.Length; ++index)
                this.TexSize_Names[index] = new GUIContent(this.texSize_Values[index].ToString());
        }

        public void ResetSettings()
        {
            SharedData.Instance.LODChain.Clear();
            SharedData.Instance.LODChain.Add(SettingsUtility.GetDefaultProcessNode(typeof(ReductionProcessor)));
            for (int index = 0; index < 10; ++index)
            {
                this.showFoldOutLOD[index] = true;
                this.showFoldOutFeatureQuality[index] = false;
                this.showFoldOutMeshPreProcessing[index] = false;
                this.showFoldOutMeshPostProcessing[index] = false;
                this.showFoldOutProxyPostProcessing[index] = false;
                this.showFoldOutMaterialLOD[index] = false;
                this.showFoldOutBoneLOD[index] = false;
            }
        }

        public void Update()
        {
        }

        public void OnInspectorUpdate()
        {
        }

        public void Show()
        {
            this.ManageSettingTemplatesGUI();
            UIUtility.SeparatorGUI();
            this.ShadingNetworkGUI();
            UIUtility.SeparatorGUI();
            this.scrollPosition = EditorGUILayout.BeginScrollView(this.scrollPosition);
            SharedData.Instance.UseCachedAsset = EditorGUILayout.Toggle(this.showHelp ? this.GUIContentUseCachedAssetWithHelp : this.GUIContentUseCachedAsset, SharedData.Instance.UseCachedAsset, new GUILayoutOption[0]);
            GUILayout.Space(4f);
            UIUtility.TargetAssetsGUI(ref this.showFoldOutAssets);
            UIUtility.SeparatorGUI();
            if (this.showFoldOutLODChainSettings = EditorGUILayout.Foldout(this.showFoldOutLODChainSettings, "LOD Chain Settings", SharedData.Instance.FoldOutStyle))
            {
                ++EditorGUI.indentLevel;
                bool flag = EditorGUILayout.Toggle(this.showHelp ? this.GUIContentShowHelpWithHelp : this.GUIContentShowHelp, this.showHelp, new GUILayoutOption[0]);
                if (flag != this.showHelp)
                {
                    this.showHelp = flag;
                    SharedData.Instance.Settings.SetShowHelp(this.showHelp);
                }
                SharedData.Instance.Settings.SetCascadedLOD(EditorGUILayout.Toggle(this.showHelp ? this.GUIContentCascadedLODChainWithHelp : this.GUIContentCascadedLODChain, SharedData.Instance.Settings.GetCascadedLOD(), new GUILayoutOption[0]));
                SettingsUtility.HandleNumberOfSelectedLODs(EditorGUILayout.IntSlider(this.showHelp ? this.GUIContentNumberOfLODsWithHelp : this.GUIContentNumberOfLODs, SharedData.Instance.LODChain.Count, 1, this.MaxLODCount, new GUILayoutOption[0]));
                --EditorGUI.indentLevel;
            }
            UIUtility.SeparatorGUI();
            if (this.showFoldOutLODList = EditorGUILayout.Foldout(this.showFoldOutLODList, "LOD Chain", SharedData.Instance.FoldOutStyle))
            {
                ++EditorGUI.indentLevel;
                int count = SharedData.Instance.LODChain.Count;
                for (int index = 0; index < count; ++index)
                {
                    int lodIndex = index;
                    if (this.showFoldOutLOD[lodIndex] = EditorGUILayout.Foldout(this.showFoldOutLOD[lodIndex], string.Format(CultureInfo.InvariantCulture, "LOD #{0}", (object)(lodIndex + 1)), SharedData.Instance.FoldOutStyle))
                    {
                        ++EditorGUI.indentLevel;
                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Space((float)(EditorGUI.indentLevel * 15));
                        ProcessNode lodProcessNode = SharedData.Instance.LODChain[lodIndex];
                        int selectedToolbarIndex = GUILayout.Toolbar(this.ProcessingTypes.First<ProcessToolbar.UIProcessingType>((Func<ProcessToolbar.UIProcessingType, bool>)(entry => entry.ProcessorType == lodProcessNode.Processor.GetType())).UIIndex, this.ProcessingTypes.Select<ProcessToolbar.UIProcessingType, GUIContent>((Func<ProcessToolbar.UIProcessingType, GUIContent>)(entry => entry.GUIContent)).ToArray<GUIContent>());
                        System.Type processorType = this.ProcessingTypes.First<ProcessToolbar.UIProcessingType>((Func<ProcessToolbar.UIProcessingType, bool>)(entry => entry.UIIndex == selectedToolbarIndex)).ProcessorType;
                        EditorGUILayout.EndHorizontal();
                        if (processorType != lodProcessNode.Processor.GetType())
                            SharedData.Instance.LODChain[lodIndex] = SettingsUtility.GetDefaultProcessNode(processorType);
                        this.ProcessNodeGUI(SharedData.Instance.LODChain[lodIndex], lodIndex);
                        --EditorGUI.indentLevel;
                    }
                    GUILayout.Box("", new GUILayoutOption[2]
                    {
            GUILayout.ExpandWidth(true),
            GUILayout.Height(1f)
                    });
                }
                --EditorGUI.indentLevel;
            }
            EditorGUILayout.EndScrollView();
            UIUtility.SeparatorGUI();
            UIUtility.ProcessGUI();
        }

        private void ManageSettingTemplatesGUI()
        {
            EditorGUILayout.LabelField("Settings Templates", EditorStyles.boldLabel, new GUILayoutOption[0]);
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button(this.showHelp ? this.GUIContentResetWithHelp : this.GUIContentReset))
                this.ResetSettings();
            if (GUILayout.Button(this.showHelp ? this.GUIContentLoadWithHelp : this.GUIContentLoad))
            {
                string filename = EditorUtility.OpenFilePanel("Select settings file", "Assets/LODs", "spl");
                if (!string.IsNullOrEmpty(filename))
                {
                    try
                    {
                        this.ResetSettings();
                        SettingsUtility.LoadSPLSettings(filename);
                    }
                    catch (Exception ex)
                    {
                        Debug.Log((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to load settings file {0}", (object)filename));
                    }
                }
            }
            if (GUILayout.Button(this.showHelp ? this.GUIContentSaveWithHelp : this.GUIContentSave))
            {
                string filename = EditorUtility.SaveFilePanel("Select settings file", "Assets/LODs", "SimplygonSettings.spl", "spl");
                if (!string.IsNullOrEmpty(filename))
                    SettingsUtility.CreateSPLFromCurrentNodes(SharedData.Instance.Settings.GetCascadedLOD()).Save(filename);
            }
            EditorGUILayout.EndHorizontal();
        }

        private void ShadingNetworkGUI()
        {
            EditorGUILayout.LabelField("Shading Network", EditorStyles.boldLabel, new GUILayoutOption[0]);
            if (!GUILayout.Button("Edit Shading Network..."))
                return;
            NodeEditorWindow.Init();
        }

        private void ProcessNodeGUI(ProcessNode processNode, int lodIndex)
        {
            if (processNode.Processor is ReductionProcessor)
            {
                ReductionSettings reductionSettings = ((ReductionProcessor)processNode.Processor).ReductionSettings;
                RepairSettings repairSettings = ((ReductionProcessor)processNode.Processor).RepairSettings;
                NormalCalculationSettings calculationSettings = ((ReductionProcessor)processNode.Processor).NormalCalculationSettings;
                BoneSettings boneSettings = ((ReductionProcessor)processNode.Processor).BoneSettings;
                this.MeshLODGUI(reductionSettings, repairSettings, calculationSettings, boneSettings, lodIndex);
                EditorGUILayout.BeginHorizontal();
                GUILayout.Space((float)(EditorGUI.indentLevel * 15));
                GUILayout.Box("", new GUILayoutOption[2]
                {
          GUILayout.ExpandWidth(true),
          GUILayout.Height(1f)
                });
                EditorGUILayout.EndHorizontal();
                this.MaterialLODGUI(processNode, ((ReductionProcessor)processNode.Processor).MappingImageSettings, lodIndex);
                double triangleRatio = reductionSettings.TriangleRatio;
                int onScreenSize = reductionSettings.OnScreenSize;
                BoneReductionTargets boneReductionTargets = reductionSettings.ReductionTargets == ReductionTargets.SG_REDUCTIONTARGET_TRIANGLERATIO ? BoneReductionTargets.SG_BONEREDUCTIONTARGET_BONERATIO : BoneReductionTargets.SG_BONEREDUCTIONTARGET_ONSCREENSIZE;
                int maxOnScreenSize = SharedData.Instance.Account.ProcessSubscriptionRestrictions.MeshLOD.MaxOnScreenSize;
                this.BoneLODGUI(boneSettings, triangleRatio, onScreenSize, boneReductionTargets, maxOnScreenSize, lodIndex);
            }
            else if (processNode.Processor is RemeshingProcessor)
            {
                RemeshingSettings remeshingSettings = ((RemeshingProcessor)processNode.Processor).RemeshingSettings;
                this.ProxyLODGUI(remeshingSettings, lodIndex);
                EditorGUILayout.BeginHorizontal();
                GUILayout.Space((float)(EditorGUI.indentLevel * 15));
                GUILayout.Box("", new GUILayoutOption[2]
                {
          GUILayout.ExpandWidth(true),
          GUILayout.Height(1f)
                });
                EditorGUILayout.EndHorizontal();
                this.MaterialLODGUI(processNode, ((RemeshingProcessor)processNode.Processor).MappingImageSettings, lodIndex);
                double numberOfTrianges = 0.0;
                int onScreenSize = remeshingSettings.OnScreenSize;
                BoneReductionTargets boneReductionTargets = BoneReductionTargets.SG_BONEREDUCTIONTARGET_ONSCREENSIZE;
                int maxOnScreenSize = SharedData.Instance.Account.ProcessSubscriptionRestrictions.ProxyLOD.MaxOnScreenSize;
                this.BoneLODGUI(((RemeshingProcessor)processNode.Processor).BoneSettings, numberOfTrianges, onScreenSize, boneReductionTargets, maxOnScreenSize, lodIndex);
            }
            else
            {
                if (!(processNode.Processor is AggregationProcessor))
                    return;
                this.SceneAggregatorLODGUI(((AggregationProcessor)processNode.Processor).AggregationSettings, lodIndex);
                EditorGUILayout.BeginHorizontal();
                GUILayout.Space((float)(EditorGUI.indentLevel * 15));
                GUILayout.Box("", new GUILayoutOption[2]
                {
          GUILayout.ExpandWidth(true),
          GUILayout.Height(1f)
                });
                EditorGUILayout.EndHorizontal();
                this.MaterialLODGUI(processNode, ((AggregationProcessor)processNode.Processor).MappingImageSettings, lodIndex);
            }
        }

        private void MeshLODGUI(ReductionSettings reductionSettings, RepairSettings repairSettings, NormalCalculationSettings normalCalculationSettings, BoneSettings boneSettings, int lodIndex)
        {
            if (SharedData.Instance.Account.ProcessSubscriptionRestrictions.MeshLOD.Enabled)
            {
                if (!Enum.IsDefined(typeof(SettingsUtility.UnityReductionTargets), (SettingsUtility.UnityReductionTargets)reductionSettings.ReductionTargets))
                    reductionSettings.ReductionTargets = ReductionTargets.SG_REDUCTIONTARGET_TRIANGLERATIO;

                ReductionTargets reductionTargets1 = reductionSettings.ReductionTargets;
                var reductionTargets2 = (SettingsUtility.UnityReductionTargets)reductionSettings.ReductionTargets;
                reductionSettings.ReductionTargets = (ReductionTargets)EditorGUILayout.EnumPopup(this.showHelp ? this.GUIContentReductionTargetsWithHelp : this.GUIContentReductionTargets, (System.Enum)reductionTargets2, new GUILayoutOption[0]);
                if (reductionSettings.ReductionTargets == ReductionTargets.SG_REDUCTIONTARGET_TRIANGLERATIO)
                    reductionSettings.TriangleRatio = (double)EditorGUILayout.IntSlider(this.showHelp ? this.GUIContentTriangleRatioWithHelp : this.GUIContentTriangleRatio, (int)(reductionSettings.TriangleRatio * 100.0), 0, 100, new GUILayoutOption[0]) * 0.01;
                else if (reductionSettings.ReductionTargets == ReductionTargets.SG_REDUCTIONTARGET_ONSCREENSIZE)
                {
                    int num = reductionSettings.OnScreenSize;
                    int leftValue = 20;
                    int maxOnScreenSize = SharedData.Instance.Account.ProcessSubscriptionRestrictions.MeshLOD.MaxOnScreenSize;
                    if (num > maxOnScreenSize)
                        num = maxOnScreenSize;
                    reductionSettings.OnScreenSize = EditorGUILayout.IntSlider(this.showHelp ? this.GUIContentOnScreenSizeWithHelp : this.GUIContentOnScreenSize, num, leftValue, maxOnScreenSize, new GUILayoutOption[0]);
                }
                if (reductionTargets1 != reductionSettings.ReductionTargets)
                {
                    boneSettings.BoneReductionTargets = reductionSettings.ReductionTargets == ReductionTargets.SG_REDUCTIONTARGET_TRIANGLERATIO ? BoneReductionTargets.SG_BONEREDUCTIONTARGET_BONERATIO : BoneReductionTargets.SG_BONEREDUCTIONTARGET_ONSCREENSIZE;
                    boneSettings.BoneRatio = reductionSettings.TriangleRatio;
                    boneSettings.OnScreenSize = reductionSettings.OnScreenSize;
                }
                bool flag1 = EditorGUILayout.Toggle(this.showHelp ? this.GUIContentRepositionVerticesWithHelp : this.GUIContentRepositionVertices, DataCreationPreferences.SG_DATACREATIONPREFERENCES_PREFER_OPTIMIZED_RESULT == reductionSettings.DataCreationPreferences, new GUILayoutOption[0]);
                reductionSettings.DataCreationPreferences = flag1 ? DataCreationPreferences.SG_DATACREATIONPREFERENCES_PREFER_OPTIMIZED_RESULT : DataCreationPreferences.SG_DATACREATIONPREFERENCES_ONLY_USE_ORIGINAL_DATA;
                if (this.showFoldOutMeshPostProcessing[lodIndex] = EditorGUILayout.Foldout(this.showFoldOutMeshPostProcessing[lodIndex], "Post Processing", SharedData.Instance.FoldOutStyle))
                {
                    ++EditorGUI.indentLevel;
                    normalCalculationSettings.RepairInvalidNormals = EditorGUILayout.Toggle(this.showHelp ? this.GUIContentRepairInvalidNormalsWithHelp : this.GUIContentRepairInvalidNormals, normalCalculationSettings.RepairInvalidNormals, new GUILayoutOption[0]);
                    normalCalculationSettings.ReplaceNormals = EditorGUILayout.Toggle(this.showHelp ? this.GUIContentReplaceNormalsWithHelp : this.GUIContentReplaceNormals, normalCalculationSettings.ReplaceNormals, new GUILayoutOption[0]);
                    if (normalCalculationSettings.ReplaceNormals)
                    {
                        double num = Mathf.PI / 180.0 * (double)EditorGUILayout.IntSlider(this.showHelp ? this.GUIContentHardAngleCutoffWithHelp : this.GUIContentHardAngleCutoff, Mathf.RoundToInt((float)(57.2957763671875 * normalCalculationSettings.HardEdgeAngleInRadians)), 0, 180, new GUILayoutOption[0]);
                        normalCalculationSettings.HardEdgeAngleInRadians = num;
                    }
                    normalCalculationSettings.Enabled = normalCalculationSettings.RepairInvalidNormals || normalCalculationSettings.ReplaceNormals;
                    --EditorGUI.indentLevel;
                }
                if (!this.showAdvancedSettings)
                    return;
                bool[] outFeatureQuality = this.showFoldOutFeatureQuality;
                int index = lodIndex;
                int num1 = this.showFoldOutFeatureQuality[lodIndex] ? 1 : 0;
                GUIContent content = this.showHelp ? this.GUIContentFeatureImportanceWithHelp : this.GUIContentFeatureImportance;
                GUIStyle foldOutStyle = SharedData.Instance.FoldOutStyle;
                int num2;
                bool flag2 = (num2 = EditorGUILayout.Foldout(num1 != 0, content, foldOutStyle) ? 1 : 0) != 0;
                outFeatureQuality[index] = num2 != 0;
                if (flag2)
                {
                    ++EditorGUI.indentLevel;
                    SettingsUtility.UnityQualityMetric qualityMetric1 = (SettingsUtility.UnityQualityMetric)EditorGUILayout.EnumPopup(this.showHelp ? this.GUIContentVertexColorImportanceWithHelp : this.GUIContentVertexColorImportance, (System.Enum)SettingsUtility.GetQualityMetric(reductionSettings.VertexColorImportance), new GUILayoutOption[0]);
                    reductionSettings.VertexColorImportance = SettingsUtility.GetQuality(qualityMetric1);
                    SettingsUtility.UnityQualityMetric qualityMetric2 = (SettingsUtility.UnityQualityMetric)EditorGUILayout.EnumPopup(this.showHelp ? this.GUIContentGeometryImportanceWithHelp : this.GUIContentGeometryImportance, (System.Enum)SettingsUtility.GetQualityMetric(reductionSettings.GeometryImportance), new GUILayoutOption[0]);
                    reductionSettings.GeometryImportance = SettingsUtility.GetQuality(qualityMetric2);
                    SettingsUtility.UnityQualityMetric qualityMetric3 = (SettingsUtility.UnityQualityMetric)EditorGUILayout.EnumPopup(this.showHelp ? this.GUIContentMaterialImportanceWithHelp : this.GUIContentMaterialImportance, (System.Enum)SettingsUtility.GetQualityMetric(reductionSettings.MaterialImportance), new GUILayoutOption[0]);
                    reductionSettings.MaterialImportance = SettingsUtility.GetQuality(qualityMetric3);
                    SettingsUtility.UnityQualityMetric qualityMetric4 = (SettingsUtility.UnityQualityMetric)EditorGUILayout.EnumPopup(this.showHelp ? this.GUIContentShadingImportanceWithHelp : this.GUIContentShadingImportance, (System.Enum)SettingsUtility.GetQualityMetric(reductionSettings.ShadingImportance), new GUILayoutOption[0]);
                    reductionSettings.ShadingImportance = SettingsUtility.GetQuality(qualityMetric4);
                    SettingsUtility.UnityQualityMetric qualityMetric5 = (SettingsUtility.UnityQualityMetric)EditorGUILayout.EnumPopup(this.showHelp ? this.GUIContentSkinningImportanceWithHelp : this.GUIContentSkinningImportance, (System.Enum)SettingsUtility.GetQualityMetric(reductionSettings.SkinningImportance), new GUILayoutOption[0]);
                    reductionSettings.SkinningImportance = SettingsUtility.GetQuality(qualityMetric5);
                    --EditorGUI.indentLevel;
                }
                if (!(this.showFoldOutMeshPreProcessing[lodIndex] = EditorGUILayout.Foldout(this.showFoldOutMeshPreProcessing[lodIndex], "Pre Processing", SharedData.Instance.FoldOutStyle)))
                    return;
                ++EditorGUI.indentLevel;
                repairSettings.WeldDist = (double)EditorGUILayout.FloatField(this.showHelp ? this.GUIContentWeldDistWithHelp : this.GUIContentWeldDist, (float)repairSettings.WeldDist, new GUILayoutOption[0]);
                --EditorGUI.indentLevel;
            }
            else
                EditorGUILayout.LabelField("MeshLOD is DISABLED");
        }

        private void ProxyLODGUI(RemeshingSettings remeshingSettings, int lodIndex)
        {
            if (SharedData.Instance.Account.ProcessSubscriptionRestrictions.ProxyLOD.Enabled)
            {
                int leftValue = 20;
                int maxOnScreenSize = SharedData.Instance.Account.ProcessSubscriptionRestrictions.ProxyLOD.MaxOnScreenSize;
                int num1 = remeshingSettings.OnScreenSize;
                if (num1 > maxOnScreenSize)
                    num1 = maxOnScreenSize;
                remeshingSettings.OnScreenSize = EditorGUILayout.IntSlider(this.showHelp ? this.GUIContentOnScreenSizeWithHelp : this.GUIContentOnScreenSize, num1, leftValue, maxOnScreenSize, new GUILayoutOption[0]);
                if (!(this.showFoldOutProxyPostProcessing[lodIndex] = EditorGUILayout.Foldout(this.showFoldOutProxyPostProcessing[lodIndex], "Post Processing", SharedData.Instance.FoldOutStyle)))
                    return;
                ++EditorGUI.indentLevel;
                double num2 = Mathf.PI / 180.0 * (double)EditorGUILayout.IntSlider(this.showHelp ? this.GUIContentHardAngleCutoffWithHelp : this.GUIContentHardAngleCutoff, Mathf.RoundToInt((float)(57.2957763671875 * remeshingSettings.HardEdgeAngleInRadians)), 0, 180, new GUILayoutOption[0]);
                remeshingSettings.HardEdgeAngleInRadians = num2;
                --EditorGUI.indentLevel;
            }
            else
                EditorGUILayout.LabelField("ProxyLOD is DISABLED");
        }

        private void SceneAggregatorLODGUI(AggregationSettings sceneAggregatorSettings, int lodIndex)
        {
            if (SharedData.Instance.Account.ProcessSubscriptionRestrictions.AggregateLOD.Enabled)
                return;
            EditorGUILayout.LabelField("AggregateLOD is DISABLED");
        }

        private void MaterialLODGUI(ProcessNode processNode, MappingImageSettings mappingImageSettings, int lodIndex)
        {
            bool flag = processNode.Processor is AggregationProcessor;
            bool enabled1 = SharedData.Instance.Account.ProcessSubscriptionRestrictions.MaterialLOD.Enabled;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginHorizontal(GUILayout.Width(100f));
            this.showFoldOutMaterialLOD[lodIndex] = EditorGUILayout.Foldout(flag || this.showFoldOutMaterialLOD[lodIndex], this.showHelp ? this.GUIContentMaterialLODWithHelp : this.GUIContentMaterialLOD, SharedData.Instance.FoldOutStyle);
            EditorGUILayout.EndHorizontal();
            if (enabled1)
            {
                bool enabled2 = mappingImageSettings.Enabled;
                if (processNode.Processor is ReductionProcessor)
                {
                    ((ReductionProcessor)processNode.Processor).MappingImageSettings.Enabled = EditorGUILayout.Toggle(enabled2);
                    ((ReductionProcessor)processNode.Processor).MappingImageSettings.TexCoordGeneratorType = TexCoordGeneratorType.SG_TEXCOORDGENERATORTYPE_CHARTAGGREGATOR;
                    ((ReductionProcessor)processNode.Processor).MappingImageSettings.ChartAggregatorMode = ChartAggregatorMode.SG_CHARTAGGREGATORMODE_TEXTURESIZEPROPORTIONS;
                    ((ReductionProcessor)processNode.Processor).MappingImageSettings.UseFullRetexturing = false;
                }
                else if (processNode.Processor is RemeshingProcessor)
                {
                    ((RemeshingProcessor)processNode.Processor).MappingImageSettings.Enabled = EditorGUILayout.Toggle(enabled2);
                    ((RemeshingProcessor)processNode.Processor).MappingImageSettings.TexCoordGeneratorType = TexCoordGeneratorType.SG_TEXCOORDGENERATORTYPE_PARAMETERIZER;
                    ((RemeshingProcessor)processNode.Processor).MappingImageSettings.UseFullRetexturing = true;
                }
                else if (processNode.Processor is AggregationProcessor)
                {
                    ((AggregationProcessor)processNode.Processor).MappingImageSettings.Enabled = true;
                    ((AggregationProcessor)processNode.Processor).MappingImageSettings.TexCoordGeneratorType = TexCoordGeneratorType.SG_TEXCOORDGENERATORTYPE_CHARTAGGREGATOR;
                    ((AggregationProcessor)processNode.Processor).MappingImageSettings.ChartAggregatorMode = ChartAggregatorMode.SG_CHARTAGGREGATORMODE_TEXTURESIZEPROPORTIONS;
                    ((AggregationProcessor)processNode.Processor).MappingImageSettings.UseFullRetexturing = false;
                }
                EditorGUILayout.EndHorizontal();
                if (!this.showFoldOutMaterialLOD[lodIndex])
                    return;
                ++EditorGUI.indentLevel;
                int num = mappingImageSettings.Width > mappingImageSettings.Height ? mappingImageSettings.Width : mappingImageSettings.Height;
                mappingImageSettings.Width = EditorGUILayout.IntPopup(this.showHelp ? this.GUIContentMappingImageSettingsWidthWithHelp : this.GUIContentMappingImageSettingsWidth, mappingImageSettings.Width, this.TexSize_Names, this.texSize_Values, new GUILayoutOption[0]);
                mappingImageSettings.Height = EditorGUILayout.IntPopup(this.showHelp ? this.GUIContentMappingImageSettingsHeightWithHelp : this.GUIContentMappingImageSettingsHeight, mappingImageSettings.Height, this.TexSize_Names, this.texSize_Values, new GUILayoutOption[0]);
                int textureSize = mappingImageSettings.Width > mappingImageSettings.Height ? mappingImageSettings.Width : mappingImageSettings.Height;
                if (this.validSamplingQualities == null || num != textureSize)
                {
                    this.validSamplingQualitiesList = ((IEnumerable<string>)this.GetValidSamplingQualities(textureSize)).ToList<string>();
                    this.validSamplingQualities = new GUIContent[this.validSamplingQualitiesList.Count];
                    for (int index = 0; index < this.validSamplingQualitiesList.Count; ++index)
                        this.validSamplingQualities[index] = new GUIContent(this.validSamplingQualitiesList[index]);
                }
                if (!System.Enum.IsDefined(typeof(SettingsUtility.UnityMappingImageSettingsSuperSampling), (object)mappingImageSettings.MultisamplingLevel))
                    mappingImageSettings.MultisamplingLevel = 3;
                int selectedIndex = this.validSamplingQualitiesList.IndexOf(((SettingsUtility.UnityMappingImageSettingsSuperSampling)mappingImageSettings.MultisamplingLevel).ToString());
                if (-1 == selectedIndex)
                    selectedIndex = 0;
                int index1 = EditorGUILayout.Popup(this.showHelp ? this.GUIContentSamplingQualityWithHelp : this.GUIContentSamplingQuality, selectedIndex, this.validSamplingQualities, new GUILayoutOption[0]);
                mappingImageSettings.MultisamplingLevel = (int)System.Enum.Parse(typeof(SettingsUtility.UnityMappingImageSettingsSuperSampling), this.validSamplingQualitiesList[index1]);
                if (this.showAdvancedSettings)
                {
                    NormalCaster normalCaster = processNode.MaterialCaster.Find((Predicate<BaseMaterialCaster>)(caster => caster is NormalCaster)) as NormalCaster;
                    if (normalCaster != null)
                    {
                        normalCaster.FlipBackfacingNormals = EditorGUILayout.Toggle(this.showHelp ? this.GUIContentFlipBackfacingNormalsWithHelp : this.GUIContentFlipBackfacingNormals, normalCaster.FlipBackfacingNormals, new GUILayoutOption[0]);
                        normalCaster.GenerateTangentSpaceNormals = EditorGUILayout.Toggle(this.showHelp ? this.GUIContentGenerateTangentSpaceNormalsWithHelp : this.GUIContentGenerateTangentSpaceNormals, normalCaster.GenerateTangentSpaceNormals, new GUILayoutOption[0]);
                    }
                }
                --EditorGUI.indentLevel;
            }
            else
            {
                EditorGUILayout.EndHorizontal();
                if (this.showFoldOutMaterialLOD[lodIndex])
                {
                    ++EditorGUI.indentLevel;
                    string empty = string.Empty;
                    EditorGUILayout.LabelField("MaterialLOD is disabled due to the current processing type being disabled.");
                    --EditorGUI.indentLevel;
                }
                mappingImageSettings.Enabled = false;
            }
        }

        private void BoneLODGUI(BoneSettings boneSettings, double numberOfTrianges, int onScreenSize, BoneReductionTargets boneReductionTargets, int maxOnScreenSize, int lodIndex)
        {
            bool enabled1 = SharedData.Instance.Account.ProcessSubscriptionRestrictions.BoneLOD.Enabled;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginHorizontal(GUILayout.Width(100f));
            GUIContent contentBoneLodEnabled = this.GUIContentBoneLODEnabled;
            GUIContent content = !enabled1 ? (this.showHelp ? this.GUIContentBoneLODDisabledWithHelp : this.GUIContentBoneLODDisabled) : (this.showHelp ? this.GUIContentBoneLODEnabledWithHelp : this.GUIContentBoneLODEnabled);
            this.showFoldOutBoneLOD[lodIndex] = EditorGUILayout.Foldout(this.showFoldOutBoneLOD[lodIndex], content, SharedData.Instance.FoldOutStyle);
            EditorGUILayout.EndHorizontal();
            if (enabled1)
            {
                bool enabled2 = boneSettings.Enabled;
                boneSettings.Enabled = EditorGUILayout.Toggle(boneSettings.Enabled);
                boneSettings.UseBoneReducer = boneSettings.Enabled;
                if (boneSettings.Enabled && enabled2 != boneSettings.Enabled)
                {
                    boneSettings.BoneRatio = numberOfTrianges;
                    boneSettings.OnScreenSize = onScreenSize;
                    boneSettings.BoneReductionTargets = boneReductionTargets;
                }
                EditorGUILayout.EndHorizontal();
                if (!this.showFoldOutBoneLOD[lodIndex])
                    return;
                ++EditorGUI.indentLevel;
                if (!System.Enum.IsDefined(typeof(SettingsUtility.UnityBoneReductionTargets), (SettingsUtility.UnityBoneReductionTargets)boneSettings.BoneReductionTargets))
                    boneSettings.BoneReductionTargets = BoneReductionTargets.SG_BONEREDUCTIONTARGET_BONERATIO;
                SettingsUtility.UnityBoneReductionTargets reductionTargets = (SettingsUtility.UnityBoneReductionTargets)boneSettings.BoneReductionTargets;
                boneSettings.BoneReductionTargets = (BoneReductionTargets)EditorGUILayout.EnumPopup(this.showHelp ? this.GUIContentBoneReductionTargetsWithHelp : this.GUIContentBoneReductionTargets, (System.Enum)reductionTargets, new GUILayoutOption[0]);
                if (boneSettings.BoneReductionTargets == BoneReductionTargets.SG_BONEREDUCTIONTARGET_BONERATIO)
                    boneSettings.BoneRatio = (double)EditorGUILayout.IntSlider(this.showHelp ? this.GUIContentBoneRatioWithHelp : this.GUIContentBoneRatio, (int)(boneSettings.BoneRatio * 100.0), 1, 100, new GUILayoutOption[0]) * 0.01;
                else
                    boneSettings.OnScreenSize = EditorGUILayout.IntSlider(this.showHelp ? this.GUIContentOnScreenSizeWithHelp : this.GUIContentOnScreenSize, boneSettings.OnScreenSize, 20, maxOnScreenSize, new GUILayoutOption[0]);
                --EditorGUI.indentLevel;
            }
            else
            {
                EditorGUILayout.EndHorizontal();
                if (this.showFoldOutBoneLOD[lodIndex])
                {
                    ++EditorGUI.indentLevel;
                    string empty = string.Empty;
                    EditorGUILayout.LabelField("BoneLOD is disabled due to the current processing type being disabled.");
                    --EditorGUI.indentLevel;
                }
                boneSettings.Enabled = false;
            }
        }

        private string[] GetValidSamplingQualities(int textureSize)
        {
            string[] strArray1;
            switch (textureSize)
            {
                case 32:
                case 64:
                case 128:
                case 256:
                case 512:
                case 1024:
                case 2048:
                    string[] strArray2 = new string[4]
                    {
            SettingsUtility.UnityMappingImageSettingsSuperSampling.Lowest.ToString(),
            null,
            null,
            null
                    };
                    int index1 = 1;
                    SettingsUtility.UnityMappingImageSettingsSuperSampling settingsSuperSampling = SettingsUtility.UnityMappingImageSettingsSuperSampling.Low;
                    string str1 = settingsSuperSampling.ToString();
                    strArray2[index1] = str1;
                    int index2 = 2;
                    settingsSuperSampling = SettingsUtility.UnityMappingImageSettingsSuperSampling.Normal;
                    string str2 = settingsSuperSampling.ToString();
                    strArray2[index2] = str2;
                    int index3 = 3;
                    settingsSuperSampling = SettingsUtility.UnityMappingImageSettingsSuperSampling.High;
                    string str3 = settingsSuperSampling.ToString();
                    strArray2[index3] = str3;
                    strArray1 = strArray2;
                    break;
                case 4096:
                    strArray1 = new string[2]
                    {
            SettingsUtility.UnityMappingImageSettingsSuperSampling.Lowest.ToString(),
            SettingsUtility.UnityMappingImageSettingsSuperSampling.Low.ToString()
                    };
                    break;
                case 8192:
                    strArray1 = new string[1]
                    {
            SettingsUtility.UnityMappingImageSettingsSuperSampling.Lowest.ToString()
                    };
                    break;
                default:
                    strArray1 = new string[1]
                    {
            SettingsUtility.UnityMappingImageSettingsSuperSampling.Lowest.ToString()
                    };
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected texture size '{0}' when retrieving valid sampling qualities.", (object)textureSize));
                    break;
            }
            return strArray1;
        }

        private class UIProcessingType
        {
            public GUIContent GUIContent
            {
                get; protected set;
            }

            public int UIIndex
            {
                get; protected set;
            }

            public System.Type ProcessorType
            {
                get; protected set;
            }

            public UIProcessingType(GUIContent guiContent, int uiIndex, System.Type processorType)
            {
                this.GUIContent = guiContent;
                this.UIIndex = uiIndex;
                this.ProcessorType = processorType;
            }
        }
    }
}
