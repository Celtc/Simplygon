﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.SlotOutput
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
  public class SlotOutput : Slot
  {
    private int slotNumber;
    private Vector2 interSlotsUiOffset;
    private Vector2 slotsUiOffset;

    public SlotOutput(int slotNumber, SlotComponents components, NodeWindow parent, Vector2 interSlotsUiOffset, Vector2 slotsUiOffset)
      : base(components, parent)
    {
      this.slotNumber = slotNumber;
      this.interSlotsUiOffset = interSlotsUiOffset;
      this.slotsUiOffset = slotsUiOffset;
    }

    public override void Draw(Rect parent)
    {
      Color backgroundColor = GUI.backgroundColor;
      GUI.backgroundColor = SharedData.Instance.NodeEditorWindow.SelectedSlot == this ? Color.yellow : Slot.GetSlotComponentsColor(this.Components);
      Rect position = new Rect((float) ((double) parent.xMax + (double) this.interSlotsUiOffset.x - 2.0), parent.yMin + this.interSlotsUiOffset.y * (float) this.slotNumber + this.slotsUiOffset.y, 20f, 16f);
      this.Rect = position;
      if (GUI.Button(position, ">", EditorStyles.miniButton))
      {
        if (Event.current.button == 0)
          SharedData.Instance.NodeEditorWindow.HandleSlotSelected((Slot) this);
        else if (Event.current.button == 1 && this.ConnectedSlots.Count > 0)
        {
          Slot slot1 = this.ConnectedSlots.ElementAt<Slot>(this.ConnectedSlots.Count - 1);
          slot1.ConnectedSlots.Remove((Slot) this);
          slot1.Parent.InputSlots.ForEach((Action<SlotInput>) (slot => slot.UpdateEnabledStatus()));
          this.ConnectedSlots.RemoveAt(this.ConnectedSlots.Count - 1);
        }
      }
      GUI.backgroundColor = backgroundColor;
    }

    protected override void InternalHandleSlotConnected(Slot otherSlot)
    {
      if (!(otherSlot is SlotInput))
        return;
      this.ConnectedSlots.Add(otherSlot);
    }
  }
}
