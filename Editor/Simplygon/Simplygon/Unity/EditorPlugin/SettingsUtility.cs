﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.SettingsUtility
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Base;
using Simplygon.SPL.v80.Enum;
using Simplygon.SPL.v80.MaterialCaster;
using Simplygon.SPL.v80.Node;
using Simplygon.SPL.v80.Processor;
using Simplygon.SPL.v80.Settings;
using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class SettingsUtility
    {
        private static readonly double UnityQualityMetricFactor = 0.001;

        public static SettingsUtility.UnityQualityMetric GetQualityMetric(double value)
        {
            int num = (int)(value / SettingsUtility.UnityQualityMetricFactor);
            SettingsUtility.UnityQualityMetric unityQualityMetric = SettingsUtility.UnityQualityMetric.NormalQuality;
            if (System.Enum.IsDefined(typeof(SettingsUtility.UnityQualityMetric), (object)num))
                unityQualityMetric = (SettingsUtility.UnityQualityMetric)num;
            return unityQualityMetric;
        }

        public static double GetQuality(SettingsUtility.UnityQualityMetric qualityMetric)
        {
            return (double)qualityMetric * SettingsUtility.UnityQualityMetricFactor;
        }

        internal static ProcessNode GetDefaultProcessNode(Type processorType)
        {
            ProcessNode processNode = new ProcessNode();
            processNode.Processor = (BaseProcessor)Activator.CreateInstance(processorType);
            processNode.DefaultTBNType = TangentSpaceMethod.SG_TANGENTSPACEMETHOD_ORTHONORMAL;
            SettingsUtility.SetDefaultProcessorSettings(processNode.Processor);
            return processNode;
        }

        internal static void ResetChannelCastSettings(List<ProcessNode> processNodes)
        {
            foreach (ProcessNode processNode in processNodes)
            {
                processNode.MaterialCaster.Clear();
                SettingsUtility.ResetChannelCastSettings(SettingsUtility.GetChildProcessNodes(processNode));
            }
        }

        public static void EnableCastChannel(string channelName)
        {
            for (int lodNumber = 0; lodNumber < SharedData.Instance.LODChain.Count; ++lodNumber)
            {
                bool flag = true;
                ReductionProcessor processor1 = SharedData.Instance.LODChain[lodNumber].Processor as ReductionProcessor;
                if (processor1 != null)
                {
                    flag = processor1.MappingImageSettings.Enabled;
                }
                else
                {
                    RemeshingProcessor processor2 = SharedData.Instance.LODChain[lodNumber].Processor as RemeshingProcessor;
                    if (processor2 != null)
                        flag = processor2.MappingImageSettings.Enabled;
                }
                if (flag)
                    SettingsUtility.AddCaster(SharedData.Instance.LODChain[lodNumber].MaterialCaster, channelName, lodNumber);
            }
        }

        public static void HandleNumberOfSelectedLODs(int newLODCount)
        {
            int count = SharedData.Instance.LODChain.Count;
            if (count == newLODCount)
                return;
            if (newLODCount > count)
            {
                for (int index = 0; index < newLODCount - count; ++index)
                    SharedData.Instance.LODChain.Add(SharedData.Instance.LODChain[SharedData.Instance.LODChain.Count - 1].DeepCopy() as ProcessNode);
            }
            else
            {
                for (int index = newLODCount; index < SharedData.Instance.LODChain.Count; ++index)
                    SharedData.Instance.LODChain.RemoveAt(index);
            }
        }

        public static List<string> GetSelectedProcessNodesOfType(Type processorType)
        {
            List<string> stringList = new List<string>();
            for (int index = 0; index < SharedData.Instance.LODChain.Count; ++index)
            {
                if (SharedData.Instance.LODChain[index].Processor.GetType() == processorType)
                    stringList.Add(index.ToString());
            }
            return stringList;
        }

        internal static void SetDefaultProcessorSettings(BaseProcessor baseProcessor)
        {
            if (baseProcessor is ReductionProcessor)
            {
                ReductionProcessor reductionProcessor = (ReductionProcessor)baseProcessor;
                SettingsUtility.SetDefaultSettings(reductionProcessor.BoneSettings, BoneReductionTargets.SG_BONEREDUCTIONTARGET_BONERATIO, 0.5, 300);
                SettingsUtility.SetDefaultSettings(reductionProcessor.MappingImageSettings, true);
                SettingsUtility.SetDefaultSettings(reductionProcessor.NormalCalculationSettings);
                SettingsUtility.SetDefaultSettings(reductionProcessor.ReductionSettings);
                SettingsUtility.SetDefaultSettings(reductionProcessor.RepairSettings);
                SettingsUtility.SetDefaultSettings(reductionProcessor.VisibilitySettings);
                reductionProcessor.MappingImageSettings.Enabled = false;
                reductionProcessor.BoneSettings.Enabled = false;
            }
            else if (baseProcessor is RemeshingProcessor)
            {
                RemeshingProcessor remeshingProcessor = (RemeshingProcessor)baseProcessor;
                SettingsUtility.SetDefaultSettings(remeshingProcessor.BoneSettings, BoneReductionTargets.SG_BONEREDUCTIONTARGET_ONSCREENSIZE, 0.0, 300);
                SettingsUtility.SetDefaultSettings(remeshingProcessor.MappingImageSettings, false);
                SettingsUtility.SetDefaultSettings(remeshingProcessor.RemeshingSettings);
                SettingsUtility.SetDefaultSettings(remeshingProcessor.VisibilitySettings);
                remeshingProcessor.BoneSettings.Enabled = false;
            }
            else if (baseProcessor is AggregationProcessor)
            {
                AggregationProcessor aggregationProcessor = (AggregationProcessor)baseProcessor;
                SettingsUtility.SetDefaultSettings(aggregationProcessor.MappingImageSettings, true);
                SettingsUtility.SetDefaultSettings(aggregationProcessor.AggregationSettings);
                SettingsUtility.SetDefaultSettings(aggregationProcessor.VisibilitySettings);
            }
            else
                Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to set default processor settings for processor '{0}'.", (object)baseProcessor.GetType().ToString()));
        }

        internal static Simplygon.SPL.v80.SPL CreateSPLFromCurrentNodes(bool isCascadedLOD)
        {
            Simplygon.SPL.v80.SPL spl = new Simplygon.SPL.v80.SPL();
            if (isCascadedLOD)
            {
                BaseNode baseNode1 = (BaseNode)null;
                for (int index = 0; index < SharedData.Instance.LODChain.Count; ++index)
                {
                    BaseNode baseNode2 = SharedData.Instance.LODChain[index].DeepCopy();
                    baseNode2.Name = string.Format(CultureInfo.InvariantCulture, "Node{0}", (object)index);
                    WriteNode writeNode = new WriteNode();
                    writeNode.Format = "ssf";
                    writeNode.Name = "Output";
                    baseNode2.Children.Add((BaseNode)writeNode);
                    if (index == 0)
                        spl.ProcessGraph = baseNode2;
                    else
                        baseNode1.Children.Add(baseNode2);
                    baseNode1 = baseNode2;
                }
            }
            else
            {
                spl.ProcessGraph = (BaseNode)new ContainerNode();
                for (int index = 0; index < SharedData.Instance.LODChain.Count; ++index)
                {
                    BaseNode baseNode = SharedData.Instance.LODChain[index].DeepCopy();
                    baseNode.Name = string.Format(CultureInfo.InvariantCulture, "Node{0}", (object)index);
                    WriteNode writeNode = new WriteNode();
                    writeNode.Format = "ssf";
                    writeNode.Name = "Output";
                    baseNode.Children.Add((BaseNode)writeNode);
                    spl.ProcessGraph.Children.Add(baseNode);
                }
            }
            return spl;
        }

        public static void LoadSPLSettings(string filename)
        {
            Simplygon.SPL.v80.SPL spl = Simplygon.SPL.v80.SPL.Load(filename);
            if (spl.ProcessGraph is ContainerNode)
            {
                SharedData.Instance.LODChain.Clear();
                SharedData.Instance.Settings.SetCascadedLOD(false);
                foreach (BaseNode child in (spl.ProcessGraph as ContainerNode).Children)
                {
                    if (child is ProcessNode)
                        SharedData.Instance.LODChain.Add(child as ProcessNode);
                    else
                        Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected container child node '{0}'.", (object)child.GetType().Name));
                }
            }
            else if (spl.ProcessGraph is ProcessNode)
            {
                SharedData.Instance.LODChain.Clear();
                SharedData.Instance.Settings.SetCascadedLOD(true);
                ProcessNode processGraph = spl.ProcessGraph as ProcessNode;
                SharedData.Instance.LODChain.Add(processGraph);
                SettingsUtility.LoadNextSPLNode(processGraph);
            }
            else
                Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to load settings; unexpected root process node '{0}'.", spl.ProcessGraph == null ? (object)"null" : (object)spl.ProcessGraph.GetType().Name));
        }

        private static void LoadNextSPLNode(ProcessNode processNode)
        {
            int num = processNode.Children.Count<BaseNode>((Func<BaseNode, bool>)(child => child is ProcessNode));
            if (num > 1)
                Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected number of child process nodes ({0}) in SPL settings.", (object)num));
            foreach (BaseNode child in processNode.Children)
            {
                if (child is ProcessNode)
                {
                    SharedData.Instance.LODChain.Add(child as ProcessNode);
                    SettingsUtility.LoadNextSPLNode(child as ProcessNode);
                }
            }
        }

        private static void SetDefaultSettings(BoneSettings boneSettings, BoneReductionTargets boneReductionTargets, double numberOfTriangles, int onScreenSize)
        {
            boneSettings.BoneReductionTargets = boneReductionTargets;
            boneSettings.BoneRatio = numberOfTriangles;
            boneSettings.OnScreenSize = onScreenSize;
        }

        private static void SetDefaultSettings(MappingImageSettings mappingImageSettings, bool useChartAggregator)
        {
            int num = SharedData.Instance.Account != null ? SharedData.Instance.Account.ProcessSubscriptionRestrictions.MaterialLOD.MaxOutputTextureSize : 8192;
            if (mappingImageSettings.Width > num)
                mappingImageSettings.Width = num;
            if (mappingImageSettings.Height > num)
                mappingImageSettings.Height = num;
            mappingImageSettings.ChartAggregatorSeparateOverlappingCharts = false;
            mappingImageSettings.UseAutomaticTextureSize = false;
            mappingImageSettings.ForcePower2Texture = false;
            mappingImageSettings.MultisamplingLevel = 3;
            mappingImageSettings.UseFullRetexturing = !useChartAggregator;
            mappingImageSettings.GenerateMappingImage = true;
            mappingImageSettings.GenerateTangents = true;
            mappingImageSettings.GenerateTexCoords = true;
            mappingImageSettings.GutterSpace = 4;
            mappingImageSettings.ParameterizerMaxStretch = 0.12;
            mappingImageSettings.MaximumLayers = 3;
            mappingImageSettings.UseVertexWeights = false;
            mappingImageSettings.ChartAggregatorMode = ChartAggregatorMode.SG_CHARTAGGREGATORMODE_TEXTURESIZEPROPORTIONS;
            mappingImageSettings.TexCoordGeneratorType = useChartAggregator ? TexCoordGeneratorType.SG_TEXCOORDGENERATORTYPE_CHARTAGGREGATOR : TexCoordGeneratorType.SG_TEXCOORDGENERATORTYPE_PARAMETERIZER;
            mappingImageSettings.Enabled = true;
        }

        private static void SetDefaultSettings(NormalCalculationSettings normalCalculationSettings)
        {
        }

        private static void SetDefaultSettings(ReductionSettings reductionSettings)
        {
            reductionSettings.Enabled = true;
            reductionSettings.StopCondition = StopCondition.SG_STOPCONDITION_ALL;
            reductionSettings.ReductionTargets = ReductionTargets.SG_REDUCTIONTARGET_TRIANGLERATIO;
            reductionSettings.ReductionHeuristics = ReductionHeuristics.SG_REDUCTIONHEURISTICS_CONSISTENT;
            reductionSettings.TriangleRatio = 0.5;
            reductionSettings.OnScreenSize = 300;
            reductionSettings.EdgeSetImportance = SettingsUtility.GetQuality(SettingsUtility.UnityQualityMetric.NormalQuality);
            reductionSettings.GeometryImportance = SettingsUtility.GetQuality(SettingsUtility.UnityQualityMetric.NormalQuality);
            reductionSettings.GroupImportance = SettingsUtility.GetQuality(SettingsUtility.UnityQualityMetric.NormalQuality);
            reductionSettings.MaterialImportance = SettingsUtility.GetQuality(SettingsUtility.UnityQualityMetric.NormalQuality);
            reductionSettings.ShadingImportance = SettingsUtility.GetQuality(SettingsUtility.UnityQualityMetric.NormalQuality);
            reductionSettings.SkinningImportance = SettingsUtility.GetQuality(SettingsUtility.UnityQualityMetric.NormalQuality);
            reductionSettings.TextureImportance = SettingsUtility.GetQuality(SettingsUtility.UnityQualityMetric.NormalQuality);
            reductionSettings.VertexColorImportance = SettingsUtility.GetQuality(SettingsUtility.UnityQualityMetric.NormalQuality);
            reductionSettings.UseVertexWeights = false;
        }

        private static void SetDefaultSettings(RepairSettings repairSettings)
        {
            repairSettings.UseTJunctionRemover = false;
        }

        private static void SetDefaultSettings(VisibilitySettings visibiltySettings)
        {
            visibiltySettings.Enabled = false;
        }

        private static void SetDefaultSettings(RemeshingSettings remeshingSettings)
        {
            remeshingSettings.Enabled = true;
            remeshingSettings.OnScreenSize = 300;
            remeshingSettings.MaxTriangleSize = 0;
            remeshingSettings.HardEdgeAngleInRadians = 4.0 * Mathf.PI / 9.0;
            remeshingSettings.MergeDistance = 4;
        }

        private static void SetDefaultSettings(AggregationSettings sceneAggregatorSettings)
        {
            sceneAggregatorSettings.Enabled = true;
        }

        public static bool IsAggregateLODEnabled()
        {
            return SettingsUtility.IsProcessorEnabled<AggregationProcessor>();
        }

        public static bool IsProxyLODEnabled()
        {
            return SettingsUtility.IsProcessorEnabled<RemeshingProcessor>();
        }

        private static bool IsProcessorEnabled<T>()
        {
            foreach (ProcessNode processNode in SharedData.Instance.LODChain)
            {
                if (processNode.Processor is T)
                    return true;
            }
            return false;
        }

        private static List<ProcessNode> GetChildProcessNodes(ProcessNode processNode)
        {
            List<ProcessNode> processNodeList = new List<ProcessNode>();
            foreach (BaseNode child in processNode.Children)
            {
                if (child is ProcessNode)
                    processNodeList.Add(child as ProcessNode);
            }
            return processNodeList;
        }

        private static void AddCaster(List<BaseMaterialCaster> baseMaterialCasters, string channelName, int lodNumber)
        {
            if (baseMaterialCasters.Exists((Predicate<BaseMaterialCaster>)(caster => caster.Channel == channelName)))
                return;
            if (Constants.Normals == channelName || Constants.SecondaryNormals == channelName)
                SettingsUtility.AddNormalsCaster(baseMaterialCasters, lodNumber, channelName);
            else if (Constants.Opacity == channelName)
                SettingsUtility.AddOpacityCaster(baseMaterialCasters, lodNumber);
            else
                SettingsUtility.AddColorCaster(baseMaterialCasters, lodNumber, channelName);
        }

        private static void AddNormalsCaster(List<BaseMaterialCaster> baseMaterialCasters, int lodNumber, string channelName)
        {
            NormalCaster normalCaster = new NormalCaster();
            normalCaster.Channel = channelName;
            normalCaster.Dilation = 8;
            normalCaster.FillMode = FillMode.SG_ATLASFILLMODE_INTERPOLATE;
            normalCaster.GenerateTangentSpaceNormals = true;
            normalCaster.Name = string.Format(CultureInfo.InvariantCulture, "{0}Map_LOD{1}", (object)channelName, (object)lodNumber);
            normalCaster.OutputChannels = 4;
            normalCaster.OutputChannelBitDepth = 8;
            normalCaster.FlipBackfacingNormals = true;
            baseMaterialCasters.Add((BaseMaterialCaster)normalCaster);
        }

        private static void AddOpacityCaster(List<BaseMaterialCaster> baseMaterialCasters, int lodNumber)
        {
            OpacityCaster opacityCaster = new OpacityCaster();
            opacityCaster.Channel = "Opacity";
            opacityCaster.Dilation = 8;
            opacityCaster.FillMode = FillMode.SG_ATLASFILLMODE_INTERPOLATE;
            opacityCaster.BakeOpacityInAlpha = true;
            opacityCaster.Name = string.Format(CultureInfo.InvariantCulture, "OpacityMap_LOD{0}", (object)lodNumber);
            opacityCaster.OutputChannels = 4;
            opacityCaster.OutputChannelBitDepth = 8;
            opacityCaster.ColorType = "Opacity";
            baseMaterialCasters.Add((BaseMaterialCaster)opacityCaster);
        }

        private static void AddColorCaster(List<BaseMaterialCaster> baseMaterialCasters, int lodNumber, string channelName)
        {
            ColorCaster colorCaster = new ColorCaster();
            colorCaster.BakeOpacityInAlpha = false;
            colorCaster.Channel = channelName;
            colorCaster.ColorType = channelName;
            colorCaster.Dilation = 8;
            colorCaster.FillMode = FillMode.SG_ATLASFILLMODE_INTERPOLATE;
            colorCaster.Name = string.Format(CultureInfo.InvariantCulture, "{0}Map_LOD{1}", (object)channelName, (object)lodNumber);
            colorCaster.OutputChannelBitDepth = 8;
            colorCaster.OutputSRGB = false;
            baseMaterialCasters.Add((BaseMaterialCaster)colorCaster);
        }

        public enum UnityReductionTargets : uint
        {
            TriangleRatio = 2,
            OnScreenSize = 8,
        }

        public enum UnityBoneReductionTargets : uint
        {
            BoneRatio = 2,
            OnScreenSize = 8,
        }

        public enum UnityMappingImageSettingsSuperSampling
        {
            Lowest = 1,
            Low = 2,
            Normal = 3,
            High = 4,
        }

        public enum UnityQualityMetric
        {
            LowestQuality = 125, // 0x0000007D
            LowQuality = 350, // 0x0000015E
            NormalQuality = 1000, // 0x000003E8
            High = 2800, // 0x00000AF0
            Highest = 8000, // 0x00001F40
        }
    }
}
