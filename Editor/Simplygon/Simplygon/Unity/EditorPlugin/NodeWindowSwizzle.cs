﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.NodeWindowSwizzle
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class NodeWindowSwizzle : NodeWindow
    {
        public NodeWindowSwizzle(int nodeRef)
          : base(nodeRef)
        {
            this.Title = "Swizzle";
            List<SlotComponents> list = ((IEnumerable<SlotComponents>)Enum.GetValues(typeof(SlotComponents))).ToList<SlotComponents>();
            list.Remove(SlotComponents.RGBA);
            list.Insert(0, SlotComponents.RGBA);
            list.Remove(SlotComponents.RGB);
            list.Insert(1, SlotComponents.RGB);
            foreach (SlotComponents slotComponents in list)
            {
                this.AddInputSlot(slotComponents);
                this.AddOutputSlot(slotComponents);
            }
            this.HelpText = "The Swizzle node enables filtering or rewiring of specific components, e.g. having an arbitrary node's red output become the swizzle node's blue output by connecting the red output to the swizzle node's blue input.\n\nAny non-specified input components will use the connected output's component, e.g. connecting a red output to the swizzle node's RGBA input will make the swizzle node's RGBA output evaluate to (R,R,R,R). Also, connecting an RGB or RGBA output to a component specific input will implicitly make the input use only its specific component, e.g. connecting an arbitrary node's RGBA output to the swizzle node's red input will make the swizzle node output the arbitrary node's red component as red (i.e. connecting RGBA -> R, RGBA -> G, RGBA -> B and RGBA -> A is the same as connecting R -> R, B -> B, G -> G and A -> A).\n\nAn unconnected input will produce a zero value or zero vector.";
        }

        public override void DrawContents(Rect parent)
        {
            base.DrawContents(parent);
            this.SetWindowWithinBounds(parent);
        }

        internal override void Save(List<ModifierShadingNodeProperties> modifierNodeProperties)
        {
            int[] inputNodes = new int[4] { -1, -1, -1, -1 };
            int[] nodeData = new int[4] { 0, 1, 2, 3 };
            foreach (SlotInput inputSlot in this.InputSlots)
            {
                int connectedNodeRef;
                SlotComponents connectedNodeOuputSlotComponents;
                if (inputSlot.GetConnectedNodeInfo(out connectedNodeRef, out connectedNodeOuputSlotComponents))
                {
                    if (SlotComponents.RGBA == inputSlot.Components)
                    {
                        for (int index = 0; index < inputNodes.Length; ++index)
                        {
                            inputNodes[index] = connectedNodeRef;
                            if (SlotComponents.RGBA != connectedNodeOuputSlotComponents && SlotComponents.RGB != connectedNodeOuputSlotComponents)
                                nodeData[index] = (int)connectedNodeOuputSlotComponents;
                        }
                    }
                    else if (SlotComponents.RGB == inputSlot.Components)
                    {
                        inputNodes[0] = inputNodes[1] = inputNodes[2] = connectedNodeRef;
                        inputNodes[3] = -1;
                        if (SlotComponents.RGBA == connectedNodeOuputSlotComponents || SlotComponents.RGB == connectedNodeOuputSlotComponents)
                        {
                            nodeData[0] = 0;
                            nodeData[1] = 1;
                            nodeData[2] = 2;
                        }
                        else
                            nodeData[0] = nodeData[1] = nodeData[2] = (int)connectedNodeOuputSlotComponents;
                        nodeData[3] = -1;
                    }
                    else
                    {
                        inputNodes[(int)inputSlot.Components] = connectedNodeRef;
                        if (SlotComponents.RGBA != connectedNodeOuputSlotComponents && SlotComponents.RGB != connectedNodeOuputSlotComponents)
                            nodeData[(int)inputSlot.Components] = (int)connectedNodeOuputSlotComponents;
                    }
                }
            }
            ModifierShadingNodeProperties shadingNodeProperties = new ModifierShadingNodeProperties(this.NodeRef, inputNodes, nodeData, ModifierShadingNodeType.Swizzle);
            modifierNodeProperties.Add(shadingNodeProperties);
        }
    }
}
