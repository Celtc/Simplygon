﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.SimplygonScreen
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
  public class SimplygonScreen
  {
    private Texture2D[] toolbarTextures;

    public QuickProcessToolbar SimplygonQuickProcessToolbar { get; private set; }

    public ProcessToolbar SimplygonProcessToolbar { get; private set; }

    public JobsToolbar SimplygonJobsToolbar { get; private set; }

    public UserToolbar SimplygonUserToolbar { get; private set; }

    public int SelectedToolbar { get; set; }

    public SimplygonScreen()
    {
      this.SelectedToolbar = 0;
    }

    public void Initialize()
    {
      this.SimplygonQuickProcessToolbar = new QuickProcessToolbar();
      this.SimplygonProcessToolbar = new ProcessToolbar();
      this.SimplygonJobsToolbar = new JobsToolbar();
      this.SimplygonUserToolbar = new UserToolbar();
      this.SimplygonQuickProcessToolbar.AdvancedSettingsSelected += (EventHandler) ((s, e) => SharedData.Instance.Settings.SetSelectedToolbar(1));
      this.toolbarTextures = new Texture2D[4];
    }

    public void Show()
    {
      this.toolbarTextures[0] = ResourcesEx.LoadCreateLODQuickStartIcon();
      this.toolbarTextures[1] = ResourcesEx.LoadCreateLODAdvancedIcon();
      this.toolbarTextures[2] = SharedData.Instance.WorkingIcon.CurrentWorkingIcon;
      this.toolbarTextures[3] = ResourcesEx.LoadUserIcon();
      EditorGUILayout.Space();
      int selectedToolbar1 = SharedData.Instance.Settings.GetSelectedToolbar();
      if (selectedToolbar1 != this.SelectedToolbar)
        this.SelectedToolbar = selectedToolbar1;
      int selectedToolbar2 = this.SelectedToolbar;
      this.SelectedToolbar = GUILayout.Toolbar(this.SelectedToolbar, (Texture[]) this.toolbarTextures);
      if (selectedToolbar2 != this.SelectedToolbar)
      {
        if (this.SelectedToolbar == 0)
          this.SimplygonQuickProcessToolbar.ResetSettings();
        SharedData.Instance.Settings.SetSelectedToolbar(this.SelectedToolbar);
      }
      EditorGUILayout.Space();
      float labelWidth = EditorGUIUtility.labelWidth;
      if (this.SelectedToolbar == 0)
      {
        EditorGUIUtility.labelWidth = 70f;
        this.SimplygonQuickProcessToolbar.Show();
        EditorGUIUtility.labelWidth = labelWidth;
      }
      else if (this.SelectedToolbar == 1)
      {
        EditorGUIUtility.labelWidth = 200f;
        this.SimplygonProcessToolbar.Show();
        EditorGUIUtility.labelWidth = labelWidth;
      }
      else if (this.SelectedToolbar == 2)
      {
        EditorGUIUtility.labelWidth = 80f;
        this.SimplygonJobsToolbar.Show();
        EditorGUIUtility.labelWidth = labelWidth;
      }
      else
      {
        if (this.SelectedToolbar != 3)
          return;
        EditorGUIUtility.labelWidth = 80f;
        GUILayout.Box("", new GUILayoutOption[2]
        {
          GUILayout.ExpandWidth(true),
          GUILayout.Height(1f)
        });
        this.SimplygonUserToolbar.Show();
        EditorGUIUtility.labelWidth = labelWidth;
      }
    }

    public void Update()
    {
      this.SimplygonQuickProcessToolbar.Update();
      this.SimplygonProcessToolbar.Update();
      this.SimplygonJobsToolbar.Update();
      this.SimplygonUserToolbar.Update();
    }

    public void OnInspectorUpdate()
    {
      this.SimplygonQuickProcessToolbar.OnInspectorUpdate();
      this.SimplygonProcessToolbar.OnInspectorUpdate();
      this.SimplygonJobsToolbar.OnInspectorUpdate();
      this.SimplygonUserToolbar.OnInspectorUpdate();
    }
  }
}
