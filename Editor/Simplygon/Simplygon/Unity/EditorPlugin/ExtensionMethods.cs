﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.ExtensionMethods
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.Collections.Generic;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
  public static class ExtensionMethods
  {
    public static void GetUVsWrapper(this Mesh mesh, int channel, ref List<Vector2> uvs)
    {
      mesh.GetUVs(channel, uvs);
    }

    public static void SetUVsWrapper(this Mesh mesh, int channel, List<Vector2> uvs)
    {
      mesh.SetUVs(channel, uvs);
    }
  }
}
