﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.GeneralManager
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Yoda.Client;
using Simplygon.Cloud.Yoda.IntegrationClient;
using Simplygon.Unity.EditorPlugin.Jobs;
using Simplygon.Unity.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class GeneralManager
    {
        private readonly string UnityJobType = "Unity";
        private BackgroundWorker backgroundWorker;

        public JobManager20 JobManager
        {
            get; private set;
        }

        public LoginState LoginState
        {
            get; private set;
        }

        internal YodaClient23 SimplygonCloudClient
        {
            get; private set;
        }

        public GeneralManager()
        {
            this.LoginState = LoginState.LoggedOut;
            this.backgroundWorker = new BackgroundWorker();
        }

        ~GeneralManager()
        {
            this.Release();
        }

        public void Initialize()
        {
            this.backgroundWorker.Start();
        }

        public void Release()
        {
            if (this.backgroundWorker == null)
                return;
            this.backgroundWorker.Stop();
        }

        public void Login(string username, string password, string hostname)
        {
            try
            {
                this.JobManager = new JobManager20();
                JobFactory.Instance.Setup(Constants.ApiKey, username, password, hostname, new Action<string>(this.Log), new Action<Simplygon.Scene.Scene, string>(this.JobManager.WriteSceneToWorkDirectory), new Action<string, string, string, Action<int, string>>(JobUtils.ZipDirectory), new Action<string, string, string, Action<int, string>>(JobUtils.UnZipToDirectory), new Action<CloudJob, List<string>, Action>(this.ImportJob), new Action<string>(this.CleanWorkDirectory), this.UnityJobType);
                this.backgroundWorker.Start();
                this.SimplygonCloudClient = new YodaClient23(Constants.ApiKey, username, password, hostname);
                this.LoginState = LoginState.LoginPending;
                this.backgroundWorker.Dispatcher.BeginInvoke(new DispatcherAction(new Action(this.GMThread_Login), (Action)(() => SharedData.Instance.SimplygonWindow.ForceRepaint = true), SharedData.Instance.UnityDispatcher));
            }
            catch (Exception ex)
            {
                UIUtility.LogException("Login failed", ex);
            }
        }

        public void Logout()
        {
            this.backgroundWorker.Dispatcher.BeginInvoke(new DispatcherAction((Action)(() =>
           {
               this.backgroundWorker.Stop();
               this.JobManager = (JobManager20)null;
               this.SimplygonCloudClient = (YodaClient23)null;
               SharedData.Instance.UnityDispatcher.BeginInvoke(new DispatcherAction((Action)(() =>
         {
                 SharedData.Instance.Username = string.Empty;
                 SharedData.Instance.Password = string.Empty;
                 SharedData.Instance.Settings.SetUsername(string.Empty);
                 SharedData.Instance.Settings.SetPassword(string.Empty);
                 SharedData.Instance.Account = (User23)null;
                 SharedData.Instance.Settings.SetKeepMeLoggedIn(false);
                 this.LoginState = LoginState.LoggedOut;
                 SharedData.Instance.SimplygonWindow.ForceRepaint = true;
             })));
           })));
        }

        public void UpdateAccount()
        {
            this.backgroundWorker.Dispatcher.BeginInvoke(new DispatcherAction(new Action(this.GMThread_UpdateAccount)));
        }

        public void UpdateClientInfo()
        {
            this.backgroundWorker.Dispatcher.BeginInvoke(new DispatcherAction(new Action(this.GMThread_UpdateClientInfo)));
        }

        public void ForceRepaint()
        {
            SharedData.Instance.UnityDispatcher.BeginInvoke(new DispatcherAction((Action)(() => SharedData.Instance.SimplygonWindow.ForceRepaint = true)));
        }

        public void CreateJob(string name, string priority, List<PrefabEx> prefabs, Action onCompleted)
        {
            try
            {
                bool isCascadedLOD = SharedData.Instance.Settings.GetCascadedLOD();
                this.backgroundWorker.Dispatcher.BeginInvoke(new DispatcherAction((Action)(() => this.JobManager.GMThread_CreateJob(name, prefabs, isCascadedLOD, this.backgroundWorker.Dispatcher, onCompleted))));
            }
            catch (Exception ex)
            {
                Debug.LogError((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: GeneralManager Failed to create job - Settings {0}, background worker {1}, background worker dispatcher {2}, JobManager {3} ({4}\n{5})", (object)(SharedData.Instance.Settings == null), (object)(this.backgroundWorker == null), this.backgroundWorker != null ? (object)(this.backgroundWorker.Dispatcher == null).ToString() : (object)"N/A", (object)(this.JobManager == null), (object)ex.Message, (object)ex.StackTrace));
            }
        }

        internal void ImportJob(CloudJob job, List<string> lodFilePaths, Action onCompleted)
        {
            UnityCloudJob jobToImport = this.JobManager.Jobs.Find((Predicate<UnityCloudJob>)(unityCloudJob => unityCloudJob.CloudJob == job));
            this.backgroundWorker.Dispatcher.BeginInvoke(new DispatcherAction((Action)(() => SharedData.Instance.UnityDispatcher.BeginInvoke(new DispatcherAction((Action)(() => this.JobManager.UnityThread_ImportJob(this.SimplygonCloudClient, jobToImport, lodFilePaths, onCompleted)))))));
        }

        public void LoadJobs()
        {
            if (this.SimplygonCloudClient == null)
                return;
            this.backgroundWorker.Dispatcher.BeginInvoke(new DispatcherAction((Action)(() => this.JobManager.GMThread_LoadJobs(this.SimplygonCloudClient))));
        }

        private void Log(string message)
        {
            Debug.Log((object)message);
        }

        private void UpdateUserSettings()
        {
            if (this.SimplygonCloudClient == null)
                return;
            this.backgroundWorker.Dispatcher.BeginInvoke(new DispatcherAction((Action)(() => this.JobManager.GMThread_UpdateUserSettings(this.SimplygonCloudClient, this.backgroundWorker.Dispatcher))));
        }

        private void GMThread_Login()
        {
            try
            {
                User23 user23 = this.SimplygonCloudClient.AccountInfo();
                if (user23 != null)
                {
                    SharedData.Instance.Account = user23;
                    this.LoginState = LoginState.LoginSuccess;
                    this.UpdateUserSettings();
                    this.LoadJobs();
                    SharedData.Instance.UnityDispatcher.BeginInvoke(new DispatcherAction((Action)(() => SharedData.Instance.SimplygonWindow.ForceRepaint = true)));
                }
                else
                    this.LoginState = LoginState.LoginFail;
            }
            catch (Exception ex)
            {
                this.LoginState = LoginState.LoginFail;
            }
        }

        private void GMThread_UpdateAccount()
        {
            User23 user23 = this.SimplygonCloudClient.AccountInfo();
            if (user23 == null)
                return;
            SharedData.Instance.Account = user23;
        }

        private void GMThread_UpdateClientInfo()
        {
            Client23 client23 = YodaClient23.ClientInfo("public.simplygon.com", Constants.ApiKey);
            if (client23 == null)
                return;
            SharedData.Instance.ClientInfo = client23;
        }

        private void CleanWorkDirectory(string directory)
        {
        }

        private void OnJobDeleted(CloudJob job)
        {
            this.JobManager.OnJobDeleted(job);
        }
    }
}
