﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.LoginScreen
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
  public class LoginScreen
  {
    private Vector2 scrollPosition = Vector2.zero;
    private Texture2D simplygonLogo;
    private Window parentWindow;
    private bool keepMeLoggedIn;
    public static bool PendingClientInfo;

    public LoginScreen(Window parentWindow)
    {
      this.parentWindow = parentWindow;
    }

    public void Initialize()
    {
      this.keepMeLoggedIn = SharedData.Instance.Settings.GetKeepMeLoggedIn();
      SharedData.Instance.Hostname = SharedData.Instance.Settings.GetHostname();
      if (!this.keepMeLoggedIn)
        return;
      SharedData.Instance.Username = SharedData.Instance.Settings.GetUsername();
      SharedData.Instance.Password = SharedData.Instance.Settings.GetPassword();
    }

    public void Update()
    {
    }

    public void OnInspectorUpdate()
    {
    }

    public void Show()
    {
      if ((Object) this.simplygonLogo == (Object) null)
        this.simplygonLogo = ResourcesEx.LoadSimplygonLogo();
      Rect position = new Rect(0.0f, 10f, this.parentWindow.position.width, 64f);
      GUI.DrawTexture(position, (Texture) this.simplygonLogo, ScaleMode.ScaleToFit, true);
      GUILayout.Label("", new GUILayoutOption[2]
      {
        GUILayout.Width(position.width),
        GUILayout.Height(position.height)
      });
      EditorGUILayout.Space();
      if (Application.isPlaying)
      {
        EditorGUILayout.Space();
        EditorGUILayout.BeginVertical();
        EditorGUILayout.HelpBox("Exit play mode to enable Simplygon", MessageType.Warning);
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndVertical();
      }
      else
      {
        this.scrollPosition = EditorGUILayout.BeginScrollView(this.scrollPosition);
        GUILayout.Space(12f);
        if (GUILayout.Button("Sign up, it's free"))
          Application.OpenURL("http://www.simplygon.com/games");
        EditorGUILayout.Space();
        GUILayout.Label("Please log in if you already have an account");
        EditorGUILayout.Space();
        GUI.SetNextControlName("UsernameControl");
        SharedData.Instance.Username = EditorGUILayout.TextField("User", SharedData.Instance.Username, new GUILayoutOption[0]);
        GUI.SetNextControlName("PasswordControl");
        SharedData.Instance.Password = EditorGUILayout.PasswordField("Password", SharedData.Instance.Password, new GUILayoutOption[0]);
        SharedData.Instance.Hostname = EditorGUILayout.TextField("Server", SharedData.Instance.Hostname, new GUILayoutOption[0]);
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        EditorGUILayout.LabelField("Set default server: ", new GUILayoutOption[1]
        {
          GUILayout.Width(116f)
        });
        if (GUILayout.Button("Grid", EditorStyles.miniButton, new GUILayoutOption[1]
        {
          GUILayout.Width(60f)
        }))
        {
          SharedData.Instance.Username = "user";
          SharedData.Instance.Password = "user";
          SharedData.Instance.Hostname = "127.0.0.1:55001";
          GUI.FocusControl("LoginControl");
        }
        if (GUILayout.Button("Cloud", EditorStyles.miniButton, new GUILayoutOption[1]
        {
          GUILayout.Width(60f)
        }))
        {
          SharedData.Instance.Username = string.Empty;
          SharedData.Instance.Password = string.Empty;
          SharedData.Instance.Hostname = "public.simplygon.com";
          GUI.FocusControl("UsernameControl");
        }
        EditorGUILayout.EndHorizontal();
        this.keepMeLoggedIn = EditorGUILayout.Toggle("Keep me logged in", this.keepMeLoggedIn, new GUILayoutOption[0]);
        if ((GUI.GetNameOfFocusedControl() == "UsernameControl" || GUI.GetNameOfFocusedControl() == "PasswordControl") && Event.current.type == EventType.Used)
          SharedData.Instance.Settings.SetKeepMeLoggedIn(false);
        GUILayout.Space(10f);
        GUI.SetNextControlName("LoginControl");
        if (GUILayout.Button("Log in") || GUI.GetNameOfFocusedControl() != "LoginControl" && SharedData.Instance.GeneralManager.LoginState == LoginState.LoggedOut && (SharedData.Instance.Settings.GetKeepMeLoggedIn() && SharedData.Instance.Username != string.Empty) && (SharedData.Instance.Password != string.Empty && SharedData.Instance.Hostname != string.Empty))
        {
          SharedData.Instance.Settings.SetKeepMeLoggedIn(this.keepMeLoggedIn);
          SharedData.Instance.Settings.SetHostname(SharedData.Instance.Hostname);
          if (SharedData.Instance.Settings.GetKeepMeLoggedIn())
          {
            SharedData.Instance.Settings.SetUsername(SharedData.Instance.Username);
            SharedData.Instance.Settings.SetPassword(SharedData.Instance.Password);
          }
          SharedData.Instance.GeneralManager.Login(SharedData.Instance.Username, SharedData.Instance.Password, SharedData.Instance.Hostname);
        }
        if (SharedData.Instance.GeneralManager.LoginState == LoginState.LoginFail)
          EditorGUILayout.HelpBox("Invalid email or password", MessageType.Error);
        else if (SharedData.Instance.GeneralManager.LoginState == LoginState.LoginPending)
          EditorGUILayout.HelpBox("Please wait", MessageType.Info);
        EditorGUILayout.EndScrollView();
      }
    }
  }
}
