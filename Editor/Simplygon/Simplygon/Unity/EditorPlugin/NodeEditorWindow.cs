﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.NodeEditorWindow
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json;
using Simplygon.Cloud.Unity;
using Simplygon.Unity.EditorPlugin.Jobs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Globalization;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class NodeEditorWindow : EditorWindow
    {
        private readonly Vector2 ToolbarButtonSize = new Vector2(20f, 20f);
        private readonly string CustomChannelName = "Custom";
        private string[] activeChannels = new string[4]
        {
      Constants.Diffuse,
      Constants.Specular,
      Constants.Normals,
      Constants.Opacity
        };
        private readonly string[] SupportedChannels = new string[20]
        {
      Constants.Diffuse,
      Constants.Specular,
      Constants.Normals,
      Constants.Opacity,
      Constants.Parallax,
      Constants.Emission,
      Constants.Occlusion,
      Constants.DetailMask,
      Constants.SecondaryDiffuse,
      Constants.SecondaryNormals,
      Constants.Custom0,
      Constants.Custom1,
      Constants.Custom2,
      Constants.Custom3,
      Constants.Custom4,
      Constants.Custom5,
      Constants.Custom6,
      Constants.Custom7,
      Constants.Custom8,
      Constants.Custom9
        };
        private Dictionary<BasicShadingNodeType, List<string>> currentShaderProperties = new Dictionary<BasicShadingNodeType, List<string>>();
        private Dictionary<int, NodeWindow> nodeWindows = new Dictionary<int, NodeWindow>();
        private string currentNodeHelpText = string.Empty;
        private int currentlySelectedNodeWindowId = -1;
        private List<NodeWindow> nodeWindowsToClose = new List<NodeWindow>();
        private Vector2 channelsScrollPosition = Vector2.zero;
        private static Texture2D BezierTexture;
        private static Texture2D ResizeHandle;
        private Shader currentShader;
        private List<ShadingChannelProperties> currentShadingChannelPropertiesCollection;
        private int currentChannel;
        private int selectedChannel;
        private bool isInitialized;
        private int currentSelectedChannelIndexToAdd;
        private string[] addableChannels;
        private MenuCommand menuCommand;
        private int mainWindowId;
        private bool handleActive;
        private Vector2 scrollPositionNodeCreationGUI;
        private bool doRevertToDefault;
        private GUIContent resizeIcon;
        private Rect handleArea;
        private GUIStyle createNodeButtonStyle;

        public Slot SelectedSlot
        {
            get; set;
        }

        public NodeEditorWindow()
        {
            this.resizeIcon = new GUIContent((Texture)NodeEditorWindow.ResizeHandle);
            this.minSize = new Vector2(500f, 200f);
            this.menuCommand = new MenuCommand(this, 0);
            SharedData.Instance.NodeEditorWindow = this;
        }

        [MenuItem("Window/Simplygon/Shading Network Setup")]
        public static void Init()
        {
            EditorWindow.GetWindow(typeof(NodeEditorWindow), false, "SN Setup");
        }

        public static void DrawNodeCurve(Rect start, Rect end)
        {
            Vector3 startPosition = new Vector3(start.x + start.width / 2f, start.y + start.height / 2f, 0.0f);
            Vector3 endPosition = new Vector3(end.x, end.y + end.height / 2f, 0.0f);
            Vector3 startTangent = startPosition + Vector3.right * 50f;
            Vector3 endTangent = endPosition + Vector3.left * 50f;
            Handles.DrawBezier(startPosition, endPosition, startTangent, endTangent, Color.black, NodeEditorWindow.BezierTexture, 1.5f);
        }

        public void InternalInit()
        {
            NodeEditorWindow.BezierTexture = ResourcesEx.LoadNodeBezierTexture();
            NodeEditorWindow.ResizeHandle = ResourcesEx.LoadNodeResizeHandle();
            this.mainWindowId = GUIUtility.GetControlID(FocusType.Passive);
            this.createNodeButtonStyle = new GUIStyle(EditorStyles.toolbarButton);
            this.createNodeButtonStyle.alignment = TextAnchor.MiddleLeft;
            this.createNodeButtonStyle.fixedHeight = 25f;
            this.currentShader = Shader.Find("Standard");
            if (!(null != this.currentShader))
                return;
            if (!SharedData.Instance.ShaderSettings.ContainsKey(JobUtils.GetShaderLookupName(this.currentShader.name)))
                SharedData.Instance.ShaderSettings.Add(JobUtils.GetShaderLookupName(this.currentShader.name), JobUtils.GetDefaultShadingChannelProperties(this.currentShader));
            this.currentShadingChannelPropertiesCollection = new List<ShadingChannelProperties>();
            foreach (ShadingChannelProperties channelProperties in SharedData.Instance.ShaderSettings[JobUtils.GetShaderLookupName(this.currentShader.name)])
                this.currentShadingChannelPropertiesCollection.Add(channelProperties.DeepCopy());
            this.OnSelectedShaderPopup((string)null, this.currentShader);
        }

        public void OnDestroy()
        {
        }

        public void OnSelectionChange()
        {
        }

        public void Update()
        {
            foreach (KeyValuePair<int, NodeWindow> nodeWindow in this.nodeWindows)
                nodeWindow.Value.Update();
            if (this.doRevertToDefault)
            {
                this.doRevertToDefault = false;
                List<ShadingChannelProperties> channelProperties1;
                if (!JobUtils.GetDefaultShaderSettings().TryGetValue(JobUtils.GetShaderLookupName(this.currentShader.name), out channelProperties1))
                    channelProperties1 = JobUtils.GetDefaultShadingChannelProperties(this.currentShader);
                this.currentShadingChannelPropertiesCollection.Clear();
                foreach (ShadingChannelProperties channelProperties2 in channelProperties1)
                    this.currentShadingChannelPropertiesCollection.Add(channelProperties2.DeepCopy());
                this.currentChannel = this.selectedChannel = 0;
                this.UpdateActiveChannels();
                this.ApplyCurrentShadingChannelProperties(this.activeChannels[this.currentChannel]);
                this.ArrangeNodeWindowsFromExitNode();
                this.ShowNotification(new GUIContent(string.Format(CultureInfo.InvariantCulture, "Reverted to default shading networks for shader '{0}'\n(click Save to persist changes)", (object)this.currentShader.name)));
            }
            if (this.currentChannel == this.selectedChannel)
                return;
            string activeChannel1 = this.activeChannels[this.currentChannel];
            string activeChannel2 = this.activeChannels[this.selectedChannel];
            this.SaveShadingChannelProperties(activeChannel1, true);
            this.ApplyCurrentShadingChannelProperties(activeChannel2);
            this.currentChannel = this.selectedChannel;
        }

        public void OnInspectorUpdate()
        {
        }

        public void OnGUI()
        {
            if (!this.isInitialized)
            {
                this.isInitialized = true;
                this.InternalInit();
            }
            Color backgroundColor = GUI.backgroundColor;
            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Shader", new GUILayoutOption[1]
            {
        GUILayout.Width(50f)
            });
            if (GUILayout.Button(null != this.currentShader ? this.currentShader.name : "<no shader selected>", EditorStyles.toolbarDropDown, new GUILayoutOption[0]))
            {
                Material material = new Material(this.currentShader == null ? Shader.Find("Standard") : this.currentShader);
                InternalEditorUtility.SetupShaderMenu(material);
                UnityEngine.Object.DestroyImmediate(material, false);
                EditorUtility.DisplayPopupMenu(new Rect(60f, 20f, 0.0f, 0.0f), "CONTEXT/ShaderPopup", this.menuCommand);
                this.UpdateActiveChannels();
            }
            if (!(null != this.currentShader))
                return;
            GUILayout.Space(7f);
            if (GUILayout.Button("Load Default", EditorStyles.miniButton, new GUILayoutOption[1]
            {
        GUILayout.Width(74f)
            }))
                this.doRevertToDefault = EditorUtility.DisplayDialog("Load Default Shading Network", string.Format(CultureInfo.InvariantCulture, "Load default shading network channels for shader '{0}'?", (object)this.currentShader.name), "Yes", "No");
            GUILayout.Space(2f);
            if (GUILayout.Button("Save", EditorStyles.miniButton, new GUILayoutOption[1]
            {
        GUILayout.Width(44f)
            }) && EditorUtility.DisplayDialog("Save Shading Network", string.Format(CultureInfo.InvariantCulture, "Save all shading network channels for shader '{0}'?", (object)this.currentShader.name), "Yes", "No"))
            {
                this.SaveShadingChannelProperties(this.activeChannels[this.currentChannel], true);
                SharedData.Instance.ShaderSettings[JobUtils.GetShaderLookupName(this.currentShader.name)] = new List<ShadingChannelProperties>((IEnumerable<ShadingChannelProperties>)this.currentShadingChannelPropertiesCollection);
                try
                {
                    File.WriteAllText(SharedData.Instance.ShaderSettingsPath, JsonConvert.SerializeObject((object)SharedData.Instance.ShaderSettings));
                    this.ShowNotification(new GUIContent(string.Format(CultureInfo.InvariantCulture, "Saved shading network for shader '{0}'", (object)this.currentShader.name)));
                }
                catch (Exception ex)
                {
                    string text = string.Format(CultureInfo.InvariantCulture, "FAILED to save shading network for shader '{0}' to {1} ({2})", (object)this.currentShader.name, (object)SharedData.Instance.ShaderSettingsPath, (object)ex.Message);
                    Debug.LogWarning((object)text);
                    this.ShowNotification(new GUIContent(text));
                }
            }
            GUI.backgroundColor = backgroundColor;
            GUILayout.EndHorizontal();
            GUILayout.Space(15f);
            EditorGUILayout.BeginHorizontal();
            this.channelsScrollPosition = EditorGUILayout.BeginScrollView(this.channelsScrollPosition);
            this.selectedChannel = GUILayout.Toolbar(this.currentChannel, this.activeChannels);
            EditorGUILayout.EndScrollView();
            GUILayout.Space(3f);
            int index = EditorGUILayout.Popup(this.currentSelectedChannelIndexToAdd, this.addableChannels, new GUILayoutOption[1]
            {
        GUILayout.Width(120f)
            });
            if (index != this.currentSelectedChannelIndexToAdd)
            {
                this.SaveShadingChannelProperties(this.addableChannels[index], false);
                this.UpdateActiveChannels();
            }
            EditorGUILayout.EndHorizontal();
            GUILayout.Space(5f);
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();
            GUILayout.Box(string.Empty, new GUILayoutOption[2]
            {
        GUILayout.ExpandWidth(true),
        GUILayout.ExpandHeight(true)
            });
            if (string.Empty != this.currentNodeHelpText)
            {
                GUILayout.BeginHorizontal();
                GUI.backgroundColor = Color.red;
                if (GUILayout.Button("X", new GUILayoutOption[1]
                {
          GUILayout.Width(24f)
                }))
                    this.currentNodeHelpText = string.Empty;
                GUI.backgroundColor = backgroundColor;
                EditorGUILayout.HelpBox(this.currentNodeHelpText, MessageType.Info);
                GUILayout.EndHorizontal();
            }
            GUILayout.Space(5f);
            GUILayout.EndVertical();
            this.NodeCreationGUI();
            GUILayout.EndHorizontal();
            NodeWindow nodeWindow1;
            this.nodeWindows.TryGetValue(this.currentlySelectedNodeWindowId, out nodeWindow1);
            foreach (KeyValuePair<int, NodeWindow> nodeWindow2 in this.nodeWindows)
            {
                NodeWindow nodeWindow3 = nodeWindow2.Value;
                if (nodeWindow3 != nodeWindow1)
                    this.DrawNodeWindowAuxiliaries(nodeWindow3);
            }
            this.BeginWindows();
            foreach (KeyValuePair<int, NodeWindow> nodeWindow2 in this.nodeWindows)
            {
                NodeWindow nodeWindow3 = nodeWindow2.Value;
                nodeWindow3.Window = GUILayout.Window(nodeWindow3.WindowId, nodeWindow3.Window, new GUI.WindowFunction(this.DrawNodeWindow), nodeWindow3.Title);
            }
            this.EndWindows();
            if (nodeWindow1 != null)
                this.DrawNodeWindowAuxiliaries(nodeWindow1);
            if (Event.current.rawType == EventType.MouseUp && GUIUtility.hotControl != this.mainWindowId)
                GUIUtility.hotControl = 0;
            if (this.nodeWindowsToClose.Count <= 0)
                return;
            foreach (NodeWindow nodeWindow2 in this.nodeWindowsToClose)
            {
                nodeWindow2.Close();
                this.nodeWindows.Remove(nodeWindow2.WindowId);
            }
            this.nodeWindowsToClose.Clear();
            this.UpdateCurrentExitNode();
        }

        public void HandleSlotSelected(Slot selectedSlot)
        {
            if (this.SelectedSlot == null)
            {
                this.SelectedSlot = selectedSlot;
            }
            else
            {
                this.SelectedSlot.HandleSlotConnected(selectedSlot);
                selectedSlot.HandleSlotConnected(this.SelectedSlot);
                this.SelectedSlot = (Slot)null;
                this.UpdateCurrentExitNode();
            }
        }

        public Rect GetCurrentWindow()
        {
            return this.position;
        }

        private void UpdateActiveChannels()
        {
            this.activeChannels = this.currentShadingChannelPropertiesCollection.Select<ShadingChannelProperties, string>((Func<ShadingChannelProperties, string>)(property => property.ChannelName)).ToArray<string>();
            List<string> stringList = new List<string>();
            stringList.AddRange((IEnumerable<string>)((IEnumerable<string>)this.SupportedChannels).Where<string>((Func<string, bool>)(channel =>
         {
             if (!channel.StartsWith(this.CustomChannelName))
                 return !((IEnumerable<string>)this.activeChannels).ToList<string>().Contains(channel);
             return false;
         })).OrderBy<string, string>((Func<string, string>)(channel => channel)));
            stringList.AddRange((IEnumerable<string>)((IEnumerable<string>)this.SupportedChannels).Where<string>((Func<string, bool>)(channel =>
         {
             if (channel.StartsWith(this.CustomChannelName))
                 return !((IEnumerable<string>)this.activeChannels).ToList<string>().Contains(channel);
             return false;
         })).OrderBy<string, string>((Func<string, string>)(channel => channel)));
            if (stringList.Count > 0)
                stringList.Insert(0, "Add channel");
            else
                stringList.Add("No channels available");
            this.addableChannels = stringList.ToArray();
        }

        private void UpdateCurrentExitNode()
        {
            List<NodeWindow> basicNodes = this.GetBasicNodes();
            List<NodeWindow> modifierNodes = this.GetModifierNodes();
            List<NodeWindow> possibleExitNodes;
            if (modifierNodes.Count > 0)
            {
                bool flag = true;
                while (flag)
                {
                    foreach (NodeWindow nodeWindow in modifierNodes)
                    {
                        NodeWindow modifierNode = nodeWindow;
                        flag = modifierNode.InputSlots.Exists((Predicate<SlotInput>)(inputSlot => inputSlot.ConnectedSlots.Select<Slot, NodeWindow>((Func<Slot, NodeWindow>)(slot => slot.Parent)).Any<NodeWindow>((Func<NodeWindow, bool>)(parent => parent.NodeRef > modifierNode.NodeRef))));
                        if (flag)
                        {
                            modifierNode.NodeRef = this.GetNextModifierNodeRef();
                            break;
                        }
                    }
                }
                possibleExitNodes = modifierNodes;
            }
            else
                possibleExitNodes = basicNodes;
            this.nodeWindows.Values.ToList<NodeWindow>().ForEach((Action<NodeWindow>)(node => node.IsExitNode = false));
            if (possibleExitNodes.Count <= 0)
                return;
            possibleExitNodes.Find((Predicate<NodeWindow>)(node => node.NodeRef == possibleExitNodes.Max<NodeWindow>((Func<NodeWindow, int>)(maxNode => maxNode.NodeRef)))).IsExitNode = true;
        }

        private List<NodeWindow> GetBasicNodes()
        {
            return this.nodeWindows.Values.Where<NodeWindow>((Func<NodeWindow, bool>)(node =>
           {
               if (!(node is NodeWindowShaderProperty))
                   return node is NodeWindowColor;
               return true;
           })).ToList<NodeWindow>();
        }

        private List<NodeWindow> GetModifierNodes()
        {
            return this.nodeWindows.Values.Except<NodeWindow>(this.nodeWindows.Values.Where<NodeWindow>((Func<NodeWindow, bool>)(node =>
           {
               if (!(node is NodeWindowShaderProperty))
                   return node is NodeWindowColor;
               return true;
           }))).ToList<NodeWindow>();
        }

        private int GetNextBasicNodeRef()
        {
            List<NodeWindow> basicNodes = this.GetBasicNodes();
            List<NodeWindow> modifierNodes = this.GetModifierNodes();
            int nextBasicNodeRef = basicNodes.Count > 0 ? basicNodes.Max<NodeWindow>((Func<NodeWindow, int>)(node => node.NodeRef)) + 1 : 0;
            if (modifierNodes.Exists((Predicate<NodeWindow>)(node => node.NodeRef == nextBasicNodeRef)))
                modifierNodes.ForEach((Action<NodeWindow>)(node => ++node.NodeRef));
            return nextBasicNodeRef;
        }

        private int GetNextModifierNodeRef()
        {
            List<NodeWindow> basicNodes = this.GetBasicNodes();
            List<NodeWindow> modifierNodes = this.GetModifierNodes();
            if (modifierNodes.Count > 0)
                return modifierNodes.Max<NodeWindow>((Func<NodeWindow, int>)(node => node.NodeRef)) + 1;
            if (basicNodes.Count <= 0)
                return 0;
            return basicNodes.Max<NodeWindow>((Func<NodeWindow, int>)(node => node.NodeRef)) + 1;
        }

        private void SaveShadingChannelProperties(string channelName, bool saveWindowsData = true)
        {
            List<BasicShadingNodeProperties> basicNodePropertiesCollection = new List<BasicShadingNodeProperties>();
            List<ModifierShadingNodeProperties> modifierNodePropertiesCollection = new List<ModifierShadingNodeProperties>();
            if (saveWindowsData)
            {
                List<NodeWindow> list = this.nodeWindows.Values.OrderBy<NodeWindow, int>((Func<NodeWindow, int>)(nodeWindow => nodeWindow.NodeRef)).ToList<NodeWindow>();
                for (int index = 0; index < list.Count; ++index)
                {
                    NodeWindow nodeWindow = list[index];
                    nodeWindow.Save(basicNodePropertiesCollection);
                    nodeWindow.Save(modifierNodePropertiesCollection);
                }
            }
            ShadingChannelProperties channelProperties = this.currentShadingChannelPropertiesCollection.Find((Predicate<ShadingChannelProperties>)(properties => properties.ChannelName == channelName));
            if (channelProperties == null)
            {
                channelProperties = new ShadingChannelProperties(channelName, new List<BasicShadingNodeProperties>(), new List<ModifierShadingNodeProperties>());
                this.currentShadingChannelPropertiesCollection.Add(channelProperties);
            }
            channelProperties.BasicShadingNodesProperties = basicNodePropertiesCollection;
            channelProperties.ModifierShadingNodesProperties = modifierNodePropertiesCollection;
        }

        private void RemoveShadingChannelProperties(string channelName)
        {
            ShadingChannelProperties channelProperties = this.currentShadingChannelPropertiesCollection.Find((Predicate<ShadingChannelProperties>)(properties => properties.ChannelName == channelName));
            if (channelProperties == null)
                return;
            this.currentShadingChannelPropertiesCollection.Remove(channelProperties);
        }

        private void DrawNodeWindowAuxiliaries(NodeWindow nodeWindow)
        {
            Color backgroundColor = GUI.backgroundColor;
            Rect window = nodeWindow.Window;
            double num1 = (double)window.xMax - (double)this.ToolbarButtonSize.x * (nodeWindow.IsCloseable ? 2.0 : 1.0);
            window = nodeWindow.Window;
            double num2 = (double)window.yMin - (double)this.ToolbarButtonSize.y;
            double x1 = (double)this.ToolbarButtonSize.x;
            double y1 = (double)this.ToolbarButtonSize.y;
            if (GUI.Button(new Rect((float)num1, (float)num2, (float)x1, (float)y1), "?", EditorStyles.miniButton))
                this.currentNodeHelpText = nodeWindow.HelpText;
            if (nodeWindow.IsCloseable)
            {
                GUI.backgroundColor = Color.red;
                window = nodeWindow.Window;
                double num3 = (double)window.xMax - (double)this.ToolbarButtonSize.x;
                window = nodeWindow.Window;
                double num4 = (double)window.yMin - (double)this.ToolbarButtonSize.y;
                double x2 = (double)this.ToolbarButtonSize.x;
                double y2 = (double)this.ToolbarButtonSize.y;
                if (GUI.Button(new Rect((float)num3, (float)num4, (float)x2, (float)y2), "X", EditorStyles.miniButton))
                    this.nodeWindowsToClose.Add(nodeWindow);
                GUI.backgroundColor = backgroundColor;
            }
            foreach (SlotOutput outputSlot in nodeWindow.OutputSlots)
            {
                foreach (Slot connectedSlot in outputSlot.ConnectedSlots)
                    NodeEditorWindow.DrawNodeCurve(outputSlot.Rect, connectedSlot.Rect);
                outputSlot.Draw(nodeWindow.Window);
            }
            foreach (Slot inputSlot in nodeWindow.InputSlots)
                inputSlot.Draw(nodeWindow.Window);
            if (!nodeWindow.IsExitNode)
                return;
            GUI.Box(new Rect(nodeWindow.Window.xMin + 2f, nodeWindow.Window.yMax + 4f, nodeWindow.Window.width - 4f, 20f), "EXIT NODE");
        }

        private void DrawNodeWindow(int id)
        {
            if (GUIUtility.hotControl == 0)
                this.handleActive = false;
            if ((Event.current.button != 0 ? 0 : (Event.current.type == EventType.MouseUp ? 1 : (Event.current.type == EventType.MouseDown ? 1 : 0))) != 0)
                this.currentlySelectedNodeWindowId = id;
            NodeWindow nodeWindow1 = this.nodeWindows[id];
            nodeWindow1.DrawContents(this.position);
            if (nodeWindow1.IsResizable)
            {
                double num1 = 1.0;
                Rect window = nodeWindow1.Window;
                double num2 = (double)window.height - 16.0;
                window = nodeWindow1.Window;
                double num3 = (double)window.width - 3.0;
                double num4 = 14.0;
                GUILayout.BeginArea(new Rect((float)num1, (float)num2, (float)num3, (float)num4));
                GUILayout.BeginHorizontal(EditorStyles.toolbarTextField, GUILayout.ExpandWidth(true));
                GUILayout.FlexibleSpace();
                this.handleArea = GUILayoutUtility.GetRect(this.resizeIcon, GUIStyle.none);
                GUI.DrawTexture(new Rect(this.handleArea.xMin + 6f, this.handleArea.yMin - 3f, 20f, 20f), (Texture)NodeEditorWindow.ResizeHandle);
                if (!this.handleActive & (Event.current.type == EventType.MouseDown || Event.current.type == EventType.MouseDrag) && this.handleArea.Contains((Vector3)Event.current.mousePosition, true))
                {
                    this.handleActive = true;
                    GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                }
                EditorGUIUtility.AddCursorRect(this.handleArea, MouseCursor.ResizeUpLeft);
                GUILayout.EndHorizontal();
                GUILayout.EndArea();
                if (this.handleActive && Event.current.type == EventType.MouseDrag)
                {
                    window = nodeWindow1.Window;
                    float num5 = window.width + Event.current.delta.x;
                    window = nodeWindow1.Window;
                    float num6 = window.height + Event.current.delta.y;
                    float num7 = (double)num5 <= (double)nodeWindow1.MinSize.x || (double)num5 >= (double)nodeWindow1.MaxSize.x ? 0.0f : Event.current.delta.x;
                    float num8 = (double)num6 <= (double)nodeWindow1.MinSize.y || (double)num6 >= (double)nodeWindow1.MaxSize.y ? 0.0f : Event.current.delta.y;
                    NodeWindow nodeWindow2 = nodeWindow1;
                    window = nodeWindow1.Window;
                    double xMin = (double)window.xMin;
                    window = nodeWindow1.Window;
                    double yMin = (double)window.yMin;
                    window = nodeWindow1.Window;
                    double num9 = (double)window.width + (double)num7;
                    window = nodeWindow1.Window;
                    double num10 = (double)window.height + (double)num8;
                    Rect rect = new Rect((float)xMin, (float)yMin, (float)num9, (float)num10);
                    nodeWindow2.Window = rect;
                    this.Repaint();
                }
            }
            if (!nodeWindow1.IsDraggable || this.handleActive)
                return;
            GUI.DragWindow();
        }

        private void OnSelectedShaderPopup(string command, Shader shader)
        {
            this.currentChannel = this.selectedChannel = 0;
            this.RemoveNotification();
            if (!(shader != null))
                return;
            this.currentShader = shader;
            if (!SharedData.Instance.ShaderSettings.ContainsKey(JobUtils.GetShaderLookupName(shader.name)))
                SharedData.Instance.ShaderSettings.Add(JobUtils.GetShaderLookupName(shader.name), JobUtils.GetDefaultShadingChannelProperties(shader));
            List<ShadingChannelProperties> channelPropertiesList = new List<ShadingChannelProperties>();
            foreach (ShadingChannelProperties channelProperties in SharedData.Instance.ShaderSettings[JobUtils.GetShaderLookupName(this.currentShader.name)])
                channelPropertiesList.Add(channelProperties.DeepCopy());
            this.currentShadingChannelPropertiesCollection = channelPropertiesList;
            this.currentShaderProperties.Clear();
            for (int propertyIdx = 0; propertyIdx < ShaderUtil.GetPropertyCount(this.currentShader); ++propertyIdx)
            {
                switch (ShaderUtil.GetPropertyType(this.currentShader, propertyIdx))
                {
                    case ShaderUtil.ShaderPropertyType.Color:
                        if (!this.currentShaderProperties.ContainsKey(BasicShadingNodeType.Color))
                            this.currentShaderProperties.Add(BasicShadingNodeType.Color, new List<string>());
                        this.currentShaderProperties[BasicShadingNodeType.Color].Add(ShaderUtil.GetPropertyName(this.currentShader, propertyIdx));
                        break;
                    case ShaderUtil.ShaderPropertyType.Vector:
                        if (!this.currentShaderProperties.ContainsKey(BasicShadingNodeType.Vector))
                            this.currentShaderProperties.Add(BasicShadingNodeType.Vector, new List<string>());
                        this.currentShaderProperties[BasicShadingNodeType.Vector].Add(ShaderUtil.GetPropertyName(this.currentShader, propertyIdx));
                        break;
                    case ShaderUtil.ShaderPropertyType.Float:
                    case ShaderUtil.ShaderPropertyType.Range:
                        if (!this.currentShaderProperties.ContainsKey(BasicShadingNodeType.Float))
                            this.currentShaderProperties.Add(BasicShadingNodeType.Float, new List<string>());
                        this.currentShaderProperties[BasicShadingNodeType.Float].Add(ShaderUtil.GetPropertyName(this.currentShader, propertyIdx));
                        break;
                    case ShaderUtil.ShaderPropertyType.TexEnv:
                        if (!this.currentShaderProperties.ContainsKey(BasicShadingNodeType.Texture))
                            this.currentShaderProperties.Add(BasicShadingNodeType.Texture, new List<string>());
                        this.currentShaderProperties[BasicShadingNodeType.Texture].Add(ShaderUtil.GetPropertyName(this.currentShader, propertyIdx));
                        break;
                }
            }
            this.ApplyCurrentShadingChannelProperties(this.activeChannels[this.currentChannel]);
            this.UpdateActiveChannels();
        }

        private void ApplyCurrentShadingChannelProperties(string channelName)
        {
            this.ClearNodeWindows();
            foreach (ShadingChannelProperties channelProperties in this.currentShadingChannelPropertiesCollection.FindAll((Predicate<ShadingChannelProperties>)(property => property.ChannelName == channelName)))
            {
                foreach (BasicShadingNodeProperties shadingNodesProperty in channelProperties.BasicShadingNodesProperties)
                {
                    int selectedShaderPropertyIndex = -1;
                    bool flag = shadingNodesProperty.ShaderPropertyName.StartsWith(Exporter.ShadingNetworkBuilder.GetExplicitColorAsPropertyName());
                    if (flag || this.currentShaderProperties.ContainsKey(shadingNodesProperty.NodeType))
                    {
                        List<string> stringList;
                        if (this.currentShaderProperties.TryGetValue(shadingNodesProperty.NodeType, out stringList))
                            selectedShaderPropertyIndex = stringList.IndexOf(shadingNodesProperty.ShaderPropertyName);
                        if (flag || -1 != selectedShaderPropertyIndex)
                        {
                            switch (shadingNodesProperty.NodeType)
                            {
                                case BasicShadingNodeType.Color:
                                    NodeWindow nodeWindow1;
                                    if (flag)
                                    {
                                        Color fromPropertyName = Exporter.ShadingNetworkBuilder.GetExplicitColorFromPropertyName(shadingNodesProperty.ShaderPropertyName);
                                        nodeWindow1 = (NodeWindow)new NodeWindowColor(shadingNodesProperty.NodeRef, fromPropertyName);
                                    }
                                    else
                                        nodeWindow1 = (NodeWindow)new NodeWindowColorProperty(shadingNodesProperty.NodeRef, selectedShaderPropertyIndex, stringList.ToArray());
                                    this.nodeWindows.Add(nodeWindow1.WindowId, nodeWindow1);
                                    continue;
                                case BasicShadingNodeType.Texture:
                                    NodeWindow nodeWindow2 = (NodeWindow)new NodeWindowTextureProperty(shadingNodesProperty.NodeRef, selectedShaderPropertyIndex, stringList.ToArray(), shadingNodesProperty.TexCoords);
                                    this.nodeWindows.Add(nodeWindow2.WindowId, nodeWindow2);
                                    continue;
                                case BasicShadingNodeType.Float:
                                    NodeWindow nodeWindow3 = (NodeWindow)new NodeWindowFloatRangeProperty(shadingNodesProperty.NodeRef, selectedShaderPropertyIndex, stringList.ToArray());
                                    this.nodeWindows.Add(nodeWindow3.WindowId, nodeWindow3);
                                    continue;
                                case BasicShadingNodeType.Vector:
                                    NodeWindow nodeWindow4;
                                    if (flag)
                                    {
                                        Vector4 fromPropertyName = Exporter.ShadingNetworkBuilder.GetExplicitColorVectorFromPropertyName(shadingNodesProperty.ShaderPropertyName);
                                        nodeWindow4 = (NodeWindow)new NodeWindowVector(shadingNodesProperty.NodeRef, fromPropertyName);
                                    }
                                    else
                                        nodeWindow4 = (NodeWindow)new NodeWindowVectorProperty(shadingNodesProperty.NodeRef, selectedShaderPropertyIndex, stringList.ToArray());
                                    this.nodeWindows.Add(nodeWindow4.WindowId, nodeWindow4);
                                    continue;
                                default:
                                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected basic node type '{0}' in current shader properties.", (object)shadingNodesProperty.NodeType));
                                    continue;
                            }
                        }
                        else
                            Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unable to find Shader {0} property '{1}' as specified by the {2} shading network.", (object)shadingNodesProperty.NodeType, (object)shadingNodesProperty.ShaderPropertyName, (object)channelName));
                    }
                    else if (BasicShadingNodeType.VertexColor == shadingNodesProperty.NodeType)
                    {
                        NodeWindow nodeWindow = (NodeWindow)new NodeWindowVertexColor(shadingNodesProperty.NodeRef);
                        this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                    }
                    else
                        Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unable to find any Shader {0} properties for shader '{1}' when trying to get desired shader property '{2}'.", (object)shadingNodesProperty.NodeType, (object)this.currentShader.name, (object)shadingNodesProperty.ShaderPropertyName));
                }
                foreach (ModifierShadingNodeProperties shadingNodesProperty in channelProperties.ModifierShadingNodesProperties)
                {
                    NodeWindow inputNodeWindow = (NodeWindow)null;
                    switch (shadingNodesProperty.NodeType)
                    {
                        case ModifierShadingNodeType.Interpolate:
                            inputNodeWindow = (NodeWindow)new NodeWindowInterpolate(shadingNodesProperty.NodeRef);
                            break;
                        case ModifierShadingNodeType.Clamp:
                            inputNodeWindow = (NodeWindow)new NodeWindowClamp(shadingNodesProperty.NodeRef);
                            break;
                        case ModifierShadingNodeType.Add:
                            inputNodeWindow = (NodeWindow)new NodeWindowAdd(shadingNodesProperty.NodeRef);
                            break;
                        case ModifierShadingNodeType.Subtract:
                            inputNodeWindow = (NodeWindow)new NodeWindowSubtract(shadingNodesProperty.NodeRef);
                            break;
                        case ModifierShadingNodeType.Multiply:
                            inputNodeWindow = (NodeWindow)new NodeWindowMultiply(shadingNodesProperty.NodeRef);
                            break;
                        case ModifierShadingNodeType.Divide:
                            inputNodeWindow = (NodeWindow)new NodeWindowDivide(shadingNodesProperty.NodeRef);
                            break;
                        case ModifierShadingNodeType.Swizzle:
                            inputNodeWindow = (NodeWindow)new NodeWindowSwizzle(shadingNodesProperty.NodeRef);
                            break;
                        case ModifierShadingNodeType.Max:
                            inputNodeWindow = (NodeWindow)new NodeWindowMax(shadingNodesProperty.NodeRef);
                            break;
                        case ModifierShadingNodeType.Min:
                            inputNodeWindow = (NodeWindow)new NodeWindowMin(shadingNodesProperty.NodeRef);
                            break;
                        case ModifierShadingNodeType.Step:
                            inputNodeWindow = (NodeWindow)new NodeWindowStep(shadingNodesProperty.NodeRef);
                            break;
                        default:
                            Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected modifier node type '{0}' in current shader properties.", (object)shadingNodesProperty.NodeType));
                            break;
                    }
                    if (inputNodeWindow != null)
                    {
                        if (ModifierShadingNodeType.Swizzle == shadingNodesProperty.NodeType)
                        {
                            if (shadingNodesProperty.InputNodes.Length == 4 && shadingNodesProperty.NodeData.Length == 4)
                            {
                                int num = (shadingNodesProperty.InputNodes[0] != shadingNodesProperty.InputNodes[1] ? 0 : (shadingNodesProperty.InputNodes[1] == shadingNodesProperty.InputNodes[2] ? 1 : 0)) == 0 ? 0 : (shadingNodesProperty.NodeData[0] == 0 && shadingNodesProperty.NodeData[1] == 1 && shadingNodesProperty.NodeData[2] == 2 ? 1 : 0);
                                bool flag = num != 0 && shadingNodesProperty.InputNodes[2] == shadingNodesProperty.InputNodes[3] && shadingNodesProperty.NodeData[3] == 3;
                                if ((num | (flag ? 1 : 0)) != 0)
                                {
                                    int inputNode = shadingNodesProperty.InputNodes[0];
                                    SlotComponents slotComponents = flag ? SlotComponents.RGBA : SlotComponents.RGB;
                                    this.ConnectSlots(inputNode, inputNodeWindow, slotComponents, slotComponents);
                                    if (!flag)
                                        this.ConnectSlots(shadingNodesProperty.InputNodes[3], inputNodeWindow, SlotComponents.A, SlotComponents.A);
                                }
                                else
                                {
                                    for (int index = 0; index < shadingNodesProperty.InputNodes.Length; ++index)
                                        this.ConnectSlots(shadingNodesProperty.InputNodes[index], inputNodeWindow, (SlotComponents)index, (SlotComponents)shadingNodesProperty.NodeData[index]);
                                }
                            }
                            else
                                Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected number of input nodes or node data for node '{0}'.", (object)shadingNodesProperty.InputNodes.Length));
                        }
                        else if (inputNodeWindow.InputSlots.Count == shadingNodesProperty.InputNodes.Length)
                        {
                            for (int index = 0; index < shadingNodesProperty.InputNodes.Length; ++index)
                            {
                                SlotComponents outputSlotComponents = SlotComponents.RGBA;
                                if (inputNodeWindow.InputSlots.Count == shadingNodesProperty.NodeData.Length && -1 != shadingNodesProperty.NodeData[index])
                                    outputSlotComponents = (SlotComponents)shadingNodesProperty.NodeData[index];
                                this.ConnectSlots(shadingNodesProperty.InputNodes[index], inputNodeWindow, SlotComponents.RGBA, outputSlotComponents);
                            }
                        }
                        else
                            Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected number of input nodes ({0}) for node '{1}'.", (object)shadingNodesProperty.InputNodes.Length, (object)shadingNodesProperty.NodeType));
                        this.nodeWindows.Add(inputNodeWindow.WindowId, inputNodeWindow);
                    }
                }
            }
            this.UpdateCurrentExitNode();
            this.ArrangeNodeWindowsFromExitNode();
        }

        private void ConnectSlots(int outputNodeRef, NodeWindow inputNodeWindow, SlotComponents inputSlotComponents, SlotComponents outputSlotComponents)
        {
            NodeWindow nodeWindow = this.nodeWindows.Values.ToList<NodeWindow>().Find((Predicate<NodeWindow>)(node => node.NodeRef == outputNodeRef));
            if (nodeWindow == null)
                return;
            SlotInput slotInput = inputNodeWindow.InputSlots.Find((Predicate<SlotInput>)(slot =>
           {
               if (slot.Components == inputSlotComponents)
                   return slot.ConnectedSlots.Count == 0;
               return false;
           }));
            SlotOutput slotOutput = nodeWindow.OutputSlots.Find((Predicate<SlotOutput>)(slot => slot.Components == outputSlotComponents));
            if (slotInput != null && slotOutput != null)
            {
                slotOutput.HandleSlotConnected((Slot)slotInput);
                slotInput.HandleSlotConnected((Slot)slotOutput);
            }
            else if (slotInput == null)
                Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unable to find a free RGBA input for node '{0}'.", (object)inputNodeWindow.Title));
            else
                Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unable to find a suitable RGBA output from node '{0}' to node '{1}'.", (object)nodeWindow.Title, (object)inputNodeWindow.Title));
        }

        private void ClearNodeWindows()
        {
            this.nodeWindows.Clear();
            this.nodeWindowsToClose.Clear();
            this.currentlySelectedNodeWindowId = -1;
        }

        private void NodeCreationGUI()
        {
            Color backgroundColor = GUI.backgroundColor;
            float width = 120f;
            GUILayout.Space(2f);
            GUILayout.BeginVertical(GUILayout.Width(130f));
            int num = this.activeChannels[this.currentChannel] != Constants.Diffuse ? 1 : 0;
            GUILayout.Space(2f);
            if (num != 0)
            {
                if (GUILayout.Button("Remove channel", EditorStyles.miniButton, new GUILayoutOption[1]
                {
          GUILayout.Width(120f)
                }))
                {
                    string activeChannel = this.activeChannels[this.currentChannel];
                    if (EditorUtility.DisplayDialog("Remove Channel", string.Format(CultureInfo.InvariantCulture, "Remove channel '{0}' from the shading network of shader '{1}'?", (object)activeChannel, (object)this.currentShader.name), "Yes", "No"))
                    {
                        this.RemoveShadingChannelProperties(this.activeChannels[this.currentChannel]);
                        this.UpdateActiveChannels();
                        if (this.currentChannel >= this.activeChannels.Length)
                            this.currentChannel = this.activeChannels.Length - 1;
                        this.selectedChannel = this.currentChannel;
                        this.ApplyCurrentShadingChannelProperties(this.activeChannels[this.currentChannel]);
                        this.ShowNotification(new GUIContent(string.Format(CultureInfo.InvariantCulture, "Removed channel '{0}'.", (object)activeChannel)));
                    }
                }
            }
            GUI.backgroundColor = backgroundColor;
            this.scrollPositionNodeCreationGUI = GUILayout.BeginScrollView(this.scrollPositionNodeCreationGUI);
            GUILayout.Label("Property Nodes", EditorStyles.boldLabel, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            });
            GUI.backgroundColor = (Color)new Color32((byte)60, (byte)160, byte.MaxValue, byte.MaxValue);
            GUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Color Property", this.createNodeButtonStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
            {
                List<string> stringList;
                if (this.currentShaderProperties.TryGetValue(BasicShadingNodeType.Color, out stringList))
                {
                    NodeWindow nodeWindow = (NodeWindow)new NodeWindowColorProperty(this.GetNextBasicNodeRef(), 0, stringList.ToArray());
                    this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                }
                else
                    this.ShowNotification(new GUIContent(string.Format(CultureInfo.InvariantCulture, "Shader '{0}' has no color properties", (object)this.currentShader.name)));
                this.UpdateCurrentExitNode();
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Float/Range Property", this.createNodeButtonStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
            {
                List<string> stringList;
                if (this.currentShaderProperties.TryGetValue(BasicShadingNodeType.Float, out stringList))
                {
                    NodeWindow nodeWindow = (NodeWindow)new NodeWindowFloatRangeProperty(this.GetNextBasicNodeRef(), 0, stringList.ToArray());
                    this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                }
                else
                    this.ShowNotification(new GUIContent(string.Format(CultureInfo.InvariantCulture, "Shader '{0}' has no float/range properties", (object)this.currentShader.name)));
                this.UpdateCurrentExitNode();
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Texture Property", this.createNodeButtonStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
            {
                List<string> stringList;
                if (this.currentShaderProperties.TryGetValue(BasicShadingNodeType.Texture, out stringList))
                {
                    NodeWindow nodeWindow = (NodeWindow)new NodeWindowTextureProperty(this.GetNextBasicNodeRef(), 0, stringList.ToArray());
                    this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                }
                else
                    this.ShowNotification(new GUIContent(string.Format(CultureInfo.InvariantCulture, "Shader '{0}' has no texture properties", (object)this.currentShader.name)));
                this.UpdateCurrentExitNode();
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Vector Property", this.createNodeButtonStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
            {
                List<string> stringList;
                if (this.currentShaderProperties.TryGetValue(BasicShadingNodeType.Vector, out stringList))
                {
                    NodeWindow nodeWindow = (NodeWindow)new NodeWindowVectorProperty(this.GetNextBasicNodeRef(), 0, stringList.ToArray());
                    this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                }
                else
                    this.ShowNotification(new GUIContent(string.Format(CultureInfo.InvariantCulture, "Shader '{0}' has no vector properties", (object)this.currentShader.name)));
                this.UpdateCurrentExitNode();
            }
            GUILayout.EndHorizontal();
            GUILayout.Space(5f);
            GUILayout.Label("Basic Nodes", EditorStyles.boldLabel, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            });
            GUI.backgroundColor = Color.green;
            GUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Color", this.createNodeButtonStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
            {
                NodeWindow nodeWindow = (NodeWindow)new NodeWindowColor(this.GetNextBasicNodeRef(), Color.black);
                this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                this.UpdateCurrentExitNode();
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Vector", this.createNodeButtonStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
            {
                NodeWindow nodeWindow = (NodeWindow)new NodeWindowVector(this.GetNextBasicNodeRef(), Vector4.zero);
                this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                this.UpdateCurrentExitNode();
            }
            GUILayout.EndHorizontal();
            GUILayout.Space(5f);
            GUILayout.Label("Arithmetic Nodes", EditorStyles.boldLabel, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            });
            GUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Add", this.createNodeButtonStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
            {
                NodeWindow nodeWindow = (NodeWindow)new NodeWindowAdd(this.GetNextModifierNodeRef());
                this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                this.UpdateCurrentExitNode();
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Subtract", this.createNodeButtonStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
            {
                NodeWindow nodeWindow = (NodeWindow)new NodeWindowSubtract(this.GetNextModifierNodeRef());
                this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                this.UpdateCurrentExitNode();
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Multiply", this.createNodeButtonStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
            {
                NodeWindow nodeWindow = (NodeWindow)new NodeWindowMultiply(this.GetNextModifierNodeRef());
                this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                this.UpdateCurrentExitNode();
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Divide", this.createNodeButtonStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
            {
                NodeWindow nodeWindow = (NodeWindow)new NodeWindowDivide(this.GetNextModifierNodeRef());
                this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                this.UpdateCurrentExitNode();
            }
            GUILayout.EndHorizontal();
            GUILayout.Space(5f);
            GUILayout.Label("Special Nodes", EditorStyles.boldLabel, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            });
            GUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Vertex Color", this.createNodeButtonStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
            {
                NodeWindow nodeWindow = (NodeWindow)new NodeWindowVertexColor(this.GetNextBasicNodeRef());
                this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                this.UpdateCurrentExitNode();
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Clamp", this.createNodeButtonStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
            {
                NodeWindow nodeWindow = (NodeWindow)new NodeWindowClamp(this.GetNextModifierNodeRef());
                this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                this.UpdateCurrentExitNode();
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Interpolate", this.createNodeButtonStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
            {
                NodeWindow nodeWindow = (NodeWindow)new NodeWindowInterpolate(this.GetNextModifierNodeRef());
                this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                this.UpdateCurrentExitNode();
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Max", this.createNodeButtonStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
            {
                NodeWindow nodeWindow = (NodeWindow)new NodeWindowMax(this.GetNextModifierNodeRef());
                this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                this.UpdateCurrentExitNode();
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Min", this.createNodeButtonStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
            {
                NodeWindow nodeWindow = (NodeWindow)new NodeWindowMin(this.GetNextModifierNodeRef());
                this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                this.UpdateCurrentExitNode();
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Step", this.createNodeButtonStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
            {
                NodeWindow nodeWindow = (NodeWindow)new NodeWindowStep(this.GetNextModifierNodeRef());
                this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                this.UpdateCurrentExitNode();
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(5f);
            if (GUILayout.Button("Swizzle", this.createNodeButtonStyle, new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
            {
                NodeWindow nodeWindow = (NodeWindow)new NodeWindowSwizzle(this.GetNextModifierNodeRef());
                this.nodeWindows.Add(nodeWindow.WindowId, nodeWindow);
                this.UpdateCurrentExitNode();
            }
            GUILayout.EndHorizontal();
            GUI.backgroundColor = backgroundColor;
            GUILayout.Space(10f);
            GUILayout.FlexibleSpace();
            UIUtility.SeparatorGUI();
            GUILayout.Space(5f);
            GUILayout.Space(5f);
            if (GUILayout.Button("Arrange Nodes", new GUILayoutOption[1]
            {
        GUILayout.Width(width)
            }))
                this.ArrangeNodeWindowsFromExitNode();
            GUILayout.Space(10f);
            GUI.backgroundColor = backgroundColor;
            GUILayout.EndScrollView();
            GUILayout.EndVertical();
        }

        private void ArrangeNodeWindowsFromExitNode()
        {
            NodeWindow nodeWindow1 = this.nodeWindows.Values.ToList<NodeWindow>().Find((Predicate<NodeWindow>)(node => node.IsExitNode));
            if (nodeWindow1 != null)
            {
                foreach (NodeWindow nodeWindow2 in this.nodeWindows.Values)
                    nodeWindow2.Window = new Rect((float)((double)this.position.width - (double)nodeWindow2.MinSize.x - 160.0), 90f, nodeWindow2.Window.width, nodeWindow2.Window.height);
                this.ArrangeNodeWindows(new List<NodeWindow>()
        {
          nodeWindow1
        }, 0);
            }
            this.Repaint();
        }

        private void ArrangeNodeWindows(List<NodeWindow> nodeWindows, int column)
        {
            float num1 = this.position.height / (float)(nodeWindows.Count + 1);
            for (int index = 0; index < nodeWindows.Count; ++index)
            {
                NodeWindow nodeWindow1 = nodeWindows[index];
                double num2 = (double)num1 * (double)(index + 1);
                Rect rect1 = nodeWindow1.Window;
                double num3 = (double)rect1.height * 0.5;
                float num4 = (float)(num2 - num3);
                rect1 = this.position;
                double width1 = (double)rect1.width;
                rect1 = nodeWindow1.Window;
                double num5 = ((double)rect1.width + 100.0) * (double)column;
                float num6 = (float)(width1 - num5 - 300.0);
                NodeWindow nodeWindow2 = nodeWindow1;
                double num7 = (double)num6;
                double num8 = (double)num4;
                rect1 = nodeWindow1.Window;
                double width2 = (double)rect1.width;
                rect1 = nodeWindow1.Window;
                double height = (double)rect1.height;
                Rect rect2 = new Rect((float)num7, (float)num8, (float)width2, (float)height);
                nodeWindow2.Window = rect2;
                nodeWindow1.SetWindowWithinBounds(SharedData.Instance.NodeEditorWindow.GetCurrentWindow());
            }
            List<NodeWindow> list = nodeWindows.SelectMany<NodeWindow, SlotInput>((Func<NodeWindow, IEnumerable<SlotInput>>)(node => (IEnumerable<SlotInput>)node.InputSlots)).SelectMany<SlotInput, Slot>((Func<SlotInput, IEnumerable<Slot>>)(inputSlot => (IEnumerable<Slot>)inputSlot.ConnectedSlots)).Select<Slot, NodeWindow>((Func<Slot, NodeWindow>)(connectedSlot => connectedSlot.Parent)).Distinct<NodeWindow>().ToList<NodeWindow>();
            if (list.Count <= 0)
                return;
            this.ArrangeNodeWindows(list, ++column);
        }
    }
}
