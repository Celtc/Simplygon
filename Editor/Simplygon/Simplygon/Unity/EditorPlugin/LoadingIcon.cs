﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.LoadingIcon
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
  public class LoadingIcon
  {
    private DateTime updateTime;

    public Texture2D CurrentLoadIcon
    {
      get
      {
        return ResourcesEx.LoadLoadingIcon(this.CurrentIndex);
      }
    }

    public int CurrentIndex { get; private set; }

    public LoadingIcon()
    {
      this.updateTime = DateTime.UtcNow;
    }

    public void Update()
    {
      double totalMilliseconds = (DateTime.UtcNow - this.updateTime).TotalMilliseconds;
      while (totalMilliseconds > 0.0)
      {
        if (totalMilliseconds > 100.0)
        {
          this.CurrentIndex = (this.CurrentIndex + 1) % 12;
          this.updateTime = DateTime.UtcNow;
        }
        totalMilliseconds -= 100.0;
      }
    }
  }
}
