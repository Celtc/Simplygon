﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.PrefabUtilityEx
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.Globalization;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class PrefabUtilityEx
    {
        public static List<PrefabEx> GetPrefabsForSelection(List<GameObject> gameObjects)
        {
            List<PrefabEx> source = new List<PrefabEx>();
            try
            {
                foreach (GameObject gameObject in gameObjects)
                {
                    GameObject prefabForGameObject = PrefabUtility.FindPrefabRoot(gameObject);
                    if (source.Where<PrefabEx>((Func<PrefabEx, bool>)(p => p.Prefab == prefabForGameObject)).SingleOrDefault<PrefabEx>() == null)
                    {
                        int triangleCount = 0;
                        Dictionary<int, int> uniqueBones = new Dictionary<int, int>();
                        PrefabUtilityEx.GetPrefabStats(prefabForGameObject, ref triangleCount, ref uniqueBones);
                        if (triangleCount > 0)
                        {
                            PrefabEx prefabEx = new PrefabEx(prefabForGameObject, triangleCount, uniqueBones.Count);
                            source.Add(prefabEx);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log((object)string.Format(CultureInfo.InvariantCulture, "Simplygon exception: {0}", (object)ex.Message));
            }
            return source;
        }

        public static GameObject CreateCloneForGameObjects(string rootName, List<PrefabEx> prefabs)
        {
            GameObject dstGameObject1 = (GameObject)null;
            try
            {
                if (prefabs.Count > 1)
                {
                    Vector3 zero = Vector3.zero;
                    foreach (PrefabEx prefab in prefabs)
                        zero += prefab.Prefab.transform.localPosition;
                    Vector3 vector3 = zero / (float)prefabs.Count;
                    dstGameObject1 = new GameObject();
                    dstGameObject1.name = rootName;
                    dstGameObject1.transform.localPosition = vector3;
                    foreach (PrefabEx prefab in prefabs)
                    {
                        GameObject dstGameObject2 = UnityEngine.Object.Instantiate<GameObject>(prefab.Prefab, prefab.Prefab.transform.localPosition, prefab.Prefab.transform.localRotation);
                        dstGameObject2.transform.parent = dstGameObject1.transform;
                        dstGameObject2.name = prefab.Prefab.name;
                        dstGameObject2.hideFlags = HideFlags.None;
                        PrefabUtilityEx.DeepCopy(prefab.Prefab, dstGameObject2, (List<Mesh>)null, (List<Avatar>)null);
                    }
                }
                else
                {
                    Quaternion rotation = Quaternion.identity;
                    if (prefabs[0].BoneCount == 0)
                        rotation = prefabs[0].Prefab.transform.localRotation;
                    dstGameObject1 = UnityEngine.Object.Instantiate<GameObject>(prefabs[0].Prefab, Vector3.zero, rotation);
                    dstGameObject1.name = rootName;
                    dstGameObject1.hideFlags = HideFlags.None;
                    if (prefabs[0].BoneCount > 0)
                        dstGameObject1.transform.localScale = Vector3.one;
                    PrefabUtilityEx.DeepCopy(prefabs[0].Prefab, dstGameObject1, (List<Mesh>)null, (List<Avatar>)null);
                }
            }
            catch (Exception ex)
            {
                Debug.Log((object)string.Format(CultureInfo.InvariantCulture, "Simplygon exception: {0}", (object)ex.Message));
            }
            return dstGameObject1;
        }

        public static string GetPendingPrefabPath(string jobId, string name, int lodIndex)
        {
            return string.Format(CultureInfo.InvariantCulture, "Assets/LODs/PendingLODs/{0}/{1}_LOD{2}.prefab", (object)jobId, (object)name, (object)lodIndex);
        }

        public static GameObject GetPendingPrefab(string jobId, string name, int lodIndex)
        {
            return AssetDatabase.LoadAssetAtPath(PrefabUtilityEx.GetPendingPrefabPath(jobId, name, lodIndex), typeof(GameObject)) as GameObject;
        }

        public static void SavePrefab(string prefabPath, GameObject gameObject)
        {
            GameObject prefab = PrefabUtility.CreatePrefab(prefabPath, gameObject, ReplacePrefabOptions.Default);
            List<Mesh> createdMeshes = new List<Mesh>();
            List<Avatar> createdAvatars = new List<Avatar>();
            PrefabUtilityEx.DeepCopy(gameObject, prefab, createdMeshes, createdAvatars);
            foreach (var objectToAdd in createdMeshes)
                AssetDatabase.AddObjectToAsset(objectToAdd, prefab);
            foreach (var objectToAdd in createdAvatars)
                AssetDatabase.AddObjectToAsset(objectToAdd, prefab);
            EditorUtility.SetDirty(prefab);
        }

        private static void DeepCopy(GameObject srcGameObject, GameObject dstGameObject, List<Mesh> createdMeshes, List<Avatar> createdAvatars)
        {
            SkinnedMeshRenderer component1 = srcGameObject.GetComponent(typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer;
            SkinnedMeshRenderer component2 = dstGameObject.GetComponent(typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer;
            if (component1 != null && component2 != null && component1.sharedMesh != null)
            {
                Mesh sharedMesh = component1.sharedMesh;
                Mesh mesh = new Mesh();
                Mesh dstMesh = mesh;
                PrefabUtilityEx.DeepCopy(sharedMesh, dstMesh);
                component2.sharedMesh = mesh;
                if (createdMeshes != null)
                    createdMeshes.Add(mesh);
            }
            Animator component3 = srcGameObject.GetComponent(typeof(Animator)) as Animator;
            Animator component4 = dstGameObject.GetComponent(typeof(Animator)) as Animator;
            if (component3 != null && component4 != null && component3.avatar != null)
            {
                Avatar avatar1 = component3.avatar;
                Avatar avatar2 = UnityEngine.Object.Instantiate<Avatar>(avatar1);
                avatar2.name = avatar1.name;
                component4.avatar = avatar2;
                if (createdAvatars != null)
                    createdAvatars.Add(avatar2);
            }
            MeshFilter component5 = srcGameObject.GetComponent(typeof(MeshFilter)) as MeshFilter;
            MeshFilter component6 = dstGameObject.GetComponent(typeof(MeshFilter)) as MeshFilter;
            if (component5 != null && component6 != null && component5.sharedMesh != null)
            {
                Mesh sharedMesh = component5.sharedMesh;
                Mesh mesh = new Mesh();
                Mesh dstMesh = mesh;
                PrefabUtilityEx.DeepCopy(sharedMesh, dstMesh);
                component6.sharedMesh = mesh;
                if (createdMeshes != null)
                    createdMeshes.Add(mesh);
            }
            for (int index = 0; index < srcGameObject.transform.childCount; ++index)
                PrefabUtilityEx.DeepCopy(srcGameObject.transform.GetChild(index).gameObject, dstGameObject.transform.GetChild(index).gameObject, createdMeshes, createdAvatars);
        }

        public static void DeepCopy(Mesh srcMesh, Mesh dstMesh)
        {
            dstMesh.Clear();
            if (srcMesh.vertices != null)
                dstMesh.vertices = (Vector3[])srcMesh.vertices.Clone();
            if (srcMesh.normals != null)
                dstMesh.normals = (Vector3[])srcMesh.normals.Clone();
            if (srcMesh.tangents != null)
                dstMesh.tangents = (Vector4[])srcMesh.tangents.Clone();
            if (srcMesh.uv != null)
                dstMesh.uv = (Vector2[])srcMesh.uv.Clone();
            if (srcMesh.uv2 != null)
                dstMesh.uv2 = (Vector2[])srcMesh.uv2.Clone();
            if (srcMesh.uv3 != null)
                dstMesh.uv3 = (Vector2[])srcMesh.uv3.Clone();
            if (srcMesh.uv4 != null)
                dstMesh.uv4 = (Vector2[])srcMesh.uv4.Clone();
            if (srcMesh.colors != null)
                dstMesh.colors = (Color[])srcMesh.colors.Clone();
            if (srcMesh.colors32 != null)
                dstMesh.colors32 = (Color32[])srcMesh.colors32.Clone();
            if (srcMesh.boneWeights != null)
                dstMesh.boneWeights = (BoneWeight[])srcMesh.boneWeights.Clone();
            if (srcMesh.bindposes != null)
                dstMesh.bindposes = (Matrix4x4[])srcMesh.bindposes.Clone();
            dstMesh.subMeshCount = srcMesh.subMeshCount;
            for (int submesh = 0; submesh < srcMesh.subMeshCount; ++submesh)
            {
                int[] triangles = srcMesh.GetTriangles(submesh);
                dstMesh.SetTriangles(triangles, submesh);
            }
            dstMesh.bounds = srcMesh.bounds;
            dstMesh.name = srcMesh.name;
        }

        public static void SetBindposes(SkinnedMeshRenderer skinnedMeshRenderer, Transform[] bones, Dictionary<Transform, Matrix4x4> boneToBindPoseMapping)
        {
            if (bones.Length == 0)
                return;
            Matrix4x4[] matrix4x4Array = new Matrix4x4[((IEnumerable<Transform>)bones).Count<Transform>()];
            List<Transform> transformList = new List<Transform>();
            for (int index = 0; index < bones.Length; ++index)
            {
                Transform bone = bones[index];
                Matrix4x4 matrix4x4 = bones[index].worldToLocalMatrix * skinnedMeshRenderer.transform.localToWorldMatrix;
                if (boneToBindPoseMapping != null && boneToBindPoseMapping.ContainsKey(bone))
                    matrix4x4 = boneToBindPoseMapping[bone];
                matrix4x4Array[index] = matrix4x4;
            }
            skinnedMeshRenderer.sharedMesh.bindposes = matrix4x4Array;
            skinnedMeshRenderer.bones = bones;
        }

        private static void GetPrefabStats(GameObject prefab, ref int triangleCount, ref Dictionary<int, int> uniqueBones)
        {
            SkinnedMeshRenderer component1 = prefab.GetComponent(typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer;
            if (component1 != null && component1.sharedMesh != null)
            {
                foreach (BoneWeight boneWeight in component1.sharedMesh.boneWeights)
                {
                    if (!uniqueBones.ContainsKey(boneWeight.boneIndex0) && (double)boneWeight.weight0 >= 0.0)
                        uniqueBones.Add(boneWeight.boneIndex0, boneWeight.boneIndex0);
                    if (!uniqueBones.ContainsKey(boneWeight.boneIndex1) && (double)boneWeight.weight1 >= 0.0)
                        uniqueBones.Add(boneWeight.boneIndex1, boneWeight.boneIndex1);
                    if (!uniqueBones.ContainsKey(boneWeight.boneIndex2) && (double)boneWeight.weight2 >= 0.0)
                        uniqueBones.Add(boneWeight.boneIndex2, boneWeight.boneIndex2);
                    if (!uniqueBones.ContainsKey(boneWeight.boneIndex3) && (double)boneWeight.weight3 >= 0.0)
                        uniqueBones.Add(boneWeight.boneIndex3, boneWeight.boneIndex3);
                }
                triangleCount += component1.sharedMesh.triangles.Length / 3;
            }
            MeshFilter component2 = prefab.GetComponent(typeof(MeshFilter)) as MeshFilter;
            if (component2 != null && component2.sharedMesh != null)
                triangleCount += component2.sharedMesh.triangles.Length / 3;
            foreach (Component component3 in prefab.transform)
                PrefabUtilityEx.GetPrefabStats(component3.gameObject, ref triangleCount, ref uniqueBones);
        }
    }
}
