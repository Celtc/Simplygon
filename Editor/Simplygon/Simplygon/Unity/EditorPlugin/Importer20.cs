﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.Importer20
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Scene.Asset;
using Simplygon.Scene.Common.Enums;
using Simplygon.Scene.Common.Progress;
using Simplygon.Scene.Common.WorkDirectory;
using Simplygon.Scene.FileFormat.SSF;
using Simplygon.Scene.Util;
using Simplygon.SPL.v80.Base;
using Simplygon.SPL.v80.Node;
using Simplygon.SPL.v80.Processor;
using Simplygon.Unity.EditorPlugin.Jobs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Globalization;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class Importer20
    {
        private List<GameObject> tempMeshHolders = new List<GameObject>();
        private WorkDirectoryInfo workDirectory;
        private Dictionary<UnityEngine.Mesh, Guid> meshToIdMapping;
        private Dictionary<Guid, UnityEngine.Mesh> idToMeshMapping;
        private Dictionary<Guid, Simplygon.Scene.Asset.Mesh> simplygonMeshes;
        private Dictionary<Guid, Simplygon.Scene.Asset.Material> simplygonMaterials;
        private List<List<int>> materialIndicesPerMesh;
        private Dictionary<Guid, Transform> BoneIdToBoneTransform;
        private Dictionary<UnityEngine.Mesh, Transform[]> MeshToBones;
        private Dictionary<SkinnedMeshRenderer, Dictionary<Transform, Matrix4x4>> boneToBindPoseMappingPerSkinnedMeshRenderer;
        private ImporterMaterial20 materialImporter;

        public string SettingsFilePath
        {
            get; private set;
        }

        private int CurrentLODIndex
        {
            get; set;
        }

        private Simplygon.Scene.Scene SimplygonScene
        {
            get; set;
        }

        private UnityCloudJob CurrentJob
        {
            get; set;
        }

        public bool IsAggregateLOD(int lodIndex)
        {
            int startIndex = 1;
            return this.IsProcessorEnabled<AggregationProcessor>((this.CurrentJob.CloudJob.SPL as Simplygon.SPL.v80.SPL).ProcessGraph, lodIndex, ref startIndex);
        }

        public bool IsProxyLOD(int lodIndex)
        {
            int startIndex = 1;
            return this.IsProcessorEnabled<RemeshingProcessor>((this.CurrentJob.CloudJob.SPL as Simplygon.SPL.v80.SPL).ProcessGraph, lodIndex, ref startIndex);
        }

        private bool IsProcessorEnabled<T>(BaseNode node, int lodIndex, ref int startIndex)
        {
            if (startIndex > lodIndex || node == null)
                return false;
            if (node is ProcessNode)
            {
                ProcessNode processNode = node as ProcessNode;
                if (lodIndex == startIndex && processNode.Processor is T)
                    return true;
                ++startIndex;
            }
            foreach (BaseNode child in node.Children)
            {
                if (this.IsProcessorEnabled<T>(child, lodIndex, ref startIndex))
                    return true;
            }
            return false;
        }

        public bool IsMaterialLODEnabled(int lodIndex)
        {
            return (this.CurrentJob.CloudJob.SPL as Simplygon.SPL.v80.SPL).IsMaterialLODEnabled(lodIndex);
        }

        internal Importer20(WorkDirectoryInfo workDirectory)
        {
            this.workDirectory = workDirectory;
        }

        internal void Import(UnityCloudJob job, List<string> lodFilePaths)
        {
            this.CurrentJob = job;
            AssetDatabaseEx.CreateLODFolders(job);
            this.CurrentLODIndex = 1;
            foreach (string lodFilePath in lodFilePaths)
            {
                string pendingPrefabPath = PrefabUtilityEx.GetPendingPrefabPath(job.CloudJob.JobCustomData.UnityPendingLODFolderName, job.CloudJob.Name, this.CurrentLODIndex);
                GameObject pendingPrefab = PrefabUtilityEx.GetPendingPrefab(job.CloudJob.JobCustomData.UnityPendingLODFolderName, job.CloudJob.Name, this.CurrentLODIndex);
                if (pendingPrefab != null)
                {
                    this.ImportLOD(lodFilePath, pendingPrefab);
                    EditorUtility.SetDirty(pendingPrefab);
                    string newPath = string.Format(CultureInfo.InvariantCulture, "{0}/{1}/{2}", (object)"Assets/LODs", (object)job.AssetDirectory, (object)Path.GetFileName(pendingPrefabPath));
                    AssetDatabase.CopyAsset(pendingPrefabPath, newPath);
                    ++this.CurrentLODIndex;
                }
            }
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        private void ImportLOD(string filename, GameObject prefab)
        {
            this.meshToIdMapping = new Dictionary<UnityEngine.Mesh, Guid>();
            this.idToMeshMapping = new Dictionary<Guid, UnityEngine.Mesh>();
            this.simplygonMeshes = new Dictionary<Guid, Simplygon.Scene.Asset.Mesh>();
            this.simplygonMaterials = new Dictionary<Guid, Simplygon.Scene.Asset.Material>();
            this.materialIndicesPerMesh = new List<List<int>>();
            this.BoneIdToBoneTransform = new Dictionary<Guid, Transform>();
            this.MeshToBones = new Dictionary<UnityEngine.Mesh, Transform[]>();
            this.boneToBindPoseMappingPerSkinnedMeshRenderer = new Dictionary<SkinnedMeshRenderer, Dictionary<Transform, Matrix4x4>>();
            this.materialImporter = (ImporterMaterial20)null;
            this.SimplygonScene = SSFReaderFactory.GetReader(filename).Read(filename, ColorSpaceType.NotDefined, MaterialModel.NotDefined, NormalMapType.TangentSpace_LeftHanded, (SceneProgress)null);
            this.SimplygonScene = SceneConverter.GetSceneCopyWithPackedGeometry(this.SimplygonScene);
            this.simplygonMeshes = this.SimplygonScene.Meshes;
            this.simplygonMaterials = this.SimplygonScene.Materials;
            this.BuildBoneMapping(this.SimplygonScene.RootNode, prefab);
            this.meshToIdMapping.Clear();
            this.idToMeshMapping.Clear();
            foreach (KeyValuePair<Guid, Simplygon.Scene.Asset.Mesh> mesh1 in this.SimplygonScene.Meshes)
            {
                UnityEngine.Mesh mesh2 = this.CreateMesh(mesh1.Value);
                this.meshToIdMapping.Add(mesh2, mesh1.Key);
                this.idToMeshMapping.Add(mesh1.Key, mesh2);
                GameObject gameObject = new GameObject();
                gameObject.hideFlags = HideFlags.HideAndDontSave;
                (gameObject.AddComponent(typeof(MeshFilter)) as MeshFilter).mesh = mesh2;
                this.tempMeshHolders.Add(gameObject);
            }
            if (this.IsMaterialLODEnabled(this.CurrentLODIndex))
            {
                this.materialImporter = new ImporterMaterial20();
                this.materialImporter.Import(this.SimplygonScene, this.CurrentJob);
            }
            if (this.IsProxyLOD(this.CurrentLODIndex) || this.IsAggregateLOD(this.CurrentLODIndex))
            {
                this.RemoveMeshes(prefab, true);
                prefab.transform.rotation = UnityEngine.Quaternion.Euler(0.0f, 0.0f, 0.0f);
                prefab.transform.localScale = new UnityEngine.Vector3(1f, 1f, 1f);
                this.ProxifyPrefab(this.SimplygonScene.RootNode, prefab);
            }
            else
                this.UpdatePrefabMeshes(this.SimplygonScene.RootNode, prefab);
            if (!this.IsMaterialLODEnabled(this.CurrentLODIndex))
                return;
            this.materialImporter.SaveMaterialsToAssetDatabase();
        }

        private void BuildBoneMapping(Simplygon.Scene.Graph.Node sceneNode, GameObject prefab)
        {
            if (sceneNode.IsBone)
                this.BoneIdToBoneTransform.Add(sceneNode.Id, prefab.transform);
            SkinnedMeshRenderer component = prefab.GetComponent(typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer;
            if (component != null && component.sharedMesh != null)
            {
                if (!this.boneToBindPoseMappingPerSkinnedMeshRenderer.ContainsKey(component))
                    this.boneToBindPoseMappingPerSkinnedMeshRenderer.Add(component, new Dictionary<Transform, Matrix4x4>());
                for (int index = 0; index < component.bones.Length; ++index)
                {
                    if (!this.boneToBindPoseMappingPerSkinnedMeshRenderer[component].ContainsKey(component.bones[index]))
                        this.boneToBindPoseMappingPerSkinnedMeshRenderer[component].Add(component.bones[index], component.sharedMesh.bindposes[index]);
                }
            }
            for (int index1 = 0; index1 < sceneNode.Children.Count; ++index1)
            {
                Simplygon.Scene.Graph.Node node = this.SimplygonScene.Nodes[sceneNode.Children[index1]];
                GameObject prefab1 = (GameObject)null;
                for (int index2 = 0; index2 < prefab.transform.childCount; ++index2)
                {
                    if (prefab.transform.GetChild(index2).gameObject.name == node.Name)
                        prefab1 = prefab.transform.GetChild(index2).gameObject;
                }
                if (prefab1 != null)
                    this.BuildBoneMapping(node, prefab1);
            }
        }

        private void UpdatePrefabMeshes(Simplygon.Scene.Graph.Node sceneNode, GameObject prefab)
        {
            Guid meshId = sceneNode.MeshId;
            if (meshId != Guid.Empty)
            {
                UnityEngine.Mesh srcMesh = this.idToMeshMapping[meshId];
                if (srcMesh.triangles.Length == 0)
                {
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon warning: MeshLOD reduced to zero triangles for {0}_LOD{1}", (object)this.CurrentJob.CloudJob.Name, (object)this.CurrentLODIndex));
                }
                else
                {
                    SkinnedMeshRenderer component1 = prefab.GetComponent(typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer;
                    if (component1 != null && component1.sharedMesh != null)
                    {
                        PrefabUtilityEx.DeepCopy(srcMesh, component1.sharedMesh);
                        PrefabUtilityEx.SetBindposes(component1, this.MeshToBones[srcMesh], this.boneToBindPoseMappingPerSkinnedMeshRenderer[component1]);
                        component1.sharedMesh.RecalculateBounds();
                        Simplygon.Scene.Asset.Mesh mesh = this.SimplygonScene.Meshes[meshId];
                        if (this.IsMaterialLODEnabled(this.CurrentLODIndex) && mesh.Materials.Count > 0)
                        {
                            UnityEngine.Material[] materialArray = new UnityEngine.Material[component1.sharedMaterials.Length];
                            for (int index = 0; index < component1.sharedMaterials.Length; ++index)
                            {
                                UnityEngine.Material srcMaterial = component1.sharedMaterials[index];
                                List<UnityEngine.Material> list = ((IEnumerable<UnityEngine.Material>)component1.sharedMaterials).Where<UnityEngine.Material>((Func<UnityEngine.Material, bool>)(material => material != srcMaterial)).ToList<UnityEngine.Material>();
                                Simplygon.Scene.Asset.Material simplygonMaterial = this.simplygonMaterials[mesh.Materials.First<Guid>()];
                                materialArray[index] = this.materialImporter.CreateMaterial(simplygonMaterial, srcMaterial, this.CurrentLODIndex, list);
                            }
                            component1.sharedMaterials = materialArray;
                        }
                    }
                    MeshFilter component2 = prefab.GetComponent(typeof(MeshFilter)) as MeshFilter;
                    if (component2 != null && component2.sharedMesh != null)
                    {
                        PrefabUtilityEx.DeepCopy(srcMesh, component2.sharedMesh);
                        component2.sharedMesh.RecalculateBounds();
                    }
                    MeshRenderer component3 = prefab.GetComponent(typeof(MeshRenderer)) as MeshRenderer;
                    if (component3 != null)
                    {
                        Simplygon.Scene.Asset.Mesh mesh = this.SimplygonScene.Meshes[meshId];
                        if (this.IsMaterialLODEnabled(this.CurrentLODIndex) && mesh.Materials.Count > 0)
                        {
                            UnityEngine.Material[] materialArray = new UnityEngine.Material[component3.sharedMaterials.Length];
                            for (int index = 0; index < component3.sharedMaterials.Length; ++index)
                            {
                                UnityEngine.Material srcMaterial = component3.sharedMaterials[index];
                                List<UnityEngine.Material> list = ((IEnumerable<UnityEngine.Material>)component3.sharedMaterials).Where<UnityEngine.Material>((Func<UnityEngine.Material, bool>)(material => material != srcMaterial)).ToList<UnityEngine.Material>();
                                Simplygon.Scene.Asset.Material simplygonMaterial = this.simplygonMaterials[mesh.Materials.First<Guid>()];
                                materialArray[index] = this.materialImporter.CreateMaterial(simplygonMaterial, srcMaterial, this.CurrentLODIndex, list);
                            }
                            component3.sharedMaterials = materialArray;
                        }
                    }
                }
            }
            for (int index1 = 0; index1 < sceneNode.Children.Count; ++index1)
            {
                Simplygon.Scene.Graph.Node node = this.SimplygonScene.Nodes[sceneNode.Children[index1]];
                GameObject prefab1 = (GameObject)null;
                for (int index2 = 0; index2 < prefab.transform.childCount; ++index2)
                {
                    if (prefab.transform.GetChild(index2).gameObject.name == node.Name)
                        prefab1 = prefab.transform.GetChild(index2).gameObject;
                }
                if (prefab1 != null)
                    this.UpdatePrefabMeshes(node, prefab1);
            }
        }

        private void ProxifyPrefab(Simplygon.Scene.Graph.Node sceneNode, GameObject rootPrefab)
        {
            if (sceneNode.Name.ToLower().Contains(string.Format(CultureInfo.InvariantCulture, "proxy_{0}", (object)sceneNode.Id)))
            {
                Guid meshId = sceneNode.MeshId;
                if (!(meshId != Guid.Empty))
                    return;
                UnityEngine.Mesh index = this.idToMeshMapping[meshId];
                if (index.triangles.Length == 0)
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon warning: ProxyLOD reduced to zero triangles for {0}_LOD{1}", (object)this.CurrentJob.CloudJob.Name, (object)this.CurrentLODIndex));
                else if (index.boneWeights.Length != 0)
                {
                    SkinnedMeshRenderer skinnedMeshRenderer = rootPrefab.GetComponent<SkinnedMeshRenderer>();
                    if (skinnedMeshRenderer == null)
                        skinnedMeshRenderer = rootPrefab.AddComponent<SkinnedMeshRenderer>();
                    if (!(skinnedMeshRenderer != null))
                        return;
                    Transform[] meshToBone = this.MeshToBones[index];
                    skinnedMeshRenderer.rootBone = this.GetRootBone(meshToBone);
                    Dictionary<Transform, Matrix4x4> dictionary = new Dictionary<Transform, Matrix4x4>();
                    foreach (KeyValuePair<SkinnedMeshRenderer, Dictionary<Transform, Matrix4x4>> keyValuePair1 in this.boneToBindPoseMappingPerSkinnedMeshRenderer)
                    {
                        foreach (KeyValuePair<Transform, Matrix4x4> keyValuePair2 in keyValuePair1.Value)
                        {
                            if (!dictionary.ContainsKey(keyValuePair2.Key))
                                dictionary.Add(keyValuePair2.Key, keyValuePair2.Value);
                        }
                    }
                    skinnedMeshRenderer.sharedMesh = index;
                    skinnedMeshRenderer.sharedMesh.RecalculateBounds();
                    PrefabUtilityEx.SetBindposes(skinnedMeshRenderer, meshToBone, (Dictionary<Transform, Matrix4x4>)null);
                    if (this.IsMaterialLODEnabled(this.CurrentLODIndex))
                    {
                        UnityEngine.Material material = (UnityEngine.Material)null;
                        if (this.SimplygonScene.Materials.Count > 0)
                            material = this.materialImporter.CreateMaterial(this.SimplygonScene.Materials.First<KeyValuePair<Guid, Simplygon.Scene.Asset.Material>>().Value, (UnityEngine.Material)null, this.CurrentLODIndex, new List<UnityEngine.Material>());
                        skinnedMeshRenderer.sharedMaterial = material;
                    }
                    AssetDatabase.AddObjectToAsset(index, rootPrefab);
                }
                else
                {
                    MeshFilter meshFilter = rootPrefab.GetComponent<MeshFilter>();
                    if (meshFilter == null)
                        meshFilter = rootPrefab.AddComponent<MeshFilter>();
                    if (meshFilter != null)
                    {
                        meshFilter.sharedMesh = index;
                        meshFilter.sharedMesh.RecalculateBounds();
                        AssetDatabase.AddObjectToAsset(index, rootPrefab);
                    }
                    MeshRenderer meshRenderer = rootPrefab.GetComponent<MeshRenderer>();
                    if (meshRenderer == null)
                        meshRenderer = rootPrefab.AddComponent<MeshRenderer>();
                    if (meshRenderer != null && this.IsMaterialLODEnabled(this.CurrentLODIndex))
                    {
                        UnityEngine.Material material = (UnityEngine.Material)null;
                        if (this.SimplygonScene.Materials.Count > 0)
                            material = this.materialImporter.CreateMaterial(this.SimplygonScene.Materials.First<KeyValuePair<Guid, Simplygon.Scene.Asset.Material>>().Value, (UnityEngine.Material)null, this.CurrentLODIndex, new List<UnityEngine.Material>());
                        meshRenderer.sharedMaterial = material;
                    }
                    MeshCollider meshCollider = rootPrefab.GetComponent<MeshCollider>();
                    if (meshCollider == null)
                        meshCollider = rootPrefab.AddComponent<MeshCollider>();
                    if (!(meshCollider != null))
                        return;
                    meshCollider.sharedMesh = index;
                }
            }
            else
            {
                for (int index = 0; index < sceneNode.Children.Count; ++index)
                    this.ProxifyPrefab(this.SimplygonScene.Nodes[sceneNode.Children[index]], rootPrefab);
            }
        }

        private Transform GetRootBone(Transform[] bones)
        {
            if (bones.Length == 0)
                return (Transform)null;
            Transform parent = bones[0];
            for (int index = 0; index < bones.Length; ++index)
            {
                Transform bone = bones[index];
                if (!bone.IsChildOf(parent) && parent.IsChildOf(bone))
                {
                    parent = bone;
                    index = 0;
                }
            }
            return parent;
        }

        private void RemoveMeshes(GameObject prefab, bool recursive)
        {
            SkinnedMeshRenderer component1 = prefab.GetComponent(typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer;
            if (component1 != null)
            {
                if (component1.sharedMesh != null)
                    UnityEngine.Object.DestroyImmediate(component1.sharedMesh, true);
                UnityEngine.Object.DestroyImmediate(component1, true);
            }
            MeshFilter component2 = prefab.GetComponent(typeof(MeshFilter)) as MeshFilter;
            if (component2 != null)
            {
                if (component2.sharedMesh != null)
                    UnityEngine.Object.DestroyImmediate(component2.sharedMesh, true);
                UnityEngine.Object.DestroyImmediate(component2, true);
            }
            MeshRenderer component3 = prefab.GetComponent(typeof(MeshRenderer)) as MeshRenderer;
            if (component3 != null)
                UnityEngine.Object.DestroyImmediate(component3, true);
            MeshCollider component4 = prefab.GetComponent(typeof(MeshCollider)) as MeshCollider;
            if (component4 != null)
                UnityEngine.Object.DestroyImmediate(component4, true);
            if (prefab.name.ToLower().StartsWith("proxy_"))
            {
                UnityEngine.Object.DestroyImmediate(prefab, true);
            }
            else
            {
                if (!recursive)
                    return;
                for (int index = 0; index < prefab.transform.childCount; ++index)
                    this.RemoveMeshes(prefab.transform.GetChild(index).gameObject, recursive);
            }
        }

        private UnityEngine.Mesh CreateMesh(Simplygon.Scene.Asset.Mesh simplygonMesh)
        {
            UnityEngine.Mesh mesh = new UnityEngine.Mesh();
            MeshData meshSource = simplygonMesh.MeshSources[0];
            mesh.Clear();
            if (this.IsProxyLOD(this.CurrentLODIndex) || this.IsAggregateLOD(this.CurrentLODIndex))
                mesh.name = string.Format(CultureInfo.InvariantCulture, "{0}_ProxyMesh_LOD{1}", (object)this.CurrentJob.CloudJob.Name, (object)this.CurrentLODIndex);
            else
                mesh.name = string.Format(CultureInfo.InvariantCulture, "{0}_LOD{1}", (object)simplygonMesh.Name, (object)this.CurrentLODIndex);
            int index1 = 0;
            if (meshSource.Coordinates.Length != 0)
            {
                UnityEngine.Vector3[] vector3Array = new UnityEngine.Vector3[meshSource.Coordinates.Length];
                foreach (Simplygon.Math.Vector4 coordinate in meshSource.Coordinates)
                {
                    vector3Array[index1] = new UnityEngine.Vector3((float)coordinate.X, (float)coordinate.Y, (float)coordinate.Z);
                    ++index1;
                }
                mesh.vertices = vector3Array;
            }
            List<int> source = new List<int>();
            int num1 = 1000;
            foreach (int materialIndex1 in meshSource.MaterialIndices)
            {
                int materialIndex = materialIndex1;
                if (source.Where<int>((Func<int, bool>)(i => i == materialIndex)).ToList<int>().Count == 0)
                {
                    source.Add(materialIndex);
                    if (materialIndex < num1)
                        num1 = materialIndex;
                }
            }
            this.materialIndicesPerMesh.Add(source);
            if (source.Count <= 1)
            {
                int index2 = 0;
                if (meshSource.Triangles.Length != 0)
                {
                    int[] numArray = new int[meshSource.Triangles.Length];
                    foreach (int triangle in meshSource.Triangles)
                    {
                        numArray[index2] = triangle;
                        ++index2;
                    }
                    mesh.triangles = numArray;
                }
            }
            else
            {
                List<int>[] intListArray = new List<int>[source.Count];
                for (int index2 = 0; index2 < intListArray.Length; ++index2)
                    intListArray[index2] = new List<int>();
                mesh.subMeshCount = source.Count;
                int index3 = 0;
                while (index3 < meshSource.Triangles.Length)
                {
                    int materialIndex = meshSource.MaterialIndices[index3 / 3];
                    int index2 = source.IndexOf(materialIndex);
                    intListArray[index2].Add(meshSource.Triangles[index3]);
                    intListArray[index2].Add(meshSource.Triangles[index3 + 1]);
                    intListArray[index2].Add(meshSource.Triangles[index3 + 2]);
                    index3 += 3;
                }
                for (int submesh = 0; submesh < intListArray.Length; ++submesh)
                    mesh.SetTriangles(intListArray[submesh].ToArray(), submesh);
            }
            int maxNumUvs = SceneUtils.GetMaxNumUVs();
            if (meshSource.TexCoords.Count > 0)
            {
                if (meshSource.TexCoords.Keys.Contains<string>("MaterialLOD"))
                {
                    Simplygon.Math.Vector2[] texCoord = meshSource.TexCoords["MaterialLOD"];
                    UnityEngine.Vector2[] vector2Array = new UnityEngine.Vector2[texCoord.Length];
                    int index2 = 0;
                    foreach (Simplygon.Math.Vector2 vector2 in texCoord)
                    {
                        vector2Array[index2] = new UnityEngine.Vector2((float)vector2.X, 1f - (float)vector2.Y);
                        ++index2;
                    }
                    mesh.uv = vector2Array;
                }
                else
                {
                    foreach (KeyValuePair<string, Simplygon.Math.Vector2[]> texCoord in meshSource.TexCoords)
                    {
                        int result;
                        if (texCoord.Key.Contains("TexCoords") && int.TryParse(texCoord.Key.Remove(0, 9), out result) && result < maxNumUvs)
                        {
                            UnityEngine.Vector2[] vector2Array = new UnityEngine.Vector2[texCoord.Value.Length];
                            int index2 = 0;
                            foreach (Simplygon.Math.Vector2 vector2 in texCoord.Value)
                            {
                                vector2Array[index2] = new UnityEngine.Vector2((float)vector2.X, 1f - (float)vector2.Y);
                                ++index2;
                            }
                            mesh.SetUVsWrapper(result, ((IEnumerable<UnityEngine.Vector2>)vector2Array).ToList<UnityEngine.Vector2>());
                        }
                    }
                }
            }
            int index4 = 0;
            if (meshSource.Normals.Length != 0)
            {
                UnityEngine.Vector3[] vector3Array = new UnityEngine.Vector3[meshSource.Normals.Length];
                foreach (Simplygon.Math.Vector3 normal in meshSource.Normals)
                {
                    vector3Array[index4] = new UnityEngine.Vector3((float)normal.X, (float)normal.Y, (float)normal.Z);
                    ++index4;
                }
                mesh.normals = vector3Array;
            }
            int index5 = 0;
            if (meshSource.Tangents.Length != 0 && meshSource.Tangents.Length == meshSource.Normals.Length && meshSource.Bitangents.Length == meshSource.Bitangents.Length)
            {
                UnityEngine.Vector4[] vector4Array = new UnityEngine.Vector4[meshSource.Tangents.Length];
                for (int index2 = 0; index2 < meshSource.Tangents.Length; ++index2)
                {
                    Simplygon.Math.Vector3 normal = meshSource.Normals[index2];
                    Simplygon.Math.Vector3 tangent = meshSource.Tangents[index2];
                    float w = Simplygon.Math.Vector3.Dot(meshSource.Bitangents[index2], Simplygon.Math.Vector3.Cross(normal, tangent)) <= 0.9 ? -1f : 1f;
                    vector4Array[index5] = new UnityEngine.Vector4((float)tangent.X, (float)tangent.Y, (float)tangent.Z, w);
                    ++index5;
                }
                mesh.tangents = vector4Array;
            }
            if (meshSource.VertexColors.Count > 0)
            {
                foreach (KeyValuePair<string, Simplygon.Math.Color[]> vertexColor in meshSource.VertexColors)
                {
                    if (vertexColor.Key == "Colors")
                    {
                        UnityEngine.Color[] colorArray = new UnityEngine.Color[vertexColor.Value.Length];
                        int index2 = 0;
                        foreach (Simplygon.Math.Color color in vertexColor.Value)
                        {
                            colorArray[index2] = new UnityEngine.Color(color.R, color.G, color.B, color.A);
                            ++index2;
                        }
                        mesh.colors = colorArray;
                    }
                }
            }
            if (simplygonMesh.Bones.Count > 0)
            {
                Transform[] transformArray = new Transform[simplygonMesh.Bones.Count];
                for (int index2 = 0; index2 < simplygonMesh.Bones.Count; ++index2)
                    transformArray[index2] = this.BoneIdToBoneTransform[simplygonMesh.Bones[index2]];
                this.MeshToBones.Add(mesh, transformArray);
            }
            if (meshSource.BoneIndices != null && meshSource.BoneWeights != null)
            {
                int index2 = 0;
                if (meshSource.BoneIndices.Length > 0 && meshSource.BoneWeights.Length > 0)
                {
                    if (meshSource.BoneIndices.Length != meshSource.BoneWeights.Length)
                        throw new Exception("Bone indices and bone weights are not the same size");
                    int length = meshSource.BoneIndices.GetLength(1);
                    BoneWeight[] boneWeightArray = new BoneWeight[meshSource.VertexCount];
                    for (int index3 = 0; index3 < meshSource.VertexCount; ++index3)
                    {
                        BoneWeight boneWeight = new BoneWeight();
                        double num2 = 0.0;
                        if (length > 0)
                        {
                            int boneIndex = meshSource.BoneIndices[index3, 0];
                            if (boneIndex >= 0)
                            {
                                boneWeight.boneIndex0 = boneIndex;
                                boneWeight.weight0 = meshSource.BoneWeights[index3, 0];
                            }
                            if (boneWeight.boneIndex0 >= 0)
                                num2 += (double)boneWeight.weight0;
                        }
                        if (length > 1)
                        {
                            int boneIndex = meshSource.BoneIndices[index3, 1];
                            if (boneIndex >= 0)
                            {
                                boneWeight.boneIndex1 = boneIndex;
                                boneWeight.weight1 = meshSource.BoneWeights[index3, 1];
                            }
                            if (boneWeight.boneIndex1 >= 0)
                                num2 += (double)boneWeight.weight1;
                        }
                        if (length > 2)
                        {
                            int boneIndex = meshSource.BoneIndices[index3, 2];
                            if (boneIndex >= 0)
                            {
                                boneWeight.boneIndex2 = boneIndex;
                                boneWeight.weight2 = meshSource.BoneWeights[index3, 2];
                            }
                            if (boneWeight.boneIndex2 >= 0)
                                num2 += (double)boneWeight.weight2;
                        }
                        if (length > 3)
                        {
                            int boneIndex = meshSource.BoneIndices[index3, 3];
                            if (boneIndex >= 0)
                            {
                                boneWeight.boneIndex3 = boneIndex;
                                boneWeight.weight3 = meshSource.BoneWeights[index3, 3];
                            }
                            if (boneWeight.boneIndex3 >= 0)
                                num2 += (double)boneWeight.weight3;
                        }
                        if (length > 0)
                            boneWeight.weight0 /= (float)num2;
                        if (length > 1)
                            boneWeight.weight1 /= (float)num2;
                        if (length > 2)
                            boneWeight.weight2 /= (float)num2;
                        if (length > 3)
                            boneWeight.weight3 /= (float)num2;
                        boneWeightArray[index2] = boneWeight;
                        ++index2;
                    }
                    mesh.boneWeights = boneWeightArray;
                }
            }
            mesh.RecalculateBounds();
            return mesh;
        }
    }
}
