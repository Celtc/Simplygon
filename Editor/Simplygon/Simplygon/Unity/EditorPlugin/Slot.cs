﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.Slot
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.Collections.Generic;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
  public abstract class Slot
  {
    public SlotComponents Components { get; protected set; }

    public NodeWindow Parent { get; protected set; }

    public bool IsEnabled { get; set; }

    public List<Slot> ConnectedSlots { get; protected set; }

    public Rect Rect { get; protected set; }

    public Slot(SlotComponents components, NodeWindow parent)
    {
      this.Components = components;
      this.Parent = parent;
      this.IsEnabled = true;
      this.ConnectedSlots = new List<Slot>();
    }

    public static Color GetSlotComponentsColor(SlotComponents components)
    {
      Color grey = Color.grey;
      Color color;
      switch (components)
      {
        case SlotComponents.R:
          color = Color.red;
          break;
        case SlotComponents.G:
          color = Color.green;
          break;
        case SlotComponents.B:
          color = Color.blue;
          break;
        case SlotComponents.A:
          color = Color.white;
          break;
        case SlotComponents.RGB:
          color = Color.white;
          break;
        case SlotComponents.RGBA:
          color = Color.white;
          break;
        default:
          color = Color.grey;
          break;
      }
      return color;
    }

    public static List<SlotComponents> GetComplementSlotRGBComponents(SlotComponents slotComponents)
    {
      List<SlotComponents> slotComponentsList = new List<SlotComponents>();
      switch (slotComponents)
      {
        case SlotComponents.R:
          slotComponentsList.Add(SlotComponents.G);
          slotComponentsList.Add(SlotComponents.B);
          break;
        case SlotComponents.G:
          slotComponentsList.Add(SlotComponents.R);
          slotComponentsList.Add(SlotComponents.B);
          break;
        case SlotComponents.B:
          slotComponentsList.Add(SlotComponents.R);
          slotComponentsList.Add(SlotComponents.G);
          break;
      }
      return slotComponentsList;
    }

    public static List<SlotComponents> GetComplementSlotRGBAComponents(SlotComponents slotComponents)
    {
      List<SlotComponents> slotComponentsList = new List<SlotComponents>();
      switch (slotComponents)
      {
        case SlotComponents.R:
          slotComponentsList.Add(SlotComponents.G);
          slotComponentsList.Add(SlotComponents.B);
          slotComponentsList.Add(SlotComponents.A);
          break;
        case SlotComponents.G:
          slotComponentsList.Add(SlotComponents.R);
          slotComponentsList.Add(SlotComponents.B);
          slotComponentsList.Add(SlotComponents.A);
          break;
        case SlotComponents.B:
          slotComponentsList.Add(SlotComponents.R);
          slotComponentsList.Add(SlotComponents.G);
          slotComponentsList.Add(SlotComponents.A);
          break;
        case SlotComponents.A:
          slotComponentsList.Add(SlotComponents.R);
          slotComponentsList.Add(SlotComponents.G);
          slotComponentsList.Add(SlotComponents.B);
          break;
      }
      return slotComponentsList;
    }

    public void Disconnect()
    {
      foreach (Slot connectedSlot in this.ConnectedSlots)
        connectedSlot.ConnectedSlots.Remove(this);
      this.ConnectedSlots.Clear();
    }

    public bool AreSlotsCompatible(Slot slot)
    {
      return this.Parent != slot.Parent & true;
    }

    public void HandleSlotConnected(Slot otherSlot)
    {
      if (this.ConnectedSlots.Contains(otherSlot) || !this.AreSlotsCompatible(otherSlot))
        return;
      this.InternalHandleSlotConnected(otherSlot);
    }

    public abstract void Draw(Rect parent);

    protected abstract void InternalHandleSlotConnected(Slot otherSlot);
  }
}
