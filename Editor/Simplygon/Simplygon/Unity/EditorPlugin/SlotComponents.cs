﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.SlotComponents
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Unity.EditorPlugin
{
  public enum SlotComponents
  {
    R,
    G,
    B,
    A,
    RGB,
    RGBA,
  }
}
