﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.SlotInput
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
  public class SlotInput : Slot
  {
    private int slotNumber;
    private Vector2 interSlotsUiOffset;
    private Vector2 slotsUiOffset;

    public SlotInput(int slotNumber, SlotComponents components, NodeWindow parent, Vector2 interSlotsUiOffset, Vector2 slotsUiOffset)
      : base(components, parent)
    {
      this.slotNumber = slotNumber;
      this.interSlotsUiOffset = interSlotsUiOffset;
      this.slotsUiOffset = slotsUiOffset;
    }

    public override void Draw(Rect parent)
    {
      if (!this.IsEnabled)
        return;
      Color backgroundColor = GUI.backgroundColor;
      GUI.backgroundColor = SharedData.Instance.NodeEditorWindow.SelectedSlot == this ? Color.yellow : Slot.GetSlotComponentsColor(this.Components);
      Rect position = new Rect((float) ((double) parent.xMin + (double) this.interSlotsUiOffset.x - 18.0), parent.yMin + this.interSlotsUiOffset.y * (float) this.slotNumber + this.slotsUiOffset.y, 20f, 16f);
      this.Rect = position;
      if (GUI.Button(position, ">"))
      {
        if (Event.current.button == 0)
          SharedData.Instance.NodeEditorWindow.HandleSlotSelected((Slot) this);
        else if (Event.current.button == 1 && this.ConnectedSlots.Count > 0)
        {
          this.ConnectedSlots.ElementAt<Slot>(this.ConnectedSlots.Count - 1).ConnectedSlots.Remove((Slot) this);
          this.ConnectedSlots.RemoveAt(this.ConnectedSlots.Count - 1);
          this.Parent.InputSlots.ForEach((Action<SlotInput>) (slot => slot.UpdateEnabledStatus()));
        }
      }
      GUI.backgroundColor = backgroundColor;
    }

    public void UpdateEnabledStatus()
    {
      if (this.ConnectedSlots.Count > 0)
        this.DisableParentInputSlots();
      else
        this.IsEnabled = true;
    }

    public bool GetConnectedNodeInfo(out int connectedNodeRef, out SlotComponents connectedNodeOuputSlotComponents)
    {
      connectedNodeRef = -1;
      connectedNodeOuputSlotComponents = SlotComponents.RGBA;
      if (this.ConnectedSlots.Count > 0 && this.ConnectedSlots[0] != null)
      {
        connectedNodeRef = this.ConnectedSlots[0].Parent.NodeRef;
        connectedNodeOuputSlotComponents = this.ConnectedSlots[0].Components;
      }
      return -1 != connectedNodeRef;
    }

    protected override void InternalHandleSlotConnected(Slot otherSlot)
    {
      if (!(otherSlot is SlotOutput))
        return;
      NodeWindow parent = otherSlot.Parent;
      if (!this.IsNodeInOutputHierarchy(parent, this.Parent.OutputSlots.SelectMany<SlotOutput, Slot>((Func<SlotOutput, IEnumerable<Slot>>) (slot => (IEnumerable<Slot>) slot.ConnectedSlots)).Select<Slot, NodeWindow>((Func<Slot, NodeWindow>) (slot => slot.Parent)).ToList<NodeWindow>()))
      {
        this.DisableParentInputSlots();
        foreach (Slot connectedSlot in this.ConnectedSlots)
          connectedSlot.ConnectedSlots.Remove((Slot) this);
        this.ConnectedSlots.Clear();
        this.ConnectedSlots.Add(otherSlot);
      }
      else
      {
        SharedData.Instance.NodeEditorWindow.ShowNotification(new GUIContent("A node may not be an ancestor to itself!"));
        parent.OutputSlots.ForEach((Action<SlotOutput>) (slot => slot.ConnectedSlots.Remove((Slot) this)));
      }
    }

    private bool IsNodeInOutputHierarchy(NodeWindow targetNode, List<NodeWindow> nodeCandidates)
    {
      bool flag;
      if (nodeCandidates.Contains(targetNode))
      {
        flag = true;
      }
      else
      {
        List<NodeWindow> list = nodeCandidates.SelectMany<NodeWindow, SlotOutput>((Func<NodeWindow, IEnumerable<SlotOutput>>) (node => (IEnumerable<SlotOutput>) node.OutputSlots)).SelectMany<SlotOutput, Slot>((Func<SlotOutput, IEnumerable<Slot>>) (slot => (IEnumerable<Slot>) slot.ConnectedSlots)).Select<Slot, NodeWindow>((Func<Slot, NodeWindow>) (slot => slot.Parent)).ToList<NodeWindow>();
        flag = list.Count > 0 && this.IsNodeInOutputHierarchy(targetNode, list);
      }
      return flag;
    }

    private void DisableParentInputSlots()
    {
      switch (this.Components)
      {
        case SlotComponents.R:
        case SlotComponents.G:
        case SlotComponents.B:
          this.Parent.SetInputSlotsEnabled(false, SlotComponents.RGBA, SlotComponents.RGB);
          break;
        case SlotComponents.A:
          this.Parent.SetInputSlotsEnabled(false, SlotComponents.RGBA);
          break;
        case SlotComponents.RGB:
          this.Parent.SetInputSlotsEnabled(false, SlotComponents.RGBA, SlotComponents.R, SlotComponents.G, SlotComponents.B);
          break;
        case SlotComponents.RGBA:
          this.Parent.SetInputSlotsEnabled(false, SlotComponents.RGB, SlotComponents.R, SlotComponents.G, SlotComponents.B, SlotComponents.A);
          break;
        default:
          Debug.LogWarning((object) string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected slot components '{0}'.", (object) this.Components));
          break;
      }
    }
  }
}
