﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.SceneUtils
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
  internal class SceneUtils
  {
    public static void GetVersion(out int major, out int minor)
    {
      string[] strArray = Application.unityVersion.Split('.');
      major = int.Parse(strArray[0]);
      minor = int.Parse(strArray[1]);
    }

    public static int GetMaxNumUVs()
    {
      return 4;
    }
  }
}
