﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.Constants
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Unity.EditorPlugin
{
  public static class Constants
  {
    public static string Ambient
    {
      get
      {
        return nameof (Ambient);
      }
    }

    public static string AmbientUI
    {
      get
      {
        return "Ambient";
      }
    }

    public static string Diffuse
    {
      get
      {
        return nameof (Diffuse);
      }
    }

    public static string DiffuseUI
    {
      get
      {
        return "Diffuse";
      }
    }

    public static string Specular
    {
      get
      {
        return nameof (Specular);
      }
    }

    public static string SpecularUI
    {
      get
      {
        return "Specular";
      }
    }

    public static string Normals
    {
      get
      {
        return nameof (Normals);
      }
    }

    public static string NormalsUI
    {
      get
      {
        return "Normals";
      }
    }

    public static string Opacity
    {
      get
      {
        return nameof (Opacity);
      }
    }

    public static string Parallax
    {
      get
      {
        return nameof (Parallax);
      }
    }

    public static string Emission
    {
      get
      {
        return nameof (Emission);
      }
    }

    public static string Occlusion
    {
      get
      {
        return nameof (Occlusion);
      }
    }

    public static string DetailMask
    {
      get
      {
        return "Detail Mask";
      }
    }

    public static string SecondaryDiffuse
    {
      get
      {
        return "Secondary Diffuse";
      }
    }

    public static string SecondaryNormals
    {
      get
      {
        return "Secondary Normals";
      }
    }

    public static string Custom0
    {
      get
      {
        return nameof (Custom0);
      }
    }

    public static string Custom1
    {
      get
      {
        return nameof (Custom1);
      }
    }

    public static string Custom2
    {
      get
      {
        return nameof (Custom2);
      }
    }

    public static string Custom3
    {
      get
      {
        return nameof (Custom3);
      }
    }

    public static string Custom4
    {
      get
      {
        return nameof (Custom4);
      }
    }

    public static string Custom5
    {
      get
      {
        return nameof (Custom5);
      }
    }

    public static string Custom6
    {
      get
      {
        return nameof (Custom6);
      }
    }

    public static string Custom7
    {
      get
      {
        return nameof (Custom7);
      }
    }

    public static string Custom8
    {
      get
      {
        return nameof (Custom8);
      }
    }

    public static string Custom9
    {
      get
      {
        return nameof (Custom9);
      }
    }

    public static string OpacityUI
    {
      get
      {
        return "Opacity";
      }
    }

    public static string MaterialMetric
    {
      get
      {
        return nameof (MaterialMetric);
      }
    }

    public static string MaterialMetricUI
    {
      get
      {
        return "LOD Type";
      }
    }

    public static string VertexColors
    {
      get
      {
        return nameof (VertexColors);
      }
    }

    public static string VertexColorsUI
    {
      get
      {
        return "Vertex Colors";
      }
    }

    public static string FlipBackfacing
    {
      get
      {
        return nameof (FlipBackfacing);
      }
    }

    public static string FlipBackfacingUI
    {
      get
      {
        return "Flip Backfacing";
      }
    }

    public static string TangentSpace
    {
      get
      {
        return nameof (TangentSpace);
      }
    }

    public static string TangentSpaceUI
    {
      get
      {
        return "Tangent Space";
      }
    }

    public static string sRGB
    {
      get
      {
        return nameof (sRGB);
      }
    }

    public static string sRGBUI
    {
      get
      {
        return "sRGB";
      }
    }

    public static string CastChannel
    {
      get
      {
        return nameof (CastChannel);
      }
    }

    public static string CastChannelUI
    {
      get
      {
        return "Cast";
      }
    }

    public static string TangentSpaceMethod
    {
      get
      {
        return nameof (TangentSpaceMethod);
      }
    }

    public static string TangentSpaceMethodUI
    {
      get
      {
        return "Tangentspace Type";
      }
    }

    public static string ApiKey
    {
      get
      {
        return "CYLBXDTUMS";
      }
    }
  }
}
