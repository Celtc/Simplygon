﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.Profiler
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
  public class Profiler
  {
    private Dictionary<string, ProfilerSession> profilerSessions = new Dictionary<string, ProfilerSession>();

    public void Clear()
    {
      this.profilerSessions.Clear();
    }

    public void DisplayResults()
    {
      Debug.Log((object) "Profiler results:");
      foreach (KeyValuePair<string, ProfilerSession> profilerSession in this.profilerSessions)
        Debug.Log((object) string.Format(CultureInfo.InvariantCulture, "{1}s {0}", (object) profilerSession.Value.Name, (object) profilerSession.Value.ElapsedTime.TotalSeconds));
    }

    public void BeginSession(string name)
    {
      ProfilerSession profilerSession = new ProfilerSession(name);
      if (this.profilerSessions.ContainsKey(name))
        this.profilerSessions[name] = profilerSession;
      else
        this.profilerSessions.Add(name, profilerSession);
      profilerSession.Begin();
    }

    public void EndSession(string name)
    {
      if (!this.profilerSessions.ContainsKey(name))
        return;
      this.profilerSessions[name].End();
    }
  }
}
