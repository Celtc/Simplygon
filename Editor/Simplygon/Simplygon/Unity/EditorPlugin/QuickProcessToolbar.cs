﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.QuickProcessToolbar
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Processor;
using Simplygon.SPL.v80.Settings;
using System;
using System.Globalization;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
  public class QuickProcessToolbar
  {
    private List<int> texSizeDefaultValues = new List<int>()
    {
      32,
      64,
      128,
      256,
      512,
      1024,
      2048,
      4096,
      8192
    };
    private Vector2 scrollPosition = Vector2.zero;
    private bool showFoldOutAssets = true;
    private readonly GUIContent MappingImageSettingsWidthGUIContent = new GUIContent("Width", "Sets the width of the textures.");
    private readonly GUIContent MappingImageSettingsHeightGUIContent = new GUIContent("Height", "Sets the height of the textures.");
    private GUIStyle quickHeaderTextStyle = new GUIStyle();
    private GUIStyle quickTextStyle = new GUIStyle();
    private GUIStyle quickButtonStyle = new GUIStyle();
    private GUIStyle quickButtonSelectedStyle = new GUIStyle();
    private GUIStyle quickAdvancedButtonStyle = new GUIStyle();
    private GUIStyle quickAdvancedButtonImageStyle = new GUIStyle();
    private readonly Dictionary<QuickProcessToolbar.QuickGUIMode, Action> QuickGUIModeHandlers;
    private bool[] quickGUIModeFoldouts;
    private QuickProcessToolbar.QuickGUIMode currentQuickGUIMode;
    private int[] texSize_Values;
    private GUIContent[] TexSize_Names;

    public event EventHandler AdvancedSettingsSelected;

    public QuickProcessToolbar()
    {
      SharedData.Instance.PropertyChanged += (PropertyChangedEventHandler) ((s, e) =>
      {
        if (!("Account" == e.PropertyName))
          return;
        this.UpdateRestricitions();
      });
      this.UpdateRestricitions();
      this.ResetSettings();
      this.QuickGUIModeHandlers = new Dictionary<QuickProcessToolbar.QuickGUIMode, Action>()
      {
        {
          QuickProcessToolbar.QuickGUIMode.Reduction,
          new Action(this.QuickModeReductionGUI)
        },
        {
          QuickProcessToolbar.QuickGUIMode.LODChain,
          new Action(this.QuickModeLODChainGUI)
        },
        {
          QuickProcessToolbar.QuickGUIMode.TextureMerging,
          new Action(this.QuickModeTextureMergingGUI)
        },
        {
          QuickProcessToolbar.QuickGUIMode.Proxy,
          new Action(this.QuickModeProxyGUI)
        }
      };
      this.quickGUIModeFoldouts = new bool[Enum.GetNames(typeof (QuickProcessToolbar.QuickGUIMode)).Length];
      for (int index = 0; index < this.quickGUIModeFoldouts.Length; ++index)
        this.quickGUIModeFoldouts[index] = false;
      this.quickHeaderTextStyle.fontSize = 35;
      this.quickHeaderTextStyle.normal.textColor = EditorGUIUtility.isProSkin ? Color.white : Color.black;
      this.quickHeaderTextStyle.alignment = TextAnchor.MiddleCenter;
      this.quickTextStyle.fontSize = 12;
      this.quickTextStyle.normal.textColor = EditorGUIUtility.isProSkin ? Color.white : Color.black;
      this.quickTextStyle.wordWrap = true;
      this.quickButtonStyle.fontSize = 20;
      this.quickButtonStyle.fontStyle = FontStyle.Normal;
      this.quickButtonStyle.alignment = TextAnchor.MiddleCenter;
      this.quickButtonStyle.normal.textColor = new Color(29f / 64f, 29f / 64f, 29f / 64f);
      this.quickButtonStyle.hover.textColor = new Color(191f / 256f, 191f / 256f, 191f / 256f);
      this.quickButtonStyle.onHover.textColor = new Color(191f / 256f, 191f / 256f, 191f / 256f);
      this.quickButtonStyle.normal.background = TextureUtility.CreateTexture(2, 2, new Color(0.0f, 11f / 32f, (float) sbyte.MaxValue / 256f));
      this.quickButtonStyle.hover.background = TextureUtility.CreateTexture(2, 2, new Color(0.0f, 33f / 64f, 191f / 256f));
      this.quickButtonStyle.onHover.background = TextureUtility.CreateTexture(2, 2, new Color(0.0f, 33f / 64f, 191f / 256f));
      this.quickButtonSelectedStyle.fixedHeight = 35f;
      this.quickButtonSelectedStyle.fontSize = 20;
      this.quickButtonSelectedStyle.alignment = TextAnchor.MiddleCenter;
      this.quickButtonSelectedStyle.normal.textColor = Color.white;
      this.quickButtonSelectedStyle.normal.background = TextureUtility.CreateTexture(2, 2, new Color(0.0f, 11f / 16f, 1f));
      this.quickButtonSelectedStyle.fixedHeight = 35f;
      this.quickAdvancedButtonStyle.fontSize = 12;
      this.quickAdvancedButtonStyle.normal.textColor = EditorGUIUtility.isProSkin ? Color.white : Color.black;
      this.quickAdvancedButtonStyle.hover.textColor = EditorGUIUtility.isProSkin ? new Color(191f / 256f, 191f / 256f, 191f / 256f) : new Color(0.25f, 0.25f, 0.25f);
      this.quickAdvancedButtonStyle.onHover.textColor = EditorGUIUtility.isProSkin ? new Color(191f / 256f, 191f / 256f, 191f / 256f) : new Color(0.25f, 0.25f, 0.25f);
      this.quickAdvancedButtonStyle.normal.background = TextureUtility.CreateTexture(2, 2, new Color(0.0f, 0.0f, 0.0f, 0.0f));
      this.quickAdvancedButtonStyle.hover.background = TextureUtility.CreateTexture(2, 2, new Color(0.0f, 0.0f, 0.0f, 0.0f));
      this.quickAdvancedButtonStyle.onHover.background = TextureUtility.CreateTexture(2, 2, new Color(0.0f, 0.0f, 0.0f, 0.0f));
      this.quickAdvancedButtonStyle.alignment = TextAnchor.MiddleRight;
      this.quickAdvancedButtonStyle.margin = new RectOffset(0, 8, 0, 0);
      this.quickAdvancedButtonImageStyle.normal.background = ResourcesEx.LoadSettingsIcon();
      this.quickAdvancedButtonImageStyle.margin = new RectOffset(0, 10, 0, 0);
      this.quickAdvancedButtonImageStyle.alignment = TextAnchor.MiddleRight;
    }

    public void UpdateRestricitions()
    {
      int maxOutputTextureSize = -1;
      if (SharedData.Instance.Account != null)
      {
        maxOutputTextureSize = SharedData.Instance.Account.ProcessSubscriptionRestrictions.MaterialLOD.MaxOutputTextureSize;
        if (SharedData.Instance.Account.ProcessSubscriptionRestrictions.MeshLOD.Enabled || SharedData.Instance.Account.ProcessSubscriptionRestrictions.AggregateLOD.Enabled || SharedData.Instance.Account.ProcessSubscriptionRestrictions.ProxyLOD.Enabled)
        {
          if (!SharedData.Instance.Account.ProcessSubscriptionRestrictions.MeshLOD.Enabled && (this.currentQuickGUIMode == QuickProcessToolbar.QuickGUIMode.Reduction || this.currentQuickGUIMode == QuickProcessToolbar.QuickGUIMode.LODChain))
            this.currentQuickGUIMode = QuickProcessToolbar.QuickGUIMode.TextureMerging;
          if (!SharedData.Instance.Account.ProcessSubscriptionRestrictions.AggregateLOD.Enabled && this.currentQuickGUIMode == QuickProcessToolbar.QuickGUIMode.TextureMerging)
            this.currentQuickGUIMode = QuickProcessToolbar.QuickGUIMode.Proxy;
          if (!SharedData.Instance.Account.ProcessSubscriptionRestrictions.ProxyLOD.Enabled && this.currentQuickGUIMode == QuickProcessToolbar.QuickGUIMode.Proxy)
            this.currentQuickGUIMode = QuickProcessToolbar.QuickGUIMode.Reduction;
        }
      }
      this.texSize_Values = -1 == maxOutputTextureSize ? this.texSizeDefaultValues.ToArray() : this.texSizeDefaultValues.FindAll((Predicate<int>) (value => value <= maxOutputTextureSize)).ToArray();
      this.TexSize_Names = new GUIContent[this.texSize_Values.Length];
      for (int index = 0; index < this.texSize_Values.Length; ++index)
        this.TexSize_Names[index] = new GUIContent(this.texSize_Values[index].ToString());
    }

    public void ResetSettings()
    {
      this.currentQuickGUIMode = QuickProcessToolbar.QuickGUIMode.Reduction;
      this.ResetSettings(typeof (ReductionProcessor));
    }

    public void ResetSettings(System.Type processorType)
    {
      SharedData.Instance.LODChain.Clear();
      SharedData.Instance.LODChain.Add(SettingsUtility.GetDefaultProcessNode(processorType));
    }

    public void Update()
    {
      this.SetupStyleBackgrounds();
    }

    public void OnInspectorUpdate()
    {
    }

    public void Show()
    {
      this.scrollPosition = EditorGUILayout.BeginScrollView(this.scrollPosition);
      UIUtility.TargetAssetsGUI(ref this.showFoldOutAssets);
      UIUtility.SeparatorGUI();
      GUILayout.Space(12f);
      EditorGUILayout.LabelField("Start Here!", this.quickHeaderTextStyle, new GUILayoutOption[0]);
      GUILayout.Space(20f);
      if (!SharedData.Instance.Account.ProcessSubscriptionRestrictions.MeshLOD.Enabled && !SharedData.Instance.Account.ProcessSubscriptionRestrictions.AggregateLOD.Enabled && !SharedData.Instance.Account.ProcessSubscriptionRestrictions.ProxyLOD.Enabled)
      {
        EditorGUILayout.LabelField("There are no quick processing selections available due to all processing types currently being disabled.", UIUtility.QuickTextCenteredStyle, new GUILayoutOption[0]);
      }
      else
      {
        if (SharedData.Instance.Account.ProcessSubscriptionRestrictions.MeshLOD.Enabled)
        {
          GUILayout.Space(8f);
          if (GUILayout.Button("Reduction", this.currentQuickGUIMode == QuickProcessToolbar.QuickGUIMode.Reduction ? this.quickButtonSelectedStyle : this.quickButtonStyle, new GUILayoutOption[1]
          {
            GUILayout.Height(35f)
          }))
          {
            this.currentQuickGUIMode = QuickProcessToolbar.QuickGUIMode.Reduction;
            this.ResetSettings(typeof (ReductionProcessor));
          }
          if (this.currentQuickGUIMode == QuickProcessToolbar.QuickGUIMode.Reduction)
            this.ShowQuickGUI(this.currentQuickGUIMode);
          GUILayout.Space(8f);
          if (GUILayout.Button("LOD Chain", this.currentQuickGUIMode == QuickProcessToolbar.QuickGUIMode.LODChain ? this.quickButtonSelectedStyle : this.quickButtonStyle, new GUILayoutOption[1]
          {
            GUILayout.Height(35f)
          }))
          {
            this.currentQuickGUIMode = QuickProcessToolbar.QuickGUIMode.LODChain;
            this.ResetSettings(typeof (ReductionProcessor));
          }
          if (this.currentQuickGUIMode == QuickProcessToolbar.QuickGUIMode.LODChain)
            this.ShowQuickGUI(this.currentQuickGUIMode);
        }
        if (SharedData.Instance.Account.ProcessSubscriptionRestrictions.AggregateLOD.Enabled)
        {
          GUILayout.Space(8f);
          if (GUILayout.Button("Texture Merging", this.currentQuickGUIMode == QuickProcessToolbar.QuickGUIMode.TextureMerging ? this.quickButtonSelectedStyle : this.quickButtonStyle, new GUILayoutOption[1]
          {
            GUILayout.Height(35f)
          }))
          {
            this.currentQuickGUIMode = QuickProcessToolbar.QuickGUIMode.TextureMerging;
            this.ResetSettings(typeof (AggregationProcessor));
          }
          if (this.currentQuickGUIMode == QuickProcessToolbar.QuickGUIMode.TextureMerging)
            this.ShowQuickGUI(this.currentQuickGUIMode);
        }
        if (SharedData.Instance.Account.ProcessSubscriptionRestrictions.ProxyLOD.Enabled)
        {
          GUILayout.Space(8f);
          if (GUILayout.Button("Proxy", this.currentQuickGUIMode == QuickProcessToolbar.QuickGUIMode.Proxy ? this.quickButtonSelectedStyle : this.quickButtonStyle, new GUILayoutOption[1]
          {
            GUILayout.Height(35f)
          }))
          {
            this.currentQuickGUIMode = QuickProcessToolbar.QuickGUIMode.Proxy;
            this.ResetSettings(typeof (RemeshingProcessor));
          }
          if (this.currentQuickGUIMode == QuickProcessToolbar.QuickGUIMode.Proxy)
            this.ShowQuickGUI(this.currentQuickGUIMode);
        }
      }
      EditorGUILayout.EndScrollView();
      UIUtility.SeparatorGUI();
      UIUtility.ProcessGUI();
    }

    private void SetupStyleBackgrounds()
    {
      if (null == this.quickButtonStyle.normal.background)
        this.quickButtonStyle.normal.background = TextureUtility.CreateTexture(2, 2, new Color(0.0f, 11f / 32f, (float) sbyte.MaxValue / 256f));
      if (null == this.quickButtonStyle.hover.background)
        this.quickButtonStyle.hover.background = TextureUtility.CreateTexture(2, 2, new Color(0.0f, 33f / 64f, 191f / 256f));
      if (null == this.quickButtonStyle.onHover.background)
        this.quickButtonStyle.onHover.background = TextureUtility.CreateTexture(2, 2, new Color(0.0f, 33f / 64f, 191f / 256f));
      if (null == this.quickButtonSelectedStyle.normal.background)
        this.quickButtonSelectedStyle.normal.background = TextureUtility.CreateTexture(2, 2, new Color(0.0f, 11f / 16f, 1f));
      if (null == this.quickAdvancedButtonStyle.normal.background)
        this.quickAdvancedButtonStyle.normal.background = TextureUtility.CreateTexture(2, 2, new Color(0.0f, 0.0f, 0.0f, 0.0f));
      if (null == this.quickAdvancedButtonStyle.hover.background)
        this.quickAdvancedButtonStyle.hover.background = TextureUtility.CreateTexture(2, 2, new Color(0.0f, 0.0f, 0.0f, 0.0f));
      if (null == this.quickAdvancedButtonStyle.onHover.background)
        this.quickAdvancedButtonStyle.onHover.background = TextureUtility.CreateTexture(2, 2, new Color(0.0f, 0.0f, 0.0f, 0.0f));
      if (!(null == this.quickAdvancedButtonImageStyle.normal.background))
        return;
      this.quickAdvancedButtonImageStyle.normal.background = ResourcesEx.LoadSettingsIcon();
    }

    private void ShowQuickGUI(QuickProcessToolbar.QuickGUIMode quickGUIMode)
    {
      EditorGUILayout.Space();
      ++EditorGUI.indentLevel;
      switch (quickGUIMode)
      {
        case QuickProcessToolbar.QuickGUIMode.Reduction:
          this.QuickModeReductionGUI();
          break;
        case QuickProcessToolbar.QuickGUIMode.LODChain:
          this.QuickModeLODChainGUI();
          break;
        case QuickProcessToolbar.QuickGUIMode.TextureMerging:
          this.QuickModeTextureMergingGUI();
          break;
        case QuickProcessToolbar.QuickGUIMode.Proxy:
          this.QuickModeProxyGUI();
          break;
        default:
          Debug.Log((object) string.Format(CultureInfo.InvariantCulture, "Simplygon: Unable to find a suitable GUI for quick GUI mode '{0}'.", (object) quickGUIMode.ToString()));
          break;
      }
      --EditorGUI.indentLevel;
      GUILayout.Space(12f);
      EditorGUILayout.BeginHorizontal();
      GUILayout.FlexibleSpace();
      if (!GUILayout.Button("Advanced settings", this.quickAdvancedButtonStyle, new GUILayoutOption[0]))
      {
        if (!GUILayout.Button(string.Empty, this.quickAdvancedButtonImageStyle, GUILayout.Height(16f), GUILayout.Width(16f)))
          goto label_10;
      }
      // ISSUE: reference to a compiler-generated field
      if (this.AdvancedSettingsSelected != null)
      {
        // ISSUE: reference to a compiler-generated field
        this.AdvancedSettingsSelected((object) this, EventArgs.Empty);
      }
label_10:
      EditorGUILayout.EndHorizontal();
      EditorGUILayout.Space();
    }

    private void QuickModeReductionGUI()
    {
      EditorGUILayout.LabelField("How many percent of the triangles would you like to keep?", this.quickTextStyle, new GUILayoutOption[0]);
      ReductionSettings reductionSettings = (SharedData.Instance.LODChain[0].Processor as ReductionProcessor).ReductionSettings;
      reductionSettings.TriangleRatio = (double) EditorGUILayout.IntSlider((int) (reductionSettings.TriangleRatio * 100.0), 0, 100) * 0.01;
    }

    private void QuickModeLODChainGUI()
    {
      EditorGUILayout.LabelField("How many LOD's would you like?", this.quickTextStyle, new GUILayoutOption[0]);
      int newLODCount = EditorGUILayout.IntSlider(SharedData.Instance.LODChain.Count, 1, 5);
      EditorGUILayout.Space();
      ReductionSettings reductionSettings = (SharedData.Instance.LODChain[0].Processor as ReductionProcessor).ReductionSettings;
      EditorGUILayout.LabelField("Reduction ratio on each LOD (%) where LODs are based on previous LOD", this.quickTextStyle, new GUILayoutOption[0]);
      reductionSettings.TriangleRatio = (double) EditorGUILayout.IntSlider((int) (reductionSettings.TriangleRatio * 100.0), 0, 100) * 0.01;
      SettingsUtility.HandleNumberOfSelectedLODs(newLODCount);
      SharedData.Instance.Settings.SetCascadedLOD(true);
      for (int index = 1; index < SharedData.Instance.LODChain.Count; ++index)
        (SharedData.Instance.LODChain[index].Processor as ReductionProcessor).ReductionSettings.TriangleRatio = reductionSettings.TriangleRatio;
    }

    private void QuickModeTextureMergingGUI()
    {
      EditorGUILayout.LabelField("Create a new replacement geometry by defining a texture size for the merged texture", this.quickTextStyle, new GUILayoutOption[0]);
      EditorGUILayout.Space();
      MappingImageSettings mappingImageSettings = (SharedData.Instance.LODChain[0].Processor as AggregationProcessor).MappingImageSettings;
      mappingImageSettings.Width = EditorGUILayout.IntPopup(this.MappingImageSettingsWidthGUIContent, mappingImageSettings.Width, this.TexSize_Names, this.texSize_Values, new GUILayoutOption[0]);
      EditorGUILayout.Space();
      mappingImageSettings.Height = EditorGUILayout.IntPopup(this.MappingImageSettingsHeightGUIContent, mappingImageSettings.Height, this.TexSize_Names, this.texSize_Values, new GUILayoutOption[0]);
    }

    private void QuickModeProxyGUI()
    {
      EditorGUILayout.LabelField("Create a new replacement geometry for the defined pixel size", this.quickTextStyle, new GUILayoutOption[0]);
      EditorGUILayout.Space();
      RemeshingSettings remeshingSettings = (SharedData.Instance.LODChain[0].Processor as RemeshingProcessor).RemeshingSettings;
      remeshingSettings.OnScreenSize = EditorGUILayout.IntSlider(remeshingSettings.OnScreenSize, 20, SharedData.Instance.Account.ProcessSubscriptionRestrictions.ProxyLOD.MaxOnScreenSize);
      GUILayout.Space(8f);
      EditorGUILayout.LabelField("Combine and recreate a new texture", this.quickTextStyle, new GUILayoutOption[0]);
      EditorGUILayout.Space();
      MappingImageSettings mappingImageSettings = (SharedData.Instance.LODChain[0].Processor as RemeshingProcessor).MappingImageSettings;
      mappingImageSettings.Enabled = SharedData.Instance.Account.ProcessSubscriptionRestrictions.MaterialLOD.Enabled;
      if (!mappingImageSettings.Enabled)
        return;
      mappingImageSettings.Width = EditorGUILayout.IntPopup(this.MappingImageSettingsWidthGUIContent, mappingImageSettings.Width, this.TexSize_Names, this.texSize_Values, new GUILayoutOption[0]);
      EditorGUILayout.Space();
      mappingImageSettings.Height = EditorGUILayout.IntPopup(this.MappingImageSettingsHeightGUIContent, mappingImageSettings.Height, this.TexSize_Names, this.texSize_Values, new GUILayoutOption[0]);
    }

    public enum QuickGUIMode
    {
      Reduction,
      LODChain,
      TextureMerging,
      Proxy,
    }
  }
}
