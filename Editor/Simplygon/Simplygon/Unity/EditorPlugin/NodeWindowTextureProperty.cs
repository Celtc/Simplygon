﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.NodeWindowTextureProperty
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class NodeWindowTextureProperty : NodeWindowShaderProperty
    {
        private static Dictionary<string, string> TexCoordsMapping = NodeWindowTextureProperty.GenerateTexCoordMapping();
        private readonly string[] SelectableTexCoords;
        private int selectedTexCoordsIndex;

        private static Dictionary<string, string> GenerateTexCoordMapping()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            for (int index = 0; index < SceneUtils.GetMaxNumUVs(); ++index)
                dictionary.Add("UV" + (object)index, "TexCoords" + (object)index);
            return dictionary;
        }

        public NodeWindowTextureProperty(int nodeRef, int selectedShaderPropertyIndex, string[] shaderTextureProperties)
          : this(nodeRef, selectedShaderPropertyIndex, shaderTextureProperties, "TexCoords0")
        {
        }

        public NodeWindowTextureProperty(int nodeRef, int selectedShaderPropertyIndex, string[] shaderTextureProperties, string texCoords)
          : base(nodeRef, selectedShaderPropertyIndex, shaderTextureProperties, new Vector2(0.0f, 60f))
        {
            this.Title = "Texture Property";
            this.HelpText = "The Texture Property node forwards an arbitrary shader texture property of the currently selected shader. The property is selectable via the drop-down within the node window. For Unity standard shaders' ('Standard' and Standard '(Specular setup)') secondary maps, the UV set is automatically set to the corresponding shader property value on export.";
            this.SelectableTexCoords = NodeWindowTextureProperty.TexCoordsMapping.Keys.ToArray<string>();
            string key = NodeWindowTextureProperty.TexCoordsMapping.FirstOrDefault<KeyValuePair<string, string>>((Func<KeyValuePair<string, string>, bool>)(kvp => kvp.Value == texCoords)).Key;
            if (key != null)
                this.selectedTexCoordsIndex = ((IEnumerable<string>)this.SelectableTexCoords).ToList<string>().IndexOf(key);
            this.TexCoords = texCoords;
        }

        protected override void DrawInnerContents()
        {
            int selectedTexCoordsIndex = this.selectedTexCoordsIndex;
            this.selectedTexCoordsIndex = EditorGUILayout.Popup(this.selectedTexCoordsIndex, this.SelectableTexCoords);
            if (this.selectedTexCoordsIndex == selectedTexCoordsIndex)
                return;
            this.TexCoords = NodeWindowTextureProperty.TexCoordsMapping[this.SelectableTexCoords[this.selectedTexCoordsIndex]];
        }

        internal override BasicShadingNodeType GetShadingNodeType()
        {
            return BasicShadingNodeType.Texture;
        }
    }
}
