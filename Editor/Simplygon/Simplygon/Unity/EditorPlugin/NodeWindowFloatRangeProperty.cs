﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.NodeWindowFloatRangeProperty
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Unity;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class NodeWindowFloatRangeProperty : NodeWindowShaderProperty
    {
        public NodeWindowFloatRangeProperty(int nodeRef, int selectedShaderPropertyIndex, string[] shaderColorProperties)
          : base(nodeRef, selectedShaderPropertyIndex, shaderColorProperties, Vector2.zero)
        {
            this.Title = "Float Property";
            this.HelpText = "The Float/Range Property node forwards an arbitrary shader float/range property of the currently selected shader. The property is selectable via the drop-down within the node window.";
        }

        internal override BasicShadingNodeType GetShadingNodeType()
        {
            return BasicShadingNodeType.Float;
        }
    }
}
