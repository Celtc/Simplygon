﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.WorkingIcon
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
  public class WorkingIcon
  {
    private DateTime updateTime;

    public Texture2D CurrentWorkingIcon
    {
      get
      {
        if (GUI.skin.name == "DarkSkin")
          return ResourcesEx.LoadWorkingLightIcon(this.CurrentIndex);
        return ResourcesEx.LoadWorkingDarkIcon(this.CurrentIndex);
      }
    }

    public int CurrentIndex { get; private set; }

    public WorkingIcon()
    {
      this.updateTime = DateTime.UtcNow;
    }

    public void Update()
    {
      this.CurrentIndex = (this.CurrentIndex + 1) % 4;
    }

    public void Reset()
    {
      this.CurrentIndex = 0;
    }
  }
}
