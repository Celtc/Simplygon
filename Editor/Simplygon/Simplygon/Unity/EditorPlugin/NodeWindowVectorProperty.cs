﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.NodeWindowVectorProperty
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Unity;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class NodeWindowVectorProperty : NodeWindowShaderProperty
    {
        public NodeWindowVectorProperty(int nodeRef, int selectedShaderPropertyIndex, string[] shaderTextureProperties)
          : base(nodeRef, selectedShaderPropertyIndex, shaderTextureProperties, Vector2.zero)
        {
            this.Title = "Vector Property";
            this.HelpText = "The Vector Property node forwards an arbitrary shader vector property of the currently selected shader. The property is selectable via the drop-down within the node window.";
        }

        internal override BasicShadingNodeType GetShadingNodeType()
        {
            return BasicShadingNodeType.Vector;
        }
    }
}
