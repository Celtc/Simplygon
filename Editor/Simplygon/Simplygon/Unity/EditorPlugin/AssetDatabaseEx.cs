﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.AssetDatabaseEx
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Unity.EditorPlugin.Jobs;
using System;
using System.Globalization;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class AssetDatabaseEx
    {
        public static void CreateRootLODFolder()
        {
            if (Directory.Exists(Path.Combine(SharedData.Instance.ApplicationDataPath, "LODs")))
                return;
            AssetDatabase.CreateFolder("Assets", "LODs");
        }

        public static void CreatePendingLODFolder(string jobId)
        {
            AssetDatabaseEx.CreateRootLODFolder();
            if (!Directory.Exists(AssetDatabaseEx.GetAbsolutePendingLODFolder()))
                AssetDatabase.CreateFolder("Assets/LODs", "PendingLODs");
            if (Directory.Exists(AssetDatabaseEx.GetAbsolutePendingLODFolder(jobId)))
                return;
            AssetDatabase.CreateFolder("Assets/LODs/PendingLODs", jobId);
        }

        public static string GetPendingLODPaths(params string[] pathsToCombine)
        {
            string path1 = "Assets/LODs/PendingLODs";
            foreach (string path2 in pathsToCombine)
                path1 = Path.Combine(path1, path2);
            return path1.Replace("\\", "/");
        }

        public static string GetAbsolutePendingLODFolder()
        {
            return Path.Combine(Path.Combine(SharedData.Instance.ApplicationDataPath, "LODs"), "PendingLODs");
        }

        public static string GetAbsolutePendingLODFolder(string jobId)
        {
            return Path.Combine(AssetDatabaseEx.GetAbsolutePendingLODFolder(), jobId);
        }

        public static bool IsPendingLODValid(string jobId, int lodCount)
        {
            bool flag = false;
            try
            {
                string path = Path.Combine(Path.Combine(Path.Combine(SharedData.Instance.ApplicationDataPath, "LODs"), "PendingLODs"), jobId);
                if (Directory.Exists(path))
                    flag = new DirectoryInfo(path).GetFiles("*.prefab").Length == lodCount;
            }
            catch (Exception ex)
            {
                Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to determine if pending LOD is valid ({0}\n{1})", (object)ex.Message, (object)ex.StackTrace));
            }
            return flag;
        }

        public static bool IsPendingLODAvailable(string jobId)
        {
            bool flag = false;
            try
            {
                if (!string.IsNullOrEmpty(jobId))
                {
                    string path = Path.Combine(Path.Combine(Path.Combine(SharedData.Instance.ApplicationDataPath, "LODs"), "PendingLODs"), jobId);
                    if (Directory.Exists(path))
                        flag = new DirectoryInfo(path).GetFiles("*.prefab").Length > 0;
                }
            }
            catch (Exception ex)
            {
                Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to determine if pending LOD is available ({0}\n{1})", (object)ex.Message, (object)ex.StackTrace));
            }
            return flag;
        }

        public static void MovePendingLODFolderPrefabs(string oldJobId, string newJobId)
        {
            try
            {
                string pendingLodFolder = AssetDatabaseEx.GetAbsolutePendingLODFolder(oldJobId);
                if (!Directory.Exists(pendingLodFolder))
                    throw new Exception(string.Format(CultureInfo.InvariantCulture, "Unable to find previous pending LOD path '{0}'", (object)pendingLodFolder));
                if (!Directory.Exists(AssetDatabaseEx.GetAbsolutePendingLODFolder(newJobId)))
                {
                    AssetDatabaseEx.CreatePendingLODFolder(newJobId);
                    foreach (string file in Directory.GetFiles(pendingLodFolder, "*.prefab"))
                    {
                        string pendingLodPaths1 = AssetDatabaseEx.GetPendingLODPaths(oldJobId, Path.GetFileName(file));
                        string pendingLodPaths2 = AssetDatabaseEx.GetPendingLODPaths(newJobId, Path.GetFileName(file));
                        if (!AssetDatabase.CopyAsset(pendingLodPaths1, pendingLodPaths2))
                            throw new Exception(string.Format(CultureInfo.InvariantCulture, "Failed to copy pending prefab from {0} to {1}.", (object)pendingLodPaths1, (object)pendingLodPaths2));
                    }
                }
                AssetDatabase.DeleteAsset(AssetDatabaseEx.GetPendingLODPaths(oldJobId));
                AssetDatabase.Refresh();
            }
            catch (Exception ex)
            {
                Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to move pending LOD folder ({0})", (object)ex.Message));
                throw;
            }
        }

        public static void DeletePendingLODFolder(string jobId)
        {
            if (!Directory.Exists(Path.Combine(Path.Combine(Path.Combine(SharedData.Instance.ApplicationDataPath, "LODs"), "PendingLODs"), jobId)))
                return;
            AssetDatabase.DeleteAsset(AssetDatabaseEx.GetPendingLODPaths(jobId));
        }

        public static List<string> GetPendingLODs()
        {
            string path = Path.Combine(Path.Combine(SharedData.Instance.ApplicationDataPath, "LODs"), "PendingLODs");
            List<string> stringList = new List<string>();
            if (Directory.Exists(path))
            {
                foreach (DirectoryInfo directory in new DirectoryInfo(path).GetDirectories())
                    stringList.Add(directory.Name);
            }
            return stringList;
        }

        internal static void CreateLODFolders(UnityCloudJob job)
        {
            AssetDatabaseEx.CreateRootLODFolder();
            if (string.IsNullOrEmpty(job.AssetDirectory))
            {
                DateTime localTime = DateTime.UtcNow.ToLocalTime();
                string path2 = string.Format(CultureInfo.InvariantCulture, "{0}_{1:0000}{2:00}{3:00}_{4:00}{5:00}{6:00}", (object)job.CloudJob.Name, (object)localTime.Year, (object)localTime.Month, (object)localTime.Day, (object)localTime.Hour, (object)localTime.Minute, (object)localTime.Second);
                string path1 = Path.Combine(Path.Combine(SharedData.Instance.ApplicationDataPath, "LODs"), path2);
                string str1 = string.Format(CultureInfo.InvariantCulture, "{0}/Materials", (object)path2);
                string str2 = Path.Combine(path1, "Materials");
                job.AssetDirectory = path2;
                job.AssetFullDirectory = path1;
                job.AssetMaterialDirectory = str1;
                job.AssetFullMaterialDirectory = str2;
            }
            if (!Directory.Exists(job.AssetFullDirectory))
                AssetDatabase.CreateFolder("Assets/LODs", job.AssetDirectory);
            if (Directory.Exists(job.AssetFullMaterialDirectory))
                return;
            AssetDatabase.CreateFolder(string.Format(CultureInfo.InvariantCulture, "Assets/LODs/{0}", (object)job.AssetDirectory), "Materials");
        }
    }
}
