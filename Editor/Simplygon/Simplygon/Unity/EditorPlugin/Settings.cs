﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.Settings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.Security.Cryptography;
using System.Text;
using UnityEditor;

namespace Simplygon.Unity.EditorPlugin
{
  public class Settings
  {
    private string key = "D14683J6cpr583T8";
    private const string KeyDownloadAssetsAutomatically = "Simplygon_DownloadAssetsAutomatically";
    private const string KeyUserName = "Simplygon_Username";
    private const string KeyPassword = "Simplygon_Password";
    private const string KeyKeepMeLoggedIn = "Simplygon_KeepMeLoggedIn";
    private const string KeyShowHelp = "Simplygon_ShowHelp";
    private const string KeyHostName = "Simplygon_Hostname";
    private const string KeySettingsType = "Simplygon_SettingsType";
    private const string KeySelectedToolbar = "Simplygon_SelectedToolbar";
    private const string KeyCascadedLOD = "Simplygon_CascadedLOD";

    public bool HasDownloadAssetsAutomatically()
    {
      return EditorPrefs.HasKey("Simplygon_DownloadAssetsAutomatically");
    }

    public void SetDownloadAssetsAutomatically(bool downloadAssetsAutomatically)
    {
      EditorPrefs.SetBool("Simplygon_DownloadAssetsAutomatically", downloadAssetsAutomatically);
    }

    public bool GetDownloadAssetsAutomatically()
    {
      if (this.HasDownloadAssetsAutomatically())
        return EditorPrefs.GetBool("Simplygon_DownloadAssetsAutomatically");
      return false;
    }

    public bool HasUsername()
    {
      return EditorPrefs.HasKey("Simplygon_Username");
    }

    public void SetUsername(string username)
    {
      EditorPrefs.SetString("Simplygon_Username", username);
    }

    public string GetUsername()
    {
      if (this.HasUsername())
        return EditorPrefs.GetString("Simplygon_Username");
      return string.Empty;
    }

    public void DeleteUsername()
    {
      EditorPrefs.DeleteKey("Simplygon_Username");
    }

    public bool HasPassword()
    {
      return EditorPrefs.HasKey("Simplygon_Password");
    }

    public void SetPassword(string password)
    {
      EditorPrefs.SetString("Simplygon_Password", this.Encrypt(password));
    }

    public string GetPassword()
    {
      if (this.HasPassword())
        return this.Decrypt(EditorPrefs.GetString("Simplygon_Password"));
      return string.Empty;
    }

    public void DeletePassword()
    {
      EditorPrefs.DeleteKey("Simplygon_Password");
    }

    public bool GetKeepMeLoggedIn()
    {
      if (EditorPrefs.HasKey("Simplygon_KeepMeLoggedIn"))
        return EditorPrefs.GetBool("Simplygon_KeepMeLoggedIn");
      return true;
    }

    public void SetKeepMeLoggedIn(bool keepMeLoggedIn)
    {
      EditorPrefs.SetBool("Simplygon_KeepMeLoggedIn", keepMeLoggedIn);
    }

    public bool GetShowHelp()
    {
      if (EditorPrefs.HasKey("Simplygon_ShowHelp"))
        return EditorPrefs.GetBool("Simplygon_ShowHelp");
      return true;
    }

    public void SetShowHelp(bool showHelp)
    {
      EditorPrefs.SetBool("Simplygon_ShowHelp", showHelp);
    }

    public string GetHostname()
    {
      if (EditorPrefs.HasKey("Simplygon_Hostname"))
        return EditorPrefs.GetString("Simplygon_Hostname");
      return "public.simplygon.com";
    }

    public void SetHostname(string hostname)
    {
      EditorPrefs.SetString("Simplygon_Hostname", hostname);
    }

    public int GetSettingsType()
    {
      return EditorPrefs.GetInt("Simplygon_SettingsType", 0);
    }

    public void SetSettingsType(int settingsType)
    {
      EditorPrefs.SetInt("Simplygon_SettingsType", settingsType);
    }

    public int GetSelectedToolbar()
    {
      return EditorPrefs.GetInt("Simplygon_SelectedToolbar", 0);
    }

    public void SetSelectedToolbar(int selectedToolbar)
    {
      EditorPrefs.SetInt("Simplygon_SelectedToolbar", selectedToolbar);
    }

    public bool GetCascadedLOD()
    {
      return EditorPrefs.GetBool("Simplygon_CascadedLOD");
    }

    public void SetCascadedLOD(bool cascadedLOD)
    {
      EditorPrefs.SetBool("Simplygon_CascadedLOD", cascadedLOD);
    }

    public string Encrypt(string value)
    {
      byte[] bytes = Encoding.UTF8.GetBytes(value);
      TripleDESCryptoServiceProvider cryptoServiceProvider = new TripleDESCryptoServiceProvider();
      cryptoServiceProvider.Key = Encoding.UTF8.GetBytes(this.key);
      cryptoServiceProvider.Mode = CipherMode.ECB;
      cryptoServiceProvider.Padding = PaddingMode.PKCS7;
      byte[] inArray = cryptoServiceProvider.CreateEncryptor().TransformFinalBlock(bytes, 0, bytes.Length);
      cryptoServiceProvider.Clear();
      return Convert.ToBase64String(inArray, 0, inArray.Length);
    }

    public string Decrypt(string value)
    {
      byte[] inputBuffer = Convert.FromBase64String(value);
      TripleDESCryptoServiceProvider cryptoServiceProvider = new TripleDESCryptoServiceProvider();
      cryptoServiceProvider.Key = Encoding.UTF8.GetBytes(this.key);
      cryptoServiceProvider.Mode = CipherMode.ECB;
      cryptoServiceProvider.Padding = PaddingMode.PKCS7;
      byte[] bytes = cryptoServiceProvider.CreateDecryptor().TransformFinalBlock(inputBuffer, 0, inputBuffer.Length);
      cryptoServiceProvider.Clear();
      return Encoding.UTF8.GetString(bytes);
    }
  }
}
