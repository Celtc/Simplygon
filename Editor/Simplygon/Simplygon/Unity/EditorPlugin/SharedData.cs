﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.SharedData
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json;
using Simplygon.Cloud.Unity;
using Simplygon.Cloud.Yoda.Client;
using Simplygon.SPL.v80.Node;
using Simplygon.Unity.EditorPlugin.Jobs;
using Simplygon.Unity.Utils;
using System;
using System.Globalization;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class SharedData : INotifyPropertyChanged
    {
        private Color freeColor = new Color(0.1529412f, 0.6941177f, 0.3019608f);
        private Color linkColor = new Color(0.2078431f, 0.6156863f, 1f);
        private bool updateClientInfo = true;
        private static SharedData instance;
        private User23 account;
        private GUIStyle foldOutStyle;
        private GUIStyle linkLabel;
        private LoadingIcon loadingIcon;
        private WorkingIcon workingIcon;
        private Dictionary<string, List<ShadingChannelProperties>> shaderSettings;

        public static SharedData Instance
        {
            get
            {
                if (SharedData.instance == null)
                    SharedData.instance = new SharedData();
                return SharedData.instance;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        internal List<ProcessNode> LODChain
        {
            get; set;
        }

        public GeneralManager GeneralManager
        {
            get; private set;
        }

        public SelectionManager SelectionManager
        {
            get; private set;
        }

        public Settings Settings
        {
            get; private set;
        }

        internal Client23 ClientInfo
        {
            get; set;
        }

        internal User23 Account
        {
            get
            {
                return this.account;
            }
            set
            {
                if (this.account == value)
                    return;
                this.account = value;
                this.OnPropertyChanged(nameof(Account));
            }
        }

        internal UserSettings10 UserSettings
        {
            get; set;
        }

        public List<GameObject> PendingGameObjects
        {
            get; private set;
        }

        public bool PendingReprocessing
        {
            get; set;
        }

        public Profiler Profiler
        {
            get; private set;
        }

        public float ExportProgress
        {
            get; set;
        }

        public float ImportProgress
        {
            get; set;
        }

        public string ImportProgressStatus
        {
            get; set;
        }

        public bool GameObjectsToExport
        {
            get; set;
        }

        public bool GameObjectsToUpload
        {
            get; set;
        }

        public bool GameObjectsToImport
        {
            get; set;
        }

        public bool GameObjectsToDownload
        {
            get; set;
        }

        public Window SimplygonWindow
        {
            get; set;
        }

        public NodeEditorWindow NodeEditorWindow
        {
            get; set;
        }

        public string ShaderSettingsPath
        {
            get; private set;
        }

        public string Username
        {
            get; set;
        }

        public string Password
        {
            get; set;
        }

        public string Hostname
        {
            get; set;
        }

        public GUIStyle DefaultJobStyle
        {
            get; set;
        }

        public GUIStyle FreeJobStyle
        {
            get; set;
        }

        public GUIStyle FoldOutStyle
        {
            get
            {
                if (this.foldOutStyle == null)
                {
                    this.foldOutStyle = new GUIStyle(EditorStyles.foldout);
                    this.foldOutStyle.fontStyle = FontStyle.Bold;
                }
                return this.foldOutStyle;
            }
        }

        public GUIStyle LinkLabel
        {
            get
            {
                if (this.linkLabel == null)
                {
                    this.linkLabel = new GUIStyle(EditorStyles.label);
                    this.linkLabel.normal.textColor = this.LinkColor;
                }
                return this.linkLabel;
            }
        }

        public Color FreeColor
        {
            get
            {
                return this.freeColor;
            }
        }

        public Color LinkColor
        {
            get
            {
                return this.linkColor;
            }
        }

        public LoadingIcon LoadingIcon
        {
            get
            {
                if (this.loadingIcon == null)
                    this.loadingIcon = new LoadingIcon();
                return this.loadingIcon;
            }
        }

        public WorkingIcon WorkingIcon
        {
            get
            {
                if (this.workingIcon == null)
                    this.workingIcon = new WorkingIcon();
                return this.workingIcon;
            }
        }

        internal Dispatcher UnityDispatcher
        {
            get; private set;
        }

        public string UniqueTempPathInProject
        {
            get; private set;
        }

        public string ApplicationDataPath
        {
            get; private set;
        }

        public bool UseCachedAsset
        {
            get; set;
        }

        internal Dictionary<string, List<ShadingChannelProperties>> ShaderSettings
        {
            get
            {
                return this.shaderSettings;
            }
        }

        public SharedData()
        {
            this.Username = string.Empty;
            this.Password = string.Empty;
            this.Hostname = string.Empty;
            this.UseCachedAsset = false;
            this.LODChain = new List<ProcessNode>();
            this.GeneralManager = new GeneralManager();
            if (this.updateClientInfo)
            {
                this.updateClientInfo = false;
                this.GeneralManager.UpdateClientInfo();
            }
            this.SelectionManager = new SelectionManager();
            this.Settings = new Settings();
            this.PendingGameObjects = new List<GameObject>();
            this.Profiler = new Profiler();
            this.UnityDispatcher = new Dispatcher();
            this.ExportProgress = 0.0f;
            this.ImportProgress = 0.0f;
            this.ImportProgressStatus = "";
            this.GameObjectsToExport = false;
            this.GameObjectsToUpload = false;
            this.GameObjectsToImport = false;
            this.GameObjectsToDownload = false;
            this.PendingReprocessing = false;
        }

        public void Initialize()
        {
            this.ApplicationDataPath = Application.dataPath;
            this.ShaderSettingsPath = Path.Combine(Path.Combine(this.ApplicationDataPath, "Simplygon/Editor"), "SimplygonShadingNetworkSetup.json").Replace('\\', '/');
            this.UniqueTempPathInProject = Path.Combine(Path.GetFullPath(Path.Combine(this.ApplicationDataPath, "../")), FileUtil.GetUniqueTempPathInProject());
            try
            {
                this.shaderSettings = !File.Exists(this.ShaderSettingsPath) ? JobUtils.GetDefaultShaderSettings() : JsonConvert.DeserializeObject<Dictionary<string, List<ShadingChannelProperties>>>(File.ReadAllText(this.ShaderSettingsPath));
            }
            catch (Exception ex)
            {
                Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to load shader settings from {0} ({1})", (object)this.ShaderSettingsPath, (object)ex.Message));
                this.shaderSettings = JobUtils.GetDefaultShaderSettings();
            }
            this.DefaultJobStyle = new GUIStyle();
            this.DefaultJobStyle.hover.background = this.MakeSolidBrush(1, 1, new Color(1f, 0.827451f, 0.9372549f));
            this.FreeJobStyle = new GUIStyle();
            this.FreeJobStyle.normal.background = this.MakeSolidBrush(1, 1, new Color(0.9607843f, 1f, 0.8862745f));
            this.GeneralManager.Initialize();
        }

        protected void OnPropertyChanged(string propertyName)
        {
            // ISSUE: reference to a compiler-generated field
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged == null)
                return;
            propertyChanged((object)this, new PropertyChangedEventArgs(propertyName));
        }

        private Texture2D MakeSolidBrush(int width, int height, Color col)
        {
            Color[] colors = new Color[width * height];
            for (int index = 0; index < colors.Length; ++index)
                colors[index] = col;
            Texture2D texture2D = new Texture2D(width, height);
            texture2D.SetPixels(colors);
            texture2D.Apply();
            return texture2D;
        }
    }
}
