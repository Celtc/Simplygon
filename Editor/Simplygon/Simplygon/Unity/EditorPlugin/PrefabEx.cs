﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.PrefabEx
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Hash;
using Simplygon.Scene.Asset;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class PrefabEx
    {
        public int TriangleCount
        {
            get; set;
        }

        public int BoneCount
        {
            get; set;
        }

        public GameObject Prefab
        {
            get; set;
        }

        public PrefabEx(GameObject prefab, int triangleCount, int boneCount)
        {
            this.Prefab = prefab;
            this.TriangleCount = triangleCount;
            this.BoneCount = boneCount;
        }

        public static string GetUniqueHash()
        {
            return SHA256.ComputeString(Guid.NewGuid().ToString());
        }

        internal static string ComputeHash(List<PrefabEx> prefabs, Simplygon.Scene.Scene scene)
        {
            List<string> source = new List<string>();
            foreach (PrefabEx prefab in prefabs)
            {
                Transform transform = prefab.Prefab.transform;
                source.Add(transform.childCount.ToString());
                foreach (Transform componentsInChild in prefab.Prefab.GetComponentsInChildren<Transform>())
                {
                    float num = Vector3.Distance(transform.position, componentsInChild.position);
                    source.Add(num.ToString());
                    source.Add(componentsInChild.rotation.ToString());
                }
                UnityEngine.Object[] roots = new UnityEngine.Object[1]
                {
          prefab.Prefab
                };
                foreach (var collectDependency in EditorUtility.CollectDependencies(roots))
                {
                    if (collectDependency is UnityEngine.Mesh || collectDependency is UnityEngine.Material || (collectDependency is UnityEngine.Texture || collectDependency is Shader))
                    {
                        string assetPath = AssetDatabase.GetAssetPath(collectDependency);
                        string str = string.IsNullOrEmpty(assetPath) ? collectDependency.name : assetPath;
                        source.Add(str);
                        if (collectDependency is UnityEngine.Mesh)
                        {
                            UnityEngine.Mesh mesh = collectDependency as UnityEngine.Mesh;
                            if (!string.IsNullOrEmpty(mesh.name))
                                source.Add(mesh.name);
                        }
                        else if (collectDependency is UnityEngine.Material)
                        {
                            UnityEngine.Material material = collectDependency as UnityEngine.Material;
                            for (int propertyIdx = 0; propertyIdx < ShaderUtil.GetPropertyCount(material.shader); ++propertyIdx)
                            {
                                if (!ShaderUtil.IsShaderPropertyHidden(material.shader, propertyIdx))
                                {
                                    ShaderUtil.ShaderPropertyType propertyType = ShaderUtil.GetPropertyType(material.shader, propertyIdx);
                                    string propertyName = ShaderUtil.GetPropertyName(material.shader, propertyIdx);
                                    string empty = string.Empty;
                                    switch (propertyType)
                                    {
                                        case ShaderUtil.ShaderPropertyType.Color:
                                            material.GetColor(propertyName).ToString();
                                            continue;
                                        case ShaderUtil.ShaderPropertyType.Vector:
                                            material.GetVector(propertyName).ToString();
                                            continue;
                                        case ShaderUtil.ShaderPropertyType.Float:
                                        case ShaderUtil.ShaderPropertyType.Range:
                                            material.GetFloat(propertyName).ToString();
                                            continue;
                                        case ShaderUtil.ShaderPropertyType.TexEnv:
                                            continue;
                                        default:
                                            Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected shader property type '{0}', property {1} will not get seperately hashed.", (object)propertyType, (object)propertyName));
                                            continue;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            List<Simplygon.Scene.Asset.Material> materialList = scene.MaterialList;
            materialList.Sort((Comparison<Simplygon.Scene.Asset.Material>)((material1, material2) => material1.Name.CompareTo(material2.Name)));
            List<string> stringList1 = new List<string>();
            foreach (Simplygon.Scene.Asset.Material material in materialList)
            {
                List<MaterialChannel> materialChannelList = new List<MaterialChannel>((IEnumerable<MaterialChannel>)material.MaterialChannels);
                materialChannelList.Sort((Comparison<MaterialChannel>)((materialChannel1, materialChannel2) => materialChannel1.ChannelName.CompareTo(materialChannel2.ChannelName)));
                List<string> stringList2 = new List<string>();
                foreach (MaterialChannel materialChannel in materialChannelList)
                {
                    string str1 = materialChannel.ShadingNetwork;
                    List<int> intList = new List<int>();
                    string str2 = "<TextureName>";
                    string str3 = "</TextureName>";
                    int startIndex;
                    int count;
                    for (; str1.Contains(str2); str1 = str1.Remove(startIndex, count))
                    {
                        startIndex = str1.IndexOf(str2);
                        count = str1.IndexOf(str3) + str3.Length - startIndex;
                    }
                    string str4 = SHA256.ComputeString(str1);
                    stringList2.Add(str4);
                }
                string str = SHA256.ComputeString(string.Join(string.Empty, stringList2.ToArray()));
                stringList1.Add(str);
            }
            string str5 = SHA256.ComputeString(string.Join(string.Empty, stringList1.ToArray()));
            source.Add(str5);
            if (prefabs.Count > 1 && (SettingsUtility.IsAggregateLODEnabled() || SettingsUtility.IsProxyLODEnabled()))
                source.Add(SHA256.ComputeString(Guid.NewGuid().ToString()));
            return SHA256.ComputeString(string.Join(string.Empty, source.Distinct<string>().OrderBy<string, string>((Func<string, string>)(s => s)).ToArray<string>()));
        }
    }
}
