﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.TextureUtility
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.Globalization;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    internal class TextureUtility
    {
        public static Texture2D CreateTexture(int width, int height, Color color)
        {
            Texture2D texture2D = new Texture2D(width, height);
            Color[] colors = new Color[width * height];
            for (int index = 0; index < colors.Length; ++index)
                colors[index] = color;
            texture2D.SetPixels(colors);
            texture2D.Apply();
            return texture2D;
        }

        public static void CreateTexture(Texture2D sourceRgbTexture, Texture2D sourceAlphaTexture, string assetFullMaterialDirectory)
        {
            string assetPath1 = AssetDatabase.GetAssetPath(sourceRgbTexture);
            string assetPath2 = AssetDatabase.GetAssetPath(sourceAlphaTexture);
            TextureImporter atPath1 = AssetImporter.GetAtPath(assetPath1) as TextureImporter;
            TextureImporter atPath2 = AssetImporter.GetAtPath(assetPath2) as TextureImporter;
            if (null != atPath1 && null != atPath2)
            {
                int maxTextureSize1 = atPath1.maxTextureSize;
                int maxTextureSize2 = atPath2.maxTextureSize;
                try
                {
                    atPath1.isReadable = true;
                    atPath2.isReadable = true;
                    atPath1.maxTextureSize = 2048;
                    atPath2.maxTextureSize = 2048;
                    AssetDatabase.ImportAsset(assetPath1);
                    AssetDatabase.ImportAsset(assetPath2);
                    Texture2D texture2D = new Texture2D(sourceRgbTexture.width, sourceRgbTexture.height, TextureFormat.ARGB32, sourceRgbTexture.mipmapCount > 1);
                    texture2D.hideFlags = HideFlags.HideAndDontSave;
                    for (int y = 0; y < sourceAlphaTexture.height; ++y)
                    {
                        for (int x = 0; x < sourceAlphaTexture.width; ++x)
                        {
                            float r = sourceAlphaTexture.GetPixel(x, y).r;
                            if (y < texture2D.height && x < texture2D.width)
                            {
                                Color color = new Color(sourceRgbTexture.GetPixel(x, y).r, sourceRgbTexture.GetPixel(x, y).g, sourceRgbTexture.GetPixel(x, y).b, r);
                                texture2D.SetPixel(x, y, color);
                            }
                        }
                    }
                    texture2D.Apply();
                    byte[] png = texture2D.EncodeToPNG();
                    UnityEngine.Object.DestroyImmediate(texture2D);
                    File.WriteAllBytes(Path.Combine(assetFullMaterialDirectory, Path.GetFileName(assetPath1)), png);
                }
                catch (Exception ex)
                {
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to set alpha data to RGB texture ({0}, {1})", (object)ex.Message, (object)ex.StackTrace));
                }
                finally
                {
                    atPath1.isReadable = false;
                    atPath2.isReadable = false;
                    atPath1.maxTextureSize = maxTextureSize1;
                    atPath2.maxTextureSize = maxTextureSize2;
                    AssetDatabase.ImportAsset(assetPath1);
                    AssetDatabase.ImportAsset(assetPath2);
                }
            }
            else
                Debug.LogWarning((object)"Simplygon: Failed to set alpha data to RGB texture");
        }
    }
}
