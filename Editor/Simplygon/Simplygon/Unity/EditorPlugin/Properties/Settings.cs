﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.Properties.Settings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Simplygon.Unity.EditorPlugin.Properties
{
  [CompilerGenerated]
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default
    {
      get
      {
        return Settings.defaultInstance;
      }
    }

    [ApplicationScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("2.5.1")]
    public string Version
    {
      get
      {
        return (string) this[nameof (Version)];
      }
    }

    [ApplicationScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("2")]
    public int MajorVersion
    {
      get
      {
        return (int) this[nameof (MajorVersion)];
      }
    }

    [ApplicationScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("5")]
    public int MinorVersion
    {
      get
      {
        return (int) this[nameof (MinorVersion)];
      }
    }

    [ApplicationScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("1")]
    public int BuildVersion
    {
      get
      {
        return (int) this[nameof (BuildVersion)];
      }
    }
  }
}
