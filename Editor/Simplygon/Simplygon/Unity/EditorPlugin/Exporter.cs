﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Unity.EditorPlugin.Exporter
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json;
using Simplygon.Cloud.Unity;
using Simplygon.Math;
using Simplygon.Scene.Asset;
using Simplygon.Scene.Common.WorkDirectory;
using Simplygon.SPL.v80.Node;
using Simplygon.SPL.v80.Processor;
using Simplygon.Unity.EditorPlugin.Jobs;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Simplygon.Unity.EditorPlugin
{
    public class Exporter
    {
        private readonly Dictionary<BasicShadingNodeType, Action<Exporter.ShadingNetworkBuildState, UnityEngine.Material, BasicShadingNodeProperties>> BasicNodeTypeToAction = new Dictionary<BasicShadingNodeType, Action<Exporter.ShadingNetworkBuildState, UnityEngine.Material, BasicShadingNodeProperties>>()
        {
          {
            BasicShadingNodeType.Color,
            new Action<ShadingNetworkBuildState, UnityEngine.Material, BasicShadingNodeProperties>(ShadingNetworkBuilder.Instance.AddShadingColorNode)
          },
          {
            BasicShadingNodeType.Texture,
            new Action<ShadingNetworkBuildState, UnityEngine.Material, BasicShadingNodeProperties>(ShadingNetworkBuilder.Instance.AddShadingTextureNode)
          },
          {
            BasicShadingNodeType.Float,
            new Action<ShadingNetworkBuildState, UnityEngine.Material, BasicShadingNodeProperties>(ShadingNetworkBuilder.Instance.AddShadingFloatModifier)
          },
          {
            BasicShadingNodeType.Vector,
            new Action<ShadingNetworkBuildState, UnityEngine.Material, BasicShadingNodeProperties>(ShadingNetworkBuilder.Instance.AddShadingColorNode)
          },
          {
            BasicShadingNodeType.VertexColor,
            new Action<ShadingNetworkBuildState, UnityEngine.Material, BasicShadingNodeProperties>(ShadingNetworkBuilder.Instance.AddShadingVertexColorNode)
          }
        };

        private readonly Dictionary<ModifierShadingNodeType, Action<Exporter.ShadingNetworkBuildState, int, int[], int[]>> ModifierNodeTypeToAction = new Dictionary<ModifierShadingNodeType, Action<Exporter.ShadingNetworkBuildState, int, int[], int[]>>()
        {
          {
            ModifierShadingNodeType.Clamp,
            new Action<ShadingNetworkBuildState, int, int[], int[]>(ShadingNetworkBuilder.Instance.AddShadingClampNode)
          },
          {
            ModifierShadingNodeType.Interpolate,
            new Action<ShadingNetworkBuildState, int, int[], int[]>(ShadingNetworkBuilder.Instance.AddShadingInterpolateNode)
          },
          {
            ModifierShadingNodeType.Add,
            new Action<ShadingNetworkBuildState, int, int[], int[]>(ShadingNetworkBuilder.Instance.AddShadingAddNode)
          },
          {
            ModifierShadingNodeType.Subtract,
            new Action<ShadingNetworkBuildState, int, int[], int[]>(ShadingNetworkBuilder.Instance.AddShadingSubtractNode)
          },
          {
            ModifierShadingNodeType.Multiply,
            new Action<ShadingNetworkBuildState, int, int[], int[]>(ShadingNetworkBuilder.Instance.AddShadingMultiplyNode)
          },
          {
            ModifierShadingNodeType.Divide,
            new Action<ShadingNetworkBuildState, int, int[], int[]>(ShadingNetworkBuilder.Instance.AddShadingDivideNode)
          },
          {
            ModifierShadingNodeType.Swizzle,
            new Action<ShadingNetworkBuildState, int, int[], int[]>(ShadingNetworkBuilder.Instance.AddShadingSwizzleNode)
          },
          {
            ModifierShadingNodeType.Max,
            new Action<ShadingNetworkBuildState, int, int[], int[]>(ShadingNetworkBuilder.Instance.AddShadingMaxNode)
          },
          {
            ModifierShadingNodeType.Min,
            new Action<ShadingNetworkBuildState, int, int[], int[]>(ShadingNetworkBuilder.Instance.AddShadingMinNode)
          },
          {
            ModifierShadingNodeType.Step,
            new Action<ShadingNetworkBuildState, int, int[], int[]>(ShadingNetworkBuilder.Instance.AddShadingStepNode)
          }
        };

        private Dictionary<string, List<ShadingChannelHandler>> shaderHandlers = new Dictionary<string, List<ShadingChannelHandler>>();
        private Dictionary<string, List<ShadingChannelHandler>> shadingChannelHandlers = new Dictionary<string, List<ShadingChannelHandler>>();
        private Scene.Scene simplygonScene;
        private WorkDirectoryInfo workDirectory;

        public string SettingsFile
        {
            get; private set;
        }

        public string WDHFile
        {
            get; private set;
        }

        public string AssetFile
        {
            get; private set;
        }

        protected bool SkinnedMesh
        {
            get; private set;
        }

        protected List<GameObject> RootBones
        {
            get; private set;
        }

        protected Dictionary<Guid, int> BoneIdToBoneIndexMapping
        {
            get; private set;
        }

        protected Dictionary<Transform, Guid> BoneTransformToBoneIdMapping
        {
            get; private set;
        }

        protected Dictionary<string, Transform[]> MeshBonesMapping
        {
            get; private set;
        }

        protected Dictionary<Transform, SkinnedMeshRenderer> BoneToSkinnedMeshRendererMapping
        {
            get; private set;
        }

        protected Dictionary<Transform, Matrix4x4> NormalizedBoneMapping
        {
            get; private set;
        }

        private bool IsMaterialLODEnabled()
        {
            bool flag = false;
            for (int index = 0; index < SharedData.Instance.LODChain.Count; ++index)
            {
                ProcessNode processNode = SharedData.Instance.LODChain[index];
                if (processNode.Processor is ReductionProcessor)
                {
                    if (((ReductionProcessor)processNode.Processor).MappingImageSettings.Enabled)
                        return true;
                }
                else if (processNode.Processor is RemeshingProcessor)
                {
                    if (((RemeshingProcessor)processNode.Processor).MappingImageSettings.Enabled)
                        return true;
                }
                else if (processNode.Processor is AggregationProcessor && ((AggregationProcessor)processNode.Processor).MappingImageSettings.Enabled)
                    return true;
            }
            return flag;
        }

        public Exporter()
        {
            this.BoneIdToBoneIndexMapping = new Dictionary<Guid, int>();
            this.BoneTransformToBoneIdMapping = new Dictionary<Transform, Guid>();
            this.MeshBonesMapping = new Dictionary<string, Transform[]>();
            this.GenerateShaderHandlers();
        }

        internal Simplygon.Scene.Scene Export(GameObject gameObject, WorkDirectoryInfo workDirectory)
        {
            this.workDirectory = workDirectory;
            this.simplygonScene = new Simplygon.Scene.Scene();
            this.simplygonScene.SceneDirectory = workDirectory.RootDirectory;
            this.RootBones = new List<GameObject>();
            SettingsUtility.ResetChannelCastSettings(SharedData.Instance.LODChain);
            GameObject gameObject1 = UnityEngine.Object.Instantiate<GameObject>(gameObject, UnityEngine.Vector3.zero, gameObject.transform.localRotation);
            try
            {
                gameObject1.transform.localScale = gameObject.transform.localScale;
                gameObject1.name = gameObject.name;
                this.BoneIdToBoneIndexMapping = new Dictionary<Guid, int>();
                this.BoneTransformToBoneIdMapping = new Dictionary<Transform, Guid>();
                this.MeshBonesMapping = new Dictionary<string, Transform[]>();
                this.BoneToSkinnedMeshRendererMapping = new Dictionary<Transform, SkinnedMeshRenderer>();
                this.NormalizedBoneMapping = new Dictionary<Transform, Matrix4x4>();
                this.AnalyzeHierarchyForRootBones(gameObject1);
                Simplygon.Scene.Graph.Node sceneNode = this.CreateSceneNode(gameObject1);
                this.simplygonScene.AddNode(sceneNode, (Simplygon.Scene.Graph.Node)null);
                this.ExportSceneMesh(this.simplygonScene, sceneNode, gameObject1);
                if (this.simplygonScene.RootNode == null)
                    this.simplygonScene.SetRootNode(sceneNode);
            }
            catch
            {
                throw;
            }
            finally
            {
                UnityEngine.Object.DestroyImmediate(gameObject1);
            }
            return this.simplygonScene;
        }

        private void GenerateShaderHandlers()
        {
            Dictionary<string, List<ShadingChannelProperties>> dictionary;
            try
            {
                dictionary = JsonConvert.DeserializeObject<Dictionary<string, List<ShadingChannelProperties>>>(SharedData.Instance.UserSettings.UnityShaderSettings);
            }
            catch (Exception ex)
            {
                dictionary = SharedData.Instance.ShaderSettings;
            }
            foreach (KeyValuePair<string, List<ShadingChannelProperties>> keyValuePair in dictionary)
            {
                List<Exporter.ShadingChannelHandler> shadingChannelHandlerList = new List<Exporter.ShadingChannelHandler>();
                foreach (ShadingChannelProperties shadingChannelProperties in keyValuePair.Value)
                {
                    List<Exporter.IShadingNodeHandler> shadingNodeHandlers = new List<Exporter.IShadingNodeHandler>();
                    foreach (BasicShadingNodeProperties shadingNodesProperty in shadingChannelProperties.BasicShadingNodesProperties)
                    {
                        Action<Exporter.ShadingNetworkBuildState, UnityEngine.Material, BasicShadingNodeProperties> createNode;
                        if (this.BasicNodeTypeToAction.TryGetValue(shadingNodesProperty.NodeType, out createNode))
                            shadingNodeHandlers.Add((Exporter.IShadingNodeHandler)new Exporter.BasicShadingNodeHandler(shadingNodesProperty, createNode));
                        else
                            Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Basic shading node type '{0}' not supported.", (object)shadingNodesProperty.NodeType.ToString()));
                    }
                    foreach (ModifierShadingNodeProperties shadingNodesProperty in shadingChannelProperties.ModifierShadingNodesProperties)
                    {
                        Action<Exporter.ShadingNetworkBuildState, int, int[], int[]> createNode;
                        if (this.ModifierNodeTypeToAction.TryGetValue(shadingNodesProperty.NodeType, out createNode))
                            shadingNodeHandlers.Add((Exporter.IShadingNodeHandler)new Exporter.ModifierShadingNodeHandler(shadingNodesProperty, createNode));
                        else
                            Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Modifier shading node type '{0}' not supported.", (object)shadingNodesProperty.NodeType.ToString()));
                    }
                    Exporter.ShadingChannelHandler shadingChannelHandler = new Exporter.ShadingChannelHandler(shadingChannelProperties, shadingNodeHandlers);
                    shadingChannelHandlerList.Add(shadingChannelHandler);
                }
                this.shaderHandlers.Add(keyValuePair.Key, shadingChannelHandlerList);
            }
        }

        private void NormalizeBones(GameObject gameObject)
        {
            if (gameObject == null)
                return;

            var component1 = gameObject.GetComponent(typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer;
            if (component1 != null)
            {
                for (int index = 0; index < component1.bones.Length; ++index)
                {
                    Transform bone = component1.bones[index];
                    Matrix4x4 bindpose = component1.sharedMesh.bindposes[index];
                    Matrix4x4 matrix4x4 = component1.transform.localToWorldMatrix * bindpose.inverse;
                    this.NormalizedBoneMapping.Add(bone, matrix4x4);
                    component1.sharedMesh.bindposes[index] = matrix4x4.inverse * component1.transform.localToWorldMatrix;
                }
            }
            foreach (Component component2 in gameObject.transform)
                this.NormalizeBones(component2.gameObject);
        }

        private void AnalyzeHierarchyForRootBones(GameObject gameObject)
        {
            if (gameObject == null)
                return;
            SkinnedMeshRenderer component1 = gameObject.GetComponent(typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer;
            if (component1 != null)
            {
                this.SkinnedMesh = true;
                if (component1.rootBone != null)
                    this.RootBones.Add(component1.rootBone.gameObject);
            }
            foreach (Component component2 in gameObject.transform)
                this.AnalyzeHierarchyForRootBones(component2.gameObject);
        }

        private bool IsBoneNode(GameObject gameObject)
        {
            if (gameObject == null || this.RootBones.Count == 0)
                return false;
            foreach (GameObject rootBone in this.RootBones)
            {
                if (rootBone == gameObject || gameObject.transform.IsChildOf(rootBone.transform))
                    return true;
            }
            return false;
        }

        private int AddBoneIndex(Guid boneId, Transform boneTransform)
        {
            int count;
            if (this.BoneIdToBoneIndexMapping.ContainsKey(boneId))
            {
                count = this.BoneIdToBoneIndexMapping[boneId];
            }
            else
            {
                count = this.BoneIdToBoneIndexMapping.Count;
                this.BoneIdToBoneIndexMapping.Add(boneId, count);
                this.BoneTransformToBoneIdMapping.Add(boneTransform, boneId);
            }
            return count;
        }

        private Simplygon.Scene.Graph.Node CreateSceneNode(GameObject gameObject)
        {
            Simplygon.Scene.Graph.Node parent = new Simplygon.Scene.Graph.Node(Guid.NewGuid(), string.IsNullOrEmpty(gameObject.name) ? "N/A" : gameObject.name);
            Matrix matrix = new Matrix();
            Matrix4x4 matrix4x4 = Matrix4x4.TRS(gameObject.transform.localPosition, gameObject.transform.localRotation, gameObject.transform.localScale);
            if (this.IsBoneNode(gameObject) && this.AddBoneIndex(parent.Id, gameObject.transform) >= 0)
                parent.IsBone = true;
            matrix.M11 = (double)matrix4x4[0, 0];
            matrix.M12 = (double)matrix4x4[1, 0];
            matrix.M13 = (double)matrix4x4[2, 0];
            matrix.M14 = (double)matrix4x4[3, 0];
            matrix.M21 = (double)matrix4x4[0, 1];
            matrix.M22 = (double)matrix4x4[1, 1];
            matrix.M23 = (double)matrix4x4[2, 1];
            matrix.M24 = (double)matrix4x4[3, 1];
            matrix.M31 = (double)matrix4x4[0, 2];
            matrix.M32 = (double)matrix4x4[1, 2];
            matrix.M33 = (double)matrix4x4[2, 2];
            matrix.M34 = (double)matrix4x4[3, 2];
            matrix.M41 = (double)matrix4x4[0, 3];
            matrix.M42 = (double)matrix4x4[1, 3];
            matrix.M43 = (double)matrix4x4[2, 3];
            matrix.M44 = (double)matrix4x4[3, 3];
            parent.LocalTransform = matrix;
            SkinnedMeshRenderer component1 = gameObject.GetComponent(typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer;
            if (component1 != null)
            {
                UnityEngine.Mesh sharedMesh = component1.sharedMesh;
                if (!this.MeshBonesMapping.ContainsKey(sharedMesh.name))
                {
                    if (component1.bones.Length != 0)
                    {
                        this.MeshBonesMapping.Add(sharedMesh.name, component1.bones);
                    }
                    else
                    {
                        Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Skinned mesh renderer '{0}' has no bones, consider using a mesh renderer instead.", (object)gameObject.name));
                        this.MeshBonesMapping.Add(sharedMesh.name, new Transform[1]
                        {
              gameObject.transform
                        });
                    }
                }
            }
            if (this.SkinnedMesh)
            {
                MeshFilter component2 = gameObject.GetComponent(typeof(MeshFilter)) as MeshFilter;
                if (component2 != null)
                {
                    UnityEngine.Mesh sharedMesh = component2.sharedMesh;
                    if (!this.MeshBonesMapping.ContainsKey(sharedMesh.name))
                        this.MeshBonesMapping.Add(sharedMesh.name, new Transform[1]
                        {
              gameObject.transform
                        });
                }
            }
            foreach (Component component2 in gameObject.transform)
                this.simplygonScene.AddNode(this.CreateSceneNode(component2.gameObject), parent);
            return parent;
        }

        private void ExportSceneMesh(Simplygon.Scene.Scene simplygonScene, Simplygon.Scene.Graph.Node sceneNode, GameObject gameObject)
        {
            SkinnedMeshRenderer component1 = gameObject.GetComponent(typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer;
            if (component1 != null)
            {
                UnityEngine.Mesh sharedMesh = component1.sharedMesh;
                Dictionary<int, int> unityMaterialIndexToSimplygonMaterialIndexMapping = new Dictionary<int, int>();
                HashSet<Guid> guidSet = new HashSet<Guid>();
                List<Guid> materialIds = new List<Guid>();
                foreach (UnityEngine.Material sharedMaterial in component1.sharedMaterials)
                {
                    Guid material = this.CreateMaterial(sharedMaterial);
                    if (!guidSet.Contains(material))
                    {
                        guidSet.Add(material);
                        materialIds.Add(material);
                    }
                    unityMaterialIndexToSimplygonMaterialIndexMapping.Add(unityMaterialIndexToSimplygonMaterialIndexMapping.Count, materialIds.IndexOf(material));
                    materialIds.Add(material);
                }
                Guid mesh = this.CreateMesh(sharedMesh, materialIds, unityMaterialIndexToSimplygonMaterialIndexMapping);
                sceneNode.SetMeshID(simplygonScene, mesh);
            }
            else
            {
                MeshFilter component2 = gameObject.GetComponent(typeof(MeshFilter)) as MeshFilter;
                MeshRenderer component3 = gameObject.GetComponent(typeof(MeshRenderer)) as MeshRenderer;
                if (component2 != null && component3 != null)
                {
                    Dictionary<int, int> unityMaterialIndexToSimplygonMaterialIndexMapping = new Dictionary<int, int>();
                    HashSet<Guid> guidSet = new HashSet<Guid>();
                    List<Guid> materialIds = new List<Guid>();
                    foreach (UnityEngine.Material sharedMaterial in component3.sharedMaterials)
                    {
                        Guid material = this.CreateMaterial(sharedMaterial);
                        if (!guidSet.Contains(material))
                        {
                            guidSet.Add(material);
                            materialIds.Add(material);
                        }
                        unityMaterialIndexToSimplygonMaterialIndexMapping.Add(unityMaterialIndexToSimplygonMaterialIndexMapping.Count, materialIds.IndexOf(material));
                        materialIds.Add(material);
                    }
                    Guid mesh = this.CreateMesh(component2.sharedMesh, materialIds, unityMaterialIndexToSimplygonMaterialIndexMapping);
                    sceneNode.SetMeshID(simplygonScene, mesh);
                }
            }
            for (int index = 0; index < sceneNode.Children.Count; ++index)
            {
                Guid child = sceneNode.Children[index];
                GameObject gameObject1 = gameObject.transform.GetChild(index).gameObject;
                this.ExportSceneMesh(simplygonScene, simplygonScene.GetNodeById(child), gameObject1);
            }
        }

        private Guid CreateMesh(UnityEngine.Mesh mesh, List<Guid> materialIds, Dictionary<int, int> unityMaterialIndexToSimplygonMaterialIndexMapping)
        {
            foreach (KeyValuePair<Guid, Simplygon.Scene.Asset.Mesh> mesh1 in this.simplygonScene.Meshes)
            {
                if (mesh1.Value.Name == mesh.name)
                {
                    bool flag = true;
                    foreach (Guid materialId in materialIds)
                    {
                        if (!mesh1.Value.Materials.Contains(materialId))
                            flag = false;
                    }
                    if (flag)
                        return mesh1.Value.Id;
                }
            }
            SharedData.Instance.Profiler.BeginSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh ", (object)mesh.name));
            Simplygon.Scene.Asset.Mesh mesh2 = new Simplygon.Scene.Asset.Mesh(Guid.NewGuid(), string.Format(CultureInfo.InvariantCulture, "{0}", (object)mesh.name));
            MeshData data = new MeshData(mesh2);
            mesh2.ClearMeshSources();
            mesh2.Materials.AddRange((IEnumerable<Guid>)materialIds);
            SharedData.Instance.Profiler.BeginSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh Triangles {0}", (object)mesh2.Name));
            List<int> intList = new List<int>();
            for (int submesh = 0; submesh < mesh.subMeshCount; ++submesh)
            {
                int[] triangles = mesh.GetTriangles(submesh);
                intList.AddRange((IEnumerable<int>)triangles);
            }
            data.Triangles = intList.ToArray();
            SharedData.Instance.Profiler.EndSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh Triangles {0}", (object)mesh2.Name));
            SharedData.Instance.Profiler.BeginSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh Vertices {0}", (object)mesh2.Name));
            List<Simplygon.Math.Vector4> vector4List = new List<Simplygon.Math.Vector4>();
            UnityEngine.Vector3[] vertices = mesh.vertices;
            vector4List.Capacity = vertices.Length;
            foreach (UnityEngine.Vector3 vector3 in vertices)
                vector4List.Add(new Simplygon.Math.Vector4((double)vector3.x, (double)vector3.y, (double)vector3.z, 1.0));
            data.Coordinates = vector4List.ToArray();
            SharedData.Instance.Profiler.EndSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh Vertices {0}", (object)mesh2.Name));
            int maxNumUvs = SceneUtils.GetMaxNumUVs();
            for (int channel = 0; channel < maxNumUvs; ++channel)
            {
                List<UnityEngine.Vector2> uvs = new List<UnityEngine.Vector2>();
                mesh.GetUVsWrapper(channel, ref uvs);
                if (uvs != null && uvs.Count > 0)
                {
                    SharedData.Instance.Profiler.BeginSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh uv {0} for {1}", (object)channel, (object)mesh2.Name));
                    string key = "TexCoords" + (object)channel;
                    List<Simplygon.Math.Vector2> vector2List = new List<Simplygon.Math.Vector2>();
                    vector2List.Capacity = intList.Count;
                    foreach (int index in intList)
                    {
                        UnityEngine.Vector2 vector2 = uvs[index];
                        vector2List.Add(new Simplygon.Math.Vector2((double)vector2.x, 1.0 - (double)vector2.y));
                    }
                    data.TexCoords.Add(key, vector2List.ToArray());
                    SharedData.Instance.Profiler.EndSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh uv {0} for {1}", (object)channel, (object)mesh2.Name));
                }
            }
            UnityEngine.Vector3[] normals = mesh.normals;
            if (normals != null && normals.Length != 0)
            {
                SharedData.Instance.Profiler.BeginSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh Normals {0}", (object)mesh2.Name));
                List<Simplygon.Math.Vector3> vector3List = new List<Simplygon.Math.Vector3>();
                vector3List.Capacity = intList.Count;
                foreach (int index in intList)
                {
                    UnityEngine.Vector3 vector3 = normals[index];
                    vector3List.Add(new Simplygon.Math.Vector3((double)vector3.x, (double)vector3.y, (double)vector3.z));
                }
                data.Normals = vector3List.ToArray();
                SharedData.Instance.Profiler.EndSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh Normals {0}", (object)mesh2.Name));
            }
            UnityEngine.Vector4[] tangents = mesh.tangents;
            if (tangents != null && tangents.Length != 0)
            {
                SharedData.Instance.Profiler.BeginSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh Tangents {0}", (object)mesh2.Name));
                List<Simplygon.Math.Vector3> vector3List = new List<Simplygon.Math.Vector3>();
                vector3List.Capacity = intList.Count;
                foreach (int index in intList)
                {
                    UnityEngine.Vector4 vector4 = tangents[index];
                    vector3List.Add(new Simplygon.Math.Vector3((double)vector4.x, (double)vector4.y, (double)vector4.z));
                }
                data.Tangents = vector3List.ToArray();
                SharedData.Instance.Profiler.EndSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh Tangents {0}", (object)mesh2.Name));
            }
            if (normals != null && tangents != null && (normals.Length != 0 && tangents.Length != 0))
            {
                SharedData.Instance.Profiler.BeginSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh Bitangents {0}", (object)mesh2.Name));
                List<Simplygon.Math.Vector3> vector3List = new List<Simplygon.Math.Vector3>();
                vector3List.Capacity = intList.Count;
                foreach (int index in intList)
                {
                    UnityEngine.Vector3 lhs = normals[index];
                    UnityEngine.Vector4 vector4 = tangents[index];
                    UnityEngine.Vector3 rhs = new UnityEngine.Vector3(vector4.x, vector4.y, vector4.z);
                    UnityEngine.Vector3 vector3 = UnityEngine.Vector3.Cross(lhs, rhs) * vector4.w;
                    vector3List.Add(new Simplygon.Math.Vector3((double)vector3.x, (double)vector3.y, (double)vector3.z));
                }
                data.Bitangents = vector3List.ToArray();
                SharedData.Instance.Profiler.EndSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh Bitangents {0}", (object)mesh2.Name));
            }
            UnityEngine.Color[] colors = mesh.colors;
            if (colors != null && colors.Length != 0)
            {
                SharedData.Instance.Profiler.BeginSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh Colors {0}", (object)mesh2.Name));
                List<Simplygon.Math.Color> colorList = new List<Simplygon.Math.Color>();
                colorList.Capacity = intList.Count;
                foreach (int index in intList)
                {
                    UnityEngine.Color color = colors[index];
                    colorList.Add(new Simplygon.Math.Color(color.r, color.g, color.b, color.a, 32));
                }
                data.VertexColors.Add("Colors", colorList.ToArray());
                SharedData.Instance.Profiler.EndSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh Colors {0}", (object)mesh2.Name));
            }
            BoneWeight[] boneWeights = mesh.boneWeights;
            if (boneWeights != null && boneWeights.Length != 0)
            {
                SharedData.Instance.Profiler.BeginSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh Bones {0}", (object)mesh2.Name));
                if (this.MeshBonesMapping.ContainsKey(mesh.name))
                {
                    data.BoneIndices = new int[boneWeights.Length, 4];
                    data.BoneWeights = new float[boneWeights.Length, 4];
                    Transform[] transformArray = this.MeshBonesMapping[mesh.name];
                    for (int index = 0; index < boneWeights.Length; ++index)
                    {
                        BoneWeight boneWeight = boneWeights[index];
                        int num1 = -1;
                        int num2 = -1;
                        int num3 = -1;
                        int num4 = -1;
                        if (boneWeight.boneIndex0 >= 0 && (double)boneWeight.weight0 > 0.0)
                        {
                            Transform key = transformArray[boneWeight.boneIndex0];
                            if (!this.BoneTransformToBoneIdMapping.ContainsKey(key))
                            {
                                string str = this.RootBones == null || this.RootBones.Count == 0 ? "." : string.Format(CultureInfo.InvariantCulture, " ({0}).", (object)string.Join(", ", this.RootBones.Select<GameObject, string>((Func<GameObject, string>)(rootbone => rootbone.name)).ToArray<string>()));
                                Debug.Log((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to find bone transform to bone ID mapping for bone {0}, please make sure that bone {0} is a child of one of the root bones{1}", (object)key.gameObject.name, (object)str));
                            }
                            Guid guid = this.BoneTransformToBoneIdMapping[key];
                            num1 = mesh2.Bones.IndexOf(guid);
                            if (num1 < 0)
                            {
                                num1 = mesh2.Bones.Count;
                                mesh2.Bones.Add(guid);
                            }
                        }
                        if (boneWeight.boneIndex1 >= 0 && (double)boneWeight.weight1 > 0.0)
                        {
                            Transform key = transformArray[boneWeight.boneIndex1];
                            if (!this.BoneTransformToBoneIdMapping.ContainsKey(key))
                            {
                                string str = this.RootBones == null || this.RootBones.Count == 0 ? "." : string.Format(CultureInfo.InvariantCulture, " ({0}).", (object)string.Join(", ", this.RootBones.Select<GameObject, string>((Func<GameObject, string>)(rootbone => rootbone.name)).ToArray<string>()));
                                Debug.Log((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to find bone transform to bone ID mapping for bone {0}, please make sure that bone {0} is a child of one of the root bones{1}", (object)key.gameObject.name, (object)str));
                            }
                            Guid guid = this.BoneTransformToBoneIdMapping[key];
                            num2 = mesh2.Bones.IndexOf(guid);
                            if (num2 < 0)
                            {
                                num2 = mesh2.Bones.Count;
                                mesh2.Bones.Add(guid);
                            }
                        }
                        if (boneWeight.boneIndex2 >= 0 && (double)boneWeight.weight2 > 0.0)
                        {
                            Transform key = transformArray[boneWeight.boneIndex2];
                            if (!this.BoneTransformToBoneIdMapping.ContainsKey(key))
                            {
                                string str = this.RootBones == null || this.RootBones.Count == 0 ? "." : string.Format(CultureInfo.InvariantCulture, " ({0}).", (object)string.Join(", ", this.RootBones.Select<GameObject, string>((Func<GameObject, string>)(rootbone => rootbone.name)).ToArray<string>()));
                                Debug.Log((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to find bone transform to bone ID mapping for bone {0}, please make sure that bone {0} is a child of one of the root bones{1}", (object)key.gameObject.name, (object)str));
                            }
                            Guid guid = this.BoneTransformToBoneIdMapping[key];
                            num3 = mesh2.Bones.IndexOf(guid);
                            if (num3 < 0)
                            {
                                num3 = mesh2.Bones.Count;
                                mesh2.Bones.Add(guid);
                            }
                        }
                        if (boneWeight.boneIndex3 >= 0 && (double)boneWeight.weight3 > 0.0)
                        {
                            Transform key = transformArray[boneWeight.boneIndex3];
                            if (!this.BoneTransformToBoneIdMapping.ContainsKey(key))
                            {
                                string str = this.RootBones == null || this.RootBones.Count == 0 ? "." : string.Format(CultureInfo.InvariantCulture, " ({0}).", (object)string.Join(", ", this.RootBones.Select<GameObject, string>((Func<GameObject, string>)(rootbone => rootbone.name)).ToArray<string>()));
                                Debug.Log((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to find bone transform to bone ID mapping for bone {0}, please make sure that bone {0} is a child of one of the root bones{1}", (object)key.gameObject.name, (object)str));
                            }
                            Guid guid = this.BoneTransformToBoneIdMapping[key];
                            num4 = mesh2.Bones.IndexOf(guid);
                            if (num4 < 0)
                            {
                                num4 = mesh2.Bones.Count;
                                mesh2.Bones.Add(guid);
                            }
                        }
                        data.BoneIndices[index, 0] = num1;
                        data.BoneIndices[index, 1] = num2;
                        data.BoneIndices[index, 2] = num3;
                        data.BoneIndices[index, 3] = num4;
                        data.BoneWeights[index, 0] = boneWeight.weight0;
                        data.BoneWeights[index, 1] = boneWeight.weight1;
                        data.BoneWeights[index, 2] = boneWeight.weight2;
                        data.BoneWeights[index, 3] = boneWeight.weight3;
                    }
                }
                SharedData.Instance.Profiler.EndSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh Bones {0}", (object)mesh2.Name));
            }
            else if (this.MeshBonesMapping.ContainsKey(mesh.name))
            {
                data.BoneIndices = new int[vertices.Length, 4];
                data.BoneWeights = new float[vertices.Length, 4];
                Transform[] transformArray = this.MeshBonesMapping[mesh.name];
                mesh2.Bones.Add(this.BoneTransformToBoneIdMapping[transformArray[0]]);
                for (int index = 0; index < vertices.Length; ++index)
                {
                    data.BoneIndices[index, 0] = 0;
                    data.BoneIndices[index, 1] = -1;
                    data.BoneIndices[index, 2] = -1;
                    data.BoneIndices[index, 3] = -1;
                    data.BoneWeights[index, 0] = 1f;
                    data.BoneWeights[index, 1] = 0.0f;
                    data.BoneWeights[index, 2] = 0.0f;
                    data.BoneWeights[index, 3] = 0.0f;
                }
            }
            SharedData.Instance.Profiler.BeginSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh MaterialIndices {0}", (object)mesh2.Name));
            data.MaterialIndices = new int[intList.Count / 3];
            int index1 = 0;
            for (int submesh = 0; submesh < mesh.subMeshCount; ++submesh)
            {
                int[] triangles = mesh.GetTriangles(submesh);
                int num1 = 0;
                while (num1 < triangles.Length)
                {
                    int num2 = unityMaterialIndexToSimplygonMaterialIndexMapping[submesh];
                    data.MaterialIndices[index1] = num2;
                    ++index1;
                    num1 += 3;
                }
            }
            SharedData.Instance.Profiler.EndSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh MaterialIndices {0}", (object)mesh2.Name));
            mesh2.AddMeshSourceData(data);
            this.simplygonScene.AddMesh(mesh2);
            SharedData.Instance.Profiler.EndSession(string.Format(CultureInfo.InvariantCulture, "CreateMesh ", (object)mesh.name));
            return mesh2.Id;
        }

        private Guid CreateMaterial(UnityEngine.Material material)
        {
            string assetPath = AssetDatabase.GetAssetPath(material);
            foreach (KeyValuePair<Guid, Simplygon.Scene.Asset.Material> material1 in this.simplygonScene.Materials)
            {
                if (material1.Value.Name == assetPath)
                    return material1.Value.Id;
            }
            SharedData.Instance.Profiler.BeginSession(string.Format(CultureInfo.InvariantCulture, "CreateMaterial ", (object)assetPath));
            Simplygon.Scene.Asset.Material material2 = new Simplygon.Scene.Asset.Material(Guid.NewGuid(), assetPath);
            if (this.IsMaterialLODEnabled())
            {
                List<Exporter.ShadingChannelHandler> shadingChannelHandlerList;
                if (this.shaderHandlers.TryGetValue(JobUtils.GetShaderLookupName(material.shader.name), out shadingChannelHandlerList) || material.HasProperty("_MainTex") && this.shaderHandlers.TryGetValue(JobUtils.SimplygonDefaultTextureShaderChannelProperties, out shadingChannelHandlerList) || (material.HasProperty("_Color") && this.shaderHandlers.TryGetValue(JobUtils.SimplygonDefaultColorShaderChannelProperties, out shadingChannelHandlerList) || this.shaderHandlers.TryGetValue(JobUtils.SimplygonDefaultColorShaderChannelProperties, out shadingChannelHandlerList)))
                {
                    List<Exporter.ShadingNetworkBuildState> otherNetworkBuildStates = new List<Exporter.ShadingNetworkBuildState>();
                    foreach (Exporter.ShadingChannelHandler shadingChannelHandler in shadingChannelHandlerList)
                        shadingChannelHandler.Handle(material, material2, this.simplygonScene, this.workDirectory, otherNetworkBuildStates);
                }
                else
                {
                    string str = string.Join(", ", this.shaderHandlers.Keys.ToArray<string>());
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unknown shader properties for shader '{0}' for material '{1}' - please select one of the supported shaders: {2}", (object)material.shader.name, (object)material.name, (object)str));
                }
            }
            this.simplygonScene.AddMaterial(material2);
            SharedData.Instance.Profiler.EndSession(string.Format(CultureInfo.InvariantCulture, "CreateMaterial ", (object)assetPath));
            return material2.Id;
        }

        public class Pair<T, U>
        {
            public Pair()
            {
            }

            public Pair(T first, U second)
            {
                this.First = first;
                this.Second = second;
            }

            public T First
            {
                get; set;
            }

            public U Second
            {
                get; set;
            }
        }

        public class ShadingNetworkBuildState
        {
            public string TexturesPath
            {
                get; protected set;
            }

            public string Xml
            {
                get; set;
            }

            public List<Exporter.ShadingNetworkBuildState.TextureInfo> TextureInfos
            {
                get; set;
            }

            public List<Exporter.ShadingNetworkBuildState> OtherBuildStates
            {
                get; protected set;
            }

            public bool Enabled
            {
                get; set;
            }

            public ShadingNetworkBuildState(string texturesPath, List<Exporter.ShadingNetworkBuildState> otherBuildStates)
            {
                this.TexturesPath = texturesPath;
                this.OtherBuildStates = otherBuildStates;
                this.Xml = string.Empty;
                this.TextureInfos = new List<Exporter.ShadingNetworkBuildState.TextureInfo>();
                this.Enabled = true;
            }

            public class TextureInfo
            {
                public Guid SimplygonTextureId
                {
                    get; protected set;
                }

                public string OriginalTexturePath
                {
                    get; protected set;
                }

                public string SimplygonTextureFileName
                {
                    get; protected set;
                }

                public TextureInfo(Guid simplygonTextureId, string originalTexturePath)
                {
                    this.SimplygonTextureId = simplygonTextureId;
                    this.OriginalTexturePath = originalTexturePath;
                    this.SimplygonTextureFileName = string.Format(CultureInfo.InvariantCulture, "{0}{1}", (object)simplygonTextureId.ToString(), (object)Path.GetExtension(originalTexturePath));
                }
            }
        }

        public class ShadingNetworkBuilder
        {
            private static readonly string SimplygonUseExplicitColor = "SimplygonUseExplicitColor:";
            private readonly string Header = "<?xml version=\"1.0\"?>\n<SimplygonShadingNetwork version=\"1.0\">\n";
            private readonly string Footer = "\n</SimplygonShadingNetwork>";
            private static Exporter.ShadingNetworkBuilder instance;

            public static Exporter.ShadingNetworkBuilder Instance
            {
                get
                {
                    if (Exporter.ShadingNetworkBuilder.instance == null)
                        Exporter.ShadingNetworkBuilder.instance = new Exporter.ShadingNetworkBuilder();
                    return Exporter.ShadingNetworkBuilder.instance;
                }
            }

            public static string GetExplicitColorAsPropertyName()
            {
                return Exporter.ShadingNetworkBuilder.SimplygonUseExplicitColor;
            }

            public static string GetExplicitColorAsPropertyName(string defaultColor)
            {
                return string.Format(CultureInfo.InvariantCulture, "{0}{1}", (object)Exporter.ShadingNetworkBuilder.SimplygonUseExplicitColor, (object)defaultColor);
            }

            public static UnityEngine.Vector4 GetExplicitColorVectorFromPropertyName(string propertyName)
            {
                UnityEngine.Vector4 zero = UnityEngine.Vector4.zero;
                try
                {
                    string[] strArray = propertyName.Remove(0, Exporter.ShadingNetworkBuilder.SimplygonUseExplicitColor.Length).Split(' ');
                    if (strArray.Length != 4)
                    {
                        Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected number of color components for property '{0}'", (object)propertyName));
                    }
                    else
                    {
                        for (int index = 0; index < strArray.Length; ++index)
                        {
                            float result = 0.0f;
                            if (!float.TryParse(strArray[index], NumberStyles.Any, CultureInfo.InvariantCulture, out result))
                                Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to parse color component '{0}' for property '{1}'", (object)strArray[index], (object)propertyName));
                            zero[index] = result;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to retrieve explicit color from property {0} ({1})", (object)propertyName, (object)ex.Message));
                }
                return zero;
            }

            public static UnityEngine.Color GetExplicitColorFromPropertyName(string propertyName)
            {
                UnityEngine.Vector4 fromPropertyName = Exporter.ShadingNetworkBuilder.GetExplicitColorVectorFromPropertyName(propertyName);
                return new UnityEngine.Color(fromPropertyName[0], fromPropertyName[1], fromPropertyName[2], fromPropertyName[3]);
            }

            internal void AddShadingColorNode(Exporter.ShadingNetworkBuildState state, UnityEngine.Material material, BasicShadingNodeProperties properties)
            {
                string shaderPropertyName = properties.ShaderPropertyName;
                if (shaderPropertyName.StartsWith(Exporter.ShadingNetworkBuilder.SimplygonUseExplicitColor))
                {
                    string str = shaderPropertyName.Substring(Exporter.ShadingNetworkBuilder.SimplygonUseExplicitColor.Length);
                    state.Xml = state.Xml.Insert(0, string.Format(CultureInfo.InvariantCulture, "<ShadingColorNode ref=\"node_{0}\" name=\"{1}\">  <Color0>    <DefaultValue>{2}</DefaultValue>  </Color0></ShadingColorNode>", (object)properties.NodeRef, (object)properties.NodeName, (object)str));
                }
                else
                {
                    if (!material.HasProperty(shaderPropertyName))
                        return;
                    UnityEngine.Color color = material.GetColor(shaderPropertyName);
                    state.Xml = state.Xml.Insert(0, string.Format(CultureInfo.InvariantCulture, "<ShadingColorNode ref=\"node_{0}\" name=\"{1}\">  <Color0>    <DefaultValue>{2} {3} {4} {5}</DefaultValue>  </Color0></ShadingColorNode>", (object)properties.NodeRef, (object)properties.NodeName, (object)color.r, (object)color.g, (object)color.b, (object)color.a));
                }
            }

            internal void AddShadingVertexColorNode(Exporter.ShadingNetworkBuildState state, UnityEngine.Material material, BasicShadingNodeProperties properties)
            {
                state.Xml = state.Xml.Insert(0, string.Format(CultureInfo.InvariantCulture, "<ShadingVertexColorNode ref=\"node_{0}\" name=\"{1}\">  <DefaultColor0>    <DefaultValue>1 1 1 1</DefaultValue>  </DefaultColor0>  <VertexColorIndex>-1</VertexColorIndex>  <VertexColorSet>Colors</VertexColorSet></ShadingVertexColorNode>", (object)properties.NodeRef, (object)properties.NodeName));
            }

            /// <summary>
            /// Adds the shading texture node.
            /// </summary>
            /// <param name="state">The state.</param>
            /// <param name="material">The material.</param>
            /// <param name="properties">The properties.</param>
            internal void AddShadingTextureNode(Exporter.ShadingNetworkBuildState state, UnityEngine.Material material, BasicShadingNodeProperties properties)
            {
                var simplygonTextureId = Guid.NewGuid();

                var tiling = new UnityEngine.Vector2(1f, 1f);
                var shaderPropertyName = properties.ShaderPropertyName;
                var texCoordsPropName = string.IsNullOrEmpty(properties.TexCoords) ? "TexCoords0" : properties.TexCoords;
                var texture = material.GetTexture(shaderPropertyName);

                var assetPath = AssetDatabase.GetAssetPath(texture);
                var textureInfo = default(ShadingNetworkBuildState.TextureInfo);

                // No texture
                if (texture == null ||
                    string.IsNullOrEmpty(assetPath))
                {
                    // Vars for generating a uniform tex
                    var uniformTexture = new Texture2D(2, 2);
                    var uniformColor = UnityEngine.Color.white;

                    // Standard Shader
                    if (material.shader.name == "Standard" ||
                        material.shader.name == "Standard (Specular setup)")
                    {
                        var disabledChannel = false;

                        // Emission
                        if (shaderPropertyName == "_EmissionMap")
                        {
                            uniformColor = material.GetColor("_EmissionColor");
                            disabledChannel = uniformColor.r == 0.0 &&
                                uniformColor.g == 0.0 &&
                                uniformColor.b == 0.0;
                        }
                        // Other maps
                        else
                        {
                            disabledChannel = shaderPropertyName == "_DetailAlbedoMap" ||
                                shaderPropertyName == "_DetailNormalMap" ||
                                shaderPropertyName == "_ParallaxMap" ||
                                shaderPropertyName == "_OcclusionMap" ||
                                shaderPropertyName == "_DetailMask";
                        }

                        // Early exit if disabled channel
                        if (disabledChannel)
                        {
                            state.Enabled = false;
                            return;
                        }

                        // Metallic
                        if ("Standard" == material.shader.name &&
                            "_MetallicGlossMap" == shaderPropertyName)
                        {
                            var glossiness = material.GetFloat("_Glossiness");
                            var metallic = material.GetFloat("_Metallic");
                            uniformColor = new UnityEngine.Color(metallic, metallic, metallic, glossiness);
                        }

                        // Specular
                        else if ("Standard (Specular setup)" == material.shader.name &&
                            "_SpecGlossMap" == shaderPropertyName)
                        {
                            var glossiness = material.GetFloat("_Glossiness");
                            var specColor = material.GetColor("_SpecColor");
                            uniformColor = new UnityEngine.Color(specColor.r, specColor.g, specColor.b, glossiness);
                        }

                        // Bump
                        else if ("_BumpMap" == shaderPropertyName ||
                            "_BumpTex" == shaderPropertyName)
                        {
                            uniformColor = new UnityEngine.Color(0.5f, 0.5f, 1f);
                        }
                    }

                    // HDRP Lit
                    else if (material.shader.name == "HDRP/Lit")
                    {
                        var disabledChannel = shaderPropertyName == "_BentNormalMap" ||
                            shaderPropertyName == "_EmissiveColorMap" ||
                            shaderPropertyName == "_CoatMaskMap" ||
                            shaderPropertyName == "_HeightMap";

                        // Early exit if disabled channel
                        if (disabledChannel)
                        {
                            state.Enabled = false;
                            return;
                        }

                        // Mask map
                        if ("_MaskMap" == shaderPropertyName)
                        {
                            var metallic = material.GetFloat("_Metallic");
                            var smoothness = material.GetFloat("_Smoothness");
                            uniformColor = new UnityEngine.Color(metallic, 1f, 0f, smoothness);
                        }

                        // Normal
                        else if ("_NormalMap" == shaderPropertyName)
                        {
                            uniformColor = new UnityEngine.Color(0.5f, 0.5f, 1f);
                        }
                    }

                    // Universal Lit
                    else if (material.shader.name == "Lightweight Render Pipeline/Lit")
                    {
                        var disabledChannel = false;

                        // Emission
                        if (shaderPropertyName == "_EmissionMap")
                        {
                            if (!material.IsKeywordEnabled("_EMISSION"))
                            {
                                disabledChannel = true;
                            }
                            else
                            {
                                uniformColor = material.GetColor("_EmissionColor");
                                disabledChannel = uniformColor.r == 0.0 &&
                                    uniformColor.g == 0.0 &&
                                    uniformColor.b == 0.0;
                            }
                        }
                        // Other maps
                        else
                        {
                            disabledChannel = shaderPropertyName == "_OcclusionMap";
                        }

                        // Early exit if disabled channel
                        if (disabledChannel)
                        {
                            state.Enabled = false;
                            return;
                        }

                        // Metallic
                        if ("_MetallicGlossMap" == shaderPropertyName)
                        {
                            var metallic = material.GetFloat("_Metallic");
                            var smoothness = material.GetFloat("_Smoothness");
                            uniformColor = new UnityEngine.Color(metallic, metallic, metallic, smoothness);
                        }

                        // Specular
                        else if ("_SpecGlossMap" == shaderPropertyName)
                        {
                            var specColor = material.GetColor("_SpecColor");
                            var smoothness = material.GetFloat("_Smoothness");
                            uniformColor = new UnityEngine.Color(specColor.r, specColor.g, specColor.b, smoothness);
                        }

                        // Bump
                        else if ("_BumpMap" == shaderPropertyName ||
                            "_BumpTex" == shaderPropertyName)
                        {
                            uniformColor = new UnityEngine.Color(0.5f, 0.5f, 1f);
                        }
                    }

                    // Fallback
                    else
                    {
                        var disabledChannel = shaderPropertyName == "_DetailAlbedoMap" ||
                            shaderPropertyName == "_DetailNormalMap" ||
                            shaderPropertyName == "_ParallaxMap" ||
                            shaderPropertyName == "_OcclusionMap" ||
                            shaderPropertyName == "_DetailMask" ||
                            shaderPropertyName == "_EmissionMap" ||
                            shaderPropertyName == "_EmissiveColorMap" ||
                            shaderPropertyName == "_CoatMaskMap" ||
                            shaderPropertyName == "_HeightMap";

                        // Early exit if disabled channel
                        if (disabledChannel)
                        {
                            state.Enabled = false;
                            return;
                        }

                        // Bump or normal
                        if ("_BumpMap" == shaderPropertyName ||
                            "_BumpTex" == shaderPropertyName ||
                            "_NormalMap" == shaderPropertyName)
                        {
                            uniformColor = new UnityEngine.Color(0.5f, 0.5f, 1f);
                        }
                    }

                    // Fill tex
                    for (int y = 0; y < uniformTexture.height; ++y)
                    {
                        for (int x = 0; x < uniformTexture.width; ++x)
                        {
                            uniformTexture.SetPixel(x, y, uniformColor);
                        }
                    }
                    uniformTexture.Apply();

                    // Write file
                    var bytes = uniformTexture.EncodeToPNG();
                    var filename = string.Format(CultureInfo.InvariantCulture, "stand_in_{0}_{1}.png", material.name, Guid.NewGuid().ToString());
                    var filepath = string.Format(CultureInfo.InvariantCulture, "{0}{1}{2}", state.TexturesPath, Path.DirectorySeparatorChar, filename);
                    File.WriteAllBytes(filepath, bytes);

                    // Create texture info
                    textureInfo = new ShadingNetworkBuildState.TextureInfo(simplygonTextureId, filepath);

                    // Destroy uniform texture
                    UnityEngine.Object.DestroyImmediate(uniformTexture);
                }

                // Has texture
                else
                {
                    // Standard Shader
                    if (material.shader.name == "Standard" ||
                        material.shader.name == "Standard (Specular setup)")
                    {
                        // Detail tiling
                        if (shaderPropertyName == "_DetailAlbedoMap" ||
                            shaderPropertyName == "_DetailNormalMap")
                        {
                            tiling = material.GetTextureScale("_DetailAlbedoMap");
                            if (material.HasProperty("_UVSec"))
                            {
                                int num = material.GetInt("_UVSec");
                                switch (num)
                                {
                                    case 0:
                                    case 1:
                                        texCoordsPropName = string.Format(CultureInfo.InvariantCulture, "TexCoords{0}", (object)num);
                                        break;
                                    default:
                                        num = 0;
                                        Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected UV set ({0}) for property {1} in shader {2}, will use default UV set (0).", (object)num, (object)shaderPropertyName, (object)material.shader.name));
                                        goto case 0;
                                }
                            }
                        }
                        // Main tiling
                        else
                        {
                            tiling = material.GetTextureScale("_MainTex");
                        }
                    }

                    // HDRP Lit
                    else if (material.shader.name == "HDRP/Lit")
                    {
                        // Detail tiling
                        if (shaderPropertyName == "_DetailMap")
                        {
                            tiling = material.GetTextureScale("_DetailMap");
                        }

                        // Main tiling
                        else
                        {
                            tiling = material.GetTextureScale("_BaseColorMap");
                        }
                    }

                    // Fallback
                    else
                    {
                        tiling = material.GetTextureScale(shaderPropertyName);
                    }
                    textureInfo = new ShadingNetworkBuildState.TextureInfo(simplygonTextureId, assetPath);
                }

                // Already created texture info?
                var prevTextureInfo = state.OtherBuildStates.SelectMany(otherState => otherState.TextureInfos).FirstOrDefault(ti => ti.OriginalTexturePath == textureInfo.OriginalTexturePath);
                if (prevTextureInfo != null)
                {
                    textureInfo = prevTextureInfo;
                }
                else
                {
                    state.TextureInfos.Add(textureInfo);
                }

                // Set xml
                state.Xml = state.Xml.Insert(0, string.Format(CultureInfo.InvariantCulture, "<ShadingTextureNode ref=\"node_{0}\" name=\"{1}\">  <DefaultColor0>    <DefaultValue>1 1 1 1</DefaultValue>  </DefaultColor0>  <TextureName>{2}</TextureName>  <TextureLevelName>{3}</TextureLevelName>  <UseSRGB>0</UseSRGB>  <TileU>{4}</TileU>  <TileV>{5}</TileV></ShadingTextureNode>", properties.NodeRef, properties.NodeName, textureInfo.SimplygonTextureFileName, texCoordsPropName, tiling.x, tiling.y));
            }

            internal void AddShadingFloatModifier(Exporter.ShadingNetworkBuildState state, UnityEngine.Material material, BasicShadingNodeProperties properties)
            {
                string shaderPropertyName = properties.ShaderPropertyName;
                float num = material.GetFloat(shaderPropertyName);
                state.Xml = state.Xml.Insert(0, string.Format(CultureInfo.InvariantCulture, "<ShadingColorNode ref=\"node_{0}\" name=\"{1}\">  <Color0>    <DefaultValue>{2} {2} {2} {2}</DefaultValue>  </Color0></ShadingColorNode>", (object)properties.NodeRef, (object)properties.NodeName, (object)num));
            }

            public void AddShadingInterpolateNode(Exporter.ShadingNetworkBuildState state, int nodeRef, int[] inputNodes, int[] nodeData)
            {
                if (inputNodes.Length == 3)
                {
                    List<string> stringList = this.HandleImplicitSwizzleNode(state, nodeRef, inputNodes, nodeData);
                    state.Xml = state.Xml.Insert(0, string.Format(CultureInfo.InvariantCulture, "<ShadingInterpolateNode ref=\"node_{0}\" name=\"ShadingInterpolateNode\">  <Color0>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[0] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[0]) : string.Empty) + "  </Color0>  <Color1>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[1] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[1]) : string.Empty) + "  </Color1>  <Blend>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[2] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[2]) : string.Empty) + "  </Blend></ShadingInterpolateNode>", (object)nodeRef));
                }
                else
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected number of input nodes ({0}) or node data ({1}) for shading network node 'InterpolateNode'", (object)inputNodes.Length, (object)nodeData.Length));
            }

            public void AddShadingClampNode(Exporter.ShadingNetworkBuildState state, int nodeRef, int[] inputNodes, int[] nodeData)
            {
                if (inputNodes.Length == 3)
                {
                    List<string> stringList = this.HandleImplicitSwizzleNode(state, nodeRef, inputNodes, nodeData);
                    state.Xml = state.Xml.Insert(0, string.Format(CultureInfo.InvariantCulture, "<ShadingClampNode ref=\"node_{0}\" name=\"ShadingInterpolateNode\">  <Input>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[0] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[0]) : string.Empty) + "  </Input>  <Lower>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[1] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[1]) : string.Empty) + "  </Lower>  <Upper>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[2] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[2]) : string.Empty) + "  </Upper></ShadingClampNode>", (object)nodeRef));
                }
                else
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected number of input nodes ({0}) or node data ({1}) for shading network node 'ClampNode'", (object)inputNodes.Length, (object)nodeData.Length));
            }

            public void AddShadingAddNode(Exporter.ShadingNetworkBuildState state, int nodeRef, int[] inputNodes, int[] nodeData)
            {
                if (inputNodes.Length == 2)
                {
                    List<string> stringList = this.HandleImplicitSwizzleNode(state, nodeRef, inputNodes, nodeData);
                    state.Xml = state.Xml.Insert(0, string.Format(CultureInfo.InvariantCulture, "<ShadingAddNode ref=\"node_{0}\" name=\"ShadingAddNode\">  <Add0>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[0] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[0]) : string.Empty) + "  </Add0>  <Add1>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[1] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[1]) : string.Empty) + "  </Add1></ShadingAddNode>", (object)nodeRef));
                }
                else
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected number of input nodes ({0}) or node data ({1}) for shading network node 'AddNode'", (object)inputNodes.Length, (object)nodeData.Length));
            }

            public void AddShadingSubtractNode(Exporter.ShadingNetworkBuildState state, int nodeRef, int[] inputNodes, int[] nodeData)
            {
                if (inputNodes.Length == 2)
                {
                    List<string> stringList = this.HandleImplicitSwizzleNode(state, nodeRef, inputNodes, nodeData);
                    state.Xml = state.Xml.Insert(0, string.Format(CultureInfo.InvariantCulture, "<ShadingSubtractNode ref=\"node_{0}\" name=\"ShadingSubtractNode\">  <Subtract0>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[0] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[0]) : string.Empty) + "  </Subtract0>  <Subtract1>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[1] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[1]) : string.Empty) + "  </Subtract1></ShadingSubtractNode>", (object)nodeRef));
                }
                else
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected number of input nodes ({0}) or node data ({1}) for shading network node 'SubtractNode'", (object)inputNodes.Length, (object)nodeData.Length));
            }

            public void AddShadingMultiplyNode(Exporter.ShadingNetworkBuildState state, int nodeRef, int[] inputNodes, int[] nodeData)
            {
                if (inputNodes.Length == 2)
                {
                    List<string> stringList = this.HandleImplicitSwizzleNode(state, nodeRef, inputNodes, nodeData);
                    state.Xml = state.Xml.Insert(0, string.Format(CultureInfo.InvariantCulture, "<ShadingMultiplyNode ref=\"node_{0}\" name=\"ShadingMultiplyNode\">  <Multiply0>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[0] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[0]) : string.Empty) + "  </Multiply0>  <Multiply1>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[1] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[1]) : string.Empty) + "  </Multiply1></ShadingMultiplyNode>", (object)nodeRef));
                }
                else
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected number of input nodes ({0}) or node data ({1}) for shading network node 'MultiplyNode'", (object)inputNodes.Length, (object)nodeData.Length));
            }

            public void AddShadingDivideNode(Exporter.ShadingNetworkBuildState state, int nodeRef, int[] inputNodes, int[] nodeData)
            {
                if (inputNodes.Length == 2)
                {
                    List<string> stringList = this.HandleImplicitSwizzleNode(state, nodeRef, inputNodes, nodeData);
                    state.Xml = state.Xml.Insert(0, string.Format(CultureInfo.InvariantCulture, "<ShadingDivideNode ref=\"node_{0}\" name=\"ShadingDivideNode\">  <Numerator>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[0] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[0]) : string.Empty) + "  </Numerator>  <Denominator>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[1] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[1]) : string.Empty) + "  </Denominator></ShadingDivideNode>", (object)nodeRef));
                }
                else
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected number of input nodes ({0}) or node data ({1}) for shading network node 'DivideNode'", (object)inputNodes.Length, (object)nodeData.Length));
            }

            public void AddShadingSwizzleNode(Exporter.ShadingNetworkBuildState state, int nodeRef, int[] inputNodes, int[] nodeData)
            {
                if (inputNodes.Length == 4 && nodeData.Length == 4)
                {
                    int inputNode1 = inputNodes[0];
                    int inputNode2 = inputNodes[1];
                    int inputNode3 = inputNodes[2];
                    int inputNode4 = inputNodes[3];
                    int num1 = nodeData[0];
                    int num2 = nodeData[1];
                    int num3 = nodeData[2];
                    int num4 = nodeData[3];
                    state.Xml = state.Xml.Insert(0, string.Format(CultureInfo.InvariantCulture, "<ShadingSwizzlingNode ref=\"node_{0}\" name=\"ShadingSwizzlingNode\">  <Red>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNode1 ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>node_{0}</InputNode>    <SourceComponent>{1}</SourceComponent>", (object)inputNode1, (object)num1) : string.Empty) + "  </Red>  <Green>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNode2 ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>node_{0}</InputNode>    <SourceComponent>{1}</SourceComponent>", (object)inputNode2, (object)num2) : string.Empty) + "  </Green>  <Blue>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNode3 ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>node_{0}</InputNode>    <SourceComponent>{1}</SourceComponent>", (object)inputNode3, (object)num3) : string.Empty) + "  </Blue>  <Alpha>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNode4 ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>node_{0}</InputNode>    <SourceComponent>{1}</SourceComponent>", (object)inputNode4, (object)num4) : string.Empty) + "  </Alpha></ShadingSwizzlingNode>", (object)nodeRef));
                }
                else
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected number of input nodes ({0}) or node data ({1}) for shading network node 'SwizzleNode'", (object)inputNodes.Length, (object)nodeData.Length));
            }

            public void AddShadingMaxNode(Exporter.ShadingNetworkBuildState state, int nodeRef, int[] inputNodes, int[] nodeData)
            {
                if (inputNodes.Length == 2)
                {
                    List<string> stringList = this.HandleImplicitSwizzleNode(state, nodeRef, inputNodes, nodeData);
                    state.Xml = state.Xml.Insert(0, string.Format(CultureInfo.InvariantCulture, "<ReShadingMaxNode ref=\"node_{0}\" name=\"ShadingMaxNode\">  <Max0>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[0] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[0]) : string.Empty) + "  </Max0>  <Max1>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[1] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[1]) : string.Empty) + "  </Max1></ReShadingMaxNode>", (object)nodeRef));
                }
                else
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected number of input nodes ({0}) or node data ({1}) for shading network node 'MaxNode'", (object)inputNodes.Length, (object)nodeData.Length));
            }

            public void AddShadingMinNode(Exporter.ShadingNetworkBuildState state, int nodeRef, int[] inputNodes, int[] nodeData)
            {
                if (inputNodes.Length == 2)
                {
                    List<string> stringList = this.HandleImplicitSwizzleNode(state, nodeRef, inputNodes, nodeData);
                    state.Xml = state.Xml.Insert(0, string.Format(CultureInfo.InvariantCulture, "<ReShadingMinNode ref=\"node_{0}\" name=\"ShadingMinNode\">  <Min0>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[0] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[0]) : string.Empty) + "  </Min0>  <Min1>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[1] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[1]) : string.Empty) + "  </Min1></ReShadingMinNode>", (object)nodeRef));
                }
                else
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected number of input nodes ({0}) or node data ({1}) for shading network node 'MinNode'", (object)inputNodes.Length, (object)nodeData.Length));
            }

            public void AddShadingStepNode(Exporter.ShadingNetworkBuildState state, int nodeRef, int[] inputNodes, int[] nodeData)
            {
                if (inputNodes.Length == 2)
                {
                    List<string> stringList = this.HandleImplicitSwizzleNode(state, nodeRef, inputNodes, nodeData);
                    state.Xml = state.Xml.Insert(0, string.Format(CultureInfo.InvariantCulture, "<ReShadingStepNode ref=\"node_{0}\" name=\"ShadingStepNode\">  <Step1>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[0] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[0]) : string.Empty) + "  </Step1>  <Step2>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNodes[1] ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>{0}</InputNode>", (object)stringList[1]) : string.Empty) + "  </Step2></ReShadingStepNode>", (object)nodeRef));
                }
                else
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected number of input nodes ({0}) or node data ({1}) for shading network node 'StepNode'", (object)inputNodes.Length, (object)nodeData.Length));
            }

            internal void Build(Exporter.ShadingNetworkBuildState state, string channelName, Simplygon.Scene.Asset.Material simplygonMaterial, Simplygon.Scene.Scene scene, WorkDirectoryInfo workDirectory)
            {
                if (!state.Enabled)
                    return;
                SettingsUtility.EnableCastChannel(channelName);
                MaterialChannel materialChannel = simplygonMaterial.AddMaterialChannel(channelName);
                materialChannel.ShadingNetwork = string.Format(CultureInfo.InvariantCulture, "{0}{1}{2}", (object)this.Header, (object)state.Xml, (object)this.Footer);
                foreach (Exporter.ShadingNetworkBuildState.TextureInfo textureInfo in state.TextureInfos)
                {
                    try
                    {
                        string destFileName = Path.Combine(workDirectory.TexturesPath, textureInfo.SimplygonTextureFileName);
                        string path = workDirectory.TexturesRelativePath + Path.DirectorySeparatorChar.ToString() + textureInfo.SimplygonTextureFileName;
                        try
                        {
                            if (!Directory.Exists(workDirectory.TexturesPath))
                                Directory.CreateDirectory(workDirectory.TexturesPath);
                        }
                        catch (Exception ex)
                        {
                            Debug.LogError((object)string.Format(CultureInfo.InvariantCulture, "Failed to create texture directory {0} ({1})", (object)workDirectory.TexturesPath, (object)ex.Message));
                            throw;
                        }
                        File.Copy(textureInfo.OriginalTexturePath, destFileName, true);
                        Simplygon.Scene.Asset.Texture texture = new Simplygon.Scene.Asset.Texture(textureInfo.SimplygonTextureId, textureInfo.SimplygonTextureFileName, path);
                        scene.AddTexture(texture);
                        materialChannel.AddTexture(texture.Id, "TexCoords0");
                    }
                    catch (Exception ex)
                    {
                        Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Failed to copy texture '{0}' to work directory '{1}' ({2}, {3})", (object)textureInfo.OriginalTexturePath, (object)workDirectory.TexturesPath, (object)ex.Message, (object)ex.StackTrace));
                    }
                }
            }

            private List<string> HandleImplicitSwizzleNode(Exporter.ShadingNetworkBuildState state, int nodeRef, int[] inputNodes, int[] nodeData)
            {
                List<string> stringList = new List<string>();
                for (int index = 0; index < inputNodes.Length; ++index)
                    stringList.Add(string.Format(CultureInfo.InvariantCulture, "node_{0}", (object)inputNodes[index]));
                if (inputNodes.Length == nodeData.Length)
                {
                    for (int index1 = 0; index1 < nodeData.Length; ++index1)
                    {
                        if (-1 != nodeData[index1])
                        {
                            int[] inputNodes1 = new int[4]
                            {
                -1,
                -1,
                -1,
                -1
                            };
                            int[] nodeData1 = new int[4] { 0, 1, 2, 3 };
                            string nodeRef1 = string.Format(CultureInfo.InvariantCulture, "node_{0}_input_{1}_implicit_swizzle", (object)nodeRef, (object)index1);
                            int index2 = nodeData[index1];
                            inputNodes1[index2] = inputNodes[index1];
                            stringList[index1] = nodeRef1;
                            this.AddShadingImplicitSwizzleNode(state, nodeRef1, inputNodes1, nodeData1);
                        }
                    }
                }
                return stringList;
            }

            private void AddShadingImplicitSwizzleNode(Exporter.ShadingNetworkBuildState state, string nodeRef, int[] inputNodes, int[] nodeData)
            {
                if (inputNodes.Length == 4 && nodeData.Length == 4)
                {
                    int inputNode1 = inputNodes[0];
                    int inputNode2 = inputNodes[1];
                    int inputNode3 = inputNodes[2];
                    int inputNode4 = inputNodes[3];
                    int num1 = nodeData[0];
                    int num2 = nodeData[1];
                    int num3 = nodeData[2];
                    int num4 = nodeData[3];
                    state.Xml = state.Xml.Insert(0, string.Format(CultureInfo.InvariantCulture, "<ShadingSwizzlingNode ref=\"{0}\" name=\"ImplicitShadingSwizzlingNode\">  <Red>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNode1 ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>node_{0}</InputNode>    <SourceComponent>{1}</SourceComponent>", (object)inputNode1, (object)num1) : string.Empty) + "  </Red>  <Green>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNode2 ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>node_{0}</InputNode>    <SourceComponent>{1}</SourceComponent>", (object)inputNode2, (object)num2) : string.Empty) + "  </Green>  <Blue>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNode3 ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>node_{0}</InputNode>    <SourceComponent>{1}</SourceComponent>", (object)inputNode3, (object)num3) : string.Empty) + "  </Blue>  <Alpha>    <DefaultValue>0 0 0 0</DefaultValue>" + (-1 != inputNode4 ? string.Format(CultureInfo.InvariantCulture, "    <InputNode>node_{0}</InputNode>    <SourceComponent>{1}</SourceComponent>", (object)inputNode4, (object)num4) : string.Empty) + "  </Alpha></ShadingSwizzlingNode>", (object)nodeRef));
                }
                else
                    Debug.LogWarning((object)string.Format(CultureInfo.InvariantCulture, "Simplygon: Unexpected number of input nodes ({0}) or node data ({1}) for shading network implicit node 'SwizzleNode'", (object)inputNodes.Length, (object)nodeData.Length));
            }
        }

        public interface IShadingNodeHandler
        {
            void Handle(Exporter.ShadingNetworkBuildState state, UnityEngine.Material material);
        }

        internal class BasicShadingNodeHandler : Exporter.IShadingNodeHandler
        {
            public BasicShadingNodeProperties Properties
            {
                get; protected set;
            }

            public Action<Exporter.ShadingNetworkBuildState, UnityEngine.Material, BasicShadingNodeProperties> CreateNode
            {
                get; protected set;
            }

            public BasicShadingNodeHandler(BasicShadingNodeProperties properties, Action<Exporter.ShadingNetworkBuildState, UnityEngine.Material, BasicShadingNodeProperties> createNode)
            {
                this.Properties = properties;
                this.CreateNode = createNode;
            }

            public void Handle(Exporter.ShadingNetworkBuildState state, UnityEngine.Material material)
            {
                this.CreateNode(state, material, this.Properties);
            }
        }

        internal class ModifierShadingNodeHandler : Exporter.IShadingNodeHandler
        {
            public ModifierShadingNodeProperties Properties
            {
                get; protected set;
            }

            public Action<Exporter.ShadingNetworkBuildState, int, int[], int[]> CreateNode
            {
                get; protected set;
            }

            public ModifierShadingNodeHandler(ModifierShadingNodeProperties properties, Action<Exporter.ShadingNetworkBuildState, int, int[], int[]> createNode)
            {
                this.Properties = properties;
                this.CreateNode = createNode;
            }

            public void Handle(Exporter.ShadingNetworkBuildState state, UnityEngine.Material material)
            {
                this.CreateNode(state, this.Properties.NodeRef, this.Properties.InputNodes, this.Properties.NodeData);
            }
        }

        internal class ShadingChannelHandler
        {
            public ShadingChannelProperties Properties
            {
                get; protected set;
            }

            public List<Exporter.IShadingNodeHandler> ShadingNodeHandlers
            {
                get; protected set;
            }

            public ShadingChannelHandler(ShadingChannelProperties shadingChannelProperties, List<Exporter.IShadingNodeHandler> shadingNodeHandlers)
            {
                this.Properties = shadingChannelProperties;
                this.ShadingNodeHandlers = shadingNodeHandlers;
            }

            public void Handle(UnityEngine.Material material, Simplygon.Scene.Asset.Material simplygonMaterial, Simplygon.Scene.Scene simplygonScene, WorkDirectoryInfo workDirectory, List<Exporter.ShadingNetworkBuildState> otherNetworkBuildStates)
            {
                Exporter.ShadingNetworkBuildState state = new Exporter.ShadingNetworkBuildState(workDirectory.TexturesPath, otherNetworkBuildStates);
                foreach (Exporter.IShadingNodeHandler shadingNodeHandler in this.ShadingNodeHandlers)
                    shadingNodeHandler.Handle(state, material);
                Exporter.ShadingNetworkBuilder.Instance.Build(state, this.Properties.ChannelName, simplygonMaterial, simplygonScene, workDirectory);
                otherNetworkBuildStates.Add(state);
            }
        }
    }
}
