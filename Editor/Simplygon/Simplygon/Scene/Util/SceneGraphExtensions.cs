﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Util.SceneGraphExtensions
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Scene.Asset;
using Simplygon.Scene.Graph;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Globalization;

namespace Simplygon.Scene.Util
{
    internal static class SceneGraphExtensions
    {
        public const string EMPTY_STRING_PLACEHOLDER = "SIMPLYGON_SSF_EMPTYSTR";

        public static void ValidateSceneGraph(this Scene scene)
        {
            try
            {
                bool flag = false;
                foreach (Material material in scene.MaterialList)
                {
                    var guid = new Guid(scene.DefaultMaterial.Id.ToString());
                    if (material.Id == guid || material.Name.Equals("SimplygonDefaultMaterial"))
                        flag = true;
                }

                if (!flag)
                    scene.AddMaterial(scene.DefaultMaterial);

                var guidList = new List<Guid>();
                foreach (KeyValuePair<Guid, Texture> texture in scene.Textures)
                {
                    if (!File.Exists(scene.SceneDirectory + "\\" + texture.Value.Path))
                        guidList.Add(texture.Key);
                }

                foreach (Material material in scene.MaterialList)
                {
                    foreach (Guid id in guidList)
                        material.RemoveTextureFromMaterial(id);
                }

                foreach (Material material in scene.MaterialList)
                {
                    foreach (MaterialChannel materialChannel in material.MaterialChannels)
                    {
                        if (materialChannel.Textures.Count == 0)
                            materialChannel.ShadingNetwork = string.Format(CultureInfo.InvariantCulture, "<SimplygonShadingNetwork version=\"1.0\">\n{0}</SimplygonShadingNetwork>", (object)ShadingNetworkHelper.GetShadingColorNodeXml(Guid.NewGuid().ToString(), materialChannel.ChannelName, materialChannel.Color));
                    }
                }

                foreach (KeyValuePair<Guid, Mesh> mesh in scene.Meshes)
                {
                    KeyValuePair<Guid, Mesh> meshKVP = mesh;
                    int num = -1;
                    if (meshKVP.Value.Materials.Count == 0)
                    {
                        meshKVP.Value.Materials.Add(scene.DefaultMaterial.Id);
                        num = meshKVP.Value.Materials.Count - 1;
                    }

                    foreach (MeshData meshSource in meshKVP.Value.MeshSources)
                    {
                        if (meshSource.MaterialIndices != null)
                        {
                            var source1 = ((IEnumerable<int>)meshSource.MaterialIndices).Select((item, pos) => new
                            {
                                ItemValue = item,
                                Position = pos
                            }).Where(x => x.ItemValue >= meshKVP.Value.Materials.Count);

                            var source2 = ((IEnumerable<int>)meshSource.MaterialIndices).Select((item, pos) => new
                            {
                                ItemValue = item,
                                Position = pos
                            }).Where(x => x.ItemValue < 0);

                            if ((source1.Count() > 0 || source2.Count() > 0) && !meshKVP.Value.Materials.Contains(scene.DefaultMaterial.Id))
                            {
                                meshKVP.Value.Materials.Add(scene.DefaultMaterial.Id);
                                num = meshKVP.Value.Materials.Count - 1;
                            }

                            foreach (var data in source1)
                                meshSource.MaterialIndices[data.Position] = num;

                            foreach (var data in source2)
                                meshSource.MaterialIndices[data.Position] = num;
                        }
                        else
                        {
                            meshSource.MaterialIndices = new int[meshSource.TriangleCount];
                            for (int index = 0; index < meshSource.TriangleCount; ++index)
                                meshSource.MaterialIndices[index] = num;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to fix error in the scenegraph");
            }
        }

        public static void ReplaceEmptyNamesWithPlaceholder(this Scene scene)
        {
            foreach (KeyValuePair<Guid, Node> node in scene.Nodes)
            {
                if (string.IsNullOrEmpty(node.Value.Name))
                    node.Value.Name = "SIMPLYGON_SSF_EMPTYSTR";
            }
            foreach (KeyValuePair<Guid, Mesh> mesh in scene.Meshes)
            {
                if (string.IsNullOrEmpty(mesh.Value.Name))
                    mesh.Value.Name = "SIMPLYGON_SSF_EMPTYSTR";
            }
            foreach (KeyValuePair<Guid, Material> material in scene.Materials)
            {
                if (string.IsNullOrEmpty(material.Value.Name))
                    material.Value.Name = "SIMPLYGON_SSF_EMPTYSTR";
            }
            foreach (KeyValuePair<Guid, Texture> texture in scene.Textures)
            {
                if (string.IsNullOrEmpty(texture.Value.Name))
                    texture.Value.Name = "SIMPLYGON_SSF_EMPTYSTR";
            }
        }

        public static void ReplacePlaceholderWithEmptyString(this Scene scene)
        {
            foreach (KeyValuePair<Guid, Node> node in scene.Nodes)
            {
                if (node.Value.Name == "SIMPLYGON_SSF_EMPTYSTR")
                    node.Value.Name = string.Empty;
            }
            foreach (KeyValuePair<Guid, Mesh> mesh in scene.Meshes)
            {
                if (mesh.Value.Name == "SIMPLYGON_SSF_EMPTYSTR")
                    mesh.Value.Name = string.Empty;
            }
            foreach (KeyValuePair<Guid, Material> material in scene.Materials)
            {
                if (material.Value.Name == "SIMPLYGON_SSF_EMPTYSTR")
                    material.Value.Name = string.Empty;
            }
            foreach (KeyValuePair<Guid, Texture> texture in scene.Textures)
            {
                if (texture.Value.Name == "SIMPLYGON_SSF_EMPTYSTR")
                    texture.Value.Name = string.Empty;
            }
        }

        public static void FixBoneTupleSize(this Scene scene)
        {
            try
            {
                var dictionary = new Dictionary<Guid, int>();
                foreach (KeyValuePair<Guid, Mesh> mesh in scene.Meshes)
                {
                    foreach (MeshData meshSource in mesh.Value.MeshSources)
                    {
                        if (meshSource.HasBoneIndices())
                        {
                            meshSource.BoneIndices.GetLength(0);
                            dictionary.Add(mesh.Value.Id, meshSource.BoneIndices.GetLength(1));
                        }
                    }
                }
                int length1 = 0;
                if (dictionary != null && dictionary.Count > 0)
                    length1 = dictionary.Values.Max();
                foreach (KeyValuePair<Guid, int> keyValuePair in dictionary)
                {
                    if (keyValuePair.Value < length1)
                    {
                        int[,] boneIndices = scene.Meshes[keyValuePair.Key].MeshSources[0].BoneIndices;
                        int[,] numArray1 = new int[boneIndices.GetLength(0), length1];
                        float[,] boneWeights = scene.Meshes[keyValuePair.Key].MeshSources[0].BoneWeights;
                        float[,] numArray2 = new float[boneWeights.GetLength(0), length1];
                        int length2 = boneIndices.GetLength(1);
                        for (int index1 = 0; index1 < numArray1.GetLength(0); ++index1)
                        {
                            for (int index2 = 0; index2 < numArray1.GetLength(1); ++index2)
                            {
                                if (index2 < length2 - 1)
                                {
                                    numArray1[index1, index2] = boneIndices[index1, index2];
                                    numArray2[index1, index2] = boneWeights[index1, index2];
                                }
                                else
                                {
                                    numArray1[index1, index2] = -1;
                                    numArray2[index1, index2] = 0.0f;
                                }
                            }
                        }
                        scene.Meshes[keyValuePair.Key].MeshSources[0].BoneIndices = numArray1;
                        scene.Meshes[keyValuePair.Key].MeshSources[0].BoneWeights = numArray2;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to match bone tuple sizes");
            }
        }
    }
}
