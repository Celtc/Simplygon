﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Util.ShadingNetworkHelper
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Math;
using System.Globalization;

namespace Simplygon.Scene.Util
{
  internal static class ShadingNetworkHelper
  {
    public const string SHADIND_NETWORK_XML_TEMPLATE = "<SimplygonShadingNetwork version=\"1.0\">\n{0}</SimplygonShadingNetwork>";

    public static string GetShadingColorNodeXml(string refId, string nodeName, Color color)
    {
      return string.Format(CultureInfo.InvariantCulture, "<ShadingColorNode ref=\"{0}\" name=\"{1}\">\n" + "<Color0><DefaultValue>{2} {3} {4} {5}</DefaultValue>\n</Color0>\n" + "</ShadingColorNode>\n", (object) refId, (object) nodeName, (object) color.R, (object) color.G, (object) color.B, (object) color.A);
    }
  }
}
