﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Util.SceneConverter
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Hash;
using Simplygon.Math;
using Simplygon.Scene.Asset;
using Simplygon.Scene.Common.Enums;
using Simplygon.Scene.Graph;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Simplygon.Scene.Util
{
  internal static class SceneConverter
  {
    public static Simplygon.Scene.Scene CopyAndConvertToCoordinateSystem(Simplygon.Scene.Scene scene, CoordinateSystem coordinateSystem)
    {
      Simplygon.Scene.Scene scene1 = scene.DeepCopy();
      SceneConverter.ConvertToCoordinateSystem(scene1, coordinateSystem);
      return scene1;
    }

    public static void ConvertToCoordinateSystem(Simplygon.Scene.Scene scene, CoordinateSystem coordinateSystem)
    {
      if (scene.OriginalCoordinateSystem == coordinateSystem)
        return;
      scene.OriginalCoordinateSystem = coordinateSystem;
      foreach (KeyValuePair<Guid, Node> node in scene.Nodes)
        node.Value.LocalTransform = Matrix.ChangeCoordinateSystem(node.Value.LocalTransform);
      foreach (KeyValuePair<Guid, Mesh> mesh in scene.Meshes)
      {
        foreach (MeshData meshSource in mesh.Value.MeshSources)
        {
          if (meshSource.Triangles != null)
          {
            int index = 0;
            while (index < meshSource.Triangles.Length)
            {
              int triangle1 = meshSource.Triangles[index];
              int triangle2 = meshSource.Triangles[index + 1];
              int triangle3 = meshSource.Triangles[index + 2];
              meshSource.Triangles[index] = triangle3;
              meshSource.Triangles[index + 1] = triangle2;
              meshSource.Triangles[index + 2] = triangle1;
              index += 3;
            }
          }
          if (meshSource.Coordinates != null)
          {
            for (int index = 0; index < meshSource.Coordinates.Length; ++index)
            {
              Vector4 coordinate = meshSource.Coordinates[index];
              if (scene.WorldOrientation != WorldOrientation.Z_UP && scene.WorldOrientation != WorldOrientation.Z_DOWN)
                coordinate.Z *= -1.0;
              else
                coordinate.Y *= -1.0;
              meshSource.Coordinates[index] = coordinate;
            }
          }
          if (meshSource.Normals != null)
          {
            int index = 0;
            while (index < meshSource.Normals.Length)
            {
              Vector3 normal1 = meshSource.Normals[index];
              Vector3 normal2 = meshSource.Normals[index + 1];
              Vector3 normal3 = meshSource.Normals[index + 2];
              if (scene.WorldOrientation != WorldOrientation.Z_UP && scene.WorldOrientation != WorldOrientation.Z_DOWN)
              {
                normal1.Z *= -1.0;
                normal2.Z *= -1.0;
                normal3.Z *= -1.0;
              }
              else
              {
                normal1.Y *= -1.0;
                normal2.Y *= -1.0;
                normal3.Y *= -1.0;
              }
              meshSource.Normals[index] = normal3;
              meshSource.Normals[index + 1] = normal2;
              meshSource.Normals[index + 2] = normal1;
              index += 3;
            }
          }
          if (meshSource.Tangents != null)
          {
            int index = 0;
            while (index < meshSource.Tangents.Length)
            {
              Vector3 tangent1 = meshSource.Tangents[index];
              Vector3 tangent2 = meshSource.Tangents[index + 1];
              Vector3 tangent3 = meshSource.Tangents[index + 2];
              if (scene.WorldOrientation != WorldOrientation.Z_UP && scene.WorldOrientation != WorldOrientation.Z_DOWN)
              {
                tangent1.Z *= -1.0;
                tangent2.Z *= -1.0;
                tangent3.Z *= -1.0;
              }
              else
              {
                tangent1.Y *= -1.0;
                tangent2.Y *= -1.0;
                tangent3.Y *= -1.0;
              }
              meshSource.Tangents[index] = tangent3;
              meshSource.Tangents[index + 1] = tangent2;
              meshSource.Tangents[index + 2] = tangent1;
              index += 3;
            }
          }
          if (meshSource.Bitangents != null)
          {
            int index = 0;
            while (index < meshSource.Bitangents.Length)
            {
              Vector3 bitangent1 = meshSource.Bitangents[index];
              Vector3 bitangent2 = meshSource.Bitangents[index + 1];
              Vector3 bitangent3 = meshSource.Bitangents[index + 2];
              if (scene.WorldOrientation != WorldOrientation.Z_UP && scene.WorldOrientation != WorldOrientation.Z_DOWN)
              {
                bitangent1.Z *= -1.0;
                bitangent2.Z *= -1.0;
                bitangent3.Z *= -1.0;
              }
              else
              {
                bitangent1.Y *= -1.0;
                bitangent2.Y *= -1.0;
                bitangent3.Y *= -1.0;
              }
              meshSource.Bitangents[index] = bitangent3;
              meshSource.Bitangents[index + 1] = bitangent2;
              meshSource.Bitangents[index + 2] = bitangent1;
              index += 3;
            }
          }
          foreach (KeyValuePair<string, Vector2[]> texCoord in meshSource.TexCoords)
          {
            int index = 0;
            while (index < texCoord.Value.Length)
            {
              Vector2 vector2_1 = texCoord.Value[index];
              Vector2 vector2_2 = texCoord.Value[index + 1];
              Vector2 vector2_3 = texCoord.Value[index + 2];
              vector2_1.Y = 1.0 - vector2_1.Y;
              vector2_2.Y = 1.0 - vector2_2.Y;
              vector2_3.Y = 1.0 - vector2_3.Y;
              texCoord.Value[index] = vector2_3;
              texCoord.Value[index + 1] = vector2_2;
              texCoord.Value[index + 2] = vector2_1;
              index += 3;
            }
          }
          foreach (KeyValuePair<string, Color[]> vertexColor in meshSource.VertexColors)
          {
            int index = 0;
            while (index < vertexColor.Value.Length)
            {
              Color color1 = vertexColor.Value[index];
              Color color2 = vertexColor.Value[index + 1];
              Color color3 = vertexColor.Value[index + 2];
              vertexColor.Value[index] = color3;
              vertexColor.Value[index + 1] = color2;
              vertexColor.Value[index + 2] = color1;
              index += 3;
            }
          }
        }
      }
    }

    public static Simplygon.Scene.Scene GetSceneCopyWithUnpackedGeometry(Simplygon.Scene.Scene scene)
    {
      Simplygon.Scene.Scene scene1 = scene.DeepCopy();
      foreach (KeyValuePair<Guid, Mesh> mesh in scene1.Meshes)
      {
        foreach (MeshData meshSource in mesh.Value.MeshSources)
        {
          int[] triangles = meshSource.Triangles;
          Vector4[] coordinates = meshSource.Coordinates;
          Vector3[] normals = meshSource.Normals;
          Vector3[] tangents = meshSource.Tangents;
          Vector3[] bitangents = meshSource.Bitangents;
          if (normals != null)
            meshSource.Normals = SceneConverter.UnpackVector<Vector3>(triangles, normals);
          if (tangents != null)
            meshSource.Tangents = SceneConverter.UnpackVector<Vector3>(triangles, tangents);
          if (bitangents != null)
            meshSource.Bitangents = SceneConverter.UnpackVector<Vector3>(triangles, bitangents);
          Dictionary<string, Vector2[]> dictionary1 = new Dictionary<string, Vector2[]>();
          Dictionary<string, Color[]> dictionary2 = new Dictionary<string, Color[]>();
          foreach (KeyValuePair<string, Vector2[]> texCoord in meshSource.TexCoords)
            dictionary1.Add(texCoord.Key, SceneConverter.UnpackVector<Vector2>(triangles, texCoord.Value));
          foreach (KeyValuePair<string, Vector2[]> keyValuePair in dictionary1)
            meshSource.TexCoords[keyValuePair.Key] = keyValuePair.Value;
          foreach (KeyValuePair<string, Color[]> vertexColor in meshSource.VertexColors)
            dictionary2.Add(vertexColor.Key, SceneConverter.UnpackVector<Color>(triangles, vertexColor.Value));
          foreach (KeyValuePair<string, Color[]> keyValuePair in dictionary2)
            meshSource.VertexColors[keyValuePair.Key] = keyValuePair.Value;
        }
      }
      return scene1;
    }

    public static Mesh GetPackedMesh(Mesh unpackedMesh)
    {
      Mesh mesh = unpackedMesh.DeepCopy() as Mesh;
      foreach (MeshData meshSource in mesh.MeshSources)
      {
        int[] triangles = meshSource.Triangles;
        Vector4[] coordinates = meshSource.Coordinates;
        int[,] boneIndices = meshSource.BoneIndices;
        float[,] boneWeights = meshSource.BoneWeights;
        int length1 = coordinates.Length;
        Dictionary<int, int> unpackedToPackedMapping = new Dictionary<int, int>();
        Dictionary<string, int[]> uniqueFieldIndicesDictionary1 = new Dictionary<string, int[]>();
        Dictionary<string, Vector3[]> uniqueFieldsDictionary1 = new Dictionary<string, Vector3[]>();
        Dictionary<string, int[]> uniqueFieldIndicesDictionary2 = new Dictionary<string, int[]>();
        Dictionary<string, Vector2[]> uniqueFieldsDictionary2 = new Dictionary<string, Vector2[]>();
        Dictionary<string, int[]> uniqueFieldIndicesDictionary3 = new Dictionary<string, int[]>();
        Dictionary<string, Color[]> uniqueFieldsDictionary3 = new Dictionary<string, Color[]>();
        SceneConverter.PerVertexData perVertexData = new SceneConverter.PerVertexData(coordinates, boneIndices, boneWeights);
        SceneConverter.RepackNTB(meshSource, ref unpackedToPackedMapping, ref triangles, ref perVertexData, uniqueFieldIndicesDictionary1, uniqueFieldsDictionary1);
        SceneConverter.RepackTextureCoordinates(meshSource, ref unpackedToPackedMapping, ref triangles, ref perVertexData, uniqueFieldIndicesDictionary2, uniqueFieldsDictionary2);
        SceneConverter.RepackVertexColors(meshSource, ref unpackedToPackedMapping, ref triangles, ref perVertexData, uniqueFieldIndicesDictionary3, uniqueFieldsDictionary3);
        int length2 = unpackedToPackedMapping.Values.Distinct<int>().Count<int>();
        if (length2 < perVertexData.Coordinates.Length)
          length2 = perVertexData.Coordinates.Length;
        int num = unpackedToPackedMapping.Values.Max() + 1;
        if (length2 < num)
          length2 = num;
        int[] numArray1 = new int[triangles.Length];
        Vector4[] vector4Array = new Vector4[length2];
        Vector3[] vector3Array = new Vector3[length2];
        int[,] numArray2 = (int[,]) null;
        float[,] numArray3 = (float[,]) null;
        int length3 = 0;
        if (boneIndices != null && boneWeights != null)
        {
          length3 = boneIndices.GetLength(1);
          numArray2 = new int[length2, length3];
          numArray3 = new float[length2, length3];
        }
        Dictionary<string, Vector2[]> dictionary1 = new Dictionary<string, Vector2[]>();
        Dictionary<string, Color[]> dictionary2 = new Dictionary<string, Color[]>();
        foreach (KeyValuePair<string, Vector2[]> keyValuePair in uniqueFieldsDictionary2)
          dictionary1.Add(keyValuePair.Key, new Vector2[length2]);
        foreach (KeyValuePair<string, Color[]> keyValuePair in uniqueFieldsDictionary3)
          dictionary2.Add(keyValuePair.Key, new Color[length2]);
        HashSet<int> intSet = new HashSet<int>();
        for (int index1 = 0; index1 < triangles.Length; ++index1)
        {
          int index2 = index1;
          int index3 = unpackedToPackedMapping[index1];
          int index4 = triangles[index2];
          if (!intSet.Contains(index3))
          {
            vector4Array[index3] = perVertexData.Coordinates[index4];
            if (boneIndices != null && boneWeights != null)
            {
              for (int index5 = 0; index5 < length3; ++index5)
              {
                numArray2[index3, index5] = perVertexData.BoneIndices[index4, index5];
                numArray3[index3, index5] = perVertexData.BoneWeights[index4, index5];
              }
            }
            if (meshSource.Normals != null)
              vector3Array[index3] = uniqueFieldsDictionary1["Normals"][uniqueFieldIndicesDictionary1["Normals"][index1]];
            foreach (KeyValuePair<string, Vector2[]> texCoord in meshSource.TexCoords)
              dictionary1[texCoord.Key][index3] = uniqueFieldsDictionary2[texCoord.Key][uniqueFieldIndicesDictionary2[texCoord.Key][index1]];
            foreach (KeyValuePair<string, Color[]> vertexColor in meshSource.VertexColors)
              dictionary2[vertexColor.Key][index3] = uniqueFieldsDictionary3[vertexColor.Key][uniqueFieldIndicesDictionary3[vertexColor.Key][index1]];
            intSet.Add(index3);
          }
          numArray1[index1] = index3;
        }
        meshSource.Coordinates = vector4Array;
        meshSource.Triangles = numArray1;
        if (meshSource.Normals != null)
          meshSource.Normals = vector3Array;
        if (meshSource.BoneIndices != null)
          meshSource.BoneIndices = numArray2;
        if (meshSource.BoneWeights != null)
          meshSource.BoneWeights = numArray3;
        foreach (KeyValuePair<string, Vector2[]> keyValuePair in uniqueFieldsDictionary2)
          meshSource.TexCoords[keyValuePair.Key] = dictionary1[keyValuePair.Key];
        foreach (KeyValuePair<string, Color[]> keyValuePair in uniqueFieldsDictionary3)
          meshSource.VertexColors[keyValuePair.Key] = dictionary2[keyValuePair.Key];
        Simplygon.Scene.Scene.ComputeTangentsAndBitangents(meshSource);
      }
      return mesh;
    }

    public static Simplygon.Scene.Scene GetSceneCopyWithPackedGeometry(Simplygon.Scene.Scene scene)
    {
      Simplygon.Scene.Scene scene1 = scene.DeepCopy();
      List<Mesh> meshList = new List<Mesh>();
      foreach (Guid key in scene1.Meshes.Keys)
      {
        Mesh packedMesh = SceneConverter.GetPackedMesh(scene1.Meshes[key]);
        meshList.Add(packedMesh);
      }
      scene1.Meshes.Clear();
      foreach (Mesh mesh in meshList)
        scene1.Meshes.Add(mesh.Id, mesh);
      return scene1;
    }

    private static void RepackNTB(MeshData meshData, ref Dictionary<int, int> unpackedToPackedMapping, ref int[] triangleIndices, ref SceneConverter.PerVertexData perVertexData, Dictionary<string, int[]> uniqueFieldIndicesDictionary, Dictionary<string, Vector3[]> uniqueFieldsDictionary)
    {
      if (meshData.Normals == null)
        return;
      SceneConverter.RepackField<Vector3>("Normals", meshData.Normals, ref unpackedToPackedMapping, ref triangleIndices, ref perVertexData, uniqueFieldIndicesDictionary, uniqueFieldsDictionary);
    }

    private static void RepackTextureCoordinates(MeshData meshData, ref Dictionary<int, int> unpackedToPackedMapping, ref int[] triangleIndices, ref SceneConverter.PerVertexData perVertexData, Dictionary<string, int[]> uniqueFieldIndicesDictionary, Dictionary<string, Vector2[]> uniqueFieldsDictionary)
    {
      foreach (KeyValuePair<string, Vector2[]> texCoord in meshData.TexCoords)
        SceneConverter.RepackField<Vector2>(texCoord.Key, texCoord.Value, ref unpackedToPackedMapping, ref triangleIndices, ref perVertexData, uniqueFieldIndicesDictionary, uniqueFieldsDictionary);
    }

    private static void RepackVertexColors(MeshData meshData, ref Dictionary<int, int> unpackedToPackedMapping, ref int[] triangleIndices, ref SceneConverter.PerVertexData perVertexData, Dictionary<string, int[]> uniqueFieldIndicesDictionary, Dictionary<string, Color[]> uniqueFieldsDictionary)
    {
      foreach (KeyValuePair<string, Color[]> vertexColor in meshData.VertexColors)
        SceneConverter.RepackField<Color>(vertexColor.Key, vertexColor.Value, ref unpackedToPackedMapping, ref triangleIndices, ref perVertexData, uniqueFieldIndicesDictionary, uniqueFieldsDictionary);
    }

    private static void RepackBoneIndicesAndWeights(Dictionary<int, int> unpackedToPackedMapping, int[] inTriangleIndices, int[,] inBoneIndices, float[,] inBoneWeights, ref int[,] outBoneIndices, ref float[,] outBoneWeights)
    {
      int length1 = unpackedToPackedMapping.Values.Distinct<int>().Count<int>();
      int length2 = inBoneIndices.GetLength(1);
      int[,] numArray1 = new int[length1, length2];
      float[,] numArray2 = new float[length1, length2];
      HashSet<int> intSet = new HashSet<int>();
      for (int index1 = 0; index1 < inTriangleIndices.Length; ++index1)
      {
        int index2 = index1;
        int index3 = unpackedToPackedMapping[index1];
        int inTriangleIndex = inTriangleIndices[index2];
        for (int index4 = 0; index4 < length2; ++index4)
        {
          numArray1[index3, index4] = inBoneIndices[inTriangleIndex, index4];
          numArray2[index3, index4] = inBoneWeights[inTriangleIndex, index4];
        }
      }
      outBoneIndices = numArray1;
      outBoneWeights = numArray2;
    }

    private static void RepackField<T>(string fieldName, T[] field, ref Dictionary<int, int> unpackedToPackedMapping, ref int[] triangleIndices, ref SceneConverter.PerVertexData perVertexData, Dictionary<string, int[]> uniqueFieldIndicesDictionary, Dictionary<string, T[]> uniqueFieldsDictionary)
    {
      if (field == null)
        return;
      int[] packedIndices = (int[]) null;
      T[] objArray = SceneConverter.UniqueItems<T>(field, out packedIndices);
      unpackedToPackedMapping = SceneConverter.GetUnpackedToPackedMapping(triangleIndices, packedIndices, perVertexData.Coordinates.Length);
      SceneConverter.Repack(unpackedToPackedMapping, triangleIndices, out triangleIndices, ref perVertexData);
      uniqueFieldIndicesDictionary.Add(fieldName, packedIndices);
      uniqueFieldsDictionary.Add(fieldName, objArray);
    }

    private static void Repack(Dictionary<int, int> unpackedToPackedMapping, int[] inTriangleIndices, out int[] outTriangleIndices, ref SceneConverter.PerVertexData perVertexData)
    {
      int length1 = unpackedToPackedMapping.Values.Distinct<int>().Count<int>();
      if (length1 < perVertexData.Coordinates.Length)
        length1 = perVertexData.Coordinates.Length;
      int num = unpackedToPackedMapping.Values.Max() + 1;
      if (length1 < num)
        length1 = num;
      int[] numArray1 = new int[inTriangleIndices.Length];
      Vector4[] vector4Array = new Vector4[length1];
      int[,] numArray2 = (int[,]) null;
      float[,] numArray3 = (float[,]) null;
      int length2 = 0;
      if (perVertexData.BoneIndices != null && perVertexData.BoneWeights != null)
      {
        length2 = perVertexData.BoneIndices.GetLength(1);
        numArray2 = new int[length1, length2];
        numArray3 = new float[length1, length2];
      }
      HashSet<int> intSet = new HashSet<int>();
      for (int index1 = 0; index1 < inTriangleIndices.Length; ++index1)
      {
        int index2 = index1;
        int index3 = unpackedToPackedMapping[index1];
        int inTriangleIndex = inTriangleIndices[index2];
        vector4Array[index3] = perVertexData.Coordinates[inTriangleIndex];
        numArray1[index1] = index3;
        if (perVertexData.BoneIndices != null && perVertexData.BoneWeights != null)
        {
          for (int index4 = 0; index4 < length2; ++index4)
          {
            numArray2[index3, index4] = perVertexData.BoneIndices[inTriangleIndex, index4];
            numArray3[index3, index4] = perVertexData.BoneWeights[inTriangleIndex, index4];
          }
        }
      }
      outTriangleIndices = numArray1;
      perVertexData.Coordinates = vector4Array;
      if (perVertexData.BoneIndices == null || perVertexData.BoneWeights == null)
        return;
      perVertexData.BoneIndices = numArray2;
      perVertexData.BoneWeights = numArray3;
    }

    private static Dictionary<int, int> GetUnpackedToPackedMapping(int[] triangleIndices, int[] uniqueFieldIndices, int vertexCount)
    {
      Dictionary<int, int> dictionary1 = new Dictionary<int, int>();
      foreach (KeyValuePair<int, List<int>> keyValuePair in SceneConverter.GetVertexToCornersMapping(triangleIndices))
      {
        List<int> source = keyValuePair.Value;
        int key1 = keyValuePair.Key;
        if (source.Count > 1)
        {
          List<int> list = source.Select<int, int>((Func<int, int>) (x => uniqueFieldIndices[x])).ToList<int>();
          Dictionary<int, int> dictionary2 = new Dictionary<int, int>();
          for (int index = 0; index < source.Count; ++index)
          {
            if (!dictionary2.ContainsKey(list[index]))
            {
              if (dictionary2.Count == 0)
              {
                dictionary2.Add(list[index], key1);
              }
              else
              {
                dictionary2.Add(list[index], vertexCount);
                ++vertexCount;
              }
            }
            dictionary1.Add(source[index], dictionary2[list[index]]);
          }
        }
        else
        {
          int key2 = source[0];
          dictionary1.Add(key2, key1);
        }
      }
      return dictionary1;
    }

    private static T[] UniqueItems<T>(T[] unpackedItems, out int[] packedIndices)
    {
      List<string> stringList = new List<string>();
      Dictionary<string, int> dictionary = new Dictionary<string, int>();
      int num = 0;
      for (int index = 0; index < unpackedItems.Length; ++index)
      {
        string empty = string.Empty;
        T unpackedItem = unpackedItems[index];
        string key;
        if (unpackedItem.GetType() == typeof (Vector2))
          key = SceneConverter.VectorHash((Vector2) (object) unpackedItem);
        else if (unpackedItem.GetType() == typeof (Vector3))
          key = SceneConverter.VectorHash((Vector3) (object) unpackedItem);
        else if (unpackedItem.GetType() == typeof (Vector4))
        {
          key = SceneConverter.VectorHash((Vector4) (object) unpackedItem);
        }
        else
        {
          if (unpackedItem.GetType() != typeof (Color))
            throw new NotImplementedException();
          key = SceneConverter.ColorHash((Color) (object) unpackedItem);
        }
        stringList.Add(key);
        if (!dictionary.ContainsKey(key))
        {
          dictionary.Add(key, num);
          ++num;
        }
      }
      T[] objArray = new T[dictionary.Count];
      packedIndices = new int[unpackedItems.Length];
      for (int index1 = 0; index1 < unpackedItems.Length; ++index1)
      {
        T unpackedItem = unpackedItems[index1];
        string index2 = stringList[index1];
        int index3 = dictionary[index2];
        objArray[index3] = unpackedItem;
        packedIndices[index1] = index3;
      }
      return objArray;
    }

    private static Dictionary<int, List<int>> GetVertexToCornersMapping(int[] triangleIndices)
    {
      Dictionary<int, List<int>> dictionary = new Dictionary<int, List<int>>();
      for (int index = 0; index < triangleIndices.Length; ++index)
      {
        if (!dictionary.ContainsKey(triangleIndices[index]))
          dictionary.Add(triangleIndices[index], new List<int>());
        dictionary[triangleIndices[index]].Add(index);
      }
      return dictionary;
    }

    private static string VectorHash(Vector2 vector)
    {
      byte[] bytes1 = BitConverter.GetBytes(vector.X);
      byte[] bytes2 = BitConverter.GetBytes(vector.Y);
      byte[] bb = new byte[16];
      int sourceIndex = 0;
      byte[] numArray = bb;
      int destinationIndex = 0;
      int length = 8;
      Array.Copy((Array) bytes1, sourceIndex, (Array) numArray, destinationIndex, length);
      Array.Copy((Array) bytes2, 0, (Array) bb, 8, 8);
      return BitConverter.ToString(new MurMur3().ComputeHash(bb));
    }

    private static string VectorHash(Vector3 vector)
    {
      byte[] bytes1 = BitConverter.GetBytes(vector.X);
      byte[] bytes2 = BitConverter.GetBytes(vector.Y);
      byte[] bytes3 = BitConverter.GetBytes(vector.Z);
      byte[] bb = new byte[24];
      int sourceIndex = 0;
      byte[] numArray = bb;
      int destinationIndex = 0;
      int length = 8;
      Array.Copy((Array) bytes1, sourceIndex, (Array) numArray, destinationIndex, length);
      Array.Copy((Array) bytes2, 0, (Array) bb, 8, 8);
      Array.Copy((Array) bytes3, 0, (Array) bb, 16, 8);
      return BitConverter.ToString(new MurMur3().ComputeHash(bb));
    }

    private static string VectorHash(Vector4 vector)
    {
      byte[] bytes1 = BitConverter.GetBytes(vector.X);
      byte[] bytes2 = BitConverter.GetBytes(vector.Y);
      byte[] bytes3 = BitConverter.GetBytes(vector.Z);
      byte[] bytes4 = BitConverter.GetBytes(vector.W);
      byte[] bb = new byte[32];
      int sourceIndex = 0;
      byte[] numArray = bb;
      int destinationIndex = 0;
      int length = 8;
      Array.Copy((Array) bytes1, sourceIndex, (Array) numArray, destinationIndex, length);
      Array.Copy((Array) bytes2, 0, (Array) bb, 8, 8);
      Array.Copy((Array) bytes3, 0, (Array) bb, 16, 8);
      Array.Copy((Array) bytes4, 0, (Array) bb, 24, 8);
      return BitConverter.ToString(new MurMur3().ComputeHash(bb));
    }

    private static string ColorHash(Color color)
    {
      byte[] bytes1 = BitConverter.GetBytes(color.R);
      byte[] bytes2 = BitConverter.GetBytes(color.G);
      byte[] bytes3 = BitConverter.GetBytes(color.B);
      byte[] bytes4 = BitConverter.GetBytes(color.A);
      byte[] bb = new byte[16];
      int sourceIndex = 0;
      byte[] numArray = bb;
      int destinationIndex = 0;
      int length = 4;
      Array.Copy((Array) bytes1, sourceIndex, (Array) numArray, destinationIndex, length);
      Array.Copy((Array) bytes2, 0, (Array) bb, 4, 4);
      Array.Copy((Array) bytes3, 0, (Array) bb, 8, 4);
      Array.Copy((Array) bytes4, 0, (Array) bb, 12, 4);
      return BitConverter.ToString(new MurMur3().ComputeHash(bb));
    }

    private static T[] UnpackVector<T>(int[] triangleIndices, T[] packedVector)
    {
      T[] objArray = new T[triangleIndices.Length];
      for (int index = 0; index < triangleIndices.Length; ++index)
        objArray[index] = packedVector[triangleIndices[index]];
      return objArray;
    }

    private class PerVertexData
    {
      public PerVertexData(Vector4[] coordinates, int[,] boneIndices, float[,] boneWeights)
      {
        this.Coordinates = coordinates;
        this.BoneIndices = boneIndices;
        this.BoneWeights = boneWeights;
      }

      public Vector4[] Coordinates { get; set; }

      public int[,] BoneIndices { get; set; }

      public float[,] BoneWeights { get; set; }
    }
  }
}
