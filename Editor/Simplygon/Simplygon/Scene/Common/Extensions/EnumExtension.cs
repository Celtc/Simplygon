﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Common.Extensions.EnumExtension
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.Scene.Common.Extensions
{
  internal static class EnumExtension
  {
    public static bool HasFlag(this Enum keys, Enum flag)
    {
      long uint64_1 = (long) Convert.ToUInt64((object) keys);
      ulong uint64_2 = Convert.ToUInt64((object) flag);
      long num = (long) uint64_2;
      return (uint64_1 & num) == (long) uint64_2;
    }
  }
}
