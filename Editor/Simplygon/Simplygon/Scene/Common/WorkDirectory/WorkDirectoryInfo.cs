﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Common.WorkDirectory.WorkDirectoryInfo
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Ionic.Zip;
using Ionic.Zlib;
using Simplygon.Scene.Common.Interfaces;
using System;
using System.IO;
using System.Globalization;

namespace Simplygon.Scene.Common.WorkDirectory
{
  [Serializable]
  internal class WorkDirectoryInfo : IWorkDirectoryInfo
  {
    public string RootDirectory { get; private set; }

    public string TempDirectory { get; private set; }

    public string InputFilename
    {
      get
      {
        return "Input.ssf";
      }
    }

    public string InputFilePath
    {
      get
      {
        return Path.Combine(this.RootDirectory, this.InputFilename);
      }
    }

    private string TextureRelativePathPrefix { get; set; }

    public string TexturesPath
    {
      get
      {
        return Path.Combine(this.RootDirectory, this.TexturesRelativePath);
      }
    }

    public string TexturesRelativePath
    {
      get
      {
        return string.Format(CultureInfo.InvariantCulture, "{0}{1}", (object) this.TextureRelativePathPrefix, (object) Path.DirectorySeparatorChar);
      }
    }

    public string JobsPath
    {
      get
      {
        return Path.Combine(this.TempDirectory, this.JobsRelativePath);
      }
    }

    public string JobsRelativePath
    {
      get
      {
        return string.Format(CultureInfo.InvariantCulture, "Jobs{0}", (object) Path.DirectorySeparatorChar);
      }
    }

    public string ExternalDataPath
    {
      get
      {
        return Path.Combine(this.RootDirectory, this.ExternalDataRelativePath);
      }
    }

    public string ExternalDataRelativePath
    {
      get
      {
        return string.Format(CultureInfo.InvariantCulture, "ExternalData{0}", (object) Path.DirectorySeparatorChar);
      }
    }

    public WorkDirectoryInfo(string rootDirectory)
    {
      this.RootDirectory = rootDirectory;
      this.TempDirectory = rootDirectory;
      this.TextureRelativePathPrefix = "Textures";
      this.CreateWorkDirectory();
    }

    public WorkDirectoryInfo(string rootDirectory, string tempWorkDirectory, string textureRelativePathPrefix = "Textures")
      : this(rootDirectory)
    {
      this.TempDirectory = tempWorkDirectory;
      this.TextureRelativePathPrefix = textureRelativePathPrefix;
    }

    public static string GetUniqueRelativePath(string rootDirectory, string relativeFilePath)
    {
      string directoryName = Path.GetDirectoryName(relativeFilePath);
      string withoutExtension = Path.GetFileNameWithoutExtension(relativeFilePath);
      string extension = Path.GetExtension(relativeFilePath);
      for (int index = 0; index < 10000; ++index)
      {
        string path2 = Path.Combine(directoryName, string.Format(CultureInfo.InvariantCulture, "{0}{1}{2}", (object) withoutExtension, index == 0 ? (object) string.Empty : (object) index.ToString(), (object) extension));
        if (!File.Exists(Path.Combine(rootDirectory, path2)))
          return path2;
      }
      return string.Empty;
    }

    private void CreateWorkDirectory()
    {
      try
      {
        Directory.CreateDirectory(this.RootDirectory);
      }
      catch (Exception ex)
      {
        throw new Exception("WorkDirectoryInfo - CreateDirectory failed. " + ex.Message);
      }
    }

    public static void ZipDirectory(string inputPath, string outputPath, string outputFileName, Action<int, string> saveProgressHandler = null)
    {
      WorkDirectoryInfo.ZipDirectory(inputPath, outputPath, outputFileName, WorkDirectoryInfo.ZipCompressionLevel.Level6, saveProgressHandler);
    }

    public static void ZipDirectory(string inputPath, string outputPath, string outputFileName, WorkDirectoryInfo.ZipCompressionLevel compressionLevel, Action<int, string> saveProgressHandler = null)
    {
      using (ZipFile zipFile = new ZipFile(Path.Combine(outputPath, outputFileName)))
      {
        zipFile.BufferSize = 1000000;
        zipFile.CodecBufferSize = 1000000;
        zipFile.CompressionLevel = (CompressionLevel) compressionLevel;
        zipFile.UseZip64WhenSaving = Zip64Option.Always;
        zipFile.AddDirectory(inputPath);
        if (saveProgressHandler != null)
          zipFile.SaveProgress += (EventHandler<SaveProgressEventArgs>) ((s, e) => saveProgressHandler(e.TotalBytesToTransfer == 0L ? 0 : (int) ((double) e.BytesTransferred / (double) e.TotalBytesToTransfer * 100.0), e.CurrentEntry != null ? e.CurrentEntry.FileName : string.Empty));
        zipFile.Save();
      }
    }

    public static void UnZipToDirectory(string inputPath, string outputPath, string inputFileName, Action<int, string> extractProgressHandler = null, bool overwrite = true)
    {
      using (ZipFile zipFile = ZipFile.Read(Path.Combine(inputPath, inputFileName)))
      {
        if (extractProgressHandler != null)
          zipFile.ExtractProgress += (EventHandler<ExtractProgressEventArgs>) ((s, e) => extractProgressHandler(e.TotalBytesToTransfer == 0L ? 0 : (int) ((double) e.BytesTransferred / (double) e.TotalBytesToTransfer * 100.0), e.CurrentEntry != null ? e.CurrentEntry.FileName : string.Empty));
        foreach (ZipEntry zipEntry in zipFile)
          zipEntry.Extract(outputPath, overwrite ? ExtractExistingFileAction.OverwriteSilently : ExtractExistingFileAction.DoNotOverwrite);
      }
    }

    public enum ZipCompressionLevel
    {
      Level0 = 0,
      None = 0,
      BestSpeed = 1,
      Level1 = 1,
      Level2 = 2,
      Level3 = 3,
      Level4 = 4,
      Level5 = 5,
      Default = 6,
      Level6 = 6,
      Level7 = 7,
      Level8 = 8,
      BestCompression = 9,
      Level9 = 9,
    }
  }
}
