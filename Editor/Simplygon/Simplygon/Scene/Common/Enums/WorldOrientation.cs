﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Common.Enums.WorldOrientation
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Scene.Common.Enums
{
  internal enum WorldOrientation
  {
    X_UP = 1,
    Y_UP = 2,
    Z_UP = 3,
    X_DOWN = 4,
    Y_DOWN = 5,
    Z_DOWN = 6,
  }
}
