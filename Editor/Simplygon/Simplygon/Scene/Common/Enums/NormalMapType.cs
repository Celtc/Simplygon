﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Common.Enums.NormalMapType
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Scene.Common.Enums
{
  internal enum NormalMapType
  {
    NotDefined,
    ObjectSpace_LeftHanded,
    ObjectSpace_RightHanded,
    TangentSpace_LeftHanded,
    TangentSpace_RightHanded,
    TangentSpace_3dsMax,
  }
}
