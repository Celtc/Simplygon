﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Common.Progress.SceneProgress
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Scene.Common.Progress
{
  internal class SceneProgress
  {
    public event SceneProgress.SceneProgressEventHandler OnProgress;

    public void ReportProgress(int value)
    {
      // ISSUE: reference to a compiler-generated field
      SceneProgress.SceneProgressEventHandler onProgress = this.OnProgress;
      if (onProgress == null)
        return;
      if (value > 100)
        value = 100;
      if (value < 0)
        value = 0;
      onProgress((object) this, new SceneProgressEventArgs(value));
    }

    public delegate void SceneProgressEventHandler(object sender, SceneProgressEventArgs e);
  }
}
