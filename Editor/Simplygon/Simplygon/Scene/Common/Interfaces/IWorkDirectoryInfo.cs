﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Common.Interfaces.IWorkDirectoryInfo
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Scene.Common.Interfaces
{
  internal interface IWorkDirectoryInfo
  {
    string RootDirectory { get; }

    string InputFilename { get; }

    string InputFilePath { get; }

    string TexturesPath { get; }

    string TexturesRelativePath { get; }

    string JobsPath { get; }

    string JobsRelativePath { get; }

    string ExternalDataPath { get; }

    string ExternalDataRelativePath { get; }
  }
}
