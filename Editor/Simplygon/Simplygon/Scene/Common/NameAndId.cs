﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Common.NameAndId
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Scene.Common
{
  internal class NameAndId
  {
    public string Id { get; set; }

    public string Name { get; set; }

    public NameAndId(string id, string name)
    {
      this.Id = id;
      this.Name = name;
    }
  }
}
