﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Asset.Material
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Scene.Common.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace Simplygon.Scene.Asset
{
  [Serializable]
  internal class Material : SceneObject
  {
    private List<MaterialChannel> materialChannels = new List<MaterialChannel>();
    private bool _IsSelected;

    public ReadOnlyCollection<MaterialChannel> MaterialChannels
    {
      get
      {
        return this.materialChannels.AsReadOnly();
      }
    }

    public GeneratedShader GeneratedShader { get; private set; }

    public MaterialModel MaterialModel { get; set; }

    public NormalMapType NormalMapType { get; set; }

    public event PropertyChangedEventHandler PropertyChanged;

    private void NotifyPropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      if (this.PropertyChanged == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.PropertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }

    public bool IsSelected
    {
      get
      {
        return this._IsSelected;
      }
      set
      {
        if (value == this._IsSelected)
          return;
        this._IsSelected = value;
        this.NotifyPropertyChanged(nameof (IsSelected));
      }
    }

    public Material(Guid id, string name)
      : base(id, name)
    {
      this.GeneratedShader = new GeneratedShader();
      this.MaterialModel = MaterialModel.NotDefined;
      this.NormalMapType = NormalMapType.TangentSpace_LeftHanded;
    }

    public new SceneObject DeepCopy()
    {
      Material material = this.MemberwiseClone() as Material;
      material.materialChannels = new List<MaterialChannel>(this.materialChannels.Count);
      for (int index = 0; index < this.materialChannels.Count; ++index)
        material.materialChannels.Add(this.materialChannels[index].DeepCopy());
      material.GeneratedShader = this.GeneratedShader.DeepCopy();
      return (SceneObject) material;
    }

    protected override void Initialize()
    {
    }

    protected override void Release()
    {
      this.materialChannels.Clear();
    }

    public MaterialChannel AddMaterialChannel(string name)
    {
      MaterialChannel materialChannel = new MaterialChannel(name);
      this.materialChannels.Add(materialChannel);
      return materialChannel;
    }

    public void RemoveMaterialChannel(string name)
    {
      this.materialChannels.RemoveAll((Predicate<MaterialChannel>) (item => item.ChannelName == name));
    }

    public void RemoveTextureFromMaterial(Guid id)
    {
      foreach (MaterialChannel materialChannel in this.materialChannels)
        materialChannel.RemoveTexture(id);
    }

    public MaterialChannel GetChannelByName(string name)
    {
      foreach (MaterialChannel materialChannel in this.materialChannels)
      {
        if (materialChannel.ChannelName == name)
          return materialChannel;
      }
      return (MaterialChannel) null;
    }

    public ReadOnlyCollection<MaterialChannel> GetActiveChannels()
    {
      return this.materialChannels.Where<MaterialChannel>((Func<MaterialChannel, bool>) (matChannel => matChannel.Textures.Count > 0)).ToList<MaterialChannel>().AsReadOnly();
    }

    public IEnumerable<string> GetActiveChannelNames()
    {
      return this.materialChannels.Where<MaterialChannel>((Func<MaterialChannel, bool>) (matChannel => matChannel.Textures.Count > 0)).Select<MaterialChannel, string>((Func<MaterialChannel, string>) (matChannel => matChannel.ChannelName));
    }
  }
}
