﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Asset.MaterialChannel
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Math;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Simplygon.Scene.Asset
{
  [Serializable]
  internal class MaterialChannel
  {
    private List<MaterialChannelTextureDescriptor> textures = new List<MaterialChannelTextureDescriptor>();
    public const float SpecularMultiplier = 128f;

    public string ChannelName { get; set; }

    public Color Color { get; set; }

    public string ShadingNetwork { get; set; }

    public float DeltagenShininess { get; set; }

    public float DeltagenReflectivity { get; set; }

    public float DeltagenReflectionExposure { get; set; }

    public float DeltagenFresnelMinimum { get; set; }

    public float DeltagenFresnelScale { get; set; }

    public float DeltagenFresnelExponent { get; set; }

    public bool DeltagenUseReflection { get; set; }

    public bool DeltagenUseFresnel { get; set; }

    public bool DeltagenUseClearCoat { get; set; }

    public bool DeltagenColoredReflection { get; set; }

    public float DeltagenTransparency { get; set; }

    public float DeltagenNormalMapScale { get; set; }

    public ReadOnlyCollection<MaterialChannelTextureDescriptor> Textures
    {
      get
      {
        return this.textures.AsReadOnly();
      }
    }

    public MaterialChannel(string name)
    {
      this.ChannelName = name;
      this.Color = Color.White;
    }

    public MaterialChannel DeepCopy()
    {
      MaterialChannel materialChannel = this.MemberwiseClone() as MaterialChannel;
      materialChannel.textures = new List<MaterialChannelTextureDescriptor>(this.textures.Count);
      for (int index = 0; index < this.textures.Count; ++index)
        materialChannel.textures.Add(this.textures[index].DeepCopy());
      return materialChannel;
    }

    public void AddTexture(Guid textureId, string texCoordSet)
    {
      if (this.textures.Any<MaterialChannelTextureDescriptor>((Func<MaterialChannelTextureDescriptor, bool>) (item => item.Texture == textureId)))
        return;
      this.textures.Add(new MaterialChannelTextureDescriptor()
      {
        Texture = textureId,
        TexCoordSet = texCoordSet
      });
    }

    public void RemoveTexture(Guid textureId)
    {
      this.textures.RemoveAll((Predicate<MaterialChannelTextureDescriptor>) (item => item.Texture == textureId));
    }

    public void RemoveAllTextures()
    {
      this.textures.Clear();
    }
  }
}
