﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Asset.ItemSet
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.Scene.Asset
{
  [Serializable]
  internal class ItemSet
  {
    public string Name { get; set; }

    public bool[] Items { get; set; }

    public ItemSet DeepCopy()
    {
      ItemSet itemSet = this.MemberwiseClone() as ItemSet;
      if (this.Items != null)
        itemSet.Items = this.Items.Clone() as bool[];
      return itemSet;
    }
  }
}
