﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Asset.MeshData
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Math;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Simplygon.Scene.Asset
{
  [Serializable]
  internal class MeshData
  {
    public Vector4[] Coordinates { get; set; }

    public Vector3[] Normals { get; set; }

    public Dictionary<string, Vector2[]> TexCoords { get; set; }

    public Dictionary<string, Color[]> VertexColors { get; set; }

    public Dictionary<string, byte[,]> CustomFields { get; set; }

    public Dictionary<string, Vector3[]> BlendShapes { get; set; }

    public Vector3[] Tangents { get; set; }

    public Vector3[] Bitangents { get; set; }

    public int[] Triangles { get; set; }

    public int[] MaterialIndices { get; set; }

    public int[,] BoneIndices { get; set; }

    public float[,] BoneWeights { get; set; }

    public int[] ChartID { get; set; }

    public List<ItemSet> VertexSets { get; set; }

    public List<ItemSet> EdgeSets { get; set; }

    public List<ItemSet> FaceSets { get; set; }

    public List<List<int>> VertexEdges { get; set; }

    public int[] EdgeNeighbors { get; set; }

    public bool[] VertexLocks { get; set; }

    public double[] VertexWeights { get; set; }

    public double[] VertexCrease { get; set; }

    public int[] OriginalVertexIndices { get; set; }

    public double[] EdgeCrease { get; set; }

    public double[] EdgeWeights { get; set; }

    public int[] OriginalTriangleIndices { get; set; }

    public int[] SmoothingGroup { get; set; }

    public double MaxDeviation { get; set; }

    public int PixelSize { get; set; }

    public bool HasCoordinates()
    {
      return this.Coordinates != null && this.Coordinates.Length != 0;
    }

    public bool HasTriangles()
    {
      return this.Triangles != null && this.Triangles.Length != 0;
    }

    public bool HasTexCoords()
    {
      return this.TexCoords != null && this.TexCoords.Count > 0;
    }

    public bool HasNormals()
    {
      return this.Normals != null && this.Normals.Length != 0;
    }

    public bool HasTangents()
    {
      return this.Tangents != null && this.Tangents.Length != 0;
    }

    public bool HasBitangents()
    {
      return this.Bitangents != null && this.Bitangents.Length != 0;
    }

    public bool HasVertexColors()
    {
      return this.VertexColors != null && this.VertexColors.Count > 0;
    }

    public bool HasCustomField()
    {
      return this.CustomFields != null && this.CustomFields.Count > 0;
    }

    public bool HasEdgeSets()
    {
      return this.EdgeSets != null && this.EdgeSets.Count > 0;
    }

    public bool HasFaceSets()
    {
      return this.FaceSets != null && this.FaceSets.Count > 0;
    }

    public bool HasVertexSets()
    {
      return this.VertexSets != null && this.VertexSets.Count > 0;
    }

    public bool HasBoneIndices()
    {
      return this.BoneIndices != null && this.BoneIndices.GetLength(0) > 0;
    }

    public bool HasBoneWeights()
    {
      return this.BoneIndices != null && this.BoneWeights.GetLength(0) > 0;
    }

    public bool HasMaxDeviation()
    {
      return this.MaxDeviation >= 0.0;
    }

    public bool HasPixelSize()
    {
      return this.PixelSize >= 0;
    }

    public Mesh ParentMesh { get; private set; }

    public Guid OverrideMaterial { get; set; }

    public int GlobalLODIndex { get; set; }

    public MeshData(Mesh parentMesh)
    {
      this.ParentMesh = parentMesh;
      this.TexCoords = new Dictionary<string, Vector2[]>();
      this.VertexColors = new Dictionary<string, Color[]>();
      this.CustomFields = new Dictionary<string, byte[,]>();
      this.BlendShapes = new Dictionary<string, Vector3[]>();
      this.VertexSets = new List<ItemSet>();
      this.EdgeSets = new List<ItemSet>();
      this.FaceSets = new List<ItemSet>();
      this.VertexEdges = new List<List<int>>();
      this.MaxDeviation = -1.0;
      this.PixelSize = -1;
      this.OverrideMaterial = Guid.Empty;
      this.GlobalLODIndex = -1;
    }

    public MeshData DeepCopy()
    {
      MeshData meshData1 = this.MemberwiseClone() as MeshData;
      if (this.Coordinates != null)
        meshData1.Coordinates = this.Coordinates.Clone() as Vector4[];
      if (this.Normals != null)
        meshData1.Normals = this.Normals.Clone() as Vector3[];
      if (this.Tangents != null)
        meshData1.Tangents = this.Tangents.Clone() as Vector3[];
      if (this.Bitangents != null)
        meshData1.Bitangents = this.Bitangents.Clone() as Vector3[];
      if (this.Triangles != null)
        meshData1.Triangles = this.Triangles.Clone() as int[];
      if (this.MaterialIndices != null)
        meshData1.MaterialIndices = this.MaterialIndices.Clone() as int[];
      if (this.BoneIndices != null)
        meshData1.BoneIndices = this.BoneIndices.Clone() as int[,];
      if (this.BoneWeights != null)
        meshData1.BoneWeights = this.BoneWeights.Clone() as float[,];
      if (this.ChartID != null)
        meshData1.ChartID = this.ChartID.Clone() as int[];
      if (this.VertexLocks != null)
        meshData1.VertexLocks = this.VertexLocks.Clone() as bool[];
      if (this.VertexWeights != null)
        meshData1.VertexWeights = this.VertexWeights.Clone() as double[];
      if (this.VertexCrease != null)
        meshData1.VertexCrease = this.VertexCrease.Clone() as double[];
      if (this.OriginalVertexIndices != null)
        meshData1.OriginalVertexIndices = this.OriginalVertexIndices.Clone() as int[];
      if (this.EdgeCrease != null)
        meshData1.EdgeCrease = this.EdgeCrease.Clone() as double[];
      if (this.EdgeWeights != null)
        meshData1.EdgeWeights = this.EdgeWeights.Clone() as double[];
      if (this.OriginalTriangleIndices != null)
        meshData1.OriginalTriangleIndices = this.OriginalTriangleIndices.Clone() as int[];
      MeshData meshData2 = meshData1;
      Dictionary<string, Vector2[]> texCoords = this.TexCoords;
      Func<KeyValuePair<string, Vector2[]>, string> func1 = (Func<KeyValuePair<string, Vector2[]>, string>) (item => item.Key);
      Dictionary<string, Vector2[]> dictionary1 = texCoords.ToDictionary<KeyValuePair<string, Vector2[]>, string, Vector2[]>(func1, (Func<KeyValuePair<string, Vector2[]>, Vector2[]>) (item => item.Value.Clone() as Vector2[]));
      meshData2.TexCoords = dictionary1;
      MeshData meshData3 = meshData1;
      Dictionary<string, Color[]> vertexColors = this.VertexColors;
      Func<KeyValuePair<string, Color[]>, string> func2 = (Func<KeyValuePair<string, Color[]>, string>) (item => item.Key);
      Dictionary<string, Color[]> dictionary2 = vertexColors.ToDictionary<KeyValuePair<string, Color[]>, string, Color[]>(func2, (Func<KeyValuePair<string, Color[]>, Color[]>) (item => item.Value.Clone() as Color[]));
      meshData3.VertexColors = dictionary2;
      MeshData meshData4 = meshData1;
      Dictionary<string, byte[,]> customFields = this.CustomFields;
      Func<KeyValuePair<string, byte[,]>, string> func3 = (Func<KeyValuePair<string, byte[,]>, string>) (item => item.Key);
      Dictionary<string, byte[,]> dictionary3 = customFields.ToDictionary<KeyValuePair<string, byte[,]>, string, byte[,]>(func3, (Func<KeyValuePair<string, byte[,]>, byte[,]>) (item => item.Value.Clone() as byte[,]));
      meshData4.CustomFields = dictionary3;
      meshData1.VertexSets = this.VertexSets.Select<ItemSet, ItemSet>((Func<ItemSet, ItemSet>) (item => item.DeepCopy())).ToList<ItemSet>();
      meshData1.EdgeSets = this.EdgeSets.Select<ItemSet, ItemSet>((Func<ItemSet, ItemSet>) (item => item.DeepCopy())).ToList<ItemSet>();
      meshData1.FaceSets = this.FaceSets.Select<ItemSet, ItemSet>((Func<ItemSet, ItemSet>) (item => item.DeepCopy())).ToList<ItemSet>();
      meshData1.VertexEdges = this.VertexEdges.Select<List<int>, List<int>>((Func<List<int>, List<int>>) (item1 => item1.Select<int, int>((Func<int, int>) (item2 => item2)).ToList<int>())).ToList<List<int>>();
      return meshData1;
    }

    public int NextEdge(int edge)
    {
      return edge / 3 * 3 + (edge + 1) % 3;
    }

    public void ComputeEdgeNeighbors()
    {
      Dictionary<MeshData.VertexPair, List<int>> dictionary = new Dictionary<MeshData.VertexPair, List<int>>();
      this.EdgeNeighbors = new int[this.Triangles.Length];
      for (int edge = 0; edge < this.Triangles.Length; ++edge)
      {
        int triangle1 = this.Triangles[edge];
        int triangle2 = this.Triangles[this.NextEdge(edge)];
        MeshData.VertexPair key = new MeshData.VertexPair(triangle1 < triangle2 ? triangle1 : triangle2, triangle1 > triangle2 ? triangle1 : triangle2);
        List<int> intList;
        if (dictionary.TryGetValue(key, out intList))
        {
          intList.Add(edge);
        }
        else
        {
          intList = new List<int>(32);
          intList.Add(edge);
          dictionary.Add(key, intList);
        }
      }
      foreach (KeyValuePair<MeshData.VertexPair, List<int>> keyValuePair in dictionary)
      {
        List<int> intList = keyValuePair.Value;
        if (intList.Count == 2)
        {
          this.EdgeNeighbors[intList[0]] = intList[1];
          this.EdgeNeighbors[intList[1]] = intList[0];
        }
        else
        {
          for (int index = 0; index < intList.Count; ++index)
            this.EdgeNeighbors[intList[index]] = -2;
        }
      }
    }

    public int VertexCount
    {
      get
      {
        if (this.Coordinates != null)
          return this.Coordinates.Length;
        return 0;
      }
    }

    public int CornerCount
    {
      get
      {
        if (this.Triangles != null)
          return this.Triangles.Length;
        return 0;
      }
    }

    public int TriangleCount
    {
      get
      {
        if (this.Triangles != null)
          return this.Triangles.Length / 3;
        return 0;
      }
    }

    public int PackedVertexCount
    {
      get
      {
        return this.ComputeNumPackedVertices();
      }
    }

    public bool Validate()
    {
      if (this.Coordinates.Length != this.VertexCount || this.Normals != null && this.Normals.Length != this.CornerCount || (this.Tangents != null && this.Tangents.Length != this.CornerCount || this.Bitangents != null && this.Bitangents.Length != this.CornerCount))
        return false;
      foreach (KeyValuePair<string, Vector2[]> texCoord in this.TexCoords)
      {
        if (string.IsNullOrEmpty(texCoord.Key) || texCoord.Value != null && texCoord.Value.Length != this.CornerCount)
          return false;
      }
      foreach (KeyValuePair<string, Color[]> vertexColor in this.VertexColors)
      {
        if (string.IsNullOrEmpty(vertexColor.Key) || vertexColor.Value != null && vertexColor.Value.Length != this.CornerCount)
          return false;
      }
      return (this.MaterialIndices == null || this.MaterialIndices.Length == this.TriangleCount) && (this.BoneIndices == null || this.BoneIndices.GetLength(0) == this.VertexCount) && (this.BoneWeights == null || this.BoneWeights.GetLength(0) == this.VertexCount);
    }

    public bool EdgesShareUVs(int edge, int neighborEdge)
    {
      if (this.TexCoords.Count == 0)
        return false;
      foreach (KeyValuePair<string, Vector2[]> texCoord in this.TexCoords)
      {
        Vector2 vector2_1 = texCoord.Value[edge];
        Vector2 vector2_2 = texCoord.Value[this.NextEdge(edge)];
        Vector2 vector2_3 = texCoord.Value[neighborEdge];
        Vector2 vector2_4 = texCoord.Value[this.NextEdge(neighborEdge)];
        if ((!(vector2_1 == vector2_4) || !(vector2_2 == vector2_3)) && (!(vector2_1 == vector2_3) || !(vector2_2 == vector2_4)))
          return false;
      }
      return true;
    }

    public void ExpandChartSearch(int edge)
    {
      int index1 = edge / 3;
      if (this.EdgeNeighbors[edge] < 0)
        return;
      int index2 = this.EdgeNeighbors[edge] / 3;
      if (this.ChartID[index2] > -1 || !this.EdgesShareUVs(edge, this.EdgeNeighbors[edge]))
        return;
      this.ChartID[index2] = this.ChartID[index1];
      for (int index3 = 0; index3 < 3; ++index3)
      {
        if (index2 * 3 + index3 != this.EdgeNeighbors[edge])
          this.ExpandChartSearch(index2 * 3 + index3);
      }
    }

    public void IdentifyCharts()
    {
      this.ChartID = new int[this.Triangles.Length / 3];
      for (int index = 0; index < this.Triangles.Length / 3; ++index)
        this.ChartID[index] = -1;
      int num = 0;
      for (int index1 = 0; index1 < this.Triangles.Length / 3; ++index1)
      {
        if (this.ChartID[index1] == -1)
        {
          this.ChartID[index1] = num++;
          List<int> intList = new List<int>();
          intList.Add(index1 * 3);
          intList.Add(index1 * 3 + 1);
          intList.Add(index1 * 3 + 2);
          for (int index2 = 0; index2 < intList.Count; ++index2)
          {
            int edge = intList[index2];
            int index3 = edge / 3;
            if (this.EdgeNeighbors[edge] >= 0)
            {
              int index4 = this.EdgeNeighbors[edge] / 3;
              if (this.ChartID[index4] <= -1 && this.EdgesShareUVs(edge, this.EdgeNeighbors[edge]))
              {
                this.ChartID[index4] = this.ChartID[index3];
                for (int index5 = 0; index5 < 3; ++index5)
                {
                  if (index4 * 3 + index5 != this.EdgeNeighbors[edge])
                    intList.Add(index4 * 3 + index5);
                }
              }
            }
          }
        }
      }
    }

    public void ComputeVertexEdges()
    {
      this.VertexEdges = new List<List<int>>(this.Coordinates.GetLength(0));
      for (int index = 0; index < this.VertexEdges.Capacity; ++index)
        this.VertexEdges.Add(new List<int>());
      for (int index = 0; index < this.Triangles.Length; ++index)
        this.VertexEdges[this.Triangles[index]].Add(index);
    }

    public int ComputeNumPackedVertices()
    {
      if (this.VertexEdges == null)
        this.ComputeVertexEdges();
      int num1 = 0;
      for (int index1 = 0; index1 < this.VertexEdges.Count; ++index1)
      {
        List<int> vertexEdge = this.VertexEdges[index1];
        int count = vertexEdge.Count;
        int[] array = new int[count];
        vertexEdge.CopyTo(array);
        int num2 = 1;
        int num3 = 0;
        int num4 = 0;
        foreach (int corner in array)
        {
          if (corner == -1)
          {
            ++num2;
          }
          else
          {
            ++num4;
            ++num3;
            for (int index2 = num2; index2 < count; ++index2)
            {
              int other_corner = array[index2];
              if (other_corner != -1 && this.CompareCorners(corner, other_corner))
              {
                array[index2] = -1;
                ++num4;
              }
            }
            if (num4 != count)
              ++num2;
            else
              break;
          }
        }
        num1 += num3;
      }
      return num1;
    }

    public bool CompareCorners(int corner, int other_corner)
    {
      if (this.Normals != null && this.Normals[corner] != this.Normals[other_corner])
        return false;
      foreach (KeyValuePair<string, Vector2[]> texCoord in this.TexCoords)
      {
        if (texCoord.Value != null && texCoord.Value[corner] != texCoord.Value[other_corner])
          return false;
      }
      return (this.Tangents == null || !(this.Tangents[corner] != this.Tangents[other_corner])) && (this.Bitangents == null || !(this.Bitangents[corner] != this.Bitangents[other_corner]));
    }

    public List<int> GetVertexStartFromCorner(int corner)
    {
      if (this.VertexEdges == null)
        return new List<int>();
      return this.VertexEdges[this.Triangles[corner]];
    }

    public List<int> GetVertexStartFromVertex(int vertex)
    {
      if (this.VertexEdges == null)
        return new List<int>();
      return this.VertexEdges[vertex];
    }

    public int GetNextEdge(int edge)
    {
      int num = edge + 1;
      if (num % 3 == 0)
        num -= 3;
      return num;
    }

    public struct VertexPair
    {
      private int v_small;
      private int v_big;

      public VertexPair(int v0, int v1)
      {
        this.v_small = v0;
        this.v_big = v1;
      }
    }
  }
}
