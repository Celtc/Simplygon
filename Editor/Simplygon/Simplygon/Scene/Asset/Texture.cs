﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Asset.Texture
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Scene.Common.Enums;
using System;

namespace Simplygon.Scene.Asset
{
  [Serializable]
  internal class Texture : SceneObject
  {
    public ColorSpaceType ColorSpace { get; set; }

    public string Path { get; set; }

    public Texture(Guid id, string name, string path)
      : base(id, name)
    {
      this.Path = path;
      this.ColorSpace = ColorSpaceType.NotDefined;
    }

    protected override void Initialize()
    {
      base.Initialize();
    }

    public new SceneObject DeepCopy()
    {
      return (SceneObject) (this.MemberwiseClone() as Texture);
    }
  }
}
