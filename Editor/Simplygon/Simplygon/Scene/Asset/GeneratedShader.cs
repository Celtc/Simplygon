﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Asset.GeneratedShader
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.Scene.Asset
{
  [Serializable]
  internal class GeneratedShader
  {
    public string[] TexCoordsLayout;
    public string[] VertexColorsLayout;
    public string[] Textures;
    public string GeneratedShaderCode;

    public GeneratedShader()
    {
      this.TexCoordsLayout = (string[]) null;
      this.VertexColorsLayout = (string[]) null;
      this.Textures = (string[]) null;
      this.GeneratedShaderCode = (string) null;
    }

    public GeneratedShader DeepCopy()
    {
      GeneratedShader generatedShader = this.MemberwiseClone() as GeneratedShader;
      if (this.TexCoordsLayout != null)
        generatedShader.TexCoordsLayout = this.TexCoordsLayout.Clone() as string[];
      if (this.VertexColorsLayout != null)
        generatedShader.VertexColorsLayout = this.VertexColorsLayout.Clone() as string[];
      if (this.Textures != null)
        generatedShader.Textures = this.Textures.Clone() as string[];
      return generatedShader;
    }
  }
}
