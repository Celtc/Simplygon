﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Asset.Mesh
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Math;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Simplygon.Scene.Asset
{
  [Serializable]
  internal class Mesh : SceneObject, INotifyPropertyChanged
  {
    private List<MeshData> meshSources;
    private bool _IsSelected;
    private bool _isTemporarySelected;

    public List<Guid> Materials { get; private set; }

    public List<Guid> Bones { get; private set; }

    public int LODCount
    {
      get
      {
        return this.meshSources.Count - 1;
      }
    }

    public int MeshSourceCount
    {
      get
      {
        return this.meshSources.Count;
      }
    }

    public BoundingBox BBox { get; set; }

    public event PropertyChangedEventHandler PropertyChanged;

    private void NotifyPropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      if (this.PropertyChanged == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.PropertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }

    public bool IsSelected
    {
      get
      {
        return this._IsSelected;
      }
      set
      {
        if (value == this._IsSelected)
          return;
        this._IsSelected = value;
        this.NotifyPropertyChanged(nameof (IsSelected));
      }
    }

    public bool IsTemporarySelected
    {
      get
      {
        return this._isTemporarySelected;
      }
      set
      {
        if (value == this._isTemporarySelected)
          return;
        this._isTemporarySelected = value;
        this.NotifyPropertyChanged(nameof (IsTemporarySelected));
      }
    }

    public MeshData this[int i]
    {
      get
      {
        return this.meshSources[i];
      }
      set
      {
        this.meshSources[i] = value;
      }
    }

    public List<MeshData> MeshSources
    {
      get
      {
        return this.meshSources;
      }
      set
      {
        this.meshSources = value;
      }
    }

    public Mesh(Guid id, string name)
      : base(id, name)
    {
      this.Materials = new List<Guid>();
      this.Bones = new List<Guid>();
      this.meshSources = new List<MeshData>();
      this.meshSources.Add(new MeshData(this));
      this.Id = id;
    }

    public new SceneObject DeepCopy()
    {
      Mesh mesh = this.MemberwiseClone() as Mesh;
      mesh.Bones = this.Bones.Select<Guid, Guid>((Func<Guid, Guid>) (item => item)).ToList<Guid>();
      mesh.Materials = this.Materials.Select<Guid, Guid>((Func<Guid, Guid>) (item => item)).ToList<Guid>();
      mesh.MeshSources = this.MeshSources.Select<MeshData, MeshData>((Func<MeshData, MeshData>) (item => item.DeepCopy())).ToList<MeshData>();
      return (SceneObject) mesh;
    }

    protected override void Initialize()
    {
      base.Initialize();
    }

    public void AddMaterial(Guid materialId)
    {
      if (this.Materials.Any<Guid>((Func<Guid, bool>) (item => item == materialId)))
        return;
      this.Materials.Add(materialId);
    }

    public void RemoveMaterial(Guid materialId)
    {
      this.Materials.RemoveAll((Predicate<Guid>) (item => item == materialId));
    }

    public void RemoveAllMaterial()
    {
      this.Materials.Clear();
    }

    public void CreateMeshSource(int count)
    {
      uint num = 0;
      while ((long) num < (long) count)
        ++num;
      this.meshSources.Add(new MeshData(this));
    }

    public void ClearMeshSources()
    {
      this.meshSources.Clear();
    }

    public void AddMeshSourceData(MeshData data)
    {
      this.meshSources.Add(data);
    }

    public void UpdateMeshSourceData(MeshData data, int srcindex)
    {
      this.meshSources[srcindex] = data;
    }

    public void AddMeshSourceDataRange(IEnumerable<MeshData> data)
    {
      foreach (MeshData meshData in data)
        this.meshSources.Add(meshData);
    }

    public bool Validate()
    {
      foreach (MeshData meshSource in this.meshSources)
      {
        if (!meshSource.Validate())
          return false;
      }
      return true;
    }
  }
}
