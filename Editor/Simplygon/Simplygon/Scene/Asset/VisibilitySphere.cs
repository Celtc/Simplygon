﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Asset.VisibilitySphere
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.Scene.Asset
{
  [Serializable]
  internal class VisibilitySphere : SceneObject
  {
    public bool IsEnabled { get; set; }

    public float Pitch { get; set; }

    public float Yaw { get; set; }

    public float Coverage { get; set; }

    public VisibilitySphere()
      : base(Guid.NewGuid(), string.Empty)
    {
    }

    public VisibilitySphere(Guid id, string name)
      : base(id, name)
    {
    }

    protected override void Initialize()
    {
      base.Initialize();
    }

    public new SceneObject DeepCopy()
    {
      return (SceneObject) (this.MemberwiseClone() as VisibilitySphere);
    }
  }
}
