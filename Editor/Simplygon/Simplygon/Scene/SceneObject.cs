﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.SceneObject
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.Scene
{
  [Serializable]
  internal abstract class SceneObject
  {
    public Guid Id { get; set; }

    public uint Index { get; protected set; }

    public string Name { get; set; }

    public object Tag { get; set; }

    public SceneObject(Guid id, string name)
    {
      this.Id = id;
      this.Index = SceneFactory.RequestObjectIndex();
      this.Name = name;
      this.Initialize();
    }

    protected virtual void Initialize()
    {
    }

    protected virtual void Release()
    {
    }

    public SceneObject DeepCopy()
    {
      return this.MemberwiseClone() as SceneObject;
    }
  }
}
