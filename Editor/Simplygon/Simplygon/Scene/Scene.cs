﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Scene
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Hash;
using Simplygon.Math;
using Simplygon.Scene.Asset;
using Simplygon.Scene.Common;
using Simplygon.Scene.Common.Enums;
using Simplygon.Scene.Graph;
using Simplygon.Scene.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Globalization;

namespace Simplygon.Scene
{
    [Serializable]
    internal class Scene
    {
        private Dictionary<Guid, Node> nodes = new Dictionary<Guid, Node>();
        private Dictionary<Guid, Node> MeshIDNodes = new Dictionary<Guid, Node>();
        private Dictionary<Guid, Mesh> meshes = new Dictionary<Guid, Mesh>();
        private Dictionary<Guid, Material> materials = new Dictionary<Guid, Material>();
        private Dictionary<Guid, Texture> textures = new Dictionary<Guid, Texture>();
        private Dictionary<Guid, ExternalData> externaldatas = new Dictionary<Guid, ExternalData>();
        private List<Material> materialList = new List<Material>();
        private bool MeshIDNodesIsDirty;
        private const string DefaultMaterial_GUID = "249C9B6E-9C40-4665-B333-8C30B919F9EA";

        public string Name
        {
            get; set;
        }

        public string SceneDirectory
        {
            get; set;
        }

        public Node RootNode
        {
            get; private set;
        }

        public string Hash
        {
            get; set;
        }

        public WorldOrientation WorldOrientation
        {
            get; set;
        }

        public CoordinateSystem OriginalCoordinateSystem
        {
            get; set;
        }

        public Dictionary<string, string[]> ProxyGroupSets
        {
            get; set;
        }

        public Dictionary<string, string[]> OccluderGroupSets
        {
            get; set;
        }

        public Dictionary<NameAndId, List<string>> SelectionGroupSets
        {
            get; set;
        }

        public Dictionary<NameAndId, List<Plane>> CuttingPlanes
        {
            get; set;
        }

        public void SetMeshIDNodesIsDirty()
        {
            this.MeshIDNodesIsDirty = true;
        }

        public int NodesCount
        {
            get
            {
                return this.nodes.Count;
            }
        }

        public int MaterialsCount
        {
            get
            {
                return this.materials.Count;
            }
        }

        public int MeshesCount
        {
            get
            {
                return this.meshes.Count;
            }
        }

        public int TexturesCount
        {
            get
            {
                return this.textures.Count;
            }
        }

        public int ExternalDatasCount
        {
            get
            {
                return this.externaldatas.Count;
            }
        }

        public Dictionary<Guid, Node> Nodes
        {
            get
            {
                return this.nodes;
            }
        }

        public Dictionary<Guid, Mesh> Meshes
        {
            get
            {
                return this.meshes;
            }
        }

        public Dictionary<Guid, Material> Materials
        {
            get
            {
                return this.materials;
            }
        }

        public Dictionary<Guid, Texture> Textures
        {
            get
            {
                return this.textures;
            }
        }

        public Dictionary<Guid, ExternalData> ExternalData
        {
            get
            {
                return this.externaldatas;
            }
        }

        public Material DefaultMaterial
        {
            get; private set;
        }

        public List<Material> MaterialList
        {
            get
            {
                return new List<Material>(materialList);
            }
        }

        public Scene()
        {
            this.Name = string.Empty;
            this.MeshIDNodesIsDirty = true;
            this.OriginalCoordinateSystem = CoordinateSystem.Left;
            this.WorldOrientation = WorldOrientation.Y_UP;
            this.ProxyGroupSets = new Dictionary<string, string[]>();
            this.OccluderGroupSets = new Dictionary<string, string[]>();
            this.SelectionGroupSets = new Dictionary<NameAndId, List<string>>();
            this.CuttingPlanes = new Dictionary<NameAndId, List<Plane>>();
            this.CreateDefaultSceneMaterail();
        }

        public List<Guid> FindAllBonesInUse()
        {
            var source = new List<Guid>();
            foreach (Mesh mesh in this.meshes.Values)
                source.AddRange(mesh.Bones);
            return source.Distinct().ToList();
        }

        private void CreateDefaultSceneMaterail()
        {
        }

        public string GetSceneHash()
        {
            List<Mesh> selectedMeshes = this.GetSelectedMeshes();
            List<Node> selectedNodes = this.GetSelectedNodes();
            bool flag = false;
            if (selectedMeshes.Count == 0 && selectedNodes.Count == 0)
                flag = true;
            List<string> stringList;
            if (flag)
            {
                stringList = new List<string>(this.ExternalDatasCount + this.MaterialsCount + this.MeshesCount);
                foreach (KeyValuePair<Guid, ExternalData> externaldata in this.externaldatas)
                {
                    foreach (string file1 in Directory.GetFiles(externaldata.Value.Path))
                    {
                        string file2 = SHA256.ComputeFile(file1);
                        stringList.Add(file2);
                    }
                }
                foreach (KeyValuePair<Guid, Mesh> mesh in this.meshes)
                {
                    string guid = SHA256.ComputeGuid(mesh.Key);
                    stringList.Add(guid);
                }
                foreach (KeyValuePair<Guid, Material> material in this.materials)
                {
                    string guid = SHA256.ComputeGuid(material.Key);
                    stringList.Add(guid);
                }
                foreach (KeyValuePair<Guid, Node> node in this.nodes)
                {
                    if (this.RootNode.Id != node.Key)
                    {
                        string guid = SHA256.ComputeGuid(node.Key);
                        stringList.Add(guid);
                    }
                }
            }
            else
            {
                stringList = new List<string>(this.ExternalDatasCount + this.MaterialsCount + this.MeshesCount);
                foreach (KeyValuePair<Guid, ExternalData> externaldata in this.externaldatas)
                {
                    foreach (string file1 in Directory.GetFiles(externaldata.Value.Path))
                    {
                        string file2 = SHA256.ComputeFile(file1);
                        stringList.Add(file2);
                    }
                }
                foreach (SceneObject sceneObject in selectedMeshes)
                {
                    string guid = SHA256.ComputeGuid(sceneObject.Id);
                    stringList.Add(guid);
                }
                foreach (KeyValuePair<Guid, Node> node in this.nodes)
                {
                    if (this.RootNode.Id != node.Key)
                    {
                        string guid = SHA256.ComputeGuid(node.Key);
                        stringList.Add(guid);
                    }
                }
            }
            stringList.Add(SHA256.ComputeString(this.RootNode.Id.ToString()));
            stringList.Sort();
            string empty = string.Empty;
            foreach (string str in stringList)
                empty += str;
            return SHA256.ComputeString(empty);
        }

        public void RenameTexturesWithSpaces()
        {
            foreach (KeyValuePair<Guid, Texture> texture in this.Textures)
            {
                string path2 = texture.Value.Path.Replace(' ', '_').Replace('\t', '_').Replace('\r', '_').Replace('\n', '_');
                string str = Path.Combine(this.SceneDirectory, texture.Value.Path);
                string destFileName = Path.Combine(this.SceneDirectory, path2);
                Console.WriteLine("Moving:");
                Console.WriteLine(str);
                Console.WriteLine(destFileName);
                if (File.Exists(str))
                    File.Copy(str, destFileName, true);
                texture.Value.Path = path2;
            }
        }

        public void Validate(bool force = false)
        {
            foreach (KeyValuePair<Guid, Mesh> mesh in this.Meshes)
            {
                if (mesh.Value.Materials.Count == 0)
                {
                    var guid1 = new Guid("249C9B6E-9C40-4665-B333-8C30B919F9EA");
                    if (!this.Materials.ContainsKey(guid1))
                    {
                        var material = new Material(guid1, "DefaultMaterial");
                        MaterialChannel materialChannel1 = material.AddMaterialChannel("Ambient");
                        MaterialChannel materialChannel2 = material.AddMaterialChannel("Diffuse");
                        MaterialChannel materialChannel3 = material.AddMaterialChannel("Specular");
                        materialChannel1.Color = new Color(0.01f, 0.01f, 0.01f, 1f, 32);
                        materialChannel2.Color = new Color(1f, 1f, 1f, 1f, 32);
                        materialChannel3.Color = new Color(0.0f, 0.0f, 0.0f, 1f, 32);
                        MaterialChannel materialChannel4 = materialChannel1;
                        string format1 = "<SimplygonShadingNetwork version=\"1.0\">\n{0}</SimplygonShadingNetwork>";
                        var guid2 = Guid.NewGuid();
                        string shadingColorNodeXml1 = ShadingNetworkHelper.GetShadingColorNodeXml(guid2.ToString(), "AmbientColor", materialChannel1.Color);
                        string str1 = string.Format(format1, shadingColorNodeXml1);
                        materialChannel4.ShadingNetwork = str1;
                        MaterialChannel materialChannel5 = materialChannel2;
                        string format2 = "<SimplygonShadingNetwork version=\"1.0\">\n{0}</SimplygonShadingNetwork>";
                        guid2 = Guid.NewGuid();
                        string shadingColorNodeXml2 = ShadingNetworkHelper.GetShadingColorNodeXml(guid2.ToString(), "DiffuseColor", materialChannel2.Color);
                        string str2 = string.Format(format2, shadingColorNodeXml2);
                        materialChannel5.ShadingNetwork = str2;
                        MaterialChannel materialChannel6 = materialChannel3;
                        string format3 = "<SimplygonShadingNetwork version=\"1.0\">\n{0}</SimplygonShadingNetwork>";
                        guid2 = Guid.NewGuid();
                        string shadingColorNodeXml3 = ShadingNetworkHelper.GetShadingColorNodeXml(guid2.ToString(), "SpecularColor", materialChannel3.Color);
                        string str3 = string.Format(format3, shadingColorNodeXml3);
                        materialChannel6.ShadingNetwork = str3;
                        this.Materials.Add(guid1, material);
                    }
                    mesh.Value.Materials.Add(guid1);
                }
                foreach (MeshData meshSource in mesh.Value.MeshSources)
                {
                    if (meshSource.MaterialIndices == null || meshSource.MaterialIndices.Length != meshSource.TriangleCount)
                        meshSource.MaterialIndices = Enumerable.Repeat(0, meshSource.TriangleCount).ToArray();
                    if (((meshSource.Normals == null ? 1 : (meshSource.Normals.Length == 0 ? 1 : 0)) | (force ? 1 : 0)) != 0)
                        this.ComputeSmoothNormals(meshSource);
                    if (((meshSource.Tangents == null || meshSource.Tangents.Length == 0 || meshSource.Bitangents == null ? 1 : (meshSource.Bitangents.Length == 0 ? 1 : 0)) | (force ? 1 : 0)) != 0)
                        Simplygon.Scene.Scene.ComputeTangentsAndBitangents(meshSource);
                }
            }
        }

        private void ComputeSmoothNormals(MeshData meshData)
        {
            meshData.Normals = new Vector3[meshData.CornerCount];
            Vector3[] vector3Array = new Vector3[meshData.VertexCount];
            for (int index = 0; index < meshData.TriangleCount; ++index)
            {
                int triangle1 = meshData.Triangles[index * 3];
                int triangle2 = meshData.Triangles[index * 3 + 1];
                int triangle3 = meshData.Triangles[index * 3 + 2];
                Vector3 triangleNormal = MathHelper.ComputeTriangleNormal(meshData.Coordinates[triangle1].ToVector3(), meshData.Coordinates[triangle2].ToVector3(), meshData.Coordinates[triangle3].ToVector3());
                vector3Array[triangle1] += triangleNormal;
                vector3Array[triangle2] += triangleNormal;
                vector3Array[triangle3] += triangleNormal;
            }
            for (int index = 0; index < meshData.CornerCount; ++index)
                meshData.Normals[index] = vector3Array[meshData.Triangles[index]].Normalize();
        }

        public static void ComputeTangentsAndBitangents(MeshData meshData)
        {
            if (meshData.TexCoords == null || meshData.TexCoords.Count <= 0)
                return;
            if (meshData.TexCoords.First().Value.Length == meshData.CornerCount)
            {
                meshData.Tangents = new Vector3[meshData.CornerCount];
                meshData.Bitangents = new Vector3[meshData.CornerCount];
                Vector3[] vector3Array1 = new Vector3[meshData.CornerCount];
                Vector3[] vector3Array2 = new Vector3[meshData.CornerCount];
                for (long index = 0; index < meshData.TriangleCount; ++index)
                {
                    long triangle1 = meshData.Triangles[index * 3L];
                    long triangle2 = meshData.Triangles[index * 3L + 1L];
                    long triangle3 = meshData.Triangles[index * 3L + 2L];
                    var vector3_1 = meshData.Coordinates[triangle1].ToVector3();
                    var vector3_2 = meshData.Coordinates[triangle2].ToVector3();
                    var vector3_3 = meshData.Coordinates[triangle3].ToVector3();
                    Vector2 vector2_1 = meshData.TexCoords.First().Value[index * 3L];
                    Vector2 vector2_2 = meshData.TexCoords.First().Value[index * 3L + 1L];
                    Vector2 vector2_3 = meshData.TexCoords.First().Value[index * 3L + 2L];
                    double num1 = vector3_2.X - vector3_1.X;
                    double num2 = vector3_3.X - vector3_1.X;
                    double num3 = vector3_2.Y - vector3_1.Y;
                    double num4 = vector3_3.Y - vector3_1.Y;
                    double num5 = vector3_2.Z - vector3_1.Z;
                    double num6 = vector3_3.Z - vector3_1.Z;
                    double num7 = vector2_2.X - vector2_1.X;
                    double num8 = vector2_3.X - vector2_1.X;
                    double num9 = vector2_2.Y - vector2_1.Y;
                    double num10 = vector2_3.Y - vector2_1.Y;
                    double num11 = num7 * num10 - num8 * num9;
                    if (num11 != 0.0)
                        num11 = 1.0 / num11;
                    var vector3_4 = new Vector3((num10 * num1 - num9 * num2) * num11, (num10 * num3 - num9 * num4) * num11, (num10 * num5 - num9 * num6) * num11);
                    var vector3_5 = new Vector3((num7 * num2 - num8 * num1) * num11, (num7 * num4 - num8 * num3) * num11, (num7 * num6 - num8 * num5) * num11);
                    vector3Array1[index * 3L] = vector3Array1[index * 3L] + vector3_4;
                    vector3Array1[index * 3L + 1L] = vector3Array1[index * 3L + 1L] + vector3_4;
                    vector3Array1[index * 3L + 2L] = vector3Array1[index * 3L + 2L] + vector3_4;
                    vector3Array2[index * 3L] = vector3Array2[index * 3L] + vector3_5;
                    vector3Array2[index * 3L + 1L] = vector3Array2[index * 3L + 1L] + vector3_5;
                    vector3Array2[index * 3L + 2L] = vector3Array2[index * 3L + 2L] + vector3_5;
                }
                for (long index = 0; index < meshData.CornerCount; ++index)
                {
                    Vector3 vector3_1 = meshData.Normals[index].Normalize();
                    Vector3 vector3_2 = vector3Array1[index] - vector3_1 * Vector3.Dot(vector3_1, vector3Array1[index]);
                    Vector3 vector3_3 = vector3_2.Normalize();
                    vector3_2 = vector3Array2[index] - vector3_1 * Vector3.Dot(vector3_1, vector3Array2[index]);
                    Vector3 left1 = vector3_2.Normalize();
                    bool flag = false;
                    if (vector3Array1[index].Length() > 0.0 && vector3_3.Length() > 0.0)
                    {
                        vector3_2 = Vector3.Cross(vector3_1, vector3_3);
                        left1 = vector3_2.Normalize();
                        if (left1.Length() == 0.0)
                            flag = true;
                        if (Vector3.Dot(vector3_3, vector3Array1[index]) < 0.0)
                            vector3_3 *= -1.0;
                        if (Vector3.Dot(left1, vector3Array2[index]) < 0.0)
                            left1 *= -1.0;
                    }
                    else if (vector3Array2[index].Length() > 0.0 && left1.Length() > 0.0)
                    {
                        vector3_2 = Vector3.Cross(left1, vector3_1);
                        vector3_3 = vector3_2.Normalize();
                        if (vector3_3.Length() == 0.0)
                            flag = true;
                        if (Vector3.Dot(vector3_3, vector3Array1[index]) < 0.0)
                            vector3_3 *= -1.0;
                        if (Vector3.Dot(left1, vector3Array2[index]) < 0.0)
                            left1 *= -1.0;
                    }
                    else
                        flag = true;
                    if (flag)
                    {
                        vector3_3 = Vector3.Zero;
                        left1 = Vector3.Zero;
                        var left2 = new Vector3(0.0, 1.0, 0.0);
                        var left3 = new Vector3(1.0, 0.0, 0.0);
                        Vector3 right = vector3_1;
                        vector3_2 = Vector3.Cross(left2, right);
                        vector3_3 = vector3_2.Normalize();
                        if (vector3_3.Length() < 0.9)
                        {
                            vector3_2 = Vector3.Cross(left3, vector3_1);
                            vector3_3 = vector3_2.Normalize();
                        }
                        vector3_2 = Vector3.Cross(vector3_1, vector3_3);
                        left1 = vector3_2.Normalize();
                    }
                    meshData.Tangents[index] = vector3_3;
                    meshData.Bitangents[index] = left1 * -1.0;
                }
            }
            else
            {
                meshData.Tangents = new Vector3[meshData.VertexCount];
                meshData.Bitangents = new Vector3[meshData.VertexCount];
                Vector3[] vector3Array1 = new Vector3[meshData.VertexCount];
                Vector3[] vector3Array2 = new Vector3[meshData.VertexCount];
                for (long index = 0; index < meshData.TriangleCount; ++index)
                {
                    long triangle1 = meshData.Triangles[index * 3L];
                    long triangle2 = meshData.Triangles[index * 3L + 1L];
                    long triangle3 = meshData.Triangles[index * 3L + 2L];
                    var vector3_1 = meshData.Coordinates[triangle1].ToVector3();
                    var vector3_2 = meshData.Coordinates[triangle2].ToVector3();
                    var vector3_3 = meshData.Coordinates[triangle3].ToVector3();
                    Vector2 vector2_1 = meshData.TexCoords.First().Value[triangle1];
                    Vector2 vector2_2 = meshData.TexCoords.First().Value[triangle2];
                    Vector2 vector2_3 = meshData.TexCoords.First().Value[triangle3];
                    double num1 = vector3_2.X - vector3_1.X;
                    double num2 = vector3_3.X - vector3_1.X;
                    double num3 = vector3_2.Y - vector3_1.Y;
                    double num4 = vector3_3.Y - vector3_1.Y;
                    double num5 = vector3_2.Z - vector3_1.Z;
                    double num6 = vector3_3.Z - vector3_1.Z;
                    double num7 = vector2_2.X - vector2_1.X;
                    double num8 = vector2_3.X - vector2_1.X;
                    double num9 = vector2_2.Y - vector2_1.Y;
                    double num10 = vector2_3.Y - vector2_1.Y;
                    double num11 = num7 * num10 - num8 * num9;
                    if (num11 != 0.0)
                        num11 = 1.0 / num11;
                    var vector3_4 = new Vector3((num10 * num1 - num9 * num2) * num11, (num10 * num3 - num9 * num4) * num11, (num10 * num5 - num9 * num6) * num11);
                    var vector3_5 = new Vector3((num7 * num2 - num8 * num1) * num11, (num7 * num4 - num8 * num3) * num11, (num7 * num6 - num8 * num5) * num11);
                    vector3Array1[triangle1] = vector3Array1[triangle1] + vector3_4;
                    vector3Array1[triangle2] = vector3Array1[triangle2] + vector3_4;
                    vector3Array1[triangle3] = vector3Array1[triangle3] + vector3_4;
                    vector3Array2[triangle1] = vector3Array2[triangle1] + vector3_5;
                    vector3Array2[triangle2] = vector3Array2[triangle2] + vector3_5;
                    vector3Array2[triangle3] = vector3Array2[triangle3] + vector3_5;
                }
                for (long index = 0; index < meshData.VertexCount; ++index)
                {
                    Vector3 vector3_1 = meshData.Normals[index].Normalize();
                    Vector3 vector3_2 = vector3Array1[index] - vector3_1 * Vector3.Dot(vector3_1, vector3Array1[index]);
                    Vector3 vector3_3 = vector3_2.Normalize();
                    vector3_2 = vector3Array2[index] - vector3_1 * Vector3.Dot(vector3_1, vector3Array2[index]);
                    Vector3 left1 = vector3_2.Normalize();
                    bool flag = false;
                    if (vector3Array1[index].Length() > 0.0 && vector3_3.Length() > 0.0)
                    {
                        vector3_2 = Vector3.Cross(vector3_1, vector3_3);
                        left1 = vector3_2.Normalize();
                        if (left1.Length() == 0.0)
                            flag = true;
                        if (Vector3.Dot(vector3_3, vector3Array1[index]) < 0.0)
                            vector3_3 *= -1.0;
                        if (Vector3.Dot(left1, vector3Array2[index]) < 0.0)
                            left1 *= -1.0;
                    }
                    else if (vector3Array2[index].Length() > 0.0 && left1.Length() > 0.0)
                    {
                        vector3_2 = Vector3.Cross(left1, vector3_1);
                        vector3_3 = vector3_2.Normalize();
                        if (vector3_3.Length() == 0.0)
                            flag = true;
                        if (Vector3.Dot(vector3_3, vector3Array1[index]) < 0.0)
                            vector3_3 *= -1.0;
                        if (Vector3.Dot(left1, vector3Array2[index]) < 0.0)
                            left1 *= -1.0;
                    }
                    else
                        flag = true;
                    if (flag)
                    {
                        vector3_3 = Vector3.Zero;
                        left1 = Vector3.Zero;
                        var left2 = new Vector3(0.0, 1.0, 0.0);
                        var left3 = new Vector3(1.0, 0.0, 0.0);
                        Vector3 right = vector3_1;
                        vector3_2 = Vector3.Cross(left2, right);
                        vector3_3 = vector3_2.Normalize();
                        if (vector3_3.Length() < 0.9)
                        {
                            vector3_2 = Vector3.Cross(left3, vector3_1);
                            vector3_3 = vector3_2.Normalize();
                        }
                        vector3_2 = Vector3.Cross(vector3_1, vector3_3);
                        left1 = vector3_2.Normalize();
                    }
                    meshData.Tangents[index] = vector3_3;
                    meshData.Bitangents[index] = left1 * -1.0;
                }
            }
        }

        public Scene DeepCopy()
        {
            var scene1 = this.MemberwiseClone() as Scene;

            Scene scene2 = scene1;
            Dictionary<Guid, Node> nodes = this.nodes;
            var dictionary1 = nodes.ToDictionary(item => item.Key, item => item.Value.DeepCopy() as Node);
            scene2.nodes = dictionary1;

            Scene scene3 = scene1;
            Dictionary<Guid, Mesh> meshes = this.meshes;
            var dictionary2 = meshes.ToDictionary(item => item.Key, item => item.Value.DeepCopy() as Mesh);
            scene3.meshes = dictionary2;

            Scene scene4 = scene1;
            Dictionary<Guid, Material> materials = this.materials;
            var dictionary3 = materials.ToDictionary(item => item.Key, item => item.Value.DeepCopy() as Material);
            scene4.materials = dictionary3;

            Scene scene5 = scene1;
            Dictionary<Guid, Texture> textures = this.textures;
            var dictionary4 = textures.ToDictionary(item => item.Key, item => item.Value.DeepCopy() as Texture);
            scene5.textures = dictionary4;

            Scene scene6 = scene1;
            Dictionary<Guid, ExternalData> externaldatas = this.externaldatas;
            var dictionary5 = externaldatas.ToDictionary(item => item.Key, item => item.Value.DeepCopy() as ExternalData);
            scene6.externaldatas = dictionary5;
            scene1.materialList = this.materialList.ToList();

            Scene scene7 = scene1;
            Dictionary<string, string[]> proxyGroupSets = this.ProxyGroupSets;
            var dictionary6 = proxyGroupSets.ToDictionary(item => item.Key, item => item.Value.Clone() as string[]);
            scene7.ProxyGroupSets = dictionary6;

            Scene scene8 = scene1;
            Dictionary<string, string[]> occluderGroupSets = this.OccluderGroupSets;
            var dictionary7 = occluderGroupSets.ToDictionary(item => item.Key, item => item.Value.Clone() as string[]);
            scene8.OccluderGroupSets = dictionary7;

            Scene scene9 = scene1;
            Dictionary<NameAndId, List<string>> selectionGroupSets = this.SelectionGroupSets;
            var dictionary8 = selectionGroupSets.ToDictionary(item => item.Key, item => item.Value.ToList());
            scene9.SelectionGroupSets = dictionary8;

            Scene scene10 = scene1;
            Dictionary<NameAndId, List<Plane>> cuttingPlanes = this.CuttingPlanes;
            var dictionary9 = cuttingPlanes.ToDictionary(item => item.Key, item => item.Value.ToList());
            scene10.CuttingPlanes = dictionary9;

            if (this.RootNode != null && this.RootNode.Id != Guid.Empty && scene1.Nodes.ContainsKey(this.RootNode.Id))
                scene1.RootNode = scene1.Nodes[this.RootNode.Id];

            return scene1;
        }

        public Guid FindReplacementNodeInScene()
        {
            return this.FindReplacementNodeID(this.RootNode);
        }

        private Guid FindReplacementNodeID(Node rootNode)
        {
            if (rootNode.OngoingReplacement)
                return rootNode.Id;
            foreach (Guid child in rootNode.Children)
            {
                Guid replacementNodeId = this.FindReplacementNodeID(this.nodes[child]);
                if (replacementNodeId != Guid.Empty)
                    return replacementNodeId;
            }
            return Guid.Empty;
        }

        public bool ContainsNode(Guid rootNodeID, Guid idToFind)
        {
            var rootNode = (Node)null;
            if (!this.nodes.TryGetValue(rootNodeID, out rootNode))
                throw new Exception("Invalid node id in scene!");
            return this.ContainsNode(rootNode, idToFind);
        }

        private bool ContainsNode(Node rootNode, Guid idToFind)
        {
            if (rootNode.Id == idToFind)
                return true;
            foreach (Guid child in rootNode.Children)
            {
                if (this.ContainsNode(this.nodes[child], idToFind))
                    return true;
            }
            return false;
        }

        public void SetRootNode(Node node)
        {
            this.RootNode = node;
            this.MeshIDNodesIsDirty = true;
        }

        public void AddNode(Node node, Node parent)
        {
            if (!this.nodes.ContainsKey(node.Id))
                this.nodes.Add(node.Id, node);
            if (parent != null)
                parent.AddChild(node);
            else
                node.Parent = Guid.Empty;
            this.MeshIDNodesIsDirty = true;
        }

        public void RemoveNode(Guid nodeId)
        {
            Node node;
            if (!this.nodes.TryGetValue(nodeId, out node))
                return;
            if (node.Parent != Guid.Empty)
            {
                if (node.IsHLODReplacementNode || node.IsBone)
                    this.RelinkChildren(node.Id, node.Parent, node.Children.ToList());
                Node nodeById = this.GetNodeById(node.Parent);
                if (nodeById == null)
                    throw new Exception("Node is referencing a parent that does not exist in the scene");
                nodeById.RemoveChild(nodeId);
            }
            this.nodes.Remove(nodeId);
            this.MeshIDNodesIsDirty = true;
        }

        public void RemoveNodeTree(Guid nodeId)
        {
            Node nodeById1 = this.GetNodeById(nodeId);
            if (nodeById1 == null)
                return;
            foreach (Guid nodeId1 in nodeById1.Children.ToList())
                this.RemoveNodeTree(nodeId1);
            Node nodeById2 = this.GetNodeById(nodeById1.Parent);
            if (nodeById2 != null)
                nodeById2.RemoveChild(nodeId);
            this.nodes.Remove(nodeId);
            this.MeshIDNodesIsDirty = true;
        }

        public void AddMesh(Mesh mesh)
        {
            if (this.meshes.ContainsKey(mesh.Id))
                return;
            this.meshes.Add(mesh.Id, mesh);
            this.MeshIDNodesIsDirty = true;
        }

        public void RemoveMesh(Guid meshId)
        {
            Mesh mesh;
            if (this.meshes.TryGetValue(meshId, out mesh))
                this.meshes.Remove(meshId);
            this.MeshIDNodesIsDirty = true;
        }

        public void ClearMeshes()
        {
            this.meshes.Clear();
            this.MeshIDNodesIsDirty = true;
        }

        public void AddMaterial(Material material)
        {
            if (material.Name == "SimplygonDefaultMaterial" && material.Id != this.DefaultMaterial.Id || this.materials.ContainsKey(material.Id) && material.Id == this.DefaultMaterial.Id)
                return;
            this.materials.Add(material.Id, material);
            this.materialList.Add(material);
        }

        public void RemoveMaterial(Guid materialId)
        {
            Material material;
            if (this.materials.TryGetValue(materialId, out material))
                this.materials.Remove(materialId);
            this.materialList.RemoveAll(item => item.Id == materialId);
        }

        public void ClearMaterials()
        {
            this.materials.Clear();
            this.materialList.Clear();
        }

        public void AddTexture(Texture texture)
        {
            this.textures.Add(texture.Id, texture);
        }

        public void RemoveTexture(Guid textureId)
        {
            Texture texture;
            if (!this.textures.TryGetValue(textureId, out texture))
                return;
            this.textures.Remove(textureId);
        }

        public void ClearTextures()
        {
            this.textures.Clear();
        }

        public void AddExternalData(ExternalData externaldata)
        {
            this.externaldatas.Add(externaldata.Id, externaldata);
        }

        public void RemoveExternalData(Guid externaldataId)
        {
            ExternalData externalData;
            if (!this.externaldatas.TryGetValue(externaldataId, out externalData))
                return;
            this.externaldatas.Remove(externaldataId);
        }

        public void ClearExternalDatas()
        {
            this.externaldatas.Clear();
        }

        public List<Node> GetSelectedNodes()
        {
            return this.nodes.Where(nodeKVP => nodeKVP.Value.IsSelected).Select(nodeKVP => nodeKVP.Value).ToList();
        }

        public Dictionary<Guid, Node> GetSelectedNodeList()
        {
            var source = this.nodes.Where(node => node.Value.IsSelected).Select(node => new
            {
                node.Key,
                node.Value
            });
            return source.ToDictionary((p) => p.Key, k => k.Value);
        }

        public Dictionary<Guid, Mesh> GetSelectedMeshesAsDictionary()
        {
            var source = this.GetSelectedNodes().Where(node => node.MeshId != Guid.Empty).Select(node => new
            {
                Key = node.MeshId,
                Value = this.meshes[node.MeshId]
            });
            return source.ToDictionary((p) => p.Key, k => k.Value);
        }

        public List<Mesh> GetSelectedMeshes()
        {
            return this.GetSelectedNodes().Where(node => node.MeshId != Guid.Empty).Select(node => this.meshes[node.MeshId]).ToList();
        }

        public List<Guid> GetSelectedMaterialsIds()
        {
            return this.GetSelectedMeshes().Select(m => m.Materials).SelectMany(v => v).ToList();
        }

        private void GetMeshesFromNode(Guid nodeId, List<Mesh> storeMeshes)
        {
            Node node = this.nodes[nodeId];
            storeMeshes.Add(this.meshes[node.MeshId]);
            foreach (Guid child in node.Children)
                this.GetMeshesFromNode(child, storeMeshes);
        }

        public Node GetNodeById(Guid id)
        {
            var node = (Node)null;
            this.nodes.TryGetValue(id, out node);
            return node;
        }

        public Mesh GetMeshById(Guid id)
        {
            var mesh = (Mesh)null;
            this.meshes.TryGetValue(id, out mesh);
            return mesh;
        }

        public Material GetMaterialById(Guid id)
        {
            var material = (Material)null;
            this.materials.TryGetValue(id, out material);
            return material;
        }

        public Texture GetTextureById(Guid id)
        {
            var texture = (Texture)null;
            this.textures.TryGetValue(id, out texture);
            return texture;
        }

        public void SetSelectionOnNodeAndItsChildren(Guid nodeId, bool selectionFlag)
        {
            Node nodeById1 = this.GetNodeById(nodeId);
            if (nodeById1 == null)
                return;
            nodeById1.IsSelected = selectionFlag;
            foreach (Guid child in nodeById1.Children)
            {
                Node nodeById2 = this.GetNodeById(child);
                if (nodeById2 != null && nodeById2.Children.Count > 0)
                    this.SetSelectionOnNodeAndItsChildren(child, selectionFlag);
                else
                    nodeById2.IsSelected = selectionFlag;
            }
        }

        private void CreateMeshIDNodes()
        {
            this.MeshIDNodes.Clear();
            foreach (KeyValuePair<Guid, Node> node in this.Nodes)
            {
                if (Guid.Empty != node.Value.MeshId && !this.MeshIDNodes.ContainsKey(node.Value.MeshId))
                    this.MeshIDNodes.Add(node.Value.MeshId, node.Value);
            }
            this.MeshIDNodesIsDirty = false;
        }

        public Node GetParentNodeForMesh(Guid id)
        {
            if (this.MeshIDNodesIsDirty)
                this.CreateMeshIDNodes();
            Node node;
            this.MeshIDNodes.TryGetValue(id, out node);
            return node;
        }

        public Node FindTopMostParentMeshNode(List<Guid> ids)
        {
            return this.RootNode.FindTopMostMeshNode(ids);
        }

        public void RelinkChildren(Guid fromID, Guid toID, List<Guid> childrenIDs)
        {
            Node node = this.nodes[fromID];
            if (this.nodes[toID] == null)
                return;
            foreach (Guid childrenId in childrenIDs)
                this.RelinkNode(fromID, toID, childrenId);
        }

        public void SetupReplacementOnNode(Guid replacementId, Guid nodeId, bool applyToChildren = false)
        {
            Node node = this.nodes[nodeId];
            node.SetReplacementID(replacementId);
            if (!applyToChildren)
                return;
            foreach (Guid child in node.Children)
                this.SetupReplacementOnNode(replacementId, child, applyToChildren);
        }

        public void SetupReplacemetOnNodes(Guid replacementId, List<Guid> nodeIDs, bool applyToChildren = false)
        {
            foreach (Guid nodeId in nodeIDs)
                this.SetupReplacementOnNode(replacementId, nodeId, applyToChildren);
        }

        public void setTransform(Guid nodeID, Guid oldParentNodeID, Guid newParentNodeID)
        {
            Node node1 = this.nodes[nodeID];
            Node node2 = this.nodes[newParentNodeID];
            Node node3 = this.nodes[oldParentNodeID];
            node1.LocalTransform *= Matrix.Inverse(node2.EvaluateTransform(this));
        }

        public void RelinkNode(Guid fromID, Guid toID, Guid idToRelink)
        {
            Node node1 = this.nodes[fromID];
            Node node2 = this.nodes[toID];
            Node node3 = this.nodes[idToRelink];
            node1.RemoveChild(node3.Id);
            node3.Parent = node2.Id;
            if (node1.IsHLODReplacementNode)
                node3.SetReplacementID(fromID);
            node2.AddChild(node3);
        }

        public List<Guid> GetAllNodeIDs(bool includeRootNode = true, bool MeshNodesOnly = false)
        {
            var guidList = new List<Guid>();
            foreach (KeyValuePair<Guid, Node> node in this.Nodes)
            {
                if (MeshNodesOnly)
                {
                    if (node.Value.MeshId != Guid.Empty)
                        guidList.Add(node.Key);
                }
                else
                    guidList.Add(node.Key);
            }
            if (!includeRootNode)
                guidList.Remove(this.RootNode.Id);
            return guidList;
        }

        public bool HasASelectedMeshParent(Guid id, List<Guid> selectedNodes)
        {
            return this.HasASelectedMeshParent(this.nodes[id], selectedNodes);
        }

        public bool HasASelectedMeshParent(Node node, List<Guid> selectedNodes)
        {
            Node node1;
            if (!this.nodes.TryGetValue(node.Parent, out node1))
                return false;
            foreach (Guid selectedNode in selectedNodes)
            {
                if (selectedNode == node.Parent)
                    return true;
            }
            return this.HasASelectedMeshParent(node1, selectedNodes);
        }

        private bool FindParentById(Guid parentToFind, Guid parentToCompare)
        {
            if (parentToCompare == this.RootNode.Id)
                return false;
            if (parentToFind == parentToCompare)
                return true;
            return this.FindParentById(parentToFind, this.nodes[parentToCompare].Parent);
        }

        public List<Material> GetSelectedMaterials()
        {
            List<Mesh> selectedMeshes = this.GetSelectedMeshes();
            var materialList = new List<Material>();
            var dictionary = new Dictionary<Guid, bool>();
            foreach (KeyValuePair<Guid, Material> material in this.Materials)
                dictionary.Add(material.Key, false);
            foreach (Mesh mesh in selectedMeshes)
            {
                foreach (Guid material in mesh.Materials)
                    dictionary[material] = true;
            }
            foreach (KeyValuePair<Guid, bool> keyValuePair in dictionary)
            {
                if (keyValuePair.Value)
                    materialList.Add(this.materials[keyValuePair.Key]);
            }
            return materialList;
        }

        public List<Texture> GetSelectedTextures()
        {
            var textureList = new List<Texture>();
            List<Material> selectedMaterials = this.GetSelectedMaterials();
            var dictionary = new Dictionary<Guid, bool>();
            foreach (KeyValuePair<Guid, Texture> texture in this.Textures)
                dictionary.Add(texture.Key, false);
            foreach (Material material in selectedMaterials)
            {
                foreach (MaterialChannel materialChannel in material.MaterialChannels)
                {
                    foreach (MaterialChannelTextureDescriptor texture in materialChannel.Textures)
                        dictionary[texture.Texture] = true;
                }
            }
            foreach (KeyValuePair<Guid, bool> keyValuePair in dictionary)
            {
                if (keyValuePair.Value)
                    textureList.Add(this.textures[keyValuePair.Key]);
            }
            return textureList;
        }

        public Scene GetAllSceneObjects()
        {
            var scene = new Scene();
            scene.SceneDirectory = this.SceneDirectory;
            this.Nodes.ToList();
            var list1 = this.Meshes.ToList();
            var list2 = this.Materials.ToList();
            var list3 = this.Textures.ToList();
            Node node = this.CloneSceneTree(this.RootNode.Id, scene.nodes);
            scene.SetRootNode(node);
            foreach (KeyValuePair<Guid, Mesh> keyValuePair in list1)
                scene.AddMesh(keyValuePair.Value.DeepCopy() as Mesh);
            foreach (KeyValuePair<Guid, Material> keyValuePair in list2)
            {
                if (keyValuePair.Value.Id != new Guid("249C9B6E-9C40-4665-B333-8C30B919F9EA"))
                    scene.AddMaterial(keyValuePair.Value.DeepCopy() as Material);
            }
            foreach (KeyValuePair<Guid, Texture> keyValuePair in list3)
                scene.AddTexture(keyValuePair.Value.DeepCopy() as Texture);
            return scene;
        }

        private Node CloneSceneTree(Guid rootID, Dictionary<Guid, Node> nodeList)
        {
            Node node1 = this.nodes[rootID];
            var node2 = node1.DeepCopy() as Node;
            nodeList.Add(node2.Id, node2);
            foreach (Guid child in node1.Children)
                this.CloneSceneTree(child, nodeList);
            return node2;
        }

        public List<string> GetSelectedNodeNames()
        {
            Dictionary<Guid, Node> selectedNodeList = this.GetSelectedNodeList();
            var stringList = new List<string>();
            if (selectedNodeList.Count == 0)
            {
                foreach (KeyValuePair<Guid, Mesh> mesh in this.Meshes)
                    stringList.Add(mesh.Value.Name);
            }
            else
            {
                foreach (KeyValuePair<Guid, Node> keyValuePair in selectedNodeList)
                    stringList.Add(keyValuePair.Value.Name);
            }
            return stringList;
        }

        public List<string> GetSelectedMeshNames()
        {
            Dictionary<Guid, Mesh> meshesAsDictionary = this.GetSelectedMeshesAsDictionary();
            var stringList = new List<string>();
            if (meshesAsDictionary.Count == 0)
            {
                foreach (KeyValuePair<Guid, Mesh> mesh in this.Meshes)
                    stringList.Add(mesh.Value.Name);
            }
            else
            {
                foreach (KeyValuePair<Guid, Mesh> keyValuePair in meshesAsDictionary)
                    stringList.Add(keyValuePair.Value.Name);
            }
            return stringList;
        }

        public Scene GetSelectedSceneObjects(bool wholeSceneFallback = false)
        {
            Dictionary<Guid, Mesh> meshesAsDictionary = this.GetSelectedMeshesAsDictionary();
            Dictionary<Guid, Node> selectedNodeList = this.GetSelectedNodeList();
            var scene = new Scene();
            scene.SceneDirectory = this.SceneDirectory;
            if (selectedNodeList.Count > 0)
            {
                Node node1 = this.CloneSceneTree(this.RootNode.Id, scene.nodes);
                scene.SetRootNode(node1);
                foreach (KeyValuePair<Guid, Node> node2 in scene.Nodes)
                {
                    var mesh = (Mesh)null;
                    if (!meshesAsDictionary.TryGetValue(node2.Value.MeshId, out mesh))
                        node2.Value.SetMeshID(scene, Guid.Empty);
                }
            }
            if (meshesAsDictionary.Count != 0)
            {
                List<Material> selectedMaterials = this.GetSelectedMaterials();
                List<Texture> selectedTextures = this.GetSelectedTextures();
                foreach (KeyValuePair<Guid, Mesh> keyValuePair in meshesAsDictionary)
                    scene.AddMesh(keyValuePair.Value.DeepCopy() as Mesh);
                foreach (Material material in selectedMaterials)
                {
                    if (material.Id != new Guid("249C9B6E-9C40-4665-B333-8C30B919F9EA"))
                        scene.AddMaterial(material.DeepCopy() as Material);
                }
                foreach (Texture texture in selectedTextures)
                    scene.AddTexture(texture.DeepCopy() as Texture);
                return scene;
            }
            if (wholeSceneFallback)
                return this.GetAllSceneObjects();
            return null;
        }

        public void PrepareSceneForSyncing(List<Guid> syncMeshes)
        {
            foreach (Guid syncMesh in syncMeshes)
            {
                var mesh = (Mesh)null;
                if (this.meshes.TryGetValue(syncMesh, out mesh))
                {
                    while (mesh.MeshSourceCount > 1)
                        mesh.MeshSources.RemoveAt(1);
                }
            }
        }

        public void RepalceMeshInstance(Guid originalMeshId, Guid newMeshId)
        {
            foreach (Node node in this.FindNodesWithMeshID(originalMeshId))
                node.SetMeshID(this, newMeshId);
        }

        public IEnumerable<Node> FindNodesWithMeshID(Guid meshId)
        {
            return this.nodes.Where(nodeKVP => nodeKVP.Value.MeshId == meshId).Select(nodeKVP => nodeKVP.Value);
        }

        public IEnumerable<Node> FindNodesByName(string name)
        {
            return this.nodes.Where(nodeKVP => nodeKVP.Value.Name == name).Select(nodeKVP => nodeKVP.Value);
        }

        public Node FindNodeByNameFirstOrDefault(string name)
        {
            IEnumerable<Node> source = this.nodes.Where(nodeKVP => nodeKVP.Value.Name == name).Select(nodeKVP => nodeKVP.Value);
            if (source.Count() > 0)
                return source.ToList()[0];
            return null;
        }

        public IEnumerable<Material> FindMaterialsByName(string name)
        {
            return this.materials.Where(materialKVP => materialKVP.Value.Name == name).Select(materialKVP => materialKVP.Value);
        }

        public IEnumerable<Mesh> FindMeshesByName(string name)
        {
            return this.meshes.Where(meshKVP => meshKVP.Value.Name == name).Select(meshKVP => meshKVP.Value);
        }

        public IEnumerable<Texture> FindTexturesByName(string name)
        {
            return this.textures.Where(textureKVP => textureKVP.Value.Name == name).Select(textureKVP => textureKVP.Value);
        }

        public IEnumerable<Texture> FindTexturesById(Guid textureId)
        {
            return this.textures.Where(textureKVP => textureKVP.Value.Id == textureId).Select(textureKVP => textureKVP.Value);
        }

        public IEnumerable<Mesh> FindAllMeshesWithMaterial(Guid materialId)
        {
            return this.meshes.Where(meshKVP => meshKVP.Value.Materials.Contains(materialId)).Select(meshKVP => meshKVP.Value);
        }

        public IEnumerable<Node> FindAllMeshNodes()
        {
            return this.nodes.Where(nodeKVP => nodeKVP.Value.MeshId != Guid.Empty).Select(nodeKVP => nodeKVP.Value);
        }

        public IEnumerable<Node> FindAllBoneNodes()
        {
            return this.nodes.Where(nodeKVP => nodeKVP.Value.IsBone).Select(nodeKVP => nodeKVP.Value);
        }

        public IEnumerable<Node> FindAllNodesWithReplacementId(Guid nodeId)
        {
            return this.nodes.Where(nodeKVP => nodeKVP.Value.ReplacementId == nodeId).Select(nodeKVP => nodeKVP.Value);
        }

        public IEnumerable<Node> FindAllReplacementNodes()
        {
            return this.nodes.Where(nodeKVP => nodeKVP.Value.IsHLODReplacementNode).Select(nodeKVP => nodeKVP.Value);
        }

        public bool IsTextureUsed(Guid textureId)
        {
            foreach (KeyValuePair<Guid, Material> material in this.materials)
            {
                foreach (MaterialChannel materialChannel in material.Value.MaterialChannels)
                {
                    foreach (MaterialChannelTextureDescriptor texture in materialChannel.Textures)
                    {
                        if (texture.Texture.Equals(textureId))
                            return true;
                    }
                }
            }
            return false;
        }

        public IEnumerable<IGrouping<string, Guid>> FindAllTexturesGroupedByChannel(Guid materialId)
        {
            var material = (Material)null;
            if (!this.materials.TryGetValue(materialId, out material))
                return null;

            var materials = this.materials;
            var matChannelDatas = materials.SelectMany((mat) => mat.Value.MaterialChannels, (mat, matChannel) => new
            {
                mat,
                matChannel
            });

            var matChannelTexDescriptors = matChannelDatas.SelectMany((matChannelData) => matChannelData.matChannel.Textures, (matChannelData, textureDesciptor) => new
            {
                matChannelData,
                textureDesciptor
            });

            var keys = textures.Keys;
            var matChannelTexDescriptorIds = matChannelTexDescriptors.Join(keys, matChannelTexDescriptor => matChannelTexDescriptor.textureDesciptor.Texture, textrueId => textrueId, (matChannelTexDescriptor, textrueId) => new
            {
                matChannelTexDescriptor,
                textrueId
            });

            return matChannelTexDescriptorIds
                .GroupBy(
                    textureDescriptor => textureDescriptor.matChannelTexDescriptor.matChannelData.matChannel.ChannelName, 
                    textureDescriptor => textureDescriptor.matChannelTexDescriptor.textureDesciptor.Texture)
                .OrderBy(newGroup => newGroup.Key);
        }

        public List<Guid> FindAllNormalTextures()
        {
            var materials = this.materials;
            var sources = materials.SelectMany(mat => mat.Value.MaterialChannels, (mat, matChannel) => new
            {
                mat,
                matChannel
            });
            return sources
                .SelectMany(source => source.matChannel.Textures, (source, texDescriptor) => new
                {
                    source,
                    texDescriptor
                })
                .Where(sourceDescriptor => sourceDescriptor.source.matChannel.ChannelName == "Normals")
                .Select(sourceDescriptor => sourceDescriptor.texDescriptor.Texture)
                .ToList();
        }

        public bool SyncScene(Scene scene, int lodIndex = -1)
        {
            Scene scene1 = scene;
            var guidList1 = new List<Guid>();
            var guidList2 = new List<Guid>();
            var guidList3 = new List<Guid>();
            var guidList4 = new List<Guid>();
            var guidList5 = new List<Guid>();
            foreach (KeyValuePair<Guid, Mesh> mesh in scene1.Meshes)
            {
                if (!this.meshes.ContainsKey(mesh.Key))
                    guidList1.Add(mesh.Key);
                else
                    guidList5.Add(mesh.Key);
            }
            foreach (KeyValuePair<Guid, Texture> texture in scene1.Textures)
            {
                if (!this.textures.ContainsKey(texture.Key))
                    guidList2.Add(texture.Key);
            }
            foreach (KeyValuePair<Guid, Material> material in scene1.Materials)
            {
                if (!this.materials.ContainsKey(material.Key))
                    guidList3.Add(material.Key);
            }
            foreach (KeyValuePair<Guid, Node> node in scene1.Nodes)
            {
                if (!this.nodes.ContainsKey(node.Key))
                    guidList4.Add(node.Key);
            }
            foreach (Guid id in guidList3)
            {
                Material materialById = scene1.GetMaterialById(id);
                materialById.Name += id.ToString().ToUpperInvariant();
                this.AddMaterial(materialById);
            }
            foreach (Guid id in guidList2)
                this.AddTexture(scene1.GetTextureById(id));
            using (List<Guid>.Enumerator enumerator = guidList4.GetEnumerator())
            {
                if (enumerator.MoveNext())
                {
                    Guid current = enumerator.Current;
                    Node nodeById = scene1.GetNodeById(current);
                    nodeById.Name = string.Format(CultureInfo.InvariantCulture, "Simplygon_Proxy_LOD{0}", lodIndex);
                    this.AddNode(nodeById, this.RootNode);
                }
            }
            foreach (Guid id in guidList5)
            {
                Mesh meshById = scene1.GetMeshById(id);
                Mesh mesh = this.meshes[id];
                foreach (MeshData data in meshById.MeshSources.ToList())
                {
                    if (data.MaterialIndices != null)
                    {
                        for (int index = 0; index < data.MaterialIndices.Length; ++index)
                        {
                            if (data.MaterialIndices[index] == -1)
                                data.MaterialIndices[index] = 0;
                        }
                    }
                    foreach (Guid materialId in meshById.Materials.ToList())
                    {
                        if (!mesh.Materials.Contains(materialId))
                        {
                            mesh.AddMaterial(materialId);
                            int element = mesh.Materials.Count - 1;
                            data.MaterialIndices = Enumerable.Repeat(element, data.MaterialIndices.Length).ToArray();
                        }
                    }
                    mesh.AddMeshSourceData(data);
                }
            }
            foreach (Guid id in guidList1)
            {
                Mesh meshById = scene1.GetMeshById(id);
                Node parentNodeForMesh = scene1.GetParentNodeForMesh(meshById.Id);
                var node1 = (Node)null;
                if (this.nodes.TryGetValue(parentNodeForMesh.Id, out node1))
                {
                    if (node1.MeshId != parentNodeForMesh.MeshId)
                    {
                        node1.SetMeshID(this, parentNodeForMesh.MeshId);
                        this.AddMesh(meshById.DeepCopy() as Mesh);
                        node1.IsHLODReplacementNode = parentNodeForMesh.IsHLODReplacementNode;
                        node1.HLODOnScreenSize = parentNodeForMesh.HLODOnScreenSize;
                        if ((int)node1.HLODOnScreenSize == 0)
                            node1.HLODOnScreenSize = 5000U;
                        var node2 = (Node)null;
                        if (this.nodes.TryGetValue(node1.Parent, out node2) && node2.Id != this.RootNode.Id)
                        {
                            foreach (Guid child in node1.Children)
                                this.nodes[child].LocalTransform *= node1.EvaluateTransform(this);
                            var matrix = Matrix.Inverse(node2.EvaluateTransform(this));
                            node1.LocalTransform *= matrix;
                        }
                        else
                        {
                            var matrix = Matrix.Inverse(node1.EvaluateTransform(this));
                            node1.LocalTransform = matrix;
                        }
                    }
                    else if (!node1.IsHLODReplacementNode)
                        this.AddMesh(meshById.DeepCopy() as Mesh);
                }
                else
                    this.AddMesh(meshById.DeepCopy() as Mesh);
            }
            return true;
        }
    }
}