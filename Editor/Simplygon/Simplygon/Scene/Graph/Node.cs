﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.Graph.Node
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Math;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace Simplygon.Scene.Graph
{
  [Serializable]
  internal class Node : SceneObject, INotifyPropertyChanged
  {
    private List<Guid> childrenList = new List<Guid>();
    private bool _isDirty;
    private Guid parent;
    public bool changedSelection;
    private bool _isSelected;

    public Guid Parent
    {
      get
      {
        return this.parent;
      }
      set
      {
        this.parent = value;
      }
    }

    public Guid ReplacementId { get; private set; }

    public void SetReplacementID(Guid replacementId)
    {
      this.ReplacementId = replacementId;
    }

    public Guid MeshId { get; private set; }

    public void SetMeshID(Simplygon.Scene.Scene scene, Guid meshId)
    {
      scene.SetMeshIDNodesIsDirty();
      this.MeshId = meshId;
    }

    public bool IsBone { get; set; }

    public bool IsHLODReplacementNode { get; set; }

    public uint HLODOnScreenSize { get; set; }

    public bool HasReplacementNode
    {
      get
      {
        return this.ReplacementId != Guid.Empty;
      }
    }

    public bool OngoingReplacement { get; set; }

    public bool ForceRenderNode { get; set; }

    public bool IsFrozen { get; set; }

    public bool IsSelected
    {
      get
      {
        return this._isSelected;
      }
      set
      {
        if (value == this._isSelected)
          return;
        this._isSelected = value;
        this.NotifyPropertyChanged(nameof (IsSelected));
        this.changedSelection = true;
      }
    }

    public bool IsDirty
    {
      get
      {
        return this._isDirty;
      }
      set
      {
        if (value == this._isDirty)
          return;
        this._isDirty = value;
        this.NotifyPropertyChanged(nameof (IsDirty));
      }
    }

    public ReadOnlyCollection<Guid> Children
    {
      get
      {
        return this.childrenList.AsReadOnly();
      }
    }

    public Matrix LocalTransform { get; set; }

    public Matrix GlobalTransform { get; private set; }

    public event PropertyChangedEventHandler PropertyChanged;

    private void NotifyPropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      if (this.PropertyChanged == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.PropertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }

    public List<int> GlobalLODIndices { get; set; }

    public Node(Guid id, string name)
      : base(id, name)
    {
      this.MeshId = Guid.Empty;
      this.IsBone = false;
      this.IsSelected = false;
      this.LocalTransform = Matrix.Identity;
      this.changedSelection = true;
      this._isDirty = false;
      this.Parent = Guid.Empty;
      this.OngoingReplacement = false;
      this.IsHLODReplacementNode = false;
      this.HLODOnScreenSize = 0U;
      this.IsFrozen = false;
      this.ReplacementId = Guid.Empty;
      this.GlobalTransform = Matrix.Zero;
      this.GlobalLODIndices = new List<int>();
    }

    public new SceneObject DeepCopy()
    {
      Node node = this.MemberwiseClone() as Node;
      node.childrenList = this.childrenList.ToList<Guid>();
      node.GlobalLODIndices = this.GlobalLODIndices.ToList<int>();
      return (SceneObject) node;
    }

    protected override void Initialize()
    {
      base.Initialize();
    }

    public void AddChild(Node node)
    {
      if (!this.childrenList.Contains(node.Id))
        this.childrenList.Add(node.Id);
      node.Parent = this.Id;
    }

    internal void RemoveChild(Guid nodeId)
    {
      this.childrenList.Remove(nodeId);
    }

    public void RemoveChildren()
    {
      this.childrenList.Clear();
    }

    public Matrix EvaluateTransform(Simplygon.Scene.Scene scene)
    {
      Matrix matrix = this.LocalTransform;
      if (this.GlobalTransform == Matrix.Zero)
      {
        if (this.parent != Guid.Empty)
        {
          Node nodeById = scene.GetNodeById(this.parent);
          this.GlobalTransform = nodeById == null ? this.LocalTransform : this.LocalTransform * nodeById.EvaluateTransform(scene);
          matrix = this.GlobalTransform;
        }
      }
      else
        matrix = this.GlobalTransform;
      return matrix;
    }

    internal Node IsMeshNode(Guid id)
    {
      if (this.MeshId == id)
        return this;
      return (Node) null;
    }

    public Node FindTopMostMeshNode(List<Guid> ids)
    {
      foreach (Guid id in ids)
      {
        if (this.MeshId == id)
          return this;
      }
      if (this.childrenList.Count > 0)
      {
        List<Node> nodeList = new List<Node>();
        foreach (Guid children in this.childrenList)
        {
          int index = this.childrenList.IndexOf(children);
          nodeList.Add(this.FindTopmostMeshNode(this.childrenList[index]));
        }
        if (nodeList.Count > 1)
          return this;
        if (nodeList.Count > 0)
          return nodeList[0];
      }
      return (Node) null;
    }

    public Node FindTopmostMeshNode(Guid id)
    {
      if (this.MeshId == id)
        return this;
      if (this.childrenList.Count > 0)
      {
        List<Node> nodeList = new List<Node>();
        foreach (Guid children in this.childrenList)
          nodeList.Add(this.FindTopmostMeshNode(id));
        if (nodeList.Count > 1)
          return this;
        if (nodeList.Count > 0)
          return nodeList[0];
      }
      return (Node) null;
    }
  }
}
