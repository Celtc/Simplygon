﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.FileFormat.SSF.WriterV10
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v10.DataType;
using Simplygon.DataFormat.v10.Scene;
using Simplygon.Scene.Common.Interfaces;
using Simplygon.Scene.Common.Progress;
using Simplygon.Scene.Graph;
using System;
using System.Collections.Generic;

namespace Simplygon.Scene.FileFormat.SSF
{
  internal class WriterV10 : IWriter
  {
    private Dictionary<Guid, int> meshTableMapping = new Dictionary<Guid, int>();
    private Dictionary<Guid, int> textureTableMapping = new Dictionary<Guid, int>();
    private Dictionary<Guid, int> materialTableMapping = new Dictionary<Guid, int>();

    internal WriterV10()
    {
    }

    public void Write(Simplygon.Scene.Scene scene, string filename, bool overWriteTexture, bool embeddedMedia, SceneProgress progress, IWorkDirectoryInfo workDirectory)
    {
      throw new NotImplementedException();
    }

    public void Write(Simplygon.Scene.Scene scene, string filename, string toolName, int toolMajorVersion, int toolMinorVersion, int toolBuildVersion, byte[] formatSignature)
    {
      SSFFile ssfFile = new SSFFile();
      ssfFile.FileHeader.FileVersion.MajorVersion = (byte) 1;
      ssfFile.FileHeader.FileVersion.MinorVersion = (byte) 0;
      MeshTable meshTable = new MeshTable();
      meshTable.Name = "MeshTable";
      MaterialTable materialTable = new MaterialTable();
      materialTable.Name = "MaterialTable";
      TextureTable textureTable = new TextureTable();
      textureTable.Name = "TextureTable";
      this.AddTextures(scene, textureTable);
      this.AddMaterials(scene, materialTable);
      this.AddMeshes(scene, meshTable);
      this.AddSceneGraph(scene, ssfFile);
      ssfFile.TextureTables.Add(textureTable);
      ssfFile.MaterialTables.Add(materialTable);
      ssfFile.MeshTables.Add(meshTable);
      ssfFile.Write(filename);
    }

    private void AddSceneGraph(Simplygon.Scene.Scene scene, SSFFile ssfFile)
    {
      Node rootNode = scene.RootNode;
      if (rootNode == null)
        return;
      SceneRoot sceneRoot = new SceneRoot();
      sceneRoot.Name = "SceneRoot";
      SceneNode sceneNode = this.AddSceneGraph(rootNode);
      sceneRoot.SceneNodes.Add(sceneNode);
      ssfFile.SceneRoots.Add(sceneRoot);
    }

    private SceneNode AddSceneGraph(Node sceneNode)
    {
      SceneNode sceneNode1 = new SceneNode();
      sceneNode1.Name = sceneNode.Name;
      sceneNode1.RootBone = 0U;
      sceneNode1.Type = 0UL;
      sceneNode1.Transform = new Matrix4x4((float) sceneNode.LocalTransform.M11, (float) sceneNode.LocalTransform.M12, (float) sceneNode.LocalTransform.M13, (float) sceneNode.LocalTransform.M14, (float) sceneNode.LocalTransform.M21, (float) sceneNode.LocalTransform.M22, (float) sceneNode.LocalTransform.M23, (float) sceneNode.LocalTransform.M24, (float) sceneNode.LocalTransform.M31, (float) sceneNode.LocalTransform.M32, (float) sceneNode.LocalTransform.M33, (float) sceneNode.LocalTransform.M34, (float) sceneNode.LocalTransform.M41, (float) sceneNode.LocalTransform.M42, (float) sceneNode.LocalTransform.M43, (float) sceneNode.LocalTransform.M44);
      if (sceneNode.MeshId != Guid.Empty)
      {
        sceneNode1.MeshTable = "MeshTable";
        sceneNode1.MeshIndex = (uint) this.meshTableMapping[sceneNode.MeshId];
        sceneNode1.Type = 1UL;
      }
      if (sceneNode.IsBone)
      {
        sceneNode1.BoneIndex = 0U;
        sceneNode1.Type = 2UL;
      }
      int num = 0;
      while (num < sceneNode.Children.Count)
        ++num;
      return sceneNode1;
    }

    private void AddMeshes(Simplygon.Scene.Scene scene, MeshTable meshTable)
    {
      int num = 0;
      foreach (KeyValuePair<Guid, Simplygon.Scene.Asset.Mesh> mesh1 in scene.Meshes)
      {
        Simplygon.DataFormat.v10.Scene.Mesh mesh2 = new Simplygon.DataFormat.v10.Scene.Mesh();
        mesh2.Name = mesh1.Value.Name;
        for (int index = 0; index < mesh1.Value[0].Coordinates.Length; ++index)
        {
          Simplygon.Math.Vector4 coordinate = mesh1.Value[0].Coordinates[index];
          mesh2.Coordinates.Add(new Simplygon.DataFormat.v10.DataType.Vector3(coordinate.X, coordinate.Y, coordinate.Z));
        }
        int index1 = 0;
        while (index1 < mesh1.Value[0].Triangles.Length)
        {
          int triangle1 = mesh1.Value[0].Triangles[index1];
          int triangle2 = mesh1.Value[0].Triangles[index1 + 1];
          int triangle3 = mesh1.Value[0].Triangles[index1 + 2];
          mesh2.Triangles.Add(new Index3(triangle1, triangle2, triangle3));
          index1 += 3;
        }
        if (mesh1.Value[0].Normals != null)
        {
          for (int index2 = 0; index2 < mesh1.Value[0].Normals.Length; ++index2)
          {
            Simplygon.Math.Vector3 normal = mesh1.Value[0].Normals[index2];
            mesh2.Normals.Add(new Simplygon.DataFormat.v10.DataType.Vector3(normal.X, normal.Y, normal.Z));
          }
        }
        if (mesh1.Value[0].Tangents != null)
        {
          for (int index2 = 0; index2 < mesh1.Value[0].Tangents.Length; ++index2)
          {
            Simplygon.Math.Vector3 tangent = mesh1.Value[0].Tangents[index2];
            mesh2.Tangents.Add(new Simplygon.DataFormat.v10.DataType.Vector4(tangent.X, tangent.Y, tangent.Z, 1.0));
          }
        }
        if (mesh1.Value[0].Bitangents != null)
        {
          for (int index2 = 0; index2 < mesh1.Value[0].Bitangents.Length; ++index2)
          {
            Simplygon.Math.Vector3 tangent = mesh1.Value[0].Tangents[index2];
            mesh2.Bitangents.Add(new Simplygon.DataFormat.v10.DataType.Vector3(tangent.X, tangent.Y, tangent.Z));
          }
        }
        Dictionary<string, Simplygon.Math.Vector2[]> texCoords = mesh1.Value[0].TexCoords;
        int[,] boneIndices = mesh1.Value[0].BoneIndices;
        if (mesh1.Value[0].MaterialIndices != null)
        {
          for (int index2 = 0; index2 < mesh1.Value[0].Coordinates.Length; ++index2)
          {
            int materialIndex = mesh1.Value[0].MaterialIndices[index2];
            if (materialIndex >= 0)
            {
              Guid material = mesh1.Value.Materials[materialIndex];
              mesh2.MaterialIndexPerPolygon.Add((uint) this.materialTableMapping[material]);
            }
          }
        }
        meshTable.Meshes.Add(mesh2);
        this.meshTableMapping.Add(mesh1.Key, num);
        ++num;
      }
    }

    private void AddTextures(Simplygon.Scene.Scene scene, TextureTable textureTable)
    {
      foreach (KeyValuePair<Guid, Simplygon.Scene.Asset.Texture> texture in scene.Textures)
      {
        textureTable.Textures.Add(new Simplygon.DataFormat.v10.Scene.Texture()
        {
          Name = texture.Value.Name,
          Path = texture.Value.Path,
          Type = TextureType.Diffuse
        });
        this.textureTableMapping.Add(texture.Key, textureTable.Textures.Count - 1);
      }
    }

    private void AddMaterials(Simplygon.Scene.Scene scene, MaterialTable materialTable)
    {
      foreach (KeyValuePair<Guid, Simplygon.Scene.Asset.Material> material in scene.Materials)
      {
        materialTable.Materials.Add(new Simplygon.DataFormat.v10.Scene.Material()
        {
          Name = material.Value.Name
        });
        this.materialTableMapping.Add(material.Key, materialTable.Materials.Count - 1);
      }
    }
  }
}
