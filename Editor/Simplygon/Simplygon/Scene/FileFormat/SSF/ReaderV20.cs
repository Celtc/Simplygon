﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.FileFormat.SSF.ReaderV20
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.DataType;
using Simplygon.DataFormat.v20.Scene;
using Simplygon.Hash;
using Simplygon.Math;
using Simplygon.Scene.Asset;
using Simplygon.Scene.Common;
using Simplygon.Scene.Common.Enums;
using Simplygon.Scene.Common.Progress;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Simplygon.Scene.FileFormat.SSF
{
    internal class ReaderV20 : IReader
    {
        private Dictionary<string, Dictionary<uint, Guid>> meshTableMapping = new Dictionary<string, Dictionary<uint, Guid>>();
        private System.Collections.Generic.List<Asset.Texture> textureMapping = new System.Collections.Generic.List<Asset.Texture>();
        private Dictionary<int, Guid> materialMapping = new Dictionary<int, Guid>();
        private HashSet<string> MaterialLODTextures = new HashSet<string>();
        private HashSet<string> NormalMapTextures = new HashSet<string>();
        private SceneProgress sceneProgress;

        public event MissingTextureEventHandler RaiseMissingTexture;

        public string RootDirectory
        {
            get; set;
        }

        private bool OnlyComputeHash
        {
            get; set;
        }

        private string Hash
        {
            get; set;
        }

        private ColorSpaceType TextureColorSpace
        {
            get; set;
        }

        public MaterialModel MaterialModel
        {
            get; private set;
        }

        public NormalMapType NormalMapType
        {
            get; private set;
        }

        internal ReaderV20()
        {
        }

        public string ComputeHash(string filename)
        {
            try
            {
                this.OnlyComputeHash = true;
                this.Hash = SHA256.ComputeString(Path.GetFileName(filename) + SHA256.ComputeFile(filename));
                this.Read(filename, ColorSpaceType.NotDefined, MaterialModel.NotDefined, NormalMapType.NotDefined, (SceneProgress)null);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                this.OnlyComputeHash = false;
            }
            return this.Hash;
        }

        public Simplygon.Scene.Scene Read(string filename, ColorSpaceType textureColorSpace, MaterialModel materialModel, NormalMapType normalMapType, SceneProgress progress)
        {
            this.RootDirectory = Path.GetDirectoryName(filename);
            this.TextureColorSpace = textureColorSpace;
            this.MaterialModel = materialModel;
            this.NormalMapType = normalMapType;
            if (progress == null)
                progress = new SceneProgress();
            this.sceneProgress = progress;
            SSFFile ssfFile = new SSFFile();
            ssfFile.Read(filename);
            Simplygon.Scene.Scene scene = new Simplygon.Scene.Scene();
            scene.WorldOrientation = ssfFile.WorldOrientation;
            scene.OriginalCoordinateSystem = ssfFile.CoordinateSystem;
            scene.SceneDirectory = this.RootDirectory;
            scene.Name = Path.GetFileNameWithoutExtension(filename);
            string[] proxyGroupSets = ssfFile.ProxyGroupSets;
            if (proxyGroupSets != null && proxyGroupSets.Length != 0)
            {
                for (int index = 0; index < proxyGroupSets.Length; ++index)
                    scene.ProxyGroupSets.Add(proxyGroupSets[index], ssfFile.GetProxyGroupSet(proxyGroupSets[index]));
            }
            string[] occluderGroupSets = ssfFile.OccluderGroupSets;
            if (occluderGroupSets != null && occluderGroupSets.Length != 0)
            {
                for (int index = 0; index < occluderGroupSets.Length; ++index)
                    scene.OccluderGroupSets.Add(occluderGroupSets[index], ssfFile.GetOccluderGroupSet(occluderGroupSets[index]));
            }
            NameAndId[] selectionGroupSets = ssfFile.SelectionGroupSets;
            if (selectionGroupSets != null && selectionGroupSets.Length != 0)
            {
                for (int index = 0; index < selectionGroupSets.Length; ++index)
                    scene.SelectionGroupSets.Add(selectionGroupSets[index], ((IEnumerable<string>)ssfFile.GetSelectionGroupSetById(selectionGroupSets[index].Id)).ToList<string>());
            }
            NameAndId[] cuttingPlanes = ssfFile.CuttingPlanes;
            if (cuttingPlanes != null && cuttingPlanes.Length != 0)
            {
                for (int index = 0; index < cuttingPlanes.Length; ++index)
                    scene.CuttingPlanes.Add(cuttingPlanes[index], ((IEnumerable<Simplygon.DataFormat.v20.DataType.Plane>)ssfFile.GetCuttingPlanesById(cuttingPlanes[index].Id)).ToList<Simplygon.DataFormat.v20.DataType.Plane>().Select<Simplygon.DataFormat.v20.DataType.Plane, Simplygon.Math.Plane>((Func<Simplygon.DataFormat.v20.DataType.Plane, Simplygon.Math.Plane>)(p => new Simplygon.Math.Plane(new Simplygon.Math.Vector3(p.Point.X, p.Point.Y, p.Point.Z), new Simplygon.Math.Vector3(p.Normal.X, p.Normal.Y, p.Normal.Z)))).ToList<Simplygon.Math.Plane>());
            }
            scene.SceneDirectory = Path.GetDirectoryName(filename);
            if (!this.OnlyComputeHash)
            {
                this.AddMaterials(ssfFile, scene);
                this.AddMeshes(ssfFile, scene);
                this.AddSceneGraph(ssfFile, scene);
                this.SetupBones(scene);
            }
            this.AddTextures(ssfFile, scene);
            scene.Validate(false);
            return scene;
        }

        public void CreateTestScene(string inFilename, string outFilename, int sizeX, int sizeY, int sizeZ, float offset)
        {
        }

        private void AddSceneGraph(SSFFile ssfFile, Simplygon.Scene.Scene scene)
        {
            int num = 0;
            foreach (Simplygon.DataFormat.v20.Scene.Node node1 in ssfFile.NodeTable.Nodes)
            {
                Simplygon.Scene.Graph.Node node2 = new Simplygon.Scene.Graph.Node(new Guid(node1.Id), node1.Name);
                node2.IsHLODReplacementNode = node1.IsHLODReplacementNode;
                node2.HLODOnScreenSize = node1.HLODOnScreenSize;
                node2.OngoingReplacement = node1.IsCurrentReplacement;
                node2.IsFrozen = node1.IsFrozen;
                int[] globalLodIndices = node1.GlobalLODIndices;
                if (globalLodIndices != null)
                    node2.GlobalLODIndices.AddRange((IEnumerable<int>)globalLodIndices);
                if (!string.IsNullOrEmpty(node1.MeshId))
                    node2.SetMeshID(scene, new Guid(node1.MeshId));
                if (!string.IsNullOrEmpty(node1.ReplacementId))
                    node2.SetReplacementID(new Guid(node1.ReplacementId));
                Matrix4x4 localTransform = node1.LocalTransform;
                node2.LocalTransform = new Matrix(localTransform._11, localTransform._12, localTransform._13, localTransform._14, localTransform._21, localTransform._22, localTransform._23, localTransform._24, localTransform._31, localTransform._32, localTransform._33, localTransform._34, localTransform._41, localTransform._42, localTransform._43, localTransform._44);
                Guid index = new Guid(node1.ParentId);
                if (index == Guid.Empty)
                {
                    scene.AddNode(node2, (Simplygon.Scene.Graph.Node)null);
                    scene.SetRootNode(node2);
                }
                else
                {
                    Simplygon.Scene.Graph.Node node3 = scene.Nodes[index];
                    scene.AddNode(node2, node3);
                }
                this.sceneProgress.ReportProgress(80 + (int)((double)(num / ssfFile.NodeTable.Nodes.Count) * 0.2 * 100.0));
            }
        }

        private void AddMeshes(SSFFile ssfFile, Simplygon.Scene.Scene scene)
        {
            int num1 = 0;
            foreach (Simplygon.DataFormat.v20.Scene.Mesh mesh1 in ssfFile.MeshTable.Meshes)
            {
                Simplygon.Scene.Asset.Mesh mesh2 = new Simplygon.Scene.Asset.Mesh(new Guid(mesh1.Id), mesh1.Name);
                mesh2.MeshSources.Clear();
                if (mesh1.MaterialIds != null)
                {
                    foreach (string materialId in mesh1.MaterialIds)
                    {
                        Guid guid = new Guid(materialId);
                        mesh2.Materials.Add(guid);
                    }
                }
                if (mesh1.BoneIds != null)
                {
                    foreach (string boneId in mesh1.BoneIds)
                    {
                        Guid guid = new Guid(boneId);
                        mesh2.Bones.Add(guid);
                    }
                }
                foreach (Simplygon.DataFormat.v20.Scene.MeshData meshData in mesh1.MeshDatas)
                {
                    Simplygon.Scene.Asset.MeshData data = new Simplygon.Scene.Asset.MeshData(mesh2);
                    if (meshData.MaxDeviation != -1.0)
                        data.MaxDeviation = meshData.MaxDeviation;
                    if (meshData.PixelSize != -1)
                        data.PixelSize = meshData.PixelSize;
                    data.GlobalLODIndex = meshData.GlobalLODIndex;
                    Simplygon.DataFormat.v20.DataType.Vector3[] coordinates = meshData.Coordinates;
                    if (coordinates != null && coordinates.Length != 0)
                    {
                        Simplygon.Math.Vector4[] vector4Array = new Simplygon.Math.Vector4[coordinates.Length];
                        int index1 = 0;
                        for (int index2 = 0; index2 < coordinates.Length; ++index2)
                        {
                            Simplygon.DataFormat.v20.DataType.Vector3 vector3 = coordinates[index2];
                            vector4Array[index1] = new Simplygon.Math.Vector4(vector3.X, vector3.Y, vector3.Z, 1.0);
                            ++index1;
                        }
                        data.Coordinates = vector4Array;
                    }
                    Index3[] triangleIndices = meshData.TriangleIndices;
                    if (triangleIndices != null && triangleIndices.Length != 0)
                    {
                        int[] numArray = new int[triangleIndices.Length * 3];
                        int index1 = 0;
                        for (int index2 = 0; index2 < triangleIndices.Length; ++index2)
                        {
                            Index3 index3 = triangleIndices[index2];
                            if (index3.X < 0 || index3.Y < 0 || index3.Z < 0)
                            {
                                index3.X = 0;
                                index3.Y = 0;
                                index3.Z = 0;
                            }
                            numArray[index1] = index3.X;
                            int index4 = index1 + 1;
                            numArray[index4] = index3.Y;
                            int index5 = index4 + 1;
                            numArray[index5] = index3.Z;
                            index1 = index5 + 1;
                        }
                        data.Triangles = numArray;
                    }
                    uint[] materialIndices = meshData.MaterialIndices;
                    if (materialIndices != null && materialIndices.Length != 0)
                    {
                        int[] numArray = new int[materialIndices.Length];
                        for (int index = 0; index < meshData.TriangleCount; ++index)
                            numArray[index] = (int)materialIndices[index];
                        data.MaterialIndices = numArray;
                    }
                    Simplygon.DataFormat.v20.DataType.Vector3[] normals = meshData.Normals;
                    if (normals != null && normals.Length != 0)
                    {
                        Simplygon.Math.Vector3[] vector3Array = new Simplygon.Math.Vector3[normals.Length];
                        int index1 = 0;
                        for (int index2 = 0; index2 < normals.Length; ++index2)
                        {
                            Simplygon.DataFormat.v20.DataType.Vector3 vector3 = normals[index2];
                            vector3Array[index1] = new Simplygon.Math.Vector3(vector3.X, vector3.Y, vector3.Z);
                            ++index1;
                        }
                        data.Normals = vector3Array;
                    }
                    Simplygon.DataFormat.v20.DataType.Vector3[] tangents = meshData.Tangents;
                    if (tangents != null && tangents.Length != 0)
                    {
                        Simplygon.Math.Vector3[] vector3Array = new Simplygon.Math.Vector3[tangents.Length];
                        int index1 = 0;
                        for (int index2 = 0; index2 < tangents.Length; ++index2)
                        {
                            Simplygon.DataFormat.v20.DataType.Vector3 vector3 = tangents[index2];
                            vector3Array[index1] = new Simplygon.Math.Vector3(vector3.X, vector3.Y, vector3.Z);
                            ++index1;
                        }
                        data.Tangents = vector3Array;
                    }
                    Simplygon.DataFormat.v20.DataType.Vector3[] bitangents = meshData.Bitangents;
                    if (bitangents != null && bitangents.Length != 0)
                    {
                        Simplygon.Math.Vector3[] vector3Array = new Simplygon.Math.Vector3[bitangents.Length];
                        int index1 = 0;
                        for (int index2 = 0; index2 < bitangents.Length; ++index2)
                        {
                            Simplygon.DataFormat.v20.DataType.Vector3 vector3 = bitangents[index2];
                            vector3Array[index1] = new Simplygon.Math.Vector3(vector3.X, vector3.Y, vector3.Z);
                            ++index1;
                        }
                        data.Bitangents = vector3Array;
                    }
                    bool[] vertexLocks = meshData.VertexLocks;
                    if (vertexLocks != null && vertexLocks.Length != 0)
                        data.VertexLocks = vertexLocks;
                    double[] vertexCrease = meshData.VertexCrease;
                    if (vertexCrease != null && vertexCrease.Length != 0)
                        data.VertexCrease = vertexCrease;
                    double[] vertexWeights = meshData.VertexWeights;
                    if (vertexWeights != null && vertexWeights.Length != 0)
                        data.VertexWeights = vertexWeights;
                    int[] originalVertexIndices = meshData.OriginalVertexIndices;
                    if (originalVertexIndices != null && originalVertexIndices.Length != 0)
                        data.OriginalVertexIndices = originalVertexIndices;
                    double[] edgeCrease = meshData.EdgeCrease;
                    if (edgeCrease != null && edgeCrease.Length != 0)
                        data.EdgeCrease = edgeCrease;
                    double[] edgeWeights = meshData.EdgeWeights;
                    if (edgeWeights != null && edgeWeights.Length != 0)
                        data.EdgeWeights = edgeWeights;
                    int[] smoothingGroup = meshData.SmoothingGroup;
                    if (smoothingGroup != null && smoothingGroup.Length != 0)
                        data.SmoothingGroup = smoothingGroup;
                    int[] originalTriangleIndices = meshData.OriginalTriangleIndices;
                    if (originalTriangleIndices != null && originalTriangleIndices.Length != 0)
                        data.OriginalTriangleIndices = originalTriangleIndices;
                    string[] textureCoordinateSets = meshData.TextureCoordinateSets;
                    if (textureCoordinateSets != null && textureCoordinateSets.Length != 0)
                    {
                        for (int index1 = 0; index1 < textureCoordinateSets.Length; ++index1)
                        {
                            Simplygon.DataFormat.v20.DataType.Vector2[] textureCoordinates = meshData.GetTextureCoordinates(textureCoordinateSets[index1]);
                            Simplygon.Math.Vector2[] vector2Array = new Simplygon.Math.Vector2[textureCoordinates.Length];
                            for (int index2 = 0; index2 < textureCoordinates.Length; ++index2)
                            {
                                Simplygon.DataFormat.v20.DataType.Vector2 vector2 = textureCoordinates[index2];
                                vector2Array[index2] = new Simplygon.Math.Vector2(vector2.X, vector2.Y);
                            }
                            data.TexCoords.Add(textureCoordinateSets[index1], vector2Array);
                        }
                    }
                    string[] colorSets = meshData.ColorSets;
                    if (colorSets != null && colorSets.Length != 0)
                    {
                        for (int index1 = 0; index1 < colorSets.Length; ++index1)
                        {
                            Simplygon.DataFormat.v20.DataType.Vector4[] colors = meshData.GetColors(colorSets[index1]);
                            Color[] colorArray = new Color[colors.Length];
                            for (int index2 = 0; index2 < colors.Length; ++index2)
                            {
                                Simplygon.DataFormat.v20.DataType.Vector4 vector4 = colors[index2];
                                colorArray[index2] = new Color((float)vector4.X, (float)vector4.Y, (float)vector4.Z, (float)vector4.W, 32);
                            }
                            data.VertexColors.Add(colorSets[index1], colorArray);
                        }
                    }
                    string[] blendShapeSet = meshData.BlendShapeSet;
                    if (blendShapeSet != null && blendShapeSet.Length != 0)
                    {
                        for (int index1 = 0; index1 < blendShapeSet.Length; ++index1)
                        {
                            Simplygon.DataFormat.v20.DataType.Vector3[] blendShapes = meshData.GetBlendShapes(blendShapeSet[index1]);
                            Simplygon.Math.Vector3[] vector3Array = new Simplygon.Math.Vector3[blendShapes.Length];
                            for (int index2 = 0; index2 < blendShapes.Length; ++index2)
                            {
                                Simplygon.DataFormat.v20.DataType.Vector3 vector3 = blendShapes[index2];
                                vector3Array[index2] = new Simplygon.Math.Vector3(vector3.X, vector3.Y, vector3.Z);
                            }
                            data.BlendShapes.Add(blendShapeSet[index1], vector3Array);
                        }
                    }
                    int[] boneIndices = meshData.BoneIndices;
                    double[] boneWeights = meshData.BoneWeights;
                    if (boneIndices != null && boneWeights != null && (boneIndices.Length != 0 && boneIndices.Length == boneWeights.Length))
                    {
                        int bonesPerVertex = meshData.BonesPerVertex;
                        int vertexCount = meshData.VertexCount;
                        int[,] numArray1 = new int[vertexCount, bonesPerVertex];
                        float[,] numArray2 = new float[vertexCount, bonesPerVertex];
                        for (int index1 = 0; index1 < vertexCount; ++index1)
                        {
                            for (int index2 = 0; index2 < bonesPerVertex; ++index2)
                            {
                                int num2 = boneIndices[index1 * bonesPerVertex + index2];
                                double num3 = boneWeights[index1 * bonesPerVertex + index2];
                                numArray1[index1, index2] = num2;
                                numArray2[index1, index2] = (float)num3;
                            }
                        }
                        data.BoneIndices = numArray1;
                        data.BoneWeights = numArray2;
                    }
                    string[] vertexSets = meshData.VertexSets;
                    if (vertexSets != null && vertexSets.Length != 0)
                    {
                        for (int index1 = 0; index1 < vertexSets.Length; ++index1)
                        {
                            uint[] vertexSet = meshData.GetVertexSet(vertexSets[index1]);
                            ItemSet itemSet = new ItemSet();
                            itemSet.Name = vertexSets[index1];
                            itemSet.Items = Enumerable.Repeat<bool>(false, meshData.VertexCount).ToArray<bool>();
                            for (int index2 = 0; index2 < vertexSet.Length; ++index2)
                                itemSet.Items[(int)vertexSet[index2]] = true;
                            data.VertexSets.Add(itemSet);
                        }
                    }
                    string[] edgeSets = meshData.EdgeSets;
                    if (edgeSets != null && edgeSets.Length != 0)
                    {
                        for (int index1 = 0; index1 < edgeSets.Length; ++index1)
                        {
                            uint[] edgeSet = meshData.GetEdgeSet(edgeSets[index1]);
                            ItemSet itemSet = new ItemSet();
                            itemSet.Name = edgeSets[index1];
                            itemSet.Items = Enumerable.Repeat<bool>(false, meshData.CornerCount).ToArray<bool>();
                            for (int index2 = 0; index2 < edgeSet.Length; ++index2)
                                itemSet.Items[(int)edgeSet[index2]] = true;
                            data.EdgeSets.Add(itemSet);
                        }
                    }
                    string[] faceSets = meshData.FaceSets;
                    if (faceSets != null && faceSets.Length != 0)
                    {
                        for (int index1 = 0; index1 < faceSets.Length; ++index1)
                        {
                            uint[] faceSet = meshData.GetFaceSet(faceSets[index1]);
                            ItemSet itemSet = new ItemSet();
                            itemSet.Name = faceSets[index1];
                            itemSet.Items = Enumerable.Repeat<bool>(false, meshData.TriangleCount).ToArray<bool>();
                            for (int index2 = 0; index2 < faceSet.Length; ++index2)
                                itemSet.Items[(int)faceSet[index2]] = true;
                            data.FaceSets.Add(itemSet);
                        }
                    }
                    mesh2.AddMeshSourceData(data);
                }
                scene.AddMesh(mesh2);
                this.sceneProgress.ReportProgress(20 + (int)((double)(num1 / ssfFile.MeshTable.Meshes.Count) * 0.6 * 100.0));
                ++num1;
            }
        }

        private void AddMaterials(SSFFile ssfFile, Simplygon.Scene.Scene scene)
        {
            int num = 0;
            foreach (Simplygon.DataFormat.v20.Scene.Material material1 in ssfFile.MaterialTable.Materials)
            {
                Simplygon.Scene.Asset.Material material2 = new Simplygon.Scene.Asset.Material(new Guid(material1.Id), material1.Name);
                foreach (Simplygon.DataFormat.v20.Scene.MaterialChannel materialChannel1 in material1.MaterialChannels)
                {
                    Simplygon.Scene.Asset.MaterialChannel materialChannel2 = material2.AddMaterialChannel(materialChannel1.ChannelName);
                    float x = (float)materialChannel1.Color.X;
                    float y = (float)materialChannel1.Color.Y;
                    float z = (float)materialChannel1.Color.Z;
                    float w = (float)materialChannel1.Color.W;
                    materialChannel2.Color = new Color(x, y, z, w, 32);
                    materialChannel2.ShadingNetwork = materialChannel1.ShadingNetwork;
                    materialChannel2.DeltagenShininess = materialChannel1.DeltagenShininess;
                    materialChannel2.DeltagenReflectivity = materialChannel1.DeltagenReflectivity;
                    materialChannel2.DeltagenReflectionExposure = materialChannel1.DeltagenReflectionExposure;
                    materialChannel2.DeltagenFresnelMinimum = materialChannel1.DeltagenFresnelMinimun;
                    materialChannel2.DeltagenFresnelScale = materialChannel1.DeltagenFresnelScale;
                    materialChannel2.DeltagenFresnelExponent = materialChannel1.DeltagenFresnelExponent;
                    materialChannel2.DeltagenUseReflection = materialChannel1.DeltagenUseReflection;
                    materialChannel2.DeltagenUseFresnel = materialChannel1.DeltagenUseFresnel;
                    materialChannel2.DeltagenUseClearCoat = materialChannel1.DeltagenUseClearCoat;
                    materialChannel2.DeltagenColoredReflection = materialChannel1.DeltagenColoredReflection;
                    materialChannel2.DeltagenTransparency = materialChannel1.DeltagenTransparency;
                    materialChannel2.DeltagenNormalMapScale = materialChannel1.DeltagenNormalMapScale;
                    foreach (Simplygon.DataFormat.v20.Scene.MaterialChannelTextureDescriptor texture in materialChannel1.Textures)
                    {
                        Guid textureId = new Guid(texture.TextureID);
                        materialChannel2.AddTexture(textureId, texture.TexCoordSet);
                        if (texture.TexCoordSet != "MaterialLOD" && !this.MaterialLODTextures.Contains(texture.TextureID))
                            this.MaterialLODTextures.Add(texture.TextureID);
                        if (materialChannel1.ChannelName == "Normals" && !this.NormalMapTextures.Contains(texture.TextureID))
                            this.NormalMapTextures.Add(texture.TextureID);
                    }
                }
                material2.MaterialModel = this.MaterialModel != MaterialModel.NotDefined ? this.MaterialModel : (MaterialModel)material1.MaterialModel;
                material2.NormalMapType = this.NormalMapType != NormalMapType.NotDefined ? this.NormalMapType : (NormalMapType)material1.NormalMapType;
                scene.AddMaterial(material2);
                this.sceneProgress.ReportProgress((int)((double)(num / ssfFile.MaterialTable.Materials.Count) * 0.2 * 100.0));
                ++num;
            }
        }

        private void AddTextures(SSFFile ssfFile, Simplygon.Scene.Scene scene)
        {
            string texturesDirectory = ssfFile.TextureTable.TexturesDirectory;
            foreach (Simplygon.DataFormat.v20.Scene.Texture texture1 in ssfFile.TextureTable.Textures)
            {
                texture1.Path = texture1.Path.TrimStart('\\', '/');
                Path.GetExtension(texture1.Path);
                Simplygon.Scene.Asset.Texture texture2 = new Simplygon.Scene.Asset.Texture(new Guid(texture1.Id), texture1.Name, texture1.Path);
                texture2.ColorSpace = !this.NormalMapTextures.Contains(texture2.Id.ToString()) ? (this.TextureColorSpace != ColorSpaceType.NotDefined ? this.TextureColorSpace : texture1.ColorSpace) : ColorSpaceType.Linear;
                scene.AddTexture(texture2);
                if (this.OnlyComputeHash)
                    this.Hash = SHA256.ComputeString(this.Hash + SHA256.ComputeFile(Path.Combine(scene.SceneDirectory, texture2.Path)));
            }
        }

        private void SetupBones(Simplygon.Scene.Scene scene)
        {
            foreach (KeyValuePair<Guid, Simplygon.Scene.Asset.Mesh> mesh in scene.Meshes)
            {
                foreach (Guid bone in mesh.Value.Bones)
                {
                    Simplygon.Scene.Graph.Node nodeById = scene.GetNodeById(bone);
                    if (nodeById != null)
                        nodeById.IsBone = true;
                }
            }
        }
    }
}
