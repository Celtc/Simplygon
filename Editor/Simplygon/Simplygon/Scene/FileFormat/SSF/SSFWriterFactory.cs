﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.FileFormat.SSF.SSFWriterFactory
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Scene.FileFormat.SSF
{
  internal class SSFWriterFactory
  {
    public static IWriter GetWriter(WriterVersion writerBase)
    {
      IWriter writer;
      switch (writerBase)
      {
        case WriterVersion.SSFWriterV10:
          writer = (IWriter) new WriterV10();
          break;
        case WriterVersion.SSFWriterV20:
          writer = (IWriter) new WriterV20();
          break;
        default:
          writer = (IWriter) new WriterV20();
          break;
      }
      return writer;
    }
  }
}
