﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.FileFormat.SSF.SSFReaderFactory
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat;

namespace Simplygon.Scene.FileFormat.SSF
{
  internal class SSFReaderFactory
  {
    public static IReader GetReader(string filename)
    {
      FileVersion fileVersion = FileHeader.ReadFileVersion(filename);
      ReaderVersion readerVersion = ReaderVersion.SSFReaderV20;
      if ((int) fileVersion.MajorVersion == 1)
        readerVersion = ReaderVersion.SSFReaderV10;
      IReader reader;
      switch (readerVersion)
      {
        case ReaderVersion.SSFReaderV10:
          reader = (IReader) new ReaderV10();
          break;
        case ReaderVersion.SSFReaderV20:
          reader = (IReader) new ReaderV20();
          break;
        default:
          reader = (IReader) new ReaderV20();
          break;
      }
      return reader;
    }
  }
}
