﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.FileFormat.SSF.ReaderV10
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v10.DataType;
using Simplygon.DataFormat.v10.Scene;
using Simplygon.Math;
using Simplygon.Scene.Common.Enums;
using Simplygon.Scene.Common.Extensions;
using Simplygon.Scene.Common.Progress;
using Simplygon.Scene.Graph;
using System;
using System.Collections.Generic;
using System.IO;
using System.Globalization;

namespace Simplygon.Scene.FileFormat.SSF
{
  internal class ReaderV10 : IReader
  {
    private Dictionary<string, Dictionary<uint, Guid>> meshTableMapping = new Dictionary<string, Dictionary<uint, Guid>>();
    private List<Simplygon.Scene.Asset.Texture> textureMapping = new List<Simplygon.Scene.Asset.Texture>();
    private Dictionary<int, Guid> materialMapping = new Dictionary<int, Guid>();

    public event MissingTextureEventHandler RaiseMissingTexture;

    internal ReaderV10()
    {
    }

    public string ComputeHash(string filename)
    {
      return string.Empty;
    }

    public Simplygon.Scene.Scene Read(string filename, ColorSpaceType textureColorSpace, MaterialModel materialModel, NormalMapType normalMapType, SceneProgress progress)
    {
      SSFFile ssfFile = new SSFFile();
      ssfFile.Read(filename);
      Simplygon.Scene.Scene scene = new Simplygon.Scene.Scene();
      scene.SceneDirectory = Path.GetDirectoryName(filename);
      this.AddMaterials(ssfFile, scene);
      this.AddMeshes(ssfFile, scene);
      this.AddSceneGraph(ssfFile, scene);
      return scene;
    }

    public void CreateTestScene(string inFilename, string outFilename, int sizeX, int sizeY, int sizeZ, float offset)
    {
      SSFFile ssfFile1 = new SSFFile();
      SSFFile ssfFile2 = new SSFFile();
      ssfFile1.Read(inFilename);
      if (ssfFile1.MeshTables.Count <= 0 || ssfFile1.MeshTables[0].Meshes.Count <= 0)
        return;
      MeshTable meshTable = new MeshTable();
      meshTable.Name = "MeshTable";
      MaterialTable materialTable = new MaterialTable();
      materialTable.Name = "MaterialTable";
      TextureTable textureTable = new TextureTable();
      textureTable.Name = "TextureTable";
      ssfFile2.MeshTables.Add(meshTable);
      ssfFile2.MaterialTables.Add(materialTable);
      ssfFile2.TextureTables.Add(textureTable);
      SceneRoot sceneRoot = new SceneRoot();
      sceneRoot.Name = "Test";
      float num1 = 0.0f;
      for (int index1 = 0; index1 < sizeZ; ++index1)
      {
        float num2 = 0.0f;
        for (int index2 = 0; index2 < sizeY; ++index2)
        {
          float num3 = 0.0f;
          for (int index3 = 0; index3 < sizeX; ++index3)
          {
            SceneNode sceneNode = new SceneNode();
            sceneNode.Name = "Test";
            sceneNode.Type = 1UL;
            meshTable.Meshes.Add(ssfFile1.MeshTables[0].Meshes[0]);
            sceneNode.MaterialTable = "MaterialTable";
            sceneNode.MaterialIndex = 0U;
            sceneNode.MeshTable = "MeshTable";
            sceneNode.MeshIndex = (uint) (meshTable.Meshes.Count - 1);
            Matrix4x4 matrix4x4 = new Matrix4x4();
            matrix4x4.SetIdentity();
            matrix4x4._41 = (double) num3;
            matrix4x4._42 = (double) num2;
            matrix4x4._43 = (double) num1;
            sceneNode.Transform = matrix4x4;
            sceneRoot.SceneNodes.Add(sceneNode);
            num3 += offset;
          }
          num2 += offset;
        }
        num1 += offset;
      }
      ssfFile2.SceneRoots.Add(sceneRoot);
      ssfFile2.Write(outFilename);
    }

    private void AddSceneGraph(SSFFile ssfFile, Simplygon.Scene.Scene scene)
    {
      if (ssfFile.SceneRoots.Count <= 0)
        return;
      SceneRoot sceneRoot = ssfFile.SceneRoots[0];
      Node node1 = new Node(Guid.NewGuid(), sceneRoot.Name);
      node1.LocalTransform = Matrix.Identity;
      scene.AddNode(node1, (Node) null);
      scene.SetRootNode(node1);
      foreach (SceneNode sceneNode1 in sceneRoot.SceneNodes)
      {
        Node node2 = new Node(Guid.NewGuid(), sceneNode1.Name);
        if (((SceneNodeType) sceneNode1.Type).HasFlag((Enum) SceneNodeType.Mesh))
          node2.SetMeshID(scene, this.meshTableMapping[sceneNode1.MeshTable][sceneNode1.MeshIndex]);
        node2.LocalTransform = new Matrix(sceneNode1.Transform._11, sceneNode1.Transform._12, sceneNode1.Transform._13, sceneNode1.Transform._14, sceneNode1.Transform._21, sceneNode1.Transform._22, sceneNode1.Transform._23, sceneNode1.Transform._24, sceneNode1.Transform._31, sceneNode1.Transform._32, sceneNode1.Transform._33, sceneNode1.Transform._34, sceneNode1.Transform._41, sceneNode1.Transform._42, sceneNode1.Transform._43, sceneNode1.Transform._44);
        scene.AddNode(node2, node1);
        foreach (SceneNode sceneNode2 in sceneNode1.SceneNodes)
          this.AddNodeChildren(scene, sceneNode2, node2);
      }
    }

    private void AddNodeChildren(Simplygon.Scene.Scene scene, SceneNode ssfNode, Node parentNode)
    {
      Node node = new Node(Guid.NewGuid(), ssfNode.Name);
      node.LocalTransform = new Matrix(ssfNode.Transform._11, ssfNode.Transform._12, ssfNode.Transform._13, ssfNode.Transform._14, ssfNode.Transform._21, ssfNode.Transform._22, ssfNode.Transform._23, ssfNode.Transform._24, ssfNode.Transform._31, ssfNode.Transform._32, ssfNode.Transform._33, ssfNode.Transform._34, ssfNode.Transform._41, ssfNode.Transform._42, ssfNode.Transform._43, ssfNode.Transform._44);
      if (((SceneNodeType) ssfNode.Type).HasFlag((Enum) SceneNodeType.Mesh))
        node.SetMeshID(scene, this.meshTableMapping[ssfNode.MeshTable][ssfNode.MeshIndex]);
      scene.AddNode(node, parentNode);
      foreach (SceneNode sceneNode in ssfNode.SceneNodes)
        this.AddNodeChildren(scene, sceneNode, node);
    }

    private void AddNodeMeshAttributes(SceneNode ssfNode, Node node, Simplygon.Scene.Scene scene)
    {
      if ((long) ssfNode.Type != 1L || !this.meshTableMapping.ContainsKey(ssfNode.MeshTable) || !this.meshTableMapping[ssfNode.MeshTable].ContainsKey(ssfNode.MeshIndex))
        return;
      Guid meshId = this.meshTableMapping[ssfNode.MeshTable][ssfNode.MeshIndex];
      node.SetMeshID(scene, meshId);
    }

    private void AddMeshes(SSFFile ssfFile, Simplygon.Scene.Scene scene)
    {
      foreach (MeshTable meshTable in ssfFile.MeshTables)
      {
        Dictionary<uint, Guid> dictionary = new Dictionary<uint, Guid>();
        uint key = 0;
        foreach (Simplygon.DataFormat.v10.Scene.Mesh mesh1 in meshTable.Meshes)
        {
          Simplygon.Scene.Asset.Mesh mesh2 = new Simplygon.Scene.Asset.Mesh(Guid.NewGuid(), mesh1.Name);
          dictionary.Add(key, mesh2.Id);
          if (mesh1.Coordinates.Count > 0)
          {
            Simplygon.Math.Vector4[] vector4Array = new Simplygon.Math.Vector4[mesh1.Coordinates.Count];
            int index = 0;
            foreach (Simplygon.DataFormat.v10.DataType.Vector3 coordinate in mesh1.Coordinates)
            {
              vector4Array[index] = new Simplygon.Math.Vector4(coordinate.X, coordinate.Y, coordinate.Z, 1.0);
              ++index;
            }
            mesh2[0].Coordinates = vector4Array;
          }
          if (mesh1.Triangles.Count > 0)
          {
            int[] numArray = new int[mesh1.Triangles.Count * 3];
            int index1 = 0;
            foreach (Index3 triangle in mesh1.Triangles)
            {
              numArray[index1] = triangle.X;
              int index2 = index1 + 1;
              numArray[index2] = triangle.Y;
              int index3 = index2 + 1;
              numArray[index3] = triangle.Z;
              index1 = index3 + 1;
            }
            mesh2[0].Triangles = numArray;
          }
          if (mesh1.Normals.Count > 0)
          {
            Simplygon.Math.Vector3[] vector3Array1 = new Simplygon.Math.Vector3[mesh1.Triangles.Count * 3];
            int num1 = 0;
            for (int index1 = 0; index1 < mesh1.Triangles.Count; ++index1)
            {
              Simplygon.DataFormat.v10.DataType.Vector3 normal1 = mesh1.Normals[mesh1.Triangles[index1].X];
              Simplygon.DataFormat.v10.DataType.Vector3 normal2 = mesh1.Normals[mesh1.Triangles[index1].Y];
              Simplygon.DataFormat.v10.DataType.Vector3 normal3 = mesh1.Normals[mesh1.Triangles[index1].Z];
              Simplygon.Math.Vector3[] vector3Array2 = vector3Array1;
              int index2 = num1;
              int num2 = index2 + 1;
              Simplygon.Math.Vector3 vector3_1 = new Simplygon.Math.Vector3(normal1.X, normal1.Y, normal1.Z);
              vector3Array2[index2] = vector3_1;
              Simplygon.Math.Vector3[] vector3Array3 = vector3Array1;
              int index3 = num2;
              int num3 = index3 + 1;
              Simplygon.Math.Vector3 vector3_2 = new Simplygon.Math.Vector3(normal2.X, normal2.Y, normal2.Z);
              vector3Array3[index3] = vector3_2;
              Simplygon.Math.Vector3[] vector3Array4 = vector3Array1;
              int index4 = num3;
              num1 = index4 + 1;
              Simplygon.Math.Vector3 vector3_3 = new Simplygon.Math.Vector3(normal3.X, normal3.Y, normal3.Z);
              vector3Array4[index4] = vector3_3;
            }
            mesh2[0].Normals = vector3Array1;
          }
          if (mesh1.Tangents.Count > 0)
          {
            Simplygon.Math.Vector3[] vector3Array1 = new Simplygon.Math.Vector3[mesh1.Triangles.Count * 3];
            int num1 = 0;
            for (int index1 = 0; index1 < mesh1.Triangles.Count; ++index1)
            {
              Simplygon.DataFormat.v10.DataType.Vector4 tangent1 = mesh1.Tangents[mesh1.Triangles[index1].X];
              Simplygon.DataFormat.v10.DataType.Vector4 tangent2 = mesh1.Tangents[mesh1.Triangles[index1].Y];
              Simplygon.DataFormat.v10.DataType.Vector4 tangent3 = mesh1.Tangents[mesh1.Triangles[index1].Z];
              Simplygon.Math.Vector3[] vector3Array2 = vector3Array1;
              int index2 = num1;
              int num2 = index2 + 1;
              Simplygon.Math.Vector3 vector3_1 = new Simplygon.Math.Vector3(tangent1.X, tangent1.Y, tangent1.Z);
              vector3Array2[index2] = vector3_1;
              Simplygon.Math.Vector3[] vector3Array3 = vector3Array1;
              int index3 = num2;
              int num3 = index3 + 1;
              Simplygon.Math.Vector3 vector3_2 = new Simplygon.Math.Vector3(tangent2.X, tangent2.Y, tangent2.Z);
              vector3Array3[index3] = vector3_2;
              Simplygon.Math.Vector3[] vector3Array4 = vector3Array1;
              int index4 = num3;
              num1 = index4 + 1;
              Simplygon.Math.Vector3 vector3_3 = new Simplygon.Math.Vector3(tangent3.X, tangent3.Y, tangent3.Z);
              vector3Array4[index4] = vector3_3;
            }
            mesh2[0].Tangents = vector3Array1;
          }
          if (mesh1.Bitangents.Count > 0)
          {
            Simplygon.Math.Vector3[] vector3Array1 = new Simplygon.Math.Vector3[mesh1.Triangles.Count * 3];
            int num1 = 0;
            for (int index1 = 0; index1 < mesh1.Triangles.Count; ++index1)
            {
              Simplygon.DataFormat.v10.DataType.Vector3 bitangent1 = mesh1.Bitangents[mesh1.Triangles[index1].X];
              Simplygon.DataFormat.v10.DataType.Vector3 bitangent2 = mesh1.Bitangents[mesh1.Triangles[index1].Y];
              Simplygon.DataFormat.v10.DataType.Vector3 bitangent3 = mesh1.Bitangents[mesh1.Triangles[index1].Z];
              Simplygon.Math.Vector3[] vector3Array2 = vector3Array1;
              int index2 = num1;
              int num2 = index2 + 1;
              Simplygon.Math.Vector3 vector3_1 = new Simplygon.Math.Vector3(bitangent1.X, bitangent1.Y, bitangent1.Z);
              vector3Array2[index2] = vector3_1;
              Simplygon.Math.Vector3[] vector3Array3 = vector3Array1;
              int index3 = num2;
              int num3 = index3 + 1;
              Simplygon.Math.Vector3 vector3_2 = new Simplygon.Math.Vector3(bitangent2.X, bitangent2.Y, bitangent2.Z);
              vector3Array3[index3] = vector3_2;
              Simplygon.Math.Vector3[] vector3Array4 = vector3Array1;
              int index4 = num3;
              num1 = index4 + 1;
              Simplygon.Math.Vector3 vector3_3 = new Simplygon.Math.Vector3(bitangent3.X, bitangent3.Y, bitangent3.Z);
              vector3Array4[index4] = vector3_3;
            }
            mesh2[0].Bitangents = vector3Array1;
          }
          if (mesh1.TextureCoordinates.Count > 0)
          {
            foreach (TextureCoordinates textureCoordinate1 in mesh1.TextureCoordinates)
            {
              Simplygon.Math.Vector2[] vector2Array1 = new Simplygon.Math.Vector2[mesh1.Triangles.Count * 3];
              int num1 = 0;
              for (int index1 = 0; index1 < mesh1.Triangles.Count; ++index1)
              {
                Simplygon.DataFormat.v10.DataType.Vector2 textureCoordinate2 = textureCoordinate1.TextureCoordinateList[mesh1.Triangles[index1].X];
                Simplygon.DataFormat.v10.DataType.Vector2 textureCoordinate3 = textureCoordinate1.TextureCoordinateList[mesh1.Triangles[index1].Y];
                Simplygon.DataFormat.v10.DataType.Vector2 textureCoordinate4 = textureCoordinate1.TextureCoordinateList[mesh1.Triangles[index1].Z];
                Simplygon.Math.Vector2[] vector2Array2 = vector2Array1;
                int index2 = num1;
                int num2 = index2 + 1;
                Simplygon.Math.Vector2 vector2_1 = new Simplygon.Math.Vector2(textureCoordinate2.X, textureCoordinate2.Y);
                vector2Array2[index2] = vector2_1;
                Simplygon.Math.Vector2[] vector2Array3 = vector2Array1;
                int index3 = num2;
                int num3 = index3 + 1;
                Simplygon.Math.Vector2 vector2_2 = new Simplygon.Math.Vector2(textureCoordinate3.X, textureCoordinate3.Y);
                vector2Array3[index3] = vector2_2;
                Simplygon.Math.Vector2[] vector2Array4 = vector2Array1;
                int index4 = num3;
                num1 = index4 + 1;
                Simplygon.Math.Vector2 vector2_3 = new Simplygon.Math.Vector2(textureCoordinate4.X, textureCoordinate4.Y);
                vector2Array4[index4] = vector2_3;
              }
              mesh2[0].TexCoords.Add(textureCoordinate1.Channel.ToString(), vector2Array1);
            }
          }
          if (mesh1.MaterialIndexPerPolygon.Count > 0)
          {
            int[] numArray = new int[mesh1.Triangles.Count];
            for (int index = 0; index < mesh1.Triangles.Count; ++index)
            {
              Guid guid = this.materialMapping[0];
              int num = mesh2.Materials.IndexOf(guid);
              if (num < 0)
              {
                mesh2.Materials.Add(guid);
                num = mesh2.Materials.Count - 1;
              }
              numArray[index] = num;
            }
            mesh2[0].MaterialIndices = numArray;
          }
          scene.AddMesh(mesh2);
          ++key;
        }
        this.meshTableMapping.Add(meshTable.Name, dictionary);
      }
    }

    private void AddMaterials(SSFFile ssfFile, Simplygon.Scene.Scene scene)
    {
      foreach (MaterialTable materialTable in ssfFile.MaterialTables)
      {
        Dictionary<uint, Guid> dictionary = new Dictionary<uint, Guid>();
        int key = 0;
        foreach (Simplygon.DataFormat.v10.Scene.Material material1 in materialTable.Materials)
        {
          Simplygon.Scene.Asset.Material material2 = new Simplygon.Scene.Asset.Material(Guid.NewGuid(), material1.Name);
          foreach (Simplygon.DataFormat.v10.Scene.MaterialChannel materialChannel1 in material1.MaterialChannels)
          {
            Simplygon.Scene.Asset.MaterialChannel materialChannel2 = material2.AddMaterialChannel(string.Format(CultureInfo.InvariantCulture, "Material{0}", (object) key));
            float x = (float) materialChannel1.Color.X;
            float y = (float) materialChannel1.Color.Y;
            float z = (float) materialChannel1.Color.Z;
            float w = (float) materialChannel1.Color.W;
            materialChannel2.Color = new Color(x, y, z, w, 32);
            materialChannel2.ShadingNetwork = string.Empty;
          }
          scene.AddMaterial(material2);
          this.materialMapping.Add(key, material2.Id);
          ++key;
        }
      }
    }
  }
}
