﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.FileFormat.SSF.WriterV20
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.DataType;
using Simplygon.DataFormat.v20.Scene;
using Simplygon.Math;
using Simplygon.Scene.Asset;
using Simplygon.Scene.Common;
using Simplygon.Scene.Common.Interfaces;
using Simplygon.Scene.Common.Progress;
using Simplygon.Scene.Common.WorkDirectory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Simplygon.Scene.FileFormat.SSF
{
  internal class WriterV20 : IWriter
  {
    private string outputFileWithoutExt = string.Empty;
    private string outputDirectory = string.Empty;
    private Simplygon.Scene.Scene sceneToWrite;
    private bool overWriteTextures;
    private SceneProgress sceneProgress;

    internal WriterV20()
    {
    }

    public string RootDirectory { get; set; }

    public void Write(Simplygon.Scene.Scene inputScene, string filename, bool overWriteTextures, bool embeddedMedia, SceneProgress progress, IWorkDirectoryInfo workDirectory)
    {
      this.overWriteTextures = overWriteTextures;
      if (inputScene == null)
        return;
      if (progress == null)
        progress = new SceneProgress();
      this.sceneProgress = progress;
      progress.ReportProgress(0);
      this.RootDirectory = Path.GetDirectoryName(filename);
      this.sceneToWrite = inputScene.DeepCopy();
      this.outputFileWithoutExt = Path.GetFileNameWithoutExtension(filename);
      this.outputDirectory = Path.GetDirectoryName(filename);
      SSFFile ssfFile = new SSFFile();
      ssfFile.FileHeader.FileVersion.MajorVersion = (byte) 2;
      ssfFile.FileHeader.FileVersion.MinorVersion = (byte) 0;
      ssfFile.WorldOrientation = inputScene.WorldOrientation;
      ssfFile.CoordinateSystem = inputScene.OriginalCoordinateSystem;
      NodeTable nodeTable = new NodeTable();
      MeshTable meshTable = new MeshTable();
      MaterialTable materialTable = new MaterialTable();
      TextureTable textureTable = new TextureTable();
      progress.ReportProgress(25);
      this.AddTextures(this.sceneToWrite, textureTable);
      this.AddMaterials(this.sceneToWrite, materialTable);
      this.AddMeshes(this.sceneToWrite, meshTable);
      this.AddNodes(this.sceneToWrite, nodeTable);
      ssfFile.TextureTable = textureTable;
      ssfFile.MaterialTable = materialTable;
      ssfFile.MeshTable = meshTable;
      ssfFile.NodeTable = nodeTable;
      progress.ReportProgress(50);
      if (this.sceneToWrite.ProxyGroupSets != null)
      {
        foreach (KeyValuePair<string, string[]> proxyGroupSet in this.sceneToWrite.ProxyGroupSets)
          ssfFile.SetProxyGroupSet(proxyGroupSet.Key, proxyGroupSet.Value);
      }
      if (this.sceneToWrite.OccluderGroupSets != null)
      {
        foreach (KeyValuePair<string, string[]> occluderGroupSet in this.sceneToWrite.OccluderGroupSets)
          ssfFile.SetOccluderGroupSet(occluderGroupSet.Key, occluderGroupSet.Value);
      }
      if (this.sceneToWrite.SelectionGroupSets != null)
      {
        foreach (var selectionGroupSet in this.sceneToWrite.SelectionGroupSets)
          ssfFile.SetSelectionGroupSet(selectionGroupSet.Key.Id, selectionGroupSet.Key.Name, selectionGroupSet.Value.ToArray());
      }
      if (this.sceneToWrite.CuttingPlanes != null)
      {
        foreach (var cuttingPlane in this.sceneToWrite.CuttingPlanes)
        {
          if (cuttingPlane.Value.Count > 0)
          {
            Simplygon.Math.Plane plane = cuttingPlane.Value.First<Simplygon.Math.Plane>();
            ssfFile.AddCuttingPlane(cuttingPlane.Key.Id, cuttingPlane.Key.Name, new Simplygon.DataFormat.v20.DataType.Plane(new Simplygon.DataFormat.v20.DataType.Vector3(plane.Point.X, plane.Point.Y, plane.Point.Z), new Simplygon.DataFormat.v20.DataType.Vector3(plane.Normal.X, plane.Normal.Y, plane.Normal.Z)));
          }
        }
      }
      progress.ReportProgress(75);
      ssfFile.Write(filename);
      progress.ReportProgress(100);
    }

    private void AddNodes(Simplygon.Scene.Scene scene, NodeTable nodeTable)
    {
      this.AddNodes(scene.RootNode, nodeTable);
    }

    private void AddNodes(Simplygon.Scene.Graph.Node sceneNode, NodeTable nodeTable)
    {
      Simplygon.DataFormat.v20.Scene.Node node = new Simplygon.DataFormat.v20.Scene.Node();
      nodeTable.Nodes.Add(node);
      node.Id = sceneNode.Id.ToString();
      node.Name = sceneNode.Name;
      node.IsHLODReplacementNode = sceneNode.IsHLODReplacementNode;
      node.HLODOnScreenSize = sceneNode.HLODOnScreenSize;
      node.IsCurrentReplacement = sceneNode.OngoingReplacement;
      node.LocalTransform = new Matrix4x4(sceneNode.LocalTransform.M11, sceneNode.LocalTransform.M12, sceneNode.LocalTransform.M13, sceneNode.LocalTransform.M14, sceneNode.LocalTransform.M21, sceneNode.LocalTransform.M22, sceneNode.LocalTransform.M23, sceneNode.LocalTransform.M24, sceneNode.LocalTransform.M31, sceneNode.LocalTransform.M32, sceneNode.LocalTransform.M33, sceneNode.LocalTransform.M34, sceneNode.LocalTransform.M41, sceneNode.LocalTransform.M42, sceneNode.LocalTransform.M43, sceneNode.LocalTransform.M44);
      node.ParentId = sceneNode.Parent.ToString();
      node.MeshId = sceneNode.MeshId.ToString();
      node.ReplacementId = sceneNode.ReplacementId.ToString();
      node.IsFrozen = sceneNode.IsFrozen;
      if (sceneNode.GlobalLODIndices.Count > 0)
        node.SetGlobalLODIndices(sceneNode.GlobalLODIndices.ToArray());
      for (int index = 0; index < sceneNode.Children.Count; ++index)
      {
        Simplygon.Scene.Graph.Node nodeById = this.sceneToWrite.GetNodeById(sceneNode.Children[index]);
        if (nodeById != null)
          this.AddNodes(nodeById, nodeTable);
      }
    }

    private void AddMeshes(Simplygon.Scene.Scene scene, MeshTable meshTable)
    {
      foreach (KeyValuePair<Guid, Simplygon.Scene.Asset.Mesh> mesh1 in scene.Meshes)
      {
        Simplygon.DataFormat.v20.Scene.Mesh mesh2 = new Simplygon.DataFormat.v20.Scene.Mesh();
        mesh2.Name = mesh1.Value.Name;
        mesh2.Id = mesh1.Value.Id.ToString();
        if (mesh1.Value.Materials.Count > 0)
        {
          string[] materialIds = new string[mesh1.Value.Materials.Count];
          for (int index = 0; index < materialIds.Length; ++index)
            materialIds[index] = mesh1.Value.Materials[index].ToString();
          mesh2.SetMaterialIds(materialIds);
        }
        if (mesh1.Value.Bones.Count > 0)
        {
          string[] boneIds = new string[mesh1.Value.Bones.Count];
          for (int index = 0; index < boneIds.Length; ++index)
            boneIds[index] = mesh1.Value.Bones[index].ToString();
          mesh2.SetBoneIds(boneIds);
        }
        foreach (Simplygon.Scene.Asset.MeshData meshSource in mesh1.Value.MeshSources)
        {
          Simplygon.DataFormat.v20.Scene.MeshData meshData = new Simplygon.DataFormat.v20.Scene.MeshData();
          if (meshSource.HasMaxDeviation())
            meshData.SetMaxDeviation(meshSource.MaxDeviation);
          if (meshSource.HasPixelSize())
            meshData.SetPixelSize(meshSource.PixelSize);
          meshData.SetGlobalLODIndex(meshSource.GlobalLODIndex);
          if (meshSource.Coordinates != null && meshSource.Coordinates.Length != 0)
          {
            Simplygon.DataFormat.v20.DataType.Vector3[] coordinates = new Simplygon.DataFormat.v20.DataType.Vector3[meshSource.Coordinates.Length];
            for (int index = 0; index < meshSource.Coordinates.Length; ++index)
            {
              Simplygon.Math.Vector4 coordinate = meshSource.Coordinates[index];
              coordinates[index] = new Simplygon.DataFormat.v20.DataType.Vector3(coordinate.X, coordinate.Y, coordinate.Z);
            }
            meshData.SetCoordinates(coordinates);
          }
          if (meshSource.Triangles != null && meshSource.Triangles.Length != 0)
          {
            Index3[] triangleIndices = new Index3[meshSource.Triangles.Length / 3];
            for (int index = 0; index < meshSource.Triangles.Length / 3; ++index)
            {
              int triangle1 = meshSource.Triangles[index * 3];
              int triangle2 = meshSource.Triangles[index * 3 + 1];
              int triangle3 = meshSource.Triangles[index * 3 + 2];
              triangleIndices[index] = new Index3(triangle1, triangle2, triangle3);
            }
            meshData.SetTriangleIndices(triangleIndices);
          }
          if (meshSource.VertexLocks != null && meshSource.VertexLocks.Length != 0)
          {
            bool[] vertexLocks = new bool[meshSource.VertexLocks.Length];
            for (int index = 0; index < meshSource.VertexLocks.Length; ++index)
              vertexLocks[index] = meshSource.VertexLocks[index];
            meshData.SetVertexLocks(vertexLocks);
          }
          if (meshSource.VertexWeights != null && meshSource.VertexWeights.Length != 0)
          {
            double[] vertexWeights = new double[meshSource.VertexWeights.Length];
            for (int index = 0; index < meshSource.VertexWeights.Length; ++index)
              vertexWeights[index] = meshSource.VertexWeights[index];
            meshData.SetVertexWeights(vertexWeights);
          }
          if (meshSource.VertexCrease != null && meshSource.VertexCrease.Length != 0)
          {
            double[] vertexCrease = new double[meshSource.VertexCrease.Length];
            for (int index = 0; index < meshSource.VertexCrease.Length; ++index)
              vertexCrease[index] = meshSource.VertexCrease[index];
            meshData.SetVertexCrease(vertexCrease);
          }
          if (meshSource.SmoothingGroup != null && meshSource.SmoothingGroup.Length != 0)
          {
            int[] smoothingGroup = new int[meshSource.SmoothingGroup.Length];
            for (int index = 0; index < meshSource.SmoothingGroup.Length; ++index)
              smoothingGroup[index] = meshSource.SmoothingGroup[index];
            meshData.SetSmoothingGroup(smoothingGroup);
          }
          if (meshSource.OriginalVertexIndices != null && meshSource.OriginalVertexIndices.Length != 0)
          {
            int[] originalVertexIndices = new int[meshSource.OriginalVertexIndices.Length];
            for (int index = 0; index < meshSource.OriginalVertexIndices.Length; ++index)
              originalVertexIndices[index] = meshSource.OriginalVertexIndices[index];
            meshData.SetOriginalVertexIndices(originalVertexIndices);
          }
          if (meshSource.EdgeCrease != null && meshSource.EdgeCrease.Length != 0)
          {
            double[] edgeCrease = new double[meshSource.EdgeCrease.Length];
            for (int index = 0; index < meshSource.EdgeCrease.Length; ++index)
              edgeCrease[index] = meshSource.EdgeCrease[index];
            meshData.SetEdgeCrease(edgeCrease);
          }
          if (meshSource.EdgeWeights != null && meshSource.EdgeWeights.Length != 0)
          {
            double[] edgeWeights = new double[meshSource.EdgeWeights.Length];
            for (int index = 0; index < meshSource.EdgeWeights.Length; ++index)
              edgeWeights[index] = meshSource.EdgeWeights[index];
            meshData.SetEdgeWeights(edgeWeights);
          }
          if (meshSource.OriginalTriangleIndices != null && meshSource.OriginalTriangleIndices.Length != 0)
          {
            int[] originalTriangleIndices = new int[meshSource.OriginalTriangleIndices.Length];
            for (int index = 0; index < meshSource.OriginalTriangleIndices.Length; ++index)
              originalTriangleIndices[index] = meshSource.OriginalTriangleIndices[index];
            meshData.SetOriginalTriangleIndices(originalTriangleIndices);
          }
          if (meshSource.MaterialIndices != null && meshSource.MaterialIndices.Length != 0 && mesh1.Value.Materials.Count > 0)
          {
            uint[] materialIndices = new uint[meshSource.Triangles.Length / 3];
            for (int index = 0; index < meshSource.Triangles.Length / 3; ++index)
            {
              int materialIndex = meshSource.MaterialIndices[index];
              materialIndices[index] = (uint) materialIndex;
            }
            meshData.SetMaterialIndices(materialIndices);
          }
          if (meshSource.Normals != null && meshSource.Normals.Length != 0)
          {
            Simplygon.DataFormat.v20.DataType.Vector3[] normals = new Simplygon.DataFormat.v20.DataType.Vector3[meshSource.Normals.Length];
            for (int index = 0; index < meshSource.Normals.Length; ++index)
            {
              Simplygon.Math.Vector3 normal = meshSource.Normals[index];
              normals[index] = new Simplygon.DataFormat.v20.DataType.Vector3(normal.X, normal.Y, normal.Z);
            }
            meshData.SetNormals(normals);
          }
          if (meshSource.Tangents != null && meshSource.Tangents.Length != 0)
          {
            Simplygon.DataFormat.v20.DataType.Vector3[] tangents = new Simplygon.DataFormat.v20.DataType.Vector3[meshSource.Tangents.Length];
            for (int index = 0; index < meshSource.Tangents.Length; ++index)
            {
              Simplygon.Math.Vector3 tangent = meshSource.Tangents[index];
              tangents[index] = new Simplygon.DataFormat.v20.DataType.Vector3(tangent.X, tangent.Y, tangent.Z);
            }
            meshData.SetTangents(tangents);
          }
          if (meshSource.Bitangents != null && meshSource.Bitangents.Length != 0)
          {
            Simplygon.DataFormat.v20.DataType.Vector3[] bitangents = new Simplygon.DataFormat.v20.DataType.Vector3[meshSource.Bitangents.Length];
            for (int index = 0; index < meshSource.Bitangents.Length; ++index)
            {
              Simplygon.Math.Vector3 bitangent = meshSource.Bitangents[index];
              bitangents[index] = new Simplygon.DataFormat.v20.DataType.Vector3(bitangent.X, bitangent.Y, bitangent.Z);
            }
            meshData.SetBitangents(bitangents);
          }
          if (meshSource.BoneIndices != null && meshSource.VertexCount > 0)
          {
            int length = meshSource.BoneIndices.GetLength(1);
            int[] boneIndices = new int[meshSource.VertexCount * length];
            double[] boneWeights = new double[meshSource.VertexCount * length];
            for (int index1 = 0; index1 < meshSource.VertexCount; ++index1)
            {
              for (int index2 = 0; index2 < length; ++index2)
              {
                boneIndices[index1 * length + index2] = meshSource.BoneIndices[index1, index2];
                boneWeights[index1 * length + index2] = (double) meshSource.BoneWeights[index1, index2];
              }
            }
            meshData.SetBoneIndices(boneIndices, length);
            meshData.SetBoneWeights(boneWeights, length);
          }
          if (meshSource.TexCoords != null)
          {
            foreach (KeyValuePair<string, Simplygon.Math.Vector2[]> texCoord in meshSource.TexCoords)
            {
              Simplygon.DataFormat.v20.DataType.Vector2[] textureCoordinates = new Simplygon.DataFormat.v20.DataType.Vector2[meshSource.CornerCount];
              for (int index = 0; index < meshSource.CornerCount; ++index)
              {
                Simplygon.Math.Vector2 vector2 = texCoord.Value[index];
                textureCoordinates[index] = new Simplygon.DataFormat.v20.DataType.Vector2(vector2.X, vector2.Y);
              }
              meshData.SetTextureCoordinates(texCoord.Key, textureCoordinates);
            }
          }
          if (meshSource.VertexColors != null)
          {
            foreach (KeyValuePair<string, Color[]> vertexColor in meshSource.VertexColors)
            {
              Simplygon.DataFormat.v20.DataType.Vector4[] colors = new Simplygon.DataFormat.v20.DataType.Vector4[meshSource.CornerCount];
              for (int index = 0; index < meshSource.CornerCount; ++index)
              {
                Color color = vertexColor.Value[index];
                colors[index] = new Simplygon.DataFormat.v20.DataType.Vector4((double) color.R, (double) color.G, (double) color.B, (double) color.A);
              }
              meshData.SetColors(vertexColor.Key, colors);
            }
          }
          if (meshSource.BlendShapes != null)
          {
            foreach (KeyValuePair<string, Simplygon.Math.Vector3[]> blendShape1 in meshSource.BlendShapes)
            {
              Simplygon.DataFormat.v20.DataType.Vector3[] blendShape2 = new Simplygon.DataFormat.v20.DataType.Vector3[meshSource.CornerCount];
              for (int index = 0; index < meshSource.CornerCount; ++index)
              {
                Simplygon.Math.Vector3 vector3 = blendShape1.Value[index];
                blendShape2[index] = new Simplygon.DataFormat.v20.DataType.Vector3(vector3.X, vector3.Y, vector3.Z);
              }
              meshData.SetBlendShapes(blendShape1.Key, blendShape2);
            }
          }
          if (meshSource.CustomFields != null)
          {
            foreach (KeyValuePair<string, byte[,]> customField in meshSource.CustomFields)
              ;
          }
          if (meshSource.VertexSets != null)
          {
            for (int index1 = 0; index1 < meshSource.VertexSets.Count; ++index1)
            {
              ItemSet vertexSet1 = meshSource.VertexSets[index1];
              int length = 0;
              for (uint index2 = 0; (long) index2 < (long) vertexSet1.Items.Length; ++index2)
              {
                if (vertexSet1.Items[(int) index2])
                  ++length;
              }
              if (length > 0)
              {
                uint[] vertexSet2 = new uint[length];
                int index2 = 0;
                for (uint index3 = 0; (long) index3 < (long) vertexSet1.Items.Length; ++index3)
                {
                  if (vertexSet1.Items[(int) index3])
                  {
                    vertexSet2[index2] = index3;
                    ++index2;
                  }
                }
                meshData.SetVertexSet(vertexSet1.Name, vertexSet2);
              }
            }
          }
          if (meshSource.EdgeSets != null)
          {
            for (int index1 = 0; index1 < meshSource.EdgeSets.Count; ++index1)
            {
              ItemSet edgeSet1 = meshSource.EdgeSets[index1];
              int length = 0;
              for (uint index2 = 0; (long) index2 < (long) edgeSet1.Items.Length; ++index2)
              {
                if (edgeSet1.Items[(int) index2])
                  ++length;
              }
              if (length > 0)
              {
                uint[] edgeSet2 = new uint[length];
                int index2 = 0;
                for (uint index3 = 0; (long) index3 < (long) edgeSet1.Items.Length; ++index3)
                {
                  if (edgeSet1.Items[(int) index3])
                  {
                    edgeSet2[index2] = index3;
                    ++index2;
                  }
                }
                meshData.SetEdgeSet(edgeSet1.Name, edgeSet2);
              }
            }
          }
          if (meshSource.FaceSets != null)
          {
            for (int index1 = 0; index1 < meshSource.FaceSets.Count; ++index1)
            {
              ItemSet faceSet1 = meshSource.FaceSets[index1];
              int length = 0;
              for (uint index2 = 0; (long) index2 < (long) faceSet1.Items.Length; ++index2)
              {
                if (faceSet1.Items[(int) index2])
                  ++length;
              }
              if (length > 0)
              {
                uint[] faceSet2 = new uint[length];
                int index2 = 0;
                for (uint index3 = 0; (long) index3 < (long) faceSet1.Items.Length; ++index3)
                {
                  if (faceSet1.Items[(int) index3])
                  {
                    faceSet2[index2] = index3;
                    ++index2;
                  }
                }
                meshData.SetFaceSet(faceSet1.Name, faceSet2);
              }
            }
          }
          mesh2.MeshDatas.Add(meshData);
        }
        meshTable.Meshes.Add(mesh2);
      }
    }

    private void AddTextures(Simplygon.Scene.Scene scene, TextureTable textureTable)
    {
      foreach (KeyValuePair<Guid, Simplygon.Scene.Asset.Texture> texture1 in scene.Textures)
      {
        if ((texture1.Value.Path.Contains("\\\\") ? 1 : (texture1.Value.Path.Contains("//") ? 1 : 0)) != 0)
          texture1.Value.Path = texture1.Value.Path;
        else
          texture1.Value.Path = texture1.Value.Path.TrimStart('\\', '/');
        Simplygon.DataFormat.v20.Scene.Texture texture2 = new Simplygon.DataFormat.v20.Scene.Texture();
        Path.GetFileNameWithoutExtension(texture1.Value.Name);
        Path.GetExtension(texture1.Value.Path);
        string str1 = this.overWriteTextures ? texture1.Value.Path : WorkDirectoryInfo.GetUniqueRelativePath(this.outputDirectory, texture1.Value.Path);
        string str2 = Path.Combine(scene.SceneDirectory, texture1.Value.Path);
        texture1.Value.Path = str1;
        string path = Path.Combine(this.outputDirectory, Path.GetDirectoryName(str1));
        string str3 = Path.Combine(this.outputDirectory, str1);
        Directory.CreateDirectory(path);
        if (Path.GetFullPath(str2) != Path.GetFullPath(str3))
        {
          try
          {
            if (!this.overWriteTextures)
            {
              if (!File.Exists(str3))
                File.Copy(str2, str3, true);
            }
            else
              File.Copy(str2, str3, true);
          }
          catch (Exception ex)
          {
          }
        }
        texture2.Name = texture1.Value.Name;
        texture2.Id = texture1.Value.Id.ToString();
        texture2.Path = str1;
        texture2.ColorSpace = texture1.Value.ColorSpace;
        textureTable.Textures.Add(texture2);
      }
    }

    private void AddMaterials(Simplygon.Scene.Scene scene, MaterialTable materialTable)
    {
      foreach (KeyValuePair<Guid, Simplygon.Scene.Asset.Material> material1 in scene.Materials)
      {
        Simplygon.DataFormat.v20.Scene.Material material2 = new Simplygon.DataFormat.v20.Scene.Material();
        materialTable.Materials.Add(material2);
        material2.Id = material1.Value.Id.ToString();
        material2.Name = material1.Value.Name;
        material2.MaterialModel = (int) material1.Value.MaterialModel;
        material2.NormalMapType = (int) material1.Value.NormalMapType;
        foreach (Simplygon.Scene.Asset.MaterialChannel materialChannel1 in material1.Value.MaterialChannels)
        {
          Simplygon.DataFormat.v20.Scene.MaterialChannel materialChannel2 = new Simplygon.DataFormat.v20.Scene.MaterialChannel();
          materialChannel2.Color = new Simplygon.DataFormat.v20.DataType.Vector4(materialChannel1.Color.ToRGBAArray());
          materialChannel2.ChannelName = materialChannel1.ChannelName;
          materialChannel2.ShadingNetwork = materialChannel1.ShadingNetwork;
          materialChannel2.DeltagenColoredReflection = materialChannel1.DeltagenColoredReflection;
          materialChannel2.DeltagenFresnelExponent = materialChannel1.DeltagenFresnelExponent;
          materialChannel2.DeltagenFresnelMinimun = materialChannel1.DeltagenFresnelMinimum;
          materialChannel2.DeltagenFresnelScale = materialChannel1.DeltagenFresnelScale;
          materialChannel2.DeltagenNormalMapScale = materialChannel1.DeltagenNormalMapScale;
          materialChannel2.DeltagenReflectionExposure = materialChannel1.DeltagenReflectionExposure;
          materialChannel2.DeltagenReflectivity = materialChannel1.DeltagenReflectivity;
          materialChannel2.DeltagenShininess = materialChannel1.DeltagenShininess;
          materialChannel2.DeltagenTransparency = materialChannel1.DeltagenTransparency;
          materialChannel2.DeltagenUseClearCoat = materialChannel1.DeltagenUseClearCoat;
          materialChannel2.DeltagenUseFresnel = materialChannel1.DeltagenUseFresnel;
          materialChannel2.DeltagenUseReflection = materialChannel1.DeltagenUseReflection;
          foreach (Simplygon.Scene.Asset.MaterialChannelTextureDescriptor texture in materialChannel1.Textures)
            materialChannel2.Textures.Add(new Simplygon.DataFormat.v20.Scene.MaterialChannelTextureDescriptor()
            {
              TextureID = texture.Texture.ToString(),
              TexCoordSet = texture.TexCoordSet
            });
          material2.MaterialChannels.Add(materialChannel2);
        }
      }
    }
  }
}
