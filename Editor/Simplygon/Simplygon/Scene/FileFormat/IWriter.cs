﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.FileFormat.IWriter
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Scene.Common.Interfaces;
using Simplygon.Scene.Common.Progress;

namespace Simplygon.Scene.FileFormat
{
  internal interface IWriter
  {
    void Write(Simplygon.Scene.Scene scene, string filename, bool overwriteTextures = true, bool embedMedia = false, SceneProgress progress = null, IWorkDirectoryInfo workDirectory = null);
  }
}
