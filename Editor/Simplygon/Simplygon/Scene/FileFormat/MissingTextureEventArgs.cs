﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.FileFormat.MissingTextureEventArgs
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Scene.Asset;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Simplygon.Scene.FileFormat
{
  internal class MissingTextureEventArgs : EventArgs
  {
    public Texture MissingTexture { get; private set; }

    public ReadOnlyCollection<string> SearchedPaths { get; private set; }

    public MissingTextureEventArgs(Texture texture, List<string> searchedPaths)
    {
      this.MissingTexture = texture;
      this.SearchedPaths = new ReadOnlyCollection<string>((IList<string>) searchedPaths);
    }
  }
}
