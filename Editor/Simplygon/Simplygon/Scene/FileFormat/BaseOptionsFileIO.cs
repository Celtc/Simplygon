﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.FileFormat.BaseOptionsFileIO
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.Collections.Generic;

namespace Simplygon.Scene.FileFormat
{
  internal class BaseOptionsFileIO : IOptionsFileIO
  {
    protected Dictionary<string, bool> optionsFlags;

    public BaseOptionsFileIO()
    {
      this.optionsFlags = new Dictionary<string, bool>();
      foreach (string name in Enum.GetNames(typeof (BaseOptionsFileIO.DefaultReadSettings)))
        this.optionsFlags.Add(name, false);
    }

    public bool HasFlag(string key)
    {
      bool flag;
      return this.optionsFlags.TryGetValue(key, out flag);
    }

    public bool GetFlag(string key)
    {
      bool flag;
      if (this.optionsFlags.TryGetValue(key, out flag))
        return flag;
      return false;
    }

    public void SetFlag(string key, bool value)
    {
      if (!this.optionsFlags.ContainsKey(key))
        return;
      this.optionsFlags[key] = value;
    }

    public enum DefaultReadSettings
    {
      REPAIR_NORMALS = 1001, // 0x000003E9
      REPAIR_ONLY_INVALID = 1002, // 0x000003EA
      GENERATE_TANGENTS = 1003, // 0x000003EB
    }
  }
}
