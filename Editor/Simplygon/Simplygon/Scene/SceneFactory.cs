﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Scene.SceneFactory
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Scene
{
  internal static class SceneFactory
  {
    private static uint currentObjectIndex = 2048;

    public static void Reset()
    {
      SceneFactory.currentObjectIndex = 2048U;
    }

    public static uint RequestObjectIndex()
    {
      return SceneFactory.currentObjectIndex++;
    }
  }
}
