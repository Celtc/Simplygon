﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Hash.SHA256
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Simplygon.Hash
{
    internal class SHA256
    {
        public static string ComputeFile(string filename)
        {
            try
            {
                using (FileStream fileStream = new FileStream(filename, FileMode.Open))
                    return BitConverter.ToString(System.Security.Cryptography.SHA256.Create().ComputeHash((Stream)fileStream)).Replace("-", "").ToLower();
            }
            catch (Exception ex)
            {
            }
            return string.Empty;
        }

        public static string ComputeString(string value)
        {
            try
            {
                return BitConverter.ToString(System.Security.Cryptography.SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(value))).Replace("-", "").ToLower();
            }
            catch (Exception ex)
            {
            }
            return string.Empty;
        }

        public static string ComputeGuid(Guid value)
        {
            return SHA256.ComputeString(value.ToString());
        }
    }
}
