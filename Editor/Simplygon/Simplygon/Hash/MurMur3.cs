﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Hash.MurMur3
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.Hash
{
  internal class MurMur3
  {
    public static ulong READ_SIZE = 16;
    private static ulong C1 = 9782798678568883157;
    private static ulong C2 = 5545529020109919103;
    private ulong length;
    private uint seed;
    private ulong h1;
    private ulong h2;

    private void MixBody(ulong k1, ulong k2)
    {
      this.h1 ^= MurMur3.MixKey1(k1);
      this.h1 = this.h1.RotateLeft(27);
      this.h1 += this.h2;
      this.h1 = this.h1 * 5UL + 1390208809UL;
      this.h2 ^= MurMur3.MixKey2(k2);
      this.h2 = this.h2.RotateLeft(31);
      this.h2 += this.h1;
      this.h2 = this.h2 * 5UL + 944331445UL;
    }

    private static ulong MixKey1(ulong k1)
    {
      k1 *= MurMur3.C1;
      k1 = k1.RotateLeft(31);
      k1 *= MurMur3.C2;
      return k1;
    }

    private static ulong MixKey2(ulong k2)
    {
      k2 *= MurMur3.C2;
      k2 = k2.RotateLeft(33);
      k2 *= MurMur3.C1;
      return k2;
    }

    private static ulong MixFinal(ulong k)
    {
      k ^= k >> 33;
      k *= 18397679294719823053UL;
      k ^= k >> 33;
      k *= 14181476777654086739UL;
      k ^= k >> 33;
      return k;
    }

    public byte[] ComputeHash(byte[] bb)
    {
      this.ProcessBytes(bb);
      return this.Hash;
    }

    private void ProcessBytes(byte[] bb)
    {
      this.h1 = (ulong) this.seed;
      this.length = 0UL;
      int pos1 = 0;
      ulong length = (ulong) bb.Length;
      while (length >= MurMur3.READ_SIZE)
      {
        ulong uint64_1 = bb.GetUInt64(pos1);
        int pos2 = pos1 + 8;
        ulong uint64_2 = bb.GetUInt64(pos2);
        pos1 = pos2 + 8;
        this.length += MurMur3.READ_SIZE;
        length -= MurMur3.READ_SIZE;
        this.MixBody(uint64_1, uint64_2);
      }
      if (length <= 0UL)
        return;
      this.ProcessBytesRemaining(bb, length, pos1);
    }

    private void ProcessBytesRemaining(byte[] bb, ulong remaining, int pos)
    {
      ulong num1 = 0;
      ulong k2 = 0;
      this.length += remaining;
      long num2 = (long) remaining - 1L;
      if ((ulong) num2 <= 14UL)
      {
        ulong k1;
        switch ((uint) num2)
        {
          case 0:
            k1 = num1 ^ (ulong) bb[pos];
            break;
          case 1:
            num1 ^= (ulong) bb[pos + 1] << 8;
            goto case 0;
          case 2:
            num1 ^= (ulong) bb[pos + 2] << 16;
            goto case 1;
          case 3:
            num1 ^= (ulong) bb[pos + 3] << 24;
            goto case 2;
          case 4:
            num1 ^= (ulong) bb[pos + 4] << 32;
            goto case 3;
          case 5:
            num1 ^= (ulong) bb[pos + 5] << 40;
            goto case 4;
          case 6:
            num1 ^= (ulong) bb[pos + 6] << 48;
            goto case 5;
          case 7:
            k1 = num1 ^ bb.GetUInt64(pos);
            break;
          case 8:
            k2 ^= (ulong) bb[pos + 8];
            goto case 7;
          case 9:
            k2 ^= (ulong) bb[pos + 9] << 8;
            goto case 8;
          case 10:
            k2 ^= (ulong) bb[pos + 10] << 16;
            goto case 9;
          case 11:
            k2 ^= (ulong) bb[pos + 11] << 24;
            goto case 10;
          case 12:
            k2 ^= (ulong) bb[pos + 12] << 32;
            goto case 11;
          case 13:
            k2 ^= (ulong) bb[pos + 13] << 40;
            goto case 12;
          case 14:
            k2 ^= (ulong) bb[pos + 14] << 48;
            goto case 13;
          default:
            goto label_17;
        }
        this.h1 ^= MurMur3.MixKey1(k1);
        this.h2 ^= MurMur3.MixKey2(k2);
        return;
      }
label_17:
      throw new Exception("Something went wrong with remaining bytes calculation.");
    }

    public byte[] Hash
    {
      get
      {
        this.h1 ^= this.length;
        this.h2 ^= this.length;
        this.h1 += this.h2;
        this.h2 += this.h1;
        this.h1 = MurMur3.MixFinal(this.h1);
        this.h2 = MurMur3.MixFinal(this.h2);
        this.h1 += this.h2;
        this.h2 += this.h1;
        byte[] numArray = new byte[MurMur3.READ_SIZE];
        Array.Copy((Array) BitConverter.GetBytes(this.h1), 0, (Array) numArray, 0, 8);
        Array.Copy((Array) BitConverter.GetBytes(this.h2), 0, (Array) numArray, 8, 8);
        return numArray;
      }
    }
  }
}
