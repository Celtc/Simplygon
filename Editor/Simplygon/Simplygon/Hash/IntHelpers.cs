﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Hash.IntHelpers
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Hash
{
    internal static class IntHelpers
    {
        public static ulong RotateLeft(this ulong original, int bits)
        {
            return original << bits | original >> 64 - bits;
        }

        public static ulong RotateRight(this ulong original, int bits)
        {
            return original >> bits | original << 64 - bits;
        }

        public static unsafe ulong GetUInt64(this byte[] bb, int pos)
        {
            fixed (byte* numPtr = &bb[pos])
                return (ulong)*(long*)numPtr;
        }
    }
}
