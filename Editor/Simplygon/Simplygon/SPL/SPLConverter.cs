﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.SPLConverter
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json.Linq;
using System;

namespace Simplygon.SPL
{
  internal class SPLConverter : SPLCreationConverter<ISPL>
  {
    protected override ISPL Create(Type objectType, JObject jObject)
    {
      JObject jObject1 = SPLConverterUtility.GetObject("Header", jObject);
      if (jObject1 != null)
      {
        JToken token = SPLConverterUtility.GetToken("SPLVersion", jObject1);
        if (token != null)
        {
          Version version = new Version(token.ToString());
          if (version.Major == 1)
            return (ISPL) new Simplygon.SPL.v1.SPL();
          if (version.Major == 7 && version.Minor == 0)
            return (ISPL) new Simplygon.SPL.v70.SPL();
          if (version.Major == 8 && version.Minor == 0)
            return (ISPL) new Simplygon.SPL.v80.SPL();
        }
      }
      return (ISPL) null;
    }
  }
}
