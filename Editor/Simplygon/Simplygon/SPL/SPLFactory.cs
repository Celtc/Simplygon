﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.SPLFactory
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Globalization;

namespace Simplygon.SPL
{
  internal class SPLFactory
  {
    public static ISPL LoadSPLFromDirectory(string directory, out string loadedSPLFile, out string error)
    {
      loadedSPLFile = string.Empty;
      error = string.Empty;
      string[] files1 = Directory.GetFiles(directory, "*.spl");
      string[] files2 = Directory.GetFiles(directory, "*.splx");
      List<string> stringList = new List<string>();
      stringList.AddRange((IEnumerable<string>) files1);
      stringList.AddRange((IEnumerable<string>) files2);
      foreach (string file in stringList)
      {
        string error1 = string.Empty;
        ISPL spl = SPLFactory.LoadSPLFromFile(file, out error1);
        if (spl != null)
        {
          loadedSPLFile = file;
          return spl;
        }
        error = !string.IsNullOrEmpty(error) ? error + string.Format(CultureInfo.InvariantCulture, "\n{0} error: {1}", (object) file, (object) error1) : error + string.Format(CultureInfo.InvariantCulture, "{0} error: {1}", (object) file, (object) error1);
      }
      return (ISPL) null;
    }

    public static ISPL LoadSPLFromFile(string file, out string error)
    {
      error = string.Empty;
      string input = File.ReadAllText(file);
      string str = Path.GetExtension(file).ToLower().TrimStart('.');
      ISPL spl = (ISPL) null;
      if (str == "spl")
        spl = SPLFactory.LoadJson(input, out error);
      else if (str == "splx")
        spl = SPLFactory.LoadXml(input, out error);
      return spl;
    }

    private static ISPL LoadJson(string input, out string error)
    {
      error = string.Empty;
      try
      {
        ISPL spl = JsonConvert.DeserializeObject<ISPL>(input, new JsonConverter[1]
        {
          (JsonConverter) new SPLConverter()
        });
        if (spl != null)
          return spl;
      }
      catch (Exception ex)
      {
        error = ex.Message;
      }
      return (ISPL) null;
    }

    private static ISPL LoadXml(string input, out string error)
    {
      error = string.Empty;
      try
      {
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(input);
        ISPL spl = JsonConvert.DeserializeObject<ISPL>(JsonConvert.SerializeXmlNode((XmlNode) xmlDocument, Newtonsoft.Json.Formatting.Indented, true), new JsonConverter[2]
        {
          (JsonConverter) new SPLConverter(),
          (JsonConverter) new StringEnumConverter()
        });
        if (spl != null)
          return spl;
      }
      catch (Exception ex)
      {
        error = ex.Message;
      }
      return (ISPL) null;
    }
  }
}
