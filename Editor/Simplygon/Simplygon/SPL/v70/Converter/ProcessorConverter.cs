﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Converter.ProcessorConverter
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json.Linq;
using Simplygon.SPL.v70.Base;
using Simplygon.SPL.v70.Processor;
using System;

namespace Simplygon.SPL.v70.Converter
{
  internal class ProcessorConverter : SPLCreationConverter<BaseProcessor>
  {
    protected override BaseProcessor Create(Type objectType, JObject jObject)
    {
      JToken token = SPLConverterUtility.GetToken("Type", jObject);
      if (token != null)
      {
        string a = token.ToString();
        if (string.Equals(a, "ImpostorProcessor", StringComparison.OrdinalIgnoreCase))
          return (BaseProcessor) new ImpostorProcessor();
        if (string.Equals(a, "OcclusionMeshProcessor", StringComparison.OrdinalIgnoreCase))
          return (BaseProcessor) new OcclusionMeshProcessor();
        if (string.Equals(a, "ReductionProcessor", StringComparison.OrdinalIgnoreCase))
          return (BaseProcessor) new ReductionProcessor();
        if (string.Equals(a, "RemeshingProcessor", StringComparison.OrdinalIgnoreCase))
          return (BaseProcessor) new RemeshingProcessor();
        if (string.Equals(a, "AggregationProcessor", StringComparison.OrdinalIgnoreCase))
          return (BaseProcessor) new AggregationProcessor();
      }
      return (BaseProcessor) null;
    }
  }
}
