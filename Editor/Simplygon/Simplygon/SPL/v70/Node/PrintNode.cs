﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Node.PrintNode
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v70.Base;
using System.Collections.Generic;

namespace Simplygon.SPL.v70.Node
{
  internal class PrintNode : BaseNode
  {
    public int OnScreenSize { get; set; }

    public List<BaseMaterialCaster> MaterialCaster { get; set; }

    public int TextureResolution { get; set; }

    public PrintNode()
    {
      this.Type = nameof (PrintNode);
    }
  }
}
