﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Node.TexturePostProcessNode
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v70.Base;

namespace Simplygon.SPL.v70.Node
{
  internal class TexturePostProcessNode : BaseNode
  {
    public string TextureSize { get; set; }

    public int TextureQuality { get; set; }

    public string TextureDirectory { get; set; }

    public TexturePostProcessNode()
    {
      this.Type = nameof (TexturePostProcessNode);
    }
  }
}
