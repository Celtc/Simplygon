﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Settings.BaseSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v70.Settings
{
  internal abstract class BaseSettings : IDeepCopy<BaseSettings>
  {
    public bool Enabled { get; set; }

    public BaseSettings()
    {
      this.Enabled = true;
    }

    public BaseSettings DeepCopy()
    {
      return this.MemberwiseClone() as BaseSettings;
    }
  }
}
