﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Settings.AggregationSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v70.Settings
{
  internal class AggregationSettings : BaseSettings
  {
    public bool BaseAtlasOnOriginalTexCoords { get; set; }

    public string ProcessSelectionSetName { get; set; }

    public AggregationSettings()
    {
      this.BaseAtlasOnOriginalTexCoords = true;
      this.ProcessSelectionSetName = string.Empty;
    }
  }
}
