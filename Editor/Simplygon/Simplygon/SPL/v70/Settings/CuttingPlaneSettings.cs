﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Settings.CuttingPlaneSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v70.Settings
{
  internal class CuttingPlaneSettings : BaseSettings
  {
    public double PointX { get; set; }

    public double PointY { get; set; }

    public double PointZ { get; set; }

    public double NormalX { get; set; }

    public double NormalY { get; set; }

    public double NormalZ { get; set; }
  }
}
