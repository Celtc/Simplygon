﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Settings.BoneSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v70.Settings
{
  internal class BoneSettings : BaseSettings
  {
    public int BoneCount { get; set; }

    public double BoneRatio { get; set; }

    public uint BoneReductionTargets { get; set; }

    public string ForceBoneRemovalSelectionSetName { get; set; }

    public string KeepBoneSelectionSetName { get; set; }

    public bool LimitBonesPerVertex { get; set; }

    public int MaxBonesPerVertex { get; set; }

    public double MaxDeviation { get; set; }

    public int OnScreenSize { get; set; }

    public bool RemoveUnusedBones { get; set; }

    public int StopCondition { get; set; }

    public bool UseBoneReducer { get; set; }

    public BoneSettings()
    {
      this.BoneRatio = 1.0;
      this.BoneCount = int.MaxValue;
      this.OnScreenSize = int.MaxValue;
      this.MaxDeviation = 0.0;
      this.MaxBonesPerVertex = 0;
      this.LimitBonesPerVertex = false;
      this.RemoveUnusedBones = false;
      this.StopCondition = 1;
      this.BoneReductionTargets = uint.MaxValue;
      this.ForceBoneRemovalSelectionSetName = string.Empty;
      this.KeepBoneSelectionSetName = string.Empty;
    }
  }
}
