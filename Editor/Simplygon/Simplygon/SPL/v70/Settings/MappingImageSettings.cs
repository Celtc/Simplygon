﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Settings.MappingImageSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v70.Settings
{
  internal class MappingImageSettings : BaseSettings
  {
    public double AutomaticTextureSizeMultiplier { get; set; }

    public int ChartAggregatorMode { get; set; }

    public int ChartAggregatorOriginalTexCoordLevel { get; set; }

    public bool ChartAggregatorUseAreaWeighting { get; set; }

    public bool ChartAggregatorKeepOriginalChartProportions { get; set; }

    public string ChartAggregatorKeepOriginalChartProportionsFromChannel { get; set; }

    public bool ChartAggregatorKeepOriginalChartSizes { get; set; }

    public bool ChartAggregatorSeparateOverlappingCharts { get; set; }

    public bool ForcePower2Texture { get; set; }

    public bool GenerateMappingImage { get; set; }

    public bool GenerateTangents { get; set; }

    public bool GenerateTexCoords { get; set; }

    public int GutterSpace { get; set; }

    public int Height { get; set; }

    public int MaximumLayers { get; set; }

    public int MultisamplingLevel { get; set; }

    public double ParameterizerMaxStretch { get; set; }

    public bool ParameterizerUseVertexWeights { get; set; }

    public bool ParameterizerUseVisibilityWeights { get; set; }

    public int TexCoordGeneratorType { get; set; }

    public int TexCoordLevel { get; set; }

    public bool UseAutomaticTextureSize { get; set; }

    public bool UseFullRetexturing { get; set; }

    public int Width { get; set; }

    public MappingImageSettings()
    {
      this.GenerateMappingImage = false;
      this.GenerateTexCoords = false;
      this.GenerateTangents = false;
      this.TexCoordLevel = (int) byte.MaxValue;
      this.UseFullRetexturing = false;
      this.MaximumLayers = 1;
      this.UseAutomaticTextureSize = false;
      this.AutomaticTextureSizeMultiplier = 1.0;
      this.ForcePower2Texture = false;
      this.Height = 512;
      this.Width = 512;
      this.GutterSpace = 1;
      this.MultisamplingLevel = 2;
      this.TexCoordGeneratorType = 0;
      this.ParameterizerMaxStretch = 0.2;
      this.ParameterizerUseVisibilityWeights = false;
      this.ParameterizerUseVertexWeights = false;
      this.ChartAggregatorMode = 0;
      this.ChartAggregatorOriginalTexCoordLevel = 0;
      this.ChartAggregatorUseAreaWeighting = true;
      this.ChartAggregatorSeparateOverlappingCharts = false;
      this.ChartAggregatorKeepOriginalChartProportions = true;
      this.ChartAggregatorKeepOriginalChartSizes = false;
      this.ChartAggregatorKeepOriginalChartProportionsFromChannel = string.Empty;
    }
  }
}
