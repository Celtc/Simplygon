﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Settings.VisibilitySettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v70.Settings
{
  internal class VisibilitySettings : BaseSettings
  {
    public bool UseCustomVisibilitySphere { get; set; }

    public int CustomVisibilitySphereFidelity { get; set; }

    public double CustomVisibilitySphereYaw { get; set; }

    public double CustomVisibilitySpherePitch { get; set; }

    public double CustomVisibilitySphereCoverage { get; set; }

    public string CameraSelectionSetName { get; set; }

    public bool CullOccludedGeometry { get; set; }

    public bool ForceVisibilityCalculation { get; set; }

    public string OccluderSelectionSetName { get; set; }

    public bool UseBackfaceCulling { get; set; }

    public bool UseVisibilityWeightsInReducer { get; set; }

    public bool UseVisibilityWeightsInTexcoordGenerator { get; set; }

    public double VisibilityWeightsPower { get; set; }

    public VisibilitySettings()
    {
      this.UseVisibilityWeightsInReducer = false;
      this.UseVisibilityWeightsInTexcoordGenerator = false;
      this.VisibilityWeightsPower = 1.0;
      this.CullOccludedGeometry = false;
      this.UseBackfaceCulling = true;
      this.ForceVisibilityCalculation = false;
      this.CameraSelectionSetName = string.Empty;
      this.OccluderSelectionSetName = string.Empty;
      this.UseCustomVisibilitySphere = false;
      this.CustomVisibilitySphereFidelity = 3;
      this.CustomVisibilitySphereYaw = 0.0;
      this.CustomVisibilitySpherePitch = 0.0;
      this.CustomVisibilitySphereCoverage = 180.0;
    }
  }
}
