﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Settings.RemeshingSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.SPL.v70.Settings
{
  internal class RemeshingSettings : BaseSettings
  {
    public int CuttingPlaneSelectionSetName { get; set; }

    public double EmptySpaceOverride { get; set; }

    public double HardEdgeAngleInRadians { get; set; }

    public int MaxTriangleSize { get; set; }

    public int MergeDistance { get; set; }

    public int OnScreenSize { get; set; }

    public string ProcessSelectionSetName { get; set; }

    public int SurfaceTransferMode { get; set; }

    public bool TransferColors { get; set; }

    public bool TransferNormals { get; set; }

    public bool UseCuttingPlanes { get; set; }

    public bool UseEmptySpaceOverride { get; set; }

    public RemeshingSettings()
    {
      this.OnScreenSize = 100;
      this.MaxTriangleSize = 0;
      this.HardEdgeAngleInRadians = 4.0 * System.Math.PI / 9.0;
      this.TransferNormals = false;
      this.TransferColors = false;
      this.MergeDistance = 4;
      this.UseEmptySpaceOverride = false;
      this.UseCuttingPlanes = false;
      this.SurfaceTransferMode = 1;
      this.ProcessSelectionSetName = string.Empty;
    }
  }
}
