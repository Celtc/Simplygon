﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Settings.NormalCalculationSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.SPL.v70.Settings
{
  internal class NormalCalculationSettings : BaseSettings
  {
    public double HardEdgeAngleInRadians { get; set; }

    public bool RepairInvalidNormals { get; set; }

    public bool ReplaceNormals { get; set; }

    public bool ScaleByAngle { get; set; }

    public bool ScaleByArea { get; set; }

    public NormalCalculationSettings()
    {
      this.HardEdgeAngleInRadians = 4.0 * System.Math.PI / 9.0;
      this.RepairInvalidNormals = false;
      this.ReplaceNormals = false;
      this.ScaleByArea = true;
      this.ScaleByAngle = true;
    }
  }
}
