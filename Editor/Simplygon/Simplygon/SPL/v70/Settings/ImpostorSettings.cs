﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Settings.ImpostorSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json;

namespace Simplygon.SPL.v70.Settings
{
  internal class ImpostorSettings : BaseSettings
  {
    [JsonConverter(typeof (SPLVector3Converter))]
    public Vector3 ViewDirection { get; set; }

    public double DepthOffset { get; set; }

    public bool UseTightFitting { get; set; }

    public ImpostorSettings()
    {
      this.DepthOffset = 0.0;
      this.UseTightFitting = false;
    }
  }
}
