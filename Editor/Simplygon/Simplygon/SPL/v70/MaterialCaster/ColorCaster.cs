﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.MaterialCaster.ColorCaster
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v70.Base;

namespace Simplygon.SPL.v70.MaterialCaster
{
  internal class ColorCaster : BaseMaterialCaster
  {
    public bool BakeOpacityInAlpha { get; set; }

    public string ColorType { get; set; }

    public int Dilation { get; set; }

    public int FillMode { get; set; }

    public bool IsSRGB { get; set; }

    public int OutputChannelBitDepth { get; set; }

    public int OutputChannels { get; set; }

    public ColorCaster()
    {
      this.Type = nameof (ColorCaster);
      this.ColorType = "Diffuse";
      this.OutputChannelBitDepth = 8;
      this.OutputChannels = 4;
      this.FillMode = 0;
    }
  }
}
