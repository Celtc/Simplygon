﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.SPL
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json;
using Simplygon.Hash;
using Simplygon.SPL.v70.Base;
using Simplygon.SPL.v70.Converter;
using Simplygon.SPL.v70.Node;
using Simplygon.SPL.v70.Processor;
using System.Collections.Generic;
using System.IO;
using System.Globalization;

namespace Simplygon.SPL.v70
{
  internal class SPL : ISPL
  {
    public IHeader Header { get; set; }

    public BaseNode ProcessGraph { get; set; }

    public SPL()
    {
      this.Header = (IHeader) new Simplygon.SPL.v70.Header();
    }

    public int LODCount()
    {
      return this.LODCount(this.ProcessGraph);
    }

    private int LODCount(BaseNode node)
    {
      if (node == null)
        return 0;
      int num = 0;
      if (node is ProcessNode)
        num = 1;
      else if (node is WebGLNode)
        num = 1;
      else if (node is WebGLConfiguratorNode)
        num = 1;
      else if (node is ProcessNode)
        num = 1;
      foreach (BaseNode child in node.Children)
        num += this.LODCount(child);
      return num;
    }

    public bool IsMaterialLODEnabled()
    {
      return this.IsMaterialLODEnabled(this.ProcessGraph);
    }

    public bool IsMaterialLODEnabled(int lodIndex)
    {
      bool flag = false;
      if (lodIndex >= 1)
      {
        int startIndex = 1;
        flag = this.IsMaterialLODEnabled(this.ProcessGraph, lodIndex, ref startIndex);
      }
      return flag;
    }

    private bool IsMaterialLODEnabled(BaseNode node)
    {
      if (node == null)
        return false;
      if (node is ProcessNode)
      {
        ProcessNode processNode = node as ProcessNode;
        bool flag = false;
        if (processNode.Processor is ReductionProcessor)
        {
          if (((ReductionProcessor) processNode.Processor).MappingImageSettings.Enabled)
            flag = true;
        }
        else if (processNode.Processor is RemeshingProcessor)
        {
          if (((RemeshingProcessor) processNode.Processor).MappingImageSettings.Enabled)
            flag = true;
        }
        else if (processNode.Processor is AggregationProcessor && ((AggregationProcessor) processNode.Processor).MappingImageSettings.Enabled)
          flag = true;
        if (processNode.MaterialCaster.Count > 0 & flag)
          return true;
      }
      foreach (BaseNode child in node.Children)
      {
        if (this.IsMaterialLODEnabled(child))
          return true;
      }
      return false;
    }

    private bool IsMaterialLODEnabled(BaseNode node, int lodIndex, ref int startIndex)
    {
      if (startIndex > lodIndex || node == null)
        return false;
      if (node is ProcessNode)
      {
        ProcessNode processNode = node as ProcessNode;
        bool flag = false;
        if (processNode.Processor is ReductionProcessor)
        {
          if (((ReductionProcessor) processNode.Processor).MappingImageSettings.Enabled)
            flag = true;
        }
        else if (processNode.Processor is RemeshingProcessor)
        {
          if (((RemeshingProcessor) processNode.Processor).MappingImageSettings.Enabled)
            flag = true;
        }
        else if (processNode.Processor is AggregationProcessor && ((AggregationProcessor) processNode.Processor).MappingImageSettings.Enabled)
          flag = true;
        if (((lodIndex != startIndex ? 0 : (processNode.MaterialCaster.Count > 0 ? 1 : 0)) & (flag ? 1 : 0)) != 0)
          return true;
        ++startIndex;
      }
      foreach (BaseNode child in node.Children)
      {
        if (this.IsMaterialLODEnabled(child, lodIndex, ref startIndex))
          return true;
      }
      return false;
    }

    public bool IsMultithreadedProcess()
    {
      return this.IsMultithreadedProcess(this.ProcessGraph);
    }

    protected bool IsMultithreadedProcess(BaseNode splNode)
    {
      if (splNode is ProcessNode && ((splNode as ProcessNode).Processor.Type == "RemeshingProcessor" || (splNode as ProcessNode).Processor.Type == "AggregationProcessor") || (splNode is WebGLNode || splNode is WebGLConfiguratorNode || splNode is PrintNode))
        return true;
      foreach (BaseNode child in splNode.Children)
      {
        bool flag = this.IsMultithreadedProcess(child);
        if (flag)
          return flag;
      }
      return false;
    }

    public List<ProcessNode> GetProcessNodes()
    {
      List<ProcessNode> processNodes = new List<ProcessNode>();
      this.GetProcessNodes(this.ProcessGraph, processNodes);
      return processNodes;
    }

    protected void GetProcessNodes(BaseNode splNode, List<ProcessNode> processNodes)
    {
      if (splNode is ProcessNode)
        processNodes.Add(splNode as ProcessNode);
      foreach (BaseNode child in splNode.Children)
        this.GetProcessNodes(child, processNodes);
    }

    public List<string> GetLODFilePaths(string rootDirectory = "")
    {
      List<string> lodFilePaths = new List<string>();
      this.GetLodFilePaths(this.ProcessGraph, rootDirectory, lodFilePaths);
      return lodFilePaths;
    }

    protected void GetLodFilePaths(BaseNode splNode, string directory, List<string> lodFilePaths)
    {
      directory = Path.Combine(directory, splNode.Name);
      if (splNode is WriteNode)
      {
        WriteNode writeNode = splNode as WriteNode;
        string str = Path.Combine(directory, string.Format(CultureInfo.InvariantCulture, "output.{0}", (object) writeNode.Format));
        lodFilePaths.Add(str);
      }
      foreach (BaseNode child in splNode.Children)
        this.GetLodFilePaths(child, directory, lodFilePaths);
    }

    public string GetSPLHash()
    {
      return SHA256.ComputeString(JsonConvert.SerializeObject((object) this, Formatting.Indented));
    }

    public void Save(string filename)
    {
      string contents = JsonConvert.SerializeObject((object) this, Formatting.Indented);
      File.WriteAllText(filename, contents);
    }

    public static Simplygon.SPL.v70.SPL Load(string filename)
    {
      return JsonConvert.DeserializeObject<ISPL>(File.ReadAllText(filename), (JsonConverter) new SPLConverter(), (JsonConverter) new MaterialCasterConverter(), (JsonConverter) new NodeConverter(), (JsonConverter) new ProcessorConverter()) as Simplygon.SPL.v70.SPL;
    }
  }
}
