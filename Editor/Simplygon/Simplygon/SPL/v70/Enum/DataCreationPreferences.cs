﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Enum.DataCreationPreferences
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v70.Enum
{
  internal enum DataCreationPreferences
  {
    SG_DATACREATIONPREFERENCES_ONLY_USE_ORIGINAL_DATA,
    SG_DATACREATIONPREFERENCES_PREFER_ORIGINAL_DATA,
    SG_DATACREATIONPREFERENCES_PREFER_OPTIMIZED_RESULT,
  }
}
