﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Base.BaseProcessor
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v70.Base
{
  internal abstract class BaseProcessor : IDeepCopy<BaseProcessor>
  {
    public string Type { get; protected set; }

    public BaseProcessor()
    {
      this.Type = string.Empty;
    }

    public BaseProcessor DeepCopy()
    {
      return this.InternalDeepCopy();
    }

    protected abstract BaseProcessor InternalDeepCopy();
  }
}
