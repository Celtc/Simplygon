﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Base.BaseMaterialCaster
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v70.Base
{
  internal abstract class BaseMaterialCaster : IDeepCopy<BaseMaterialCaster>
  {
    public string Type { get; protected set; }

    public string Name { get; set; }

    public string Channel { get; set; }

    public int DitherType { get; set; }

    public BaseMaterialCaster()
    {
      this.Type = string.Empty;
      this.Name = string.Empty;
      this.Channel = string.Empty;
      this.DitherType = 0;
    }

    public BaseMaterialCaster DeepCopy()
    {
      return this.MemberwiseClone() as BaseMaterialCaster;
    }
  }
}
