﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Base.BaseNode
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.Collections.Generic;

namespace Simplygon.SPL.v70.Base
{
  internal class BaseNode
  {
    public string Type { get; protected set; }

    public string Name { get; set; }

    public List<BaseNode> Children { get; set; }

    public BaseNode()
    {
      this.Type = string.Empty;
      this.Name = "Node";
      this.Children = new List<BaseNode>();
    }

    public BaseNode DeepCopy()
    {
      BaseNode baseNode = this.MemberwiseClone() as BaseNode;
      baseNode.Children = new List<BaseNode>();
      foreach (BaseNode child in this.Children)
        baseNode.Children.Add(child.DeepCopy());
      return baseNode;
    }
  }
}
