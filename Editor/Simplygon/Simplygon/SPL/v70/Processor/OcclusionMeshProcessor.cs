﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Processor.OcclusionMeshProcessor
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v70.Base;
using Simplygon.SPL.v70.Settings;

namespace Simplygon.SPL.v70.Processor
{
  internal class OcclusionMeshProcessor : BaseProcessor
  {
    public OcclusionMeshSettings OcclusionMeshSettings { get; set; }

    public OcclusionMeshProcessor()
    {
      this.Type = nameof (OcclusionMeshProcessor);
      this.OcclusionMeshSettings = new OcclusionMeshSettings();
    }

    protected override BaseProcessor InternalDeepCopy()
    {
      OcclusionMeshProcessor occlusionMeshProcessor = this.MemberwiseClone() as OcclusionMeshProcessor;
      occlusionMeshProcessor.OcclusionMeshSettings = this.OcclusionMeshSettings.DeepCopy() as OcclusionMeshSettings;
      return (BaseProcessor) occlusionMeshProcessor;
    }
  }
}
