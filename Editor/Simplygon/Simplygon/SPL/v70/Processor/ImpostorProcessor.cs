﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v70.Processor.ImpostorProcessor
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v70.Base;
using Simplygon.SPL.v70.Settings;

namespace Simplygon.SPL.v70.Processor
{
  internal class ImpostorProcessor : BaseProcessor
  {
    public ImpostorSettings ImpostorSettings { get; set; }

    public MappingImageSettings MappingImageSettings { get; set; }

    public ImpostorProcessor()
    {
      this.Type = nameof (ImpostorProcessor);
      this.ImpostorSettings = new ImpostorSettings();
      this.MappingImageSettings = new MappingImageSettings();
    }

    protected override BaseProcessor InternalDeepCopy()
    {
      ImpostorProcessor impostorProcessor = this.MemberwiseClone() as ImpostorProcessor;
      impostorProcessor.ImpostorSettings = this.ImpostorSettings.DeepCopy() as ImpostorSettings;
      impostorProcessor.MappingImageSettings = this.MappingImageSettings.DeepCopy() as MappingImageSettings;
      return (BaseProcessor) impostorProcessor;
    }
  }
}
