﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.IHeader
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL
{
  internal interface IHeader
  {
    string SPLVersion { get; set; }

    string ClientName { get; set; }

    string ClientVersion { get; set; }

    string SimplygonVersion { get; set; }
  }
}
