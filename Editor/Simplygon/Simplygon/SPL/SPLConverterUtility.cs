﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.SPLConverterUtility
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json.Linq;
using System;

namespace Simplygon.SPL
{
  internal static class SPLConverterUtility
  {
    public static JObject GetObject(string fieldName, JObject jObject)
    {
      return jObject.GetValue(fieldName, StringComparison.OrdinalIgnoreCase) as JObject;
    }
            
    public static JToken GetToken(string fieldName, JObject jObject)
    {
      return jObject.GetValue(fieldName, StringComparison.OrdinalIgnoreCase);
    }
  }
}
