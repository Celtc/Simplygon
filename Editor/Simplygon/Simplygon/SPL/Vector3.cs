﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.Vector3
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.Globalization;

namespace Simplygon.SPL
{
  internal struct Vector3
  {
    public double X { get; set; }

    public double Y { get; set; }

    public double Z { get; set; }

    public override string ToString()
    {
      return string.Format(CultureInfo.InvariantCulture, "{0}.{1}.{2}", (object) this.X, (object) this.Y, (object) this.Z);
    }
  }
}
