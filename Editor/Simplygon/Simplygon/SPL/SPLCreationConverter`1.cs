﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.SPLCreationConverter`1
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Simplygon.SPL
{
  internal abstract class SPLCreationConverter<T> : JsonConverter
  {
    protected abstract T Create(Type objectType, JObject jObject);

    public override bool CanConvert(Type objectType)
    {
      return typeof (T).IsAssignableFrom(objectType);
    }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
      JObject jObject = JObject.Load(reader);
      T obj = this.Create(objectType, jObject);
      if ((object) obj != null)
        serializer.Populate(jObject.CreateReader(), (object) obj);
      return (object) obj;
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
      throw new NotImplementedException();
    }

    private JObject GetObject(string fieldName, JObject jObject)
    {
      return jObject.GetValue(fieldName, StringComparison.OrdinalIgnoreCase) as JObject;
    }

    private JToken GetToken(string fieldName, JObject jObject)
    {
      return jObject.GetValue(fieldName, StringComparison.OrdinalIgnoreCase);
    }
  }
}
