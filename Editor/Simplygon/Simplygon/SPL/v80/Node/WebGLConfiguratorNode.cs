﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Node.WebGLConfiguratorNode
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Base;
using System.Collections.Generic;

namespace Simplygon.SPL.v80.Node
{
  internal class WebGLConfiguratorNode : BaseNode
  {
    public int OnScreenSize { get; set; }

    public List<BaseMaterialCaster> MaterialCaster { get; set; }

    public int TextureResolution { get; set; }

    public bool UseBoundingBoxCoordinates { get; set; }

    public float VisibilityCoverage { get; set; }

    public float VisibilityCenterX { get; set; }

    public float VisibilityCenterY { get; set; }

    public float VisibilityCenterZ { get; set; }

    public float InfX { get; set; }

    public float InfY { get; set; }

    public float InfZ { get; set; }

    public float SupX { get; set; }

    public float SupY { get; set; }

    public float SupZ { get; set; }

    public float UVStretch { get; set; }

    public int GutterSpace { get; set; }

    public WebGLConfiguratorNode()
    {
      this.Type = nameof (WebGLConfiguratorNode);
      this.UVStretch = 0.5f;
      this.GutterSpace = 2;
      this.VisibilityCoverage = 180f;
      this.UseBoundingBoxCoordinates = false;
    }
  }
}
