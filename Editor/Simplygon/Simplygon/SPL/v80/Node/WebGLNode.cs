﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Node.WebGLNode
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Base;
using System.Collections.Generic;

namespace Simplygon.SPL.v80.Node
{
  internal class WebGLNode : BaseNode
  {
    public int OnScreenSize { get; set; }

    public List<BaseMaterialCaster> MaterialCaster { get; set; }

    public int TextureResolution { get; set; }

    public WebGLNode()
    {
      this.Type = nameof (WebGLNode);
    }
  }
}
