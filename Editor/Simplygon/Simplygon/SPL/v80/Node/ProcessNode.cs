﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Node.ProcessNode
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Base;
using Simplygon.SPL.v80.Enum;
using System.Collections.Generic;

namespace Simplygon.SPL.v80.Node
{
  internal class ProcessNode : BaseNode
  {
    public BaseProcessor Processor { get; set; }

    public List<BaseMaterialCaster> MaterialCaster { get; set; }

    public string OutputMaterialName { get; set; }

    public TangentSpaceMethod DefaultTBNType { get; set; }

    public bool AllowGPUAcceleration { get; set; }

    public ProcessNode()
    {
      this.Type = nameof (ProcessNode);
      this.OutputMaterialName = "SimplygonMaterial";
      this.MaterialCaster = new List<BaseMaterialCaster>();
      this.DefaultTBNType = TangentSpaceMethod.SG_TANGENTSPACEMETHOD_ORTHONORMAL;
      this.AllowGPUAcceleration = false;
    }

    public new BaseNode DeepCopy()
    {
      ProcessNode processNode = base.DeepCopy() as ProcessNode;
      processNode.Processor = this.Processor.DeepCopy();
      processNode.MaterialCaster = new List<BaseMaterialCaster>();
      foreach (BaseMaterialCaster baseMaterialCaster in this.MaterialCaster)
        processNode.MaterialCaster.Add(baseMaterialCaster.DeepCopy());
      return (BaseNode) processNode;
    }
  }
}
