﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Enum.ValidationFrequency
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v80.Enum
{
  internal enum ValidationFrequency : uint
  {
    SG_VALIDATIONFREQUENCY_NORMAL,
    SG_VALIDATIONFREQUENCY_CORE,
    SG_VALIDATIONFREQUENCY_ULTRA,
  }
}
