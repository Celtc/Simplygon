﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Enum.DitherType
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v80.Enum
{
  internal enum DitherType : uint
  {
    SG_DITHERPATTERNS_NO_DITHER,
    SG_DITHERPATTERNS_FLOYDSTEINBERG,
    SG_DITHERPATTERNS_JARVISJUDICENINKE,
    SG_DITHERPATTERNS_SIERRA,
  }
}
