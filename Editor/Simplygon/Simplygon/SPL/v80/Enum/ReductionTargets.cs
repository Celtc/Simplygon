﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Enum.ReductionTargets
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v80.Enum
{
  internal enum ReductionTargets : uint
  {
    SG_REDUCTIONTARGET_TRIANGLECOUNT = 1,
    SG_REDUCTIONTARGET_TRIANGLERATIO = 2,
    SG_REDUCTIONTARGET_MAXDEVIATION = 4,
    SG_REDUCTIONTARGET_ONSCREENSIZE = 8,
    SG_REDUCTIONTARGET_ALL = 4294967295, // 0xFFFFFFFF
  }
}
