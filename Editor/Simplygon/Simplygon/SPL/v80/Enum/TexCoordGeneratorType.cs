﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Enum.TexCoordGeneratorType
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v80.Enum
{
  internal enum TexCoordGeneratorType : uint
  {
    SG_TEXCOORDGENERATORTYPE_PARAMETERIZER,
    SG_TEXCOORDGENERATORTYPE_CHARTAGGREGATOR,
    SG_TEXCOORDGENERATORTYPE_PARAMETERIZER_NEW,
  }
}
