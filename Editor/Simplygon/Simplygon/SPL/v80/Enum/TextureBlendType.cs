﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Enum.TextureBlendType
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v80.Enum
{
  internal enum TextureBlendType : uint
  {
    SG_TEXTUREBLEND_REPLACE,
    SG_TEXTUREBLEND_ADD,
    SG_TEXTUREBLEND_SUBTRACT,
    SG_TEXTUREBLEND_MULTIPLY,
    SG_TEXTUREBLEND_ALPHA,
    SG_TEXTUREBLEND_PRE_MULTIPLIED_ALPHA,
    SG_TEXTUREBLEND_OVER,
    SG_TEXTUREBLEND_IN,
    SG_TEXTUREBLEND_OUT,
    SG_TEXTUREBLEND_ADD_W_ALPHA,
    SG_TEXTUREBLEND_SUBTRACT_W_ALPHA,
    SG_TEXTUREBLEND_MULTIPLY_W_ALPHA,
  }
}
