﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Header
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v80
{
  internal class Header : IHeader
  {
    public string SPLVersion { get; set; }

    public string ClientName { get; set; }

    public string ClientVersion { get; set; }

    public string SimplygonVersion { get; set; }

    public Header()
    {
      this.SPLVersion = "8.0";
      this.ClientName = string.Empty;
      this.ClientVersion = string.Empty;
      this.SimplygonVersion = string.Empty;
    }
  }
}
