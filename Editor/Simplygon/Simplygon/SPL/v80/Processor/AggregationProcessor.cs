﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Processor.AggregationProcessor
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Base;
using Simplygon.SPL.v80.Settings;

namespace Simplygon.SPL.v80.Processor
{
  internal class AggregationProcessor : BaseProcessor
  {
    public AggregationSettings AggregationSettings { get; set; }

    public MappingImageSettings MappingImageSettings { get; set; }

    public VisibilitySettings VisibilitySettings { get; set; }

    public AggregationProcessor()
    {
      this.Type = nameof (AggregationProcessor);
      this.MappingImageSettings = new MappingImageSettings();
      this.AggregationSettings = new AggregationSettings();
      this.VisibilitySettings = new VisibilitySettings();
    }

    protected override BaseProcessor InternalDeepCopy()
    {
      AggregationProcessor aggregationProcessor = this.MemberwiseClone() as AggregationProcessor;
      aggregationProcessor.AggregationSettings = this.AggregationSettings.DeepCopy() as AggregationSettings;
      aggregationProcessor.MappingImageSettings = this.MappingImageSettings.DeepCopy() as MappingImageSettings;
      aggregationProcessor.VisibilitySettings = this.VisibilitySettings.DeepCopy() as VisibilitySettings;
      return (BaseProcessor) aggregationProcessor;
    }
  }
}
