﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Processor.ReductionProcessor
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Base;
using Simplygon.SPL.v80.Settings;

namespace Simplygon.SPL.v80.Processor
{
  internal class ReductionProcessor : BaseProcessor
  {
    public ReductionSettings ReductionSettings { get; set; }

    public MappingImageSettings MappingImageSettings { get; set; }

    public NormalCalculationSettings NormalCalculationSettings { get; set; }

    public RepairSettings RepairSettings { get; set; }

    public VisibilitySettings VisibilitySettings { get; set; }

    public BoneSettings BoneSettings { get; set; }

    public ReductionProcessor()
    {
      this.Type = nameof (ReductionProcessor);
      this.BoneSettings = new BoneSettings();
      this.MappingImageSettings = new MappingImageSettings();
      this.NormalCalculationSettings = new NormalCalculationSettings();
      this.ReductionSettings = new ReductionSettings();
      this.RepairSettings = new RepairSettings();
      this.VisibilitySettings = new VisibilitySettings();
    }

    protected override BaseProcessor InternalDeepCopy()
    {
      ReductionProcessor reductionProcessor = this.MemberwiseClone() as ReductionProcessor;
      reductionProcessor.ReductionSettings = this.ReductionSettings.DeepCopy() as ReductionSettings;
      reductionProcessor.MappingImageSettings = this.MappingImageSettings.DeepCopy() as MappingImageSettings;
      reductionProcessor.NormalCalculationSettings = this.NormalCalculationSettings.DeepCopy() as NormalCalculationSettings;
      reductionProcessor.RepairSettings = this.RepairSettings.DeepCopy() as RepairSettings;
      reductionProcessor.VisibilitySettings = this.VisibilitySettings.DeepCopy() as VisibilitySettings;
      reductionProcessor.BoneSettings = this.BoneSettings.DeepCopy() as BoneSettings;
      return (BaseProcessor) reductionProcessor;
    }
  }
}
