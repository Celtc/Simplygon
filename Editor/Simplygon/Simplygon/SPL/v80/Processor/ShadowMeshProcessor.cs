﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Processor.ShadowMeshProcessor
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Base;
using Simplygon.SPL.v80.Settings;

namespace Simplygon.SPL.v80.Processor
{
  internal class ShadowMeshProcessor : BaseProcessor
  {
    public ShadowMeshSettings ShadowMeshSettings { get; set; }

    public ShadowMeshProcessor()
    {
      this.Type = nameof (ShadowMeshProcessor);
      this.ShadowMeshSettings = new ShadowMeshSettings();
    }

    protected override BaseProcessor InternalDeepCopy()
    {
      ShadowMeshProcessor shadowMeshProcessor = this.MemberwiseClone() as ShadowMeshProcessor;
      shadowMeshProcessor.ShadowMeshSettings = this.ShadowMeshSettings.DeepCopy() as ShadowMeshSettings;
      return (BaseProcessor) shadowMeshProcessor;
    }
  }
}
