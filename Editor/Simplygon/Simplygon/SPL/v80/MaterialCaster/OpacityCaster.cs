﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.MaterialCaster.OpacityCaster
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Base;
using Simplygon.SPL.v80.Enum;

namespace Simplygon.SPL.v80.MaterialCaster
{
  internal class OpacityCaster : BaseMaterialCaster
  {
    public bool BakeOpacityInAlpha { get; set; }

    public bool BakeVertexColors { get; set; }

    public string ColorType { get; set; }

    public int Dilation { get; set; }

    public DitherType DitherType { get; set; }

    public FillMode FillMode { get; set; }

    public int OutputChannelBitDepth { get; set; }

    public int OutputChannels { get; set; }

    public TextureFormat OutputTextureFormat { get; set; }

    public OpacityCaster()
    {
      this.Type = nameof (OpacityCaster);
      this.BakeOpacityInAlpha = false;
      this.BakeVertexColors = false;
      this.ColorType = "";
      this.Dilation = 10;
      this.DitherType = DitherType.SG_DITHERPATTERNS_FLOYDSTEINBERG;
      this.FillMode = FillMode.SG_ATLASFILLMODE_INTERPOLATE;
      this.OutputChannelBitDepth = 8;
      this.OutputChannels = 4;
      this.OutputTextureFormat = TextureFormat.png;
    }
  }
}
