﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.MaterialCaster.AmbientOcclusionCaster
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Base;
using Simplygon.SPL.v80.Enum;

namespace Simplygon.SPL.v80.MaterialCaster
{
  internal class AmbientOcclusionCaster : BaseMaterialCaster
  {
    public string ColorType { get; set; }

    public int Dilation { get; set; }

    public DitherType DitherType { get; set; }

    public double OcclusionFalloff { get; set; }

    public double OcclusionMultiplier { get; set; }

    public int OutputChannelBitDepth { get; set; }

    public int OutputChannels { get; set; }

    public int RaysPerPixel { get; set; }

    public bool UseSimpleOcclusionMode { get; set; }

    public TextureFormat OutputTextureFormat { get; set; }

    public AmbientOcclusionCaster()
    {
      this.Type = nameof (AmbientOcclusionCaster);
      this.ColorType = "Diffuse";
      this.Dilation = 10;
      this.DitherType = DitherType.SG_DITHERPATTERNS_FLOYDSTEINBERG;
      this.OcclusionFalloff = -1.0;
      this.OcclusionMultiplier = -1.0;
      this.OutputChannelBitDepth = 8;
      this.OutputChannels = 4;
      this.RaysPerPixel = 256;
      this.UseSimpleOcclusionMode = false;
      this.OutputTextureFormat = TextureFormat.png;
    }
  }
}
