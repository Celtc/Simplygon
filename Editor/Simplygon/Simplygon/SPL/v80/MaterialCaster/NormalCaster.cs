﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.MaterialCaster.NormalCaster
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Base;
using Simplygon.SPL.v80.Enum;

namespace Simplygon.SPL.v80.MaterialCaster
{
  internal class NormalCaster : BaseMaterialCaster
  {
    public int Dilation { get; set; }

    public DitherType DitherType { get; set; }

    public FillMode FillMode { get; set; }

    public bool FlipBackfacingNormals { get; set; }

    public bool FlipGreen { get; set; }

    public bool GenerateTangentSpaceNormals { get; set; }

    public string NormalsChannel { get; set; }

    public int OutputChannelBitDepth { get; set; }

    public int OutputChannels { get; set; }

    public TextureFormat OutputTextureFormat { get; set; }

    public NormalCaster()
    {
      this.Type = nameof (NormalCaster);
      this.Dilation = 10;
      this.DitherType = DitherType.SG_DITHERPATTERNS_FLOYDSTEINBERG;
      this.FillMode = FillMode.SG_ATLASFILLMODE_INTERPOLATE;
      this.FlipBackfacingNormals = false;
      this.FlipGreen = false;
      this.GenerateTangentSpaceNormals = false;
      this.NormalsChannel = "Normals";
      this.OutputChannelBitDepth = 8;
      this.OutputChannels = 4;
      this.OutputTextureFormat = TextureFormat.png;
    }
  }
}
