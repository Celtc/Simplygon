﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.MaterialCaster.ColorCaster
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Base;
using Simplygon.SPL.v80.Enum;

namespace Simplygon.SPL.v80.MaterialCaster
{
  internal class ColorCaster : BaseMaterialCaster
  {
    public bool BakeOpacityInAlpha { get; set; }

    public string ColorType { get; set; }

    public int Dilation { get; set; }

    public bool BakeFromVertexColor { get; set; }

    public bool BakeToVertexColor { get; set; }

    public DitherType DitherType { get; set; }

    public FillMode FillMode { get; set; }

    public int OutputChannelBitDepth { get; set; }

    public int OutputChannels { get; set; }

    public bool OutputSRGB { get; set; }

    public bool UseMultisampling { get; set; }

    public TextureFormat OutputTextureFormat { get; set; }

    public ColorCaster()
    {
      this.Type = nameof (ColorCaster);
      this.BakeOpacityInAlpha = false;
      this.ColorType = "Diffuse";
      this.Dilation = 10;
      this.BakeFromVertexColor = false;
      this.BakeToVertexColor = false;
      this.DitherType = DitherType.SG_DITHERPATTERNS_FLOYDSTEINBERG;
      this.FillMode = FillMode.SG_ATLASFILLMODE_INTERPOLATE;
      this.OutputChannelBitDepth = 8;
      this.OutputChannels = 4;
      this.OutputSRGB = false;
      this.UseMultisampling = true;
      this.OutputTextureFormat = TextureFormat.png;
    }
  }
}
