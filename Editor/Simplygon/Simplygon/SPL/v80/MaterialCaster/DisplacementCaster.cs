﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.MaterialCaster.DisplacementCaster
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Base;
using Simplygon.SPL.v80.Enum;

namespace Simplygon.SPL.v80.MaterialCaster
{
  internal class DisplacementCaster : BaseMaterialCaster
  {
    public int Dilation { get; set; }

    public double DistanceScaling { get; set; }

    public DitherType DitherType { get; set; }

    public bool GenerateScalarDisplacement { get; set; }

    public bool GenerateTangentSpaceDisplacement { get; set; }

    public int OutputChannelBitDepth { get; set; }

    public int OutputChannels { get; set; }

    public TextureFormat OutputTextureFormat { get; set; }

    public DisplacementCaster()
    {
      this.Type = nameof (DisplacementCaster);
      this.Dilation = 10;
      this.DistanceScaling = 0.0;
      this.DitherType = DitherType.SG_DITHERPATTERNS_FLOYDSTEINBERG;
      this.GenerateScalarDisplacement = false;
      this.GenerateTangentSpaceDisplacement = false;
      this.OutputChannelBitDepth = 8;
      this.OutputChannels = 4;
      this.OutputTextureFormat = TextureFormat.png;
    }
  }
}
