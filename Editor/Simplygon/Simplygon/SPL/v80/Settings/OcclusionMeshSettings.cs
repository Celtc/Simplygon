﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Settings.OcclusionMeshSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Enum;

namespace Simplygon.SPL.v80.Settings
{
  internal class OcclusionMeshSettings : BaseSettings
  {
    public bool InvertOutputMesh { get; set; }

    public OcclusionModes OcclusionMode { get; set; }

    public int OnScreenSize { get; set; }

    public int OnScreenErrorTolerance { get; set; }

    public OcclusionMeshSettings()
    {
      this.InvertOutputMesh = false;
      this.OcclusionMode = OcclusionModes.SG_OCCLUSIONMODE_OCCLUDER;
      this.OnScreenSize = 100;
      this.OnScreenErrorTolerance = 4;
    }
  }
}
