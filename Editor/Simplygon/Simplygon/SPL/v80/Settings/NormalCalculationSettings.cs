﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Settings.NormalCalculationSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v80.Settings
{
  internal class NormalCalculationSettings : BaseSettings
  {
    public bool DetectPrimitiveNormals { get; set; }

    public double HardEdgeAngleInRadians { get; set; }

    public bool RepairInvalidNormals { get; set; }

    public bool ReplaceNormals { get; set; }

    public bool ReplaceTangents { get; set; }

    public bool ScaleByAngle { get; set; }

    public bool ScaleByArea { get; set; }

    public NormalCalculationSettings()
    {
      this.DetectPrimitiveNormals = false;
      this.HardEdgeAngleInRadians = 314.0 / 225.0;
      this.RepairInvalidNormals = false;
      this.ReplaceNormals = false;
      this.ReplaceTangents = false;
      this.ScaleByAngle = true;
      this.ScaleByArea = true;
    }
  }
}
