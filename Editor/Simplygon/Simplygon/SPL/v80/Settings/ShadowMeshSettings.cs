﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Settings.ShadowMeshSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v80.Settings
{
  internal class ShadowMeshSettings : BaseSettings
  {
    public double LightDirectionX { get; set; }

    public double LightDirectionY { get; set; }

    public double LightDirectionZ { get; set; }

    public int OnScreenErrorTolerance { get; set; }

    public int OnScreenSize { get; set; }

    public bool UseDirectionalMode { get; set; }

    public ShadowMeshSettings()
    {
      this.LightDirectionX = 0.0;
      this.LightDirectionY = 0.0;
      this.LightDirectionZ = 0.0;
      this.OnScreenErrorTolerance = 4;
      this.OnScreenSize = 100;
      this.UseDirectionalMode = true;
    }
  }
}
