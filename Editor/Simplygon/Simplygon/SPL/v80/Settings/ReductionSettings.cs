﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Settings.ReductionSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Enum;

namespace Simplygon.SPL.v80.Settings
{
  internal class ReductionSettings : BaseSettings
  {
    public bool AllowDegenerateTexCoords { get; set; }

    public bool CreateGeomorphGeometry { get; set; }

    public DataCreationPreferences DataCreationPreferences { get; set; }

    public double EdgeSetImportance { get; set; }

    public bool GenerateGeomorphData { get; set; }

    public double GeometryImportance { get; set; }

    public double GroupImportance { get; set; }

    public double InwardMoveMultiplier { get; set; }

    public bool KeepSymmetry { get; set; }

    public double MaterialImportance { get; set; }

    public double MaxDeviation { get; set; }

    public double MaxEdgeLength { get; set; }

    public int OnScreenSize { get; set; }

    public double OutwardMoveMultiplier { get; set; }

    public string ProcessSelectionSetName { get; set; }

    public ReductionHeuristics ReductionHeuristics { get; set; }

    public ReductionTargets ReductionTargets { get; set; }

    public double ShadingImportance { get; set; }

    public double SkinningImportance { get; set; }

    public StopCondition StopCondition { get; set; }

    public int SymmetryAxis { get; set; }

    public double SymmetryDetectionTolerance { get; set; }

    public double SymmetryOffset { get; set; }

    public double TextureImportance { get; set; }

    public int TriangleCount { get; set; }

    public double TriangleRatio { get; set; }

    public bool UseAutomaticSymmetryDetection { get; set; }

    public bool UseHighQualityNormalCalculation { get; set; }

    public bool UseSymmetryQuadRetriangulator { get; set; }

    public bool UseVertexWeights { get; set; }

    public double VertexColorImportance { get; set; }

    public ReductionSettings()
    {
      this.AllowDegenerateTexCoords = true;
      this.CreateGeomorphGeometry = false;
      this.DataCreationPreferences = DataCreationPreferences.SG_DATACREATIONPREFERENCES_PREFER_OPTIMIZED_RESULT;
      this.EdgeSetImportance = 1.0;
      this.GenerateGeomorphData = false;
      this.GeometryImportance = 1.0;
      this.GroupImportance = 1.0;
      this.InwardMoveMultiplier = 1.0;
      this.KeepSymmetry = false;
      this.MaterialImportance = 1.0;
      this.MaxDeviation = 0.0;
      this.MaxEdgeLength = (double) int.MaxValue;
      this.OnScreenSize = int.MaxValue;
      this.OutwardMoveMultiplier = 1.0;
      this.ProcessSelectionSetName = "";
      this.ReductionHeuristics = ReductionHeuristics.SG_REDUCTIONHEURISTICS_FAST;
      this.ReductionTargets = ReductionTargets.SG_REDUCTIONTARGET_ALL;
      this.ShadingImportance = 1.0;
      this.SkinningImportance = 1.0;
      this.StopCondition = StopCondition.SG_STOPCONDITION_ALL;
      this.SymmetryAxis = 0;
      this.SymmetryDetectionTolerance = 0.0004;
      this.SymmetryOffset = 0.0;
      this.TextureImportance = 1.0;
      this.TriangleCount = int.MaxValue;
      this.TriangleRatio = 1.0;
      this.UseAutomaticSymmetryDetection = false;
      this.UseHighQualityNormalCalculation = true;
      this.UseSymmetryQuadRetriangulator = true;
      this.UseVertexWeights = true;
      this.VertexColorImportance = 1.0;
    }
  }
}
