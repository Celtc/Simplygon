﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Settings.MappingImageSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Enum;

namespace Simplygon.SPL.v80.Settings
{
  internal class MappingImageSettings : BaseSettings
  {
    public bool AllowTransparencyMapping { get; set; }

    public double AutomaticTextureSizeMultiplier { get; set; }

    public ChartAggregatorMode ChartAggregatorMode { get; set; }

    public string ChartAggregatorOriginalChartProportionsChannel { get; set; }

    public int ChartAggregatorOriginalTexCoordLevel { get; set; }

    public string ChartAggregatorOriginalTexCoordLevelName { get; set; }

    public bool ChartAggregatorSeparateOverlappingCharts { get; set; }

    public bool ForcePower2Texture { get; set; }

    public bool GenerateMappingImage { get; set; }

    public bool GenerateTangents { get; set; }

    public bool GenerateTexCoords { get; set; }

    public int GutterSpace { get; set; }

    public int Height { get; set; }

    public int MaximumLayers { get; set; }

    public int MultisamplingLevel { get; set; }

    public double ParameterizerMaxStretch { get; set; }

    public TexCoordGeneratorType TexCoordGeneratorType { get; set; }

    public int TexCoordLevel { get; set; }

    public bool UseAutomaticTextureSize { get; set; }

    public bool UseFullRetexturing { get; set; }

    public bool UseVertexWeights { get; set; }

    public int Width { get; set; }

    public MappingImageSettings()
    {
      this.AllowTransparencyMapping = true;
      this.AutomaticTextureSizeMultiplier = 1.0;
      this.ChartAggregatorMode = ChartAggregatorMode.SG_CHARTAGGREGATORMODE_TEXTURESIZEPROPORTIONS;
      this.ChartAggregatorOriginalChartProportionsChannel = "Diffuse";
      this.ChartAggregatorOriginalTexCoordLevel = 0;
      this.ChartAggregatorOriginalTexCoordLevelName = "";
      this.ChartAggregatorSeparateOverlappingCharts = true;
      this.ForcePower2Texture = false;
      this.GenerateMappingImage = false;
      this.GenerateTangents = false;
      this.GenerateTexCoords = false;
      this.GutterSpace = 1;
      this.Height = 512;
      this.MaximumLayers = 3;
      this.MultisamplingLevel = 2;
      this.ParameterizerMaxStretch = 0.2;
      this.TexCoordGeneratorType = TexCoordGeneratorType.SG_TEXCOORDGENERATORTYPE_PARAMETERIZER;
      this.TexCoordLevel = (int) byte.MaxValue;
      this.UseAutomaticTextureSize = false;
      this.UseFullRetexturing = false;
      this.UseVertexWeights = false;
      this.Width = 512;
    }
  }
}
