﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Settings.BoneSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Enum;

namespace Simplygon.SPL.v80.Settings
{
  internal class BoneSettings : BaseSettings
  {
    public int BoneCount { get; set; }

    public double BoneRatio { get; set; }

    public BoneReductionTargets BoneReductionTargets { get; set; }

    public bool LimitBonesPerVertex { get; set; }

    public string LockBoneSelectionSetName { get; set; }

    public int MaxBonePerVertex { get; set; }

    public double MaxDeviation { get; set; }

    public int OnScreenSize { get; set; }

    public bool RemoveUnusedBones { get; set; }

    public string RemoveBoneSelectionSetName { get; set; }

    public bool UseBoneReducer { get; set; }

    public BoneSettings()
    {
      this.BoneCount = int.MaxValue;
      this.BoneRatio = 1.0;
      this.BoneReductionTargets = BoneReductionTargets.SG_BONEREDUCTIONTARGET_ALL;
      this.LimitBonesPerVertex = false;
      this.LockBoneSelectionSetName = "";
      this.MaxBonePerVertex = 4;
      this.MaxDeviation = 0.0;
      this.OnScreenSize = int.MaxValue;
      this.RemoveUnusedBones = false;
      this.RemoveBoneSelectionSetName = "";
      this.UseBoneReducer = false;
    }
  }
}
