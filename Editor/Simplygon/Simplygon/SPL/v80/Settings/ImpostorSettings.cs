﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Settings.ImpostorSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v80.Settings
{
  internal class ImpostorSettings : BaseSettings
  {
    public double TightFittingDepthOffset { get; set; }

    public bool UseTightFitting { get; set; }

    public double ViewDirX { get; set; }

    public double ViewDirY { get; set; }

    public double ViewDirZ { get; set; }

    public ImpostorSettings()
    {
      this.TightFittingDepthOffset = 0.0;
      this.UseTightFitting = false;
      this.ViewDirX = 0.0;
      this.ViewDirY = 0.0;
      this.ViewDirZ = 1.0;
    }
  }
}
