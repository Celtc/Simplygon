﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v80.Settings.RemeshingSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v80.Enum;

namespace Simplygon.SPL.v80.Settings
{
  internal class RemeshingSettings : BaseSettings
  {
    public string CuttingPlaneSelectionSetName { get; set; }

    public double HardEdgeAngleInRadians { get; set; }

    public double EmptySpaceOverride { get; set; }

    public int MaxTriangleSize { get; set; }

    public int MergeDistance { get; set; }

    public int OnScreenSize { get; set; }

    public string ProcessSelectionSetName { get; set; }

    public SurfaceTransferMode SurfaceTransferMode { get; set; }

    public bool TransferColors { get; set; }

    public bool TransferNormals { get; set; }

    public bool UseCuttingPlanes { get; set; }

    public bool UseEmptySpaceOverride { get; set; }

    public RemeshingSettings()
    {
      this.CuttingPlaneSelectionSetName = "";
      this.HardEdgeAngleInRadians = 314.0 / 225.0;
      this.EmptySpaceOverride = 0.0;
      this.MaxTriangleSize = 0;
      this.MergeDistance = 4;
      this.OnScreenSize = 100;
      this.ProcessSelectionSetName = "";
      this.SurfaceTransferMode = SurfaceTransferMode.SG_SURFACETRANSFER_ACCURATE;
      this.TransferColors = false;
      this.TransferNormals = false;
      this.UseCuttingPlanes = false;
      this.UseEmptySpaceOverride = false;
    }
  }
}
