﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.Version
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json;
using System.Globalization;

namespace Simplygon.SPL
{
  internal class Version
  {
    [JsonIgnore]
    public int Major { get; set; }

    [JsonIgnore]
    public int Minor { get; set; }

    [JsonIgnore]
    public int Build { get; set; }

    public Version(string version)
    {
      string[] strArray = version.Split('.');
      if (strArray.Length != 0)
      {
        int result = 0;
        if (int.TryParse(strArray[0], out result))
          this.Major = result;
      }
      if (strArray.Length > 1)
      {
        int result = 0;
        if (int.TryParse(strArray[1], out result))
          this.Minor = result;
      }
      if (strArray.Length <= 2)
        return;
      int result1 = 0;
      if (!int.TryParse(strArray[2], out result1))
        return;
      this.Build = result1;
    }

    public override string ToString()
    {
      return string.Format(CultureInfo.InvariantCulture, "{0}.{1}.{2}", (object) this.Major, (object) this.Minor, (object) this.Build);
    }
  }
}
