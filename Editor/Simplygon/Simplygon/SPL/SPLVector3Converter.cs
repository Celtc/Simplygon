﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.SPLVector3Converter
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json;
using System;

namespace Simplygon.SPL
{
  internal class SPLVector3Converter : JsonConverter
  {
    public override bool CanConvert(Type objectType)
    {
      return objectType == typeof (Vector3);
    }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
      return (object) serializer.Deserialize<Vector3>(reader);
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
      if (!(value is Vector3))
        return;
      serializer.Serialize(writer, (object) ((Vector3) value).ToString());
    }
  }
}
