﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v1.Settings.BoneSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v1.Settings
{
  internal class BoneSettings : BaseSettings
  {
    public int BoneLodPixelSize { get; set; }

    public int BoneLodProcess { get; set; }

    public double BoneLodRatio { get; set; }

    public int MaxBonePerVertex { get; set; }

    public bool RemoveUnusedBones { get; set; }

    public BoneSettings()
    {
      this.BoneLodPixelSize = 100;
      this.BoneLodProcess = 0;
      this.BoneLodRatio = 1.0;
      this.MaxBonePerVertex = int.MaxValue;
      this.RemoveUnusedBones = false;
    }
  }
}
