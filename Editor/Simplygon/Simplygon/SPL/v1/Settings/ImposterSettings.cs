﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v1.Settings.ImposterSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v1.Settings
{
  internal class ImposterSettings : BaseSettings
  {
    public double DepthOffset { get; set; }

    public bool UseTightFitting { get; set; }

    public bool UsePerspective { get; set; }

    public Vector3 ViewDirection { get; set; }

    public ImposterSettings()
    {
      this.DepthOffset = 0.0;
      this.UseTightFitting = false;
      this.UsePerspective = false;
    }
  }
}
