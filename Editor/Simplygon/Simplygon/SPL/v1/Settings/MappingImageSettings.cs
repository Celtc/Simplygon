﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v1.Settings.MappingImageSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v1.Settings
{
  internal class MappingImageSettings : BaseSettings
  {
    public bool AutomaticTextureSize { get; set; }

    public double AutomaticTextureSizeMultiplier { get; set; }

    public int BaseAtlasOnOriginalTexCoordLevel { get; set; }

    public bool BaseAtlasOnOriginalTexCoords { get; set; }

    public bool BaseAtlasOnOriginalUseAreaWeighting { get; set; }

    public bool ForcePower2Texture { get; set; }

    public bool FullRetexturing { get; set; }

    public bool GenerateMappingImage { get; set; }

    public bool GenerateTangents { get; set; }

    public bool GenerateTexCoords { get; set; }

    public int GutterSpace { get; set; }

    public double MaxStretch { get; set; }

    public int MultisamplingLevel { get; set; }

    public bool UseVertexWeights { get; set; }

    public bool UseVisibilityWeights { get; set; }

    public int Height { get; set; }

    public int Width { get; set; }

    public MappingImageSettings()
    {
      this.GenerateMappingImage = false;
      this.GenerateTexCoords = false;
      this.GenerateTangents = false;
      this.MaxStretch = 0.2;
      this.FullRetexturing = false;
      this.AutomaticTextureSize = false;
      this.AutomaticTextureSizeMultiplier = 1.0;
      this.ForcePower2Texture = false;
      this.UseVertexWeights = false;
      this.BaseAtlasOnOriginalTexCoords = false;
      this.BaseAtlasOnOriginalTexCoordLevel = 0;
      this.BaseAtlasOnOriginalUseAreaWeighting = true;
      this.MultisamplingLevel = 2;
      this.Height = 512;
      this.Width = 512;
      this.GutterSpace = 1;
    }
  }
}
