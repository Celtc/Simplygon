﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v1.Settings.VisibilitySettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v1.Settings
{
  internal class VisibilitySettings : BaseSettings
  {
    public bool CullOccludedGeometry { get; set; }

    public bool ForceVisibilityCalculation { get; set; }

    public bool IsBackfaceCullingEnabled { get; set; }

    public bool UseVisibilityWeightsInReducer { get; set; }

    public bool UseVisibilityWeightsInTexcoordGenerator { get; set; }

    public double VisibilityWeightsPower { get; set; }

    public VisibilitySettings()
    {
      this.UseVisibilityWeightsInReducer = false;
      this.UseVisibilityWeightsInTexcoordGenerator = false;
      this.VisibilityWeightsPower = 1.0;
      this.CullOccludedGeometry = false;
      this.IsBackfaceCullingEnabled = true;
      this.ForceVisibilityCalculation = false;
    }
  }
}
