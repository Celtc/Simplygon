﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v1.Settings.RepairSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v1.Settings
{
  internal class RepairSettings : BaseSettings
  {
    public int ProgressivePasses { get; set; }

    public double TjuncDist { get; set; }

    public bool UseTJunctionRemover { get; set; }

    public bool UseWelding { get; set; }

    public double WeldDist { get; set; }

    public bool WeldOnlyBorderVertices { get; set; }

    public bool WeldOnlyObjectBoundary { get; set; }

    public RepairSettings()
    {
      this.TjuncDist = 0.0;
      this.WeldDist = 0.0;
      this.WeldOnlyBorderVertices = false;
      this.WeldOnlyObjectBoundary = false;
      this.UseWelding = true;
      this.UseTJunctionRemover = true;
      this.ProgressivePasses = 3;
    }
  }
}
