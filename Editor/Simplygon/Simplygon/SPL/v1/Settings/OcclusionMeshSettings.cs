﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v1.Settings.OcclusionMeshSettings
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v1.Settings
{
  internal class OcclusionMeshSettings : BaseSettings
  {
    public int OnScreenSize { get; set; }

    public int SillhouetteFidelity { get; set; }

    public OcclusionMeshSettings()
    {
      this.OnScreenSize = 100;
      this.SillhouetteFidelity = 3;
    }
  }
}
