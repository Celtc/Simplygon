﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v1.Processor.RemeshingProcessor
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v1.Base;
using Simplygon.SPL.v1.Settings;

namespace Simplygon.SPL.v1.Processor
{
  internal class RemeshingProcessor : BaseProcessor
  {
    public RemeshingSettings RemeshingSettings { get; set; }

    public MappingImageSettings MappingImageSettings { get; set; }

    public VisibilitySettings VisibilitySettings { get; set; }

    public BoneSettings BoneSettings { get; set; }

    public RemeshingProcessor()
    {
      this.Type = nameof (RemeshingProcessor);
      this.BoneSettings = new BoneSettings();
      this.MappingImageSettings = new MappingImageSettings();
      this.RemeshingSettings = new RemeshingSettings();
      this.VisibilitySettings = new VisibilitySettings();
    }

    protected override BaseProcessor InternalDeepCopy()
    {
      RemeshingProcessor remeshingProcessor = this.MemberwiseClone() as RemeshingProcessor;
      remeshingProcessor.RemeshingSettings = this.RemeshingSettings.DeepCopy() as RemeshingSettings;
      remeshingProcessor.MappingImageSettings = this.MappingImageSettings.DeepCopy() as MappingImageSettings;
      remeshingProcessor.VisibilitySettings = this.VisibilitySettings.DeepCopy() as VisibilitySettings;
      remeshingProcessor.BoneSettings = this.BoneSettings.DeepCopy() as BoneSettings;
      return (BaseProcessor) remeshingProcessor;
    }
  }
}
