﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v1.Processor.ImposterProcessor
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v1.Base;
using Simplygon.SPL.v1.Settings;

namespace Simplygon.SPL.v1.Processor
{
  internal class ImposterProcessor : BaseProcessor
  {
    public ImposterSettings ImposterSettings { get; set; }

    public ImposterProcessor()
    {
      this.Type = nameof (ImposterProcessor);
      this.ImposterSettings = new ImposterSettings();
    }

    protected override BaseProcessor InternalDeepCopy()
    {
      ImposterProcessor imposterProcessor = this.MemberwiseClone() as ImposterProcessor;
      imposterProcessor.ImposterSettings = this.ImposterSettings.DeepCopy() as ImposterSettings;
      return (BaseProcessor) imposterProcessor;
    }
  }
}
