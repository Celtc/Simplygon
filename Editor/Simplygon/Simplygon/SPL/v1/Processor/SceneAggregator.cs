﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v1.Processor.SceneAggregator
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v1.Base;
using Simplygon.SPL.v1.Settings;

namespace Simplygon.SPL.v1.Processor
{
  internal class SceneAggregator : BaseProcessor
  {
    public double MaxDeviation { get; set; }

    public SceneAggregatorSettings SceneAggregatorSettings { get; set; }

    public MappingImageSettings MappingImageSettings { get; set; }

    public VisibilitySettings VisibilitySettings { get; set; }

    public SceneAggregator()
    {
      this.Type = nameof (SceneAggregator);
      this.MappingImageSettings = new MappingImageSettings();
      this.SceneAggregatorSettings = new SceneAggregatorSettings();
      this.VisibilitySettings = new VisibilitySettings();
    }

    protected override BaseProcessor InternalDeepCopy()
    {
      SceneAggregator sceneAggregator = this.MemberwiseClone() as SceneAggregator;
      sceneAggregator.SceneAggregatorSettings = this.SceneAggregatorSettings.DeepCopy() as SceneAggregatorSettings;
      sceneAggregator.MappingImageSettings = this.MappingImageSettings.DeepCopy() as MappingImageSettings;
      sceneAggregator.VisibilitySettings = this.VisibilitySettings.DeepCopy() as VisibilitySettings;
      return (BaseProcessor) sceneAggregator;
    }
  }
}
