﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v1.Enum.FeatureFlags
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.SPL.v1.Enum
{
  internal enum FeatureFlags : uint
  {
    SG_FEATUREFLAGS_GEOMETRYBOUNDARY = 1,
    SG_FEATUREFLAGS_MATERIAL = 2,
    SG_FEATUREFLAGS_SHADING = 4,
    SG_FEATUREFLAGS_USER = 8,
    SG_FEATUREFLAGS_GROUP = 16, // 0x00000010
    SG_FEATUREFLAGS_NONMANIFOLD = 32, // 0x00000020
    SG_FEATUREFLAGS_FLIPPEDTRIANGLE = 64, // 0x00000040
    SG_FEATUREFLAGS_NORMALCHART = 128, // 0x00000080
    SG_FEATUREFLAGS_COLORS = 256, // 0x00000100
    SG_FEATUREFLAGS_SYMMETRY = 512, // 0x00000200
    SG_FEATUREFLAGS_BORDEREND = 1024, // 0x00000400
    SG_FEATUREFLAGS_BORDERJUNCTION = 2048, // 0x00000800
    SG_FEATUREFLAGS_COMPONENTCONNECTOR = 4096, // 0x00001000
    SG_FEATUREFLAGS_NONCONNECTED = 8192, // 0x00002000
    SG_FEATUREFLAGS_TEXTURE0 = 65536, // 0x00010000
    SG_FEATUREFLAGS_TEXTURE1 = 131072, // 0x00020000
    SG_FEATUREFLAGS_TEXTURE2 = 262144, // 0x00040000
    SG_FEATUREFLAGS_TEXTURE3 = 524288, // 0x00080000
    SG_FEATUREFLAGS_TEXTURE4 = 1048576, // 0x00100000
    SG_FEATUREFLAGS_TEXTURE5 = 2097152, // 0x00200000
    SG_FEATUREFLAGS_TEXTURE6 = 4194304, // 0x00400000
    SG_FEATUREFLAGS_TEXTURE7 = 8388608, // 0x00800000
    SG_FEATUREFLAGS_TEXTURE_ALL = 16711680, // 0x00FF0000
    SG_FEATUREFLAGS_ALL = 4294967295, // 0xFFFFFFFF
  }
}
