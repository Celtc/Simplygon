﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v1.Converter.MaterialCasterConverter
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json.Linq;
using Simplygon.SPL.v1.Base;
using Simplygon.SPL.v1.MaterialCaster;
using System;

namespace Simplygon.SPL.v1.Converter
{
  internal class MaterialCasterConverter : SPLCreationConverter<BaseMaterialCaster>
  {
    protected override BaseMaterialCaster Create(Type objectType, JObject jObject)
    {
      JToken token = SPLConverterUtility.GetToken("Type", jObject);
      if (token != null)
      {
        string a = token.ToString();
        if (string.Equals(a, "ColorCaster", StringComparison.OrdinalIgnoreCase))
          return (BaseMaterialCaster) new ColorCaster();
        if (string.Equals(a, "DisplacementCaster", StringComparison.OrdinalIgnoreCase))
          return (BaseMaterialCaster) new DisplacementCaster();
        if (string.Equals(a, "NormalCaster", StringComparison.OrdinalIgnoreCase))
          return (BaseMaterialCaster) new NormalCaster();
        if (string.Equals(a, "OpacityCaster", StringComparison.OrdinalIgnoreCase))
          return (BaseMaterialCaster) new OpacityCaster();
      }
      return (BaseMaterialCaster) null;
    }
  }
}
