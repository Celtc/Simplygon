﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v1.Converter.NodeConverter
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json.Linq;
using Simplygon.SPL.v1.Base;
using Simplygon.SPL.v1.Node;
using System;

namespace Simplygon.SPL.v1.Converter
{
  internal class NodeConverter : SPLCreationConverter<BaseNode>
  {
    protected override BaseNode Create(Type objectType, JObject jObject)
    {
      JToken token = SPLConverterUtility.GetToken("Type", jObject);
      if (token != null)
      {
        string a = token.ToString();
        if (string.Equals(a, "ProcessNode", StringComparison.OrdinalIgnoreCase))
          return (BaseNode) new ProcessNode();
        if (string.Equals(a, "ContainerNode", StringComparison.OrdinalIgnoreCase))
          return (BaseNode) new ContainerNode();
        if (string.Equals(a, "WriteNode", StringComparison.OrdinalIgnoreCase))
          return (BaseNode) new WriteNode();
      }
      return (BaseNode) new UnknownNode();
    }
  }
}
