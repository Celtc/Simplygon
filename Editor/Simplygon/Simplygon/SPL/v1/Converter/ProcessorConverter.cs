﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v1.Converter.ProcessorConverter
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json.Linq;
using Simplygon.SPL.v1.Base;
using Simplygon.SPL.v1.Processor;
using System;

namespace Simplygon.SPL.v1.Converter
{
  internal class ProcessorConverter : SPLCreationConverter<BaseProcessor>
  {
    protected override BaseProcessor Create(Type objectType, JObject jObject)
    {
      JToken token = SPLConverterUtility.GetToken("Type", jObject);
      if (token != null)
      {
        string a = token.ToString();
        if (string.Equals(a, "ImposterProcessor", StringComparison.OrdinalIgnoreCase))
          return (BaseProcessor) new ImposterProcessor();
        if (string.Equals(a, "ReductionProcessor", StringComparison.OrdinalIgnoreCase))
          return (BaseProcessor) new ReductionProcessor();
        if (string.Equals(a, "RemeshingProcessor", StringComparison.OrdinalIgnoreCase))
          return (BaseProcessor) new RemeshingProcessor();
        if (string.Equals(a, "SceneAggregator", StringComparison.OrdinalIgnoreCase))
          return (BaseProcessor) new SceneAggregator();
      }
      return (BaseProcessor) null;
    }
  }
}
