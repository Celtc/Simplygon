﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v1.MaterialCaster.DisplacementCaster
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v1.Base;

namespace Simplygon.SPL.v1.MaterialCaster
{
  internal class DisplacementCaster : BaseMaterialCaster
  {
    public int Dilation { get; set; }

    public double DistanceScaling { get; set; }

    public bool GenerateScalarDisplacement { get; set; }

    public bool GenerateTangentSpaceDisplacement { get; set; }

    public int OutputChannelBitDepth { get; set; }

    public DisplacementCaster()
    {
      this.Type = nameof (DisplacementCaster);
    }
  }
}
