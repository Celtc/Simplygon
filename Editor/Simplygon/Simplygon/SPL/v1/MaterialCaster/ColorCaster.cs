﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v1.MaterialCaster.ColorCaster
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v1.Base;

namespace Simplygon.SPL.v1.MaterialCaster
{
  internal class ColorCaster : BaseMaterialCaster
  {
    public bool BakeOpacityInAlpha { get; set; }

    public bool BakeVertexColors { get; set; }

    public string ColorType { get; set; }

    public int Dilation { get; set; }

    public int FillMode { get; set; }

    public int OutputChannelBitDepth { get; set; }

    public bool sRGB { get; set; }

    public ColorCaster()
    {
      this.Type = nameof (ColorCaster);
      this.ColorType = "Diffuse";
    }
  }
}
