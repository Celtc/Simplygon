﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.SPL.v1.MaterialCaster.NormalCaster
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL.v1.Base;

namespace Simplygon.SPL.v1.MaterialCaster
{
  internal class NormalCaster : BaseMaterialCaster
  {
    public int Dilation { get; set; }

    public int FillMode { get; set; }

    public bool FlipBackfacingNormals { get; set; }

    public bool FlippedGreen { get; set; }

    public bool GenerateTangentSpaceNormals { get; set; }

    public int OutputChannelBitDepth { get; set; }

    public int OutputChannels { get; set; }

    public NormalCaster()
    {
      this.Type = nameof (NormalCaster);
      this.OutputChannelBitDepth = 8;
    }
  }
}
