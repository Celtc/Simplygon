﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Math.Vector4
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Math
{
  internal struct Vector4
  {
    public double X;
    public double Y;
    public double Z;
    public double W;

    public static Vector4 Zero
    {
      get
      {
        return new Vector4(0.0, 0.0, 0.0, 0.0);
      }
    }

    public static Vector4 One
    {
      get
      {
        return new Vector4(1.0, 1.0, 1.0, 1.0);
      }
    }

    public Vector4(double x, double y, double z, double w)
    {
      this = new Vector4();
      this.X = x;
      this.Y = y;
      this.Z = z;
      this.W = w;
    }

    public Vector4(Vector3 value, double w)
    {
      this = new Vector4();
      this.X = value.X;
      this.Y = value.Y;
      this.Z = value.Z;
      this.W = w;
    }

    public double[] ToArray()
    {
      return new double[4]{ this.X, this.Y, this.Z, this.W };
    }

    public float[] ToArrayF()
    {
      return new float[4]
      {
        (float) this.X,
        (float) this.Y,
        (float) this.Z,
        (float) this.W
      };
    }

    public static bool operator ==(Vector4 left, Vector4 right)
    {
      if (left.X == right.X && left.Y == right.Y && left.Z == right.Z)
        return left.W == right.W;
      return false;
    }

    public static bool operator !=(Vector4 left, Vector4 right)
    {
      return !(left == right);
    }

    public static Vector4 operator *(Vector4 left, Matrix right)
    {
      return new Vector4(left.X * right.M11 + left.Y * right.M21 + left.Z * right.M31 + left.W * right.M41, left.X * right.M12 + left.Y * right.M22 + left.Z * right.M32 + left.W * right.M42, left.X * right.M13 + left.Y * right.M23 + left.Z * right.M33 + left.W * right.M43, left.X * right.M14 + left.Y * right.M24 + left.Z * right.M34 + left.W * right.M44);
    }

    public static Vector4 operator +(Vector4 left, Vector4 right)
    {
      return new Vector4(left.X + right.X, left.Y + right.Y, left.Z + right.Z, left.W + right.W);
    }

    public static Vector4 operator -(Vector4 left, Vector4 right)
    {
      return new Vector4(left.X - right.X, left.Y - right.Y, left.Z - right.Z, left.W - right.W);
    }

    public static Vector4 operator *(Vector4 left, double scale)
    {
      return new Vector4(left.X * scale, left.Y * scale, left.Z * scale, left.W * scale);
    }

    public static bool operator <(Vector4 left, Vector4 right)
    {
      if (left.X < right.X && left.Y < right.Y && left.Z < right.Z)
        return left.W < right.W;
      return false;
    }

    public static bool operator <=(Vector4 left, Vector4 right)
    {
      if (left.X <= right.X && left.Y <= right.Y && left.Z <= right.Z)
        return left.W <= right.W;
      return false;
    }

    public static bool operator >(Vector4 left, Vector4 right)
    {
      if (left.X > right.X && left.Y > right.Y && left.Z > right.Z)
        return left.W > right.W;
      return false;
    }

    public static bool operator >=(Vector4 left, Vector4 right)
    {
      if (left.X >= right.X && left.Y >= right.Y && left.Z >= right.Z)
        return left.W >= right.W;
      return false;
    }

    public Vector3 ToVector3()
    {
      return new Vector3(this.X, this.Y, this.Z);
    }

    public Vector4 Normalize()
    {
      double num = this.Length();
      if (num != 0.0)
        return this * (1.0 / num);
      return Vector4.Zero;
    }

    public double Length()
    {
      return System.Math.Sqrt(this.LengthSquared());
    }

    public double LengthSquared()
    {
      return this.X * this.X + this.Y * this.Y + this.Z * this.Z + this.W * this.W;
    }

    public static double Dot(Vector4 left, Vector4 right)
    {
      return left.X * right.X + left.Y * right.Y + left.Z * right.Z + left.W * right.W;
    }

    public static Vector4 Transform(Vector4 left, Matrix right)
    {
      return left * right;
    }

    public static Vector4 Lerp(Vector4 start, Vector4 end, double t)
    {
      return start * (1.0 - t) + end * t;
    }
  }
}
