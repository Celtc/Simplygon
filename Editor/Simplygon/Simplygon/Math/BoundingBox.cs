﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Math.BoundingBox
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Math
{
  internal struct BoundingBox
  {
    public Vector3 Min;
    public Vector3 Max;

    public Vector3 Center
    {
      get
      {
        return this.Min + (this.Max - this.Min) * 0.5;
      }
    }

    public double Radius
    {
      get
      {
        return (this.Max - this.Center).Length();
      }
    }

    public static BoundingBox Default
    {
      get
      {
        return new BoundingBox(new Vector3(double.MaxValue, double.MaxValue, double.MaxValue), new Vector3(double.MinValue, double.MinValue, double.MinValue));
      }
    }

    public BoundingBox(Vector3 min, Vector3 max)
    {
      this = new BoundingBox();
      this.Min = min;
      this.Max = max;
    }

    public BoundingBox(Vector4[] points)
    {
      this = new BoundingBox();
      this.Min = new Vector3(double.MaxValue, double.MaxValue, double.MaxValue);
      this.Max = new Vector3(double.MinValue, double.MinValue, double.MinValue);
      foreach (Vector4 point in points)
      {
        if (point.X < this.Min.X)
          this.Min.X = point.X;
        if (point.Y < this.Min.Y)
          this.Min.Y = point.Y;
        if (point.Z < this.Min.Z)
          this.Min.Z = point.Z;
        if (point.X > this.Max.X)
          this.Max.X = point.X;
        if (point.Y > this.Max.Y)
          this.Max.Y = point.Y;
        if (point.Z > this.Max.Z)
          this.Max.Z = point.Z;
      }
    }

    public static bool operator ==(BoundingBox left, BoundingBox right)
    {
      if (left.Min == right.Min)
        return left.Max == right.Max;
      return false;
    }

    public static bool operator !=(BoundingBox left, BoundingBox right)
    {
      return !(left == right);
    }

    public static BoundingBox operator +(BoundingBox left, Vector3 position)
    {
      return new BoundingBox(left.Min + position, left.Max + position);
    }

    public static BoundingBox operator +(BoundingBox left, BoundingBox right)
    {
      Vector3 min = left.Min;
      Vector3 max = left.Max;
      if (right.Min.X < min.X)
        min.X = right.Min.X;
      if (right.Min.Y < min.Y)
        min.Y = right.Min.Y;
      if (right.Min.Z < min.Z)
        min.Z = right.Min.Z;
      if (right.Max.X > max.X)
        max.X = right.Max.X;
      if (right.Max.Y > max.Y)
        max.Y = right.Max.Y;
      if (right.Max.Z > max.Z)
        max.Z = right.Max.Z;
      return new BoundingBox(min, max);
    }

    public static BoundingBox operator *(BoundingBox left, double scale)
    {
      return new BoundingBox(left.Min * scale, left.Max * scale);
    }

    public static BoundingBox operator *(BoundingBox left, Vector3 scale)
    {
      return new BoundingBox(new Vector3(left.Min.X * scale.X, left.Min.Y * scale.Y, left.Min.Z * scale.Z), new Vector3(left.Max.X * scale.X, left.Max.Y * scale.Y, left.Max.Z * scale.Z));
    }

    public static BoundingBox operator *(BoundingBox left, Matrix right)
    {
      Vector3[] vector3Array = new Vector3[8]
      {
        new Vector3(left.Min.X, left.Min.Y, left.Min.Z),
        new Vector3(left.Min.X, left.Min.Y, left.Max.Z),
        new Vector3(left.Min.X, left.Max.Y, left.Min.Z),
        new Vector3(left.Min.X, left.Max.Y, left.Max.Z),
        new Vector3(left.Max.X, left.Min.Y, left.Min.Z),
        new Vector3(left.Max.X, left.Min.Y, left.Max.Z),
        new Vector3(left.Max.X, left.Max.Y, left.Min.Z),
        new Vector3(left.Max.X, left.Max.Y, left.Max.Z)
      };
      Vector3 min = new Vector3(double.MaxValue, double.MaxValue, double.MaxValue);
      Vector3 max = new Vector3(double.MinValue, double.MinValue, double.MinValue);
      for (int index = 0; index < 8; ++index)
      {
        Vector3 vector3 = Vector3.Transform(vector3Array[index], right);
        min.X = System.Math.Min(vector3.X, min.X);
        min.Y = System.Math.Min(vector3.Y, min.Y);
        min.Z = System.Math.Min(vector3.Z, min.Z);
        max.X = System.Math.Max(vector3.X, max.X);
        max.Y = System.Math.Max(vector3.Y, max.Y);
        max.Z = System.Math.Max(vector3.Z, max.Z);
      }
      return new BoundingBox(min, max);
    }

    public static Vector3 ClosestPointOnBoundingBox(BoundingBox boundingBox, Vector3 point)
    {
      return new Vector3(point.X < boundingBox.Min.X ? boundingBox.Min.X : (point.X > boundingBox.Max.X ? boundingBox.Max.X : point.X), point.Y < boundingBox.Min.Y ? boundingBox.Min.Y : (point.Y > boundingBox.Max.Y ? boundingBox.Max.Y : point.Y), point.Z < boundingBox.Min.Z ? boundingBox.Min.Z : (point.Z > boundingBox.Max.Z ? boundingBox.Max.Z : point.Z));
    }

    public static double DistanceToPoint(BoundingBox boundingBox, Vector3 point)
    {
      return (point - BoundingBox.ClosestPointOnBoundingBox(boundingBox, point)).Length();
    }
  }
}
