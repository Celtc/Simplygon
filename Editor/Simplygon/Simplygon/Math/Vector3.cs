﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Math.Vector3
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Math
{
  internal struct Vector3
  {
    public double X;
    public double Y;
    public double Z;

    public static Vector3 Zero
    {
      get
      {
        return new Vector3(0.0, 0.0, 0.0);
      }
    }

    public static Vector3 One
    {
      get
      {
        return new Vector3(1.0, 1.0, 1.0);
      }
    }

    public static Vector3 Up
    {
      get
      {
        return new Vector3(0.0, 1.0, 0.0);
      }
    }

    public static Vector3 Down
    {
      get
      {
        return new Vector3(0.0, -1.0, 0.0);
      }
    }

    public static Vector3 Right
    {
      get
      {
        return new Vector3(1.0, 0.0, 0.0);
      }
    }

    public static Vector3 Left
    {
      get
      {
        return new Vector3(-1.0, 0.0, 0.0);
      }
    }

    public static Vector3 ForwardLH
    {
      get
      {
        return new Vector3(0.0, 0.0, 1.0);
      }
    }

    public static Vector3 ForwardRH
    {
      get
      {
        return new Vector3(0.0, 0.0, -1.0);
      }
    }

    public Vector3(double x, double y, double z)
    {
      this = new Vector3();
      this.X = x;
      this.Y = y;
      this.Z = z;
    }

    public double[] ToArray()
    {
      return new double[3]{ this.X, this.Y, this.Z };
    }

    public float[] ToArrayF()
    {
      return new float[3]
      {
        (float) this.X,
        (float) this.Y,
        (float) this.Z
      };
    }

    public static bool operator ==(Vector3 left, Vector3 right)
    {
      if (left.X == right.X && left.Y == right.Y)
        return left.Z == right.Z;
      return false;
    }

    public static bool operator !=(Vector3 left, Vector3 right)
    {
      return !(left == right);
    }

    public static Vector3 operator +(Vector3 left, Vector3 right)
    {
      return new Vector3(left.X + right.X, left.Y + right.Y, left.Z + right.Z);
    }

    public static Vector3 operator -(Vector3 left, Vector3 right)
    {
      return new Vector3(left.X - right.X, left.Y - right.Y, left.Z - right.Z);
    }

    public static Vector3 operator *(Vector3 left, double scale)
    {
      return new Vector3(left.X * scale, left.Y * scale, left.Z * scale);
    }

    public static bool operator <(Vector3 left, Vector3 right)
    {
      if (left.X < right.X && left.Y < right.Y)
        return left.Z < right.Z;
      return false;
    }

    public static bool operator <=(Vector3 left, Vector3 right)
    {
      if (left.X <= right.X && left.Y <= right.Y)
        return left.Z <= right.Z;
      return false;
    }

    public static bool operator >(Vector3 left, Vector3 right)
    {
      if (left.X > right.X && left.Y > right.Y)
        return left.Z > right.Z;
      return false;
    }

    public static bool operator >=(Vector3 left, Vector3 right)
    {
      if (left.X >= right.X && left.Y >= right.Y)
        return left.Z >= right.Z;
      return false;
    }

    public Vector3 Normalize()
    {
      double num = this.Length();
      if (num != 0.0)
        return this * (1.0 / num);
      return Vector3.Zero;
    }

    public double Length()
    {
      return System.Math.Sqrt(this.LengthSquared());
    }

    public double LengthSquared()
    {
      return this.X * this.X + this.Y * this.Y + this.Z * this.Z;
    }

    public static double Dot(Vector3 left, Vector3 right)
    {
      return left.X * right.X + left.Y * right.Y + left.Z * right.Z;
    }

    public static Vector3 Transform(Vector3 left, Matrix right)
    {
      return (new Vector4(left, 1.0) * right).ToVector3();
    }

    public static Vector3 Cross(Vector3 left, Vector3 right)
    {
      return new Vector3(left.Y * right.Z - left.Z * right.Y, left.Z * right.X - left.X * right.Z, left.X * right.Y - left.Y * right.X);
    }

    public static Vector3 Transform(Vector3 left, Quaternion right)
    {
      return right.Rotate(left);
    }

    public static Vector3 Lerp(Vector3 start, Vector3 end, double t)
    {
      return start * (1.0 - t) + end * t;
    }
  }
}
