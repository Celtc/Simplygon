﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Math.Vector2
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Math
{
  internal struct Vector2
  {
    public double X;
    public double Y;

    public static Vector2 Zero
    {
      get
      {
        return new Vector2(0.0, 0.0);
      }
    }

    public static Vector2 One
    {
      get
      {
        return new Vector2(1.0, 1.0);
      }
    }

    public Vector2(double x, double y)
    {
      this = new Vector2();
      this.X = x;
      this.Y = y;
    }

    public double[] ToArray()
    {
      return new double[2]{ this.X, this.Y };
    }

    public float[] ToArrayF()
    {
      return new float[2]{ (float) this.X, (float) this.Y };
    }

    public static bool operator ==(Vector2 left, Vector2 right)
    {
      if (left.X == right.X)
        return left.Y == right.Y;
      return false;
    }

    public static bool operator !=(Vector2 left, Vector2 right)
    {
      return !(left == right);
    }

    public static Vector2 operator +(Vector2 left, Vector2 right)
    {
      return new Vector2(left.X + right.X, left.Y + right.Y);
    }

    public static Vector2 operator -(Vector2 left, Vector2 right)
    {
      return new Vector2(left.X - right.X, left.Y - right.Y);
    }

    public static Vector2 operator *(Vector2 left, double scale)
    {
      return new Vector2(left.X * scale, left.Y * scale);
    }

    public static bool operator <(Vector2 left, Vector2 right)
    {
      if (left.X < right.X)
        return left.Y < right.Y;
      return false;
    }

    public static bool operator <=(Vector2 left, Vector2 right)
    {
      if (left.X <= right.X)
        return left.Y <= right.Y;
      return false;
    }

    public static bool operator >(Vector2 left, Vector2 right)
    {
      if (left.X > right.X)
        return left.Y > right.Y;
      return false;
    }

    public static bool operator >=(Vector2 left, Vector2 right)
    {
      if (left.X >= right.X)
        return left.Y >= right.Y;
      return false;
    }

    public Vector2 Normalize()
    {
      double num = this.Length();
      if (num != 0.0)
        return this * (1.0 / num);
      return Vector2.Zero;
    }

    public double Length()
    {
      return System.Math.Sqrt(this.LengthSquared());
    }

    public double LengthSquared()
    {
      return this.X * this.X + this.Y * this.Y;
    }

    public static double Dot(Vector2 left, Vector2 right)
    {
      return left.X * right.X + left.Y * right.Y;
    }

    public static Vector2 Lerp(Vector2 start, Vector2 end, double t)
    {
      return start * (1.0 - t) + end * t;
    }
  }
}
