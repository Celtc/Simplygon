﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Math.MathHelper
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Math
{
  internal static class MathHelper
  {
    public static int Clamp(int value, int min, int max)
    {
      if (value < min)
        return min;
      if (value > max)
        return max;
      return value;
    }

    public static float Clamp(float value, float min, float max)
    {
      if ((double) value < (double) min)
        return min;
      if ((double) value > (double) max)
        return max;
      return value;
    }

    public static double Clamp(double value, double min, double max)
    {
      if (value < min)
        return min;
      if (value > max)
        return max;
      return value;
    }

    public static double SmoothStep(double min, double max, double value)
    {
      double num = MathHelper.Clamp((value - min) / (max - min), 0.0, 1.0);
      return num * num * (3.0 - 2.0 * num);
    }

    public static double DegreesToRadians(double degrees)
    {
      return degrees * (System.Math.PI / 180.0);
    }

    public static double RadianToDegrees(double radians)
    {
      return radians * 180.0 / System.Math.PI;
    }

    public static Vector3[] ComputeTangentAndBinormal(Vector2[] tv, Vector3[] v)
    {
      Vector3[] vector3Array = new Vector3[2];
      double num1 = tv[1].X - tv[0].X;
      double num2 = tv[2].X - tv[0].X;
      double num3 = tv[1].Y - tv[0].Y;
      double num4 = tv[2].Y - tv[0].Y;
      double num5 = num2 * num3 - num1 * num4;
      Vector3 vector3_1 = v[1] - v[0];
      Vector3 vector3_2 = v[2] - v[0];
      if (num5 != 0.0)
      {
        Vector3 vector3_3 = vector3_2 * (num3 / num5);
        Vector3 vector3_4 = vector3_1 * (num4 / num5);
        vector3Array[0] = (vector3_3 - vector3_4).Normalize();
      }
      else
        vector3Array[0] = num1 == 0.0 ? (num2 == 0.0 ? Vector3.Zero : (vector3_2 * (1.0 / num2)).Normalize()) : (vector3_1 * (1.0 / num1)).Normalize();
      Vector3 triangleNormal = MathHelper.ComputeTriangleNormal(v[0], v[1], v[2]);
      vector3Array[1] = Vector3.Cross(vector3Array[0], triangleNormal);
      return vector3Array;
    }

    public static Vector3 ComputeTriangleNormal(Vector3 v1, Vector3 v2, Vector3 v3)
    {
      Vector3 zero = Vector3.Zero;
      return Vector3.Cross(v2 - v1, v3 - v1).Normalize();
    }
  }
}
