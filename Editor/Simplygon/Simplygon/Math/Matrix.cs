﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Math.Matrix
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Math
{
  internal struct Matrix
  {
    public double M11;
    public double M12;
    public double M13;
    public double M14;
    public double M21;
    public double M22;
    public double M23;
    public double M24;
    public double M31;
    public double M32;
    public double M33;
    public double M34;
    public double M41;
    public double M42;
    public double M43;
    public double M44;

    public static Matrix Identity
    {
      get
      {
        return new Matrix(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0);
      }
    }

    public static Matrix Zero
    {
      get
      {
        return new Matrix(0.0);
      }
    }

    public static Matrix RotationX(double degrees)
    {
      double num = System.Math.Cos(MathHelper.DegreesToRadians(degrees));
      double m23 = System.Math.Sin(MathHelper.DegreesToRadians(degrees));
      return new Matrix(1.0, 0.0, 0.0, 0.0, 0.0, num, m23, 0.0, 0.0, -m23, num, 0.0, 0.0, 0.0, 0.0, 1.0);
    }

    public static Matrix RotationY(double degrees)
    {
      double num = System.Math.Cos(MathHelper.DegreesToRadians(degrees));
      double m31 = System.Math.Sin(MathHelper.DegreesToRadians(degrees));
      return new Matrix(num, 0.0, -m31, 0.0, 0.0, 1.0, 0.0, 0.0, m31, 0.0, num, 0.0, 0.0, 0.0, 0.0, 1.0);
    }

    public static Matrix RotationZ(double degrees)
    {
      double num = System.Math.Cos(MathHelper.DegreesToRadians(degrees));
      double m12 = System.Math.Sin(MathHelper.DegreesToRadians(degrees));
      return new Matrix(num, m12, 0.0, 0.0, -m12, num, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0);
    }

    public Matrix(double value)
    {
      this = new Matrix();
      this.M11 = value;
      this.M12 = value;
      this.M13 = value;
      this.M14 = value;
      this.M21 = value;
      this.M22 = value;
      this.M23 = value;
      this.M24 = value;
      this.M31 = value;
      this.M32 = value;
      this.M33 = value;
      this.M34 = value;
      this.M41 = value;
      this.M42 = value;
      this.M43 = value;
      this.M44 = value;
    }

    public Matrix(double[] values)
    {
      this = new Matrix();
      this.M11 = values[0];
      this.M12 = values[1];
      this.M13 = values[2];
      this.M14 = values[3];
      this.M21 = values[4];
      this.M22 = values[5];
      this.M23 = values[6];
      this.M24 = values[7];
      this.M31 = values[8];
      this.M32 = values[9];
      this.M33 = values[10];
      this.M34 = values[11];
      this.M41 = values[12];
      this.M42 = values[13];
      this.M43 = values[14];
      this.M44 = values[15];
    }

    public Matrix(float[] values)
    {
      this = new Matrix();
      this.M11 = (double) values[0];
      this.M12 = (double) values[1];
      this.M13 = (double) values[2];
      this.M14 = (double) values[3];
      this.M21 = (double) values[4];
      this.M22 = (double) values[5];
      this.M23 = (double) values[6];
      this.M24 = (double) values[7];
      this.M31 = (double) values[8];
      this.M32 = (double) values[9];
      this.M33 = (double) values[10];
      this.M34 = (double) values[11];
      this.M41 = (double) values[12];
      this.M42 = (double) values[13];
      this.M43 = (double) values[14];
      this.M44 = (double) values[15];
    }

    public Matrix(double m11, double m12, double m13, double m14, double m21, double m22, double m23, double m24, double m31, double m32, double m33, double m34, double m41, double m42, double m43, double m44)
    {
      this = new Matrix();
      this.M11 = m11;
      this.M12 = m12;
      this.M13 = m13;
      this.M14 = m14;
      this.M21 = m21;
      this.M22 = m22;
      this.M23 = m23;
      this.M24 = m24;
      this.M31 = m31;
      this.M32 = m32;
      this.M33 = m33;
      this.M34 = m34;
      this.M41 = m41;
      this.M42 = m42;
      this.M43 = m43;
      this.M44 = m44;
    }

    public double[] ToArray()
    {
      return new double[16]
      {
        this.M11,
        this.M12,
        this.M13,
        this.M14,
        this.M21,
        this.M22,
        this.M23,
        this.M24,
        this.M31,
        this.M32,
        this.M33,
        this.M34,
        this.M41,
        this.M42,
        this.M43,
        this.M44
      };
    }

    public float[] ToArrayF()
    {
      return new float[16]
      {
        (float) this.M11,
        (float) this.M12,
        (float) this.M13,
        (float) this.M14,
        (float) this.M21,
        (float) this.M22,
        (float) this.M23,
        (float) this.M24,
        (float) this.M31,
        (float) this.M32,
        (float) this.M33,
        (float) this.M34,
        (float) this.M41,
        (float) this.M42,
        (float) this.M43,
        (float) this.M44
      };
    }

    public static Matrix operator -(Matrix value)
    {
      return value * -1.0;
    }

    public static Matrix operator -(Matrix left, Matrix right)
    {
      return new Matrix(left.M11 - right.M11, left.M12 - right.M11, left.M13 - right.M13, left.M14 - right.M14, left.M21 - right.M21, left.M22 - right.M22, left.M23 - right.M23, left.M24 - right.M24, left.M31 - right.M31, left.M32 - right.M32, left.M33 - right.M33, left.M34 - right.M34, left.M41 - right.M41, left.M42 - right.M42, left.M43 - right.M43, left.M44 - right.M44);
    }

    public static bool operator !=(Matrix left, Matrix right)
    {
      return !(left == right);
    }

    public static Matrix operator *(Matrix left, double right)
    {
      return new Matrix(left.M11 * right, left.M12 * right, left.M13 * right, left.M14 * right, left.M21 * right, left.M22 * right, left.M23 * right, left.M24 * right, left.M31 * right, left.M32 * right, left.M33 * right, left.M34 * right, left.M41 * right, left.M42 * right, left.M43 * right, left.M44 * right);
    }

    public static Matrix operator *(Matrix left, Vector4 right)
    {
      return new Matrix(left.M11 * right.X, left.M12 * right.Y, left.M13 * right.Z, left.M14 * right.W, left.M21 * right.X, left.M22 * right.Y, left.M23 * right.Z, left.M24 * right.W, left.M31 * right.X, left.M32 * right.Y, left.M33 * right.Z, left.M34 * right.W, left.M41 * right.X, left.M42 * right.Y, left.M43 * right.Z, left.M44 * right.W);
    }

    public static Matrix operator *(Matrix left, Matrix right)
    {
      double m11 = left.M11 * right.M11 + left.M12 * right.M21 + left.M13 * right.M31 + left.M14 * right.M41;
      double num1 = left.M11 * right.M12 + left.M12 * right.M22 + left.M13 * right.M32 + left.M14 * right.M42;
      double num2 = left.M11 * right.M13 + left.M12 * right.M23 + left.M13 * right.M33 + left.M14 * right.M43;
      double num3 = left.M11 * right.M14 + left.M12 * right.M24 + left.M13 * right.M34 + left.M14 * right.M44;
      double num4 = left.M21 * right.M11 + left.M22 * right.M21 + left.M23 * right.M31 + left.M24 * right.M41;
      double num5 = left.M21 * right.M12 + left.M22 * right.M22 + left.M23 * right.M32 + left.M24 * right.M42;
      double num6 = left.M21 * right.M13 + left.M22 * right.M23 + left.M23 * right.M33 + left.M24 * right.M43;
      double num7 = left.M21 * right.M14 + left.M22 * right.M24 + left.M23 * right.M34 + left.M24 * right.M44;
      double num8 = left.M31 * right.M11 + left.M32 * right.M21 + left.M33 * right.M31 + left.M34 * right.M41;
      double num9 = left.M31 * right.M12 + left.M32 * right.M22 + left.M33 * right.M32 + left.M34 * right.M42;
      double num10 = left.M31 * right.M13 + left.M32 * right.M23 + left.M33 * right.M33 + left.M34 * right.M43;
      double num11 = left.M31 * right.M14 + left.M32 * right.M24 + left.M33 * right.M34 + left.M34 * right.M44;
      double num12 = left.M41 * right.M11 + left.M42 * right.M21 + left.M43 * right.M31 + left.M44 * right.M41;
      double num13 = left.M41 * right.M12 + left.M42 * right.M22 + left.M43 * right.M32 + left.M44 * right.M42;
      double num14 = left.M41 * right.M13 + left.M42 * right.M23 + left.M43 * right.M33 + left.M44 * right.M43;
      double num15 = left.M41 * right.M14 + left.M42 * right.M24 + left.M43 * right.M34 + left.M44 * right.M44;
      double m12 = num1;
      double m13 = num2;
      double m14 = num3;
      double m21 = num4;
      double m22 = num5;
      double m23 = num6;
      double m24 = num7;
      double m31 = num8;
      double m32 = num9;
      double m33 = num10;
      double m34 = num11;
      double m41 = num12;
      double m42 = num13;
      double m43 = num14;
      double m44 = num15;
      return new Matrix(m11, m12, m13, m14, m21, m22, m23, m24, m31, m32, m33, m34, m41, m42, m43, m44);
    }

    public static Matrix operator +(Matrix left, Matrix right)
    {
      return new Matrix(left.M11 + right.M11, left.M12 + right.M11, left.M13 + right.M13, left.M14 + right.M14, left.M21 + right.M21, left.M22 + right.M22, left.M23 + right.M23, left.M24 + right.M24, left.M31 + right.M31, left.M32 + right.M32, left.M33 + right.M33, left.M34 + right.M34, left.M41 + right.M41, left.M42 + right.M42, left.M43 + right.M43, left.M44 + right.M44);
    }

    public static bool operator ==(Matrix left, Matrix right)
    {
      if (left.M11 == right.M11 && left.M12 == right.M11 && (left.M13 == right.M13 && left.M14 == right.M14) && (left.M21 == right.M21 && left.M22 == right.M22 && (left.M23 == right.M23 && left.M24 == right.M24)) && (left.M31 == right.M31 && left.M32 == right.M32 && (left.M33 == right.M33 && left.M34 == right.M34) && (left.M41 == right.M41 && left.M42 == right.M42 && left.M43 == right.M43)))
        return left.M44 == right.M44;
      return false;
    }

    public Vector4 Column1
    {
      get
      {
        return new Vector4(this.M11, this.M21, this.M31, this.M41);
      }
      set
      {
        this.M11 = value.X;
        this.M21 = value.Y;
        this.M31 = value.Z;
        this.M41 = value.W;
      }
    }

    public Vector4 Column2
    {
      get
      {
        return new Vector4(this.M12, this.M22, this.M32, this.M42);
      }
      set
      {
        this.M12 = value.X;
        this.M22 = value.Y;
        this.M32 = value.Z;
        this.M42 = value.W;
      }
    }

    public Vector4 Column3
    {
      get
      {
        return new Vector4(this.M13, this.M23, this.M33, this.M43);
      }
      set
      {
        this.M13 = value.X;
        this.M23 = value.Y;
        this.M33 = value.Z;
        this.M43 = value.W;
      }
    }

    public Vector4 Column4
    {
      get
      {
        return new Vector4(this.M14, this.M24, this.M34, this.M44);
      }
      set
      {
        this.M14 = value.X;
        this.M24 = value.Y;
        this.M34 = value.Z;
        this.M44 = value.W;
      }
    }

    public Vector4 Row1
    {
      get
      {
        return new Vector4(this.M11, this.M12, this.M13, this.M14);
      }
      set
      {
        this.M11 = value.X;
        this.M12 = value.Y;
        this.M13 = value.Z;
        this.M14 = value.W;
      }
    }

    public Vector4 Row2
    {
      get
      {
        return new Vector4(this.M21, this.M22, this.M23, this.M24);
      }
      set
      {
        this.M21 = value.X;
        this.M22 = value.Y;
        this.M23 = value.Z;
        this.M24 = value.W;
      }
    }

    public Vector4 Row3
    {
      get
      {
        return new Vector4(this.M31, this.M32, this.M33, this.M34);
      }
      set
      {
        this.M31 = value.X;
        this.M32 = value.Y;
        this.M33 = value.Z;
        this.M34 = value.W;
      }
    }

    public Vector4 Row4
    {
      get
      {
        return new Vector4(this.M41, this.M42, this.M43, this.M44);
      }
      set
      {
        this.M41 = value.X;
        this.M42 = value.Y;
        this.M43 = value.Z;
        this.M44 = value.W;
      }
    }

    public Vector3 ScaleVector
    {
      get
      {
        return new Vector3(this.M11, this.M22, this.M33);
      }
    }

    public Vector3 TranslationVector
    {
      get
      {
        return new Vector3(this.M41, this.M42, this.M43);
      }
    }

    public double Determinant()
    {
      return this.M11 * this.M22 * this.M33 * this.M44 + this.M11 * this.M23 * this.M34 * this.M42 + this.M11 * this.M24 * this.M32 * this.M43 + this.M12 * this.M21 * this.M34 * this.M43 + this.M12 * this.M23 * this.M31 * this.M44 + this.M12 * this.M24 * this.M33 * this.M41 + this.M13 * this.M21 * this.M32 * this.M44 + this.M13 * this.M22 * this.M34 * this.M41 + this.M13 * this.M24 * this.M31 * this.M42 + this.M14 * this.M21 * this.M33 * this.M42 + this.M14 * this.M22 * this.M31 * this.M43 + this.M14 * this.M23 * this.M32 * this.M41 - this.M11 * this.M22 * this.M34 * this.M43 - this.M11 * this.M23 * this.M32 * this.M44 - this.M11 * this.M24 * this.M33 * this.M42 - this.M12 * this.M21 * this.M33 * this.M44 - this.M12 * this.M23 * this.M34 * this.M41 - this.M12 * this.M24 * this.M31 * this.M43 - this.M13 * this.M21 * this.M34 * this.M42 - this.M13 * this.M22 * this.M31 * this.M44 - this.M13 * this.M24 * this.M32 * this.M41 - this.M14 * this.M21 * this.M32 * this.M43 - this.M14 * this.M22 * this.M33 * this.M41 - this.M14 * this.M23 * this.M31 * this.M42;
    }

    public bool Equals(Matrix other)
    {
      return this == other;
    }

    public override bool Equals(object obj)
    {
      if (obj is Matrix)
        return this.Equals((Matrix) obj);
      return false;
    }

    public static Matrix Add(Matrix left, Matrix right)
    {
      return left + right;
    }

    public static Matrix Subtract(Matrix left, Matrix right)
    {
      return left - right;
    }

    public static Matrix Multiply(Matrix left, double right)
    {
      return left * right;
    }

    public static Matrix Multiply(Matrix left, Matrix right)
    {
      return left * right;
    }

    public static Matrix Scale(Matrix left, Vector3 scale)
    {
      return new Matrix(left.M11 * scale.X, left.M12, left.M13, left.M14, left.M21, left.M22 * scale.Y, left.M23, left.M24, left.M31, left.M32, left.M33 * scale.Z, left.M34, left.M41, left.M42, left.M43, left.M44);
    }

    public static Matrix ScaleMatrix(Vector3 scale)
    {
      return new Matrix(scale.X, 0.0, 0.0, 0.0, 0.0, scale.Y, 0.0, 0.0, 0.0, 0.0, scale.Z, 0.0, 0.0, 0.0, 0.0, 1.0);
    }

    public static Matrix Translate(Matrix left, Vector3 translation)
    {
      return new Matrix(left.M11, left.M12, left.M13, left.M14, left.M21, left.M22, left.M23, left.M24, left.M31, left.M32, left.M33, left.M34, translation.X, translation.Y, translation.Z, left.M44);
    }

    public static Matrix TranslationMatrix(Vector3 translation)
    {
      return new Matrix(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, translation.X, translation.Y, translation.Z, 1.0);
    }

    public static Matrix Transpose(Matrix value)
    {
      return new Matrix(value.M11, value.M21, value.M31, value.M41, value.M12, value.M22, value.M32, value.M42, value.M13, value.M23, value.M33, value.M43, value.M14, value.M24, value.M34, value.M44);
    }

    public static Matrix Inverse(Matrix value)
    {
      double num1 = value.Determinant();
      if (num1 == 0.0)
        return Matrix.Identity;
      double m11 = value.M22 * value.M33 * value.M44 + value.M23 * value.M34 * value.M42 + value.M24 * value.M32 * value.M43 - value.M22 * value.M34 * value.M43 - value.M23 * value.M32 * value.M44 - value.M24 * value.M33 * value.M42;
      double num2 = value.M12 * value.M34 * value.M43 + value.M13 * value.M32 * value.M44 + value.M14 * value.M33 * value.M42 - value.M12 * value.M33 * value.M44 - value.M13 * value.M34 * value.M42 - value.M14 * value.M32 * value.M43;
      double num3 = value.M12 * value.M23 * value.M44 + value.M13 * value.M24 * value.M42 + value.M14 * value.M22 * value.M43 - value.M12 * value.M24 * value.M43 - value.M13 * value.M22 * value.M44 - value.M14 * value.M23 * value.M42;
      double num4 = value.M12 * value.M24 * value.M33 + value.M13 * value.M22 * value.M34 + value.M14 * value.M23 * value.M32 - value.M12 * value.M23 * value.M34 - value.M13 * value.M24 * value.M32 - value.M14 * value.M22 * value.M33;
      double num5 = value.M21 * value.M34 * value.M43 + value.M23 * value.M31 * value.M44 + value.M24 * value.M33 * value.M41 - value.M21 * value.M33 * value.M44 - value.M23 * value.M34 * value.M41 - value.M24 * value.M31 * value.M43;
      double num6 = value.M11 * value.M33 * value.M44 + value.M13 * value.M34 * value.M41 + value.M14 * value.M31 * value.M43 - value.M11 * value.M34 * value.M43 - value.M13 * value.M31 * value.M44 - value.M14 * value.M33 * value.M41;
      double num7 = value.M11 * value.M24 * value.M43 + value.M13 * value.M21 * value.M44 + value.M14 * value.M23 * value.M41 - value.M11 * value.M23 * value.M44 - value.M13 * value.M24 * value.M41 - value.M14 * value.M21 * value.M43;
      double num8 = value.M11 * value.M23 * value.M34 + value.M13 * value.M24 * value.M31 + value.M14 * value.M21 * value.M33 - value.M11 * value.M24 * value.M33 - value.M13 * value.M21 * value.M34 - value.M14 * value.M23 * value.M31;
      double num9 = value.M21 * value.M32 * value.M44 + value.M22 * value.M34 * value.M41 + value.M24 * value.M31 * value.M42 - value.M21 * value.M34 * value.M42 - value.M22 * value.M31 * value.M44 - value.M24 * value.M32 * value.M41;
      double num10 = value.M11 * value.M34 * value.M42 + value.M12 * value.M31 * value.M44 + value.M14 * value.M32 * value.M41 - value.M11 * value.M32 * value.M44 - value.M12 * value.M34 * value.M41 - value.M14 * value.M31 * value.M42;
      double num11 = value.M11 * value.M22 * value.M44 + value.M12 * value.M24 * value.M41 + value.M14 * value.M21 * value.M42 - value.M11 * value.M24 * value.M42 - value.M12 * value.M21 * value.M44 - value.M14 * value.M22 * value.M41;
      double num12 = value.M11 * value.M24 * value.M32 + value.M12 * value.M21 * value.M34 + value.M14 * value.M22 * value.M31 - value.M11 * value.M22 * value.M34 - value.M12 * value.M24 * value.M31 - value.M14 * value.M21 * value.M32;
      double num13 = value.M21 * value.M33 * value.M42 + value.M22 * value.M31 * value.M43 + value.M23 * value.M32 * value.M41 - value.M21 * value.M32 * value.M43 - value.M22 * value.M33 * value.M41 - value.M23 * value.M31 * value.M42;
      double num14 = value.M11 * value.M32 * value.M43 + value.M12 * value.M33 * value.M41 + value.M13 * value.M31 * value.M42 - value.M11 * value.M33 * value.M42 - value.M12 * value.M31 * value.M43 - value.M13 * value.M32 * value.M41;
      double num15 = value.M11 * value.M23 * value.M42 + value.M12 * value.M21 * value.M43 + value.M13 * value.M22 * value.M41 - value.M11 * value.M22 * value.M43 - value.M12 * value.M23 * value.M41 - value.M13 * value.M21 * value.M42;
      double num16 = value.M11 * value.M22 * value.M33 + value.M12 * value.M23 * value.M31 + value.M13 * value.M21 * value.M32 - value.M11 * value.M23 * value.M32 - value.M12 * value.M21 * value.M33 - value.M13 * value.M22 * value.M31;
      double m12 = num2;
      double m13 = num3;
      double m14 = num4;
      double m21 = num5;
      double m22 = num6;
      double m23 = num7;
      double m24 = num8;
      double m31 = num9;
      double m32 = num10;
      double m33 = num11;
      double m34 = num12;
      double m41 = num13;
      double m42 = num14;
      double m43 = num15;
      double m44 = num16;
      return new Matrix(m11, m12, m13, m14, m21, m22, m23, m24, m31, m32, m33, m34, m41, m42, m43, m44) * (1.0 / num1);
    }

    public static Matrix CreateFromQuaternion(Quaternion quaternion)
    {
      double x = quaternion.X;
      double y = quaternion.Y;
      double z = quaternion.Z;
      double w = quaternion.W;
      double num1 = x * x + y * y + z * z + w * w;
      double num2 = num1 == 0.0 ? 0.0 : 2.0 / num1;
      double num3 = num2 * w * x;
      double num4 = num2 * w * y;
      double num5 = num2 * w * z;
      double num6 = num2 * x * x;
      double num7 = num2 * x * y;
      double num8 = num2 * x * z;
      double num9 = num2 * y * y;
      double num10 = num2 * y * z;
      double num11 = num2 * z * z;
      double m11 = 1.0 - (num9 + num11);
      double num12 = num7 - num5;
      double num13 = num8 + num4;
      double num14 = 0.0;
      double num15 = num7 + num5;
      double num16 = 1.0 - (num6 + num11);
      double num17 = num10 - num3;
      double num18 = 0.0;
      double num19 = num8 - num4;
      double num20 = num10 + num3;
      double num21 = 1.0 - (num6 + num9);
      double num22 = 0.0;
      double num23 = 0.0;
      double num24 = 0.0;
      double num25 = 0.0;
      double num26 = 1.0;
      double m12 = num12;
      double m13 = num13;
      double m14 = num14;
      double m21 = num15;
      double m22 = num16;
      double m23 = num17;
      double m24 = num18;
      double m31 = num19;
      double m32 = num20;
      double m33 = num21;
      double m34 = num22;
      double m41 = num23;
      double m42 = num24;
      double m43 = num25;
      double m44 = num26;
      return new Matrix(m11, m12, m13, m14, m21, m22, m23, m24, m31, m32, m33, m34, m41, m42, m43, m44);
    }

    public static Matrix PerspectiveFovLH(double fieldOfView, double aspectRatio, double nearZ, double farZ)
    {
      double m22 = 1.0 / System.Math.Tan(fieldOfView / 2.0);
      return new Matrix(m22 * aspectRatio, 0.0, 0.0, 0.0, 0.0, m22, 0.0, 0.0, 0.0, 0.0, farZ / (farZ - nearZ), 1.0, 0.0, 0.0, -nearZ * farZ / (farZ - nearZ), 0.0);
    }

    public static Matrix LookAtLH(Vector3 cameraPosition, Vector3 cameraTarget, Vector3 cameraUpVector)
    {
      Vector3 vector3_1 = (cameraTarget - cameraPosition).Normalize();
      Vector3 vector3_2 = Vector3.Cross(cameraUpVector, vector3_1).Normalize();
      Vector3 left = Vector3.Cross(vector3_1, vector3_2);
      return new Matrix(vector3_2.X, left.X, vector3_1.X, 0.0, vector3_2.Y, left.Y, vector3_1.Y, 0.0, vector3_2.Z, left.Z, vector3_1.Z, 0.0, -Vector3.Dot(vector3_2, cameraPosition), -Vector3.Dot(left, cameraPosition), -Vector3.Dot(vector3_1, cameraPosition), 1.0);
    }

    public static Matrix ChangeCoordinateSystem(Matrix srcMatrix)
    {
      return Matrix.Transpose(new Matrix(srcMatrix.M11, srcMatrix.M12, -srcMatrix.M13, srcMatrix.M14, srcMatrix.M21, srcMatrix.M22, -srcMatrix.M23, srcMatrix.M24, -srcMatrix.M31, -srcMatrix.M32, srcMatrix.M33, -srcMatrix.M34, srcMatrix.M41, srcMatrix.M42, -srcMatrix.M43, srcMatrix.M44));
    }
  }
}
