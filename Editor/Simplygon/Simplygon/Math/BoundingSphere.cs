﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Math.BoundingSphere
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Math
{
  internal struct BoundingSphere
  {
    public Vector3 Center { get; set; }

    public double Radius { get; set; }

    public BoundingSphere(Vector3 center, double radius)
    {
      this = new BoundingSphere();
      this.Center = center;
      this.Radius = radius;
    }

    public static bool operator ==(BoundingSphere left, BoundingSphere right)
    {
      if (left.Center == right.Center)
        return left.Radius == right.Radius;
      return false;
    }

    public static bool operator !=(BoundingSphere left, BoundingSphere right)
    {
      return !(left == right);
    }
  }
}
