﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Math.OpenTK.Extensions
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Math.OpenTK
{
  internal static class Extensions
  {
    public static Vector4[] ToARGB(this Color[] array)
    {
      Vector4[] vector4Array = new Vector4[array.Length];
      for (int index = 0; index < array.Length; ++index)
        vector4Array[index] = array[index].ToARGB();
      return vector4Array;
    }
  }
}
