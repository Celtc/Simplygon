﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Math.Quaternion
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Math
{
  internal struct Quaternion
  {
    public static Quaternion Identity
    {
      get
      {
        return new Quaternion(0.0, 0.0, 0.0, 1.0);
      }
    }

    public double X { get; set; }

    public double Y { get; set; }

    public double Z { get; set; }

    public double W { get; set; }

    public Quaternion(double value)
    {
      this = new Quaternion();
      this.X = this.Y = this.Z = this.W = value;
    }

    public Quaternion(double[] values)
    {
      this = new Quaternion();
      this.X = values[0];
      this.Y = values[1];
      this.Z = values[2];
      this.W = values[3];
    }

    public Quaternion(Vector4 value)
    {
      this = new Quaternion();
      this.X = value.X;
      this.Y = value.Y;
      this.Z = value.Z;
      this.W = value.W;
    }

    public Quaternion(Vector3 value, double w)
    {
      this = new Quaternion();
      this.X = value.X;
      this.Y = value.Y;
      this.Z = value.Z;
      this.W = w;
    }

    public Quaternion(double x, double y, double z, double w)
    {
      this = new Quaternion();
      this.X = x;
      this.Y = y;
      this.Z = z;
      this.W = w;
    }

    public static Quaternion operator *(Quaternion left, Quaternion right)
    {
      double x = left.X * right.W + left.Y * right.Z - left.Z * right.Y + left.W * right.X;
      double num1 = -left.X * right.Z + left.Y * right.W + left.Z * right.X + left.W * right.Y;
      double num2 = left.X * right.Y - left.Y * right.X + left.Z * right.W + left.W * right.Z;
      double num3 = -left.X * right.X - left.Y * right.Y - left.Z * right.Z + left.W * right.W;
      double y = num1;
      double z = num2;
      double w = num3;
      return new Quaternion(x, y, z, w);
    }

    public static Quaternion operator *(Quaternion left, double right)
    {
      return new Quaternion(left.X * right, left.Y * right, left.Z * right, left.W * right);
    }

    public double[] ToArray()
    {
      return new double[4]{ this.X, this.Y, this.Z, this.W };
    }

    public Quaternion Normalize()
    {
      double x1 = this.X;
      double y1 = this.Y;
      double z1 = this.Z;
      double w1 = this.W;
      double d = x1 * x1 + y1 * y1 + z1 * z1 + w1 * w1;
      double x2;
      double y2;
      double z2;
      double w2;
      if (d > 0.001)
      {
        double num = System.Math.Sqrt(d);
        x2 = x1 / num;
        y2 = y1 / num;
        z2 = z1 / num;
        w2 = w1 / num;
      }
      else
      {
        x2 = 0.0;
        y2 = 0.0;
        z2 = 0.0;
        w2 = 1.0;
      }
      return new Quaternion(x2, y2, z2, w2);
    }

    public Quaternion Conjugate()
    {
      return new Quaternion(-this.X, -this.Y, -this.Z, this.W);
    }

    public Vector3 Rotate(Vector3 axis)
    {
      Quaternion quaternion1 = this.Normalize();
      Quaternion quaternion2 = quaternion1.Conjugate();
      Quaternion quaternion3 = new Quaternion(axis, 0.0);
      Quaternion quaternion4 = quaternion1 * quaternion3 * quaternion2;
      return new Vector3(quaternion4.X, quaternion4.Y, quaternion4.Z);
    }

    public static Quaternion FromAxisAngle(Vector3 axis, double angleRadian)
    {
      double num1 = axis.Length();
      double x;
      double y;
      double z;
      double w;
      if (num1 > 0.0001)
      {
        double num2 = System.Math.Cos(angleRadian / 2.0);
        double num3 = System.Math.Sin(angleRadian / 2.0);
        x = axis.X / num1 * num3;
        y = axis.Y / num1 * num3;
        z = axis.Z / num1 * num3;
        w = num2;
      }
      else
      {
        x = 0.0;
        y = 0.0;
        z = 0.0;
        w = 1.0;
      }
      return new Quaternion(x, y, z, w);
    }

    public static Quaternion Multiply(Quaternion left, Quaternion right)
    {
      return left * right;
    }
  }
}
