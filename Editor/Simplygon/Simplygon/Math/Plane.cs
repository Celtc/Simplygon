﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Math.Plane
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Math
{
  internal struct Plane
  {
    public Vector3 Point;
    public Vector3 Normal;

    public Plane(Vector3 point, Vector3 normal)
    {
      this = new Plane();
      this.Point = point;
      this.Normal = normal;
    }
  }
}
