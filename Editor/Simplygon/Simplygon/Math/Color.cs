﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Math.Color
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.Math
{
  internal struct Color
  {
    private static Color simplygon = new Color(1f, 0.9960784f, 0.0f, 1f, 32);
    private static Color[] lods = new Color[14]
    {
      new Color(1f, 1f, 1f, 1f, 32),
      new Color(0.1529412f, 0.6627451f, 0.8784314f, 1f, 32),
      new Color(0.2156863f, 0.8666667f, 0.2156863f, 1f, 32),
      new Color(1f, 0.0f, 0.3882353f, 1f, 32),
      new Color(0.8666667f, 0.6078432f, 0.2156863f, 1f, 32),
      new Color(0.0f, 0.4588235f, 0.6784314f, 1f, 32),
      new Color(0.8666667f, 0.2156863f, 0.8666667f, 1f, 32),
      new Color(0.4117647f, 0.5686275f, 0.3411765f, 1f, 32),
      new Color(0.2313726f, 0.2784314f, 0.7372549f, 1f, 32),
      new Color(0.7254902f, 0.8745098f, 0.1215686f, 1f, 32),
      new Color(0.6235294f, 0.2509804f, 0.7490196f, 1f, 32),
      new Color(0.3137255f, 0.0f, 0.3882353f, 1f, 32),
      new Color(0.1607843f, 0.3176471f, 0.09019608f, 1f, 32),
      new Color(0.0f, 0.3333333f, 0.3294118f, 1f, 32)
    };
    private static Color[] NamedColors = new Color[173]
    {
      new Color(0.7f, 0.7f, 0.7f, 1f, 32),
      new Color(0.6f, 0.7f, 0.8f, 1f, 32),
      new Color(0.0f, 0.0f, 0.0f, 1f, 32),
      new Color(0.9f, 1f, 1f, 1f, 32),
      new Color(1f, 0.9f, 0.8f, 1f, 32),
      new Color(0.7f, 0.7f, 0.7f, 1f, 32),
      new Color(0.0f, 1f, 1f, 1f, 32),
      new Color(0.5f, 1f, 0.8f, 1f, 32),
      new Color(0.9f, 1f, 1f, 1f, 32),
      new Color(1f, 1f, 0.9f, 1f, 32),
      new Color(1f, 0.9f, 0.8f, 1f, 32),
      new Color(0.0f, 0.0f, 0.0f, 1f, 32),
      new Color(1f, 0.9f, 0.8f, 1f, 32),
      new Color(0.0f, 0.0f, 1f, 1f, 32),
      new Color(0.5f, 0.2f, 0.9f, 1f, 32),
      new Color(0.6f, 0.2f, 0.2f, 1f, 32),
      new Color(0.9f, 0.7f, 0.5f, 1f, 32),
      new Color(0.9f, 0.9f, 0.9f, 1f, 32),
      new Color(1f, 1f, 1f, 1f, 32),
      new Color(0.6f, 0.6f, 0.6f, 1f, 32),
      new Color(0.4f, 0.6f, 0.6f, 1f, 32),
      new Color(0.5f, 1f, 0.0f, 1f, 32),
      new Color(0.8f, 0.4f, 0.1f, 1f, 32),
      new Color(0.9f, 0.9f, 0.9f, 1f, 32),
      new Color(0.6f, 0.6f, 0.6f, 1f, 32),
      new Color(0.4f, 0.4f, 0.4f, 1f, 32),
      new Color(0.9f, 0.9f, 0.9f, 1f, 32),
      new Color(1f, 1f, 1f, 1f, 32),
      new Color(0.0f, 0.0f, 0.0f, 1f, 32),
      new Color(1f, 0.5f, 0.3f, 1f, 32),
      new Color(0.4f, 0.6f, 0.9f, 1f, 32),
      new Color(1f, 1f, 0.9f, 1f, 32),
      new Color(0.9f, 0.1f, 0.2f, 1f, 32),
      new Color(0.0f, 1f, 1f, 1f, 32),
      new Color(0.0f, 0.0f, 0.5f, 1f, 32),
      new Color(0.0f, 0.5f, 0.5f, 1f, 32),
      new Color(0.7f, 0.5f, 0.0f, 1f, 32),
      new Color(0.7f, 0.7f, 0.7f, 1f, 32),
      new Color(0.0f, 0.4f, 0.0f, 1f, 32),
      new Color(0.7f, 0.7f, 0.4f, 1f, 32),
      new Color(0.5f, 0.0f, 0.5f, 1f, 32),
      new Color(0.3f, 0.4f, 0.2f, 1f, 32),
      new Color(1f, 0.5f, 0.0f, 1f, 32),
      new Color(0.6f, 0.2f, 0.8f, 1f, 32),
      new Color(0.5f, 0.0f, 0.0f, 1f, 32),
      new Color(0.9f, 0.6f, 0.5f, 1f, 32),
      new Color(0.6f, 0.7f, 0.5f, 1f, 32),
      new Color(0.3f, 0.2f, 0.5f, 1f, 32),
      new Color(0.2f, 0.3f, 0.3f, 1f, 32),
      new Color(0.0f, 0.8f, 0.8f, 1f, 32),
      new Color(0.6f, 0.0f, 0.8f, 1f, 32),
      new Color(1f, 0.1f, 0.6f, 1f, 32),
      new Color(0.0f, 0.7f, 1f, 1f, 32),
      new Color(0.0f, 0.0f, 0.0f, 1f, 32),
      new Color(0.4f, 0.4f, 0.4f, 1f, 32),
      new Color(0.1f, 0.6f, 1f, 1f, 32),
      new Color(0.7f, 0.1f, 0.1f, 1f, 32),
      new Color(1f, 1f, 0.9f, 1f, 32),
      new Color(0.1f, 0.5f, 0.1f, 1f, 32),
      new Color(1f, 0.0f, 1f, 1f, 32),
      new Color(0.9f, 0.9f, 0.9f, 1f, 32),
      new Color(1f, 1f, 1f, 1f, 32),
      new Color(1f, 0.8f, 0.0f, 1f, 32),
      new Color(0.9f, 0.6f, 0.1f, 1f, 32),
      new Color(0.7f, 0.8f, 0.9f, 1f, 32),
      new Color(0.8f, 0.9f, 0.9f, 1f, 32),
      new Color(0.5f, 0.5f, 0.5f, 1f, 32),
      new Color(0.4f, 0.4f, 0.4f, 1f, 32),
      new Color(0.0f, 0.5f, 0.0f, 1f, 32),
      new Color(0.7f, 1f, 0.2f, 1f, 32),
      new Color(0.2f, 0.6f, 1f, 1f, 32),
      new Color(1f, 1f, 1f, 1f, 32),
      new Color(0.9f, 1f, 0.9f, 1f, 32),
      new Color(1f, 0.4f, 0.7f, 1f, 32),
      new Color(0.0f, 0.4f, 0.8f, 1f, 32),
      new Color(1f, 1f, 1f, 1f, 32),
      new Color(0.7f, 0.8f, 0.9f, 1f, 32),
      new Color(0.0f, 0.0f, 0.0f, 1f, 32),
      new Color(0.8f, 0.4f, 0.4f, 1f, 32),
      new Color(0.3f, 0.0f, 0.5f, 1f, 32),
      new Color(1f, 1f, 0.9f, 1f, 32),
      new Color(0.0f, 0.0f, 0.0f, 1f, 32),
      new Color(1f, 1f, 0.9f, 1f, 32),
      new Color(0.9f, 0.9f, 0.5f, 1f, 32),
      new Color(0.9f, 0.9f, 1f, 1f, 32),
      new Color(1f, 0.9f, 1f, 1f, 32),
      new Color(0.5f, 1f, 0.0f, 1f, 32),
      new Color(1f, 1f, 0.8f, 1f, 32),
      new Color(0.7f, 0.8f, 0.9f, 1f, 32),
      new Color(0.9f, 0.5f, 0.5f, 1f, 32),
      new Color(0.9f, 1f, 1f, 1f, 32),
      new Color(1f, 1f, 0.8f, 1f, 32),
      new Color(0.8f, 0.8f, 0.8f, 1f, 32),
      new Color(0.6f, 0.9f, 0.6f, 1f, 32),
      new Color(1f, 0.7f, 0.8f, 1f, 32),
      new Color(1f, 0.6f, 0.5f, 1f, 32),
      new Color(0.1f, 0.7f, 0.7f, 1f, 32),
      new Color(0.5f, 0.8f, 1f, 1f, 32),
      new Color(0.5f, 0.5f, 0.6f, 1f, 32),
      new Color(0.7f, 0.8f, 0.9f, 1f, 32),
      new Color(1f, 1f, 0.9f, 1f, 32),
      new Color(0.0f, 1f, 0.0f, 1f, 32),
      new Color(0.2f, 0.8f, 0.2f, 1f, 32),
      new Color(1f, 0.9f, 0.9f, 1f, 32),
      new Color(1f, 0.0f, 1f, 1f, 32),
      new Color(0.5f, 0.0f, 0.0f, 1f, 32),
      new Color(0.4f, 0.8f, 0.7f, 1f, 32),
      new Color(0.0f, 0.0f, 0.8f, 1f, 32),
      new Color(0.7f, 0.3f, 0.8f, 1f, 32),
      new Color(0.6f, 0.4f, 0.9f, 1f, 32),
      new Color(0.2f, 0.7f, 0.4f, 1f, 32),
      new Color(0.5f, 0.4f, 0.9f, 1f, 32),
      new Color(0.0f, 1f, 0.6f, 1f, 32),
      new Color(0.3f, 0.8f, 0.8f, 1f, 32),
      new Color(0.8f, 0.1f, 0.5f, 1f, 32),
      new Color(0.9f, 0.9f, 0.9f, 1f, 32),
      new Color(0.9f, 0.9f, 0.9f, 1f, 32),
      new Color(0.2f, 0.6f, 1f, 1f, 32),
      new Color(0.0f, 0.0f, 0.0f, 1f, 32),
      new Color(0.1f, 0.1f, 0.4f, 1f, 32),
      new Color(1f, 1f, 1f, 1f, 32),
      new Color(1f, 0.9f, 0.9f, 1f, 32),
      new Color(1f, 0.9f, 0.7f, 1f, 32),
      new Color(1f, 0.9f, 0.7f, 1f, 32),
      new Color(0.0f, 0.0f, 0.5f, 1f, 32),
      new Color(1f, 1f, 0.9f, 1f, 32),
      new Color(0.5f, 0.5f, 0.0f, 1f, 32),
      new Color(0.4f, 0.6f, 0.1f, 1f, 32),
      new Color(1f, 0.6f, 0.0f, 1f, 32),
      new Color(1f, 0.3f, 0.0f, 1f, 32),
      new Color(0.9f, 0.4f, 0.8f, 1f, 32),
      new Color(0.9f, 0.9f, 0.7f, 1f, 32),
      new Color(0.6f, 1f, 0.6f, 1f, 32),
      new Color(0.7f, 0.9f, 0.9f, 1f, 32),
      new Color(0.9f, 0.4f, 0.6f, 1f, 32),
      new Color(1f, 0.9f, 0.8f, 1f, 32),
      new Color(1f, 0.9f, 0.7f, 1f, 32),
      new Color(0.8f, 0.5f, 0.2f, 1f, 32),
      new Color(1f, 0.8f, 0.8f, 1f, 32),
      new Color(0.9f, 0.6f, 0.9f, 1f, 32),
      new Color(0.7f, 0.9f, 0.9f, 1f, 32),
      new Color(0.5f, 0.0f, 0.5f, 1f, 32),
      new Color(1f, 0.0f, 0.0f, 1f, 32),
      new Color(0.7f, 0.6f, 0.6f, 1f, 32),
      new Color(0.3f, 0.4f, 0.9f, 1f, 32),
      new Color(0.5f, 0.3f, 0.1f, 1f, 32),
      new Color(1f, 0.5f, 0.4f, 1f, 32),
      new Color(1f, 0.6f, 0.4f, 1f, 32),
      new Color(0.8f, 0.8f, 0.8f, 1f, 32),
      new Color(0.2f, 0.5f, 0.3f, 1f, 32),
      new Color(1f, 1f, 0.9f, 1f, 32),
      new Color(0.6f, 0.3f, 0.2f, 1f, 32),
      new Color(0.8f, 0.8f, 0.8f, 1f, 32),
      new Color(0.5f, 0.8f, 0.9f, 1f, 32),
      new Color(0.4f, 0.4f, 0.8f, 1f, 32),
      new Color(0.4f, 0.5f, 0.6f, 1f, 32),
      new Color(1f, 1f, 1f, 1f, 32),
      new Color(0.0f, 1f, 0.5f, 1f, 32),
      new Color(0.3f, 0.5f, 0.7f, 1f, 32),
      new Color(0.8f, 0.7f, 0.5f, 1f, 32),
      new Color(0.0f, 0.5f, 0.5f, 1f, 32),
      new Color(0.8f, 0.7f, 0.8f, 1f, 32),
      new Color(1f, 0.4f, 0.3f, 1f, 32),
      new Color(0.3f, 0.9f, 0.8f, 1f, 32),
      new Color(1f, 0.9f, 0.7f, 1f, 32),
      new Color(1f, 1f, 1f, 1f, 32),
      new Color(1f, 1f, 1f, 1f, 32),
      new Color(1f, 1f, 1f, 1f, 32),
      new Color(0.4f, 0.4f, 0.4f, 1f, 32),
      new Color(0.0f, 0.0f, 0.0f, 1f, 32),
      new Color(0.9f, 0.5f, 0.9f, 1f, 32),
      new Color(1f, 1f, 0.0f, 1f, 32),
      new Color(0.6f, 0.8f, 0.2f, 1f, 32)
    };
    public float R;
    public float G;
    public float B;
    public float A;
    public int BitDepth;

    public Color(float r, float g, float b, float a, int bitDepth = 32)
    {
      this = new Color();
      this.R = r;
      this.G = g;
      this.B = b;
      this.A = a;
      this.BitDepth = bitDepth;
    }

    public Color(float[] rgba)
    {
      this = new Color();
      this.R = rgba[0];
      this.G = rgba[1];
      this.B = rgba[2];
      this.A = rgba[3];
      this.BitDepth = 32;
    }

    public Color(string argb)
    {
      this = new Color();
      try
      {
        if (argb.Length == 10)
        {
          this.A = (float) Convert.ToInt32(argb.Substring(2, 2), 16) / (float) byte.MaxValue;
          this.R = (float) Convert.ToInt32(argb.Substring(4, 2), 16) / (float) byte.MaxValue;
          this.G = (float) Convert.ToInt32(argb.Substring(6, 2), 16) / (float) byte.MaxValue;
          this.B = (float) Convert.ToInt32(argb.Substring(8, 2), 16) / (float) byte.MaxValue;
        }
        else if (argb.Length == 9)
        {
          this.A = (float) Convert.ToInt32(argb.Substring(1, 2), 16) / (float) byte.MaxValue;
          this.R = (float) Convert.ToInt32(argb.Substring(3, 2), 16) / (float) byte.MaxValue;
          this.G = (float) Convert.ToInt32(argb.Substring(5, 2), 16) / (float) byte.MaxValue;
          this.B = (float) Convert.ToInt32(argb.Substring(7, 2), 16) / (float) byte.MaxValue;
        }
        this.BitDepth = 32;
      }
      catch (Exception ex)
      {
      }
    }

    public Color(float value)
    {
      this = new Color(value, value, value, value, 32);
    }

    public Color(uint seed)
    {
      this = new Color();
      Random random = new Random(55001 + (int) seed);
      Color namedColor = Color.NamedColors[random.Next(Color.NamedColors.Length)];
      this.R = namedColor.R;
      this.G = namedColor.G;
      this.B = namedColor.B;
      this.A = namedColor.A;
    }

    public float[] ToARGBArray()
    {
      return new float[4]{ this.A, this.R, this.G, this.B };
    }

    public float[] ToRGBAArray()
    {
      return new float[4]{ this.R, this.G, this.B, this.A };
    }

    public Vector4 ToARGB()
    {
      return new Vector4((double) this.A, (double) this.R, (double) this.G, (double) this.B);
    }

    public Vector4 ToRGBA()
    {
      return new Vector4((double) this.R, (double) this.G, (double) this.B, (double) this.A);
    }

    public static bool operator ==(Color c1, Color c2)
    {
      if ((double) c1.R == (double) c2.R && (double) c1.G == (double) c2.G && (double) c1.B == (double) c2.B)
        return (double) c1.A == (double) c2.A;
      return false;
    }

    public static bool operator !=(Color c1, Color c2)
    {
      return !(c1 == c2);
    }

    public static Color operator +(Color c1, Color c2)
    {
      return new Color(c1.R + c2.R, c1.G + c2.G, c1.B + c2.B, c1.A + c2.A, 32);
    }

    public static Color operator -(Color c1, Color c2)
    {
      return new Color(c1.R - c2.R, c1.G - c2.G, c1.B - c2.B, c1.A - c2.A, 32);
    }

    public static Color operator *(Color c, float scale)
    {
      return new Color(c.R * scale, c.G * scale, c.B * scale, c.A * scale, 32);
    }

    public Color Saturate()
    {
      return new Color(MathHelper.Clamp(this.R, 0.0f, 1f), MathHelper.Clamp(this.G, 0.0f, 1f), MathHelper.Clamp(this.B, 0.0f, 1f), MathHelper.Clamp(this.A, 0.0f, 1f), 32);
    }

    public static Color FromHSV(double h, double s, double v)
    {
      double num1 = h;
      double num2 = s;
      double num3 = v;
      while (num1 < 0.0)
        num1 += 360.0;
      while (num1 >= 360.0)
        num1 -= 360.0;
      double num4;
      double num5;
      double num6;
      if (num3 <= 0.0)
      {
        double num7;
        num4 = num7 = 0.0;
        num5 = num7;
        num6 = num7;
      }
      else if (num2 <= 0.0)
      {
        double num7;
        num4 = num7 = num3;
        num5 = num7;
        num6 = num7;
      }
      else
      {
        double d = num1 / 60.0;
        int num7 = (int) System.Math.Floor(d);
        double num8 = d - (double) num7;
        double num9 = num3 * (1.0 - num2);
        double num10 = num3 * (1.0 - num2 * num8);
        double num11 = num3 * (1.0 - num2 * (1.0 - num8));
        switch (num7)
        {
          case -1:
            num6 = num3;
            num5 = num9;
            num4 = num10;
            break;
          case 0:
            num6 = num3;
            num5 = num11;
            num4 = num9;
            break;
          case 1:
            num6 = num10;
            num5 = num3;
            num4 = num9;
            break;
          case 2:
            num6 = num9;
            num5 = num3;
            num4 = num11;
            break;
          case 3:
            num6 = num9;
            num5 = num10;
            num4 = num3;
            break;
          case 4:
            num6 = num11;
            num5 = num9;
            num4 = num3;
            break;
          case 5:
            num6 = num3;
            num5 = num9;
            num4 = num10;
            break;
          case 6:
            num6 = num3;
            num5 = num11;
            num4 = num9;
            break;
          default:
            double num12;
            num4 = num12 = num3;
            num5 = num12;
            num6 = num12;
            break;
        }
      }
      return new Color((float) num6, (float) num5, (float) num4, 1f, 32);
    }

    public static Color Simplygon
    {
      get
      {
        return Color.simplygon;
      }
    }

    public static Color LOD(int index)
    {
      switch (index)
      {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
          return Color.lods[index];
        default:
          return Color.lods[0];
      }
    }

    public static Color ActiveBorder
    {
      get
      {
        return Color.NamedColors[0];
      }
    }

    public static Color ActiveCaption
    {
      get
      {
        return Color.NamedColors[1];
      }
    }

    public static Color ActiveCaptionText
    {
      get
      {
        return Color.NamedColors[2];
      }
    }

    public static Color AliceBlue
    {
      get
      {
        return Color.NamedColors[3];
      }
    }

    public static Color AntiqueWhite
    {
      get
      {
        return Color.NamedColors[4];
      }
    }

    public static Color AppWorkspace
    {
      get
      {
        return Color.NamedColors[5];
      }
    }

    public static Color Aqua
    {
      get
      {
        return Color.NamedColors[6];
      }
    }

    public static Color Aquamarine
    {
      get
      {
        return Color.NamedColors[7];
      }
    }

    public static Color Azure
    {
      get
      {
        return Color.NamedColors[8];
      }
    }

    public static Color Beige
    {
      get
      {
        return Color.NamedColors[9];
      }
    }

    public static Color Bisque
    {
      get
      {
        return Color.NamedColors[10];
      }
    }

    public static Color Black
    {
      get
      {
        return Color.NamedColors[11];
      }
    }

    public static Color BlanchedAlmond
    {
      get
      {
        return Color.NamedColors[12];
      }
    }

    public static Color Blue
    {
      get
      {
        return Color.NamedColors[13];
      }
    }

    public static Color BlueViolet
    {
      get
      {
        return Color.NamedColors[14];
      }
    }

    public static Color Brown
    {
      get
      {
        return Color.NamedColors[15];
      }
    }

    public static Color BurlyWood
    {
      get
      {
        return Color.NamedColors[16];
      }
    }

    public static Color ButtonFace
    {
      get
      {
        return Color.NamedColors[17];
      }
    }

    public static Color ButtonHighlight
    {
      get
      {
        return Color.NamedColors[18];
      }
    }

    public static Color ButtonShadow
    {
      get
      {
        return Color.NamedColors[19];
      }
    }

    public static Color CadetBlue
    {
      get
      {
        return Color.NamedColors[20];
      }
    }

    public static Color Chartreuse
    {
      get
      {
        return Color.NamedColors[21];
      }
    }

    public static Color Chocolate
    {
      get
      {
        return Color.NamedColors[22];
      }
    }

    public static Color Control
    {
      get
      {
        return Color.NamedColors[23];
      }
    }

    public static Color ControlDark
    {
      get
      {
        return Color.NamedColors[24];
      }
    }

    public static Color ControlDarkDark
    {
      get
      {
        return Color.NamedColors[25];
      }
    }

    public static Color ControlLight
    {
      get
      {
        return Color.NamedColors[26];
      }
    }

    public static Color ControlLightLight
    {
      get
      {
        return Color.NamedColors[27];
      }
    }

    public static Color ControlText
    {
      get
      {
        return Color.NamedColors[28];
      }
    }

    public static Color Coral
    {
      get
      {
        return Color.NamedColors[29];
      }
    }

    public static Color CornflowerBlue
    {
      get
      {
        return Color.NamedColors[30];
      }
    }

    public static Color Cornsilk
    {
      get
      {
        return Color.NamedColors[31];
      }
    }

    public static Color Crimson
    {
      get
      {
        return Color.NamedColors[32];
      }
    }

    public static Color Cyan
    {
      get
      {
        return Color.NamedColors[33];
      }
    }

    public static Color DarkBlue
    {
      get
      {
        return Color.NamedColors[34];
      }
    }

    public static Color DarkCyan
    {
      get
      {
        return Color.NamedColors[35];
      }
    }

    public static Color DarkGoldenrod
    {
      get
      {
        return Color.NamedColors[36];
      }
    }

    public static Color DarkGray
    {
      get
      {
        return Color.NamedColors[37];
      }
    }

    public static Color DarkGreen
    {
      get
      {
        return Color.NamedColors[38];
      }
    }

    public static Color DarkKhaki
    {
      get
      {
        return Color.NamedColors[39];
      }
    }

    public static Color DarkMagenta
    {
      get
      {
        return Color.NamedColors[40];
      }
    }

    public static Color DarkOliveGreen
    {
      get
      {
        return Color.NamedColors[41];
      }
    }

    public static Color DarkOrange
    {
      get
      {
        return Color.NamedColors[42];
      }
    }

    public static Color DarkOrchid
    {
      get
      {
        return Color.NamedColors[43];
      }
    }

    public static Color DarkRed
    {
      get
      {
        return Color.NamedColors[44];
      }
    }

    public static Color DarkSalmon
    {
      get
      {
        return Color.NamedColors[45];
      }
    }

    public static Color DarkSeaGreen
    {
      get
      {
        return Color.NamedColors[46];
      }
    }

    public static Color DarkSlateBlue
    {
      get
      {
        return Color.NamedColors[47];
      }
    }

    public static Color DarkSlateGray
    {
      get
      {
        return Color.NamedColors[48];
      }
    }

    public static Color DarkTurquoise
    {
      get
      {
        return Color.NamedColors[49];
      }
    }

    public static Color DarkViolet
    {
      get
      {
        return Color.NamedColors[50];
      }
    }

    public static Color DeepPink
    {
      get
      {
        return Color.NamedColors[51];
      }
    }

    public static Color DeepSkyBlue
    {
      get
      {
        return Color.NamedColors[52];
      }
    }

    public static Color Desktop
    {
      get
      {
        return Color.NamedColors[53];
      }
    }

    public static Color DimGray
    {
      get
      {
        return Color.NamedColors[54];
      }
    }

    public static Color DodgerBlue
    {
      get
      {
        return Color.NamedColors[55];
      }
    }

    public static Color Firebrick
    {
      get
      {
        return Color.NamedColors[56];
      }
    }

    public static Color FloralWhite
    {
      get
      {
        return Color.NamedColors[57];
      }
    }

    public static Color ForestGreen
    {
      get
      {
        return Color.NamedColors[58];
      }
    }

    public static Color Fuchsia
    {
      get
      {
        return Color.NamedColors[59];
      }
    }

    public static Color Gainsboro
    {
      get
      {
        return Color.NamedColors[60];
      }
    }

    public static Color GhostWhite
    {
      get
      {
        return Color.NamedColors[61];
      }
    }

    public static Color Gold
    {
      get
      {
        return Color.NamedColors[62];
      }
    }

    public static Color Goldenrod
    {
      get
      {
        return Color.NamedColors[63];
      }
    }

    public static Color GradientActiveCaption
    {
      get
      {
        return Color.NamedColors[64];
      }
    }

    public static Color GradientInactiveCaption
    {
      get
      {
        return Color.NamedColors[65];
      }
    }

    public static Color Gray
    {
      get
      {
        return Color.NamedColors[66];
      }
    }

    public static Color GrayText
    {
      get
      {
        return Color.NamedColors[67];
      }
    }

    public static Color Green
    {
      get
      {
        return Color.NamedColors[68];
      }
    }

    public static Color GreenYellow
    {
      get
      {
        return Color.NamedColors[69];
      }
    }

    public static Color Highlight
    {
      get
      {
        return Color.NamedColors[70];
      }
    }

    public static Color HighlightText
    {
      get
      {
        return Color.NamedColors[71];
      }
    }

    public static Color Honeydew
    {
      get
      {
        return Color.NamedColors[72];
      }
    }

    public static Color HotPink
    {
      get
      {
        return Color.NamedColors[73];
      }
    }

    public static Color HotTrack
    {
      get
      {
        return Color.NamedColors[74];
      }
    }

    public static Color InactiveBorder
    {
      get
      {
        return Color.NamedColors[75];
      }
    }

    public static Color InactiveCaption
    {
      get
      {
        return Color.NamedColors[76];
      }
    }

    public static Color InactiveCaptionText
    {
      get
      {
        return Color.NamedColors[77];
      }
    }

    public static Color IndianRed
    {
      get
      {
        return Color.NamedColors[78];
      }
    }

    public static Color Indigo
    {
      get
      {
        return Color.NamedColors[79];
      }
    }

    public static Color Info
    {
      get
      {
        return Color.NamedColors[80];
      }
    }

    public static Color InfoText
    {
      get
      {
        return Color.NamedColors[81];
      }
    }

    public static Color Ivory
    {
      get
      {
        return Color.NamedColors[82];
      }
    }

    public static Color Khaki
    {
      get
      {
        return Color.NamedColors[83];
      }
    }

    public static Color Lavender
    {
      get
      {
        return Color.NamedColors[84];
      }
    }

    public static Color LavenderBlush
    {
      get
      {
        return Color.NamedColors[85];
      }
    }

    public static Color LawnGreen
    {
      get
      {
        return Color.NamedColors[86];
      }
    }

    public static Color LemonChiffon
    {
      get
      {
        return Color.NamedColors[87];
      }
    }

    public static Color LightBlue
    {
      get
      {
        return Color.NamedColors[88];
      }
    }

    public static Color LightCoral
    {
      get
      {
        return Color.NamedColors[89];
      }
    }

    public static Color LightCyan
    {
      get
      {
        return Color.NamedColors[90];
      }
    }

    public static Color LightGoldenrodYellow
    {
      get
      {
        return Color.NamedColors[91];
      }
    }

    public static Color LightGray
    {
      get
      {
        return Color.NamedColors[92];
      }
    }

    public static Color LightGreen
    {
      get
      {
        return Color.NamedColors[93];
      }
    }

    public static Color LightPink
    {
      get
      {
        return Color.NamedColors[94];
      }
    }

    public static Color LightSalmon
    {
      get
      {
        return Color.NamedColors[95];
      }
    }

    public static Color LightSeaGreen
    {
      get
      {
        return Color.NamedColors[96];
      }
    }

    public static Color LightSkyBlue
    {
      get
      {
        return Color.NamedColors[97];
      }
    }

    public static Color LightSlateGray
    {
      get
      {
        return Color.NamedColors[98];
      }
    }

    public static Color LightSteelBlue
    {
      get
      {
        return Color.NamedColors[99];
      }
    }

    public static Color LightYellow
    {
      get
      {
        return Color.NamedColors[100];
      }
    }

    public static Color Lime
    {
      get
      {
        return Color.NamedColors[101];
      }
    }

    public static Color LimeGreen
    {
      get
      {
        return Color.NamedColors[102];
      }
    }

    public static Color Linen
    {
      get
      {
        return Color.NamedColors[103];
      }
    }

    public static Color Magenta
    {
      get
      {
        return Color.NamedColors[104];
      }
    }

    public static Color Maroon
    {
      get
      {
        return Color.NamedColors[105];
      }
    }

    public static Color MediumAquamarine
    {
      get
      {
        return Color.NamedColors[106];
      }
    }

    public static Color MediumBlue
    {
      get
      {
        return Color.NamedColors[107];
      }
    }

    public static Color MediumOrchid
    {
      get
      {
        return Color.NamedColors[108];
      }
    }

    public static Color MediumPurple
    {
      get
      {
        return Color.NamedColors[109];
      }
    }

    public static Color MediumSeaGreen
    {
      get
      {
        return Color.NamedColors[110];
      }
    }

    public static Color MediumSlateBlue
    {
      get
      {
        return Color.NamedColors[111];
      }
    }

    public static Color MediumSpringGreen
    {
      get
      {
        return Color.NamedColors[112];
      }
    }

    public static Color MediumTurquoise
    {
      get
      {
        return Color.NamedColors[113];
      }
    }

    public static Color MediumVioletRed
    {
      get
      {
        return Color.NamedColors[114];
      }
    }

    public static Color Menu
    {
      get
      {
        return Color.NamedColors[115];
      }
    }

    public static Color MenuBar
    {
      get
      {
        return Color.NamedColors[116];
      }
    }

    public static Color MenuHighlight
    {
      get
      {
        return Color.NamedColors[117];
      }
    }

    public static Color MenuText
    {
      get
      {
        return Color.NamedColors[118];
      }
    }

    public static Color MidnightBlue
    {
      get
      {
        return Color.NamedColors[119];
      }
    }

    public static Color MintCream
    {
      get
      {
        return Color.NamedColors[120];
      }
    }

    public static Color MistyRose
    {
      get
      {
        return Color.NamedColors[121];
      }
    }

    public static Color Moccasin
    {
      get
      {
        return Color.NamedColors[122];
      }
    }

    public static Color NavajoWhite
    {
      get
      {
        return Color.NamedColors[123];
      }
    }

    public static Color Navy
    {
      get
      {
        return Color.NamedColors[124];
      }
    }

    public static Color OldLace
    {
      get
      {
        return Color.NamedColors[125];
      }
    }

    public static Color Olive
    {
      get
      {
        return Color.NamedColors[126];
      }
    }

    public static Color OliveDrab
    {
      get
      {
        return Color.NamedColors[(int) sbyte.MaxValue];
      }
    }

    public static Color Orange
    {
      get
      {
        return Color.NamedColors[128];
      }
    }

    public static Color OrangeRed
    {
      get
      {
        return Color.NamedColors[129];
      }
    }

    public static Color Orchid
    {
      get
      {
        return Color.NamedColors[130];
      }
    }

    public static Color PaleGoldenrod
    {
      get
      {
        return Color.NamedColors[131];
      }
    }

    public static Color PaleGreen
    {
      get
      {
        return Color.NamedColors[132];
      }
    }

    public static Color PaleTurquoise
    {
      get
      {
        return Color.NamedColors[133];
      }
    }

    public static Color PaleVioletRed
    {
      get
      {
        return Color.NamedColors[134];
      }
    }

    public static Color PapayaWhip
    {
      get
      {
        return Color.NamedColors[135];
      }
    }

    public static Color PeachPuff
    {
      get
      {
        return Color.NamedColors[136];
      }
    }

    public static Color Peru
    {
      get
      {
        return Color.NamedColors[137];
      }
    }

    public static Color Pink
    {
      get
      {
        return Color.NamedColors[138];
      }
    }

    public static Color Plum
    {
      get
      {
        return Color.NamedColors[139];
      }
    }

    public static Color PowderBlue
    {
      get
      {
        return Color.NamedColors[140];
      }
    }

    public static Color Purple
    {
      get
      {
        return Color.NamedColors[141];
      }
    }

    public static Color Red
    {
      get
      {
        return Color.NamedColors[142];
      }
    }

    public static Color RosyBrown
    {
      get
      {
        return Color.NamedColors[143];
      }
    }

    public static Color RoyalBlue
    {
      get
      {
        return Color.NamedColors[144];
      }
    }

    public static Color SaddleBrown
    {
      get
      {
        return Color.NamedColors[145];
      }
    }

    public static Color Salmon
    {
      get
      {
        return Color.NamedColors[146];
      }
    }

    public static Color SandyBrown
    {
      get
      {
        return Color.NamedColors[147];
      }
    }

    public static Color ScrollBar
    {
      get
      {
        return Color.NamedColors[148];
      }
    }

    public static Color SeaGreen
    {
      get
      {
        return Color.NamedColors[149];
      }
    }

    public static Color SeaShell
    {
      get
      {
        return Color.NamedColors[150];
      }
    }

    public static Color Sienna
    {
      get
      {
        return Color.NamedColors[151];
      }
    }

    public static Color Silver
    {
      get
      {
        return Color.NamedColors[152];
      }
    }

    public static Color SkyBlue
    {
      get
      {
        return Color.NamedColors[153];
      }
    }

    public static Color SlateBlue
    {
      get
      {
        return Color.NamedColors[154];
      }
    }

    public static Color SlateGray
    {
      get
      {
        return Color.NamedColors[155];
      }
    }

    public static Color Snow
    {
      get
      {
        return Color.NamedColors[156];
      }
    }

    public static Color SpringGreen
    {
      get
      {
        return Color.NamedColors[157];
      }
    }

    public static Color SteelBlue
    {
      get
      {
        return Color.NamedColors[158];
      }
    }

    public static Color Tan
    {
      get
      {
        return Color.NamedColors[159];
      }
    }

    public static Color Teal
    {
      get
      {
        return Color.NamedColors[160];
      }
    }

    public static Color Thistle
    {
      get
      {
        return Color.NamedColors[161];
      }
    }

    public static Color Tomato
    {
      get
      {
        return Color.NamedColors[162];
      }
    }

    public static Color Turquoise
    {
      get
      {
        return Color.NamedColors[163];
      }
    }

    public static Color Wheat
    {
      get
      {
        return Color.NamedColors[164];
      }
    }

    public static Color White
    {
      get
      {
        return Color.NamedColors[165];
      }
    }

    public static Color WhiteSmoke
    {
      get
      {
        return Color.NamedColors[166];
      }
    }

    public static Color Window
    {
      get
      {
        return Color.NamedColors[167];
      }
    }

    public static Color WindowFrame
    {
      get
      {
        return Color.NamedColors[168];
      }
    }

    public static Color WindowText
    {
      get
      {
        return Color.NamedColors[169];
      }
    }

    public static Color Violet
    {
      get
      {
        return Color.NamedColors[170];
      }
    }

    public static Color Yellow
    {
      get
      {
        return Color.NamedColors[171];
      }
    }

    public static Color YellowGreen
    {
      get
      {
        return Color.NamedColors[172];
      }
    }

    public static Color Transparent
    {
      get
      {
        return new Color(0.0f, 0.0f, 0.0f, 0.0f, 32);
      }
    }
  }
}
