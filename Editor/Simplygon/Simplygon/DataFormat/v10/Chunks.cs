﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.Chunks
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.Collections.Generic;

namespace Simplygon.DataFormat.v10
{
  internal class Chunks : ISDFObject
  {
    private List<ChunkInfo> ChunkInfoList;

    public ulong ChunksLength { get; protected set; }

    public uint ChunkCount { get; protected set; }

    public List<Chunk> ChunkList { get; set; }

    public Chunks()
    {
      this.ChunkInfoList = new List<ChunkInfo>();
      this.ChunkList = new List<Chunk>();
    }

    public ulong GetTotalSizeInBytes()
    {
      return 12UL + this.ChunksLength;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.ChunksLength = binaryStream.ReadUInt64();
      this.ChunkCount = binaryStream.ReadUInt32();
      for (uint index = 0; index < this.ChunkCount; ++index)
      {
        ChunkInfo chunkInfo = new ChunkInfo();
        chunkInfo.Read(binaryStream);
        this.ChunkInfoList.Add(chunkInfo);
      }
      for (uint index = 0; index < this.ChunkCount; ++index)
      {
        Chunk chunk = new Chunk();
        chunk.Read(binaryStream);
        this.ChunkList.Add(chunk);
      }
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteUInt64(this.ChunksLength);
      binaryStream.WriteUInt32(this.ChunkCount);
      foreach (ChunkInfo chunkInfo in this.ChunkInfoList)
        chunkInfo.Write(binaryStream);
      foreach (Chunk chunk in this.ChunkList)
        chunk.Write(binaryStream);
    }

    public void Update()
    {
      foreach (Chunk chunk in this.ChunkList)
        chunk.Update();
      ulong num1 = 0;
      ulong num2 = 0;
      this.ChunkCount = (uint) this.ChunkList.Count;
      this.ChunkInfoList.Clear();
      foreach (Chunk chunk in this.ChunkList)
      {
        ChunkInfo chunkInfo = new ChunkInfo();
        chunkInfo.ChunkType = chunk.ChunkHeader.ChunkType;
        chunkInfo.ChunkOffset = this.ChunksLength;
        this.ChunkInfoList.Add(chunkInfo);
        num2 += chunkInfo.GetTotalSizeInBytes();
        num1 += chunk.GetTotalSizeInBytes();
      }
      this.ChunksLength = num2 + num1;
    }

    public void Validate()
    {
      foreach (Chunk chunk in this.ChunkList)
        chunk.Validate();
    }

    public void Release()
    {
    }
  }
}
