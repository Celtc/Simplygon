﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.DataType.JaggedList
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.Collections.Generic;

namespace Simplygon.DataFormat.v10.DataType
{
  internal class JaggedList : ISDFObject
  {
    public DataTypeId DataTypeId { get; set; }

    public uint SubListSize { get; set; }

    public uint Count { get; protected set; }

    public List<List<object>> Items { get; protected set; }

    public JaggedList()
    {
      this.Items = new List<List<object>>();
    }

    public ulong GetTotalSizeInBytes()
    {
      ulong num = 0;
      if (this.Items.Count > 0)
        num = Util.GetSizeInBytes(this.DataTypeId, (object) this.Items[0]) * (ulong) this.Items.Count * (ulong) this.SubListSize;
      return 16UL + num;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.DataTypeId = (DataTypeId) binaryStream.ReadUInt64();
      this.SubListSize = binaryStream.ReadUInt32();
      this.Count = binaryStream.ReadUInt32();
      for (uint index1 = 0; index1 < this.Count; ++index1)
      {
        List<object> objectList = new List<object>();
        for (uint index2 = 0; index2 < this.SubListSize; ++index2)
        {
          object obj = Util.Read(binaryStream, this.DataTypeId);
          objectList.Add(obj);
        }
        this.Items.Add(objectList);
      }
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Count = (uint) this.Items.Count;
      binaryStream.WriteUInt64((ulong) this.DataTypeId);
      binaryStream.WriteUInt32(this.SubListSize);
      binaryStream.WriteUInt32(this.Count);
      foreach (List<object> objectList in this.Items)
      {
        foreach (object obj in objectList)
          Util.Write(binaryStream, this.DataTypeId, obj);
      }
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
