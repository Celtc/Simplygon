﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.DataType.Index4
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.DataFormat.v10.DataType
{
  internal class Index4 : ISDFObject
  {
    public int X { get; set; }

    public int Y { get; set; }

    public int Z { get; set; }

    public int W { get; set; }

    public Index4()
    {
    }

    public Index4(int x, int y, int z, int w)
    {
      this.X = x;
      this.Y = y;
      this.Z = z;
      this.W = w;
    }

    public ulong GetTotalSizeInBytes()
    {
      return 16;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.X = binaryStream.ReadInt32();
      this.Y = binaryStream.ReadInt32();
      this.Z = binaryStream.ReadInt32();
      this.W = binaryStream.ReadInt32();
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteInt32(this.X);
      binaryStream.WriteInt32(this.Y);
      binaryStream.WriteInt32(this.Z);
      binaryStream.WriteInt32(this.W);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
