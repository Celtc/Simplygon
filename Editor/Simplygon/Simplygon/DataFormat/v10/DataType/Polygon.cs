﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.DataType.Polygon
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.DataFormat.v10.DataType
{
  internal class Polygon : ISDFObject
  {
    public List Indices { get; protected set; }

    public ulong GetTotalSizeInBytes()
    {
      return this.Indices.GetTotalSizeInBytes();
    }

    public void Read(BinaryStream binaryStream)
    {
      this.Indices = new List();
      this.Indices.Read(binaryStream);
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Indices.Write(binaryStream);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
