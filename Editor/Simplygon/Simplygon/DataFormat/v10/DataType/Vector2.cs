﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.DataType.Vector2
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.DataFormat.v10.DataType
{
  internal class Vector2 : ISDFObject
  {
    public double X { get; set; }

    public double Y { get; set; }

    public Vector2()
    {
    }

    public Vector2(double x, double y)
    {
      this.X = x;
      this.Y = y;
    }

    public ulong GetTotalSizeInBytes()
    {
      return 16;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.X = binaryStream.ReadDouble();
      this.Y = binaryStream.ReadDouble();
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteDouble(this.X);
      binaryStream.WriteDouble(this.Y);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }

    public static Vector2 operator +(Vector2 a, Vector2 b)
    {
      return new Vector2(a.X + b.X, a.Y + b.Y);
    }

    public Vector2 Normalize()
    {
      double num = System.Math.Sqrt(this.X * this.X + this.Y * this.Y);
      return new Vector2(this.X / num, this.Y / num);
    }

    public bool IsZero()
    {
      if (this.X == 0.0)
        return this.Y == 0.0;
      return false;
    }
  }
}
