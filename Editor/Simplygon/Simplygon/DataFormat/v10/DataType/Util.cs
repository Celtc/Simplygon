﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.DataType.Util
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.Text;

namespace Simplygon.DataFormat.v10.DataType
{
  internal class Util
  {
    public static object Read(BinaryStream binaryStream, DataTypeId dataTypeId)
    {
      switch (dataTypeId)
      {
        case DataTypeId.Custom:
          throw new Exception("Custom data type not supported yet");
        case DataTypeId.Bool:
          return (object) binaryStream.ReadBool();
        case DataTypeId.Int8:
          return (object) binaryStream.ReadInt8();
        case DataTypeId.UInt8:
          return (object) binaryStream.ReadUInt8();
        case DataTypeId.Int16:
          return (object) binaryStream.ReadInt16();
        case DataTypeId.UInt16:
          return (object) binaryStream.ReadUInt16();
        case DataTypeId.Int32:
          return (object) binaryStream.ReadInt32();
        case DataTypeId.UInt32:
          return (object) binaryStream.ReadUInt32();
        case DataTypeId.Int64:
          return (object) binaryStream.ReadInt64();
        case DataTypeId.UInt64:
          return (object) binaryStream.ReadUInt64();
        case DataTypeId.Float:
          return (object) binaryStream.ReadFloat();
        case DataTypeId.Double:
          return (object) binaryStream.ReadDouble();
        case DataTypeId.String:
          return (object) binaryStream.ReadString();
        case DataTypeId.List:
          List list = new List();
          list.Read(binaryStream);
          return (object) list;
        case DataTypeId.JaggedList:
          JaggedList jaggedList = new JaggedList();
          jaggedList.Read(binaryStream);
          return (object) jaggedList;
        case DataTypeId.Datetime:
          return (object) new DateTime((long) binaryStream.ReadUInt64());
        case DataTypeId.Vector2:
          Vector2 vector2 = new Vector2();
          vector2.Read(binaryStream);
          return (object) vector2;
        case DataTypeId.Vector3:
          Vector3 vector3 = new Vector3();
          vector3.Read(binaryStream);
          return (object) vector3;
        case DataTypeId.Vector4:
          Vector4 vector4 = new Vector4();
          vector4.Read(binaryStream);
          return (object) vector4;
        case DataTypeId.Polygon:
          Polygon polygon = new Polygon();
          polygon.Read(binaryStream);
          return (object) polygon;
        case DataTypeId.Corner:
          Corner corner = new Corner();
          corner.Read(binaryStream);
          return (object) corner;
        case DataTypeId.Quaternion:
          Quaternion quaternion = new Quaternion();
          quaternion.Read(binaryStream);
          return (object) quaternion;
        case DataTypeId.Matrix3x3:
          Matrix3x3 matrix3x3 = new Matrix3x3();
          matrix3x3.Read(binaryStream);
          return (object) matrix3x3;
        case DataTypeId.Matrix4x4:
          Matrix4x4 matrix4x4 = new Matrix4x4();
          matrix4x4.Read(binaryStream);
          return (object) matrix4x4;
        case DataTypeId.Plane:
          Plane plane = new Plane();
          plane.Read(binaryStream);
          return (object) plane;
        case DataTypeId.Index2:
          Index2 index2 = new Index2();
          index2.Read(binaryStream);
          return (object) index2;
        case DataTypeId.Index3:
          Index3 index3 = new Index3();
          index3.Read(binaryStream);
          return (object) index3;
        case DataTypeId.Index4:
          Index4 index4 = new Index4();
          index4.Read(binaryStream);
          return (object) index4;
        default:
          return (object) null;
      }
    }

    public static void Write(BinaryStream binaryStream, DataTypeId dataTypeId, object value)
    {
      switch (dataTypeId)
      {
        case DataTypeId.Bool:
          binaryStream.WriteBool((bool) value);
          break;
        case DataTypeId.Int8:
          binaryStream.WriteInt8((sbyte) value);
          break;
        case DataTypeId.UInt8:
          binaryStream.WriteUInt8((byte) value);
          break;
        case DataTypeId.Int16:
          binaryStream.WriteInt16((short) value);
          break;
        case DataTypeId.UInt16:
          binaryStream.WriteUInt16((ushort) value);
          break;
        case DataTypeId.Int32:
          binaryStream.WriteInt32((int) value);
          break;
        case DataTypeId.UInt32:
          binaryStream.WriteUInt32((uint) value);
          break;
        case DataTypeId.Int64:
          binaryStream.WriteInt64((long) value);
          break;
        case DataTypeId.UInt64:
          binaryStream.WriteUInt64((ulong) value);
          break;
        case DataTypeId.Float:
          binaryStream.WriteFloat((float) value);
          break;
        case DataTypeId.Double:
          binaryStream.WriteDouble((double) value);
          break;
        case DataTypeId.String:
          binaryStream.WriteString((string) value);
          break;
        case DataTypeId.List:
          ((List) value).Write(binaryStream);
          break;
        case DataTypeId.JaggedList:
          ((JaggedList) value).Write(binaryStream);
          break;
        case DataTypeId.Datetime:
          binaryStream.WriteUInt64((ulong) ((DateTime) value).Ticks);
          break;
        case DataTypeId.Vector2:
          ((Vector2) value).Write(binaryStream);
          break;
        case DataTypeId.Vector3:
          ((Vector3) value).Write(binaryStream);
          break;
        case DataTypeId.Vector4:
          ((Vector4) value).Write(binaryStream);
          break;
        case DataTypeId.Polygon:
          ((Polygon) value).Write(binaryStream);
          break;
        case DataTypeId.Corner:
          ((Corner) value).Write(binaryStream);
          break;
        case DataTypeId.Quaternion:
          ((Quaternion) value).Write(binaryStream);
          break;
        case DataTypeId.Matrix3x3:
          ((Matrix3x3) value).Write(binaryStream);
          break;
        case DataTypeId.Matrix4x4:
          ((Matrix4x4) value).Write(binaryStream);
          break;
        case DataTypeId.Plane:
          ((Plane) value).Write(binaryStream);
          break;
        case DataTypeId.Index2:
          ((Index2) value).Write(binaryStream);
          break;
        case DataTypeId.Index3:
          ((Index3) value).Write(binaryStream);
          break;
        case DataTypeId.Index4:
          ((Index4) value).Write(binaryStream);
          break;
      }
    }

    public static ulong GetSizeInBytes(DataTypeId dataTypeId, object value)
    {
      switch (dataTypeId)
      {
        case DataTypeId.Bool:
          return 1;
        case DataTypeId.Int8:
          return 1;
        case DataTypeId.UInt8:
          return 1;
        case DataTypeId.Int16:
          return 2;
        case DataTypeId.UInt16:
          return 2;
        case DataTypeId.Int32:
          return 4;
        case DataTypeId.UInt32:
          return 4;
        case DataTypeId.Int64:
          return 8;
        case DataTypeId.UInt64:
          return 8;
        case DataTypeId.Float:
          return 4;
        case DataTypeId.Double:
          return 8;
        case DataTypeId.String:
          return Util.GetStringSizeInBytes((string) value);
        case DataTypeId.List:
          long totalSizeInBytes1 = (long) ((List) value).GetTotalSizeInBytes();
          break;
        case DataTypeId.JaggedList:
          long totalSizeInBytes2 = (long) ((JaggedList) value).GetTotalSizeInBytes();
          break;
        case DataTypeId.Datetime:
          return 8;
        case DataTypeId.Vector2:
          long totalSizeInBytes3 = (long) ((Vector2) value).GetTotalSizeInBytes();
          break;
        case DataTypeId.Vector3:
          long totalSizeInBytes4 = (long) ((Vector3) value).GetTotalSizeInBytes();
          break;
        case DataTypeId.Vector4:
          long totalSizeInBytes5 = (long) ((Vector4) value).GetTotalSizeInBytes();
          break;
        case DataTypeId.Polygon:
          long totalSizeInBytes6 = (long) ((Polygon) value).GetTotalSizeInBytes();
          break;
        case DataTypeId.Corner:
          long totalSizeInBytes7 = (long) ((Corner) value).GetTotalSizeInBytes();
          break;
        case DataTypeId.Quaternion:
          long totalSizeInBytes8 = (long) ((Quaternion) value).GetTotalSizeInBytes();
          break;
        case DataTypeId.Matrix3x3:
          long totalSizeInBytes9 = (long) ((Matrix3x3) value).GetTotalSizeInBytes();
          break;
        case DataTypeId.Matrix4x4:
          long totalSizeInBytes10 = (long) ((Matrix4x4) value).GetTotalSizeInBytes();
          break;
        case DataTypeId.Plane:
          long totalSizeInBytes11 = (long) ((Plane) value).GetTotalSizeInBytes();
          break;
        case DataTypeId.Index2:
          long totalSizeInBytes12 = (long) ((Index2) value).GetTotalSizeInBytes();
          break;
        case DataTypeId.Index3:
          long totalSizeInBytes13 = (long) ((Index3) value).GetTotalSizeInBytes();
          break;
        case DataTypeId.Index4:
          long totalSizeInBytes14 = (long) ((Index4) value).GetTotalSizeInBytes();
          break;
      }
      return 0;
    }

    public static ulong GetStringSizeInBytes(string value)
    {
      return (ulong) (4 + Encoding.UTF8.GetBytes(value).Length);
    }
  }
}
