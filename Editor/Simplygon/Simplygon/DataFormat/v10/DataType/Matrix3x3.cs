﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.DataType.Matrix3x3
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.DataFormat.v10.DataType
{
  internal class Matrix3x3 : ISDFObject
  {
    public double _11 { get; set; }

    public double _12 { get; set; }

    public double _13 { get; set; }

    public double _21 { get; set; }

    public double _22 { get; set; }

    public double _23 { get; set; }

    public double _31 { get; set; }

    public double _32 { get; set; }

    public double _33 { get; set; }

    public ulong GetTotalSizeInBytes()
    {
      return 72;
    }

    public void Read(BinaryStream binaryStream)
    {
      this._11 = binaryStream.ReadDouble();
      this._12 = binaryStream.ReadDouble();
      this._13 = binaryStream.ReadDouble();
      this._21 = binaryStream.ReadDouble();
      this._22 = binaryStream.ReadDouble();
      this._23 = binaryStream.ReadDouble();
      this._31 = binaryStream.ReadDouble();
      this._32 = binaryStream.ReadDouble();
      this._33 = binaryStream.ReadDouble();
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteDouble(this._11);
      binaryStream.WriteDouble(this._12);
      binaryStream.WriteDouble(this._13);
      binaryStream.WriteDouble(this._21);
      binaryStream.WriteDouble(this._22);
      binaryStream.WriteDouble(this._23);
      binaryStream.WriteDouble(this._31);
      binaryStream.WriteDouble(this._32);
      binaryStream.WriteDouble(this._33);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
