﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.DataType.List
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.Collections.Generic;

namespace Simplygon.DataFormat.v10.DataType
{
  internal class List : ISDFObject
  {
    public DataTypeId DataTypeId { get; set; }

    public uint Count { get; protected set; }

    public List<object> Items { get; protected set; }

    public List()
    {
      this.Items = new List<object>();
    }

    public ulong GetTotalSizeInBytes()
    {
      ulong num = 0;
      if (this.Items.Count > 0)
        num = Util.GetSizeInBytes(this.DataTypeId, this.Items[0]) * (ulong) this.Items.Count;
      return 12UL + num;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.DataTypeId = (DataTypeId) binaryStream.ReadUInt64();
      this.Count = binaryStream.ReadUInt32();
      for (uint index = 0; index < this.Count; ++index)
        this.Items.Add(Util.Read(binaryStream, this.DataTypeId));
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Count = (uint) this.Items.Count;
      binaryStream.WriteUInt64((ulong) this.DataTypeId);
      binaryStream.WriteUInt32(this.Count);
      foreach (object obj in this.Items)
        Util.Write(binaryStream, this.DataTypeId, obj);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
