﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.DataType.Quaternion
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.DataFormat.v10.DataType
{
  internal class Quaternion : ISDFObject
  {
    public double X { get; set; }

    public double Y { get; set; }

    public double Z { get; set; }

    public double W { get; set; }

    public Quaternion()
    {
    }

    public Quaternion(double x, double y, double z, double w)
    {
      this.X = x;
      this.Y = y;
      this.Z = z;
      this.W = w;
    }

    public ulong GetTotalSizeInBytes()
    {
      return 32;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.X = binaryStream.ReadDouble();
      this.Y = binaryStream.ReadDouble();
      this.Z = binaryStream.ReadDouble();
      this.W = binaryStream.ReadDouble();
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteDouble(this.X);
      binaryStream.WriteDouble(this.Y);
      binaryStream.WriteDouble(this.Z);
      binaryStream.WriteDouble(this.W);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
