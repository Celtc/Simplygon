﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.DataType.DataTypeId
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.DataFormat.v10.DataType
{
  internal enum DataTypeId
  {
    Custom = 0,
    Bool = 1,
    Int8 = 2,
    UInt8 = 3,
    Int16 = 4,
    UInt16 = 5,
    Int32 = 6,
    UInt32 = 7,
    Int64 = 8,
    UInt64 = 9,
    Float = 10, // 0x0000000A
    Double = 11, // 0x0000000B
    String = 12, // 0x0000000C
    List = 4096, // 0x00001000
    JaggedList = 4097, // 0x00001001
    Datetime = 65536, // 0x00010000
    Vector2 = 131072, // 0x00020000
    Vector3 = 196608, // 0x00030000
    Vector4 = 262144, // 0x00040000
    Polygon = 327680, // 0x00050000
    Corner = 393216, // 0x00060000
    Quaternion = 458752, // 0x00070000
    Matrix3x3 = 524288, // 0x00080000
    Matrix4x4 = 589824, // 0x00090000
    Plane = 655360, // 0x000A0000
    Index2 = 720896, // 0x000B0000
    Index3 = 786432, // 0x000C0000
    Index4 = 851968, // 0x000D0000
  }
}
