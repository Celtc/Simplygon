﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.DataType.Plane
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.DataFormat.v10.DataType
{
  internal class Plane : ISDFObject
  {
    public double A { get; set; }

    public double B { get; set; }

    public double C { get; set; }

    public double D { get; set; }

    public Plane()
    {
    }

    public Plane(double a, double b, double c, double d)
    {
      this.A = a;
      this.B = b;
      this.C = c;
      this.D = d;
    }

    public ulong GetTotalSizeInBytes()
    {
      return 32;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.A = binaryStream.ReadDouble();
      this.B = binaryStream.ReadDouble();
      this.C = binaryStream.ReadDouble();
      this.D = binaryStream.ReadDouble();
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteDouble(this.A);
      binaryStream.WriteDouble(this.B);
      binaryStream.WriteDouble(this.C);
      binaryStream.WriteDouble(this.D);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
