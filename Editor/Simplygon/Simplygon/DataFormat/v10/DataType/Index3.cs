﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.DataType.Index3
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.DataFormat.v10.DataType
{
  internal class Index3 : ISDFObject
  {
    public int X { get; set; }

    public int Y { get; set; }

    public int Z { get; set; }

    public Index3()
    {
    }

    public Index3(int x, int y, int z)
    {
      this.X = x;
      this.Y = y;
      this.Z = z;
    }

    public ulong GetTotalSizeInBytes()
    {
      return 12;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.X = binaryStream.ReadInt32();
      this.Y = binaryStream.ReadInt32();
      this.Z = binaryStream.ReadInt32();
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteInt32(this.X);
      binaryStream.WriteInt32(this.Y);
      binaryStream.WriteInt32(this.Z);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
