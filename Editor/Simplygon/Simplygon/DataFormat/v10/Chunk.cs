﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.Chunk
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v10.DataType;
using System;
using System.Collections.Generic;

namespace Simplygon.DataFormat.v10
{
  internal class Chunk : ISDFObject
  {
    public ChunkHeader ChunkHeader { get; protected set; }

    public Attributes Attributes { get; protected set; }

    public Chunks Chunks { get; protected set; }

    public Chunk()
    {
      this.ChunkHeader = new ChunkHeader();
      this.Attributes = new Attributes();
      this.Chunks = new Chunks();
    }

    public ulong GetTotalSizeInBytes()
    {
      return this.ChunkHeader.GetTotalSizeInBytes() + this.Attributes.GetTotalSizeInBytes() + this.Chunks.GetTotalSizeInBytes();
    }

    public void Read(BinaryStream binaryStream)
    {
      this.ChunkHeader = new ChunkHeader();
      this.ChunkHeader.Read(binaryStream);
      this.Attributes = new Attributes();
      this.Attributes.Read(binaryStream);
      this.Chunks = new Chunks();
      this.Chunks.Read(binaryStream);
    }

    public void Write(BinaryStream binaryStream)
    {
      this.ChunkHeader.Write(binaryStream);
      this.Attributes.Write(binaryStream);
      this.Chunks.Write(binaryStream);
    }

    public void Update()
    {
      this.Attributes.Update();
      this.Chunks.Update();
      this.ChunkHeader.ChunksLength = this.Chunks.GetTotalSizeInBytes();
      this.ChunkHeader.ChunksOffset = this.Attributes.GetTotalSizeInBytes();
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }

    public void SetAttribute<T>(DataTypeId dataTypeId, string name, object value)
    {
      Attribute attribute = new Attribute();
      attribute.AttributeName = name;
      attribute.SetData(dataTypeId, value);
      this.Attributes.AttributeList.Add(attribute);
    }

    public void SetListAttribute<T>(DataTypeId dataTypeId, string name, List<T> srcList)
    {
      if (srcList == null || srcList.Count == 0)
        return;
      Attribute attribute = new Attribute();
      attribute.AttributeName = name;
      List list = new List();
      list.DataTypeId = dataTypeId;
      srcList.ForEach((Action<T>) (o => list.Items.Add((object) o)));
      attribute.SetData(DataTypeId.List, (object) list);
      this.Attributes.AttributeList.Add(attribute);
    }

    public void SetJaggedListAttribute<T>(DataTypeId dataTypeId, string name, List<List<T>> srcJaggedList)
    {
      if (srcJaggedList == null || srcJaggedList.Count == 0)
        return;
      Attribute attribute = new Attribute();
      attribute.AttributeName = name;
      JaggedList list = new JaggedList();
      list.DataTypeId = dataTypeId;
      list.SubListSize = (uint) srcJaggedList[0].Count;
      srcJaggedList.ForEach((Action<List<T>>) (lo =>
      {
        List<object> subList = new List<object>();
        lo.ForEach((Action<T>) (o => subList.Add((object) o)));
        list.Items.Add(subList);
      }));
      attribute.SetData(DataTypeId.JaggedList, (object) list);
      this.Attributes.AttributeList.Add(attribute);
    }
  }
}
