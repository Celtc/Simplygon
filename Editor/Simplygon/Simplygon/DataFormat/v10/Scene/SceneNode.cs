﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.Scene.SceneNode
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v10.DataType;
using System;
using System.Collections.Generic;

namespace Simplygon.DataFormat.v10.Scene
{
  internal class SceneNode : ISDFObject
  {
    public string Id { get; set; }

    public string ParentId { get; set; }

    public string Name { get; set; }

    public ulong Type { get; set; }

    public Matrix4x4 Transform { get; set; }

    public bool IsSkinned { get; set; }

    public uint RootBone { get; set; }

    public string MeshTable { get; set; }

    public uint MeshIndex { get; set; }

    public string MaterialTable { get; set; }

    public uint MaterialIndex { get; set; }

    public uint BoneIndex { get; set; }

    public double MinViewDistance { get; set; }

    public double MaxViewDistance { get; set; }

    public List<SceneNode> SceneNodes { get; set; }

    public Chunk Chunk { get; set; }

    private void InitializeDefaultValues()
    {
      this.Chunk = new Chunk();
      this.SceneNodes = new List<SceneNode>();
      this.Id = Guid.NewGuid().ToString();
    }

    public SceneNode()
    {
      this.InitializeDefaultValues();
    }

    public SceneNode(string id)
    {
      this.InitializeDefaultValues();
      this.Id = id;
    }

    public ulong GetTotalSizeInBytes()
    {
      this.Update();
      return this.Chunk.GetTotalSizeInBytes();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Update();
      this.Chunk.Write(binaryStream);
    }

    public void Read(BinaryStream binaryStream)
    {
    }

    public void Read(Chunk chunk)
    {
      this.Chunk = chunk;
      foreach (Attribute attribute in this.Chunk.Attributes.AttributeList)
      {
        if (attribute.AttributeName == "Name" && attribute.DataTypeId == DataTypeId.String)
          this.Name = (string) attribute.AttributeData;
        else if (attribute.AttributeName == "Id" && attribute.DataTypeId == DataTypeId.String)
          this.Id = (string) attribute.AttributeData;
        else if (attribute.AttributeName == "Type" && attribute.DataTypeId == DataTypeId.UInt64)
          this.Type = (ulong) attribute.AttributeData;
        else if (attribute.AttributeName == "Transform" && attribute.DataTypeId == DataTypeId.Matrix4x4)
          this.Transform = (Matrix4x4) attribute.AttributeData;
        else if (attribute.AttributeName == "IsSkinned" && attribute.DataTypeId == DataTypeId.Bool)
          this.IsSkinned = (bool) attribute.AttributeData;
        else if (attribute.AttributeName == "RootBone" && attribute.DataTypeId == DataTypeId.UInt32)
          this.RootBone = (uint) attribute.AttributeData;
        else if (attribute.AttributeName == "MeshTable" && attribute.DataTypeId == DataTypeId.String)
          this.MeshTable = (string) attribute.AttributeData;
        else if (attribute.AttributeName == "MeshIndex" && attribute.DataTypeId == DataTypeId.UInt32)
          this.MeshIndex = (uint) attribute.AttributeData;
        else if (attribute.AttributeName == "MaterialTable" && attribute.DataTypeId == DataTypeId.String)
          this.MaterialTable = (string) attribute.AttributeData;
        else if (attribute.AttributeName == "MaterialIndex" && attribute.DataTypeId == DataTypeId.UInt32)
          this.MaterialIndex = (uint) attribute.AttributeData;
        else if (attribute.AttributeName == "BoneIndex" && attribute.DataTypeId == DataTypeId.UInt32)
          this.BoneIndex = (uint) attribute.AttributeData;
        else if (attribute.AttributeName == "MinViewDistance" && attribute.DataTypeId == DataTypeId.Double)
          this.MinViewDistance = (double) attribute.AttributeData;
        else if (attribute.AttributeName == "MaxViewDistance" && attribute.DataTypeId == DataTypeId.Double)
          this.MaxViewDistance = (double) attribute.AttributeData;
      }
      foreach (Chunk chunk1 in this.Chunk.Chunks.ChunkList)
      {
        if (chunk1.ChunkHeader.ChunkType == nameof (SceneNode))
        {
          SceneNode sceneNode = new SceneNode();
          sceneNode.Read(chunk1);
          this.SceneNodes.Add(sceneNode);
        }
      }
    }

    public void Update()
    {
      this.Chunk.Attributes.AttributeList.Clear();
      this.Chunk.ChunkHeader.ChunkType = nameof (SceneNode);
      if (!string.IsNullOrEmpty(this.Name))
        this.Chunk.SetAttribute<string>(DataTypeId.String, "Name", (object) this.Name);
      if (!string.IsNullOrEmpty(this.Id))
        this.Chunk.SetAttribute<string>(DataTypeId.String, "Id", (object) this.Id);
      this.Chunk.SetAttribute<ulong>(DataTypeId.UInt64, "Type", (object) this.Type);
      this.Chunk.SetAttribute<Matrix4x4>(DataTypeId.Matrix4x4, "Transform", (object) this.Transform);
      if ((long) this.Type == 1L)
      {
        if (this.MeshTable != null)
        {
          this.Chunk.SetAttribute<string>(DataTypeId.String, "MeshTable", (object) this.MeshTable);
          this.Chunk.SetAttribute<uint>(DataTypeId.UInt32, "MeshIndex", (object) this.MeshIndex);
        }
        if (this.MaterialTable != null)
        {
          this.Chunk.SetAttribute<string>(DataTypeId.String, "MaterialTable", (object) this.MaterialTable);
          this.Chunk.SetAttribute<uint>(DataTypeId.UInt32, "MaterialIndex", (object) this.MaterialIndex);
        }
        this.Chunk.SetAttribute<bool>(DataTypeId.Bool, "IsSkinned", (object) this.IsSkinned);
        if (this.IsSkinned)
          this.Chunk.SetAttribute<uint>(DataTypeId.UInt32, "RootBone", (object) this.RootBone);
      }
      if ((long) this.Type == 2L)
        this.Chunk.SetAttribute<uint>(DataTypeId.UInt32, "BoneIndex", (object) this.BoneIndex);
      if ((long) this.Type == 4L)
      {
        this.Chunk.SetAttribute<double>(DataTypeId.Double, "MinViewDistanceAttribute", (object) this.MinViewDistance);
        this.Chunk.SetAttribute<double>(DataTypeId.Double, "MaxViewDistanceAttribute", (object) this.MaxViewDistance);
      }
      this.Chunk.Chunks.ChunkList.Clear();
      foreach (SceneNode sceneNode in this.SceneNodes)
      {
        sceneNode.Update();
        this.Chunk.Chunks.ChunkList.Add(sceneNode.Chunk);
      }
      this.Chunk.Update();
    }

    public void Validate()
    {
      foreach (SceneNode sceneNode in this.SceneNodes)
        sceneNode.Validate();
    }

    public void Release()
    {
    }
  }
}
