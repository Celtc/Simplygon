﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.Scene.TextureTable
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v10.DataType;
using System.Collections.Generic;

namespace Simplygon.DataFormat.v10.Scene
{
  internal class TextureTable : ISDFObject
  {
    public string Name { get; set; }

    public List<Texture> Textures { get; set; }

    public Chunk Chunk { get; set; }

    public TextureTable()
    {
      this.Chunk = new Chunk();
      this.Textures = new List<Texture>();
    }

    public ulong GetTotalSizeInBytes()
    {
      this.Update();
      return this.Chunk.GetTotalSizeInBytes();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Update();
      this.Chunk.Write(binaryStream);
    }

    public void Read(BinaryStream binaryStream)
    {
    }

    public void Read(Chunk chunk)
    {
      this.Chunk = chunk;
      foreach (Attribute attribute in this.Chunk.Attributes.AttributeList)
      {
        if (attribute.AttributeName == "Name" && attribute.DataTypeId == DataTypeId.String)
          this.Name = (string) attribute.AttributeData;
      }
      foreach (Chunk chunk1 in this.Chunk.Chunks.ChunkList)
      {
        if (chunk1.ChunkHeader.ChunkType == "Texture")
        {
          Texture texture = new Texture();
          texture.Read(chunk1);
          this.Textures.Add(texture);
        }
      }
    }

    public void Update()
    {
      this.Chunk.ChunkHeader.ChunkType = nameof (TextureTable);
      Attribute attribute = new Attribute();
      attribute.AttributeName = "Name";
      attribute.SetData(DataTypeId.String, (object) this.Name);
      this.Chunk.Attributes.AttributeList.Clear();
      this.Chunk.Attributes.AttributeList.Add(attribute);
      this.Chunk.Chunks.ChunkList.Clear();
      foreach (Texture texture in this.Textures)
      {
        texture.Update();
        this.Chunk.Chunks.ChunkList.Add(texture.Chunk);
      }
      this.Chunk.Update();
    }

    public void Validate()
    {
      foreach (Texture texture in this.Textures)
        texture.Validate();
    }

    public void Release()
    {
    }
  }
}
