﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.Scene.SSFFile
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.Collections.Generic;
using System.IO;

namespace Simplygon.DataFormat.v10.Scene
{
  internal class SSFFile
  {
    public FileHeader FileHeader { get; set; }

    public List<SceneRoot> SceneRoots { get; set; }

    public List<MeshTable> MeshTables { get; set; }

    public List<MaterialTable> MaterialTables { get; set; }

    public List<TextureTable> TextureTables { get; set; }

    public SSFFile()
    {
      this.FileHeader = new FileHeader();
      this.SceneRoots = new List<SceneRoot>();
      this.MeshTables = new List<MeshTable>();
      this.MaterialTables = new List<MaterialTable>();
      this.TextureTables = new List<TextureTable>();
    }

    public SSFFile(string toolName, int toolMajorVersion, int toolMinorVersion, int toolBuildVersion, byte[] formatSignature)
    {
      this.FileHeader = new FileHeader();
      this.SceneRoots = new List<SceneRoot>();
      this.MeshTables = new List<MeshTable>();
      this.MaterialTables = new List<MaterialTable>();
      this.TextureTables = new List<TextureTable>();
      this.FileHeader.FileVersion.MajorVersion = (byte) 1;
      this.FileHeader.FileVersion.MinorVersion = (byte) 0;
      this.FileHeader.Tool.Name = toolName;
      this.FileHeader.Tool.MajorVersion = (byte) toolMajorVersion;
      this.FileHeader.Tool.MinorVersion = (byte) toolMinorVersion;
      this.FileHeader.Tool.BuildVersion = (ushort) toolBuildVersion;
      if (formatSignature.Length != 8)
        return;
      this.FileHeader.FormatSignature.Signature = formatSignature;
    }

    public void Read(string fileName)
    {
      using (BinaryReader binaryReader = new BinaryReader((Stream) File.Open(fileName, FileMode.Open)))
      {
        BinaryStream binaryStream = new BinaryStream(binaryReader);
        binaryStream.ComputeCRC = true;
        this.FileHeader.Read(binaryStream);
        new Attributes().Read(binaryStream);
        Chunks chunks = new Chunks();
        chunks.Read(binaryStream);
        foreach (Chunk chunk in chunks.ChunkList)
        {
          if (chunk.ChunkHeader.ChunkType == "SceneRoot")
          {
            SceneRoot sceneRoot = new SceneRoot();
            sceneRoot.Read(chunk);
            this.SceneRoots.Add(sceneRoot);
          }
          else if (chunk.ChunkHeader.ChunkType == "MeshTable")
          {
            MeshTable meshTable = new MeshTable();
            meshTable.Read(chunk);
            this.MeshTables.Add(meshTable);
          }
          else if (chunk.ChunkHeader.ChunkType == "MaterialTable")
          {
            MaterialTable materialTable = new MaterialTable();
            materialTable.Read(chunk);
            this.MaterialTables.Add(materialTable);
          }
          else if (chunk.ChunkHeader.ChunkType == "TextureTable")
          {
            TextureTable textureTable = new TextureTable();
            textureTable.Read(chunk);
            this.TextureTables.Add(textureTable);
          }
        }
        binaryStream.ComputeCRC = false;
        uint num = binaryStream.ReadUInt32();
        if ((int) binaryStream.CRC != (int) num)
          throw new Exception("Invalid CRC value");
      }
    }

    public void Write(string fileName)
    {
      using (BinaryWriter binaryWriter = new BinaryWriter((Stream) File.Open(fileName, FileMode.Create)))
      {
        BinaryStream binaryStream = new BinaryStream(binaryWriter);
        binaryStream.ComputeCRC = true;
        this.FileHeader.Write(binaryStream);
        Attributes attributes = new Attributes();
        attributes.Update();
        attributes.Write(binaryStream);
        Chunks chunks = new Chunks();
        foreach (SceneRoot sceneRoot in this.SceneRoots)
        {
          sceneRoot.Update();
          chunks.ChunkList.Add(sceneRoot.Chunk);
        }
        foreach (MeshTable meshTable in this.MeshTables)
        {
          meshTable.Update();
          chunks.ChunkList.Add(meshTable.Chunk);
        }
        foreach (MaterialTable materialTable in this.MaterialTables)
        {
          materialTable.Update();
          chunks.ChunkList.Add(materialTable.Chunk);
        }
        foreach (TextureTable textureTable in this.TextureTables)
        {
          textureTable.Update();
          chunks.ChunkList.Add(textureTable.Chunk);
        }
        chunks.Update();
        chunks.Write(binaryStream);
        binaryStream.ComputeCRC = false;
        binaryStream.WriteUInt32(binaryStream.CRC);
      }
    }
  }
}
