﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.Scene.Material
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v10.DataType;
using System.Collections.Generic;

namespace Simplygon.DataFormat.v10.Scene
{
  internal class Material : ISDFObject
  {
    public string Name { get; set; }

    public Vector4 AmbientColor { get; set; }

    public Vector4 DiffuseColor { get; set; }

    public Vector4 SpecularColor { get; set; }

    public double Shininess { get; set; }

    public double Opacity { get; set; }

    public List<MaterialChannel> MaterialChannels { get; set; }

    public Chunk Chunk { get; set; }

    public Material()
    {
      this.Chunk = new Chunk();
      this.MaterialChannels = new List<MaterialChannel>();
      this.AmbientColor = new Vector4();
      this.DiffuseColor = new Vector4();
      this.SpecularColor = new Vector4();
    }

    public ulong GetTotalSizeInBytes()
    {
      this.Update();
      return this.Chunk.GetTotalSizeInBytes();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Update();
      this.Chunk.Write(binaryStream);
    }

    public void Read(BinaryStream binaryStream)
    {
    }

    public void Read(Chunk chunk)
    {
      this.Chunk = chunk;
      foreach (Attribute attribute in this.Chunk.Attributes.AttributeList)
      {
        if (attribute.AttributeName == "Name" && attribute.DataTypeId == DataTypeId.String)
          this.Name = (string) attribute.AttributeData;
        else if (attribute.AttributeName == "AmbientColor" && attribute.DataTypeId == DataTypeId.Vector4)
          this.AmbientColor = (Vector4) attribute.AttributeData;
        else if (attribute.AttributeName == "DiffuseColor" && attribute.DataTypeId == DataTypeId.Vector4)
          this.DiffuseColor = (Vector4) attribute.AttributeData;
        else if (attribute.AttributeName == "SpecularColor" && attribute.DataTypeId == DataTypeId.Vector4)
          this.SpecularColor = (Vector4) attribute.AttributeData;
        else if (attribute.AttributeName == "Shininess" && attribute.DataTypeId == DataTypeId.Double)
          this.Shininess = (double) attribute.AttributeData;
        else if (attribute.AttributeName == "Opacity" && attribute.DataTypeId == DataTypeId.Double)
          this.Opacity = (double) attribute.AttributeData;
      }
      this.MaterialChannels.Clear();
      foreach (Chunk chunk1 in this.Chunk.Chunks.ChunkList)
      {
        if (chunk1.ChunkHeader.ChunkType == "MaterialChannel")
        {
          MaterialChannel materialChannel = new MaterialChannel();
          materialChannel.Read(chunk1);
          this.MaterialChannels.Add(materialChannel);
        }
      }
    }

    public void Update()
    {
      this.Chunk.ChunkHeader.ChunkType = nameof (Material);
      Attribute attribute1 = new Attribute();
      attribute1.AttributeName = "Name";
      attribute1.SetData(DataTypeId.String, (object) this.Name);
      Attribute attribute2 = new Attribute();
      attribute2.AttributeName = "AmbientColor";
      attribute2.SetData(DataTypeId.Vector4, (object) this.AmbientColor);
      Attribute attribute3 = new Attribute();
      attribute3.AttributeName = "DiffuseColor";
      attribute3.SetData(DataTypeId.Vector4, (object) this.DiffuseColor);
      Attribute attribute4 = new Attribute();
      attribute4.AttributeName = "SpecularColor";
      attribute4.SetData(DataTypeId.Vector4, (object) this.SpecularColor);
      Attribute attribute5 = new Attribute();
      attribute5.AttributeName = "Shininess";
      attribute5.SetData(DataTypeId.Double, (object) this.Shininess);
      Attribute attribute6 = new Attribute();
      attribute6.AttributeName = "Opacity";
      attribute6.SetData(DataTypeId.Double, (object) this.Opacity);
      this.Chunk.Attributes.AttributeList.Clear();
      this.Chunk.Attributes.AttributeList.Add(attribute1);
      this.Chunk.Attributes.AttributeList.Add(attribute2);
      this.Chunk.Attributes.AttributeList.Add(attribute3);
      this.Chunk.Attributes.AttributeList.Add(attribute4);
      this.Chunk.Attributes.AttributeList.Add(attribute5);
      this.Chunk.Attributes.AttributeList.Add(attribute6);
      this.Chunk.Chunks.ChunkList.Clear();
      foreach (MaterialChannel materialChannel in this.MaterialChannels)
      {
        materialChannel.Update();
        this.Chunk.Chunks.ChunkList.Add(materialChannel.Chunk);
      }
      this.Chunk.Update();
    }

    public void Validate()
    {
      foreach (MaterialChannel materialChannel in this.MaterialChannels)
        materialChannel.Validate();
    }

    public void Release()
    {
    }
  }
}
