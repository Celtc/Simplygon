﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.Scene.MaterialType
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.DataFormat.v10.Scene
{
  internal enum MaterialType
  {
    Ambient,
    Diffuse,
    Specular,
    Opacity,
    Emissive,
    Normals,
    GroupIds,
    Displacement,
  }
}
