﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.Scene.SceneRoot
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v10.DataType;
using System;
using System.Collections.Generic;

namespace Simplygon.DataFormat.v10.Scene
{
  internal class SceneRoot : ISDFObject
  {
    public string Name { get; set; }

    public string Id { get; set; }

    public List<SceneNode> SceneNodes { get; set; }

    public Chunk Chunk { get; set; }

    private void InitializeDefaultValues()
    {
      this.Chunk = new Chunk();
      this.SceneNodes = new List<SceneNode>();
      this.Id = Guid.NewGuid().ToString();
    }

    public SceneRoot()
    {
      this.InitializeDefaultValues();
    }

    public SceneRoot(string id)
    {
      this.InitializeDefaultValues();
      this.Id = id;
    }

    public ulong GetTotalSizeInBytes()
    {
      this.Update();
      return this.Chunk.GetTotalSizeInBytes();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Update();
      this.Chunk.Write(binaryStream);
    }

    public void Read(BinaryStream binaryStream)
    {
    }

    public void Read(Chunk chunk)
    {
      this.Chunk = chunk;
      foreach (Attribute attribute in this.Chunk.Attributes.AttributeList)
      {
        if (attribute.AttributeName == "Name" && attribute.DataTypeId == DataTypeId.String)
          this.Name = (string) attribute.AttributeData;
        else if (attribute.AttributeName == "Id" && attribute.DataTypeId == DataTypeId.String)
          this.Id = (string) attribute.AttributeData;
      }
      foreach (Chunk chunk1 in this.Chunk.Chunks.ChunkList)
      {
        if (chunk1.ChunkHeader.ChunkType == "SceneNode")
        {
          SceneNode sceneNode = new SceneNode();
          sceneNode.Read(chunk1);
          this.SceneNodes.Add(sceneNode);
        }
      }
    }

    public void Update()
    {
      this.Chunk.ChunkHeader.ChunkType = nameof (SceneRoot);
      this.Chunk.Attributes.AttributeList.Clear();
      this.Chunk.SetAttribute<string>(DataTypeId.String, "Name", (object) this.Name);
      this.Chunk.Chunks.ChunkList.Clear();
      foreach (SceneNode sceneNode in this.SceneNodes)
      {
        sceneNode.Update();
        this.Chunk.Chunks.ChunkList.Add(sceneNode.Chunk);
      }
      this.Chunk.Update();
    }

    public void Validate()
    {
      foreach (SceneNode sceneNode in this.SceneNodes)
        sceneNode.Validate();
    }

    public void Release()
    {
    }
  }
}
