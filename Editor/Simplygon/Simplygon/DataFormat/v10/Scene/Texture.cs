﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.Scene.Texture
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v10.DataType;

namespace Simplygon.DataFormat.v10.Scene
{
  internal class Texture : ISDFObject
  {
    public string Name { get; set; }

    public TextureType Type { get; set; }

    public string Path { get; set; }

    public Chunk Chunk { get; set; }

    public Texture()
    {
      this.Chunk = new Chunk();
    }

    public ulong GetTotalSizeInBytes()
    {
      this.Update();
      return this.Chunk.GetTotalSizeInBytes();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Update();
      this.Chunk.Write(binaryStream);
    }

    public void Read(BinaryStream binaryStream)
    {
    }

    public void Read(Chunk chunk)
    {
      this.Chunk = chunk;
      foreach (Attribute attribute in this.Chunk.Attributes.AttributeList)
      {
        if (attribute.AttributeName == "Name" && attribute.DataTypeId == DataTypeId.String)
          this.Name = (string) attribute.AttributeData;
        else if (attribute.AttributeName == "Type" && attribute.DataTypeId == DataTypeId.UInt32)
          this.Type = (TextureType) (uint) attribute.AttributeData;
        else if (attribute.AttributeName == "Path" && attribute.DataTypeId == DataTypeId.String)
          this.Path = (string) attribute.AttributeData;
      }
    }

    public void Update()
    {
      this.Chunk.ChunkHeader.ChunkType = nameof (Texture);
      Attribute attribute1 = new Attribute();
      attribute1.AttributeName = "Name";
      attribute1.SetData(DataTypeId.String, (object) this.Name);
      Attribute attribute2 = new Attribute();
      attribute2.AttributeName = "Type";
      attribute2.SetData(DataTypeId.UInt32, (object) (uint) this.Type);
      Attribute attribute3 = new Attribute();
      attribute3.AttributeName = "Path";
      attribute3.SetData(DataTypeId.String, (object) this.Path);
      this.Chunk.Attributes.AttributeList.Clear();
      this.Chunk.Attributes.AttributeList.Add(attribute1);
      this.Chunk.Attributes.AttributeList.Add(attribute2);
      this.Chunk.Attributes.AttributeList.Add(attribute3);
      this.Chunk.Update();
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
