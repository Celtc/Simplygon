﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.Scene.MaterialTable
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v10.DataType;
using System.Collections.Generic;

namespace Simplygon.DataFormat.v10.Scene
{
  internal class MaterialTable : ISDFObject
  {
    public string Name { get; set; }

    public List<Material> Materials { get; set; }

    public Chunk Chunk { get; set; }

    public MaterialTable()
    {
      this.Chunk = new Chunk();
      this.Materials = new List<Material>();
    }

    public ulong GetTotalSizeInBytes()
    {
      this.Update();
      return this.Chunk.GetTotalSizeInBytes();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Update();
      this.Chunk.Write(binaryStream);
    }

    public void Read(BinaryStream binaryStream)
    {
    }

    public void Read(Chunk chunk)
    {
      this.Chunk = chunk;
      foreach (Attribute attribute in this.Chunk.Attributes.AttributeList)
      {
        if (attribute.AttributeName == "Name" && attribute.DataTypeId == DataTypeId.String)
          this.Name = (string) attribute.AttributeData;
      }
      foreach (Chunk chunk1 in this.Chunk.Chunks.ChunkList)
      {
        if (chunk1.ChunkHeader.ChunkType == "Material")
        {
          Material material = new Material();
          material.Read(chunk1);
          this.Materials.Add(material);
        }
      }
    }

    public void Update()
    {
      this.Chunk.ChunkHeader.ChunkType = nameof (MaterialTable);
      Attribute attribute = new Attribute();
      attribute.AttributeName = "Name";
      attribute.SetData(DataTypeId.String, (object) this.Name);
      this.Chunk.Attributes.AttributeList.Clear();
      this.Chunk.Attributes.AttributeList.Add(attribute);
      this.Chunk.Chunks.ChunkList.Clear();
      foreach (Material material in this.Materials)
      {
        material.Update();
        this.Chunk.Chunks.ChunkList.Add(material.Chunk);
      }
      this.Chunk.Update();
    }

    public void Validate()
    {
      foreach (Material material in this.Materials)
        material.Validate();
    }

    public void Release()
    {
    }
  }
}
