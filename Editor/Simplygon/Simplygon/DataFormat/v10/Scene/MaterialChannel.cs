﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.Scene.MaterialChannel
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v10.DataType;

namespace Simplygon.DataFormat.v10.Scene
{
  internal class MaterialChannel : ISDFObject
  {
    public string TextureTable { get; set; }

    public uint TextureIndex { get; set; }

    public Vector4 Color { get; set; }

    public BlendMode BlendMode { get; set; }

    public MaterialType MaterialType { get; set; }

    public Chunk Chunk { get; set; }

    public MaterialChannel()
    {
      this.Chunk = new Chunk();
      this.Color = new Vector4(1.0, 1.0, 1.0, 1.0);
    }

    public ulong GetTotalSizeInBytes()
    {
      this.Update();
      return this.Chunk.GetTotalSizeInBytes();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Update();
      this.Chunk.Write(binaryStream);
    }

    public void Read(BinaryStream binaryStream)
    {
    }

    public void Read(Chunk chunk)
    {
      this.Chunk = chunk;
      foreach (Attribute attribute in this.Chunk.Attributes.AttributeList)
      {
        if (attribute.AttributeName == "TextureTable" && attribute.DataTypeId == DataTypeId.String)
          this.TextureTable = (string) attribute.AttributeData;
        else if (attribute.AttributeName == "TextureIndex" && attribute.DataTypeId == DataTypeId.UInt32)
          this.TextureIndex = (uint) attribute.AttributeData;
        else if (attribute.AttributeName == "Color" && attribute.DataTypeId == DataTypeId.Vector4)
          this.Color = (Vector4) attribute.AttributeData;
        else if (attribute.AttributeName == "BlendMode" && attribute.DataTypeId == DataTypeId.UInt32)
          this.BlendMode = (BlendMode) (uint) attribute.AttributeData;
        else if (attribute.AttributeName == "MaterialType" && attribute.DataTypeId == DataTypeId.UInt32)
          this.MaterialType = (MaterialType) (uint) attribute.AttributeData;
      }
    }

    public void Update()
    {
      this.Chunk.ChunkHeader.ChunkType = nameof (MaterialChannel);
      this.Chunk.Attributes.AttributeList.Clear();
      if (!string.IsNullOrEmpty(this.TextureTable))
      {
        Attribute attribute1 = new Attribute();
        attribute1.AttributeName = "TextureTable";
        attribute1.SetData(DataTypeId.String, (object) this.TextureTable);
        Attribute attribute2 = new Attribute();
        attribute2.AttributeName = "TextureIndex";
        attribute2.SetData(DataTypeId.UInt32, (object) this.TextureIndex);
        this.Chunk.Attributes.AttributeList.Add(attribute1);
        this.Chunk.Attributes.AttributeList.Add(attribute2);
      }
      Attribute attribute3 = new Attribute();
      attribute3.AttributeName = "Color";
      attribute3.SetData(DataTypeId.Vector4, (object) this.Color);
      Attribute attribute4 = new Attribute();
      attribute4.AttributeName = "BlendMode";
      attribute4.SetData(DataTypeId.UInt32, (object) (uint) this.BlendMode);
      Attribute attribute5 = new Attribute();
      attribute5.AttributeName = "MaterialType";
      attribute5.SetData(DataTypeId.UInt32, (object) (uint) this.MaterialType);
      this.Chunk.Attributes.AttributeList.Add(attribute3);
      this.Chunk.Attributes.AttributeList.Add(attribute4);
      this.Chunk.Attributes.AttributeList.Add(attribute5);
      this.Chunk.Update();
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
