﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.Scene.Mesh
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v10.DataType;
using System;
using System.Collections.Generic;

namespace Simplygon.DataFormat.v10.Scene
{
  internal class Mesh : ISDFObject
  {
    public string Name { get; set; }

    public string Id { get; set; }

    public string ParentId { get; set; }

    public uint LODIndex { get; set; }

    public ulong TypeOfMesh { get; set; }

    public List<Vector3> Coordinates { get; set; }

    public List<Index3> Triangles { get; set; }

    public List<Polygon> Polygons { get; set; }

    public List<Simplygon.DataFormat.v10.DataType.TextureCoordinates> TextureCoordinates { get; set; }

    public List<Simplygon.DataFormat.v10.DataType.TextureCoordinates> TextureCoordinatesPerCorner { get; set; }

    public List<Vector3> Normals { get; set; }

    public List<Vector4> Tangents { get; set; }

    public List<Vector3> Bitangents { get; set; }

    public List<Simplygon.DataFormat.v10.DataType.Colors> Colors { get; set; }

    public List<List<double>> BoneWeights { get; set; }

    public List<List<int>> BoneIndices { get; set; }

    public uint MaterialIndexPerMesh { get; set; }

    public List<bool> VertexLocks { get; set; }

    public List<double> VertexWeights { get; set; }

    public List<uint> MaterialIndexPerPolygon { get; set; }

    public Chunk Chunk { get; set; }

    private void InitializeDefaultValues()
    {
      this.Chunk = new Chunk();
      this.Coordinates = new List<Vector3>();
      this.Triangles = new List<Index3>();
      this.Polygons = new List<Polygon>();
      this.TextureCoordinates = new List<Simplygon.DataFormat.v10.DataType.TextureCoordinates>();
      this.TextureCoordinatesPerCorner = new List<Simplygon.DataFormat.v10.DataType.TextureCoordinates>();
      this.Normals = new List<Vector3>();
      this.Tangents = new List<Vector4>();
      this.Bitangents = new List<Vector3>();
      this.Colors = new List<Simplygon.DataFormat.v10.DataType.Colors>();
      this.BoneWeights = new List<List<double>>();
      this.BoneIndices = new List<List<int>>();
      this.VertexLocks = new List<bool>();
      this.VertexWeights = new List<double>();
      this.MaterialIndexPerPolygon = new List<uint>();
      this.Id = Guid.NewGuid().ToString();
      this.ParentId = string.Empty;
      this.LODIndex = 0U;
      this.TypeOfMesh = 0UL;
    }

    public Mesh()
    {
      this.InitializeDefaultValues();
    }

    public Mesh(string id)
    {
      this.InitializeDefaultValues();
      this.Id = id;
    }

    public Mesh(string id, uint lodIndex)
    {
      this.InitializeDefaultValues();
      this.Id = id;
      this.LODIndex = lodIndex;
    }

    public Mesh(string id, string parentId)
    {
      this.InitializeDefaultValues();
      this.Id = id;
      this.ParentId = parentId;
    }

    public Mesh(string id, string parentId, uint lodIndex)
    {
      this.InitializeDefaultValues();
      this.Id = id;
      this.ParentId = parentId;
      this.LODIndex = lodIndex;
    }

    public ulong GetTotalSizeInBytes()
    {
      this.Update();
      return this.Chunk.GetTotalSizeInBytes();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Update();
      this.Chunk.Write(binaryStream);
    }

    public void Read(BinaryStream binaryStream)
    {
    }

    public void Read(Chunk chunk)
    {
      this.Chunk = chunk;
      this.Coordinates.Clear();
      this.Triangles.Clear();
      this.Polygons.Clear();
      this.TextureCoordinates.Clear();
      this.Normals.Clear();
      this.Tangents.Clear();
      this.Colors.Clear();
      this.BoneWeights.Clear();
      this.BoneIndices.Clear();
      this.VertexLocks.Clear();
      this.VertexWeights.Clear();
      this.MaterialIndexPerPolygon.Clear();
      foreach (Attribute attribute in this.Chunk.Attributes.AttributeList)
      {
        if (attribute.AttributeName == "Name" && attribute.DataTypeId == DataTypeId.String)
          this.Name = (string) attribute.AttributeData;
        else if (attribute.AttributeName == "Id" && attribute.DataTypeId == DataTypeId.String)
          this.Id = (string) attribute.AttributeData;
        else if (attribute.AttributeName == "ParentId" && attribute.DataTypeId == DataTypeId.String)
          this.ParentId = (string) attribute.AttributeData;
        else if (attribute.AttributeName == "LODIndex" && attribute.DataTypeId == DataTypeId.UInt32)
          this.LODIndex = (uint) attribute.AttributeData;
        else if (attribute.AttributeName == "TypeOfMesh" && attribute.DataTypeId == DataTypeId.UInt64)
          this.TypeOfMesh = (ulong) attribute.AttributeData;
        else if (attribute.AttributeName == "Coordinates" && attribute.DataTypeId == DataTypeId.List)
        {
          List attributeData = (List) attribute.AttributeData;
          if (attributeData.DataTypeId == DataTypeId.Vector3)
            attributeData.Items.ForEach((Action<object>) (o => this.Coordinates.Add((Vector3) o)));
        }
        else if (attribute.AttributeName == "TextureCoordinateChannel" && attribute.DataTypeId == DataTypeId.UInt32)
        {
          uint attributeData = (uint) attribute.AttributeData;
          this.TextureCoordinates.Add(new Simplygon.DataFormat.v10.DataType.TextureCoordinates()
          {
            Channel = attributeData
          });
        }
        else if (attribute.AttributeName == "TextureCoordinates" && attribute.DataTypeId == DataTypeId.List)
        {
          if (this.TextureCoordinates.Count > 0)
          {
            List attributeData = (List) attribute.AttributeData;
            if (attributeData.DataTypeId == DataTypeId.Vector2)
              attributeData.Items.ForEach((Action<object>) (o => this.TextureCoordinates[this.TextureCoordinates.Count - 1].TextureCoordinateList.Add((Vector2) o)));
          }
        }
        else if (attribute.AttributeName == "TextureCoordinateChannelPerCorner" && attribute.DataTypeId == DataTypeId.UInt32)
        {
          uint attributeData = (uint) attribute.AttributeData;
          this.TextureCoordinatesPerCorner.Add(new Simplygon.DataFormat.v10.DataType.TextureCoordinates()
          {
            Channel = attributeData
          });
        }
        else if (attribute.AttributeName == "TextureCoordinatesPerCorner" && attribute.DataTypeId == DataTypeId.List)
        {
          if (this.TextureCoordinatesPerCorner.Count > 0)
          {
            List attributeData = (List) attribute.AttributeData;
            if (attributeData.DataTypeId == DataTypeId.Vector2)
              attributeData.Items.ForEach((Action<object>) (o => this.TextureCoordinatesPerCorner[this.TextureCoordinatesPerCorner.Count - 1].TextureCoordinateList.Add((Vector2) o)));
          }
        }
        else if (attribute.AttributeName == "Normals" && attribute.DataTypeId == DataTypeId.List)
        {
          List attributeData = (List) attribute.AttributeData;
          if (attributeData.DataTypeId == DataTypeId.Vector3)
            attributeData.Items.ForEach((Action<object>) (o => this.Normals.Add((Vector3) o)));
        }
        else if (attribute.AttributeName == "Tangents" && attribute.DataTypeId == DataTypeId.List)
        {
          List attributeData = (List) attribute.AttributeData;
          if (attributeData.DataTypeId == DataTypeId.Vector4)
            attributeData.Items.ForEach((Action<object>) (o => this.Tangents.Add((Vector4) o)));
        }
        else if (attribute.AttributeName == "Bitangents" && attribute.DataTypeId == DataTypeId.List)
        {
          List attributeData = (List) attribute.AttributeData;
          if (attributeData.DataTypeId == DataTypeId.Vector3)
            attributeData.Items.ForEach((Action<object>) (o => this.Bitangents.Add((Vector3) o)));
        }
        else if (attribute.AttributeName == "ColorChannel" && attribute.DataTypeId == DataTypeId.UInt32)
        {
          uint attributeData = (uint) attribute.AttributeData;
          this.Colors.Add(new Simplygon.DataFormat.v10.DataType.Colors()
          {
            Channel = attributeData
          });
        }
        else if (attribute.AttributeName == "Colors" && attribute.DataTypeId == DataTypeId.List)
        {
          List attributeData = (List) attribute.AttributeData;
          if (attributeData.DataTypeId == DataTypeId.Vector4)
            attributeData.Items.ForEach((Action<object>) (o => this.Colors[this.Colors.Count - 1].ColorList.Add((Vector4) o)));
        }
        else if (attribute.AttributeName == "BoneWeights" && attribute.DataTypeId == DataTypeId.JaggedList)
        {
          JaggedList attributeData = (JaggedList) attribute.AttributeData;
          if (attributeData.DataTypeId == DataTypeId.Double)
            attributeData.Items.ForEach((Action<List<object>>) (ol =>
            {
              List<double> l = new List<double>();
              ol.ForEach((Action<object>) (o => l.Add((double) o)));
              this.BoneWeights.Add(l);
            }));
        }
        else if (attribute.AttributeName == "BoneIndices" && attribute.DataTypeId == DataTypeId.JaggedList)
        {
          JaggedList attributeData = (JaggedList) attribute.AttributeData;
          if (attributeData.DataTypeId == DataTypeId.Int32)
            attributeData.Items.ForEach((Action<List<object>>) (ol =>
            {
              List<int> l = new List<int>();
              ol.ForEach((Action<object>) (o => l.Add((int) o)));
              this.BoneIndices.Add(l);
            }));
        }
        else if (attribute.AttributeName == "VertexLocks" && attribute.DataTypeId == DataTypeId.List)
        {
          List attributeData = (List) attribute.AttributeData;
          if (attributeData.DataTypeId == DataTypeId.Bool)
            attributeData.Items.ForEach((Action<object>) (o => this.VertexLocks.Add((bool) o)));
        }
        else if (attribute.AttributeName == "VertexWeights" && attribute.DataTypeId == DataTypeId.List)
        {
          List attributeData = (List) attribute.AttributeData;
          if (attributeData.DataTypeId == DataTypeId.Double)
            attributeData.Items.ForEach((Action<object>) (o => this.VertexWeights.Add((double) o)));
        }
        else if (attribute.AttributeName == "Triangles" && attribute.DataTypeId == DataTypeId.List)
        {
          List attributeData = (List) attribute.AttributeData;
          if (attributeData.DataTypeId == DataTypeId.Index3)
            attributeData.Items.ForEach((Action<object>) (o => this.Triangles.Add((Index3) o)));
        }
        else if (attribute.AttributeName == "Polygons" && attribute.DataTypeId == DataTypeId.List)
        {
          List attributeData = (List) attribute.AttributeData;
          if (attributeData.DataTypeId == DataTypeId.Polygon)
            attributeData.Items.ForEach((Action<object>) (o => this.Polygons.Add((Polygon) o)));
        }
        else if (attribute.AttributeName == "MaterialIndexPerMesh" && attribute.DataTypeId == DataTypeId.UInt32)
          this.MaterialIndexPerMesh = (uint) attribute.AttributeData;
        else if (attribute.AttributeName == "MaterialIndexPerPolygon" && attribute.DataTypeId == DataTypeId.List)
        {
          List attributeData = (List) attribute.AttributeData;
          if (attributeData.DataTypeId == DataTypeId.UInt32)
            attributeData.Items.ForEach((Action<object>) (o => this.MaterialIndexPerPolygon.Add((uint) o)));
        }
      }
    }

    public void Update()
    {
      this.Chunk.ChunkHeader.ChunkType = nameof (Mesh);
      this.Chunk.Attributes.AttributeList.Clear();
      this.Chunk.SetAttribute<string>(DataTypeId.String, "Name", (object) this.Name);
      this.Chunk.SetAttribute<string>(DataTypeId.String, "Id", (object) this.Id);
      if (this.ParentId != null)
        this.Chunk.SetAttribute<string>(DataTypeId.String, "ParentId", (object) this.ParentId);
      this.Chunk.SetAttribute<uint>(DataTypeId.UInt32, "LODIndex", (object) this.LODIndex);
      this.Chunk.SetAttribute<ulong>(DataTypeId.UInt64, "TypeOfMesh", (object) this.TypeOfMesh);
      this.Chunk.SetListAttribute<Vector3>(DataTypeId.Vector3, "Coordinates", this.Coordinates);
      foreach (Simplygon.DataFormat.v10.DataType.TextureCoordinates textureCoordinate in this.TextureCoordinates)
      {
        this.Chunk.SetAttribute<uint>(DataTypeId.UInt32, "TextureCoordinateChannel", (object) textureCoordinate.Channel);
        this.Chunk.SetListAttribute<Vector2>(DataTypeId.Vector2, "TextureCoordinates", textureCoordinate.TextureCoordinateList);
      }
      foreach (Simplygon.DataFormat.v10.DataType.TextureCoordinates textureCoordinates in this.TextureCoordinatesPerCorner)
      {
        this.Chunk.SetAttribute<uint>(DataTypeId.UInt32, "TextureCoordinateChannelPerCorner", (object) textureCoordinates.Channel);
        this.Chunk.SetListAttribute<Vector2>(DataTypeId.Vector2, "TextureCoordinatesPerCorner", textureCoordinates.TextureCoordinateList);
      }
      this.Chunk.SetListAttribute<Vector3>(DataTypeId.Vector3, "Normals", this.Normals);
      this.Chunk.SetListAttribute<Vector4>(DataTypeId.Vector4, "Tangents", this.Tangents);
      this.Chunk.SetListAttribute<Vector3>(DataTypeId.Vector3, "Bitangents", this.Bitangents);
      foreach (Simplygon.DataFormat.v10.DataType.Colors color in this.Colors)
      {
        this.Chunk.SetAttribute<uint>(DataTypeId.UInt32, "ColorChannel", (object) color.Channel);
        this.Chunk.SetListAttribute<Vector4>(DataTypeId.Vector4, "Colors", color.ColorList);
      }
      this.Chunk.SetJaggedListAttribute<double>(DataTypeId.Double, "BoneWeights", this.BoneWeights);
      this.Chunk.SetJaggedListAttribute<int>(DataTypeId.Int32, "BoneIndices", this.BoneIndices);
      this.Chunk.SetListAttribute<bool>(DataTypeId.Bool, "VertexLocks", this.VertexLocks);
      this.Chunk.SetListAttribute<double>(DataTypeId.Double, "VertexWeights", this.VertexWeights);
      this.Chunk.SetListAttribute<Index3>(DataTypeId.Index3, "Triangles", this.Triangles);
      this.Chunk.SetListAttribute<Polygon>(DataTypeId.Polygon, "Polygon", this.Polygons);
      this.Chunk.SetAttribute<uint>(DataTypeId.UInt32, "MaterialIndexPerMesh", (object) this.MaterialIndexPerMesh);
      this.Chunk.SetListAttribute<uint>(DataTypeId.UInt32, "MaterialIndexPerPolygon", this.MaterialIndexPerPolygon);
      this.Chunk.Update();
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
