﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v10.Attribute
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v10.DataType;
using System;
using System.Globalization;

namespace Simplygon.DataFormat.v10
{
  internal class Attribute : ISDFObject
  {
    public DataTypeId DataTypeId { get; protected set; }

    public string AttributeName { get; set; }

    public ulong AttributeLength { get; protected set; }

    public object AttributeData { get; protected set; }

    public void SetData(DataTypeId dataTypeId, object data)
    {
      if (data == null)
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "Invalid attribute {0} with type {1}", (object) this.AttributeName, (object) dataTypeId));
      this.DataTypeId = dataTypeId;
      this.AttributeData = data;
      this.AttributeLength = Util.GetSizeInBytes(this.DataTypeId, this.AttributeData);
    }

    public void SetStringData(string value)
    {
      this.DataTypeId = DataTypeId.String;
      this.AttributeData = (object) value;
      this.AttributeLength = Util.GetSizeInBytes(this.DataTypeId, this.AttributeData);
    }

    public string GetStringData()
    {
      if (this.AttributeData == null || this.DataTypeId != DataTypeId.String)
        return string.Empty;
      return (string) this.AttributeData;
    }

    public ulong GetTotalSizeInBytes()
    {
      return (ulong) (8L + (long) Util.GetStringSizeInBytes(this.AttributeName) + 8L) + this.AttributeLength;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.DataTypeId = (DataTypeId) binaryStream.ReadUInt64();
      this.AttributeName = binaryStream.ReadString();
      this.AttributeLength = binaryStream.ReadUInt64();
      this.AttributeData = Util.Read(binaryStream, this.DataTypeId);
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteUInt64((ulong) this.DataTypeId);
      binaryStream.WriteString(this.AttributeName);
      binaryStream.WriteUInt64(this.AttributeLength);
      Util.Write(binaryStream, this.DataTypeId, this.AttributeData);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
