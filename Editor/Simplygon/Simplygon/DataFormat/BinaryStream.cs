﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.BinaryStream
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.Hash;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Simplygon.DataFormat
{
    internal class BinaryStream
    {
        private CRC32 crc32 = new CRC32();
        private BinaryReader binaryReader;
        private BinaryWriter binaryWriter;

        public uint CRC
        {
            get
            {
                return this.crc32.CRCValue;
            }
        }

        public bool ComputeCRC
        {
            get; set;
        }

        public BinaryStream(BinaryReader binaryReader)
        {
            this.binaryReader = binaryReader;
        }

        public BinaryStream(BinaryWriter binaryWriter)
        {
            this.binaryWriter = binaryWriter;
        }

        private object Read(Type type)
        {
            switch (type.Name)
            {
                case "Boolean":
                    return (object)this.ReadBool();
                case "Byte":
                    return (object)this.ReadUInt8();
                case "Double":
                    return (object)this.ReadDouble();
                case "Int16":
                    return (object)this.ReadInt16();
                case "Int32":
                    return (object)this.ReadInt32();
                case "Int64":
                    return (object)this.ReadInt64();
                case "SByte":
                    return (object)this.ReadInt8();
                case "Single":
                    return (object)this.ReadFloat();
                case "UInt16":
                    return (object)this.ReadUInt16();
                case "UInt32":
                    return (object)this.ReadUInt32();
                case "UInt64":
                    return (object)this.ReadUInt64();
                default:
                    return (object)null;
            }
        }

        public byte[] ReadBytes(int size)
        {
            byte[] buffer = this.binaryReader.ReadBytes(size);
            if (!BitConverter.IsLittleEndian)
                buffer = ((IEnumerable<byte>)buffer).Reverse<byte>().ToArray<byte>();
            if (this.ComputeCRC)
                this.crc32.Compute(buffer, 0, buffer.Length);
            return buffer;
        }

        public bool ReadBool()
        {
            return (int)this.ReadUInt8() > 0;
        }

        public sbyte ReadInt8()
        {
            sbyte num = this.binaryReader.ReadSByte();
            if (this.ComputeCRC)
                this.crc32.Compute(new byte[1] { (byte)num }, 0, 1);
            return num;
        }

        public byte ReadUInt8()
        {
            byte num = this.binaryReader.ReadByte();
            if (this.ComputeCRC)
                this.crc32.Compute(new byte[1] { num }, 0, 1);
            return num;
        }

        public short ReadInt16()
        {
            return BitConverter.ToInt16(this.ReadBytes(2), 0);
        }

        public ushort ReadUInt16()
        {
            return BitConverter.ToUInt16(this.ReadBytes(2), 0);
        }

        public int ReadInt32()
        {
            return BitConverter.ToInt32(this.ReadBytes(4), 0);
        }

        public uint ReadUInt32()
        {
            return BitConverter.ToUInt32(this.ReadBytes(4), 0);
        }

        public long ReadInt64()
        {
            return BitConverter.ToInt64(this.ReadBytes(8), 0);
        }

        public ulong ReadUInt64()
        {
            return BitConverter.ToUInt64(this.ReadBytes(8), 0);
        }

        public double ReadFloat()
        {
            return (double)BitConverter.ToSingle(this.ReadBytes(4), 0);
        }

        public double ReadDouble()
        {
            return BitConverter.ToDouble(this.ReadBytes(8), 0);
        }

        public string ReadString()
        {
            uint num = this.ReadUInt32();
            if (num > 0U)
                return Encoding.UTF8.GetString(this.ReadBytes((int)num));
            return (string)null;
        }

        public byte[] ReadBlob()
        {
            uint num = this.ReadUInt32();
            if (num > 0U)
                return this.ReadBytes((int)num);
            return (byte[])null;
        }

        public T[] ReadList<T>()
        {
            uint num = this.ReadUInt32();
            if (num > 0U)
            {
                T[] objArray = new T[(int)num];
                for (int index = 0; (long)index < (long)num; ++index)
                    objArray[index] = (T)this.Read(typeof(T));
            }
            return (T[])null;
        }

        private void Write(object value)
        {
            string name = value.GetType().Name;
            switch (name)
            {
                case "SByte":
                    this.WriteInt8((sbyte)value);
                    break;
                case "Int64":
                    this.WriteInt64((long)value);
                    break;
                case "Int16":
                    this.WriteInt16((short)value);
                    break;
                case "UInt16":
                    this.WriteUInt16((ushort)value);
                    break;
                case "UInt64":
                    this.WriteUInt64((ulong)value);
                    break;
                case "Double":
                    this.WriteDouble((double)value);
                    break;
                case "Int32":
                    this.WriteInt32((int)value);
                    break;
                case "Byte":
                    this.WriteUInt8((byte)value);
                    break;
                case "UInt32":
                    this.WriteUInt32((uint)value);
                    break;
                case "Boolean":
                    this.WriteBool((bool)value);
                    break;
                case "Single":
                    this.WriteFloat((float)value);
                    break;
            }
        }

        public void WriteBytes(byte[] value)
        {
            if (!BitConverter.IsLittleEndian)
            {
                byte[] numArray = new byte[value.Length];
                Array.Copy((Array)value, (Array)numArray, value.Length);
                byte[] array = ((IEnumerable<byte>)numArray).Reverse<byte>().ToArray<byte>();
                if (this.ComputeCRC)
                    this.crc32.Compute(value, 0, value.Length);
                this.binaryWriter.Write(array);
            }
            else
            {
                if (this.ComputeCRC)
                    this.crc32.Compute(value, 0, value.Length);
                this.binaryWriter.Write(value);
            }
        }

        public void WriteBool(bool value)
        {
            this.WriteUInt8(!value ? (byte)0 : (byte)1);
        }

        public void WriteInt8(sbyte value)
        {
            this.WriteBytes(new byte[1] { (byte)value });
        }

        public void WriteUInt8(byte value)
        {
            this.WriteBytes(new byte[1] { value });
        }

        public void WriteInt16(short value)
        {
            this.WriteBytes(BitConverter.GetBytes(value));
        }

        public void WriteUInt16(ushort value)
        {
            this.WriteBytes(BitConverter.GetBytes(value));
        }

        public void WriteInt32(int value)
        {
            this.WriteBytes(BitConverter.GetBytes(value));
        }

        public void WriteUInt32(uint value)
        {
            this.WriteBytes(BitConverter.GetBytes(value));
        }

        public void WriteInt64(long value)
        {
            this.WriteBytes(BitConverter.GetBytes(value));
        }

        public void WriteUInt64(ulong value)
        {
            this.WriteBytes(BitConverter.GetBytes(value));
        }

        public void WriteFloat(float value)
        {
            this.WriteBytes(BitConverter.GetBytes(value));
        }

        public void WriteDouble(double value)
        {
            this.WriteBytes(BitConverter.GetBytes(value));
        }

        public void WriteString(string value)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(value);
            this.WriteUInt32((uint)bytes.Length);
            this.WriteBytes(bytes);
        }

        public void WriteBlob(byte[] value)
        {
            this.WriteUInt32((uint)value.Length);
            this.WriteBytes(value);
        }

        public void WriteList<T>(T[] list)
        {
            uint length = (uint)list.Length;
            this.WriteUInt32(length);
            if (length <= 0U)
                return;
            for (int index = 0; (long)index < (long)length; ++index)
                this.Write((object)list[index]);
        }

        public void Seek(long offset, SeekOrigin origin)
        {
            if (this.binaryReader != null)
            {
                this.binaryReader.BaseStream.Seek(offset, origin);
            }
            else
            {
                if (this.binaryWriter == null)
                    return;
                this.binaryWriter.BaseStream.Seek(offset, origin);
            }
        }
    }
}
