﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.DataType.TupleList`1
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.DataFormat.v20.DataType
{
  internal class TupleList<T> : List<T>
  {
    public int TupleSize { get; protected set; }

    protected TupleList()
    {
    }

    public TupleList(DataTypeId dataTypeId, int tupleSize, int itemCount)
    {
      this.DataTypeId = dataTypeId;
      this.TupleSize = tupleSize;
      this.Items = new T[itemCount * tupleSize];
    }

    public TupleList(DataTypeId dataTypeId, int tupleSize, T[] items)
    {
      this.DataTypeId = dataTypeId;
      this.TupleSize = tupleSize;
      this.Items = items;
    }

    public override ulong GetTotalSizeInBytes()
    {
      ulong num = 0;
      for (int index = 0; index < this.Items.Length; ++index)
        num += Util.GetSizeInBytes(this.DataTypeId, (object) this.Items[index]);
      return 16UL + num;
    }

    public override void Read(BinaryStream binaryStream)
    {
      this.DataTypeId = (DataTypeId) binaryStream.ReadUInt64();
      int length = (int) binaryStream.ReadUInt32();
      this.TupleSize = (int) binaryStream.ReadUInt32();
      this.Items = new T[length];
      for (int index = 0; index < this.Items.Length; ++index)
      {
        T obj = (T) Util.Read(binaryStream, this.DataTypeId);
        this.Items[index] = obj;
      }
    }

    public static TupleList<T> ReadList<T>(BinaryStream binaryStream)
    {
      TupleList<T> tupleList = new TupleList<T>();
      tupleList.Read(binaryStream);
      return tupleList;
    }

    public override void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteUInt64((ulong) this.DataTypeId);
      binaryStream.WriteUInt32((uint) this.Items.Length);
      binaryStream.WriteUInt32((uint) this.TupleSize);
      for (int index = 0; index < this.Items.Length; ++index)
        Util.Write(binaryStream, this.DataTypeId, (object) this.Items[index]);
    }

    public override void Update()
    {
    }

    public override void Validate()
    {
    }

    public override void Release()
    {
    }
  }
}
