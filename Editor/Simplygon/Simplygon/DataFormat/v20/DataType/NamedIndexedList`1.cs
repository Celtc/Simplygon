﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.DataType.NamedIndexedList`1
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.Interfaces;

namespace Simplygon.DataFormat.v20.DataType
{
  internal class NamedIndexedList<T> : IndexedList<T>, INamedList
  {
    public string Name { get; private set; }

    protected NamedIndexedList()
    {
      this.Name = "Unknown";
    }

    public NamedIndexedList(string name, DataTypeId dataTypeId, int itemCount, int indexCount)
      : base(dataTypeId, itemCount, indexCount)
    {
      this.Name = "Unknown";
      if (string.IsNullOrEmpty(name))
        return;
      this.Name = name;
    }

    public NamedIndexedList(string name, DataTypeId dataTypeId, T[] items, int[] indices)
      : base(dataTypeId, items, indices)
    {
      this.Name = "Unknown";
      if (string.IsNullOrEmpty(name))
        return;
      this.Name = name;
    }

    public override ulong GetTotalSizeInBytes()
    {
      return Util.GetStringSizeInBytes(this.Name) + base.GetTotalSizeInBytes();
    }

    public override void Read(BinaryStream binaryStream)
    {
      this.Name = binaryStream.ReadString();
      base.Read(binaryStream);
    }

    public static NamedIndexedList<T> ReadList<T>(BinaryStream binaryStream)
    {
      NamedIndexedList<T> namedIndexedList = new NamedIndexedList<T>();
      namedIndexedList.Read(binaryStream);
      return namedIndexedList;
    }

    public override void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteString(this.Name);
      base.Write(binaryStream);
    }

    public override void Update()
    {
    }

    public override void Validate()
    {
    }

    public override void Release()
    {
    }
  }
}
