﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.DataType.Util
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.IO;
using System.Text;
using System.Globalization;

namespace Simplygon.DataFormat.v20.DataType
{
  internal class Util
  {
    public static object Read(BinaryStream binaryStream, DataTypeId dataTypeId)
    {
      switch (dataTypeId)
      {
        case DataTypeId.Custom:
          throw new Exception("Custom data type not supported yet");
        case DataTypeId.Bool:
          return (object) binaryStream.ReadBool();
        case DataTypeId.Int8:
          return (object) binaryStream.ReadInt8();
        case DataTypeId.UInt8:
          return (object) binaryStream.ReadUInt8();
        case DataTypeId.Int16:
          return (object) binaryStream.ReadInt16();
        case DataTypeId.UInt16:
          return (object) binaryStream.ReadUInt16();
        case DataTypeId.Int32:
          return (object) binaryStream.ReadInt32();
        case DataTypeId.UInt32:
          return (object) binaryStream.ReadUInt32();
        case DataTypeId.Int64:
          return (object) binaryStream.ReadInt64();
        case DataTypeId.UInt64:
          return (object) binaryStream.ReadUInt64();
        case DataTypeId.Float:
          return (object) binaryStream.ReadFloat();
        case DataTypeId.Double:
          return (object) binaryStream.ReadDouble();
        case DataTypeId.String:
          return (object) binaryStream.ReadString();
        case DataTypeId.Blob:
          return (object) binaryStream.ReadBlob();
        case DataTypeId.List:
          return Util.ReadList(binaryStream);
        case DataTypeId.TupleList:
          return Util.ReadTupleList(binaryStream);
        case DataTypeId.NamedList:
          return Util.ReadNamedList(binaryStream);
        case DataTypeId.NamedTupleList:
          return Util.ReadNamedTupleList(binaryStream);
        case DataTypeId.IndexedList:
          return Util.ReadIndexedList(binaryStream);
        case DataTypeId.NamedIndexedList:
          return Util.ReadNamedIndexedList(binaryStream);
        case DataTypeId.NamedIdList:
          return Util.ReadNamedIdList(binaryStream);
        case DataTypeId.Datetime:
          return (object) new DateTime((long) binaryStream.ReadUInt64());
        case DataTypeId.Vector2:
          Vector2 vector2 = new Vector2();
          vector2.Read(binaryStream);
          return (object) vector2;
        case DataTypeId.Vector3:
          Vector3 vector3 = new Vector3();
          vector3.Read(binaryStream);
          return (object) vector3;
        case DataTypeId.Vector4:
          Vector4 zero = Vector4.Zero;
          zero.Read(binaryStream);
          return (object) zero;
        case DataTypeId.Polygon:
          Polygon polygon = new Polygon();
          polygon.Read(binaryStream);
          return (object) polygon;
        case DataTypeId.Corner:
          Corner corner = new Corner();
          corner.Read(binaryStream);
          return (object) corner;
        case DataTypeId.Quaternion:
          Quaternion quaternion = new Quaternion();
          quaternion.Read(binaryStream);
          return (object) quaternion;
        case DataTypeId.Matrix3x3:
          Matrix3x3 matrix3x3 = new Matrix3x3();
          matrix3x3.Read(binaryStream);
          return (object) matrix3x3;
        case DataTypeId.Matrix4x4:
          Matrix4x4 matrix4x4 = new Matrix4x4();
          matrix4x4.Read(binaryStream);
          return (object) matrix4x4;
        case DataTypeId.Plane:
          Plane plane = new Plane();
          plane.Read(binaryStream);
          return (object) plane;
        case DataTypeId.Index2:
          Index2 index2 = new Index2();
          index2.Read(binaryStream);
          return (object) index2;
        case DataTypeId.Index3:
          Index3 index3 = new Index3();
          index3.Read(binaryStream);
          return (object) index3;
        case DataTypeId.Index4:
          Index4 index4 = new Index4();
          index4.Read(binaryStream);
          return (object) index4;
        default:
          return (object) null;
      }
    }

    public static void Write(BinaryStream binaryStream, DataTypeId dataTypeId, object value)
    {
      switch (dataTypeId)
      {
        case DataTypeId.Bool:
          binaryStream.WriteBool((bool) value);
          break;
        case DataTypeId.Int8:
          binaryStream.WriteInt8((sbyte) value);
          break;
        case DataTypeId.UInt8:
          binaryStream.WriteUInt8((byte) value);
          break;
        case DataTypeId.Int16:
          binaryStream.WriteInt16((short) value);
          break;
        case DataTypeId.UInt16:
          binaryStream.WriteUInt16((ushort) value);
          break;
        case DataTypeId.Int32:
          binaryStream.WriteInt32((int) value);
          break;
        case DataTypeId.UInt32:
          binaryStream.WriteUInt32((uint) value);
          break;
        case DataTypeId.Int64:
          binaryStream.WriteInt64((long) value);
          break;
        case DataTypeId.UInt64:
          binaryStream.WriteUInt64((ulong) value);
          break;
        case DataTypeId.Float:
          binaryStream.WriteFloat((float) value);
          break;
        case DataTypeId.Double:
          binaryStream.WriteDouble((double) value);
          break;
        case DataTypeId.String:
          binaryStream.WriteString((string) value);
          break;
        case DataTypeId.Blob:
          binaryStream.WriteBlob((byte[]) value);
          break;
        case DataTypeId.List:
          throw new NotImplementedException();
        case DataTypeId.TupleList:
          throw new NotImplementedException();
        case DataTypeId.NamedList:
          throw new NotImplementedException();
        case DataTypeId.NamedTupleList:
          throw new NotImplementedException();
        case DataTypeId.IndexedList:
          throw new NotImplementedException();
        case DataTypeId.NamedIndexedList:
          throw new NotImplementedException();
        case DataTypeId.NamedIdList:
          throw new NotImplementedException();
        case DataTypeId.Datetime:
          binaryStream.WriteUInt64((ulong) ((DateTime) value).Ticks);
          break;
        case DataTypeId.Vector2:
          ((Vector2) value).Write(binaryStream);
          break;
        case DataTypeId.Vector3:
          ((Vector3) value).Write(binaryStream);
          break;
        case DataTypeId.Vector4:
          ((Vector4) value).Write(binaryStream);
          break;
        case DataTypeId.Polygon:
          ((Polygon) value).Write(binaryStream);
          break;
        case DataTypeId.Corner:
          ((Corner) value).Write(binaryStream);
          break;
        case DataTypeId.Quaternion:
          ((Quaternion) value).Write(binaryStream);
          break;
        case DataTypeId.Matrix3x3:
          ((Matrix3x3) value).Write(binaryStream);
          break;
        case DataTypeId.Matrix4x4:
          ((Matrix4x4) value).Write(binaryStream);
          break;
        case DataTypeId.Plane:
          ((Plane) value).Write(binaryStream);
          break;
        case DataTypeId.Index2:
          ((Index2) value).Write(binaryStream);
          break;
        case DataTypeId.Index3:
          ((Index3) value).Write(binaryStream);
          break;
        case DataTypeId.Index4:
          ((Index4) value).Write(binaryStream);
          break;
      }
    }

    public static void WriteList(BinaryStream binaryStream, DataTypeId dataTypeId, object value)
    {
      switch (dataTypeId)
      {
        case DataTypeId.Custom:
          throw new NotImplementedException();
        case DataTypeId.Bool:
          ((List<bool>) value).Write(binaryStream);
          break;
        case DataTypeId.Int8:
          ((List<sbyte>) value).Write(binaryStream);
          break;
        case DataTypeId.UInt8:
          ((List<byte>) value).Write(binaryStream);
          break;
        case DataTypeId.Int16:
          ((List<short>) value).Write(binaryStream);
          break;
        case DataTypeId.UInt16:
          ((List<ushort>) value).Write(binaryStream);
          break;
        case DataTypeId.Int32:
          ((List<int>) value).Write(binaryStream);
          break;
        case DataTypeId.UInt32:
          ((List<uint>) value).Write(binaryStream);
          break;
        case DataTypeId.Int64:
          ((List<long>) value).Write(binaryStream);
          break;
        case DataTypeId.UInt64:
          ((List<ulong>) value).Write(binaryStream);
          break;
        case DataTypeId.Float:
          ((List<float>) value).Write(binaryStream);
          break;
        case DataTypeId.Double:
          ((List<double>) value).Write(binaryStream);
          break;
        case DataTypeId.String:
          ((List<string>) value).Write(binaryStream);
          break;
        case DataTypeId.Blob:
          ((List<byte[]>) value).Write(binaryStream);
          break;
        case DataTypeId.List:
          throw new NotImplementedException();
        case DataTypeId.TupleList:
          throw new NotImplementedException();
        case DataTypeId.NamedList:
          throw new NotImplementedException();
        case DataTypeId.NamedTupleList:
          throw new NotImplementedException();
        case DataTypeId.IndexedList:
          throw new NotImplementedException();
        case DataTypeId.NamedIndexedList:
          throw new NotImplementedException();
        case DataTypeId.Datetime:
          ((List<DateTime>) value).Write(binaryStream);
          break;
        case DataTypeId.Vector2:
          ((List<Vector2>) value).Write(binaryStream);
          break;
        case DataTypeId.Vector3:
          ((List<Vector3>) value).Write(binaryStream);
          break;
        case DataTypeId.Vector4:
          ((List<Vector4>) value).Write(binaryStream);
          break;
        case DataTypeId.Polygon:
          ((List<Polygon>) value).Write(binaryStream);
          break;
        case DataTypeId.Corner:
          ((List<Corner>) value).Write(binaryStream);
          break;
        case DataTypeId.Quaternion:
          ((List<Quaternion>) value).Write(binaryStream);
          break;
        case DataTypeId.Matrix3x3:
          ((List<Matrix3x3>) value).Write(binaryStream);
          break;
        case DataTypeId.Matrix4x4:
          ((List<Matrix4x4>) value).Write(binaryStream);
          break;
        case DataTypeId.Plane:
          ((List<Plane>) value).Write(binaryStream);
          break;
        case DataTypeId.Index2:
          ((List<Index2>) value).Write(binaryStream);
          break;
        case DataTypeId.Index3:
          ((List<Index3>) value).Write(binaryStream);
          break;
        case DataTypeId.Index4:
          ((List<Index4>) value).Write(binaryStream);
          break;
      }
    }

    public static ulong GetSizeInBytes(DataTypeId dataTypeId, object value)
    {
      switch (dataTypeId)
      {
        case DataTypeId.Custom:
          return 0;
        case DataTypeId.Bool:
          return 1;
        case DataTypeId.Int8:
          return 1;
        case DataTypeId.UInt8:
          return 1;
        case DataTypeId.Int16:
          return 2;
        case DataTypeId.UInt16:
          return 2;
        case DataTypeId.Int32:
          return 4;
        case DataTypeId.UInt32:
          return 4;
        case DataTypeId.Int64:
          return 8;
        case DataTypeId.UInt64:
          return 8;
        case DataTypeId.Float:
          return 4;
        case DataTypeId.Double:
          return 8;
        case DataTypeId.String:
          return Util.GetStringSizeInBytes((string) value);
        case DataTypeId.Blob:
          return (ulong) ((byte[]) value).Length;
        case DataTypeId.List:
          throw new NotImplementedException();
        case DataTypeId.TupleList:
          throw new NotImplementedException();
        case DataTypeId.NamedList:
          throw new NotImplementedException();
        case DataTypeId.NamedTupleList:
          throw new NotImplementedException();
        case DataTypeId.IndexedList:
          throw new NotImplementedException();
        case DataTypeId.NamedIndexedList:
          throw new NotImplementedException();
        case DataTypeId.NamedIdList:
          throw new NotImplementedException();
        case DataTypeId.Datetime:
          return 8;
        case DataTypeId.Vector2:
          return ((Vector2) value).GetTotalSizeInBytes();
        case DataTypeId.Vector3:
          return ((Vector3) value).GetTotalSizeInBytes();
        case DataTypeId.Vector4:
          return ((Vector4) value).GetTotalSizeInBytes();
        case DataTypeId.Polygon:
          return ((Polygon) value).GetTotalSizeInBytes();
        case DataTypeId.Corner:
          return ((Corner) value).GetTotalSizeInBytes();
        case DataTypeId.Quaternion:
          return ((Quaternion) value).GetTotalSizeInBytes();
        case DataTypeId.Matrix3x3:
          return ((Matrix3x3) value).GetTotalSizeInBytes();
        case DataTypeId.Matrix4x4:
          return ((Matrix4x4) value).GetTotalSizeInBytes();
        case DataTypeId.Plane:
          return ((Plane) value).GetTotalSizeInBytes();
        case DataTypeId.Index2:
          return ((Index2) value).GetTotalSizeInBytes();
        case DataTypeId.Index3:
          return ((Index3) value).GetTotalSizeInBytes();
        case DataTypeId.Index4:
          return ((Index4) value).GetTotalSizeInBytes();
        default:
          throw new Exception("GetSizeInBytes: unrecognized DataTypeId");
      }
    }

    public static ulong GetSizeInBytesForList(DataTypeId dataTypeId, object value)
    {
      switch (dataTypeId)
      {
        case DataTypeId.Custom:
          throw new NotImplementedException();
        case DataTypeId.Bool:
          return ((List<bool>) value).GetTotalSizeInBytes();
        case DataTypeId.Int8:
          return ((List<sbyte>) value).GetTotalSizeInBytes();
        case DataTypeId.UInt8:
          return ((List<byte>) value).GetTotalSizeInBytes();
        case DataTypeId.Int16:
          return ((List<short>) value).GetTotalSizeInBytes();
        case DataTypeId.UInt16:
          return ((List<ushort>) value).GetTotalSizeInBytes();
        case DataTypeId.Int32:
          return ((List<int>) value).GetTotalSizeInBytes();
        case DataTypeId.UInt32:
          return ((List<uint>) value).GetTotalSizeInBytes();
        case DataTypeId.Int64:
          return ((List<long>) value).GetTotalSizeInBytes();
        case DataTypeId.UInt64:
          return ((List<ulong>) value).GetTotalSizeInBytes();
        case DataTypeId.Float:
          return ((List<float>) value).GetTotalSizeInBytes();
        case DataTypeId.Double:
          return ((List<double>) value).GetTotalSizeInBytes();
        case DataTypeId.String:
          return ((List<string>) value).GetTotalSizeInBytes();
        case DataTypeId.Blob:
          return ((List<byte[]>) value).GetTotalSizeInBytes();
        case DataTypeId.List:
          throw new NotImplementedException();
        case DataTypeId.TupleList:
          throw new NotImplementedException();
        case DataTypeId.NamedList:
          throw new NotImplementedException();
        case DataTypeId.NamedTupleList:
          throw new NotImplementedException();
        case DataTypeId.IndexedList:
          throw new NotImplementedException();
        case DataTypeId.NamedIndexedList:
          throw new NotImplementedException();
        case DataTypeId.Datetime:
          return ((List<DateTime>) value).GetTotalSizeInBytes();
        case DataTypeId.Vector2:
          return ((List<Vector2>) value).GetTotalSizeInBytes();
        case DataTypeId.Vector3:
          return ((List<Vector3>) value).GetTotalSizeInBytes();
        case DataTypeId.Vector4:
          return ((List<Vector4>) value).GetTotalSizeInBytes();
        case DataTypeId.Polygon:
          return ((List<Polygon>) value).GetTotalSizeInBytes();
        case DataTypeId.Corner:
          return ((List<Corner>) value).GetTotalSizeInBytes();
        case DataTypeId.Quaternion:
          return ((List<Quaternion>) value).GetTotalSizeInBytes();
        case DataTypeId.Matrix3x3:
          return ((List<Matrix3x3>) value).GetTotalSizeInBytes();
        case DataTypeId.Matrix4x4:
          return ((List<Matrix4x4>) value).GetTotalSizeInBytes();
        case DataTypeId.Plane:
          return ((List<Plane>) value).GetTotalSizeInBytes();
        case DataTypeId.Index2:
          return ((List<Index2>) value).GetTotalSizeInBytes();
        case DataTypeId.Index3:
          return ((List<Index3>) value).GetTotalSizeInBytes();
        case DataTypeId.Index4:
          return ((List<Index4>) value).GetTotalSizeInBytes();
        default:
          throw new Exception("GetSizeInBytesForList: unrecognized DataTypeId");
      }
    }

    public static ulong GetStringSizeInBytes(string value)
    {
      return (ulong) (4 + Encoding.UTF8.GetBytes(value).Length);
    }

    public static object ReadList(BinaryStream binaryStream)
    {
      DataTypeId dataTypeId = (DataTypeId) binaryStream.ReadUInt64();
      binaryStream.Seek(-(long) Util.GetSizeInBytes(DataTypeId.UInt64, (object) 0), SeekOrigin.Current);
      switch (dataTypeId)
      {
        case DataTypeId.Custom:
          throw new NotImplementedException();
        case DataTypeId.Bool:
          return (object) List<bool>.ReadList<bool>(binaryStream);
        case DataTypeId.Int8:
          return (object) List<sbyte>.ReadList<sbyte>(binaryStream);
        case DataTypeId.UInt8:
          return (object) List<byte>.ReadList<byte>(binaryStream);
        case DataTypeId.Int16:
          return (object) List<short>.ReadList<short>(binaryStream);
        case DataTypeId.UInt16:
          return (object) List<ushort>.ReadList<ushort>(binaryStream);
        case DataTypeId.Int32:
          return (object) List<int>.ReadList<int>(binaryStream);
        case DataTypeId.UInt32:
          return (object) List<uint>.ReadList<uint>(binaryStream);
        case DataTypeId.Int64:
          return (object) List<long>.ReadList<long>(binaryStream);
        case DataTypeId.UInt64:
          return (object) List<ulong>.ReadList<ulong>(binaryStream);
        case DataTypeId.Float:
          return (object) List<float>.ReadList<float>(binaryStream);
        case DataTypeId.Double:
          return (object) List<double>.ReadList<double>(binaryStream);
        case DataTypeId.String:
          return (object) List<string>.ReadList<string>(binaryStream);
        case DataTypeId.Blob:
          return (object) List<byte[]>.ReadList<byte[]>(binaryStream);
        case DataTypeId.List:
          throw new NotImplementedException();
        case DataTypeId.TupleList:
          throw new NotImplementedException();
        case DataTypeId.NamedList:
          throw new NotImplementedException();
        case DataTypeId.NamedTupleList:
          throw new NotImplementedException();
        case DataTypeId.IndexedList:
          throw new NotImplementedException();
        case DataTypeId.NamedIndexedList:
          throw new NotImplementedException();
        case DataTypeId.Datetime:
          return (object) List<DateTime>.ReadList<DateTime>(binaryStream);
        case DataTypeId.Vector2:
          return (object) List<Vector2>.ReadList<Vector2>(binaryStream);
        case DataTypeId.Vector3:
          return (object) List<Vector3>.ReadList<Vector3>(binaryStream);
        case DataTypeId.Vector4:
          return (object) List<Vector4>.ReadList<Vector4>(binaryStream);
        case DataTypeId.Polygon:
          return (object) List<Polygon>.ReadList<Polygon>(binaryStream);
        case DataTypeId.Corner:
          return (object) List<Corner>.ReadList<Corner>(binaryStream);
        case DataTypeId.Quaternion:
          return (object) List<Quaternion>.ReadList<Quaternion>(binaryStream);
        case DataTypeId.Matrix3x3:
          return (object) List<Matrix3x3>.ReadList<Matrix3x3>(binaryStream);
        case DataTypeId.Matrix4x4:
          return (object) List<Matrix4x4>.ReadList<Matrix4x4>(binaryStream);
        case DataTypeId.Plane:
          return (object) List<Plane>.ReadList<Plane>(binaryStream);
        case DataTypeId.Index2:
          return (object) List<Index2>.ReadList<Index2>(binaryStream);
        case DataTypeId.Index3:
          return (object) List<Index3>.ReadList<Index3>(binaryStream);
        case DataTypeId.Index4:
          return (object) List<Index4>.ReadList<Index4>(binaryStream);
        default:
          return (object) null;
      }
    }

    public static object ReadIndexedList(BinaryStream binaryStream)
    {
      DataTypeId dataTypeId = (DataTypeId) binaryStream.ReadUInt64();
      binaryStream.Seek(-(long) Util.GetSizeInBytes(DataTypeId.UInt64, (object) 0), SeekOrigin.Current);
      switch (dataTypeId)
      {
        case DataTypeId.Custom:
          throw new NotImplementedException();
        case DataTypeId.Bool:
          return (object) List<bool>.ReadList<bool>(binaryStream);
        case DataTypeId.Int8:
          return (object) List<sbyte>.ReadList<sbyte>(binaryStream);
        case DataTypeId.UInt8:
          return (object) List<byte>.ReadList<byte>(binaryStream);
        case DataTypeId.Int16:
          return (object) List<short>.ReadList<short>(binaryStream);
        case DataTypeId.UInt16:
          return (object) List<ushort>.ReadList<ushort>(binaryStream);
        case DataTypeId.Int32:
          return (object) List<int>.ReadList<int>(binaryStream);
        case DataTypeId.UInt32:
          return (object) List<uint>.ReadList<uint>(binaryStream);
        case DataTypeId.Int64:
          return (object) List<long>.ReadList<long>(binaryStream);
        case DataTypeId.UInt64:
          return (object) List<ulong>.ReadList<ulong>(binaryStream);
        case DataTypeId.Float:
          return (object) List<float>.ReadList<float>(binaryStream);
        case DataTypeId.Double:
          return (object) List<double>.ReadList<double>(binaryStream);
        case DataTypeId.String:
          return (object) List<string>.ReadList<string>(binaryStream);
        case DataTypeId.Blob:
          return (object) List<byte[]>.ReadList<byte[]>(binaryStream);
        case DataTypeId.List:
          throw new NotImplementedException();
        case DataTypeId.TupleList:
          throw new NotImplementedException();
        case DataTypeId.NamedList:
          throw new NotImplementedException();
        case DataTypeId.NamedTupleList:
          throw new NotImplementedException();
        case DataTypeId.IndexedList:
          throw new NotImplementedException();
        case DataTypeId.NamedIndexedList:
          throw new NotImplementedException();
        case DataTypeId.Datetime:
          return (object) List<DateTime>.ReadList<DateTime>(binaryStream);
        case DataTypeId.Vector2:
          return (object) List<Vector2>.ReadList<Vector2>(binaryStream);
        case DataTypeId.Vector3:
          return (object) List<Vector3>.ReadList<Vector3>(binaryStream);
        case DataTypeId.Vector4:
          return (object) List<Vector4>.ReadList<Vector4>(binaryStream);
        case DataTypeId.Polygon:
          return (object) List<Polygon>.ReadList<Polygon>(binaryStream);
        case DataTypeId.Corner:
          return (object) List<Corner>.ReadList<Corner>(binaryStream);
        case DataTypeId.Quaternion:
          return (object) List<Quaternion>.ReadList<Quaternion>(binaryStream);
        case DataTypeId.Matrix3x3:
          return (object) List<Matrix3x3>.ReadList<Matrix3x3>(binaryStream);
        case DataTypeId.Matrix4x4:
          return (object) List<Matrix4x4>.ReadList<Matrix4x4>(binaryStream);
        case DataTypeId.Plane:
          return (object) List<Plane>.ReadList<Plane>(binaryStream);
        case DataTypeId.Index2:
          return (object) List<Index2>.ReadList<Index2>(binaryStream);
        case DataTypeId.Index3:
          return (object) List<Index3>.ReadList<Index3>(binaryStream);
        case DataTypeId.Index4:
          return (object) List<Index4>.ReadList<Index4>(binaryStream);
        default:
          return (object) null;
      }
    }

    public static object ReadNamedList(BinaryStream binaryStream)
    {
      string str = binaryStream.ReadString();
      DataTypeId dataTypeId = (DataTypeId) binaryStream.ReadUInt64();
      binaryStream.Seek(-(long) Util.GetSizeInBytes(DataTypeId.UInt64, (object) 0), SeekOrigin.Current);
      binaryStream.Seek(-(long) Util.GetSizeInBytes(DataTypeId.String, (object) str), SeekOrigin.Current);
      switch (dataTypeId)
      {
        case DataTypeId.Custom:
          throw new NotImplementedException();
        case DataTypeId.Bool:
          return (object) NamedList<bool>.ReadList<bool>(binaryStream);
        case DataTypeId.Int8:
          return (object) NamedList<sbyte>.ReadList<sbyte>(binaryStream);
        case DataTypeId.UInt8:
          return (object) NamedList<byte>.ReadList<byte>(binaryStream);
        case DataTypeId.Int16:
          return (object) NamedList<short>.ReadList<short>(binaryStream);
        case DataTypeId.UInt16:
          return (object) NamedList<ushort>.ReadList<ushort>(binaryStream);
        case DataTypeId.Int32:
          return (object) NamedList<int>.ReadList<int>(binaryStream);
        case DataTypeId.UInt32:
          return (object) NamedList<uint>.ReadList<uint>(binaryStream);
        case DataTypeId.Int64:
          return (object) NamedList<long>.ReadList<long>(binaryStream);
        case DataTypeId.UInt64:
          return (object) NamedList<ulong>.ReadList<ulong>(binaryStream);
        case DataTypeId.Float:
          return (object) NamedList<float>.ReadList<float>(binaryStream);
        case DataTypeId.Double:
          return (object) NamedList<double>.ReadList<double>(binaryStream);
        case DataTypeId.String:
          return (object) NamedList<string>.ReadList<string>(binaryStream);
        case DataTypeId.Blob:
          return (object) NamedList<byte[]>.ReadList<byte[]>(binaryStream);
        case DataTypeId.List:
          throw new NotImplementedException();
        case DataTypeId.TupleList:
          throw new NotImplementedException();
        case DataTypeId.NamedList:
          throw new NotImplementedException();
        case DataTypeId.NamedTupleList:
          throw new NotImplementedException();
        case DataTypeId.IndexedList:
          throw new NotImplementedException();
        case DataTypeId.NamedIndexedList:
          throw new NotImplementedException();
        case DataTypeId.Datetime:
          return (object) NamedList<DateTime>.ReadList<DateTime>(binaryStream);
        case DataTypeId.Vector2:
          return (object) NamedList<Vector2>.ReadList<Vector2>(binaryStream);
        case DataTypeId.Vector3:
          return (object) NamedList<Vector3>.ReadList<Vector3>(binaryStream);
        case DataTypeId.Vector4:
          return (object) NamedList<Vector4>.ReadList<Vector4>(binaryStream);
        case DataTypeId.Polygon:
          return (object) NamedList<Polygon>.ReadList<Polygon>(binaryStream);
        case DataTypeId.Corner:
          return (object) NamedList<Corner>.ReadList<Corner>(binaryStream);
        case DataTypeId.Quaternion:
          return (object) NamedList<Quaternion>.ReadList<Quaternion>(binaryStream);
        case DataTypeId.Matrix3x3:
          return (object) NamedList<Matrix3x3>.ReadList<Matrix3x3>(binaryStream);
        case DataTypeId.Matrix4x4:
          return (object) NamedList<Matrix4x4>.ReadList<Matrix4x4>(binaryStream);
        case DataTypeId.Plane:
          return (object) NamedList<Plane>.ReadList<Plane>(binaryStream);
        case DataTypeId.Index2:
          return (object) NamedList<Index2>.ReadList<Index2>(binaryStream);
        case DataTypeId.Index3:
          return (object) NamedList<Index3>.ReadList<Index3>(binaryStream);
        case DataTypeId.Index4:
          return (object) NamedList<Index4>.ReadList<Index4>(binaryStream);
        default:
          return (object) null;
      }
    }

    public static object ReadNamedIdList(BinaryStream binaryStream)
    {
      string str1 = binaryStream.ReadString();
      string str2 = binaryStream.ReadString();
      DataTypeId dataTypeId = (DataTypeId) binaryStream.ReadUInt64();
      binaryStream.Seek(-(long) Util.GetSizeInBytes(DataTypeId.UInt64, (object) 0), SeekOrigin.Current);
      binaryStream.Seek(-(long) Util.GetSizeInBytes(DataTypeId.String, (object) str1), SeekOrigin.Current);
      binaryStream.Seek(-(long) Util.GetSizeInBytes(DataTypeId.String, (object) str2), SeekOrigin.Current);
      switch (dataTypeId)
      {
        case DataTypeId.Custom:
          throw new NotImplementedException();
        case DataTypeId.Bool:
          return (object) NamedIdList<bool>.ReadList<bool>(binaryStream);
        case DataTypeId.Int8:
          return (object) NamedIdList<sbyte>.ReadList<sbyte>(binaryStream);
        case DataTypeId.UInt8:
          return (object) NamedIdList<byte>.ReadList<byte>(binaryStream);
        case DataTypeId.Int16:
          return (object) NamedIdList<short>.ReadList<short>(binaryStream);
        case DataTypeId.UInt16:
          return (object) NamedIdList<ushort>.ReadList<ushort>(binaryStream);
        case DataTypeId.Int32:
          return (object) NamedIdList<int>.ReadList<int>(binaryStream);
        case DataTypeId.UInt32:
          return (object) NamedIdList<uint>.ReadList<uint>(binaryStream);
        case DataTypeId.Int64:
          return (object) NamedIdList<long>.ReadList<long>(binaryStream);
        case DataTypeId.UInt64:
          return (object) NamedIdList<ulong>.ReadList<ulong>(binaryStream);
        case DataTypeId.Float:
          return (object) NamedIdList<float>.ReadList<float>(binaryStream);
        case DataTypeId.Double:
          return (object) NamedIdList<double>.ReadList<double>(binaryStream);
        case DataTypeId.String:
          return (object) NamedIdList<string>.ReadList<string>(binaryStream);
        case DataTypeId.Blob:
          return (object) NamedIdList<byte[]>.ReadList<byte[]>(binaryStream);
        case DataTypeId.List:
          throw new NotImplementedException();
        case DataTypeId.TupleList:
          throw new NotImplementedException();
        case DataTypeId.NamedList:
          throw new NotImplementedException();
        case DataTypeId.NamedTupleList:
          throw new NotImplementedException();
        case DataTypeId.IndexedList:
          throw new NotImplementedException();
        case DataTypeId.NamedIndexedList:
          throw new NotImplementedException();
        case DataTypeId.Datetime:
          return (object) NamedIdList<DateTime>.ReadList<DateTime>(binaryStream);
        case DataTypeId.Vector2:
          return (object) NamedIdList<Vector2>.ReadList<Vector2>(binaryStream);
        case DataTypeId.Vector3:
          return (object) NamedIdList<Vector3>.ReadList<Vector3>(binaryStream);
        case DataTypeId.Vector4:
          return (object) NamedIdList<Vector4>.ReadList<Vector4>(binaryStream);
        case DataTypeId.Polygon:
          return (object) NamedIdList<Polygon>.ReadList<Polygon>(binaryStream);
        case DataTypeId.Corner:
          return (object) NamedIdList<Corner>.ReadList<Corner>(binaryStream);
        case DataTypeId.Quaternion:
          return (object) NamedIdList<Quaternion>.ReadList<Quaternion>(binaryStream);
        case DataTypeId.Matrix3x3:
          return (object) NamedIdList<Matrix3x3>.ReadList<Matrix3x3>(binaryStream);
        case DataTypeId.Matrix4x4:
          return (object) NamedIdList<Matrix4x4>.ReadList<Matrix4x4>(binaryStream);
        case DataTypeId.Plane:
          return (object) NamedIdList<Plane>.ReadList<Plane>(binaryStream);
        case DataTypeId.Index2:
          return (object) NamedIdList<Index2>.ReadList<Index2>(binaryStream);
        case DataTypeId.Index3:
          return (object) NamedIdList<Index3>.ReadList<Index3>(binaryStream);
        case DataTypeId.Index4:
          return (object) NamedIdList<Index4>.ReadList<Index4>(binaryStream);
        default:
          return (object) null;
      }
    }

    public static object ReadNamedIndexedList(BinaryStream binaryStream)
    {
      string str = binaryStream.ReadString();
      DataTypeId dataTypeId = (DataTypeId) binaryStream.ReadUInt64();
      binaryStream.Seek(-(long) Util.GetSizeInBytes(DataTypeId.UInt64, (object) 0), SeekOrigin.Current);
      binaryStream.Seek(-(long) Util.GetSizeInBytes(DataTypeId.String, (object) str), SeekOrigin.Current);
      switch (dataTypeId)
      {
        case DataTypeId.Custom:
          throw new NotImplementedException();
        case DataTypeId.Bool:
          return (object) NamedIndexedList<bool>.ReadList<bool>(binaryStream);
        case DataTypeId.Int8:
          return (object) NamedIndexedList<sbyte>.ReadList<sbyte>(binaryStream);
        case DataTypeId.UInt8:
          return (object) NamedIndexedList<byte>.ReadList<byte>(binaryStream);
        case DataTypeId.Int16:
          return (object) NamedIndexedList<short>.ReadList<short>(binaryStream);
        case DataTypeId.UInt16:
          return (object) NamedIndexedList<ushort>.ReadList<ushort>(binaryStream);
        case DataTypeId.Int32:
          return (object) NamedIndexedList<int>.ReadList<int>(binaryStream);
        case DataTypeId.UInt32:
          return (object) NamedIndexedList<uint>.ReadList<uint>(binaryStream);
        case DataTypeId.Int64:
          return (object) NamedIndexedList<long>.ReadList<long>(binaryStream);
        case DataTypeId.UInt64:
          return (object) NamedIndexedList<ulong>.ReadList<ulong>(binaryStream);
        case DataTypeId.Float:
          return (object) NamedIndexedList<float>.ReadList<float>(binaryStream);
        case DataTypeId.Double:
          return (object) NamedIndexedList<double>.ReadList<double>(binaryStream);
        case DataTypeId.String:
          return (object) NamedIndexedList<string>.ReadList<string>(binaryStream);
        case DataTypeId.Blob:
          return (object) NamedIndexedList<byte[]>.ReadList<byte[]>(binaryStream);
        case DataTypeId.List:
          throw new NotImplementedException();
        case DataTypeId.TupleList:
          throw new NotImplementedException();
        case DataTypeId.NamedList:
          throw new NotImplementedException();
        case DataTypeId.NamedTupleList:
          throw new NotImplementedException();
        case DataTypeId.IndexedList:
          throw new NotImplementedException();
        case DataTypeId.NamedIndexedList:
          throw new NotImplementedException();
        case DataTypeId.Datetime:
          return (object) NamedIndexedList<DateTime>.ReadList<DateTime>(binaryStream);
        case DataTypeId.Vector2:
          return (object) NamedIndexedList<Vector2>.ReadList<Vector2>(binaryStream);
        case DataTypeId.Vector3:
          return (object) NamedIndexedList<Vector3>.ReadList<Vector3>(binaryStream);
        case DataTypeId.Vector4:
          return (object) NamedIndexedList<Vector4>.ReadList<Vector4>(binaryStream);
        case DataTypeId.Polygon:
          return (object) NamedIndexedList<Polygon>.ReadList<Polygon>(binaryStream);
        case DataTypeId.Corner:
          return (object) NamedIndexedList<Corner>.ReadList<Corner>(binaryStream);
        case DataTypeId.Quaternion:
          return (object) NamedIndexedList<Quaternion>.ReadList<Quaternion>(binaryStream);
        case DataTypeId.Matrix3x3:
          return (object) NamedIndexedList<Matrix3x3>.ReadList<Matrix3x3>(binaryStream);
        case DataTypeId.Matrix4x4:
          return (object) NamedIndexedList<Matrix4x4>.ReadList<Matrix4x4>(binaryStream);
        case DataTypeId.Plane:
          return (object) NamedIndexedList<Plane>.ReadList<Plane>(binaryStream);
        case DataTypeId.Index2:
          return (object) NamedIndexedList<Index2>.ReadList<Index2>(binaryStream);
        case DataTypeId.Index3:
          return (object) NamedIndexedList<Index3>.ReadList<Index3>(binaryStream);
        case DataTypeId.Index4:
          return (object) NamedIndexedList<Index4>.ReadList<Index4>(binaryStream);
        default:
          return (object) null;
      }
    }

    public static object ReadTupleList(BinaryStream binaryStream)
    {
      DataTypeId dataTypeId = (DataTypeId) binaryStream.ReadUInt64();
      binaryStream.Seek(-(long) Util.GetSizeInBytes(DataTypeId.UInt64, (object) 0), SeekOrigin.Current);
      switch (dataTypeId)
      {
        case DataTypeId.Custom:
          throw new NotImplementedException();
        case DataTypeId.Bool:
          return (object) TupleList<bool>.ReadList<bool>(binaryStream);
        case DataTypeId.Int8:
          return (object) TupleList<sbyte>.ReadList<sbyte>(binaryStream);
        case DataTypeId.UInt8:
          return (object) TupleList<byte>.ReadList<byte>(binaryStream);
        case DataTypeId.Int16:
          return (object) TupleList<short>.ReadList<short>(binaryStream);
        case DataTypeId.UInt16:
          return (object) TupleList<ushort>.ReadList<ushort>(binaryStream);
        case DataTypeId.Int32:
          return (object) TupleList<int>.ReadList<int>(binaryStream);
        case DataTypeId.UInt32:
          return (object) TupleList<uint>.ReadList<uint>(binaryStream);
        case DataTypeId.Int64:
          return (object) TupleList<long>.ReadList<long>(binaryStream);
        case DataTypeId.UInt64:
          return (object) TupleList<ulong>.ReadList<ulong>(binaryStream);
        case DataTypeId.Float:
          return (object) TupleList<float>.ReadList<float>(binaryStream);
        case DataTypeId.Double:
          return (object) TupleList<double>.ReadList<double>(binaryStream);
        case DataTypeId.String:
          return (object) TupleList<string>.ReadList<string>(binaryStream);
        case DataTypeId.Blob:
          return (object) TupleList<byte[]>.ReadList<byte[]>(binaryStream);
        case DataTypeId.List:
          throw new NotImplementedException();
        case DataTypeId.TupleList:
          throw new NotImplementedException();
        case DataTypeId.NamedList:
          throw new NotImplementedException();
        case DataTypeId.NamedTupleList:
          throw new NotImplementedException();
        case DataTypeId.IndexedList:
          throw new NotImplementedException();
        case DataTypeId.NamedIndexedList:
          throw new NotImplementedException();
        case DataTypeId.Datetime:
          return (object) TupleList<DateTime>.ReadList<DateTime>(binaryStream);
        case DataTypeId.Vector2:
          return (object) TupleList<Vector2>.ReadList<Vector2>(binaryStream);
        case DataTypeId.Vector3:
          return (object) TupleList<Vector3>.ReadList<Vector3>(binaryStream);
        case DataTypeId.Vector4:
          return (object) TupleList<Vector4>.ReadList<Vector4>(binaryStream);
        case DataTypeId.Polygon:
          return (object) TupleList<Polygon>.ReadList<Polygon>(binaryStream);
        case DataTypeId.Corner:
          return (object) TupleList<Corner>.ReadList<Corner>(binaryStream);
        case DataTypeId.Quaternion:
          return (object) TupleList<Quaternion>.ReadList<Quaternion>(binaryStream);
        case DataTypeId.Matrix3x3:
          return (object) TupleList<Matrix3x3>.ReadList<Matrix3x3>(binaryStream);
        case DataTypeId.Matrix4x4:
          return (object) TupleList<Matrix4x4>.ReadList<Matrix4x4>(binaryStream);
        case DataTypeId.Plane:
          return (object) TupleList<Plane>.ReadList<Plane>(binaryStream);
        case DataTypeId.Index2:
          return (object) TupleList<Index2>.ReadList<Index2>(binaryStream);
        case DataTypeId.Index3:
          return (object) TupleList<Index3>.ReadList<Index3>(binaryStream);
        case DataTypeId.Index4:
          return (object) TupleList<Index4>.ReadList<Index4>(binaryStream);
        default:
          return (object) null;
      }
    }

    public static object ReadNamedTupleList(BinaryStream binaryStream)
    {
      string str = binaryStream.ReadString();
      DataTypeId dataTypeId = (DataTypeId) binaryStream.ReadUInt64();
      binaryStream.Seek(-(long) Util.GetSizeInBytes(DataTypeId.UInt64, (object) 0), SeekOrigin.Current);
      binaryStream.Seek(-(long) Util.GetSizeInBytes(DataTypeId.String, (object) str), SeekOrigin.Current);
      switch (dataTypeId)
      {
        case DataTypeId.Custom:
          throw new NotImplementedException();
        case DataTypeId.Bool:
          return (object) NamedTupleList<bool>.ReadList<bool>(binaryStream);
        case DataTypeId.Int8:
          return (object) NamedTupleList<sbyte>.ReadList<sbyte>(binaryStream);
        case DataTypeId.UInt8:
          return (object) NamedTupleList<byte>.ReadList<byte>(binaryStream);
        case DataTypeId.Int16:
          return (object) NamedTupleList<short>.ReadList<short>(binaryStream);
        case DataTypeId.UInt16:
          return (object) NamedTupleList<ushort>.ReadList<ushort>(binaryStream);
        case DataTypeId.Int32:
          return (object) NamedTupleList<int>.ReadList<int>(binaryStream);
        case DataTypeId.UInt32:
          return (object) NamedTupleList<uint>.ReadList<uint>(binaryStream);
        case DataTypeId.Int64:
          return (object) NamedTupleList<long>.ReadList<long>(binaryStream);
        case DataTypeId.UInt64:
          return (object) NamedTupleList<ulong>.ReadList<ulong>(binaryStream);
        case DataTypeId.Float:
          return (object) NamedTupleList<float>.ReadList<float>(binaryStream);
        case DataTypeId.Double:
          return (object) NamedTupleList<double>.ReadList<double>(binaryStream);
        case DataTypeId.String:
          return (object) NamedTupleList<string>.ReadList<string>(binaryStream);
        case DataTypeId.Blob:
          return (object) NamedTupleList<byte[]>.ReadList<byte[]>(binaryStream);
        case DataTypeId.List:
          throw new NotImplementedException();
        case DataTypeId.TupleList:
          throw new NotImplementedException();
        case DataTypeId.NamedList:
          throw new NotImplementedException();
        case DataTypeId.NamedTupleList:
          throw new NotImplementedException();
        case DataTypeId.IndexedList:
          throw new NotImplementedException();
        case DataTypeId.NamedIndexedList:
          throw new NotImplementedException();
        case DataTypeId.Datetime:
          return (object) NamedTupleList<DateTime>.ReadList<DateTime>(binaryStream);
        case DataTypeId.Vector2:
          return (object) NamedTupleList<Vector2>.ReadList<Vector2>(binaryStream);
        case DataTypeId.Vector3:
          return (object) NamedTupleList<Vector3>.ReadList<Vector3>(binaryStream);
        case DataTypeId.Vector4:
          return (object) NamedTupleList<Vector4>.ReadList<Vector4>(binaryStream);
        case DataTypeId.Polygon:
          return (object) NamedTupleList<Polygon>.ReadList<Polygon>(binaryStream);
        case DataTypeId.Corner:
          return (object) NamedTupleList<Corner>.ReadList<Corner>(binaryStream);
        case DataTypeId.Quaternion:
          return (object) NamedTupleList<Quaternion>.ReadList<Quaternion>(binaryStream);
        case DataTypeId.Matrix3x3:
          return (object) NamedTupleList<Matrix3x3>.ReadList<Matrix3x3>(binaryStream);
        case DataTypeId.Matrix4x4:
          return (object) NamedTupleList<Matrix4x4>.ReadList<Matrix4x4>(binaryStream);
        case DataTypeId.Plane:
          return (object) NamedTupleList<Plane>.ReadList<Plane>(binaryStream);
        case DataTypeId.Index2:
          return (object) NamedTupleList<Index2>.ReadList<Index2>(binaryStream);
        case DataTypeId.Index3:
          return (object) NamedTupleList<Index3>.ReadList<Index3>(binaryStream);
        case DataTypeId.Index4:
          return (object) NamedTupleList<Index4>.ReadList<Index4>(binaryStream);
        default:
          return (object) null;
      }
    }

    public static void ValidateGuid(string propertyName, string guid, string className, bool allowNull)
    {
      if (string.IsNullOrEmpty(guid) & allowNull)
        return;
      try
      {
        Guid guid1 = new Guid(guid);
      }
      catch (Exception ex)
      {
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "SFF exception: Invalid guid {0} for {1} in {2}", (object) guid, (object) propertyName, (object) className));
      }
    }
  }
}
