﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.DataType.NamedIdList`1
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.Interfaces;

namespace Simplygon.DataFormat.v20.DataType
{
  internal class NamedIdList<T> : List<T>, INamedIdList
  {
    public string Name { get; protected set; }

    public string ID { get; protected set; }

    protected NamedIdList()
    {
      this.Name = "Unknown";
    }

    public NamedIdList(string name, string id, DataTypeId dataTypeId, int count)
      : base(dataTypeId, count)
    {
      this.Name = "Unknown";
      if (!string.IsNullOrEmpty(name))
        this.Name = name;
      this.ID = id;
    }

    public NamedIdList(string name, string id, DataTypeId dataTypeId, T[] items)
      : base(dataTypeId, items)
    {
      this.Name = "Unknown";
      if (!string.IsNullOrEmpty(name))
        this.Name = name;
      this.ID = id;
    }

    public override ulong GetTotalSizeInBytes()
    {
      return Util.GetStringSizeInBytes(this.Name) + Util.GetStringSizeInBytes(this.ID) + base.GetTotalSizeInBytes();
    }

    public override void Read(BinaryStream binaryStream)
    {
      this.Name = binaryStream.ReadString();
      this.ID = binaryStream.ReadString();
      base.Read(binaryStream);
    }

    public static NamedIdList<T> ReadList<T>(BinaryStream binaryStream)
    {
      NamedIdList<T> namedIdList = new NamedIdList<T>();
      namedIdList.Read(binaryStream);
      return namedIdList;
    }

    public override void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteString(this.Name);
      binaryStream.WriteString(this.ID);
      base.Write(binaryStream);
    }

    public override void Update()
    {
    }

    public override void Validate()
    {
    }

    public override void Release()
    {
    }
  }
}
