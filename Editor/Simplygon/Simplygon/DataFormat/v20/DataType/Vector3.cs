﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.DataType.Vector3
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.DataFormat.v20.DataType
{
  internal struct Vector3 : ISDFObject
  {
    public double X;
    public double Y;
    public double Z;

    public static Vector3 Zero
    {
      get
      {
        return new Vector3(0.0, 0.0, 0.0);
      }
    }

    public Vector3(double x, double y, double z)
    {
      this.X = x;
      this.Y = y;
      this.Z = z;
    }

    public ulong GetTotalSizeInBytes()
    {
      return 24;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.X = binaryStream.ReadDouble();
      this.Y = binaryStream.ReadDouble();
      this.Z = binaryStream.ReadDouble();
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteDouble(this.X);
      binaryStream.WriteDouble(this.Y);
      binaryStream.WriteDouble(this.Z);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }

    public static Vector3 operator +(Vector3 a, Vector3 b)
    {
      return new Vector3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
    }

    public static Vector3 operator *(Vector3 a, double v)
    {
      return new Vector3(a.X * v, a.Y * v, a.Z * v);
    }

    public static Vector3 CrossProduct(Vector3 a, Vector3 b)
    {
      return new Vector3(a.Y * b.Z - b.Y * a.Z, a.Z * b.X - b.Z * a.X, a.X * b.Y - b.X * a.Y);
    }

    public static double DotProduct(Vector3 a, Vector3 b)
    {
      return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
    }

    public Vector3 Normalize()
    {
      double num = this.Length();
      return new Vector3(this.X / num, this.Y / num, this.Z / num);
    }

    public double Length()
    {
      return System.Math.Sqrt(this.X * this.X + this.Y * this.Y + this.Z * this.Z);
    }

    public bool IsZero()
    {
      if (this.X == 0.0 && this.Y == 0.0)
        return this.Z == 0.0;
      return false;
    }
  }
}
