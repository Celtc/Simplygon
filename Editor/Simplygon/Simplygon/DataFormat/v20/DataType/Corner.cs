﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.DataType.Corner
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.DataFormat.v20.DataType
{
  internal class Corner : ISDFObject
  {
    public uint VertexIndex { get; set; }

    public Vector3 Normal { get; set; }

    public List<Vector2> TextureCoordinates { get; protected set; }

    public ulong GetTotalSizeInBytes()
    {
      return 4UL + this.Normal.GetTotalSizeInBytes() + this.TextureCoordinates.GetTotalSizeInBytes();
    }

    public void Read(BinaryStream binaryStream)
    {
      this.VertexIndex = binaryStream.ReadUInt32();
      this.Normal = new Vector3();
      this.Normal.Read(binaryStream);
      this.TextureCoordinates = List<Vector2>.ReadList<Vector2>(binaryStream);
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteUInt32(this.VertexIndex);
      this.Normal.Write(binaryStream);
      this.TextureCoordinates.Write(binaryStream);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
