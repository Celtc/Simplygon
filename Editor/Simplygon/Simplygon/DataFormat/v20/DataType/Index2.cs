﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.DataType.Index2
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.DataFormat.v20.DataType
{
  internal struct Index2 : ISDFObject
  {
    public int X;
    public int Y;

    public Index2(int x, int y)
    {
      this.X = x;
      this.Y = y;
    }

    public ulong GetTotalSizeInBytes()
    {
      return 8;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.X = binaryStream.ReadInt32();
      this.Y = binaryStream.ReadInt32();
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteInt32(this.X);
      binaryStream.WriteInt32(this.Y);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
