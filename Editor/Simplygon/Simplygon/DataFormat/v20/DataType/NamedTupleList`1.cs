﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.DataType.NamedTupleList`1
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.Interfaces;

namespace Simplygon.DataFormat.v20.DataType
{
  internal class NamedTupleList<T> : TupleList<T>, INamedList
  {
    public string Name { get; protected set; }

    protected NamedTupleList()
    {
      this.Name = "Unknown";
    }

    public NamedTupleList(string name, DataTypeId dataTypeId, int tupleSize, int itemCount)
      : base(dataTypeId, tupleSize, itemCount)
    {
      this.Name = "Unknown";
      if (string.IsNullOrEmpty(name))
        return;
      this.Name = name;
    }

    public NamedTupleList(string name, DataTypeId dataTypeId, int tupleSize, T[] items)
      : base(dataTypeId, tupleSize, items)
    {
      this.Name = "Unknown";
      if (string.IsNullOrEmpty(name))
        return;
      this.Name = name;
    }

    public override ulong GetTotalSizeInBytes()
    {
      return Util.GetStringSizeInBytes(this.Name) + base.GetTotalSizeInBytes();
    }

    public override void Read(BinaryStream binaryStream)
    {
      this.Name = binaryStream.ReadString();
      base.Read(binaryStream);
    }

    public static NamedTupleList<T> ReadList<T>(BinaryStream binaryStream)
    {
      NamedTupleList<T> namedTupleList = new NamedTupleList<T>();
      namedTupleList.Read(binaryStream);
      return namedTupleList;
    }

    public override void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteString(this.Name);
      base.Write(binaryStream);
    }

    public override void Update()
    {
    }

    public override void Validate()
    {
    }

    public override void Release()
    {
    }
  }
}
