﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.DataType.Vector4
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.DataFormat.v20.DataType
{
  internal struct Vector4 : ISDFObject
  {
    public double X;
    public double Y;
    public double Z;
    public double W;

    public static Vector4 Zero
    {
      get
      {
        return new Vector4(0.0, 0.0, 0.0, 0.0);
      }
    }

    public Vector4(float[] xyzw)
    {
      this = new Vector4();
      if (xyzw.Length != 0)
        this.X = (double) xyzw[0];
      if (xyzw.Length > 1)
        this.Y = (double) xyzw[1];
      if (xyzw.Length > 2)
        this.Z = (double) xyzw[2];
      if (xyzw.Length <= 3)
        return;
      this.W = (double) xyzw[3];
    }

    public Vector4(double x, double y, double z, double w)
    {
      this.X = x;
      this.Y = y;
      this.Z = z;
      this.W = w;
    }

    public ulong GetTotalSizeInBytes()
    {
      return 32;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.X = binaryStream.ReadDouble();
      this.Y = binaryStream.ReadDouble();
      this.Z = binaryStream.ReadDouble();
      this.W = binaryStream.ReadDouble();
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteDouble(this.X);
      binaryStream.WriteDouble(this.Y);
      binaryStream.WriteDouble(this.Z);
      binaryStream.WriteDouble(this.W);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }

    public static Vector4 operator +(Vector4 a, Vector4 b)
    {
      return new Vector4(a.X + b.X, a.Y + b.Y, a.Z + b.Z, a.W + b.W);
    }

    public Vector4 Normalize()
    {
      double num = System.Math.Sqrt(this.X * this.X + this.Y * this.Y + this.Z * this.Z + this.W * this.W);
      return new Vector4(this.X / num, this.Y / num, this.Z / num, this.W / num);
    }

    public bool IsZero()
    {
      if (this.X == 0.0 && this.Y == 0.0 && this.Z == 0.0)
        return this.W == 0.0;
      return false;
    }

    public Vector3 AsVector3()
    {
      return new Vector3(this.X, this.Y, this.Z);
    }

    public void SetValues(Array xyzw)
    {
      xyzw.CopyTo((Array) new double[4]
      {
        this.X,
        this.Y,
        this.Z,
        this.W
      }, 0);
    }
  }
}
