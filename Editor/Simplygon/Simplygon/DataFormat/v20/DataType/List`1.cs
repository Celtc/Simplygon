﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.DataType.List`1
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.Interfaces;

namespace Simplygon.DataFormat.v20.DataType
{
  internal class List<T> : ISDFObject, IDataType
  {
    public DataTypeId DataTypeId { get; protected set; }

    public T[] Items { get; protected set; }

    protected List()
    {
    }

    public List(DataTypeId dataTypeId, int count)
    {
      this.DataTypeId = dataTypeId;
      this.Items = new T[count];
    }

    public List(DataTypeId dataTypeId, T[] items)
    {
      this.DataTypeId = dataTypeId;
      this.Items = items;
    }

    public virtual ulong GetTotalSizeInBytes()
    {
      ulong num = 0;
      for (int index = 0; index < this.Items.Length; ++index)
        num += Util.GetSizeInBytes(this.DataTypeId, (object) this.Items[index]);
      return 12UL + num;
    }

    public virtual void Read(BinaryStream binaryStream)
    {
      this.DataTypeId = (DataTypeId) binaryStream.ReadUInt64();
      uint num = binaryStream.ReadUInt32();
      this.Items = new T[(int) num];
      for (uint index = 0; index < num; ++index)
      {
        T obj = (T) Util.Read(binaryStream, this.DataTypeId);
        this.Items[(int) index] = obj;
      }
    }

    public static List<T> ReadList<T>(BinaryStream binaryStream)
    {
      List<T> list = new List<T>();
      list.Read(binaryStream);
      return list;
    }

    public virtual void Write(BinaryStream binaryStream)
    {
      uint length = (uint) this.Items.Length;
      binaryStream.WriteUInt64((ulong) this.DataTypeId);
      binaryStream.WriteUInt32(length);
      for (int index = 0; index < this.Items.Length; ++index)
        Util.Write(binaryStream, this.DataTypeId, (object) this.Items[index]);
    }

    public virtual void Update()
    {
    }

    public virtual void Validate()
    {
    }

    public virtual void Release()
    {
    }
  }
}
