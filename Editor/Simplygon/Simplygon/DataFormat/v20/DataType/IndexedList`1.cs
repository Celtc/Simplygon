﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.DataType.IndexedList`1
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.DataFormat.v20.DataType
{
  internal class IndexedList<T> : List<T>
  {
    public int[] Indices { get; protected set; }

    protected IndexedList()
    {
    }

    public IndexedList(DataTypeId dataTypeId, int itemCount, int indexCount)
      : base(dataTypeId, itemCount)
    {
      this.Indices = new int[indexCount];
    }

    public IndexedList(DataTypeId dataTypeId, T[] items, int[] indices)
      : base(dataTypeId, items)
    {
      this.Indices = indices;
    }

    public override ulong GetTotalSizeInBytes()
    {
      return 4UL + Util.GetSizeInBytes(DataTypeId.Int32, (object) 0) * (ulong) this.Indices.Length + base.GetTotalSizeInBytes();
    }

    public override void Read(BinaryStream binaryStream)
    {
      base.Read(binaryStream);
      this.Indices = new int[(int) binaryStream.ReadUInt32()];
      for (int index = 0; index < this.Indices.Length; ++index)
      {
        int num = (int) Util.Read(binaryStream, DataTypeId.Int32);
        this.Indices[index] = num;
      }
    }

    public static IndexedList<T> Read<T>(BinaryStream binaryStream)
    {
      IndexedList<T> indexedList = new IndexedList<T>();
      indexedList.Read(binaryStream);
      return indexedList;
    }

    public override void Write(BinaryStream binaryStream)
    {
      base.Write(binaryStream);
      binaryStream.WriteUInt32((uint) this.Indices.Length);
      for (int index = 0; index < this.Indices.Length; ++index)
        Util.Write(binaryStream, DataTypeId.Int32, (object) this.Indices[index]);
    }

    public override void Update()
    {
    }

    public override void Validate()
    {
    }

    public override void Release()
    {
    }
  }
}
