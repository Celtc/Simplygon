﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.DataType.Plane
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.DataFormat.v20.DataType
{
  internal class Plane : ISDFObject
  {
    public Vector3 Point { get; set; }

    public Vector3 Normal { get; set; }

    public Plane()
    {
      this.Point = new Vector3();
      this.Normal = new Vector3();
    }

    public Plane(Vector3 point, Vector3 normal)
    {
      this.Point = point;
      this.Normal = normal;
    }

    public ulong GetTotalSizeInBytes()
    {
      Vector3 vector3 = this.Point;
      long totalSizeInBytes1 = (long) vector3.GetTotalSizeInBytes();
      vector3 = this.Normal;
      long totalSizeInBytes2 = (long) vector3.GetTotalSizeInBytes();
      return (ulong) (totalSizeInBytes1 + totalSizeInBytes2);
    }

    public void Read(BinaryStream binaryStream)
    {
      this.Point = new Vector3(binaryStream.ReadDouble(), binaryStream.ReadDouble(), binaryStream.ReadDouble());
      this.Normal = new Vector3(binaryStream.ReadDouble(), binaryStream.ReadDouble(), binaryStream.ReadDouble());
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Point.Write(binaryStream);
      this.Normal.Write(binaryStream);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
