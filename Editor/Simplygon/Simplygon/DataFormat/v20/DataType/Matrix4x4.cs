﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.DataType.Matrix4x4
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.Globalization;

namespace Simplygon.DataFormat.v20.DataType
{
    internal class Matrix4x4 : ISDFObject
    {
        public double _11
        {
            get; set;
        }

        public double _12
        {
            get; set;
        }

        public double _13
        {
            get; set;
        }

        public double _14
        {
            get; set;
        }

        public double _21
        {
            get; set;
        }

        public double _22
        {
            get; set;
        }

        public double _23
        {
            get; set;
        }

        public double _24
        {
            get; set;
        }

        public double _31
        {
            get; set;
        }

        public double _32
        {
            get; set;
        }

        public double _33
        {
            get; set;
        }

        public double _34
        {
            get; set;
        }

        public double _41
        {
            get; set;
        }

        public double _42
        {
            get; set;
        }

        public double _43
        {
            get; set;
        }

        public double _44
        {
            get; set;
        }

        public Matrix4x4()
        {
            this.SetIdentity();
        }

        public Matrix4x4(double _11, double _12, double _13, double _14, double _21, double _22, double _23, double _24, double _31, double _32, double _33, double _34, double _41, double _42, double _43, double _44)
        {
            this._11 = _11;
            this._12 = _12;
            this._13 = _13;
            this._14 = _14;
            this._21 = _21;
            this._22 = _22;
            this._23 = _23;
            this._24 = _24;
            this._31 = _31;
            this._32 = _32;
            this._33 = _33;
            this._34 = _34;
            this._41 = _41;
            this._42 = _42;
            this._43 = _43;
            this._44 = _44;
        }

        public ulong GetTotalSizeInBytes()
        {
            return 128;
        }

        public void Read(BinaryStream binaryStream)
        {
            this._11 = binaryStream.ReadDouble();
            this._12 = binaryStream.ReadDouble();
            this._13 = binaryStream.ReadDouble();
            this._14 = binaryStream.ReadDouble();
            this._21 = binaryStream.ReadDouble();
            this._22 = binaryStream.ReadDouble();
            this._23 = binaryStream.ReadDouble();
            this._24 = binaryStream.ReadDouble();
            this._31 = binaryStream.ReadDouble();
            this._32 = binaryStream.ReadDouble();
            this._33 = binaryStream.ReadDouble();
            this._34 = binaryStream.ReadDouble();
            this._41 = binaryStream.ReadDouble();
            this._42 = binaryStream.ReadDouble();
            this._43 = binaryStream.ReadDouble();
            this._44 = binaryStream.ReadDouble();
        }

        public void Write(BinaryStream binaryStream)
        {
            binaryStream.WriteDouble(this._11);
            binaryStream.WriteDouble(this._12);
            binaryStream.WriteDouble(this._13);
            binaryStream.WriteDouble(this._14);
            binaryStream.WriteDouble(this._21);
            binaryStream.WriteDouble(this._22);
            binaryStream.WriteDouble(this._23);
            binaryStream.WriteDouble(this._24);
            binaryStream.WriteDouble(this._31);
            binaryStream.WriteDouble(this._32);
            binaryStream.WriteDouble(this._33);
            binaryStream.WriteDouble(this._34);
            binaryStream.WriteDouble(this._41);
            binaryStream.WriteDouble(this._42);
            binaryStream.WriteDouble(this._43);
            binaryStream.WriteDouble(this._44);
        }

        public void Update()
        {
        }

        public void Validate()
        {
        }

        public void Release()
        {
        }

        public static Matrix4x4 ChangeCoordinateSystem(Matrix4x4 srcMatrix)
        {
            return new Matrix4x4(srcMatrix._11, srcMatrix._12, -srcMatrix._13, srcMatrix._14, srcMatrix._21, srcMatrix._22, -srcMatrix._23, srcMatrix._24, -srcMatrix._31, -srcMatrix._32, srcMatrix._33, -srcMatrix._34, srcMatrix._41, srcMatrix._42, -srcMatrix._43, srcMatrix._44).Transpose();
        }

        public static Matrix4x4 Identity()
        {
            Matrix4x4 matrix4x4 = new Matrix4x4();
            matrix4x4.SetIdentity();
            return matrix4x4;
        }

        public void SetIdentity()
        {
            this._11 = 1.0;
            this._12 = 0.0;
            this._13 = 0.0;
            this._14 = 0.0;
            this._21 = 0.0;
            this._22 = 1.0;
            this._23 = 0.0;
            this._24 = 0.0;
            this._31 = 0.0;
            this._32 = 0.0;
            this._33 = 1.0;
            this._34 = 0.0;
            this._41 = 0.0;
            this._42 = 0.0;
            this._43 = 0.0;
            this._44 = 1.0;
        }

        public Quaternion GetRotation()
        {
            double num1 = this._11 + this._22 + this._33;
            double w;
            double x;
            double y;
            double z;
            if (num1 > 0.0)
            {
                double num2 = System.Math.Sqrt(num1 + 1.0) * 2.0;
                w = 0.25 * num2;
                x = (this._32 - this._23) / num2;
                y = (this._13 - this._31) / num2;
                z = (this._21 - this._12) / num2;
            }
            else if (this._11 > this._22 && this._11 > this._22)
            {
                double num2 = System.Math.Sqrt(1.0 + this._11 - this._22 - this._33) * 2.0;
                w = (this._32 - this._23) / num2;
                x = 0.25 * num2;
                y = (this._12 + this._21) / num2;
                z = (this._13 + this._31) / num2;
            }
            else if (this._22 > this._33)
            {
                double num2 = System.Math.Sqrt(1.0 + this._22 - this._11 - this._33) * 2.0;
                w = (this._13 - this._31) / num2;
                x = (this._12 + this._21) / num2;
                y = 0.25 * num2;
                z = (this._23 + this._32) / num2;
            }
            else
            {
                double num2 = System.Math.Sqrt(1.0 + this._33 - this._11 - this._22) * 2.0;
                w = (this._21 - this._12) / num2;
                x = (this._13 + this._31) / num2;
                y = (this._23 + this._32) / num2;
                z = 0.25 * num2;
            }
            return new Quaternion(x, y, z, w);
        }

        public Vector3 GetPosition()
        {
            double x = this._41;
            double num1 = this._42;
            double num2 = this._43;
            double y = num1;
            double z = num2;
            return new Vector3(x, y, z);
        }

        public Vector3 GetScale()
        {
            double x = System.Math.Sqrt(this._11 * this._11 + this._12 * this._12 + this._13 * this._13);
            double num1 = System.Math.Sqrt(this._21 * this._21 + this._22 * this._22 + this._23 * this._23);
            double num2 = System.Math.Sqrt(this._31 * this._31 + this._32 * this._32 + this._33 * this._33);
            double y = num1;
            double z = num2;
            return new Vector3(x, y, z);
        }

        public Matrix4x4 Transpose()
        {
            return new Matrix4x4()
            {
                _11 = this._11,
                _12 = this._21,
                _13 = this._31,
                _14 = this._41,
                _21 = this._12,
                _22 = this._22,
                _23 = this._32,
                _24 = this._42,
                _31 = this._13,
                _32 = this._23,
                _33 = this._33,
                _34 = this._43,
                _41 = this._14,
                _42 = this._24,
                _43 = this._34,
                _44 = this._44
            };
        }

        public Matrix4x4 Multiply(Matrix4x4 a)
        {
            return new Matrix4x4()
            {
                _11 = this._11 * a._11 + this._12 * a._21 + this._13 * a._31 + this._14 * a._41,
                _12 = this._11 * a._12 + this._12 * a._22 + this._13 * a._32 + this._14 * a._42,
                _13 = this._11 * a._13 + this._12 * a._23 + this._13 * a._33 + this._14 * a._43,
                _14 = this._11 * a._14 + this._12 * a._24 + this._13 * a._34 + this._14 * a._44,
                _21 = this._21 * a._11 + this._22 * a._21 + this._23 * a._31 + this._24 * a._41,
                _22 = this._21 * a._12 + this._22 * a._22 + this._23 * a._32 + this._24 * a._42,
                _23 = this._21 * a._13 + this._22 * a._23 + this._23 * a._33 + this._24 * a._43,
                _24 = this._21 * a._14 + this._22 * a._24 + this._23 * a._34 + this._24 * a._44,
                _31 = this._31 * a._11 + this._32 * a._21 + this._33 * a._31 + this._34 * a._41,
                _32 = this._31 * a._12 + this._32 * a._22 + this._33 * a._32 + this._34 * a._42,
                _33 = this._31 * a._13 + this._32 * a._23 + this._33 * a._33 + this._34 * a._43,
                _34 = this._31 * a._14 + this._32 * a._24 + this._33 * a._34 + this._34 * a._44,
                _41 = this._41 * a._11 + this._42 * a._21 + this._43 * a._31 + this._44 * a._41,
                _42 = this._41 * a._12 + this._42 * a._22 + this._43 * a._32 + this._44 * a._42,
                _43 = this._41 * a._13 + this._42 * a._23 + this._43 * a._33 + this._44 * a._43,
                _44 = this._41 * a._14 + this._42 * a._24 + this._43 * a._34 + this._44 * a._44
            };
        }

        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "{0}/{1}/{2}/{3}\n{4}/{5}/{6}/{7}\n{8}/{9}/{10}/{11}\n{12}/{13}/{14}/{15}\n", (object)this._11, (object)this._12, (object)this._13, (object)this._14, (object)this._21, (object)this._22, (object)this._23, (object)this._24, (object)this._31, (object)this._32, (object)this._33, (object)this._34, (object)this._41, (object)this._42, (object)this._43, (object)this._44);
        }
    }
}
