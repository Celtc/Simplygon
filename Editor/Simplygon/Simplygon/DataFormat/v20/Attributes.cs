﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.Attributes
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.Collections.Generic;

namespace Simplygon.DataFormat.v20
{
  internal class Attributes : ISDFObject
  {
    public ulong AttributesLength { get; protected set; }

    public uint AttributesCount { get; protected set; }

    public List<Attribute> AttributeList { get; set; }

    public Attributes()
    {
      this.AttributeList = new List<Attribute>();
    }

    public ulong GetTotalSizeInBytes()
    {
      return 12UL + this.AttributesLength;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.AttributesLength = binaryStream.ReadUInt64();
      this.AttributesCount = binaryStream.ReadUInt32();
      for (uint index = 0; index < this.AttributesCount; ++index)
        this.AttributeList.Add(Attribute.ReadAttribute(binaryStream));
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteUInt64(this.AttributesLength);
      binaryStream.WriteUInt32(this.AttributesCount);
      foreach (Attribute attribute in this.AttributeList)
        attribute.Write(binaryStream);
    }

    public void Update()
    {
      ulong num = 0;
      foreach (Attribute attribute in this.AttributeList)
      {
        attribute.Update();
        num += attribute.GetTotalSizeInBytes();
      }
      this.AttributesCount = (uint) this.AttributeList.Count;
      this.AttributesLength = num;
    }

    public void Validate()
    {
      foreach (Attribute attribute in this.AttributeList)
        attribute.Validate();
    }

    public void Release()
    {
      foreach (Attribute attribute in this.AttributeList)
        attribute.Release();
    }
  }
}
