﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.Chunk
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.DataType;

namespace Simplygon.DataFormat.v20
{
  internal class Chunk : ISDFObject
  {
    public ChunkHeader ChunkHeader { get; protected set; }

    public Attributes Attributes { get; protected set; }

    public Chunks Chunks { get; protected set; }

    public Chunk()
    {
      this.ChunkHeader = new ChunkHeader();
      this.Attributes = new Attributes();
      this.Chunks = new Chunks();
    }

    public ulong GetTotalSizeInBytes()
    {
      return this.ChunkHeader.GetTotalSizeInBytes() + this.Attributes.GetTotalSizeInBytes() + this.Chunks.GetTotalSizeInBytes();
    }

    public void Read(BinaryStream binaryStream)
    {
      this.ChunkHeader = new ChunkHeader();
      this.ChunkHeader.Read(binaryStream);
      this.Attributes = new Attributes();
      this.Attributes.Read(binaryStream);
      this.Chunks = new Chunks();
      this.Chunks.Read(binaryStream);
    }

    public void Write(BinaryStream binaryStream)
    {
      this.ChunkHeader.Write(binaryStream);
      this.Attributes.Write(binaryStream);
      this.Chunks.Write(binaryStream);
    }

    public void Update()
    {
      this.Attributes.Update();
      this.Chunks.Update();
      this.ChunkHeader.ChunksLength = this.Chunks.GetTotalSizeInBytes();
      this.ChunkHeader.ChunksOffset = this.Attributes.GetTotalSizeInBytes();
    }

    public void Validate()
    {
      this.Attributes.Validate();
    }

    public void Release()
    {
      this.Attributes.Release();
    }

    public Attribute AddAttribute(string name, DataTypeId dataTypeId, object data)
    {
      Attribute attribute = new Attribute(name);
      attribute.SetData(dataTypeId, data);
      this.Attributes.AttributeList.Add(attribute);
      return attribute;
    }
  }
}
