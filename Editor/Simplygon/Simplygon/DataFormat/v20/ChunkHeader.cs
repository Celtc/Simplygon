﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.ChunkHeader
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.DataType;

namespace Simplygon.DataFormat.v20
{
  internal class ChunkHeader : ISDFObject
  {
    public string ChunkType { get; set; }

    public ChunkVersion ChunkVersion { get; set; }

    public ulong ChunksOffset { get; set; }

    public ulong ChunksLength { get; set; }

    public ChunkHeader()
    {
      this.ChunkVersion = new ChunkVersion();
      this.ChunkType = string.Empty;
    }

    public ulong GetTotalSizeInBytes()
    {
      return (ulong) ((long) Util.GetStringSizeInBytes(this.ChunkType) + (long) this.ChunkVersion.GetTotalSizeInBytes() + 8L) + 8UL;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.ChunkType = binaryStream.ReadString();
      this.ChunkVersion = new ChunkVersion();
      this.ChunkVersion.Read(binaryStream);
      this.ChunksOffset = binaryStream.ReadUInt64();
      this.ChunksLength = binaryStream.ReadUInt64();
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteString(this.ChunkType);
      this.ChunkVersion.Write(binaryStream);
      binaryStream.WriteUInt64(this.ChunksOffset);
      binaryStream.WriteUInt64(this.ChunksLength);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
