﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.ChunkInfo
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.DataType;

namespace Simplygon.DataFormat.v20
{
  internal class ChunkInfo : ISDFObject
  {
    public string ChunkType { get; set; }

    public ulong ChunkOffset { get; set; }

    public ulong GetTotalSizeInBytes()
    {
      return Util.GetStringSizeInBytes(this.ChunkType) + 8UL;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.ChunkType = binaryStream.ReadString();
      this.ChunkOffset = binaryStream.ReadUInt64();
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteString(this.ChunkType);
      binaryStream.WriteUInt64(this.ChunkOffset);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
