﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.Attribute
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.DataType;
using Simplygon.DataFormat.v20.Interfaces;
using System;
using System.Globalization;

namespace Simplygon.DataFormat.v20
{
  internal class Attribute : ISDFObject
  {
    public DataTypeId DataTypeId { get; protected set; }

    public string AttributeName { get; protected set; }

    public ulong AttributeLength { get; protected set; }

    public object AttributeData { get; protected set; }

    protected Attribute()
    {
    }

    public Attribute(string name)
    {
      this.AttributeName = name;
    }

    public Attribute(string name, DataTypeId dataTypeId, object data)
    {
      this.AttributeName = name;
      this.SetData(dataTypeId, data);
    }

    public void SetData(DataTypeId dataTypeId, object data)
    {
      if (data == null)
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "Invalid attribute {0} with type {1}", (object) this.AttributeName, (object) dataTypeId));
      this.DataTypeId = dataTypeId;
      this.AttributeData = data;
      this.AttributeLength = this.GetAttributeDataSizeInBytes();
    }

    public void SetStringData(string value)
    {
      this.DataTypeId = DataTypeId.String;
      this.AttributeData = string.IsNullOrEmpty(value) ? (object) string.Empty : (object) value;
      this.AttributeLength = this.GetAttributeDataSizeInBytes();
    }

    public string GetStringData()
    {
      if (this.AttributeData == null || this.DataTypeId != DataTypeId.String)
        return string.Empty;
      return (string) this.AttributeData;
    }

    public ulong GetTotalSizeInBytes()
    {
      return (ulong) (8L + (long) Util.GetStringSizeInBytes(this.AttributeName) + 8L) + this.AttributeLength;
    }

    private ulong GetAttributeDataSizeInBytes()
    {
      switch (this.DataTypeId)
      {
        case DataTypeId.List:
        case DataTypeId.TupleList:
        case DataTypeId.NamedList:
        case DataTypeId.NamedTupleList:
        case DataTypeId.IndexedList:
        case DataTypeId.NamedIndexedList:
        case DataTypeId.NamedIdList:
          return Util.GetSizeInBytesForList(((IDataType) this.AttributeData).DataTypeId, this.AttributeData);
        default:
          return Util.GetSizeInBytes(this.DataTypeId, this.AttributeData);
      }
    }

    public void Read(BinaryStream binaryStream)
    {
      this.DataTypeId = (DataTypeId) binaryStream.ReadUInt64();
      this.AttributeName = binaryStream.ReadString();
      this.AttributeLength = binaryStream.ReadUInt64();
      switch (this.DataTypeId)
      {
        case DataTypeId.List:
          this.AttributeData = Util.ReadList(binaryStream);
          break;
        case DataTypeId.TupleList:
          this.AttributeData = Util.ReadTupleList(binaryStream);
          break;
        case DataTypeId.NamedList:
          this.AttributeData = Util.ReadNamedList(binaryStream);
          break;
        case DataTypeId.NamedTupleList:
          this.AttributeData = Util.ReadNamedTupleList(binaryStream);
          break;
        case DataTypeId.IndexedList:
          this.AttributeData = Util.ReadIndexedList(binaryStream);
          break;
        case DataTypeId.NamedIndexedList:
          this.AttributeData = Util.ReadNamedIndexedList(binaryStream);
          break;
        case DataTypeId.NamedIdList:
          this.AttributeData = Util.ReadNamedIdList(binaryStream);
          break;
        default:
          this.AttributeData = Util.Read(binaryStream, this.DataTypeId);
          break;
      }
    }

    public static Attribute ReadAttribute(BinaryStream binaryStream)
    {
      Attribute attribute = new Attribute();
      attribute.Read(binaryStream);
      return attribute;
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteUInt64((ulong) this.DataTypeId);
      binaryStream.WriteString(this.AttributeName);
      binaryStream.WriteUInt64(this.AttributeLength);
      switch (this.DataTypeId)
      {
        case DataTypeId.List:
        case DataTypeId.TupleList:
        case DataTypeId.NamedList:
        case DataTypeId.NamedTupleList:
        case DataTypeId.IndexedList:
        case DataTypeId.NamedIndexedList:
        case DataTypeId.NamedIdList:
          Util.WriteList(binaryStream, ((IDataType) this.AttributeData).DataTypeId, this.AttributeData);
          break;
        default:
          Util.Write(binaryStream, this.DataTypeId, this.AttributeData);
          break;
      }
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
