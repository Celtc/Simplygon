﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.SDFFile
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.IO;

namespace Simplygon.DataFormat.v20
{
  internal class SDFFile
  {
    public FileHeader FileHeader { get; set; }

    public Attributes Attributes { get; set; }

    public Chunks Chunks { get; set; }

    public SDFFile()
    {
      this.FileHeader = new FileHeader();
      this.Attributes = new Attributes();
      this.Chunks = new Chunks();
    }

    public void Read(string fileName)
    {
      using (BinaryReader binaryReader = new BinaryReader((Stream) File.Open(fileName, FileMode.Open)))
      {
        BinaryStream binaryStream = new BinaryStream(binaryReader);
        binaryStream.ComputeCRC = true;
        this.FileHeader.Read(binaryStream);
        this.Attributes.Read(binaryStream);
        this.Chunks.Read(binaryStream);
        binaryStream.ComputeCRC = false;
        uint num = binaryStream.ReadUInt32();
        if ((int) binaryStream.CRC != (int) num)
          throw new Exception("Invalid CRC value");
      }
    }

    public void Write(string fileName)
    {
      using (BinaryWriter binaryWriter = new BinaryWriter((Stream) File.Open(fileName, FileMode.Create)))
      {
        BinaryStream binaryStream = new BinaryStream(binaryWriter);
        binaryStream.ComputeCRC = true;
        this.FileHeader.Write(binaryStream);
        this.Attributes.Update();
        this.Attributes.Write(binaryStream);
        this.Chunks.Update();
        this.Chunks.Write(binaryStream);
        binaryStream.ComputeCRC = false;
        binaryStream.WriteUInt32(binaryStream.CRC);
      }
    }
  }
}
