﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.ChunkVersion
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.DataFormat.v20
{
  internal class ChunkVersion : ISDFObject
  {
    public byte MajorVersion { get; set; }

    public byte MinorVersion { get; set; }

    public ulong GetTotalSizeInBytes()
    {
      return 2;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.MajorVersion = binaryStream.ReadUInt8();
      this.MinorVersion = binaryStream.ReadUInt8();
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteUInt8(this.MajorVersion);
      binaryStream.WriteUInt8(this.MinorVersion);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
