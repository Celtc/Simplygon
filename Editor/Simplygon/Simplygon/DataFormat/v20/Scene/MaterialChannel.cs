﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.Scene.MaterialChannel
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.DataType;
using System;
using System.Collections.Generic;

namespace Simplygon.DataFormat.v20.Scene
{
  internal class MaterialChannel : ISDFObject
  {
    private Attribute channelNameAttribute;
    private Attribute colorAttribute;
    private Attribute shadingNetworkAttribute;
    private Attribute deltagenShininessAttribute;
    private Attribute deltagenReflectivityAttribute;
    private Attribute deltagenReflectionExposureAttribute;
    private Attribute deltagenFresnelMinimunAttribute;
    private Attribute deltagenFresnelScaleAttribute;
    private Attribute deltagenFresnelExponentAttribute;
    private Attribute deltagenUseReflectionAttribute;
    private Attribute deltagenUseFresnelAttribute;
    private Attribute deltagenUseClearCoatAttribute;
    private Attribute deltagenColoredReflectionAttribute;
    private Attribute deltagenNormalMapScaleAttribute;
    private Attribute deltagenTransparencyAttribute;

    public string ChannelName
    {
      get
      {
        return this.channelNameAttribute.GetStringData();
      }
      set
      {
        this.channelNameAttribute.SetStringData(value);
      }
    }

    public Vector4 Color
    {
      get
      {
        return (Vector4) this.colorAttribute.AttributeData;
      }
      set
      {
        this.colorAttribute.SetData(DataTypeId.Vector4, (object) value);
      }
    }

    public string ShadingNetwork
    {
      get
      {
        return this.shadingNetworkAttribute.GetStringData();
      }
      set
      {
        this.shadingNetworkAttribute.SetStringData(value);
      }
    }

    public float DeltagenShininess
    {
      get
      {
        return Convert.ToSingle(this.deltagenShininessAttribute.AttributeData);
      }
      set
      {
        this.deltagenShininessAttribute.SetData(DataTypeId.Float, (object) value);
      }
    }

    public float DeltagenReflectivity
    {
      get
      {
        return Convert.ToSingle(this.deltagenReflectivityAttribute.AttributeData);
      }
      set
      {
        this.deltagenReflectivityAttribute.SetData(DataTypeId.Float, (object) value);
      }
    }

    public float DeltagenReflectionExposure
    {
      get
      {
        return Convert.ToSingle(this.deltagenReflectionExposureAttribute.AttributeData);
      }
      set
      {
        this.deltagenReflectionExposureAttribute.SetData(DataTypeId.Float, (object) value);
      }
    }

    public float DeltagenFresnelMinimun
    {
      get
      {
        return Convert.ToSingle(this.deltagenFresnelMinimunAttribute.AttributeData);
      }
      set
      {
        this.deltagenFresnelMinimunAttribute.SetData(DataTypeId.Float, (object) value);
      }
    }

    public float DeltagenFresnelScale
    {
      get
      {
        return Convert.ToSingle(this.deltagenFresnelScaleAttribute.AttributeData);
      }
      set
      {
        this.deltagenFresnelScaleAttribute.SetData(DataTypeId.Float, (object) value);
      }
    }

    public float DeltagenFresnelExponent
    {
      get
      {
        return Convert.ToSingle(this.deltagenFresnelExponentAttribute.AttributeData);
      }
      set
      {
        this.deltagenFresnelExponentAttribute.SetData(DataTypeId.Float, (object) value);
      }
    }

    public bool DeltagenUseReflection
    {
      get
      {
        return (bool) this.deltagenUseReflectionAttribute.AttributeData;
      }
      set
      {
        this.deltagenUseReflectionAttribute.SetData(DataTypeId.Bool, (object) value);
      }
    }

    public bool DeltagenUseFresnel
    {
      get
      {
        return (bool) this.deltagenUseFresnelAttribute.AttributeData;
      }
      set
      {
        this.deltagenUseFresnelAttribute.SetData(DataTypeId.Bool, (object) value);
      }
    }

    public bool DeltagenUseClearCoat
    {
      get
      {
        return (bool) this.deltagenUseClearCoatAttribute.AttributeData;
      }
      set
      {
        this.deltagenUseClearCoatAttribute.SetData(DataTypeId.Bool, (object) value);
      }
    }

    public bool DeltagenColoredReflection
    {
      get
      {
        return (bool) this.deltagenColoredReflectionAttribute.AttributeData;
      }
      set
      {
        this.deltagenColoredReflectionAttribute.SetData(DataTypeId.Bool, (object) value);
      }
    }

    public float DeltagenNormalMapScale
    {
      get
      {
        return Convert.ToSingle(this.deltagenNormalMapScaleAttribute.AttributeData);
      }
      set
      {
        this.deltagenNormalMapScaleAttribute.SetData(DataTypeId.Float, (object) value);
      }
    }

    public float DeltagenTransparency
    {
      get
      {
        return Convert.ToSingle(this.deltagenTransparencyAttribute.AttributeData);
      }
      set
      {
        this.deltagenTransparencyAttribute.SetData(DataTypeId.Float, (object) value);
      }
    }

    public System.Collections.Generic.List<MaterialChannelTextureDescriptor> Textures { get; set; }

    public Chunk Chunk { get; set; }

    public MaterialChannel()
    {
      this.Chunk = new Chunk();
      this.channelNameAttribute = this.Chunk.AddAttribute(nameof (ChannelName), DataTypeId.String, (object) string.Empty);
      this.colorAttribute = this.Chunk.AddAttribute(nameof (Color), DataTypeId.Vector4, (object) new Vector4(1.0, 1.0, 1.0, 1.0));
      this.shadingNetworkAttribute = this.Chunk.AddAttribute(nameof (ShadingNetwork), DataTypeId.String, (object) string.Empty);
      this.deltagenShininessAttribute = this.Chunk.AddAttribute(nameof (DeltagenShininess), DataTypeId.Float, (object) 0.0f);
      this.deltagenReflectivityAttribute = this.Chunk.AddAttribute(nameof (DeltagenReflectivity), DataTypeId.Float, (object) 0.0f);
      this.deltagenReflectionExposureAttribute = this.Chunk.AddAttribute(nameof (DeltagenReflectionExposure), DataTypeId.Float, (object) 0.0f);
      this.deltagenFresnelMinimunAttribute = this.Chunk.AddAttribute(nameof (DeltagenFresnelMinimun), DataTypeId.Float, (object) 0.0f);
      this.deltagenFresnelScaleAttribute = this.Chunk.AddAttribute(nameof (DeltagenFresnelScale), DataTypeId.Float, (object) 0.0f);
      this.deltagenFresnelExponentAttribute = this.Chunk.AddAttribute(nameof (DeltagenFresnelExponent), DataTypeId.Float, (object) 0.0f);
      this.deltagenUseReflectionAttribute = this.Chunk.AddAttribute(nameof (DeltagenUseReflection), DataTypeId.Bool, (object) false);
      this.deltagenUseFresnelAttribute = this.Chunk.AddAttribute(nameof (DeltagenUseFresnel), DataTypeId.Bool, (object) false);
      this.deltagenUseClearCoatAttribute = this.Chunk.AddAttribute(nameof (DeltagenUseClearCoat), DataTypeId.Bool, (object) false);
      this.deltagenColoredReflectionAttribute = this.Chunk.AddAttribute(nameof (DeltagenColoredReflection), DataTypeId.Bool, (object) false);
      this.deltagenNormalMapScaleAttribute = this.Chunk.AddAttribute(nameof (DeltagenNormalMapScale), DataTypeId.Float, (object) 0.0f);
      this.deltagenTransparencyAttribute = this.Chunk.AddAttribute(nameof (DeltagenTransparency), DataTypeId.Float, (object) 0.0f);
      this.Textures = new System.Collections.Generic.List<MaterialChannelTextureDescriptor>();
    }

    public ulong GetTotalSizeInBytes()
    {
      this.Update();
      return this.Chunk.GetTotalSizeInBytes();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Update();
      this.Chunk.Write(binaryStream);
    }

    public void Read(BinaryStream binaryStream)
    {
    }

    public void Read(Chunk chunk)
    {
      this.Chunk = chunk;
      foreach (Attribute attribute in this.Chunk.Attributes.AttributeList)
      {
        if (attribute.AttributeName == "ChannelName" && attribute.DataTypeId == DataTypeId.String)
          this.channelNameAttribute = attribute;
        else if (attribute.AttributeName == "Color" && attribute.DataTypeId == DataTypeId.Vector4)
          this.colorAttribute = attribute;
        else if (attribute.AttributeName == "ShadingNetwork" && attribute.DataTypeId == DataTypeId.String)
          this.shadingNetworkAttribute = attribute;
        else if (attribute.AttributeName == "DeltagenShininess" && attribute.DataTypeId == DataTypeId.Float)
          this.deltagenShininessAttribute = attribute;
        else if (attribute.AttributeName == "DeltagenReflectivity" && attribute.DataTypeId == DataTypeId.Float)
          this.deltagenReflectivityAttribute = attribute;
        else if (attribute.AttributeName == "DeltagenReflectionExposure" && attribute.DataTypeId == DataTypeId.Float)
          this.deltagenReflectionExposureAttribute = attribute;
        else if (attribute.AttributeName == "DeltagenFresnelMinimun" && attribute.DataTypeId == DataTypeId.Float)
          this.deltagenFresnelMinimunAttribute = attribute;
        else if (attribute.AttributeName == "DeltagenFresnelScale" && attribute.DataTypeId == DataTypeId.Float)
          this.deltagenFresnelScaleAttribute = attribute;
        else if (attribute.AttributeName == "DeltagenFresnelExponent" && attribute.DataTypeId == DataTypeId.Float)
          this.deltagenFresnelExponentAttribute = attribute;
        else if (attribute.AttributeName == "DeltagenUseReflection" && attribute.DataTypeId == DataTypeId.Bool)
          this.deltagenUseReflectionAttribute = attribute;
        else if (attribute.AttributeName == "DeltagenUseFresnel" && attribute.DataTypeId == DataTypeId.Bool)
          this.deltagenUseFresnelAttribute = attribute;
        else if (attribute.AttributeName == "DeltagenUseClearCoat" && attribute.DataTypeId == DataTypeId.Bool)
          this.deltagenUseClearCoatAttribute = attribute;
        else if (attribute.AttributeName == "DeltagenColoredReflection" && attribute.DataTypeId == DataTypeId.Bool)
          this.deltagenColoredReflectionAttribute = attribute;
        else if (attribute.AttributeName == "DeltagenNormalMapScale" && attribute.DataTypeId == DataTypeId.Float)
          this.deltagenNormalMapScaleAttribute = attribute;
        else if (attribute.AttributeName == "DeltagenTransparency" && attribute.DataTypeId == DataTypeId.Float)
          this.deltagenTransparencyAttribute = attribute;
      }
      foreach (Chunk chunk1 in this.Chunk.Chunks.ChunkList)
      {
        if (chunk1.ChunkHeader.ChunkType == "MaterialChannelTextureDescriptor")
        {
          MaterialChannelTextureDescriptor textureDescriptor = new MaterialChannelTextureDescriptor();
          textureDescriptor.Read(chunk1);
          this.Textures.Add(textureDescriptor);
        }
      }
      this.Validate();
    }

    public void Update()
    {
      this.Validate();
      this.Chunk.ChunkHeader.ChunkType = nameof (MaterialChannel);
      this.Chunk.Chunks.ChunkList.Clear();
      foreach (MaterialChannelTextureDescriptor texture in this.Textures)
      {
        texture.Update();
        this.Chunk.Chunks.ChunkList.Add(texture.Chunk);
      }
      this.Chunk.Update();
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
