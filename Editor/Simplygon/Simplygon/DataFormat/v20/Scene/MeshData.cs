﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.Scene.MeshData
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.DataType;
using Simplygon.DataFormat.v20.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Simplygon.DataFormat.v20.Scene
{
  internal class MeshData : ISDFObject
  {
    private Dictionary<string, Attribute> textureCoordinatesAttributes = new Dictionary<string, Attribute>();
    private Dictionary<string, Attribute> colorsAttributes = new Dictionary<string, Attribute>();
    private Dictionary<string, Attribute> customFieldsAttributes = new Dictionary<string, Attribute>();
    private Dictionary<string, Attribute> blendShapesAttributes = new Dictionary<string, Attribute>();
    private Dictionary<string, Attribute> vertexSetsAttributes = new Dictionary<string, Attribute>();
    private Dictionary<string, Attribute> edgeSetsAttributes = new Dictionary<string, Attribute>();
    private Dictionary<string, Attribute> faceSetsAttributes = new Dictionary<string, Attribute>();
    private Attribute coordinatesAttribute;
    private Attribute boneWeightsAttribute;
    private Attribute boneIndicesAttribute;
    private Attribute vertexLocksAttribute;
    private Attribute vertexWeightsAttribute;
    private Attribute vertexCreaseAttribute;
    private Attribute originalVertexIndicesAttribute;
    private Attribute normalsAttribute;
    private Attribute tangentsAttribute;
    private Attribute bitangentsAttribute;
    private Attribute blendShapeTargetsAttribute;
    private Attribute edgeCreaseAttribute;
    private Attribute edgeWeightsAttribute;
    private Attribute triangleIndicesAttribute;
    private Attribute materialIndicesAttribute;
    private Attribute originalTriangleIndicesAttribute;
    private Attribute smoothingGroupAttribute;
    private Attribute maxDeviationAttribute;
    private Attribute pixelSizeAttribute;
    private Attribute globalLODIndexAttribute;

    public Chunk Chunk { get; set; }

    public Vector3[] Coordinates
    {
      get
      {
        if (this.coordinatesAttribute != null)
          return ((DataType.List<Vector3>) this.coordinatesAttribute.AttributeData).Items;
        return (Vector3[]) null;
      }
    }

    public Index3[] TriangleIndices
    {
      get
      {
        if (this.triangleIndicesAttribute != null)
          return ((DataType.List<Index3>) this.triangleIndicesAttribute.AttributeData).Items;
        return (Index3[]) null;
      }
    }

    public uint[] MaterialIndices
    {
      get
      {
        if (this.materialIndicesAttribute != null)
          return ((DataType.List<uint>) this.materialIndicesAttribute.AttributeData).Items;
        return (uint[]) null;
      }
    }

    public double[] BoneWeights
    {
      get
      {
        if (this.boneWeightsAttribute != null)
          return ((DataType.List<double>) this.boneWeightsAttribute.AttributeData).Items;
        return (double[]) null;
      }
    }

    public int[] BoneIndices
    {
      get
      {
        if (this.boneIndicesAttribute != null)
          return ((DataType.List<int>) this.boneIndicesAttribute.AttributeData).Items;
        return (int[]) null;
      }
    }

    public bool[] VertexLocks
    {
      get
      {
        if (this.vertexLocksAttribute != null)
          return ((DataType.List<bool>) this.vertexLocksAttribute.AttributeData).Items;
        return (bool[]) null;
      }
    }

    public double[] VertexWeights
    {
      get
      {
        if (this.vertexWeightsAttribute != null)
          return ((DataType.List<double>) this.vertexWeightsAttribute.AttributeData).Items;
        return (double[]) null;
      }
    }

    public double[] VertexCrease
    {
      get
      {
        if (this.vertexCreaseAttribute != null)
          return ((DataType.List<double>) this.vertexCreaseAttribute.AttributeData).Items;
        return (double[]) null;
      }
    }

    public int[] OriginalVertexIndices
    {
      get
      {
        if (this.originalVertexIndicesAttribute != null)
          return ((DataType.List<int>) this.originalVertexIndicesAttribute.AttributeData).Items;
        return (int[]) null;
      }
    }

    public Vector3[] Normals
    {
      get
      {
        if (this.normalsAttribute != null)
          return ((DataType.List<Vector3>) this.normalsAttribute.AttributeData).Items;
        return (Vector3[]) null;
      }
    }

    public Vector3[] Tangents
    {
      get
      {
        if (this.tangentsAttribute != null)
          return ((DataType.List<Vector3>) this.tangentsAttribute.AttributeData).Items;
        return (Vector3[]) null;
      }
    }

    public Vector3[] Bitangents
    {
      get
      {
        if (this.bitangentsAttribute != null)
          return ((DataType.List<Vector3>) this.bitangentsAttribute.AttributeData).Items;
        return (Vector3[]) null;
      }
    }

    public Vector3[] BlendShapeTargets
    {
      get
      {
        if (this.blendShapeTargetsAttribute != null)
          return ((DataType.List<Vector3>) this.blendShapeTargetsAttribute.AttributeData).Items;
        return (Vector3[]) null;
      }
    }

    public double[] EdgeCrease
    {
      get
      {
        if (this.edgeCreaseAttribute != null)
          return ((DataType.List<double>) this.edgeCreaseAttribute.AttributeData).Items;
        return (double[]) null;
      }
    }

    public double[] EdgeWeights
    {
      get
      {
        if (this.edgeWeightsAttribute != null)
          return ((DataType.List<double>) this.edgeWeightsAttribute.AttributeData).Items;
        return (double[]) null;
      }
    }

    public int[] SmoothingGroup
    {
      get
      {
        if (this.smoothingGroupAttribute != null)
          return ((DataType.List<int>) this.smoothingGroupAttribute.AttributeData).Items;
        return (int[]) null;
      }
    }

    public int[] OriginalTriangleIndices
    {
      get
      {
        if (this.originalTriangleIndicesAttribute != null)
          return ((DataType.List<int>) this.originalTriangleIndicesAttribute.AttributeData).Items;
        return (int[]) null;
      }
    }

    public int BonesPerVertex
    {
      get
      {
        if (this.boneIndicesAttribute != null)
          return ((TupleList<int>) this.boneIndicesAttribute.AttributeData).TupleSize;
        return 0;
      }
    }

    public double MaxDeviation
    {
      get
      {
        if (this.maxDeviationAttribute != null)
          return (double) this.maxDeviationAttribute.AttributeData;
        return -1.0;
      }
    }

    public int PixelSize
    {
      get
      {
        if (this.pixelSizeAttribute != null)
          return (int) this.pixelSizeAttribute.AttributeData;
        return -1;
      }
    }

    public int GlobalLODIndex
    {
      get
      {
        if (this.globalLODIndexAttribute != null)
          return (int) this.globalLODIndexAttribute.AttributeData;
        return -1;
      }
    }

    private void InitializeDefaultValues()
    {
      this.Chunk = new Chunk();
      this.Chunk.ChunkHeader.ChunkType = nameof (MeshData);
    }

    public MeshData()
    {
      this.InitializeDefaultValues();
    }

    public int VertexCount
    {
      get
      {
        return this.Coordinates.Length;
      }
    }

    public int TriangleCount
    {
      get
      {
        return this.TriangleIndices.Length;
      }
    }

    public int CornerCount
    {
      get
      {
        return this.TriangleIndices.Length * 3;
      }
    }

    public string[] TextureCoordinateSets
    {
      get
      {
        return this.textureCoordinatesAttributes.Keys.ToArray<string>();
      }
    }

    public string[] ColorSets
    {
      get
      {
        return this.colorsAttributes.Keys.ToArray<string>();
      }
    }

    public string[] CustomFieldSet
    {
      get
      {
        return this.customFieldsAttributes.Keys.ToArray<string>();
      }
    }

    public string[] BlendShapeSet
    {
      get
      {
        return this.blendShapesAttributes.Keys.ToArray<string>();
      }
    }

    public string[] VertexSets
    {
      get
      {
        return this.vertexSetsAttributes.Keys.ToArray<string>();
      }
    }

    public string[] EdgeSets
    {
      get
      {
        return this.edgeSetsAttributes.Keys.ToArray<string>();
      }
    }

    public string[] FaceSets
    {
      get
      {
        return this.faceSetsAttributes.Keys.ToArray<string>();
      }
    }

    public Vector2[] GetTextureCoordinates(string name)
    {
      return ((DataType.List<Vector2>) this.textureCoordinatesAttributes[name].AttributeData).Items;
    }

    public Vector4[] GetColors(string name)
    {
      return ((DataType.List<Vector4>) this.colorsAttributes[name].AttributeData).Items;
    }

    public byte[][] GetCustomFields(string name)
    {
      return ((DataType.List<byte[]>) this.customFieldsAttributes[name].AttributeData).Items;
    }

    public Vector3[] GetBlendShapes(string name)
    {
      return ((DataType.List<Vector3>) this.blendShapesAttributes[name].AttributeData).Items;
    }

    public uint[] GetVertexSet(string name)
    {
      return ((DataType.List<uint>) this.vertexSetsAttributes[name].AttributeData).Items;
    }

    public uint[] GetEdgeSet(string name)
    {
      return ((DataType.List<uint>) this.edgeSetsAttributes[name].AttributeData).Items;
    }

    public uint[] GetFaceSet(string name)
    {
      return ((DataType.List<uint>) this.faceSetsAttributes[name].AttributeData).Items;
    }

    public void SetPixelSize(int pixelsize)
    {
      if (this.pixelSizeAttribute == null)
        this.pixelSizeAttribute = this.Chunk.AddAttribute("PixelSize", DataTypeId.Int32, (object) pixelsize);
      else
        this.pixelSizeAttribute.SetData(DataTypeId.Int32, (object) pixelsize);
    }

    public void SetGlobalLODIndex(int globalLODIndex)
    {
      if (this.globalLODIndexAttribute == null)
        this.globalLODIndexAttribute = this.Chunk.AddAttribute("GlobalLODIndex", DataTypeId.Int32, (object) globalLODIndex);
      else
        this.globalLODIndexAttribute.SetData(DataTypeId.Int32, (object) globalLODIndex);
    }

    public void SetMaxDeviation(double maxdeviation)
    {
      if (this.maxDeviationAttribute == null)
        this.maxDeviationAttribute = this.Chunk.AddAttribute("MaxDeviation", DataTypeId.Double, (object) maxdeviation);
      else
        this.maxDeviationAttribute.SetData(DataTypeId.Double, (object) maxdeviation);
    }

    public void SetCoordinates(Vector3[] coordinates)
    {
      if (this.coordinatesAttribute == null)
        this.coordinatesAttribute = this.Chunk.AddAttribute("Coordinates", DataTypeId.List, (object) new DataType.List<Vector3>(DataTypeId.Vector3, coordinates));
      else
        this.coordinatesAttribute.SetData(DataTypeId.List, (object) new DataType.List<Vector3>(DataTypeId.Vector3, coordinates));
    }

    public void SetTriangleIndices(Index3[] triangleIndices)
    {
      if (this.triangleIndicesAttribute == null)
        this.triangleIndicesAttribute = this.Chunk.AddAttribute("TriangleIndices", DataTypeId.List, (object) new DataType.List<Index3>(DataTypeId.Index3, triangleIndices));
      else
        this.triangleIndicesAttribute.SetData(DataTypeId.List, (object) new DataType.List<Index3>(DataTypeId.Index3, triangleIndices));
    }

    public void SetMaterialIndices(uint[] materialIndices)
    {
      if (this.materialIndicesAttribute == null)
        this.materialIndicesAttribute = this.Chunk.AddAttribute("MaterialIndices", DataTypeId.List, (object) new DataType.List<uint>(DataTypeId.UInt32, materialIndices));
      else
        this.materialIndicesAttribute.SetData(DataTypeId.List, (object) new DataType.List<uint>(DataTypeId.UInt32, materialIndices));
    }

    public void SetBoneWeights(double[] boneWeights, int tupleSize)
    {
      if (this.boneWeightsAttribute == null)
        this.boneWeightsAttribute = this.Chunk.AddAttribute("BoneWeights", DataTypeId.TupleList, (object) new TupleList<double>(DataTypeId.Double, tupleSize, boneWeights));
      else
        this.boneWeightsAttribute.SetData(DataTypeId.TupleList, (object) new TupleList<double>(DataTypeId.Double, tupleSize, boneWeights));
    }

    public void SetBoneIndices(int[] boneIndices, int tupleSize)
    {
      if (this.boneIndicesAttribute == null)
        this.boneIndicesAttribute = this.Chunk.AddAttribute("BoneIndices", DataTypeId.TupleList, (object) new TupleList<int>(DataTypeId.Int32, tupleSize, boneIndices));
      else
        this.boneIndicesAttribute.SetData(DataTypeId.TupleList, (object) new TupleList<int>(DataTypeId.Int32, tupleSize, boneIndices));
    }

    public void SetVertexLocks(bool[] vertexLocks)
    {
      if (this.vertexLocksAttribute == null)
        this.vertexLocksAttribute = this.Chunk.AddAttribute("VertexLocks", DataTypeId.List, (object) new DataType.List<bool>(DataTypeId.Bool, vertexLocks));
      else
        this.vertexLocksAttribute.SetData(DataTypeId.List, (object) new DataType.List<bool>(DataTypeId.Bool, vertexLocks));
    }

    public void SetVertexWeights(double[] vertexWeights)
    {
      if (this.vertexWeightsAttribute == null)
        this.vertexWeightsAttribute = this.Chunk.AddAttribute("VertexWeights", DataTypeId.List, (object) new DataType.List<double>(DataTypeId.Double, vertexWeights));
      else
        this.vertexWeightsAttribute.SetData(DataTypeId.List, (object) new DataType.List<double>(DataTypeId.Double, vertexWeights));
    }

    public void SetVertexCrease(double[] vertexCrease)
    {
      if (this.vertexCreaseAttribute == null)
        this.vertexCreaseAttribute = this.Chunk.AddAttribute("VertexCrease", DataTypeId.List, (object) new DataType.List<double>(DataTypeId.Double, vertexCrease));
      else
        this.vertexCreaseAttribute.SetData(DataTypeId.List, (object) new DataType.List<double>(DataTypeId.Double, vertexCrease));
    }

    public void SetOriginalVertexIndices(int[] originalVertexIndices)
    {
      if (this.originalVertexIndicesAttribute == null)
        this.originalVertexIndicesAttribute = this.Chunk.AddAttribute("OriginalVertexIndices", DataTypeId.List, (object) new DataType.List<int>(DataTypeId.Int32, originalVertexIndices));
      else
        this.originalVertexIndicesAttribute.SetData(DataTypeId.List, (object) new DataType.List<int>(DataTypeId.Int32, originalVertexIndices));
    }

    public void SetNormals(Vector3[] normals)
    {
      if (this.normalsAttribute == null)
        this.normalsAttribute = this.Chunk.AddAttribute("Normals", DataTypeId.List, (object) new DataType.List<Vector3>(DataTypeId.Vector3, normals));
      else
        this.normalsAttribute.SetData(DataTypeId.List, (object) new DataType.List<Vector3>(DataTypeId.Vector3, normals));
    }

    public void SetTangents(Vector3[] tangents)
    {
      if (this.tangentsAttribute == null)
        this.tangentsAttribute = this.Chunk.AddAttribute("Tangents", DataTypeId.List, (object) new DataType.List<Vector3>(DataTypeId.Vector3, tangents));
      else
        this.tangentsAttribute.SetData(DataTypeId.List, (object) new DataType.List<Vector3>(DataTypeId.Vector3, tangents));
    }

    public void SetBitangents(Vector3[] bitangents)
    {
      if (this.bitangentsAttribute == null)
        this.bitangentsAttribute = this.Chunk.AddAttribute("Bitangents", DataTypeId.List, (object) new DataType.List<Vector3>(DataTypeId.Vector3, bitangents));
      else
        this.bitangentsAttribute.SetData(DataTypeId.List, (object) new DataType.List<Vector3>(DataTypeId.Vector3, bitangents));
    }

    public void SetBlendShapeTargets(Vector3[] blendShapeTargets)
    {
      if (this.blendShapeTargetsAttribute == null)
        this.blendShapeTargetsAttribute = this.Chunk.AddAttribute("BlendShapeTargets", DataTypeId.List, (object) new DataType.List<Vector3>(DataTypeId.Vector3, blendShapeTargets));
      else
        this.blendShapeTargetsAttribute.SetData(DataTypeId.List, (object) new DataType.List<Vector3>(DataTypeId.Vector3, blendShapeTargets));
    }

    public void SetEdgeCrease(double[] edgeCrease)
    {
      if (this.edgeCreaseAttribute == null)
        this.edgeCreaseAttribute = this.Chunk.AddAttribute("EdgeCrease", DataTypeId.List, (object) new DataType.List<double>(DataTypeId.Double, edgeCrease));
      else
        this.edgeCreaseAttribute.SetData(DataTypeId.List, (object) new DataType.List<double>(DataTypeId.Double, edgeCrease));
    }

    public void SetEdgeWeights(double[] edgeWeights)
    {
      if (this.edgeWeightsAttribute == null)
        this.edgeWeightsAttribute = this.Chunk.AddAttribute("EdgeWeights", DataTypeId.List, (object) new DataType.List<double>(DataTypeId.Double, edgeWeights));
      else
        this.edgeWeightsAttribute.SetData(DataTypeId.List, (object) new DataType.List<double>(DataTypeId.Double, edgeWeights));
    }

    public void SetSmoothingGroup(int[] smoothingGroup)
    {
      if (this.smoothingGroupAttribute == null)
        this.smoothingGroupAttribute = this.Chunk.AddAttribute("SmoothingGroup", DataTypeId.List, (object) new DataType.List<int>(DataTypeId.Int32, smoothingGroup));
      else
        this.smoothingGroupAttribute.SetData(DataTypeId.List, (object) new DataType.List<int>(DataTypeId.Int32, smoothingGroup));
    }

    public void SetOriginalTriangleIndices(int[] originalTriangleIndices)
    {
      if (this.originalTriangleIndicesAttribute == null)
        this.originalTriangleIndicesAttribute = this.Chunk.AddAttribute("OriginalTriangleIndices", DataTypeId.List, (object) new DataType.List<int>(DataTypeId.Int32, originalTriangleIndices));
      else
        this.originalTriangleIndicesAttribute.SetData(DataTypeId.List, (object) new DataType.List<int>(DataTypeId.Int32, originalTriangleIndices));
    }

    public void SetTextureCoordinates(string name, Vector2[] textureCoordinates)
    {
      if (!this.textureCoordinatesAttributes.ContainsKey(name))
      {
        Attribute attribute = this.Chunk.AddAttribute("TextureCoordinates", DataTypeId.NamedList, (object) new NamedList<Vector2>(name, DataTypeId.Vector2, textureCoordinates));
        this.textureCoordinatesAttributes.Add(name, attribute);
      }
      else
        this.textureCoordinatesAttributes[name].SetData(DataTypeId.NamedList, (object) new NamedList<Vector2>(name, DataTypeId.Vector2, textureCoordinates));
    }

    public void SetColors(string name, Vector4[] colors)
    {
      if (!this.colorsAttributes.ContainsKey(name))
      {
        Attribute attribute = this.Chunk.AddAttribute("Colors", DataTypeId.NamedList, (object) new NamedList<Vector4>(name, DataTypeId.Vector4, colors));
        this.colorsAttributes.Add(name, attribute);
      }
      else
        this.colorsAttributes[name].SetData(DataTypeId.NamedList, (object) new NamedList<Vector4>(name, DataTypeId.Vector4, colors));
    }

    public void SetCustomField(string name, byte[][] customField)
    {
      if (!this.customFieldsAttributes.ContainsKey(name))
      {
        Attribute attribute = this.Chunk.AddAttribute("CustomField", DataTypeId.NamedList, (object) new NamedList<byte[]>(name, DataTypeId.Blob, customField));
        this.customFieldsAttributes.Add(name, attribute);
      }
      else
        this.customFieldsAttributes[name].SetData(DataTypeId.NamedList, (object) new NamedList<byte[]>(name, DataTypeId.Blob, customField));
    }

    public void SetBlendShapes(string name, Vector3[] blendShape)
    {
      if (!this.blendShapesAttributes.ContainsKey(name))
      {
        Attribute attribute = this.Chunk.AddAttribute("BlendShapes", DataTypeId.NamedList, (object) new NamedList<Vector3>(name, DataTypeId.Vector3, blendShape));
        this.blendShapesAttributes.Add(name, attribute);
      }
      else
        this.blendShapesAttributes[name].SetData(DataTypeId.NamedList, (object) new NamedList<Vector3>(name, DataTypeId.Vector3, blendShape));
    }

    public void SetVertexSet(string name, uint[] vertexSet)
    {
      if (!this.vertexSetsAttributes.ContainsKey(name))
      {
        Attribute attribute = this.Chunk.AddAttribute("VertexSet", DataTypeId.NamedList, (object) new NamedList<uint>(name, DataTypeId.UInt32, vertexSet));
        this.vertexSetsAttributes.Add(name, attribute);
      }
      else
        this.vertexSetsAttributes[name].SetData(DataTypeId.NamedList, (object) new NamedList<uint>(name, DataTypeId.UInt32, vertexSet));
    }

    public void SetEdgeSet(string name, uint[] edgeSet)
    {
      if (!this.edgeSetsAttributes.ContainsKey(name))
      {
        Attribute attribute = this.Chunk.AddAttribute("EdgeSet", DataTypeId.NamedList, (object) new NamedList<uint>(name, DataTypeId.UInt32, edgeSet));
        this.edgeSetsAttributes.Add(name, attribute);
      }
      else
        this.edgeSetsAttributes[name].SetData(DataTypeId.NamedList, (object) new NamedList<uint>(name, DataTypeId.UInt32, edgeSet));
    }

    public void SetFaceSet(string name, uint[] faceSet)
    {
      if (!this.faceSetsAttributes.ContainsKey(name))
      {
        Attribute attribute = this.Chunk.AddAttribute("FaceSet", DataTypeId.NamedList, (object) new NamedList<uint>(name, DataTypeId.UInt32, faceSet));
        this.faceSetsAttributes.Add(name, attribute);
      }
      else
        this.faceSetsAttributes[name].SetData(DataTypeId.NamedList, (object) new NamedList<uint>(name, DataTypeId.UInt32, faceSet));
    }

    public ulong GetTotalSizeInBytes()
    {
      this.Update();
      return this.Chunk.GetTotalSizeInBytes();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Update();
      this.Chunk.Write(binaryStream);
    }

    public void Read(BinaryStream binaryStream)
    {
    }

    public void Read(Chunk chunk)
    {
      this.Chunk = chunk;
      this.coordinatesAttribute = (Attribute) null;
      this.triangleIndicesAttribute = (Attribute) null;
      this.materialIndicesAttribute = (Attribute) null;
      this.boneWeightsAttribute = (Attribute) null;
      this.boneIndicesAttribute = (Attribute) null;
      this.vertexLocksAttribute = (Attribute) null;
      this.vertexWeightsAttribute = (Attribute) null;
      this.vertexCreaseAttribute = (Attribute) null;
      this.originalVertexIndicesAttribute = (Attribute) null;
      this.normalsAttribute = (Attribute) null;
      this.tangentsAttribute = (Attribute) null;
      this.bitangentsAttribute = (Attribute) null;
      this.maxDeviationAttribute = (Attribute) null;
      this.pixelSizeAttribute = (Attribute) null;
      this.blendShapeTargetsAttribute = (Attribute) null;
      this.edgeCreaseAttribute = (Attribute) null;
      this.edgeWeightsAttribute = (Attribute) null;
      this.smoothingGroupAttribute = (Attribute) null;
      this.originalTriangleIndicesAttribute = (Attribute) null;
      this.textureCoordinatesAttributes.Clear();
      this.colorsAttributes.Clear();
      this.customFieldsAttributes.Clear();
      this.blendShapesAttributes.Clear();
      this.vertexSetsAttributes.Clear();
      this.edgeSetsAttributes.Clear();
      this.faceSetsAttributes.Clear();
      foreach (Attribute attribute in this.Chunk.Attributes.AttributeList)
      {
        switch (attribute.AttributeName)
        {
          case "Bitangents":
            this.bitangentsAttribute = attribute;
            continue;
          case "BlendShapeTargets":
            this.blendShapeTargetsAttribute = attribute;
            continue;
          case "BlendShapes":
            this.blendShapesAttributes.Add(((INamedList) attribute.AttributeData).Name, attribute);
            continue;
          case "BoneIndices":
            this.boneIndicesAttribute = attribute;
            continue;
          case "BoneWeights":
            this.boneWeightsAttribute = attribute;
            continue;
          case "Colors":
            this.colorsAttributes.Add(((INamedList) attribute.AttributeData).Name, attribute);
            continue;
          case "Coordinates":
            this.coordinatesAttribute = attribute;
            continue;
          case "CustomField":
            this.customFieldsAttributes.Add(((INamedList) attribute.AttributeData).Name, attribute);
            continue;
          case "EdgeCrease":
            this.edgeCreaseAttribute = attribute;
            continue;
          case "EdgeSet":
            this.edgeSetsAttributes.Add(((INamedList) attribute.AttributeData).Name, attribute);
            continue;
          case "EdgeWeights":
            this.edgeWeightsAttribute = attribute;
            continue;
          case "FaceSet":
            this.faceSetsAttributes.Add(((INamedList) attribute.AttributeData).Name, attribute);
            continue;
          case "GlobalLODIndex":
            this.globalLODIndexAttribute = attribute;
            continue;
          case "MaterialIndices":
            this.materialIndicesAttribute = attribute;
            continue;
          case "MaxDeviation":
            this.maxDeviationAttribute = attribute;
            continue;
          case "Normals":
            this.normalsAttribute = attribute;
            continue;
          case "OriginalTriangleIndices":
            this.originalTriangleIndicesAttribute = attribute;
            continue;
          case "OriginalVertexIndices":
            this.originalVertexIndicesAttribute = attribute;
            continue;
          case "PixelSize":
            this.pixelSizeAttribute = attribute;
            continue;
          case "SmoothingGroup":
            this.smoothingGroupAttribute = attribute;
            continue;
          case "Tangents":
            this.tangentsAttribute = attribute;
            continue;
          case "TextureCoordinates":
            this.textureCoordinatesAttributes.Add(((INamedList) attribute.AttributeData).Name, attribute);
            continue;
          case "TriangleIndices":
            this.triangleIndicesAttribute = attribute;
            continue;
          case "VertexCrease":
            this.vertexCreaseAttribute = attribute;
            continue;
          case "VertexLocks":
            this.vertexLocksAttribute = attribute;
            continue;
          case "VertexSet":
            this.vertexSetsAttributes.Add(((INamedList) attribute.AttributeData).Name, attribute);
            continue;
          case "VertexWeights":
            this.vertexWeightsAttribute = attribute;
            continue;
          default:
            continue;
        }
      }
    }

    public void Update()
    {
      this.Chunk.Update();
    }

    public void Validate()
    {
      if (this.Coordinates == null || this.Coordinates.Length == 0)
        this.ThrowSizeException("Coordinates", 0);
      if (this.TriangleIndices == null || this.TriangleIndices.Length == 0)
        this.ThrowSizeException("TriangleIndices", 0);
      if (this.MaterialIndices != null && this.MaterialIndices.Length != 0 && this.MaterialIndices.Length != this.TriangleIndices.Length)
        this.ThrowSizeException("MaterialIndices", this.MaterialIndices.Length);
      if (this.BoneWeights != null && this.BoneWeights.Length != 0 && this.BoneWeights.Length / ((TupleList<double>) this.boneWeightsAttribute.AttributeData).TupleSize != this.Coordinates.Length)
        this.ThrowSizeException("BoneWeights", this.BoneWeights.Length);
      if (this.BoneIndices != null && this.BoneIndices.Length != 0 && this.BoneIndices.Length / ((TupleList<double>) this.boneWeightsAttribute.AttributeData).TupleSize != this.Coordinates.Length)
        this.ThrowSizeException("BoneIndices", this.BoneIndices.Length);
      if (this.VertexLocks != null && this.VertexLocks.Length != 0 && this.VertexLocks.Length != this.Coordinates.Length)
        this.ThrowSizeException("VertexLocks", this.VertexLocks.Length);
      if (this.VertexWeights != null && this.VertexWeights.Length != 0 && this.VertexWeights.Length != this.Coordinates.Length)
        this.ThrowSizeException("VertexWeights", this.VertexWeights.Length);
      if (this.Normals != null && this.Normals.Length != 0 && this.Normals.Length != this.TriangleIndices.Length * 3)
        this.ThrowSizeException("Normals", this.Normals.Length);
      if (this.Tangents != null && this.Tangents.Length != 0 && this.Tangents.Length != this.TriangleIndices.Length * 3)
        this.ThrowSizeException("Tangents", this.Tangents.Length);
      if (this.Bitangents != null && this.Bitangents.Length != 0 && this.Bitangents.Length != this.TriangleIndices.Length * 3)
        this.ThrowSizeException("Bitangents", this.Bitangents.Length);
      foreach (string textureCoordinateSet in this.TextureCoordinateSets)
      {
        Vector2[] textureCoordinates = this.GetTextureCoordinates(textureCoordinateSet);
        if (textureCoordinates != null && textureCoordinates.Length != 0 && textureCoordinates.Length != this.TriangleIndices.Length * 3)
          this.ThrowSizeException(string.Format(CultureInfo.InvariantCulture, "TextureCoordinate {0", (object) textureCoordinateSet), textureCoordinates.Length);
      }
      foreach (string colorSet in this.ColorSets)
      {
        Vector4[] colors = this.GetColors(colorSet);
        if (colors != null && colors.Length != 0 && colors.Length != this.TriangleIndices.Length * 3)
          this.ThrowSizeException(string.Format(CultureInfo.InvariantCulture, "Colors {0", (object) colorSet), colors.Length);
      }
      foreach (string customField in this.CustomFieldSet)
      {
        byte[][] customFields = this.GetCustomFields(customField);
        if (customFields != null && customFields.Length != 0 && customFields.Length != this.TriangleIndices.Length * 3)
          this.ThrowSizeException(string.Format(CultureInfo.InvariantCulture, "CustomFields {0", (object) customField), customFields.Length);
      }
      foreach (string blendShape in this.BlendShapeSet)
      {
        Vector3[] blendShapes = this.GetBlendShapes(blendShape);
        if (blendShapes != null && blendShapes.Length != 0 && blendShapes.Length != this.TriangleIndices.Length * 3)
          this.ThrowSizeException(string.Format(CultureInfo.InvariantCulture, "BlendShapes {0", (object) blendShape), blendShapes.Length);
      }
    }

    public void Release()
    {
    }

    private void ThrowSizeException(string property, int size)
    {
      throw new Exception(string.Format(CultureInfo.InvariantCulture, "SFF exception: Invalid size {0} for {1} in MeshData", (object) size, (object) property));
    }
  }
}
