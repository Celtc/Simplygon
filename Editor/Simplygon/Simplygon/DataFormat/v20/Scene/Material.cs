﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.Scene.Material
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.DataType;
using System.Collections.Generic;

namespace Simplygon.DataFormat.v20.Scene
{
  internal class Material : ISDFObject
  {
    private Attribute idAttribute;
    private Attribute nameAttribute;
    private Attribute tangentSpaceNormalsAttribute;
    private Attribute materialModelAttribute;
    private Attribute normalMapTypeAttribute;

    public string Id
    {
      get
      {
        return this.idAttribute.GetStringData();
      }
      set
      {
        this.idAttribute.SetStringData(value);
      }
    }

    public string Name
    {
      get
      {
        return this.nameAttribute.GetStringData();
      }
      set
      {
        this.nameAttribute.SetStringData(value);
      }
    }

    public bool TangentSpaceNormals
    {
      get
      {
        return (bool) this.tangentSpaceNormalsAttribute.AttributeData;
      }
      set
      {
        this.tangentSpaceNormalsAttribute.SetData(DataTypeId.Bool, (object) value);
      }
    }

    public int MaterialModel
    {
      get
      {
        return (int) this.materialModelAttribute.AttributeData;
      }
      set
      {
        this.materialModelAttribute.SetData(DataTypeId.Int32, (object) value);
      }
    }

    public int NormalMapType
    {
      get
      {
        return (int) this.normalMapTypeAttribute.AttributeData;
      }
      set
      {
        this.normalMapTypeAttribute.SetData(DataTypeId.Int32, (object) value);
      }
    }

    public System.Collections.Generic.List<MaterialChannel> MaterialChannels { get; set; }

    public Chunk Chunk { get; set; }

    public Material()
    {
      this.Chunk = new Chunk();
      this.nameAttribute = this.Chunk.AddAttribute(nameof (Name), DataTypeId.String, (object) string.Empty);
      this.idAttribute = this.Chunk.AddAttribute(nameof (Id), DataTypeId.String, (object) string.Empty);
      this.tangentSpaceNormalsAttribute = this.Chunk.AddAttribute(nameof (TangentSpaceNormals), DataTypeId.Bool, (object) false);
      this.materialModelAttribute = this.Chunk.AddAttribute(nameof (MaterialModel), DataTypeId.Int32, (object) 0);
      this.normalMapTypeAttribute = this.Chunk.AddAttribute(nameof (NormalMapType), DataTypeId.Int32, (object) 0);
      this.MaterialChannels = new System.Collections.Generic.List<MaterialChannel>();
    }

    public ulong GetTotalSizeInBytes()
    {
      this.Update();
      return this.Chunk.GetTotalSizeInBytes();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Update();
      this.Chunk.Write(binaryStream);
    }

    public void Read(BinaryStream binaryStream)
    {
    }

    public void Read(Chunk chunk)
    {
      this.Chunk = chunk;
      foreach (Attribute attribute in this.Chunk.Attributes.AttributeList)
      {
        if (attribute.AttributeName == "Name" && attribute.DataTypeId == DataTypeId.String)
          this.nameAttribute = attribute;
        else if (attribute.AttributeName == "Id" && attribute.DataTypeId == DataTypeId.String)
          this.idAttribute = attribute;
        else if (attribute.AttributeName == "TangentSpaceNormals" && attribute.DataTypeId == DataTypeId.Bool)
        {
          this.tangentSpaceNormalsAttribute = attribute;
          this.NormalMapType = this.TangentSpaceNormals ? 4 : 2;
        }
        else if (attribute.AttributeName == "MaterialModel" && attribute.DataTypeId == DataTypeId.Int32)
          this.materialModelAttribute = attribute;
        else if (attribute.AttributeName == "NormalMapType" && attribute.DataTypeId == DataTypeId.Int32)
          this.normalMapTypeAttribute = attribute;
      }
      this.MaterialChannels.Clear();
      foreach (Chunk chunk1 in this.Chunk.Chunks.ChunkList)
      {
        if (chunk1.ChunkHeader.ChunkType == "MaterialChannel")
        {
          MaterialChannel materialChannel = new MaterialChannel();
          materialChannel.Read(chunk1);
          this.MaterialChannels.Add(materialChannel);
        }
      }
      this.Validate();
    }

    public void Update()
    {
      this.Validate();
      this.Chunk.ChunkHeader.ChunkType = nameof (Material);
      this.Chunk.Chunks.ChunkList.Clear();
      foreach (MaterialChannel materialChannel in this.MaterialChannels)
      {
        materialChannel.Update();
        this.Chunk.Chunks.ChunkList.Add(materialChannel.Chunk);
      }
      this.Chunk.Update();
    }

    public void Validate()
    {
      Util.ValidateGuid("Id", this.Id, nameof (Material), false);
    }

    public void Release()
    {
    }
  }
}
