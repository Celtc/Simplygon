﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.Scene.Node
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.DataType;
using System;

namespace Simplygon.DataFormat.v20.Scene
{
  internal class Node : ISDFObject
  {
    private Attribute idAttribute;
    private Attribute parentIdAttribute;
    private Attribute nameAttribute;
    private Attribute meshIdAttribute;
    private Attribute replancementIdAttribute;
    private Attribute isFrozenAttribute;
    private Attribute isHLODReplacementNodeAttribute;
    private Attribute hLODScreenSizeAttribute;
    private Attribute isCurrentReplacementAttribute;
    private Attribute localTransformAttribute;
    private Attribute globalLODIndicesAttribute;

    public string Id
    {
      get
      {
        return this.idAttribute.GetStringData();
      }
      set
      {
        this.idAttribute.SetStringData(value);
      }
    }

    public string ParentId
    {
      get
      {
        return this.parentIdAttribute.GetStringData();
      }
      set
      {
        this.parentIdAttribute.SetStringData(value);
      }
    }

    public string Name
    {
      get
      {
        return this.nameAttribute.GetStringData();
      }
      set
      {
        this.nameAttribute.SetStringData(value);
      }
    }

    public string MeshId
    {
      get
      {
        return this.meshIdAttribute.GetStringData();
      }
      set
      {
        this.meshIdAttribute.SetStringData(value);
      }
    }

    public bool IsFrozen
    {
      get
      {
        return (bool) this.isFrozenAttribute.AttributeData;
      }
      set
      {
        this.isFrozenAttribute.SetData(DataTypeId.Bool, (object) value);
      }
    }

    public bool IsHLODReplacementNode
    {
      get
      {
        return (bool) this.isHLODReplacementNodeAttribute.AttributeData;
      }
      set
      {
        this.isHLODReplacementNodeAttribute.SetData(DataTypeId.Bool, (object) value);
      }
    }

    public uint HLODOnScreenSize
    {
      get
      {
        return (uint) this.hLODScreenSizeAttribute.AttributeData;
      }
      set
      {
        this.hLODScreenSizeAttribute.SetData(DataTypeId.UInt32, (object) value);
      }
    }

    public bool IsCurrentReplacement
    {
      get
      {
        return (bool) this.isCurrentReplacementAttribute.AttributeData;
      }
      set
      {
        this.isCurrentReplacementAttribute.SetData(DataTypeId.Bool, (object) value);
      }
    }

    public string ReplacementId
    {
      get
      {
        return this.replancementIdAttribute.GetStringData();
      }
      set
      {
        this.replancementIdAttribute.SetStringData(value);
      }
    }

    public Matrix4x4 LocalTransform
    {
      get
      {
        return (Matrix4x4) this.localTransformAttribute.AttributeData;
      }
      set
      {
        this.localTransformAttribute.SetData(DataTypeId.Matrix4x4, (object) value);
      }
    }

    public int[] GlobalLODIndices
    {
      get
      {
        if (this.globalLODIndicesAttribute != null)
          return ((List<int>) this.globalLODIndicesAttribute.AttributeData).Items;
        return (int[]) null;
      }
    }

    public Chunk Chunk { get; set; }

    private void InitializeDefaultValues()
    {
      this.Chunk = new Chunk();
      this.idAttribute = this.Chunk.AddAttribute("Id", DataTypeId.String, (object) Guid.NewGuid().ToString());
      this.nameAttribute = this.Chunk.AddAttribute("Name", DataTypeId.String, (object) string.Empty);
      this.parentIdAttribute = this.Chunk.AddAttribute("ParentId", DataTypeId.String, (object) string.Empty);
      this.meshIdAttribute = this.Chunk.AddAttribute("MeshId", DataTypeId.String, (object) string.Empty);
      this.replancementIdAttribute = this.Chunk.AddAttribute("ReplacementId", DataTypeId.String, (object) string.Empty);
      this.isFrozenAttribute = this.Chunk.AddAttribute("IsFrozen", DataTypeId.Bool, (object) false);
      this.isHLODReplacementNodeAttribute = this.Chunk.AddAttribute("IsHLODReplacementNode", DataTypeId.Bool, (object) false);
      this.hLODScreenSizeAttribute = this.Chunk.AddAttribute("HLODOnScreenSize", DataTypeId.UInt32, (object) 0U);
      this.isCurrentReplacementAttribute = this.Chunk.AddAttribute("IsCurrentReplacement", DataTypeId.Bool, (object) false);
      this.localTransformAttribute = this.Chunk.AddAttribute("LocalTransform", DataTypeId.Bool, (object) Matrix4x4.Identity());
    }

    public Node()
    {
      this.InitializeDefaultValues();
    }

    public Node(string id)
    {
      this.InitializeDefaultValues();
      this.Id = id;
    }

    public ulong GetTotalSizeInBytes()
    {
      this.Update();
      return this.Chunk.GetTotalSizeInBytes();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Update();
      this.Chunk.Write(binaryStream);
    }

    public void Read(BinaryStream binaryStream)
    {
    }

    public void Read(Chunk chunk)
    {
      this.Chunk = chunk;
      foreach (Attribute attribute in this.Chunk.Attributes.AttributeList)
      {
        if (attribute.AttributeName == "Name" && attribute.DataTypeId == DataTypeId.String)
          this.nameAttribute = attribute;
        else if (attribute.AttributeName == "Id" && attribute.DataTypeId == DataTypeId.String)
          this.idAttribute = attribute;
        else if (attribute.AttributeName == "LocalTransform" && attribute.DataTypeId == DataTypeId.Matrix4x4)
          this.localTransformAttribute = attribute;
        else if (attribute.AttributeName == "MeshId" && attribute.DataTypeId == DataTypeId.String)
          this.meshIdAttribute = attribute;
        else if (attribute.AttributeName == "ReplacementId" && attribute.DataTypeId == DataTypeId.String)
          this.replancementIdAttribute = attribute;
        else if (attribute.AttributeName == "ParentId" && attribute.DataTypeId == DataTypeId.String)
          this.parentIdAttribute = attribute;
        else if (attribute.AttributeName == "IsHLODReplacementNode" && attribute.DataTypeId == DataTypeId.Bool)
          this.isHLODReplacementNodeAttribute = attribute;
        else if (attribute.AttributeName == "HLODOnScreenSize" && attribute.DataTypeId == DataTypeId.UInt32)
          this.hLODScreenSizeAttribute = attribute;
        else if (attribute.AttributeName == "IsCurrentReplacement" && attribute.DataTypeId == DataTypeId.Bool)
          this.isCurrentReplacementAttribute = attribute;
        else if (attribute.AttributeName == "IsFrozen" && attribute.DataTypeId == DataTypeId.Bool)
          this.isFrozenAttribute = attribute;
        else if (attribute.AttributeName == "GlobalLODIndices" && attribute.DataTypeId == DataTypeId.List)
          this.globalLODIndicesAttribute = attribute;
      }
      this.Validate();
    }

    public void Update()
    {
      this.Validate();
      this.Chunk.ChunkHeader.ChunkType = nameof (Node);
      this.Chunk.Update();
    }

    public void Validate()
    {
      Util.ValidateGuid("Id", this.Id, nameof (Node), false);
      Util.ValidateGuid("ParentId", this.ParentId, nameof (Node), true);
      Util.ValidateGuid("MeshId", this.MeshId, nameof (Node), true);
      Util.ValidateGuid("ReplacementId", this.MeshId, nameof (Node), true);
    }

    public void Release()
    {
    }

    public void SetGlobalLODIndices(int[] globalLODIndices)
    {
      if (this.globalLODIndicesAttribute == null)
        this.globalLODIndicesAttribute = this.Chunk.AddAttribute("GlobalLODIndices", DataTypeId.List, (object) new List<int>(DataTypeId.Int32, globalLODIndices));
      else
        this.globalLODIndicesAttribute.SetData(DataTypeId.List, (object) new List<int>(DataTypeId.Int32, globalLODIndices));
    }
  }
}
