﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.Scene.Mesh
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.DataType;
using System;
using System.Collections.Generic;

namespace Simplygon.DataFormat.v20.Scene
{
  internal class Mesh : ISDFObject
  {
    private Attribute nameAttribute;
    private Attribute idAttribute;
    private Attribute materialIdListAttribute;
    private Attribute boneIdListAttribute;

    public string Name
    {
      get
      {
        return this.nameAttribute.GetStringData();
      }
      set
      {
        this.nameAttribute.SetStringData(value);
      }
    }

    public string Id
    {
      get
      {
        return this.idAttribute.GetStringData();
      }
      set
      {
        this.idAttribute.SetStringData(value);
      }
    }

    public System.Collections.Generic.List<MeshData> MeshDatas { get; set; }

    public string[] MaterialIds
    {
      get
      {
        if (this.materialIdListAttribute != null)
          return ((DataType.List<string>) this.materialIdListAttribute.AttributeData).Items;
        return (string[]) null;
      }
    }

    public string[] BoneIds
    {
      get
      {
        if (this.boneIdListAttribute != null)
          return ((DataType.List<string>) this.boneIdListAttribute.AttributeData).Items;
        return (string[]) null;
      }
    }

    public Chunk Chunk { get; set; }

    private void InitializeDefaultValues()
    {
      this.Chunk = new Chunk();
      this.nameAttribute = this.Chunk.AddAttribute("Name", DataTypeId.String, (object) string.Empty);
      this.idAttribute = this.Chunk.AddAttribute("Id", DataTypeId.String, (object) string.Empty);
      this.Id = Guid.NewGuid().ToString();
      this.MeshDatas = new System.Collections.Generic.List<MeshData>();
    }

    public Mesh()
    {
      this.InitializeDefaultValues();
    }

    public Mesh(string id)
    {
      this.InitializeDefaultValues();
      this.Id = id;
    }

    public void SetMaterialIds(string[] materialIds)
    {
      if (this.materialIdListAttribute == null)
        this.materialIdListAttribute = this.Chunk.AddAttribute("MaterialIds", DataTypeId.List, (object) new DataType.List<string>(DataTypeId.String, materialIds));
      else
        this.materialIdListAttribute.SetData(DataTypeId.List, (object) new DataType.List<string>(DataTypeId.String, materialIds));
    }

    public void SetBoneIds(string[] boneIds)
    {
      if (this.boneIdListAttribute == null)
        this.boneIdListAttribute = this.Chunk.AddAttribute("BoneIds", DataTypeId.List, (object) new DataType.List<string>(DataTypeId.String, boneIds));
      else
        this.boneIdListAttribute.SetData(DataTypeId.List, (object) new DataType.List<string>(DataTypeId.String, boneIds));
    }

    public ulong GetTotalSizeInBytes()
    {
      this.Update();
      return this.Chunk.GetTotalSizeInBytes();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Update();
      this.Chunk.Write(binaryStream);
    }

    public void Read(BinaryStream binaryStream)
    {
    }

    public void Read(Chunk chunk)
    {
      this.Chunk = chunk;
      this.materialIdListAttribute = (Attribute) null;
      this.boneIdListAttribute = (Attribute) null;
      foreach (Attribute attribute in this.Chunk.Attributes.AttributeList)
      {
        if (attribute.AttributeName == "Name" && attribute.DataTypeId == DataTypeId.String)
          this.Name = (string) attribute.AttributeData;
        else if (attribute.AttributeName == "Id" && attribute.DataTypeId == DataTypeId.String)
          this.Id = (string) attribute.AttributeData;
        else if (attribute.AttributeName == "MaterialIds" && attribute.DataTypeId == DataTypeId.List)
          this.materialIdListAttribute = attribute;
        else if (attribute.AttributeName == "BoneIds" && attribute.DataTypeId == DataTypeId.List)
          this.boneIdListAttribute = attribute;
      }
      foreach (Chunk chunk1 in this.Chunk.Chunks.ChunkList)
      {
        if (chunk1.ChunkHeader.ChunkType == "MeshData")
        {
          MeshData meshData = new MeshData();
          meshData.Read(chunk1);
          this.MeshDatas.Add(meshData);
        }
      }
      this.Validate();
    }

    public void Update()
    {
      this.Validate();
      this.Chunk.ChunkHeader.ChunkType = nameof (Mesh);
      this.Chunk.Chunks.ChunkList.Clear();
      foreach (MeshData meshData in this.MeshDatas)
      {
        meshData.Update();
        this.Chunk.Chunks.ChunkList.Add(meshData.Chunk);
      }
      this.Chunk.Update();
    }

    public void Validate()
    {
      Util.ValidateGuid("Id", this.Id, nameof (Mesh), false);
    }

    public void Release()
    {
    }
  }
}
