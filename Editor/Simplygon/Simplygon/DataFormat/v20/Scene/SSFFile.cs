﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.Scene.SSFFile
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.DataType;
using Simplygon.DataFormat.v20.Interfaces;
using Simplygon.Scene.Common;
using Simplygon.Scene.Common.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Simplygon.DataFormat.v20.Scene
{
  internal class SSFFile
  {
    private Attribute worldOrientationAttribute = new Attribute(nameof (WorldOrientation), DataTypeId.Int32, (object) WorldOrientation.Y_UP);
    private Attribute coordinateSystemAttribute = new Attribute(nameof (CoordinateSystem), DataTypeId.Int32, (object) CoordinateSystem.Left);
    private Dictionary<string, Attribute> proxyGroupSetsAttributes = new Dictionary<string, Attribute>();
    private Dictionary<string, Attribute> occluderGroupSetsAttributes = new Dictionary<string, Attribute>();
    private Dictionary<NameAndId, Attribute> selectionGroupSetsAttributes = new Dictionary<NameAndId, Attribute>();
    private Dictionary<NameAndId, Attribute> cuttingPlanesAttributes = new Dictionary<NameAndId, Attribute>();

    public FileHeader FileHeader { get; set; }

    public NodeTable NodeTable { get; set; }

    public MeshTable MeshTable { get; set; }

    public MaterialTable MaterialTable { get; set; }

    public TextureTable TextureTable { get; set; }

    public WorldOrientation WorldOrientation
    {
      get
      {
        return (WorldOrientation) this.worldOrientationAttribute.AttributeData;
      }
      set
      {
        this.worldOrientationAttribute.SetData(DataTypeId.Int32, (object) value);
      }
    }

    public CoordinateSystem CoordinateSystem
    {
      get
      {
        return (CoordinateSystem) this.coordinateSystemAttribute.AttributeData;
      }
      set
      {
        this.coordinateSystemAttribute.SetData(DataTypeId.Int32, (object) value);
      }
    }

    public string[] ProxyGroupSets
    {
      get
      {
        return this.proxyGroupSetsAttributes.Keys.ToArray<string>();
      }
    }

    public string[] OccluderGroupSets
    {
      get
      {
        return this.occluderGroupSetsAttributes.Keys.ToArray<string>();
      }
    }

    public NameAndId[] SelectionGroupSets
    {
      get
      {
        return this.selectionGroupSetsAttributes.Keys.ToArray<NameAndId>();
      }
    }

    public NameAndId[] CuttingPlanes
    {
      get
      {
        return this.cuttingPlanesAttributes.Keys.ToArray<NameAndId>();
      }
    }

    public string[] GetProxyGroupSet(string name)
    {
      return ((DataType.List<string>)this.proxyGroupSetsAttributes[name].AttributeData).Items;
    }

    public string[] GetOccluderGroupSet(string name)
    {
      return ((DataType.List<string>) this.occluderGroupSetsAttributes[name].AttributeData).Items;
    }

    public string[] GetSelectionGroupSetById(string id)
    {
      return ((DataType.List<string>) this.selectionGroupSetsAttributes.Where<KeyValuePair<NameAndId, Attribute>>((Func<KeyValuePair<NameAndId, Attribute>, bool>) (kvp => kvp.Key.Id == id)).SingleOrDefault<KeyValuePair<NameAndId, Attribute>>().Value.AttributeData).Items;
    }

    public string[] GetSelectionGroupSetByName(string name)
    {
      return ((DataType.List<string>) this.selectionGroupSetsAttributes.Where<KeyValuePair<NameAndId, Attribute>>((Func<KeyValuePair<NameAndId, Attribute>, bool>) (kvp => kvp.Key.Name == name)).SingleOrDefault<KeyValuePair<NameAndId, Attribute>>().Value.AttributeData).Items;
    }

    public Plane[] GetCuttingPlanesById(string id)
    {
      return ((DataType.List<Plane>) this.cuttingPlanesAttributes.Where<KeyValuePair<NameAndId, Attribute>>((Func<KeyValuePair<NameAndId, Attribute>, bool>) (kvp => kvp.Key.Id == id)).SingleOrDefault<KeyValuePair<NameAndId, Attribute>>().Value.AttributeData).Items;
    }

    public SSFFile()
    {
      this.FileHeader = new FileHeader();
      this.NodeTable = new NodeTable();
      this.MeshTable = new MeshTable();
      this.MaterialTable = new MaterialTable();
      this.TextureTable = new TextureTable();
    }

    public SSFFile(string toolName, int toolMajorVersion, int toolMinorVersion, int toolBuildVersion, byte[] formatSignature)
    {
      this.FileHeader = new FileHeader();
      this.NodeTable = new NodeTable();
      this.MeshTable = new MeshTable();
      this.MaterialTable = new MaterialTable();
      this.TextureTable = new TextureTable();
      this.FileHeader.FileVersion.MajorVersion = (byte) 2;
      this.FileHeader.FileVersion.MinorVersion = (byte) 0;
      this.FileHeader.Tool.Name = toolName;
      this.FileHeader.Tool.MajorVersion = (byte) toolMajorVersion;
      this.FileHeader.Tool.MinorVersion = (byte) toolMinorVersion;
      this.FileHeader.Tool.BuildVersion = (ushort) toolBuildVersion;
      if (formatSignature.Length != 8)
        return;
      this.FileHeader.FormatSignature.Signature = formatSignature;
    }

    public void SetProxyGroupSet(string name, string[] proxyGroupSet)
    {
      if (!this.proxyGroupSetsAttributes.ContainsKey(name))
      {
        Attribute attribute = new Attribute("ProxyGroupSet");
        attribute.SetData(DataTypeId.NamedList, (object) new NamedList<string>(name, DataTypeId.String, proxyGroupSet));
        this.proxyGroupSetsAttributes.Add(name, attribute);
      }
      else
        this.proxyGroupSetsAttributes[name].SetData(DataTypeId.NamedList, (object) new NamedList<string>(name, DataTypeId.String, proxyGroupSet));
    }

    public void SetOccluderGroupSet(string name, string[] occluderGroupSet)
    {
      if (!this.occluderGroupSetsAttributes.ContainsKey(name))
      {
        Attribute attribute = new Attribute("OccluderGroupSet");
        attribute.SetData(DataTypeId.NamedList, (object) new NamedList<string>(name, DataTypeId.String, occluderGroupSet));
        this.occluderGroupSetsAttributes.Add(name, attribute);
      }
      else
        this.occluderGroupSetsAttributes[name].SetData(DataTypeId.NamedList, (object) new NamedList<string>(name, DataTypeId.String, occluderGroupSet));
    }

    public void SetSelectionGroupSet(string id, string name, string[] selectionGroupSet)
    {
      KeyValuePair<NameAndId, Attribute> keyValuePair = this.selectionGroupSetsAttributes.Where<KeyValuePair<NameAndId, Attribute>>((Func<KeyValuePair<NameAndId, Attribute>, bool>) (kvp => kvp.Key.Id == id)).SingleOrDefault<KeyValuePair<NameAndId, Attribute>>();
      if (keyValuePair.Key == null)
      {
        Attribute attribute = new Attribute("SelectionGroupSet");
        attribute.SetData(DataTypeId.NamedIdList, (object) new NamedIdList<string>(name, id, DataTypeId.String, selectionGroupSet));
        this.selectionGroupSetsAttributes.Add(new NameAndId(id, name), attribute);
      }
      else
        keyValuePair.Value.SetData(DataTypeId.NamedIdList, (object) new NamedIdList<string>(name, id, DataTypeId.String, selectionGroupSet));
    }

    public void AddCuttingPlane(string id, string name, Plane cuttingPlane)
    {
      KeyValuePair<NameAndId, Attribute> keyValuePair = this.cuttingPlanesAttributes.Where<KeyValuePair<NameAndId, Attribute>>((Func<KeyValuePair<NameAndId, Attribute>, bool>) (kvp => kvp.Key.Id == id)).SingleOrDefault<KeyValuePair<NameAndId, Attribute>>();
      if (keyValuePair.Key == null)
      {
        Attribute attribute = new Attribute("CuttingPlane");
        attribute.SetData(DataTypeId.NamedIdList, (object) new NamedIdList<Plane>(name, id, DataTypeId.Plane, new Plane[1]
        {
          cuttingPlane
        }));
        this.cuttingPlanesAttributes.Add(new NameAndId(id, name), attribute);
      }
      else
        keyValuePair.Value.SetData(DataTypeId.NamedIdList, (object) new NamedIdList<Plane>(name, id, DataTypeId.Plane, new Plane[1]
        {
          cuttingPlane
        }));
    }

    public void Read(string fileName)
    {
      using (FileStream fileStream = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
      {
        using (BinaryReader binaryReader = new BinaryReader((Stream) fileStream))
        {
          BinaryStream binaryStream = new BinaryStream(binaryReader);
          binaryStream.ComputeCRC = true;
          this.FileHeader.Read(binaryStream);
          Attributes attributes = new Attributes();
          attributes.Read(binaryStream);
          this.proxyGroupSetsAttributes.Clear();
          this.occluderGroupSetsAttributes.Clear();
          this.selectionGroupSetsAttributes.Clear();
          foreach (Attribute attribute in attributes.AttributeList)
          {
            string attributeName = attribute.AttributeName;
            if (!(attributeName == "WorldOrientation"))
            {
              if (!(attributeName == "CoordinateSystem"))
              {
                if (!(attributeName == "ProxyGroupSet"))
                {
                  if (!(attributeName == "OccluderGroupSet"))
                  {
                    if (!(attributeName == "SelectionGroupSet"))
                    {
                      if (attributeName == "CuttingPlane")
                        this.cuttingPlanesAttributes.Add(new NameAndId(((INamedIdList) attribute.AttributeData).ID, ((INamedIdList) attribute.AttributeData).Name), attribute);
                    }
                    else
                      this.selectionGroupSetsAttributes.Add(new NameAndId(((INamedIdList) attribute.AttributeData).ID, ((INamedIdList) attribute.AttributeData).Name), attribute);
                  }
                  else
                    this.occluderGroupSetsAttributes.Add(((INamedList) attribute.AttributeData).Name, attribute);
                }
                else
                  this.proxyGroupSetsAttributes.Add(((INamedList) attribute.AttributeData).Name, attribute);
              }
              else
                this.coordinateSystemAttribute = attribute;
            }
            else
              this.worldOrientationAttribute = attribute;
          }
          Chunks chunks = new Chunks();
          chunks.Read(binaryStream);
          foreach (Chunk chunk in chunks.ChunkList)
          {
            if (chunk.ChunkHeader.ChunkType == "NodeTable")
            {
              NodeTable nodeTable = new NodeTable();
              nodeTable.Read(chunk);
              this.NodeTable = nodeTable;
            }
            else if (chunk.ChunkHeader.ChunkType == "MeshTable")
            {
              MeshTable meshTable = new MeshTable();
              meshTable.Read(chunk);
              this.MeshTable = meshTable;
            }
            else if (chunk.ChunkHeader.ChunkType == "MaterialTable")
            {
              MaterialTable materialTable = new MaterialTable();
              materialTable.Read(chunk);
              this.MaterialTable = materialTable;
            }
            else if (chunk.ChunkHeader.ChunkType == "TextureTable")
            {
              TextureTable textureTable = new TextureTable();
              textureTable.Read(chunk);
              this.TextureTable = textureTable;
            }
          }
          binaryStream.ComputeCRC = false;
          int num = (int) binaryStream.ReadUInt32();
        }
      }
    }

    public void Write(string fileName)
    {
      using (FileStream fileStream = File.Open(fileName, FileMode.Create))
      {
        using (BinaryWriter binaryWriter = new BinaryWriter((Stream) fileStream))
        {
          BinaryStream binaryStream = new BinaryStream(binaryWriter);
          binaryStream.ComputeCRC = true;
          this.FileHeader.Write(binaryStream);
          Attributes attributes = new Attributes();
          attributes.AttributeList.Add(this.worldOrientationAttribute);
          attributes.AttributeList.Add(this.coordinateSystemAttribute);
          foreach (KeyValuePair<string, Attribute> groupSetsAttribute in this.proxyGroupSetsAttributes)
            attributes.AttributeList.Add(groupSetsAttribute.Value);
          foreach (KeyValuePair<string, Attribute> groupSetsAttribute in this.occluderGroupSetsAttributes)
            attributes.AttributeList.Add(groupSetsAttribute.Value);
          foreach (KeyValuePair<NameAndId, Attribute> groupSetsAttribute in this.selectionGroupSetsAttributes)
            attributes.AttributeList.Add(groupSetsAttribute.Value);
          foreach (KeyValuePair<NameAndId, Attribute> cuttingPlanesAttribute in this.cuttingPlanesAttributes)
            attributes.AttributeList.Add(cuttingPlanesAttribute.Value);
          attributes.Update();
          attributes.Write(binaryStream);
          Chunks chunks = new Chunks();
          this.NodeTable.Update();
          chunks.ChunkList.Add(this.NodeTable.Chunk);
          this.MeshTable.Update();
          chunks.ChunkList.Add(this.MeshTable.Chunk);
          this.MaterialTable.Update();
          chunks.ChunkList.Add(this.MaterialTable.Chunk);
          this.TextureTable.Update();
          chunks.ChunkList.Add(this.TextureTable.Chunk);
          chunks.Update();
          chunks.Write(binaryStream);
          chunks.Release();
          binaryStream.ComputeCRC = false;
          binaryStream.WriteUInt32(binaryStream.CRC);
        }
      }
    }
  }
}
