﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.Scene.Texture
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.DataType;
using Simplygon.Scene.Common.Enums;

namespace Simplygon.DataFormat.v20.Scene
{
  internal class Texture : ISDFObject
  {
    private Attribute nameAttribute;
    private Attribute idAttribute;
    private Attribute pathAttribute;
    private Attribute colorSpaceAttribute;

    public string Id
    {
      get
      {
        return this.idAttribute.GetStringData();
      }
      set
      {
        this.idAttribute.SetStringData(value);
      }
    }

    public string Name
    {
      get
      {
        return this.nameAttribute.GetStringData();
      }
      set
      {
        this.nameAttribute.SetStringData(value);
      }
    }

    public string Path
    {
      get
      {
        return this.pathAttribute.GetStringData();
      }
      set
      {
        this.pathAttribute.SetStringData(value);
      }
    }

    public ColorSpaceType ColorSpace
    {
      get
      {
        return (ColorSpaceType) this.colorSpaceAttribute.AttributeData;
      }
      set
      {
        this.colorSpaceAttribute.SetData(DataTypeId.Int32, (object) value);
      }
    }

    public Chunk Chunk { get; set; }

    public Texture()
    {
      this.Chunk = new Chunk();
      this.nameAttribute = this.Chunk.AddAttribute(nameof (Name), DataTypeId.String, (object) string.Empty);
      this.idAttribute = this.Chunk.AddAttribute(nameof (Id), DataTypeId.String, (object) string.Empty);
      this.pathAttribute = this.Chunk.AddAttribute(nameof (Path), DataTypeId.String, (object) string.Empty);
      this.colorSpaceAttribute = this.Chunk.AddAttribute(nameof (ColorSpace), DataTypeId.Int32, (object) 0);
    }

    public ulong GetTotalSizeInBytes()
    {
      this.Update();
      return this.Chunk.GetTotalSizeInBytes();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Update();
      this.Chunk.Write(binaryStream);
    }

    public void Read(BinaryStream binaryStream)
    {
    }

    public void Read(Chunk chunk)
    {
      this.Chunk = chunk;
      foreach (Attribute attribute in this.Chunk.Attributes.AttributeList)
      {
        if (attribute.AttributeName == "Name" && attribute.DataTypeId == DataTypeId.String)
          this.nameAttribute = attribute;
        else if (attribute.AttributeName == "Id" && attribute.DataTypeId == DataTypeId.String)
          this.idAttribute = attribute;
        else if (attribute.AttributeName == "Path" && attribute.DataTypeId == DataTypeId.String)
          this.pathAttribute = attribute;
        else if (attribute.AttributeName == "ColorSpace" && attribute.DataTypeId == DataTypeId.Int32)
          this.colorSpaceAttribute = attribute;
      }
      this.Validate();
    }

    public void Update()
    {
      this.Validate();
      this.Chunk.ChunkHeader.ChunkType = nameof (Texture);
      this.Chunk.Update();
    }

    public void Validate()
    {
      Util.ValidateGuid("Id", this.Id, nameof (Texture), false);
    }

    public void Release()
    {
    }
  }
}
