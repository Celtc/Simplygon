﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.Scene.TextureTable
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.DataType;
using System.Collections.Generic;

namespace Simplygon.DataFormat.v20.Scene
{
  internal class TextureTable : ISDFObject
  {
    private Attribute texturesDirectoryAttribute;

    public string TexturesDirectory
    {
      get
      {
        return this.texturesDirectoryAttribute.GetStringData();
      }
      set
      {
        this.texturesDirectoryAttribute.SetStringData(value);
      }
    }

    public System.Collections.Generic.List<Texture> Textures { get; set; }

    public Chunk Chunk { get; set; }

    public TextureTable()
    {
      this.Chunk = new Chunk();
      this.texturesDirectoryAttribute = this.Chunk.AddAttribute(nameof (TexturesDirectory), DataTypeId.String, (object) string.Empty);
      this.Textures = new System.Collections.Generic.List<Texture>();
    }

    public ulong GetTotalSizeInBytes()
    {
      this.Update();
      return this.Chunk.GetTotalSizeInBytes();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Update();
      this.Chunk.Write(binaryStream);
    }

    public void Read(BinaryStream binaryStream)
    {
    }

    public void Read(Chunk chunk)
    {
      this.Chunk = chunk;
      foreach (Attribute attribute in this.Chunk.Attributes.AttributeList)
      {
        if (attribute.AttributeName == "TexturesDirectory" && attribute.DataTypeId == DataTypeId.String)
          this.texturesDirectoryAttribute = attribute;
      }
      foreach (Chunk chunk1 in this.Chunk.Chunks.ChunkList)
      {
        if (chunk1.ChunkHeader.ChunkType == "Texture")
        {
          Texture texture = new Texture();
          texture.Read(chunk1);
          this.Textures.Add(texture);
        }
      }
      this.Validate();
    }

    public void Update()
    {
      this.Validate();
      this.Chunk.ChunkHeader.ChunkType = nameof (TextureTable);
      this.Chunk.Chunks.ChunkList.Clear();
      foreach (Texture texture in this.Textures)
      {
        texture.Update();
        this.Chunk.Chunks.ChunkList.Add(texture.Chunk);
      }
      this.Chunk.Update();
    }

    public void Validate()
    {
      foreach (Texture texture in this.Textures)
        texture.Validate();
    }

    public void Release()
    {
    }
  }
}
