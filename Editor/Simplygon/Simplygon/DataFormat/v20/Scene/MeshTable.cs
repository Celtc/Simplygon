﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.Scene.MeshTable
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.Collections.Generic;

namespace Simplygon.DataFormat.v20.Scene
{
  internal class MeshTable : ISDFObject
  {
    public List<Mesh> Meshes { get; set; }

    public Chunk Chunk { get; set; }

    public MeshTable()
    {
      this.Chunk = new Chunk();
      this.Meshes = new List<Mesh>();
    }

    public ulong GetTotalSizeInBytes()
    {
      this.Update();
      return this.Chunk.GetTotalSizeInBytes();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Update();
      this.Chunk.Write(binaryStream);
    }

    public void Read(BinaryStream binaryStream)
    {
    }

    public void Read(Chunk chunk)
    {
      this.Chunk = chunk;
      foreach (Chunk chunk1 in this.Chunk.Chunks.ChunkList)
      {
        if (chunk1.ChunkHeader.ChunkType == "Mesh")
        {
          Mesh mesh = new Mesh();
          mesh.Read(chunk1);
          this.Meshes.Add(mesh);
        }
      }
      this.Validate();
    }

    public void Update()
    {
      this.Validate();
      this.Chunk.ChunkHeader.ChunkType = nameof (MeshTable);
      this.Chunk.Attributes.AttributeList.Clear();
      this.Chunk.Chunks.ChunkList.Clear();
      foreach (Mesh mesh in this.Meshes)
      {
        mesh.Update();
        this.Chunk.Chunks.ChunkList.Add(mesh.Chunk);
      }
      this.Chunk.Update();
    }

    public void Validate()
    {
      foreach (Mesh mesh in this.Meshes)
        mesh.Validate();
    }

    public void Release()
    {
    }
  }
}
