﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.v20.Scene.MaterialChannelTextureDescriptor
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.DataType;

namespace Simplygon.DataFormat.v20.Scene
{
  internal class MaterialChannelTextureDescriptor : ISDFObject
  {
    private Attribute textureIdAttribute;
    private Attribute texCoordSetAttribute;

    public string TextureID
    {
      get
      {
        return this.textureIdAttribute.GetStringData();
      }
      set
      {
        this.textureIdAttribute.SetStringData(value);
      }
    }

    public string TexCoordSet
    {
      get
      {
        return this.texCoordSetAttribute.GetStringData();
      }
      set
      {
        this.texCoordSetAttribute.SetStringData(value);
      }
    }

    public Chunk Chunk { get; set; }

    public MaterialChannelTextureDescriptor()
    {
      this.Chunk = new Chunk();
      this.textureIdAttribute = this.Chunk.AddAttribute(nameof (TextureID), DataTypeId.String, (object) string.Empty);
      this.texCoordSetAttribute = this.Chunk.AddAttribute(nameof (TexCoordSet), DataTypeId.String, (object) string.Empty);
    }

    public ulong GetTotalSizeInBytes()
    {
      this.Update();
      return this.Chunk.GetTotalSizeInBytes();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.Update();
      this.Chunk.Write(binaryStream);
    }

    public void Read(BinaryStream binaryStream)
    {
    }

    public void Read(Chunk chunk)
    {
      this.Chunk = chunk;
      foreach (Attribute attribute in this.Chunk.Attributes.AttributeList)
      {
        if (attribute.AttributeName == "TextureID" && attribute.DataTypeId == DataTypeId.String)
          this.textureIdAttribute = attribute;
        else if (attribute.AttributeName == "TexCoordSet" && attribute.DataTypeId == DataTypeId.String)
          this.texCoordSetAttribute = attribute;
      }
      this.Validate();
    }

    public void Update()
    {
      this.Validate();
      this.Chunk.ChunkHeader.ChunkType = nameof (MaterialChannelTextureDescriptor);
      this.Chunk.Chunks.ChunkList.Clear();
      this.Chunk.Update();
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
