﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.FormatSignature
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.DataFormat
{
  internal class FormatSignature : ISDFObject
  {
    public byte[] Signature { get; set; }

    public FormatSignature()
    {
      this.Signature = new byte[8]
      {
        byte.MaxValue,
        byte.MaxValue,
        byte.MaxValue,
        byte.MaxValue,
        byte.MaxValue,
        byte.MaxValue,
        byte.MaxValue,
        byte.MaxValue
      };
    }

    public ulong GetTotalSizeInBytes()
    {
      return (ulong) this.Signature.Length;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.Signature = binaryStream.ReadBytes(8);
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteBytes(this.Signature);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
