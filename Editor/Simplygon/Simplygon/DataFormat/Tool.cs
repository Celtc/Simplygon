﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.Tool
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.DataFormat.v20.DataType;

namespace Simplygon.DataFormat
{
  internal class Tool : ISDFObject
  {
    public string Name { get; set; }

    public byte MajorVersion { get; set; }

    public byte MinorVersion { get; set; }

    public ushort BuildVersion { get; set; }

    public Tool()
    {
      this.Name = "Unknown";
    }

    public ulong GetTotalSizeInBytes()
    {
      return (ulong) ((long) Util.GetStringSizeInBytes(this.Name) + 1L + 1L) + 2UL;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.Name = binaryStream.ReadString();
      this.MajorVersion = binaryStream.ReadUInt8();
      this.MinorVersion = binaryStream.ReadUInt8();
      this.BuildVersion = binaryStream.ReadUInt16();
    }

    public void Write(BinaryStream binaryStream)
    {
      binaryStream.WriteString(this.Name);
      binaryStream.WriteUInt8(this.MajorVersion);
      binaryStream.WriteUInt8(this.MinorVersion);
      binaryStream.WriteUInt16(this.BuildVersion);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }
  }
}
