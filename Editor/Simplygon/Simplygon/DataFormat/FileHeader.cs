﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.DataFormat.FileHeader
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.IO;

namespace Simplygon.DataFormat
{
  internal class FileHeader : ISDFObject
  {
    public FileSignature FileSignature { get; protected set; }

    public FileVersion FileVersion { get; protected set; }

    public Tool Tool { get; protected set; }

    public FormatSignature FormatSignature { get; protected set; }

    public ulong AttributesOffset { get; protected set; }

    public ulong ChunksOffset { get; protected set; }

    public FileHeader()
    {
      this.FileSignature = new FileSignature();
      this.FileVersion = new FileVersion();
      this.Tool = new Tool();
      this.FormatSignature = new FormatSignature();
      this.AttributesOffset = 0UL;
      this.ChunksOffset = 0UL;
    }

    public ulong GetTotalSizeInBytes()
    {
      return (ulong) ((long) this.FileSignature.GetTotalSizeInBytes() + (long) this.FileVersion.GetTotalSizeInBytes() + (long) this.Tool.GetTotalSizeInBytes() + (long) this.FormatSignature.GetTotalSizeInBytes() + 8L) + 8UL;
    }

    public void Read(BinaryStream binaryStream)
    {
      this.FileSignature = new FileSignature();
      this.FileSignature.Read(binaryStream);
      this.FileVersion = new FileVersion();
      this.FileVersion.Read(binaryStream);
      this.Tool = new Tool();
      this.Tool.Read(binaryStream);
      this.FormatSignature = new FormatSignature();
      this.FormatSignature.Read(binaryStream);
      this.AttributesOffset = binaryStream.ReadUInt64();
      this.ChunksOffset = binaryStream.ReadUInt64();
    }

    public void Write(BinaryStream binaryStream)
    {
      this.FileSignature.Write(binaryStream);
      this.FileVersion.Write(binaryStream);
      this.Tool.Write(binaryStream);
      this.FormatSignature.Write(binaryStream);
      binaryStream.WriteUInt64(this.AttributesOffset);
      binaryStream.WriteUInt64(this.ChunksOffset);
    }

    public void Update()
    {
    }

    public void Validate()
    {
    }

    public void Release()
    {
    }

    public static FileVersion ReadFileVersion(string fileName)
    {
      using (BinaryReader binaryReader = new BinaryReader((Stream) File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Read)))
      {
        BinaryStream binaryStream = new BinaryStream(binaryReader);
        FileHeader fileHeader = new FileHeader();
        fileHeader.Read(binaryStream);
        return fileHeader.FileVersion;
      }
    }
  }
}
