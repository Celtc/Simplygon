﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Unity.BasicShadingNodeProperties
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Cloud.Unity
{
  internal class BasicShadingNodeProperties
  {
    public string ShaderPropertyName { get; set; }

    public string NodeName { get; set; }

    public int NodeRef { get; set; }

    public BasicShadingNodeType NodeType { get; set; }

    public string TexCoords { get; set; }

    public BasicShadingNodeProperties(string shaderPropertyName, string nodeName, int nodeRef, BasicShadingNodeType nodeType, string texCoords = "TexCoords0")
    {
      this.ShaderPropertyName = shaderPropertyName;
      this.NodeName = nodeName;
      this.NodeRef = nodeRef;
      this.NodeType = nodeType;
      this.TexCoords = texCoords;
    }

    public BasicShadingNodeProperties DeepCopy()
    {
      return (BasicShadingNodeProperties) this.MemberwiseClone();
    }
  }
}
