﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Unity.ModifierShadingNodeProperties
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Cloud.Unity
{
  internal class ModifierShadingNodeProperties
  {
    public int NodeRef { get; set; }

    public int[] InputNodes { get; protected set; }

    public int[] NodeData { get; protected set; }

    public ModifierShadingNodeType NodeType { get; protected set; }

    public ModifierShadingNodeProperties(int nodeRef, int[] inputNodes, int[] nodeData, ModifierShadingNodeType nodeType)
    {
      this.NodeRef = nodeRef;
      this.InputNodes = inputNodes == null ? new int[0] : inputNodes;
      this.NodeData = nodeData == null ? new int[0] : nodeData;
      this.NodeType = nodeType;
    }

    public ModifierShadingNodeProperties DeepCopy()
    {
      return (ModifierShadingNodeProperties) this.MemberwiseClone();
    }
  }
}
