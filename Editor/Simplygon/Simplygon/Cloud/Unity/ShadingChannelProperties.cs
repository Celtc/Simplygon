﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Unity.ShadingChannelProperties
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.Collections.Generic;

namespace Simplygon.Cloud.Unity
{
  internal class ShadingChannelProperties
  {
    public string ChannelName { get; set; }

    public List<BasicShadingNodeProperties> BasicShadingNodesProperties { get; set; }

    public List<ModifierShadingNodeProperties> ModifierShadingNodesProperties { get; set; }

    public ShadingChannelProperties()
    {
    }

    public ShadingChannelProperties(string channelName, List<BasicShadingNodeProperties> basicPropertyNodes)
      : this(channelName, basicPropertyNodes, new List<ModifierShadingNodeProperties>())
    {
    }

    public ShadingChannelProperties(string channelName, List<BasicShadingNodeProperties> basicPropertyNodes, List<ModifierShadingNodeProperties> modifierPropertyNodes)
    {
      this.ChannelName = channelName;
      this.BasicShadingNodesProperties = basicPropertyNodes;
      this.ModifierShadingNodesProperties = modifierPropertyNodes;
    }

    public ShadingChannelProperties DeepCopy()
    {
      ShadingChannelProperties channelProperties = (ShadingChannelProperties) this.MemberwiseClone();
      channelProperties.BasicShadingNodesProperties = new List<BasicShadingNodeProperties>();
      for (int index = 0; index < this.BasicShadingNodesProperties.Count; ++index)
        channelProperties.BasicShadingNodesProperties.Add(this.BasicShadingNodesProperties[index].DeepCopy());
      channelProperties.ModifierShadingNodesProperties = new List<ModifierShadingNodeProperties>();
      for (int index = 0; index < this.ModifierShadingNodesProperties.Count; ++index)
        channelProperties.ModifierShadingNodesProperties.Add(this.ModifierShadingNodesProperties[index].DeepCopy());
      return channelProperties;
    }
  }
}
