﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Unity.ModifierShadingNodeType
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Cloud.Unity
{
  internal enum ModifierShadingNodeType
  {
    Interpolate = 0,
    Clamp = 1,
    Add = 2,
    Subtract = 3,
    Multiply = 4,
    Divide = 5,
    Swizzle = 6,
    Max = 12, // 0x0000000C
    Min = 13, // 0x0000000D
    Step = 14, // 0x0000000E
  }
}
