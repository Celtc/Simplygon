﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.Client.SettingsPreset
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.Cloud.Yoda.Client
{
  internal class SettingsPreset
  {
    public string SettingsPresetId { get; set; }

    public DateTime Created { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public string Contents { get; set; }

    public string Type { get; set; }

    public SettingsPreset()
    {
    }

    public SettingsPreset(string name, string description, string type, string contents)
    {
      this.SettingsPresetId = Guid.NewGuid().ToString();
      this.Created = DateTime.UtcNow;
      this.Name = name;
      this.Description = description;
      this.Type = type;
      this.Contents = contents;
    }

    public void Update(SettingsPreset preset)
    {
      this.Name = preset.Name;
      this.Description = preset.Description;
      this.Type = preset.Type;
      this.Contents = preset.Contents;
    }
  }
}
