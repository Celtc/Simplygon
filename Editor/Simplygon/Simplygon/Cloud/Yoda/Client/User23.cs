﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.Client.User23
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Cloud.Yoda.Client
{
  internal class User23
  {
    public string Name { get; set; }

    public string Email { get; set; }

    public string UserId { get; set; }

    public string ProcessSubscription { get; set; }

    public float ProcesssingTimeUsedThisMonth { get; set; }

    public ProcessSubscriptionRestrictions23 ProcessSubscriptionRestrictions { get; set; }

    public User23()
    {
      this.ProcessSubscriptionRestrictions = new ProcessSubscriptionRestrictions23();
    }
  }
}
