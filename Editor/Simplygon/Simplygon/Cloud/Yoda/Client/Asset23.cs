﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.Client.Asset23
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.Cloud.Yoda.Client
{
  internal class Asset23
  {
    public string AssetId { get; set; }

    public string UserId { get; set; }

    public DateTime Created { get; set; }

    public string AssetTypeValue { get; set; }

    public string Name { get; set; }

    public int FileSize { get; set; }

    public string FileFormat { get; set; }

    public string JobId { get; set; }

    public string ParentAssetId { get; set; }

    public string Region { get; set; }

    public string SettingsId { get; set; }

    public Asset23()
    {
    }

    public Asset23(string userId, string assetId)
    {
      this.UserId = userId;
      this.AssetId = assetId.ToLower();
      this.Created = DateTime.UtcNow;
    }

    public void Update(Asset23 asset)
    {
      this.FileSize = asset.FileSize;
      this.FileFormat = asset.FileFormat;
      this.JobId = asset.JobId;
      this.ParentAssetId = asset.ParentAssetId;
      this.Region = asset.Region;
      this.SettingsId = asset.SettingsId;
      asset.AssetId = asset.AssetId.ToString();
    }
  }
}
