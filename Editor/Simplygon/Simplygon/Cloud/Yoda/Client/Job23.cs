﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.Client.Job23
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Cloud.Yoda.Client
{
  internal class Job23
  {
    public string JobId { get; set; }

    public string JobType { get; set; }

    public string APIKey { get; set; }

    public string Name { get; set; }

    public string Status { get; set; }

    public string UserId { get; set; }

    public string Created { get; set; }

    public string InputAssetId { get; set; }

    public string OutputAssetId { get; set; }

    public string SettingsId { get; set; }

    public string CustomData { get; set; }

    public string ProgressLODIndex { get; set; }

    public string ProgressLODCount { get; set; }

    public string ProgressCategory { get; set; }

    public string ProgressPercentage { get; set; }

    public string ErrorMessage { get; set; }

    public string EstimatedTimeOfProcess { get; set; }

    public void Update(Job23 job)
    {
      this.JobId = job.JobId;
      this.JobType = job.JobType;
      this.APIKey = job.APIKey;
      this.Name = job.Name;
      this.Status = job.Status;
      this.UserId = job.UserId;
      this.Created = job.Created;
      this.InputAssetId = job.InputAssetId;
      this.OutputAssetId = job.OutputAssetId;
      this.SettingsId = job.SettingsId;
      this.CustomData = job.CustomData;
      this.ProgressLODIndex = job.ProgressLODIndex;
      this.ProgressLODCount = job.ProgressLODCount;
      this.ProgressCategory = job.ProgressCategory;
      this.ProgressPercentage = job.ProgressPercentage;
      this.ErrorMessage = job.ErrorMessage;
      this.EstimatedTimeOfProcess = job.EstimatedTimeOfProcess;
    }
  }
}
