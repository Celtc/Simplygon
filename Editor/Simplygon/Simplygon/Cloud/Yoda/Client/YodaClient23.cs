﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.Client.YodaClient23
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Globalization;

namespace Simplygon.Cloud.Yoda.Client
{
  internal class YodaClient23
  {
    private int timeout = 20000;

    public string APIKey { get; set; }

    public string Username { get; set; }

    public string Password { get; set; }

    public string Hostname { get; private set; }

    private bool abortTransfer { get; set; }

    public event EventHandler<YodaClientTransferProgress> TransferProgress;

    private void OnTransferProgress(YodaClientTransferProgress args)
    {
      // ISSUE: reference to a compiler-generated field
      EventHandler<YodaClientTransferProgress> transferProgress = this.TransferProgress;
      if (transferProgress == null)
        return;
      transferProgress((object) this, args);
    }

    public void AbortTransfer()
    {
      this.abortTransfer = true;
    }

    public static bool Validator(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
      return true;
    }

    public YodaClient23(string apiKey, string username, string password)
    {
      this.APIKey = apiKey;
      this.Username = username;
      this.Password = password;
      this.Hostname = "public.simplygon.com";
      ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(YodaClient23.Validator);
      ServicePointManager.DefaultConnectionLimit = 1000;
    }

    public YodaClient23(string apiKey, string username, string password, string hostname)
    {
      this.APIKey = apiKey;
      this.Username = username;
      this.Password = password;
      this.Hostname = hostname;
      ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(YodaClient23.Validator);
      ServicePointManager.DefaultConnectionLimit = 1000;
    }

    public YodaClient23(string hostname)
    {
      this.APIKey = "LocalAPIKey";
      this.Username = string.Empty;
      this.Password = string.Empty;
      this.Hostname = hostname;
      ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(YodaClient23.Validator);
      ServicePointManager.DefaultConnectionLimit = 1000;
    }

    public UserSettings10 UserSettings(string name)
    {
      WebRequest request = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/1.0/UserSettings/{1}", (object) this.Hostname, (object) name));
      request.Method = "GET";
      request.ContentLength = 0L;
      request.Timeout = this.timeout;
      this.SetBasicAuthHeader(request);
      using (WebResponse response = request.GetResponse())
      {
        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
          return JsonConvert.DeserializeObject<UserSettings10>(streamReader.ReadToEnd());
      }
    }

    public User23 AccountInfo()
    {
      WebRequest request = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/account?apikey={1}", (object) this.Hostname, (object) this.APIKey));
      request.Method = "GET";
      request.ContentLength = 0L;
      request.Timeout = this.timeout;
      this.SetBasicAuthHeader(request);
      using (WebResponse response = request.GetResponse())
      {
        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
          return JsonConvert.DeserializeObject<User23>(streamReader.ReadToEnd());
      }
    }

    public static Client23 ClientInfo(string hostname, string apiKey)
    {
      WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/client?apikey={1}", (object) hostname, (object) apiKey));
      webRequest.Method = "GET";
      webRequest.ContentLength = 0L;
      using (WebResponse response = webRequest.GetResponse())
      {
        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
          return JsonConvert.DeserializeObject<Client23>(streamReader.ReadToEnd());
      }
    }

    public bool PostFeedback(string client, string feedback)
    {
      byte[] bytes = Encoding.UTF8.GetBytes(feedback);
      WebRequest request = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://public.simplygon.com/2.3/feedback?apikey={0}&client={1}", (object) this.APIKey, (object) client));
      request.Method = "POST";
      request.ContentType = "text/plain";
      request.ContentLength = (long) bytes.Length;
      request.Timeout = this.timeout;
      this.SetBasicAuthHeader(request);
      using (Stream requestStream = request.GetRequestStream())
        requestStream.Write(bytes, 0, bytes.Length);
      using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
        return response.StatusCode == HttpStatusCode.OK;
    }

    public bool PostFeedbackAnonymous(string client, string feedback)
    {
      byte[] bytes = Encoding.UTF8.GetBytes(feedback);
      WebRequest request = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://public.simplygon.com/2.3/FeedbackAnonymous?apikey={0}&client={1}", (object) this.APIKey, (object) client));
      request.Method = "POST";
      request.ContentType = "text/plain";
      request.ContentLength = (long) bytes.Length;
      request.Timeout = this.timeout;
      this.SetBasicAuthHeader(request);
      using (Stream requestStream = request.GetRequestStream())
        requestStream.Write(bytes, 0, bytes.Length);
      using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
        return response.StatusCode == HttpStatusCode.OK;
    }

    public bool PostCrashReport(string message)
    {
      byte[] bytes = Encoding.UTF8.GetBytes(message);
      WebRequest request = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://public.simplygon.com/2.3/Support/CrashReport"));
      request.Method = "POST";
      request.ContentType = "text/plain";
      request.ContentLength = (long) bytes.Length;
      request.Timeout = this.timeout;
      this.SetBasicAuthHeader(request);
      using (Stream requestStream = request.GetRequestStream())
        requestStream.Write(bytes, 0, bytes.Length);
      using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
        return response.StatusCode == HttpStatusCode.OK;
    }

    public Job23 CreateJob(string jobName, string assetId, string customData)
    {
      byte[] bytes = Encoding.UTF8.GetBytes(customData);
      WebRequest request = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/job/create?apikey={1}&job_name={2}&asset_id={3}", (object) this.Hostname, (object) this.APIKey, (object) jobName, (object) assetId));
      request.Method = "POST";
      request.ContentType = "text/plain";
      request.ContentLength = (long) bytes.Length;
      request.Timeout = this.timeout;
      this.SetBasicAuthHeader(request);
      using (Stream requestStream = request.GetRequestStream())
        requestStream.Write(bytes, 0, bytes.Length);
      using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
      {
        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
          return JsonConvert.DeserializeObject<Job23>(streamReader.ReadToEnd());
      }
    }

    public bool UploadJobSettings(string jobId, string filename)
    {
      using (FileStream fileStream = new FileStream(filename, FileMode.Open))
        return this.UploadJobSettings(jobId, (Stream) fileStream);
    }

    public bool UploadJobSettings(string jobId, Stream stream)
    {
      HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/job/{1}/uploadsettings?apikey={2}", (object) this.Hostname, (object) jobId, (object) this.APIKey));
      httpWebRequest.AllowWriteStreamBuffering = false;
      httpWebRequest.ReadWriteTimeout = 7200000;
      httpWebRequest.Timeout = 7200000;
      httpWebRequest.SendChunked = true;
      httpWebRequest.Method = "POST";
      httpWebRequest.ContentType = "application/octet-stream";
      httpWebRequest.ContentLength = stream.Length;
      this.SetBasicAuthHeader((WebRequest) httpWebRequest);
      using (Stream requestStream = httpWebRequest.GetRequestStream())
      {
        if (!this.CopyStream(stream, stream.Length, requestStream))
        {
          httpWebRequest.Abort();
          return false;
        }
      }
      using (HttpWebResponse response = (HttpWebResponse) httpWebRequest.GetResponse())
        return response.StatusCode == HttpStatusCode.OK;
    }

    public bool ProcessJob(string jobId)
    {
      WebRequest request = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/job/{1}/process?apikey={2}", (object) this.Hostname, (object) jobId, (object) this.APIKey));
      request.Method = "PUT";
      request.ContentLength = 0L;
      request.Timeout = this.timeout;
      this.SetBasicAuthHeader(request);
      using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
        return response.StatusCode == HttpStatusCode.OK;
    }

    public Job23 GetJob(string jobId)
    {
      WebRequest request = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/job/{1}?apikey={2}", (object) this.Hostname, (object) jobId, (object) this.APIKey));
      request.Method = "GET";
      request.ContentLength = 0L;
      request.Timeout = this.timeout;
      this.SetBasicAuthHeader(request);
      using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
      {
        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
          return JsonConvert.DeserializeObject<Job23>(streamReader.ReadToEnd());
      }
    }

    public List<Job23> GetActiveJobs()
    {
      WebRequest request = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/job/active?apikey={1}", (object) this.Hostname, (object) this.APIKey));
      request.Method = "GET";
      request.ContentLength = 0L;
      request.Timeout = this.timeout;
      this.SetBasicAuthHeader(request);
      using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
      {
        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
          return JsonConvert.DeserializeObject<List<Job23>>(streamReader.ReadToEnd());
      }
    }

    public bool DeleteJob(string jobId)
    {
      try
      {
        WebRequest request = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/job/{1}/delete?apikey={2}", (object) this.Hostname, (object) jobId, (object) this.APIKey));
        request.Method = "DELETE";
        request.ContentLength = 0L;
        request.Timeout = this.timeout;
        this.SetBasicAuthHeader(request);
        using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public bool DeleteAllJobs()
    {
      try
      {
        WebRequest request = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/job/delete?apikey={1}", (object) this.Hostname, (object) this.APIKey));
        request.Method = "DELETE";
        request.ContentLength = 0L;
        request.Timeout = this.timeout;
        this.SetBasicAuthHeader(request);
        using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public Asset23 UploadAsset(string filename, string assetName)
    {
      return this.UploadAsset(filename, assetName, string.Empty);
    }

    public Asset23 UploadAsset(string filename, string assetName, string assetId)
    {
      using (FileStream fileStream = new FileStream(filename, FileMode.Open))
      {
        HttpWebRequest httpWebRequest;
        if (string.IsNullOrEmpty(assetId))
          httpWebRequest = (HttpWebRequest) WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/asset/upload?apikey={1}&asset_name={2}", (object) this.Hostname, (object) this.APIKey, (object) assetName));
        else
          httpWebRequest = (HttpWebRequest) WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/asset/upload?apikey={1}&asset_name={2}&asset_id={3}", (object) this.Hostname, (object) this.APIKey, (object) assetName, (object) assetId));
        httpWebRequest.AllowWriteStreamBuffering = false;
        httpWebRequest.ReadWriteTimeout = 7200000;
        httpWebRequest.Timeout = 7200000;
        httpWebRequest.SendChunked = true;
        httpWebRequest.Method = "POST";
        httpWebRequest.ContentType = "application/octet-stream";
        httpWebRequest.ContentLength = fileStream.Length;
        this.SetBasicAuthHeader((WebRequest) httpWebRequest);
        using (Stream requestStream = httpWebRequest.GetRequestStream())
        {
          if (!this.CopyStream((Stream) fileStream, fileStream.Length, requestStream))
          {
            httpWebRequest.Abort();
            return (Asset23) null;
          }
        }
        using (HttpWebResponse response = (HttpWebResponse) httpWebRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<Asset23>(streamReader.ReadToEnd());
        }
      }
    }

    public Upload23 UploadAssetPart_Init(string assetName)
    {
      return this.UploadAssetPart_Init(assetName, string.Empty);
    }

    public Upload23 UploadAssetPart_Init(string assetName, string assetId)
    {
      HttpWebRequest httpWebRequest;
      if (string.IsNullOrEmpty(assetId))
        httpWebRequest = (HttpWebRequest) WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/asset/uploadpart?apikey={1}&asset_name={2}", (object) this.Hostname, (object) this.APIKey, (object) assetName));
      else
        httpWebRequest = (HttpWebRequest) WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/asset/uploadpart?apikey={1}&asset_name={2}&asset_id={3}", (object) this.Hostname, (object) this.APIKey, (object) assetName, (object) assetId));
      httpWebRequest.Method = "POST";
      httpWebRequest.ContentLength = 0L;
      httpWebRequest.Timeout = this.timeout;
      this.SetBasicAuthHeader((WebRequest) httpWebRequest);
      using (HttpWebResponse response = (HttpWebResponse) httpWebRequest.GetResponse())
      {
        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
          return JsonConvert.DeserializeObject<Upload23>(streamReader.ReadToEnd());
      }
    }

    public Upload23 UploadAssetPart_Complete(string uploadId, int partCount, long uploadSize)
    {
      HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/asset/uploadpart/{1}/Complete?apikey={2}&part_count={3}&upload_size={4}", (object) this.Hostname, (object) uploadId, (object) this.APIKey, (object) partCount, (object) uploadSize));
      httpWebRequest.Method = "POST";
      httpWebRequest.ContentLength = 0L;
      this.SetBasicAuthHeader((WebRequest) httpWebRequest);
      using (HttpWebResponse response = (HttpWebResponse) httpWebRequest.GetResponse())
      {
        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
          return JsonConvert.DeserializeObject<Upload23>(streamReader.ReadToEnd());
      }
    }

    public Upload23 UploadAssetPart_Abort(string uploadId)
    {
      HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/asset/uploadpart/{1}/abort?apikey={2}", (object) this.Hostname, (object) uploadId, (object) this.APIKey));
      httpWebRequest.Method = "POST";
      httpWebRequest.ContentLength = 0L;
      httpWebRequest.Timeout = this.timeout;
      this.SetBasicAuthHeader((WebRequest) httpWebRequest);
      using (HttpWebResponse response = (HttpWebResponse) httpWebRequest.GetResponse())
      {
        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
          return JsonConvert.DeserializeObject<Upload23>(streamReader.ReadToEnd());
      }
    }

    public Upload23 UploadAssetPart_Get(string uploadId)
    {
      HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/asset/uploadpart/{1}?apikey={2}", (object) this.Hostname, (object) uploadId, (object) this.APIKey));
      httpWebRequest.Method = "GET";
      httpWebRequest.ContentLength = 0L;
      httpWebRequest.Timeout = this.timeout;
      this.SetBasicAuthHeader((WebRequest) httpWebRequest);
      using (HttpWebResponse response = (HttpWebResponse) httpWebRequest.GetResponse())
      {
        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
          return JsonConvert.DeserializeObject<Upload23>(streamReader.ReadToEnd());
      }
    }

    public Upload23 UploadAssetPart(Stream stream, string uploadId, int partNumber)
    {
      HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/asset/uploadpart/{1}/upload?apikey={2}&part_number={3}", (object) this.Hostname, (object) uploadId, (object) this.APIKey, (object) partNumber));
      httpWebRequest.AllowWriteStreamBuffering = false;
      httpWebRequest.ReadWriteTimeout = 7200000;
      httpWebRequest.Timeout = 7200000;
      httpWebRequest.SendChunked = false;
      httpWebRequest.Method = "PUT";
      httpWebRequest.ContentType = "application/octet-stream";
      httpWebRequest.ContentLength = stream.Length;
      this.SetBasicAuthHeader((WebRequest) httpWebRequest);
      using (Stream requestStream = httpWebRequest.GetRequestStream())
      {
        if (!this.CopyStream(stream, stream.Length, requestStream))
        {
          httpWebRequest.Abort();
          this.UploadAssetPart_Abort(uploadId);
          return (Upload23) null;
        }
      }
      using (HttpWebResponse response = (HttpWebResponse) httpWebRequest.GetResponse())
      {
        using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
          return JsonConvert.DeserializeObject<Upload23>(streamReader.ReadToEnd());
      }
    }

    public bool DownloadAsset(string assetId, string filename)
    {
      try
      {
        WebRequest request = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/asset/{1}/download?apikey={2}", (object) this.Hostname, (object) assetId, (object) this.APIKey));
        request.Method = "GET";
        request.ContentLength = 0L;
        request.Timeout = this.timeout;
        this.SetBasicAuthHeader(request);
        HttpWebResponse response = (HttpWebResponse) request.GetResponse();
        using (FileStream fileStream = new FileStream(filename, FileMode.Create, FileAccess.Write))
        {
          using (Stream responseStream = response.GetResponseStream())
          {
            if (!this.CopyStream(responseStream, response.ContentLength, (Stream) fileStream))
            {
              request.Abort();
              return false;
            }
          }
          return true;
        }
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public Asset23 GetAsset(string assetId, string settingsId)
    {
      try
      {
        WebRequest request;
        if (!string.IsNullOrEmpty(settingsId))
          request = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/asset/{1}/{2}?apikey={3}", (object) this.Hostname, (object) assetId, (object) settingsId, (object) this.APIKey));
        else
          request = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/asset/{1}?apikey={2}", (object) this.Hostname, (object) assetId, (object) this.APIKey));
        request.Method = "GET";
        request.ContentLength = 0L;
        request.Timeout = this.timeout;
        this.SetBasicAuthHeader(request);
        using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<Asset23>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (Asset23) null;
    }

    public List<SimplygonPreset10> GetSimplygonPresets(string type = "")
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/SettingsPreset?apiKey={1}&type={2}", (object) this.Hostname, (object) this.APIKey, (object) type));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<List<SimplygonPreset10>>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (List<SimplygonPreset10>) null;
    }

    public List<SimplygonPreset10> GetSimplygonPreset(string contentsHash)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/SettingsPreset/{1}?apiKey={2}", (object) this.Hostname, (object) contentsHash, (object) this.APIKey));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<List<SimplygonPreset10>>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (List<SimplygonPreset10>) null;
    }

    public bool LocalHealthStatus(string server, string serverName, string serverSpec)
    {
      try
      {
        byte[] bytes = Encoding.UTF8.GetBytes(string.Format(CultureInfo.InvariantCulture, "{0};{1}", (object) serverName, (object) serverSpec));
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/healthstatus/{1}", (object) this.Hostname, (object) server));
        webRequest.Method = "POST";
        webRequest.ContentLength = (long) bytes.Length;
        webRequest.Timeout = this.timeout;
        using (Stream requestStream = webRequest.GetRequestStream())
          requestStream.Write(bytes, 0, bytes.Length);
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public User LocalAddUser(string firstName, string lastName, string email, string password, bool isAdmin)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/adduser?firstName={1}&lastName={2}&email={3}&password={4}&isadmin={5}", (object) this.Hostname, (object) firstName, (object) lastName, (object) email, (object) password, (object) isAdmin));
        webRequest.Method = "POST";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<User>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (User) null;
    }

    public List<User> LocalGetAllUsers()
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/getallusers", (object) this.Hostname));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<List<User>>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (List<User>) null;
    }

    public User LocalGetUser(string userId)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/getuser/{1}", (object) this.Hostname, (object) userId));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<User>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (User) null;
    }

    public User LocalRemoveUser(string userId)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/removeuser/{1}", (object) this.Hostname, (object) userId));
        webRequest.Method = "POST";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<User>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (User) null;
    }

    public ServerTask LocalGetServerTask(string server)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/servertask/{1}", (object) this.Hostname, (object) server));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<ServerTask>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (ServerTask) null;
    }

    public List<ServerTask> LocalGetAllServerTasks()
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/servertask", (object) this.Hostname));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<List<ServerTask>>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (List<ServerTask>) null;
    }

    public List<Server> LocalGetAllServers()
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/server", (object) this.Hostname));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<List<Server>>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (List<Server>) null;
    }

    public int LocalGetAssetCount()
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/assetcount", (object) this.Hostname));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
          {
            int result;
            int.TryParse(streamReader.ReadToEnd(), out result);
            return result;
          }
        }
      }
      catch (Exception ex)
      {
      }
      return 0;
    }

    public Asset23 LocalGetAsset(string userId, string assetId)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/asset/{1}/{2}", (object) this.Hostname, (object) userId, (object) assetId));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<Asset23>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (Asset23) null;
    }

    public List<Asset23> LocalGetAssets(int pageIndex, int pageSize)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/asset/paging/{1}/{2}", (object) this.Hostname, (object) pageIndex, (object) pageSize));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<List<Asset23>>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (List<Asset23>) null;
    }

    public List<Asset23> LocalGetAllAssets()
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/asset", (object) this.Hostname));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<List<Asset23>>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (List<Asset23>) null;
    }

    public bool LocalAddAsset(Asset23 asset)
    {
      try
      {
        byte[] bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject((object) asset));
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/asset/add", (object) this.Hostname));
        webRequest.Method = "POST";
        webRequest.ContentType = "text/json";
        webRequest.ContentLength = (long) bytes.Length;
        webRequest.Timeout = this.timeout;
        using (Stream requestStream = webRequest.GetRequestStream())
          requestStream.Write(bytes, 0, bytes.Length);
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public bool LocalUpdateAsset(Asset23 asset)
    {
      try
      {
        byte[] bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject((object) asset));
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/asset/update", (object) this.Hostname));
        webRequest.Method = "POST";
        webRequest.ContentType = "text/json";
        webRequest.ContentLength = (long) bytes.Length;
        webRequest.Timeout = this.timeout;
        using (Stream requestStream = webRequest.GetRequestStream())
          requestStream.Write(bytes, 0, bytes.Length);
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public bool LocalUploadAsset(string userId, string assetId, string filename)
    {
      using (FileStream fileStream = new FileStream(filename, FileMode.Open))
      {
        HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/Asset/{1}/{2}/Upload", (object) this.Hostname, (object) userId, (object) assetId));
        httpWebRequest.AllowWriteStreamBuffering = false;
        httpWebRequest.ReadWriteTimeout = 7200000;
        httpWebRequest.Timeout = 7200000;
        httpWebRequest.SendChunked = true;
        httpWebRequest.Method = "POST";
        httpWebRequest.ContentType = "application/octet-stream";
        httpWebRequest.ContentLength = fileStream.Length;
        using (Stream requestStream = httpWebRequest.GetRequestStream())
        {
          if (!this.CopyStream((Stream) fileStream, fileStream.Length, requestStream))
          {
            httpWebRequest.Abort();
            return false;
          }
        }
        using (HttpWebResponse response = (HttpWebResponse) httpWebRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
    }

    public bool LocalDownloadAsset(string userId, string assetId, string filename)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/Asset/{1}/{2}/Download", (object) this.Hostname, (object) userId, (object) assetId));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse();
        using (FileStream fileStream = new FileStream(filename, FileMode.Create, FileAccess.Write))
        {
          using (Stream responseStream = response.GetResponseStream())
          {
            if (!this.CopyStream(responseStream, response.ContentLength, (Stream) fileStream))
            {
              webRequest.Abort();
              return false;
            }
          }
          return true;
        }
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public bool LocalDeleteJob(string userId, string jobId)
    {
      try
      {
        WebRequest request = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/job/{1}/{2}/delete", (object) this.Hostname, (object) userId, (object) jobId));
        request.Method = "DELETE";
        request.ContentLength = 0L;
        request.Timeout = this.timeout;
        this.SetBasicAuthHeader(request);
        using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public Asset23 LocalDeleteAsset(string userId, string assetId)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/asset/{1}/{2}/delete", (object) this.Hostname, (object) userId, (object) assetId));
        webRequest.Method = "POST";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<Asset23>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (Asset23) null;
    }

    public Job23 LocalGetJob(string userId, string jobId)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/job/{1}/{2}", (object) this.Hostname, (object) userId, (object) jobId));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<Job23>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (Job23) null;
    }

    public bool LocalUpdateJob(Job23 job, string skipStateAttributes)
    {
      try
      {
        byte[] bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject((object) job));
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/job/update?SkipStateAttributes={1}", (object) this.Hostname, (object) skipStateAttributes));
        webRequest.Method = "POST";
        webRequest.ContentType = "text/json";
        webRequest.ContentLength = (long) bytes.Length;
        webRequest.Timeout = this.timeout;
        using (Stream requestStream = webRequest.GetRequestStream())
          requestStream.Write(bytes, 0, bytes.Length);
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public int LocalGetJobCount()
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/jobcount", (object) this.Hostname));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
          {
            int result;
            int.TryParse(streamReader.ReadToEnd(), out result);
            return result;
          }
        }
      }
      catch (Exception ex)
      {
      }
      return 0;
    }

    public List<Job23> LocalGetJobs(int pageIndex, int pageSize)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/job/paging/{1}/{2}", (object) this.Hostname, (object) pageIndex, (object) pageSize));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<List<Job23>>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (List<Job23>) null;
    }

    public List<Job23> LocalGetAllJobs()
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/job", (object) this.Hostname));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<List<Job23>>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (List<Job23>) null;
    }

    public bool LocalDownloadSettings(string userId, string jobId, string filename)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/Job/{1}/{2}/DownloadSettings", (object) this.Hostname, (object) userId, (object) jobId));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse();
        using (FileStream fileStream = new FileStream(filename, FileMode.Create, FileAccess.Write))
        {
          using (Stream responseStream = response.GetResponseStream())
          {
            if (!this.CopyStream(responseStream, response.ContentLength, (Stream) fileStream))
            {
              webRequest.Abort();
              return false;
            }
          }
          return true;
        }
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public bool LocalAddJobStats(JobStats22 jobStats)
    {
      try
      {
        byte[] bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject((object) jobStats));
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/jobstats/add", (object) this.Hostname));
        webRequest.Method = "POST";
        webRequest.ContentType = "text/json";
        webRequest.ContentLength = (long) bytes.Length;
        webRequest.Timeout = this.timeout;
        using (Stream requestStream = webRequest.GetRequestStream())
          requestStream.Write(bytes, 0, bytes.Length);
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public bool LocalUpdateJobStats(JobStats22 jobStats)
    {
      try
      {
        byte[] bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject((object) jobStats));
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/local/jobstats/update", (object) this.Hostname));
        webRequest.Method = "POST";
        webRequest.ContentType = "text/json";
        webRequest.ContentLength = (long) bytes.Length;
        webRequest.Timeout = this.timeout;
        using (Stream requestStream = webRequest.GetRequestStream())
          requestStream.Write(bytes, 0, bytes.Length);
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public List<SimplygonSDK10> LocalGetSimplygonSDKs()
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/SimplygonSDK", (object) this.Hostname));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<List<SimplygonSDK10>>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (List<SimplygonSDK10>) null;
    }

    public bool LocalDownloadSimplygonSDKArchive(string simplygonSDKArchiveFileName, string dstFilePath)
    {
      WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/SimplygonSDK/{1}/Download", (object) this.Hostname, (object) simplygonSDKArchiveFileName));
      webRequest.Method = "GET";
      webRequest.ContentLength = 0L;
      webRequest.Timeout = this.timeout;
      HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse();
      using (FileStream fileStream = new FileStream(dstFilePath, FileMode.Create, FileAccess.Write))
      {
        using (Stream responseStream = response.GetResponseStream())
        {
          if (!this.CopyStream(responseStream, response.ContentLength, (Stream) fileStream))
          {
            webRequest.Abort();
            return false;
          }
        }
        return true;
      }
    }

    public bool LocalUploadSimplygonSDKArchive(string archiveFilePath)
    {
      using (FileStream fileStream = new FileStream(archiveFilePath, FileMode.Open, FileAccess.Read))
      {
        HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/SimplygonSDK/{1}/Upload", (object) this.Hostname, (object) Path.GetFileName(archiveFilePath)));
        httpWebRequest.AllowWriteStreamBuffering = false;
        httpWebRequest.ReadWriteTimeout = 7200000;
        httpWebRequest.Timeout = 7200000;
        httpWebRequest.SendChunked = true;
        httpWebRequest.Method = "POST";
        httpWebRequest.ContentType = "application/octet-stream";
        httpWebRequest.ContentLength = fileStream.Length;
        using (Stream requestStream = httpWebRequest.GetRequestStream())
        {
          if (!this.CopyStream((Stream) fileStream, fileStream.Length, requestStream))
          {
            httpWebRequest.Abort();
            return false;
          }
        }
        using (HttpWebResponse response = (HttpWebResponse) httpWebRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
    }

    public bool LocalRemoveSimplygonSDKArchive(string archiveFileName)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/SimplygonSDK/{1}/Remove", (object) this.Hostname, (object) archiveFileName));
        webRequest.Method = "POST";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public bool LocalAddSimplygonPreset(SettingsPreset preset)
    {
      try
      {
        byte[] bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject((object) preset));
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/SettingsPreset/Add", (object) this.Hostname));
        webRequest.Method = "POST";
        webRequest.ContentType = "text/json";
        webRequest.ContentLength = (long) bytes.Length;
        webRequest.Timeout = this.timeout;
        using (Stream requestStream = webRequest.GetRequestStream())
          requestStream.Write(bytes, 0, bytes.Length);
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public SettingsPreset LocalRemoveSimplygonPreset(string presetId)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/SettingsPreset/{1}/Remove", (object) this.Hostname, (object) presetId));
        webRequest.Method = "POST";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<SettingsPreset>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (SettingsPreset) null;
    }

    public BackendSettings LocalDownloadBackendSettings()
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/Settings/Backend/Download", (object) this.Hostname));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<BackendSettings>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (BackendSettings) null;
    }

    public bool LocalUploadBackendSettings(BackendSettings backendSettings)
    {
      try
      {
        byte[] bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject((object) backendSettings));
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/Settings/Backend/Upload", (object) this.Hostname));
        webRequest.Method = "POST";
        webRequest.ContentType = "text/json";
        webRequest.ContentLength = (long) bytes.Length;
        webRequest.Timeout = this.timeout;
        using (Stream requestStream = webRequest.GetRequestStream())
          requestStream.Write(bytes, 0, bytes.Length);
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public bool LocalCheckWritePath(string path)
    {
      try
      {
        string newValue = ";;;";
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/Settings/CheckWritePath/{1}/{2}", (object) this.Hostname, (object) path.Replace("\\", newValue), (object) newValue));
        webRequest.Method = "POST";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public bool LocalChangePort(string port)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/Settings/Port/{1}", (object) this.Hostname, (object) port));
        webRequest.Method = "POST";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public double LocalGetAssetsTTL()
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/Settings/Assets/TTL", (object) this.Hostname));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
          {
            double result;
            double.TryParse(streamReader.ReadToEnd(), out result);
            return result;
          }
        }
      }
      catch (Exception ex)
      {
      }
      return 0.0;
    }

    public bool LocalChangeAssetsTTL(double assetsTTL)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/Settings/Assets/TTL/{1}", (object) this.Hostname, (object) assetsTTL));
        webRequest.Method = "POST";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public double LocalGetAssetsCleanupInterval()
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/Settings/Assets/CleanupInterval", (object) this.Hostname));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
          {
            double result;
            double.TryParse(streamReader.ReadToEnd(), out result);
            return result;
          }
        }
      }
      catch (Exception ex)
      {
      }
      return 0.0;
    }

    public bool LocalChangeAssetsCleanupInterval(int startHour, int startMinute, double msInterval)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/Settings/Assets/CleanupInterval/{1}/{2}/{3}", (object) this.Hostname, (object) startHour, (object) startMinute, (object) msInterval));
        webRequest.Method = "POST";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public string LocalGetTempDirectory()
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/Settings/TempDirectory", (object) this.Hostname));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return streamReader.ReadToEnd();
        }
      }
      catch (Exception ex)
      {
      }
      return string.Empty;
    }

    public bool LocalChangeTempDirectory(string path)
    {
      try
      {
        string newValue = ";;;";
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/Settings/TempDirectory/{1}/{2}", (object) this.Hostname, (object) path.Replace("\\", newValue), (object) newValue));
        webRequest.Method = "POST";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public string LocalGetWorkDirectory()
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/Settings/WorkDirectory", (object) this.Hostname));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return streamReader.ReadToEnd();
        }
      }
      catch (Exception ex)
      {
      }
      return string.Empty;
    }

    public bool LocalChangeWorkDirectory(string path)
    {
      try
      {
        string newValue = ";;;";
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/Settings/WorkDirectory/{1}/{2}", (object) this.Hostname, (object) path.Replace("\\", newValue), (object) newValue));
        webRequest.Method = "POST";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public string LocalGetLogDirectory()
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/Settings/LogDirectory", (object) this.Hostname));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return streamReader.ReadToEnd();
        }
      }
      catch (Exception ex)
      {
      }
      return string.Empty;
    }

    public bool LocalChangeLogDirectory(string path)
    {
      try
      {
        string newValue = ";;;";
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/Settings/LogDirectory/{1}/{2}", (object) this.Hostname, (object) path.Replace("\\", newValue), (object) newValue));
        webRequest.Method = "POST";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public bool LocalGetServerLoggingEnabled()
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/ServerLog/Enabled", (object) this.Hostname));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
          {
            bool result;
            bool.TryParse(streamReader.ReadToEnd(), out result);
            return result;
          }
        }
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public bool LocalSetServerLoggingEnabled(bool enabled)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/ServerLog/Enabled/{1}", (object) this.Hostname, (object) enabled.ToString()));
        webRequest.Method = "POST";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
          return response.StatusCode == HttpStatusCode.OK;
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public List<ServerLog> LocalGetAllServerLogs()
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/ServerLog", (object) this.Hostname));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return JsonConvert.DeserializeObject<List<ServerLog>>(streamReader.ReadToEnd());
        }
      }
      catch (Exception ex)
      {
      }
      return (List<ServerLog>) null;
    }

    public string LocalGetServerLogContents(string logFileName)
    {
      try
      {
        WebRequest webRequest = WebRequest.Create(string.Format(CultureInfo.InvariantCulture, "https://{0}/2.3/Local/ServerLog/{1}", (object) this.Hostname, (object) logFileName));
        webRequest.Method = "GET";
        webRequest.ContentLength = 0L;
        webRequest.Timeout = this.timeout;
        using (HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse())
        {
          using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            return streamReader.ReadToEnd();
        }
      }
      catch (Exception ex)
      {
      }
      return string.Empty;
    }

    protected void SetBasicAuthHeader(WebRequest request)
    {
      string base64String = Convert.ToBase64String(Encoding.Default.GetBytes(this.Username + ":" + this.Password));
      request.Headers["Authorization"] = "Basic " + base64String;
    }

    protected bool CopyStream(Stream input, long inputLength, Stream output)
    {
      this.abortTransfer = false;
      byte[] buffer = new byte[131072];
      int bytesTransfered = 0;
      DateTime utcNow = DateTime.UtcNow;
      int count;
      while ((count = input.Read(buffer, 0, buffer.Length)) > 0)
      {
        output.Write(buffer, 0, count);
        bytesTransfered += count;
        if ((DateTime.UtcNow - utcNow).TotalMilliseconds > 100.0)
        {
          this.OnTransferProgress(new YodaClientTransferProgress((int) inputLength, bytesTransfered));
          utcNow = DateTime.UtcNow;
        }
        if (this.abortTransfer)
        {
          this.abortTransfer = false;
          return false;
        }
      }
      return true;
    }
  }
}
