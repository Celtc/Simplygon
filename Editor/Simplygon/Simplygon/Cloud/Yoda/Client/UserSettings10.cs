﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.Client.UserSettings10
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Cloud.Yoda.Client
{
  internal class UserSettings10
  {
    public string Name { get; set; }

    public int MajorVersion { get; set; }

    public int MinorVersion { get; set; }

    public int BuildVersion { get; set; }

    public string UnityShaderSettings { get; set; }
  }
}
