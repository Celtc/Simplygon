﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.Client.Server
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.Cloud.Yoda.Client
{
  internal class Server
  {
    public string Id { get; set; }

    public string Name { get; set; }

    public string IP { get; set; }

    public string Spec { get; set; }

    public DateTime LastHealthStatus { get; set; }

    public Server()
    {
    }

    public Server(string id, string name, string ip, string spec)
    {
      this.Id = id;
      this.Name = name;
      this.IP = ip;
      this.Spec = spec;
      this.LastHealthStatus = DateTime.UtcNow;
    }
  }
}
