﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.Client.ProcessSubscriptionRestrictions23
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Cloud.Yoda.Client
{
  internal class ProcessSubscriptionRestrictions23
  {
    public int SimultaneousJobs { get; set; }

    public int MaxReruns { get; set; }

    public int MaxTriangles { get; set; }

    public float ProcessingTimePerMonth { get; set; }

    public MeshLODRestrictions23 MeshLOD { get; set; }

    public ProxyLODRestrictions23 ProxyLOD { get; set; }

    public BoneLODRestrictions23 BoneLOD { get; set; }

    public MaterialLODRestrictions23 MaterialLOD { get; set; }

    public AggregateLODRestrictions23 AggregateLOD { get; set; }

    public ProcessSubscriptionRestrictions23()
    {
      this.SimultaneousJobs = -1;
      this.MaxReruns = -1;
      this.MaxTriangles = -1;
      this.ProcessingTimePerMonth = -1f;
      this.MeshLOD = new MeshLODRestrictions23();
      this.ProxyLOD = new ProxyLODRestrictions23();
      this.BoneLOD = new BoneLODRestrictions23();
      this.MaterialLOD = new MaterialLODRestrictions23();
      this.AggregateLOD = new AggregateLODRestrictions23();
    }
  }
}
