﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.Client.JobStatus22
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Cloud.Yoda.Client
{
  internal enum JobStatus22
  {
    Unknown,
    Created,
    Uploaded,
    ReadyForProcessing,
    Processing,
    Processed,
    Accepted,
    Failed,
    Deleted,
  }
}
