﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.Client.Asset22
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Cloud.Yoda.Client
{
  internal class Asset22
  {
    public string UserId { get; set; }

    public string AssetId { get; set; }

    public string Name { get; set; }

    public int FileSize { get; set; }

    public string SettingsId { get; set; }

    public void Update(Asset22 asset)
    {
      this.UserId = asset.UserId;
      this.AssetId = asset.AssetId;
      this.Name = asset.Name;
      this.FileSize = asset.FileSize;
      this.SettingsId = asset.SettingsId;
    }
  }
}
