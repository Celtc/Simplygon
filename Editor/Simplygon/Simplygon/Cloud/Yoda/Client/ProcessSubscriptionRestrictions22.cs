﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.Client.ProcessSubscriptionRestrictions22
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Cloud.Yoda.Client
{
  internal class ProcessSubscriptionRestrictions22
  {
    public int SimultaneousJobs { get; set; }

    public int MaxReruns { get; set; }

    public int MaxTriangles { get; set; }

    public float ProcessingTimePerMonth { get; set; }

    public MeshLODRestrictions22 MeshLOD { get; set; }

    public ProxyLODRestrictions22 ProxyLOD { get; set; }

    public BoneLODRestrictions22 BoneLOD { get; set; }

    public MaterialLODRestrictions22 MaterialLOD { get; set; }

    public AggregateLODRestrictions22 AggregateLOD { get; set; }

    public ProcessSubscriptionRestrictions22()
    {
      this.SimultaneousJobs = -1;
      this.MaxReruns = -1;
      this.MaxTriangles = -1;
      this.ProcessingTimePerMonth = -1f;
      this.MeshLOD = new MeshLODRestrictions22();
      this.ProxyLOD = new ProxyLODRestrictions22();
      this.BoneLOD = new BoneLODRestrictions22();
      this.MaterialLOD = new MaterialLODRestrictions22();
      this.AggregateLOD = new AggregateLODRestrictions22();
    }
  }
}
