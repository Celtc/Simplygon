﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.Client.JobStats22
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.Collections.Generic;

namespace Simplygon.Cloud.Yoda.Client
{
  internal class JobStats22
  {
    public string UserId { get; set; }

    public string JobId { get; set; }

    public DateTime Created { get; set; }

    public DateTime Started { get; set; }

    public string JobType { get; set; }

    public string JobStatus { get; set; }

    public string InstanceId { get; set; }

    public string InstanceType { get; set; }

    public string ClientVersion { get; set; }

    public string ClientPlatform { get; set; }

    public string ClientLicense { get; set; }

    public string OutputFormats { get; set; }

    public double TotalTime { get; set; }

    public double ProcessingTime { get; set; }

    public double ProcessingCPUTime { get; set; }

    public long PeakMemoryUsage { get; set; }

    public int LODCount { get; set; }

    public List<JobStats22.JobStatsLOD22> LOD { get; private set; }

    public JobStats22()
    {
    }

    public JobStats22(string userId, string jobId, DateTime created, string jobType, int lodCount)
    {
      this.UserId = userId;
      this.JobId = jobId;
      this.Created = created;
      this.JobType = jobType;
      this.LODCount = lodCount;
      this.LOD = new List<JobStats22.JobStatsLOD22>();
      for (int index = 0; index < this.LODCount; ++index)
        this.LOD.Add(new JobStats22.JobStatsLOD22());
    }

    public void Update(JobStats22 jobStats)
    {
      throw new NotImplementedException();
    }

    public class JobStatsLOD22
    {
      public string ProcessType { get; set; }

      public string ProcessMetricType { get; set; }

      public float ProcessReductionValue { get; set; }

      public int TriangleCount { get; set; }

      public int MeshCount { get; set; }

      public int BoneCount { get; set; }

      public bool MaterialLOD { get; set; }

      public int TextureSizeX { get; set; }

      public int TextureSizeY { get; set; }

      public bool BoneLOD { get; set; }

      public string BoneMetricType { get; set; }

      public float BoneReductionValue { get; set; }
    }
  }
}
