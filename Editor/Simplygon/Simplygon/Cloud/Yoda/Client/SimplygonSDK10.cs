﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.Client.SimplygonSDK10
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.Cloud.Yoda.Client
{
  internal class SimplygonSDK10
  {
    public string SDKId { get; set; }

    public string SDKPath { get; set; }

    public DateTime Uploaded { get; set; }

    public SimplygonSDK10()
    {
    }

    public SimplygonSDK10(string sdkId, string sdkPath, DateTime uploaded)
    {
      this.SDKId = sdkId;
      this.SDKPath = sdkPath;
      this.Uploaded = uploaded;
    }

    public void Update(SimplygonSDK10 sdk)
    {
      this.Uploaded = sdk.Uploaded;
    }
  }
}
