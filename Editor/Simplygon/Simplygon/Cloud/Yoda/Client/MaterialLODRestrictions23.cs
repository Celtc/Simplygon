﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.Client.MaterialLODRestrictions23
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Cloud.Yoda.Client
{
  internal class MaterialLODRestrictions23
  {
    public bool Enabled { get; set; }

    public int MaxOutputTextureSize { get; set; }

    public MaterialLODRestrictions23()
    {
      this.Enabled = false;
      this.MaxOutputTextureSize = -1;
    }
  }
}
