﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.Client.SimplygonPreset10
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.Cloud.Yoda.Client
{
  internal class SimplygonPreset10
  {
    public string SettingsPresetId { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public string Contents { get; set; }

    public string Type { get; set; }

    public DateTime Created { get; set; }

    public void Update(SimplygonPreset10 preset)
    {
      this.SettingsPresetId = preset.SettingsPresetId;
      this.Name = preset.Name;
      this.Description = preset.Description;
      this.Contents = preset.Contents;
      this.Type = preset.Type;
      this.Created = preset.Created;
    }
  }
}
