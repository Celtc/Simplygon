﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.Client.User
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.Cloud.Yoda.Client
{
  internal class User
  {
    public string UserId { get; set; }

    public DateTime CreatedOn { get; set; }

    public string Email { get; set; }

    public string HashedPassword { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public bool IsAdmin { get; set; }

    public User()
    {
      this.UserId = Guid.NewGuid().ToString();
      this.CreatedOn = DateTime.UtcNow;
    }

    public void Update(User user)
    {
      this.Email = user.Email;
      this.HashedPassword = user.HashedPassword;
      this.FirstName = user.FirstName;
      this.LastName = user.LastName;
      this.IsAdmin = user.IsAdmin;
    }
  }
}
