﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.IntegrationClient.CloudJob
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Scene.Common.WorkDirectory;
using Simplygon.SPL;
using System;
using System.ComponentModel;
using System.Globalization;

namespace Simplygon.Cloud.Yoda.IntegrationClient
{
  internal class CloudJob : INotifyPropertyChanged
  {
    private static int counter;
    private string createdOnShort;
    private DateTime createdOn;
    private int taskProgress;
    private int totalProgress;
    private string primaryMessage;
    private string secondaryMessage;
    private readonly Simplygon.Scene.Scene currentScene;
    private string name;
    private string assetId;

    public event PropertyChangedEventHandler PropertyChanged;

    public string MappingId { get; set; }

    public ISPL SPL { get; set; }

    public WorkDirectoryInfo WorkDirectory { get; set; }

    public CloudJob.CustomData JobCustomData { get; set; }

    public IJobStateHandler StateHandler { get; protected set; }

    public string CreatedOnShort
    {
      get
      {
        return this.createdOnShort;
      }
      set
      {
        if (!(this.createdOnShort != value))
          return;
        this.createdOnShort = value;
        this.OnPropertyChanged(nameof (CreatedOnShort));
      }
    }

    public DateTime CreatedOn
    {
      get
      {
        return this.createdOn;
      }
      set
      {
        if (!(this.createdOn != value))
          return;
        this.createdOn = value;
        this.CreatedOnShort = !(DateTime.MinValue == this.createdOn) ? string.Format(CultureInfo.InvariantCulture, "{0} {1}", (object) this.CreatedOn.ToShortDateString(), (object) this.CreatedOn.ToLongTimeString()) : "Unknown";
        this.OnPropertyChanged(nameof (CreatedOn));
      }
    }

    public int TaskProgress
    {
      get
      {
        return this.taskProgress;
      }
      set
      {
        if (this.taskProgress == value)
          return;
        this.taskProgress = value;
        this.OnPropertyChanged(nameof (TaskProgress));
      }
    }

    public int TotalProgress
    {
      get
      {
        return this.totalProgress;
      }
      set
      {
        if (this.totalProgress == value)
          return;
        this.totalProgress = value;
        this.OnPropertyChanged(nameof (TotalProgress));
      }
    }

    public string PrimaryMessage
    {
      get
      {
        return this.primaryMessage;
      }
      set
      {
        if (!(this.primaryMessage != value))
          return;
        this.primaryMessage = value;
        this.OnPropertyChanged("Message");
      }
    }

    public string SecondaryMessage
    {
      get
      {
        return this.secondaryMessage;
      }
      set
      {
        if (!(this.secondaryMessage != value))
          return;
        this.secondaryMessage = value;
        this.OnPropertyChanged("Message");
      }
    }

    public string Message
    {
      get
      {
        return string.Format(CultureInfo.InvariantCulture, "{0} {1}", (object) this.primaryMessage, (object) this.secondaryMessage);
      }
    }

    public Simplygon.Scene.Scene CurrentScene
    {
      get
      {
        return this.currentScene;
      }
    }

    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        if (!(this.name != value))
          return;
        this.name = value;
        this.OnPropertyChanged(nameof (Name));
      }
    }

    public string AssetId
    {
      get
      {
        return this.assetId;
      }
      set
      {
        if (!(this.assetId != value))
          return;
        this.assetId = value;
        this.OnPropertyChanged(nameof (AssetId));
      }
    }

    public CloudJob(Simplygon.Scene.Scene scene, ISPL spl, WorkDirectoryInfo wdi, IJobStateHandler jobStateHandler)
    {
      this.currentScene = scene;
      this.SPL = spl;
      this.WorkDirectory = wdi;
      this.StateHandler = jobStateHandler;
      this.StateHandler.Job = this;
      this.TaskProgress = 0;
      this.Name = CloudJob.counter.ToString();
      ++CloudJob.counter;
      this.primaryMessage = "Initializing...";
      this.CreatedOn = DateTime.Now;
    }

    public CloudJob(Simplygon.Scene.Scene scene, string assetId, ISPL spl, WorkDirectoryInfo wdi, IJobStateHandler jobStateHandler)
    {
      this.currentScene = scene;
      this.AssetId = assetId;
      this.SPL = spl;
      this.WorkDirectory = wdi;
      this.StateHandler = jobStateHandler;
      this.StateHandler.Job = this;
      this.TaskProgress = 0;
      this.Name = CloudJob.counter.ToString();
      ++CloudJob.counter;
      this.primaryMessage = "Initializing...";
      this.CreatedOn = DateTime.Now;
    }

    public CloudJob(Simplygon.Scene.Scene scene, string assetId, CloudJob.CustomData jobCustomData, ISPL spl, WorkDirectoryInfo wdi, IJobStateHandler jobStateHandler)
      : this(scene, assetId, spl, wdi, jobStateHandler)
    {
      this.JobCustomData = jobCustomData;
    }

    public CloudJob(WorkDirectoryInfo wdi, string jobId, IJobStateHandler jobStateHandler)
    {
      this.WorkDirectory = wdi;
      this.MappingId = jobId;
      this.StateHandler = jobStateHandler;
      this.StateHandler.Job = this;
      this.TaskProgress = 0;
      this.Name = CloudJob.counter.ToString();
      ++CloudJob.counter;
      this.primaryMessage = "Initializing...";
      this.CreatedOn = DateTime.Now;
      this.currentScene = (Simplygon.Scene.Scene) null;
    }

    public void AcceptDownload()
    {
      this.StateHandler.AcceptDownload();
    }

    public void AcceptImport()
    {
      this.StateHandler.AcceptImport();
    }

    protected void OnPropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      if (this.PropertyChanged == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.PropertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }

    public class CustomData
    {
      public string SceneRootNodeId { get; set; }

      public string SceneHash { get; set; }

      public string UnityPendingLODFolderName { get; set; }

      public string ApplicationName { get; set; }

      public string ApplicationVersion { get; set; }

      public CustomData()
      {
        this.SceneRootNodeId = string.Empty;
        this.SceneHash = string.Empty;
        this.UnityPendingLODFolderName = string.Empty;
        this.ApplicationName = string.Empty;
        this.ApplicationVersion = string.Empty;
      }
    }
  }
}
