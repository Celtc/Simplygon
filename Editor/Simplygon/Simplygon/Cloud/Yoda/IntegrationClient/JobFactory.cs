﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.IntegrationClient.JobFactory
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Yoda.Client;
using Simplygon.Scene.Common.WorkDirectory;
using Simplygon.SPL;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Simplygon.Cloud.Yoda.IntegrationClient
{
  internal class JobFactory
  {
    private string apiKey = string.Empty;
    private string clientUserName = string.Empty;
    private string clientPassword = string.Empty;
    private string clientServer = string.Empty;
    private string jobType = "Mjolnir";
    private readonly Dictionary<string, Type> ServerJobStateMapping = new Dictionary<string, Type>()
    {
      {
        "Initialize",
        typeof (JobStateInitialize)
      },
      {
        "Created",
        typeof (JobStateFailed)
      },
      {
        "Uploaded",
        typeof (JobStateProcessJob)
      },
      {
        "ReadyForProcessing",
        typeof (JobStateProcessJob)
      },
      {
        "Processing",
        typeof (JobStateProcessJob)
      },
      {
        "Processed",
        typeof (JobStateAwaitingDownloadAccept)
      },
      {
        "Downloaded",
        typeof (JobStateImport)
      },
      {
        "Deleted",
        typeof (JobStateDeleted)
      },
      {
        "Failed",
        typeof (JobStateFailed)
      }
    };
    private static JobFactory instance;
    private bool isSetup;
    private Action<string> logAction;
    private Action<Simplygon.Scene.Scene, string> saveSSFAction;
    private Action<string, string, string, Action<int, string>> zipAction;
    private Action<string, string, string, Action<int, string>> unzipAction;
    private Action<List<string>, Action> importAction;
    private Action<CloudJob, List<string>, Action> importActionWithCloudJob;
    private Action<string> cleanWorkDirectoryAction;

    public static JobFactory Instance
    {
      get
      {
        if (JobFactory.instance == null)
          JobFactory.instance = new JobFactory();
        return JobFactory.instance;
      }
    }

    private JobFactory()
    {
    }

    public void Setup(string apiKey, string clientUserName, string clientPassword, string clientServer, Action<string> logAction, Action<Simplygon.Scene.Scene, string> saveSSFAction, Action<string, string, string, Action<int, string>> zipAction, Action<string, string, string, Action<int, string>> unzipAction, Action<CloudJob, List<string>, Action> importActionWithCloudJob, Action<string> cleanWorkDirectoryAction, string jobType = "Mjolnir")
    {
      this.apiKey = apiKey;
      this.clientUserName = clientUserName;
      this.clientPassword = clientPassword;
      this.clientServer = clientServer;
      this.logAction = logAction;
      this.saveSSFAction = saveSSFAction;
      this.zipAction = zipAction;
      this.unzipAction = unzipAction;
      this.importActionWithCloudJob = importActionWithCloudJob;
      this.cleanWorkDirectoryAction = cleanWorkDirectoryAction;
      this.jobType = jobType;
      this.isSetup = true;
    }

    public void Setup(string apiKey, string clientUserName, string clientPassword, string clientServer, Action<string> logAction, Action<Simplygon.Scene.Scene, string> saveSSFAction, Action<string, string, string, Action<int, string>> zipAction, Action<string, string, string, Action<int, string>> unzipAction, Action<List<string>, Action> importAction, Action<string> cleanWorkDirectoryAction, string jobType = "Mjolnir")
    {
      this.apiKey = apiKey;
      this.clientUserName = clientUserName;
      this.clientPassword = clientPassword;
      this.clientServer = clientServer;
      this.logAction = logAction;
      this.saveSSFAction = saveSSFAction;
      this.zipAction = zipAction;
      this.unzipAction = unzipAction;
      this.importAction = importAction;
      this.cleanWorkDirectoryAction = cleanWorkDirectoryAction;
      this.jobType = jobType;
      this.isSetup = true;
    }

    public void UpdateClientCredentials(string apiKey, string clientUserName, string clientPassword, string clientServer)
    {
      this.apiKey = apiKey;
      this.clientUserName = clientUserName;
      this.clientPassword = clientPassword;
      this.clientServer = clientServer;
    }

    public CloudJob CreateJob(Simplygon.Scene.Scene scene, ISPL spl, WorkDirectoryInfo currentWorkDirectory, string initialStatus = "Initialize")
    {
      if (!this.isSetup)
        throw new Exception("Unable to create job - the job factory hasn't yet been setup.");
      IJobStateHandler stateHandler = this.CreateStateHandler(initialStatus);
      return new CloudJob(scene, spl, currentWorkDirectory, stateHandler);
    }

    public CloudJob CreateJob(Simplygon.Scene.Scene scene, string assetId, ISPL spl, WorkDirectoryInfo currentWorkDirectory, string initialStatus = "Initialize")
    {
      if (!this.isSetup)
        throw new Exception("Unable to create job - the job factory hasn't yet been setup.");
      IJobStateHandler stateHandler = this.CreateStateHandler(initialStatus);
      return new CloudJob(scene, assetId, spl, currentWorkDirectory, stateHandler);
    }

    public CloudJob CreateJob(Simplygon.Scene.Scene scene, string assetId, CloudJob.CustomData customData, ISPL spl, WorkDirectoryInfo currentWorkDirectory, string initialStatus = "Initialize")
    {
      if (!this.isSetup)
        throw new Exception("Unable to create job - the job factory hasn't yet been setup.");
      IJobStateHandler stateHandler = this.CreateStateHandler(initialStatus);
      return new CloudJob(scene, assetId, customData, spl, currentWorkDirectory, stateHandler);
    }

    public CloudJob CreateJob(WorkDirectoryInfo currentWorkDirectory, string jobId, string initialStatus)
    {
      if (!this.isSetup)
        throw new Exception("Unable to create job - the job factory hasn't yet been setup.");
      IJobStateHandler stateHandler = this.CreateStateHandler(initialStatus);
      return new CloudJob(currentWorkDirectory, jobId, stateHandler);
    }

    private IJobStateHandler CreateStateHandler(string initialStatus)
    {
      IJobState initialJobState = (IJobState) new JobStateInitialize();
      Type type;
      if (this.ServerJobStateMapping.TryGetValue(initialStatus, out type))
        initialJobState = Activator.CreateInstance(type) as IJobState;
      else if (this.logAction != null)
        this.logAction(string.Format(CultureInfo.InvariantCulture, "Unable to find a suitable job state for status '{0}', job will be set in an 'initialized' state.", (object) initialStatus));
      YodaClient23 client = new YodaClient23(this.apiKey, this.clientUserName, this.clientPassword, this.clientServer);
      IJobStateHandler jobStateHandler = (IJobStateHandler) null;
      if (this.importAction != null)
        jobStateHandler = (IJobStateHandler) new JobStateHandler(client, initialJobState, this.logAction, this.saveSSFAction, this.zipAction, this.unzipAction, this.importAction, this.cleanWorkDirectoryAction, this.jobType);
      else if (this.importActionWithCloudJob != null)
        jobStateHandler = (IJobStateHandler) new JobStateHandler(client, initialJobState, this.logAction, this.saveSSFAction, this.zipAction, this.unzipAction, this.importActionWithCloudJob, this.cleanWorkDirectoryAction, this.jobType);
      return jobStateHandler;
    }
  }
}
