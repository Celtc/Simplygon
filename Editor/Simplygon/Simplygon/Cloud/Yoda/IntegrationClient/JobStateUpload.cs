﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.IntegrationClient.JobStateUpload
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Newtonsoft.Json;
using Simplygon.Cloud.Yoda.Client;
using System;
using System.IO;

namespace Simplygon.Cloud.Yoda.IntegrationClient
{
  internal class JobStateUpload : AbstractJobState
  {
    public override event EventHandler OnStateExit;

    public override void Enter(IJobStateHandlerAccessor jobStateHandlerAccessor)
    {
      this.jobStateHandlerAccessor = jobStateHandlerAccessor;
      jobStateHandlerAccessor.Job.PrimaryMessage = "Evaluating upload need...";
      jobStateHandlerAccessor.Job.TaskProgress = 0;
      jobStateHandlerAccessor.Job.TotalProgress = 15;
      this.transferProgressTotal = 5;
      jobStateHandlerAccessor.Client.TransferProgress += new EventHandler<YodaClientTransferProgress>(this.UpdateTaskProgressAsParts);
      User23 user23 = jobStateHandlerAccessor.Client.AccountInfo();
      if (user23 == null || string.IsNullOrEmpty(user23.ProcessSubscription) || !(user23.ProcessSubscription.ToLower() == "local"))
        return;
      AbstractJobState.UpdateInterval = 500;
    }

    public override bool Execute()
    {
      bool flag1 = false;
      bool flag2 = true;
      if (this.jobStateHandlerAccessor.ForceAssetUpload || this.jobStateHandlerAccessor.Client.GetAsset(this.jobStateHandlerAccessor.CurrentAssetId, (string) null) == null)
      {
        this.taskText = "Uploading asset";
        flag2 = this.UploadAssetAsParts();
        this.ResetTaskProgress();
      }
      if (flag2)
      {
        this.CreateJob();
        this.MoveTempFiles();
        this.taskText = "Uploading job settings";
        this.jobStateHandlerAccessor.Job.TotalProgress = 20;
        if (this.UploadJobSettings())
          flag1 = true;
      }
      if (flag1)
        this.jobStateHandlerAccessor.FireJobCreated();
      return flag1;
    }

    public override void Exit()
    {
      this.jobStateHandlerAccessor.Client.TransferProgress -= new EventHandler<YodaClientTransferProgress>(this.UpdateTaskProgressAsParts);
      // ISSUE: reference to a compiler-generated field
      if (this.OnStateExit == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.OnStateExit((object) this, EventArgs.Empty);
    }

    public override string AsText()
    {
      return "Upload";
    }

    private void CreateJob()
    {
      try
      {
        if (this.jobStateHandlerAccessor.Job.JobCustomData == null)
          this.jobStateHandlerAccessor.Job.JobCustomData = new CloudJob.CustomData();
        this.jobStateHandlerAccessor.Job.JobCustomData.SceneRootNodeId = this.jobStateHandlerAccessor.Job.CurrentScene.RootNode.Id.ToString();
        this.jobStateHandlerAccessor.Job.MappingId = this.jobStateHandlerAccessor.Client.CreateJob(this.jobStateHandlerAccessor.Job.Name, this.jobStateHandlerAccessor.CurrentAssetId, JsonConvert.SerializeObject((object) this.jobStateHandlerAccessor.Job.JobCustomData)).JobId;
      }
      catch (Exception ex)
      {
        throw new JobStateException("Failed to create job", ex);
      }
    }

    private void MoveTempFiles()
    {
      try
      {
        string str1 = Path.Combine(this.jobStateHandlerAccessor.Job.WorkDirectory.JobsPath, Path.Combine(this.jobStateHandlerAccessor.Job.MappingId, JobConstants.InputDirectory));
        if (!Directory.Exists(str1))
          Directory.CreateDirectory(str1);
        string[] files = Directory.GetFiles(this.jobStateHandlerAccessor.CurrentJobDirectory);
        string[] directories = Directory.GetDirectories(this.jobStateHandlerAccessor.CurrentJobDirectory);
        foreach (string str2 in files)
        {
          string destFileName = Path.Combine(str1, Path.GetFileName(str2));
          File.Move(str2, destFileName);
        }
        foreach (string str2 in directories)
        {
          string fileName = Path.GetFileName(str2);
          Directory.Move(str2, Path.Combine(str1, fileName));
        }
        Directory.Delete(this.jobStateHandlerAccessor.CurrentJobDirectory, true);
        this.jobStateHandlerAccessor.CurrentJobDirectory = str1;
      }
      catch (Exception ex)
      {
        throw new JobStateException("Failed to move temporary files", ex);
      }
    }
  }
}
