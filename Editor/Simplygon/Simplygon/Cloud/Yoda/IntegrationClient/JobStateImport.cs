﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.IntegrationClient.JobStateImport
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Globalization;

namespace Simplygon.Cloud.Yoda.IntegrationClient
{
  internal class JobStateImport : AbstractJobState
  {
    public override event EventHandler OnStateExit;

    public override void Enter(IJobStateHandlerAccessor jobStateHandlerAccessor)
    {
      this.jobStateHandlerAccessor = jobStateHandlerAccessor;
      jobStateHandlerAccessor.Job.PrimaryMessage = "Importing...";
      jobStateHandlerAccessor.Job.TaskProgress = 0;
      jobStateHandlerAccessor.Job.TotalProgress = 99;
    }

    public override bool Execute()
    {
      bool flag = true;
      if (this.jobStateHandlerAccessor.Job.SPL == null)
        this.jobStateHandlerAccessor.Job.SPL = (ISPL) Simplygon.SPL.v80.SPL.Load(Path.Combine(this.jobStateHandlerAccessor.CurrentJobDirectory, JobConstants.SPLFileName));
      List<string> lodFilePaths = new List<string>();
      if (this.jobStateHandlerAccessor.Job.SPL is Simplygon.SPL.v1.SPL)
        lodFilePaths = ((Simplygon.SPL.v1.SPL) this.jobStateHandlerAccessor.Job.SPL).GetLODFilePaths(this.jobStateHandlerAccessor.CurrentJobDirectory);
      else if (this.jobStateHandlerAccessor.Job.SPL is Simplygon.SPL.v70.SPL)
        lodFilePaths = ((Simplygon.SPL.v70.SPL) this.jobStateHandlerAccessor.Job.SPL).GetLODFilePaths(this.jobStateHandlerAccessor.CurrentJobDirectory);
      else if (this.jobStateHandlerAccessor.Job.SPL is Simplygon.SPL.v80.SPL)
        lodFilePaths = ((Simplygon.SPL.v80.SPL) this.jobStateHandlerAccessor.Job.SPL).GetLODFilePaths(this.jobStateHandlerAccessor.CurrentJobDirectory);
      try
      {
        this.jobStateHandlerAccessor.FireImportAction(lodFilePaths, (Action) (() => {}));
        this.jobStateHandlerAccessor.FireJobImported();
      }
      catch (Exception ex)
      {
        throw new JobStateException(string.Format(CultureInfo.InvariantCulture, "Failed to import scene ({0})", (object) ex.Message), ex);
      }
      return flag;
    }

    public override void Exit()
    {
      // ISSUE: reference to a compiler-generated field
      if (this.OnStateExit == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.OnStateExit((object) this, EventArgs.Empty);
    }

    public override string AsText()
    {
      return "Import";
    }
  }
}
