﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.IntegrationClient.IJobStateHandler
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Yoda.Client;
using System;

namespace Simplygon.Cloud.Yoda.IntegrationClient
{
  internal interface IJobStateHandler
  {
    event EventHandler OnStateChanged;

    event EventHandler<JobStateChangedEventargs> OnJobInitialized;

    event EventHandler<JobStateChangedEventargs> OnJobCreated;

    event EventHandler<JobStateChangedEventargs> OnJobReadyForDownload;

    event EventHandler<JobStateChangedEventargs> OnJobReadyForImport;

    event EventHandler<JobStateChangedEventargs> OnJobImported;

    event EventHandler<JobStateChangedEventargs> OnJobDeleted;

    event EventHandler<JobFailedEventargs> OnJobFailed;

    YodaClient23 Client { get; set; }

    CloudJob Job { get; set; }

    string CurrentSPLId { get; }

    string CurrentJobDirectory { get; }

    bool IsJobBeingCreated { get; }

    bool IsJobBeingUploaded { get; }

    bool IsJobAwaitingDownloadAccept { get; }

    bool IsJobBeingDownloaded { get; }

    bool IsJobAwaitingImportAccept { get; }

    bool IsJobBeingImported { get; }

    bool IsJobCompleted { get; }

    bool IsJobDeletionRequested { get; }

    bool IsJobBeingDeleted { get; }

    bool IsJobDeleted { get; }

    bool IsJobFailed { get; }

    bool IsJobInFinalState { get; }

    bool ForceAssetUpload { get; }

    void Run(bool forceAssetUpload = false);

    void Reimport();

    void RequestJobDeletion();

    void Stop();

    void AcceptDownload();

    void AcceptImport();
  }
}
