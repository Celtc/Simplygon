﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.IntegrationClient.JobStateFailed
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Yoda.Client;
using System;
using System.Globalization;

namespace Simplygon.Cloud.Yoda.IntegrationClient
{
  internal class JobStateFailed : AbstractJobState
  {
    private static string DefaultErrorMessage = "Failed";
    private string errorMessage = JobStateFailed.DefaultErrorMessage;

    public override event EventHandler OnStateExit;

    public JobStateFailed()
    {
    }

    public JobStateFailed(string errorMessage)
    {
      this.errorMessage = errorMessage;
    }

    public override void Enter(IJobStateHandlerAccessor jobStateHandlerAccessor)
    {
      this.jobStateHandlerAccessor = jobStateHandlerAccessor;
      jobStateHandlerAccessor.Job.PrimaryMessage = this.errorMessage;
      jobStateHandlerAccessor.Job.TaskProgress = 100;
      jobStateHandlerAccessor.Job.TotalProgress = 100;
    }

    public override bool Execute()
    {
      bool flag = true;
      try
      {
        if (!string.IsNullOrEmpty(this.jobStateHandlerAccessor.Job.MappingId))
        {
          Job23 job = (Job23) null;
          if (this.GetJob(ref job))
          {
            if (job.Status == "Failed")
            {
              if (!string.IsNullOrEmpty(job.ErrorMessage))
                this.errorMessage = job.ErrorMessage;
            }
          }
        }
      }
      catch (Exception ex)
      {
        this.errorMessage = string.Format(CultureInfo.InvariantCulture, "{0} ({1})", (object) this.errorMessage, (object) ex.Message);
      }
      finally
      {
        if (this.errorMessage != JobStateFailed.DefaultErrorMessage)
          this.jobStateHandlerAccessor.Job.PrimaryMessage = string.Format(CultureInfo.InvariantCulture, "Failed: {0}", (object) this.errorMessage);
      }
      this.jobStateHandlerAccessor.FireJobFailed(this.errorMessage);
      this.jobStateHandlerAccessor.FireCleanWorkDirectoryAction(this.jobStateHandlerAccessor.CurrentJobDirectory);
      return flag;
    }

    public override void Exit()
    {
      // ISSUE: reference to a compiler-generated field
      if (this.OnStateExit == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.OnStateExit((object) this, EventArgs.Empty);
    }

    public override string AsText()
    {
      return "Failed";
    }
  }
}
