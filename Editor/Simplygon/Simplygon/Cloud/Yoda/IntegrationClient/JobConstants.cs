﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.IntegrationClient.JobConstants
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Simplygon.Cloud.Yoda.IntegrationClient
{
  internal class JobConstants
  {
    public static readonly string InputDirectory = "Input";
    public static readonly string OutputDirectory = "Output";
    public static readonly string InputPackedFileName = "input.zip";
    public static readonly string OutputPackedFileName = "output.zip";
    public static readonly string SSFFileName = "input.ssf";
    public static readonly string SPLFileName = "input.spl";
    public static readonly string LODFileName = "output_lod{0}.ssf";
  }
}
