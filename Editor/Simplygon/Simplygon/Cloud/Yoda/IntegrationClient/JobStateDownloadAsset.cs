﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.IntegrationClient.JobStateDownloadAsset
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Yoda.Client;
using Simplygon.Scene.Common.WorkDirectory;
using System;
using System.IO;
using System.Threading;
using System.Globalization;

namespace Simplygon.Cloud.Yoda.IntegrationClient
{
  internal class JobStateDownloadAsset : AbstractJobState
  {
    private bool useComputedAssetId;
    private string zipFilePath;

    public override event EventHandler OnStateExit;

    public override void Enter(IJobStateHandlerAccessor jobStateHandlerAccessor)
    {
      this.jobStateHandlerAccessor = jobStateHandlerAccessor;
      jobStateHandlerAccessor.Job.PrimaryMessage = "Preparing to download...";
      jobStateHandlerAccessor.Job.TaskProgress = 0;
      jobStateHandlerAccessor.Job.TotalProgress = 80;
      this.transferProgressTotal = 10;
      this.UpdateTransferProgressTotalBytes();
      Thread.Sleep(1000);
      WorkDirectoryInfo workDirectory = jobStateHandlerAccessor.Job.WorkDirectory;
      string path1 = (this.useComputedAssetId = string.IsNullOrEmpty(jobStateHandlerAccessor.Job.MappingId)) ? string.Format(CultureInfo.InvariantCulture, "TempDownload-{0}", (object) Guid.NewGuid().ToString()) : jobStateHandlerAccessor.Job.MappingId;
      jobStateHandlerAccessor.CurrentJobDirectory = Path.Combine(workDirectory.JobsPath, Path.Combine(path1, JobConstants.OutputDirectory));
      if (!Directory.Exists(workDirectory.JobsPath))
        Directory.CreateDirectory(workDirectory.JobsPath);
      if (!Directory.Exists(jobStateHandlerAccessor.CurrentJobDirectory))
        Directory.CreateDirectory(jobStateHandlerAccessor.CurrentJobDirectory);
      this.zipFilePath = Path.Combine(jobStateHandlerAccessor.CurrentJobDirectory, JobConstants.OutputPackedFileName);
      jobStateHandlerAccessor.Client.TransferProgress += new EventHandler<YodaClientTransferProgress>(this.UpdateTaskProgress);
    }

    public override bool Execute()
    {
      bool flag = false;
      this.taskText = "Downloading asset";
      if (this.useComputedAssetId)
      {
        this.DownloadAsset(this.jobStateHandlerAccessor.CurrentAssetId, this.zipFilePath);
        flag = true;
      }
      else
      {
        Job23 job = (Job23) null;
        if (this.GetJob(ref job))
        {
          this.DownloadAsset(job.OutputAssetId, this.zipFilePath);
          flag = true;
        }
      }
      return flag;
    }

    public override void Exit()
    {
      this.jobStateHandlerAccessor.Client.TransferProgress -= new EventHandler<YodaClientTransferProgress>(this.UpdateTaskProgress);
      // ISSUE: reference to a compiler-generated field
      if (this.OnStateExit == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.OnStateExit((object) this, EventArgs.Empty);
    }

    public override string AsText()
    {
      return "Download asset";
    }
  }
}
