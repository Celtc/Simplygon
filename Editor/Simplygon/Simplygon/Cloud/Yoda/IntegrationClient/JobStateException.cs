﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.IntegrationClient.JobStateException
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Simplygon.Cloud.Yoda.IntegrationClient
{
  internal class JobStateException : Exception
  {
    public JobStateException(string message)
      : base(message)
    {
    }

    public JobStateException(string message, Exception innerException)
      : base(message, innerException)
    {
    }
  }
}
