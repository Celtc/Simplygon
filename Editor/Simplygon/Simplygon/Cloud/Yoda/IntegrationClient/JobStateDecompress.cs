﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.IntegrationClient.JobStateDecompress
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.IO;
using System.Globalization;

namespace Simplygon.Cloud.Yoda.IntegrationClient
{
  internal class JobStateDecompress : AbstractJobState
  {
    public override event EventHandler OnStateExit;

    public override void Enter(IJobStateHandlerAccessor jobStateHandlerAccessor)
    {
      this.jobStateHandlerAccessor = jobStateHandlerAccessor;
      jobStateHandlerAccessor.Job.PrimaryMessage = "Decompressing...";
      jobStateHandlerAccessor.Job.TaskProgress = 0;
      jobStateHandlerAccessor.Job.TotalProgress = 90;
    }

    public override bool Execute()
    {
      int num = 1;
      this.jobStateHandlerAccessor.FireUnzipAction(this.jobStateHandlerAccessor.CurrentJobDirectory, this.jobStateHandlerAccessor.CurrentJobDirectory, JobConstants.OutputPackedFileName, (Action<int, string>) ((progress, fileName) =>
      {
        this.jobStateHandlerAccessor.Job.PrimaryMessage = string.Format(CultureInfo.InvariantCulture, "Decompressing {0}", (object) Path.GetFileName(fileName));
        this.jobStateHandlerAccessor.Job.TaskProgress = progress;
      }));
      return num != 0;
    }

    public override void Exit()
    {
      // ISSUE: reference to a compiler-generated field
      if (this.OnStateExit == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.OnStateExit((object) this, EventArgs.Empty);
    }

    public override string AsText()
    {
      return "Decompress";
    }
  }
}
