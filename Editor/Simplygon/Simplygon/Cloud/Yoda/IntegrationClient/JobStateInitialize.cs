﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.IntegrationClient.JobStateInitialize
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Scene.Asset;
using Simplygon.Scene.Common.WorkDirectory;
using Simplygon.Scene.Graph;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Globalization;

namespace Simplygon.Cloud.Yoda.IntegrationClient
{
  internal class JobStateInitialize : AbstractJobState
  {
    public override event EventHandler OnStateExit;

    public override void Enter(IJobStateHandlerAccessor jobStateHandlerAccessor)
    {
      this.jobStateHandlerAccessor = jobStateHandlerAccessor;
      jobStateHandlerAccessor.Job.PrimaryMessage = "Initializing...";
      jobStateHandlerAccessor.Job.TaskProgress = 0;
      jobStateHandlerAccessor.Job.TotalProgress = 0;
    }

    public override bool Execute()
    {
      bool flag = true;
      WorkDirectoryInfo workDirectory = this.jobStateHandlerAccessor.Job.WorkDirectory;
      this.jobStateHandlerAccessor.CurrentJobDirectory = Path.Combine(workDirectory.JobsPath, string.Format(CultureInfo.InvariantCulture, "TempUpload-{0}", (object) Guid.NewGuid().ToString()));
      if (!Directory.Exists(workDirectory.JobsPath))
        Directory.CreateDirectory(workDirectory.JobsPath);
      if (!Directory.Exists(this.jobStateHandlerAccessor.CurrentJobDirectory))
        Directory.CreateDirectory(this.jobStateHandlerAccessor.CurrentJobDirectory);
      string ssfFilePath = Path.Combine(this.jobStateHandlerAccessor.CurrentJobDirectory, JobConstants.SSFFileName);
      Path.Combine(this.jobStateHandlerAccessor.CurrentJobDirectory, JobConstants.InputPackedFileName);
      string str1 = Path.Combine(this.jobStateHandlerAccessor.CurrentJobDirectory, JobConstants.SPLFileName);
      string empty1 = string.Empty;
      string empty2 = string.Empty;
      this.SaveToSSF(ssfFilePath);
      this.jobStateHandlerAccessor.Job.TotalProgress = 3;
      this.CreateZip();
      this.jobStateHandlerAccessor.Job.TotalProgress = 6;
      this.SaveSPL(str1);
      this.jobStateHandlerAccessor.Job.TotalProgress = 10;
      if (string.IsNullOrEmpty(this.jobStateHandlerAccessor.Job.AssetId))
      {
        string[] files = Directory.GetFiles(workDirectory.ExternalDataPath);
        List<string> stringList = new List<string>();
        foreach (string filename in files)
        {
          string file = this.ComputeFile(filename);
          stringList.Add(file);
        }
        foreach (KeyValuePair<Guid, Mesh> mesh in this.jobStateHandlerAccessor.Job.CurrentScene.Meshes)
        {
          string guid = this.ComputeGuid(mesh.Key);
          stringList.Add(guid);
        }
        foreach (KeyValuePair<Guid, Material> material in this.jobStateHandlerAccessor.Job.CurrentScene.Materials)
        {
          string guid = this.ComputeGuid(material.Key);
          stringList.Add(guid);
        }
        foreach (KeyValuePair<Guid, Node> node in this.jobStateHandlerAccessor.Job.CurrentScene.Nodes)
        {
          string guid = this.ComputeGuid(node.Key);
          stringList.Add(guid);
        }
        stringList.Add(this.jobStateHandlerAccessor.Job.CurrentScene.RootNode.Id.ToString());
        stringList.Sort();
        string empty3 = string.Empty;
        foreach (string str2 in stringList)
          empty3 += str2;
        this.jobStateHandlerAccessor.CurrentAssetId = this.ComputeString(empty3);
      }
      else
        this.jobStateHandlerAccessor.CurrentAssetId = this.jobStateHandlerAccessor.Job.AssetId;
      this.jobStateHandlerAccessor.CurrentSPLId = this.ComputeFile(str1);
      this.jobStateHandlerAccessor.FireJobInitialized();
      return flag;
    }

    public override void Exit()
    {
      // ISSUE: reference to a compiler-generated field
      if (this.OnStateExit == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.OnStateExit((object) this, EventArgs.Empty);
    }

    public override string AsText()
    {
      return "Initialize";
    }

    private void SaveToSSF(string ssfFilePath)
    {
      this.jobStateHandlerAccessor.Job.PrimaryMessage = "Writing SSF...";
      try
      {
        this.jobStateHandlerAccessor.FireSaveSSFAction(this.jobStateHandlerAccessor.Job.CurrentScene, ssfFilePath);
      }
      catch (Exception ex)
      {
        throw new JobStateException(string.Format(CultureInfo.InvariantCulture, "Write to disk failed ({0})", (object) ex.Message), ex);
      }
    }

    private void CreateZip()
    {
      this.jobStateHandlerAccessor.Job.PrimaryMessage = "Compressing...";
      try
      {
        this.jobStateHandlerAccessor.FireZipAction(this.jobStateHandlerAccessor.CurrentJobDirectory, this.jobStateHandlerAccessor.CurrentJobDirectory, JobConstants.InputPackedFileName, (Action<int, string>) ((progress, fileName) =>
        {
          this.jobStateHandlerAccessor.Job.PrimaryMessage = string.Format(CultureInfo.InvariantCulture, "Compressing {0}", (object) Path.GetFileName(fileName));
          this.jobStateHandlerAccessor.Job.TaskProgress = progress;
        }));
      }
      catch (Exception ex)
      {
        throw new JobStateException("Compress failed", ex);
      }
    }

    private void SaveSPL(string splFilePath)
    {
      this.jobStateHandlerAccessor.Job.PrimaryMessage = "Writing SPL...";
      if (this.jobStateHandlerAccessor.Job.SPL is Simplygon.SPL.v1.SPL)
        ((Simplygon.SPL.v1.SPL) this.jobStateHandlerAccessor.Job.SPL).Save(splFilePath);
      else if (this.jobStateHandlerAccessor.Job.SPL is Simplygon.SPL.v70.SPL)
      {
        ((Simplygon.SPL.v70.SPL) this.jobStateHandlerAccessor.Job.SPL).Save(splFilePath);
      }
      else
      {
        if (!(this.jobStateHandlerAccessor.Job.SPL is Simplygon.SPL.v80.SPL))
          return;
        ((Simplygon.SPL.v80.SPL) this.jobStateHandlerAccessor.Job.SPL).Save(splFilePath);
      }
    }

    private string ComputeFile(string filename)
    {
      string empty = string.Empty;
      try
      {
        using (FileStream fileStream = new FileStream(filename, FileMode.Open))
          return BitConverter.ToString(SHA256.Create().ComputeHash((Stream) fileStream)).Replace("-", "");
      }
      catch (Exception ex)
      {
        throw new JobStateException("Failed to compute file hash", ex);
      }
    }

    private string ComputeString(string value)
    {
      string empty = string.Empty;
      try
      {
        return BitConverter.ToString(SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(value))).Replace("-", "");
      }
      catch (Exception ex)
      {
        throw new JobStateException("Failed to compute string hash", ex);
      }
    }

    private string ComputeGuid(Guid value)
    {
      string empty = string.Empty;
      try
      {
        return BitConverter.ToString(SHA256.Create().ComputeHash(value.ToByteArray())).Replace("-", "");
      }
      catch (Exception ex)
      {
        throw new JobStateException("Failed to compute Guid hash", ex);
      }
    }
  }
}
