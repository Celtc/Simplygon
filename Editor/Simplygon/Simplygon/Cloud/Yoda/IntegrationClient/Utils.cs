﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.IntegrationClient.Utils
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.SPL;
using System;
using System.Globalization;

namespace Simplygon.Cloud.Yoda.IntegrationClient
{
  internal class Utils
  {
    public static string GetTransferFactor(double originalNumberOfBytes, out double divisor)
    {
      divisor = 1.0;
      int num1 = 0;
      double num2 = originalNumberOfBytes;
      while (num2 >= 1024.0)
      {
        ++num1;
        divisor *= 1024.0;
        num2 /= 1024.0;
      }
      string empty = string.Empty;
      switch (num1)
      {
        case 0:
          return "bytes";
        case 1:
          return "KB";
        case 2:
          return "MB";
        case 3:
          return "GB";
        case 4:
          return "TB";
        default:
          throw new Exception(string.Format(CultureInfo.InvariantCulture, "Unexpected '{0}' base 1024 steps.", (object) num1));
      }
    }

    public static int GetNumberOfLods(ISPL spl)
    {
      int num = 0;
      if (spl is Simplygon.SPL.v1.SPL)
        num = Utils.GetNumberOfLods(((Simplygon.SPL.v1.SPL) spl).ProcessGraph);
      else if (spl is Simplygon.SPL.v70.SPL)
        num = Utils.GetNumberOfLods(((Simplygon.SPL.v70.SPL) spl).ProcessGraph);
      else if (spl is Simplygon.SPL.v80.SPL)
        num = Utils.GetNumberOfLods(((Simplygon.SPL.v80.SPL) spl).ProcessGraph);
      return num;
    }

    protected static int GetNumberOfLods(Simplygon.SPL.v1.Base.BaseNode splNode)
    {
      int num = 0;
      if (splNode.Type == "ProcessNode")
        ++num;
      foreach (Simplygon.SPL.v1.Base.BaseNode child in splNode.Children)
        num += Utils.GetNumberOfLods(child);
      return num;
    }

    protected static int GetNumberOfLods(Simplygon.SPL.v70.Base.BaseNode splNode)
    {
      int num = 0;
      if (splNode.Type == "ProcessNode")
        ++num;
      foreach (Simplygon.SPL.v70.Base.BaseNode child in splNode.Children)
        num += Utils.GetNumberOfLods(child);
      return num;
    }

    protected static int GetNumberOfLods(Simplygon.SPL.v80.Base.BaseNode splNode)
    {
      int num = 0;
      if (splNode.Type == "ProcessNode")
        ++num;
      foreach (Simplygon.SPL.v80.Base.BaseNode child in splNode.Children)
        num += Utils.GetNumberOfLods(child);
      return num;
    }
  }
}
