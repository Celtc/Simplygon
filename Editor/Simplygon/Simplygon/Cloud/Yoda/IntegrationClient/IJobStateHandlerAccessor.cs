﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.IntegrationClient.IJobStateHandlerAccessor
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Yoda.Client;
using System;
using System.Collections.Generic;

namespace Simplygon.Cloud.Yoda.IntegrationClient
{
  internal interface IJobStateHandlerAccessor
  {
    CloudJob Job { get; set; }

    YodaClient23 Client { get; }

    bool InitiateDownloadAccepted { get; }

    bool InitiateImportAccepted { get; }

    bool ForceAssetUpload { get; }

    string CurrentAssetId { get; set; }

    string CurrentSPLId { get; set; }

    string CurrentJobDirectory { get; set; }

    bool IsJobDeletionRequested { get; set; }

    void Log(string message);

    void AcceptDownload();

    void AcceptImport();

    void FireJobInitialized();

    void FireJobCreated();

    void FireJobReadyForDownload();

    void FireJobReadyForImport();

    void FireJobImported();

    void FireJobDeleted();

    void FireJobFailed(string message);

    void FireSaveSSFAction(Simplygon.Scene.Scene scene, string sceneFilePath);

    void FireZipAction(string inputDirectory, string outputDirectory, string fileName, Action<int, string> progressHandler);

    void FireUnzipAction(string inputDirectory, string outputDirectory, string fileName, Action<int, string> progressHandler);

    void FireImportAction(List<string> lodFilePaths, Action onCompleted);

    void FireCleanWorkDirectoryAction(string jobDirectory);
  }
}
