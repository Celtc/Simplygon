﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.IntegrationClient.JobStateDeleteJob
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Yoda.Client;
using System;

namespace Simplygon.Cloud.Yoda.IntegrationClient
{
  internal class JobStateDeleteJob : AbstractJobState
  {
    public override event EventHandler OnStateExit;

    public override void Enter(IJobStateHandlerAccessor jobStateHandlerAccessor)
    {
      this.jobStateHandlerAccessor = jobStateHandlerAccessor;
      jobStateHandlerAccessor.Job.PrimaryMessage = "Cancelling job...";
      jobStateHandlerAccessor.Job.SecondaryMessage = string.Empty;
      jobStateHandlerAccessor.Job.TaskProgress = 0;
      jobStateHandlerAccessor.Job.TotalProgress = 100;
      jobStateHandlerAccessor.IsJobDeletionRequested = false;
    }

    public override bool Execute()
    {
      int num = 1;
      if (!string.IsNullOrEmpty(this.jobStateHandlerAccessor.Job.MappingId))
      {
        Job23 job = (Job23) null;
        if (this.GetJob(ref job))
          this.jobStateHandlerAccessor.Client.DeleteJob(job.JobId);
      }
      this.jobStateHandlerAccessor.FireCleanWorkDirectoryAction(this.jobStateHandlerAccessor.CurrentJobDirectory);
      return num != 0;
    }

    public override void Exit()
    {
      // ISSUE: reference to a compiler-generated field
      if (this.OnStateExit == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.OnStateExit((object) this, EventArgs.Empty);
    }

    public override string AsText()
    {
      return "Delete job";
    }
  }
}
