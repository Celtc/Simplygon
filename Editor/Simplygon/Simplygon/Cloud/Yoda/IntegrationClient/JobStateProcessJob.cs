﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.IntegrationClient.JobStateProcessJob
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Yoda.Client;
using System;
using System.Globalization;

namespace Simplygon.Cloud.Yoda.IntegrationClient
{
  internal class JobStateProcessJob : AbstractJobState
  {
    private readonly int ProcessingProgressPart = 20;
    private DateTime lastUpdate = DateTime.MinValue;
    private int lastTotalProgressIncrease;

    public override event EventHandler OnStateExit;

    public override void Enter(IJobStateHandlerAccessor jobStateHandlerAccessor)
    {
      this.jobStateHandlerAccessor = jobStateHandlerAccessor;
      jobStateHandlerAccessor.Job.PrimaryMessage = "Initializing processing...";
      jobStateHandlerAccessor.Job.TaskProgress = 0;
      jobStateHandlerAccessor.Job.TotalProgress = 30;
    }

    public override bool Execute()
    {
      bool flag = false;
      Job23 job = (Job23) null;
      if (DateTime.Now.Subtract(this.lastUpdate).TotalMilliseconds >= (double) AbstractJobState.UpdateInterval && this.GetJob(ref job))
      {
        this.lastUpdate = DateTime.Now;
        if ("Uploaded" == job.Status)
        {
          this.jobStateHandlerAccessor.Job.PrimaryMessage = "Uploaded";
          this.jobStateHandlerAccessor.Job.TotalProgress = 30;
          this.ProcessJob();
        }
        else if ("ReadyForProcessing" == job.Status)
        {
          this.jobStateHandlerAccessor.Job.PrimaryMessage = "Queued";
          this.jobStateHandlerAccessor.Job.TotalProgress = 40;
        }
        else if ("Processing" == job.Status)
        {
          int num1 = int.Parse(job.ProgressPercentage);
          int num2 = int.Parse(job.ProgressLODIndex);
          if ("Processing" == job.ProgressCategory && num1 > 0)
          {
            this.jobStateHandlerAccessor.Job.PrimaryMessage = string.Format(CultureInfo.InvariantCulture, "Processing LOD {0} of {1}", (object) job.ProgressLODIndex, (object) job.ProgressLODCount);
            int num3 = int.Parse(job.ProgressLODCount);
            this.jobStateHandlerAccessor.Job.TaskProgress = num1;
            float num4 = num3 == 0 ? 0.0f : (float) this.ProcessingProgressPart / (float) num3;
            int num5 = (int) ((double) num4 * ((double) num2 - 1.0) + (double) num4 * (double) num1 * 0.00999999977648258);
            this.jobStateHandlerAccessor.Job.TotalProgress += num5 - this.lastTotalProgressIncrease;
            this.lastTotalProgressIncrease = num5;
          }
          else
          {
            if (this.jobStateHandlerAccessor.Job.TotalProgress < 50)
              this.jobStateHandlerAccessor.Job.TotalProgress = 50;
            if (num2 > 0)
              this.jobStateHandlerAccessor.Job.PrimaryMessage = string.Format(CultureInfo.InvariantCulture, "Initializing processing LOD {0}", (object) num2);
            else
              this.jobStateHandlerAccessor.Job.PrimaryMessage = "Initializing processing...";
          }
        }
        else if ("Processed" == job.Status)
        {
          this.jobStateHandlerAccessor.Job.PrimaryMessage = "Processed";
          this.jobStateHandlerAccessor.Job.TotalProgress = 70;
          flag = true;
        }
        else
        {
          if (!("Failed" == job.Status) && !("Deleted" == job.Status))
            throw new JobStateException(string.Format(CultureInfo.InvariantCulture, "Processing failed, unexpected status '{0}'.", (object) job.Status));
          flag = true;
        }
      }
      return flag;
    }

    public override void Exit()
    {
      // ISSUE: reference to a compiler-generated field
      if (this.OnStateExit == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.OnStateExit((object) this, EventArgs.Empty);
    }

    public override string AsText()
    {
      return "Processing";
    }
  }
}
