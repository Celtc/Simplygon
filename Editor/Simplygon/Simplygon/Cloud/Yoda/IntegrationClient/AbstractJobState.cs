﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.IntegrationClient.AbstractJobState
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Yoda.Client;
using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;

namespace Simplygon.Cloud.Yoda.IntegrationClient
{
  internal abstract class AbstractJobState : IJobState
  {
    protected static int UpdateInterval = 10000;
    private readonly int MaxMbTransferPartSize = 5;
    private readonly int MaxGetJobRetries = 10;
    private readonly int MaxUploadRetries = 3;
    private readonly int MaxProcessRetries = 3;
    private readonly int MaxDownloadRetries = 3;
    protected string taskText = string.Empty;
    private int currentPartNumber = 1;
    private int assetBytesTotal = -1;
    private DateTime lastTransferProgressUpdate = DateTime.MinValue;
    protected int transferProgressTotal;
    private long currentFileSize;
    private int currentRetries;
    private int lastTotalProgressIncrease;
    private double lastBytesTransferred;
    protected IJobStateHandlerAccessor jobStateHandlerAccessor;

    public abstract event EventHandler OnStateExit;

    public abstract void Enter(IJobStateHandlerAccessor jobStateHandlerAccessor);

    public abstract bool Execute();

    public abstract void Exit();

    public abstract string AsText();

    protected bool GetJob(ref Job23 job)
    {
      try
      {
        if ((job = this.jobStateHandlerAccessor.Client.GetJob(this.jobStateHandlerAccessor.Job.MappingId)) == null)
          throw new JobStateException("Failed to get job");
        this.currentRetries = 0;
        this.jobStateHandlerAccessor.Job.SecondaryMessage = string.Empty;
      }
      catch (Exception ex)
      {
        if (this.currentRetries > this.MaxGetJobRetries)
          throw new JobStateException("Failed to get job", ex);
        ++this.currentRetries;
        Thread.Sleep(3000);
      }
      return job != null;
    }

    protected bool UploadAsset()
    {
      Asset23 asset23 = (Asset23) null;
      try
      {
        string filename = Path.Combine(this.jobStateHandlerAccessor.CurrentJobDirectory, JobConstants.InputPackedFileName);
        if ((asset23 = this.jobStateHandlerAccessor.Client.UploadAsset(filename, this.jobStateHandlerAccessor.Job.Name, this.jobStateHandlerAccessor.CurrentAssetId)) == null)
          throw new JobStateException("Failed to upload asset");
        this.currentRetries = 0;
        this.jobStateHandlerAccessor.Job.SecondaryMessage = string.Empty;
      }
      catch (Exception ex)
      {
        if (this.currentRetries >= this.MaxUploadRetries)
          throw new JobStateException("Failed to upload asset", ex);
        ++this.currentRetries;
        this.jobStateHandlerAccessor.Job.SecondaryMessage = string.Format(CultureInfo.InvariantCulture, "(retry {0} of {1})", (object) this.currentRetries, (object) this.MaxDownloadRetries);
        Thread.Sleep(3000);
      }
      return asset23 != null;
    }

    protected bool UploadAssetAsParts()
    {
      bool flag1 = false;
      try
      {
        Upload23 upload23 = this.jobStateHandlerAccessor.Client.UploadAssetPart_Init(this.jobStateHandlerAccessor.Job.Name, this.jobStateHandlerAccessor.CurrentAssetId);
        this.currentPartNumber = 1;
        this.currentFileSize = 0L;
        using (FileStream fileStream = new FileStream(Path.Combine(this.jobStateHandlerAccessor.CurrentJobDirectory, JobConstants.InputPackedFileName), FileMode.Open))
        {
          this.currentFileSize = fileStream.Length;
          byte[] buffer = new byte[this.MaxMbTransferPartSize * 1024 * 1024];
          long num = 0;
          int count;
          while ((count = fileStream.Read(buffer, 0, buffer.Length)) > 0)
          {
            using (MemoryStream memoryStream = new MemoryStream(buffer, 0, count))
            {
              if (this.jobStateHandlerAccessor.Client.UploadAssetPart((Stream) memoryStream, upload23.UploadId, this.currentPartNumber) == null)
                throw new JobStateException("Failed to upload asset");
              num += (long) count;
              ++this.currentPartNumber;
            }
          }
        }
        this.jobStateHandlerAccessor.Client.UploadAssetPart_Complete(upload23.UploadId, this.currentPartNumber - 1, this.currentFileSize);
        this.jobStateHandlerAccessor.Job.PrimaryMessage = "Finalizing asset upload...";
        string uploadId = upload23.UploadId;
        bool flag2 = true;
        while (flag2)
        {
          upload23 = this.jobStateHandlerAccessor.Client.UploadAssetPart_Get(upload23.UploadId);
          if (!string.IsNullOrEmpty(upload23.AssetId))
            flag2 = false;
          Thread.Sleep(AbstractJobState.UpdateInterval);
        }
        flag1 = true;
      }
      catch (WebException ex)
      {
        if (WebExceptionStatus.RequestCanceled != ex.Status)
        {
          if (WebExceptionStatus.Timeout != ex.Status)
            throw;
        }
      }
      catch (Exception ex)
      {
        if (this.currentRetries >= this.MaxUploadRetries)
          throw new JobStateException("Failed to upload asset", ex);
        ++this.currentRetries;
        this.jobStateHandlerAccessor.Job.SecondaryMessage = string.Format(CultureInfo.InvariantCulture, "(retry {0} of {1})", (object) this.currentRetries, (object) this.MaxDownloadRetries);
        Thread.Sleep(3000);
      }
      return flag1;
    }

    protected bool UploadJobSettings()
    {
      bool flag = false;
      try
      {
        flag = this.jobStateHandlerAccessor.Client.UploadJobSettings(this.jobStateHandlerAccessor.Job.MappingId, Path.Combine(this.jobStateHandlerAccessor.CurrentJobDirectory, JobConstants.SPLFileName));
        this.currentRetries = 0;
        this.jobStateHandlerAccessor.Job.SecondaryMessage = string.Empty;
      }
      catch (Exception ex)
      {
        if (this.currentRetries >= this.MaxUploadRetries)
          throw new JobStateException("Failed to upload job settings", ex);
        ++this.currentRetries;
        this.jobStateHandlerAccessor.Job.SecondaryMessage = string.Format(CultureInfo.InvariantCulture, "(retry {0} of {1})", (object) this.currentRetries, (object) this.MaxDownloadRetries);
        Thread.Sleep(3000);
      }
      return flag;
    }

    protected bool ProcessJob()
    {
      bool flag = false;
      try
      {
        if (!(flag = this.jobStateHandlerAccessor.Client.ProcessJob(this.jobStateHandlerAccessor.Job.MappingId)))
          throw new JobStateException("Failed to start processing job.");
        this.currentRetries = 0;
        this.jobStateHandlerAccessor.Job.SecondaryMessage = string.Empty;
      }
      catch (Exception ex)
      {
        if (this.currentRetries >= this.MaxProcessRetries)
          throw new JobStateException("Failed to start processing job", ex);
        ++this.currentRetries;
        this.jobStateHandlerAccessor.Job.SecondaryMessage = string.Format(CultureInfo.InvariantCulture, "(retry {0} of {1})", (object) this.currentRetries, (object) this.MaxDownloadRetries);
        Thread.Sleep(3000);
      }
      return flag;
    }

    protected bool DownloadAsset(string assetId, string zipFilePath)
    {
      bool flag = false;
      try
      {
        flag = this.jobStateHandlerAccessor.Client.DownloadAsset(assetId, zipFilePath);
      }
      catch (Exception ex)
      {
        ++this.currentRetries;
        if (this.currentRetries >= this.MaxDownloadRetries)
          throw new JobStateException("Failed to download asset.", ex);
      }
      this.jobStateHandlerAccessor.Job.SecondaryMessage = string.Empty;
      if (flag)
        this.currentRetries = 0;
      else if (this.currentRetries < this.MaxDownloadRetries)
      {
        ++this.currentRetries;
        this.jobStateHandlerAccessor.Job.SecondaryMessage = string.Format(CultureInfo.InvariantCulture, "(retry {0} of {1})", (object) this.currentRetries, (object) this.MaxDownloadRetries);
        Thread.Sleep(3000);
        flag = this.DownloadAsset(assetId, zipFilePath);
      }
      return flag;
    }

    protected void UpdateTaskProgress(object sender, YodaClientTransferProgress arg)
    {
      try
      {
        if (DateTime.MinValue != this.lastTransferProgressUpdate)
        {
          double totalSeconds = DateTime.Now.Subtract(this.lastTransferProgressUpdate).TotalSeconds;
          double num1 = totalSeconds > 0.0 ? ((double) arg.BytesTransfered - this.lastBytesTransferred) / totalSeconds / 1024.0 : 0.0;
          int num2 = -1 == this.assetBytesTotal ? arg.BytesTotal : this.assetBytesTotal;
          double divisor = 1.0;
          string transferFactor = Utils.GetTransferFactor((double) num2, out divisor);
          double num3 = (double) arg.BytesTransfered / divisor;
          double num4 = (double) num2 / divisor;
          this.jobStateHandlerAccessor.Job.PrimaryMessage = string.Format((IFormatProvider) CultureInfo.InvariantCulture, "{0} {1:0.0} KB/s ({2:0.00}/{3:0.00} {4})", (object) this.taskText, (object) num1, (object) num3, (object) num4, (object) transferFactor);
          this.jobStateHandlerAccessor.Job.TaskProgress = arg.ProgressInPercent;
          this.jobStateHandlerAccessor.Job.TotalProgress -= this.lastTotalProgressIncrease;
          int num5 = (int) ((double) this.transferProgressTotal * (double) arg.ProgressInPercent * 0.00999999977648258);
          this.jobStateHandlerAccessor.Job.TotalProgress += num5;
          this.lastTotalProgressIncrease = num5;
          this.lastBytesTransferred = (double) arg.BytesTransfered;
        }
        this.lastTransferProgressUpdate = DateTime.Now;
      }
      catch (Exception ex)
      {
        this.jobStateHandlerAccessor.Log(string.Format(CultureInfo.InvariantCulture, "UpdateTaskProgress failed ({0})", (object) ex.Message));
      }
    }

    protected void UpdateTaskProgressAsParts(object sender, YodaClientTransferProgress arg)
    {
      try
      {
        if (DateTime.MinValue != this.lastTransferProgressUpdate)
        {
          double totalSeconds = DateTime.Now.Subtract(this.lastTransferProgressUpdate).TotalSeconds;
          double num1 = (double) arg.BytesTransfered - this.lastBytesTransferred < 0.0 ? (double) arg.BytesTransfered : (double) arg.BytesTransfered - this.lastBytesTransferred;
          double num2 = totalSeconds > 0.0 ? num1 / totalSeconds / 1024.0 : 0.0;
          long currentFileSize = this.currentFileSize;
          double divisor = 1.0;
          string transferFactor = Utils.GetTransferFactor((double) currentFileSize, out divisor);
          double num3 = (double) (arg.BytesTransfered + (this.currentPartNumber - 1) * this.MaxMbTransferPartSize * 1024 * 1024) / divisor;
          double num4 = (double) currentFileSize / divisor;
          this.jobStateHandlerAccessor.Job.PrimaryMessage = string.Format((IFormatProvider) CultureInfo.InvariantCulture, "{0} {1:0.0} KB/s ({2:0.00}/{3:0.00} {4})", (object) this.taskText, (object) num2, (object) num3, (object) num4, (object) transferFactor);
          this.jobStateHandlerAccessor.Job.TaskProgress = (int) (num3 / num4 * 100.0);
          this.jobStateHandlerAccessor.Job.TotalProgress -= this.lastTotalProgressIncrease;
          int num5 = (int) ((double) this.transferProgressTotal * (double) this.jobStateHandlerAccessor.Job.TaskProgress * 0.00999999977648258);
          this.jobStateHandlerAccessor.Job.TotalProgress += num5;
          this.lastTotalProgressIncrease = num5;
          this.lastBytesTransferred = (double) arg.BytesTransfered;
        }
        this.lastTransferProgressUpdate = DateTime.Now;
      }
      catch (Exception ex)
      {
        this.jobStateHandlerAccessor.Log(string.Format(CultureInfo.InvariantCulture, "UpdateTaskProgress failed ({0})", (object) ex.Message));
      }
    }

    protected void UpdateTransferProgressTotalBytes()
    {
      try
      {
        Asset23 asset = this.jobStateHandlerAccessor.Client.GetAsset(this.jobStateHandlerAccessor.CurrentAssetId, this.jobStateHandlerAccessor.CurrentSPLId);
        if (asset == null)
          return;
        this.assetBytesTotal = asset.FileSize;
      }
      catch (Exception ex)
      {
        this.assetBytesTotal = -1;
      }
    }

    protected void ResetTaskProgress()
    {
      this.assetBytesTotal = -1;
      this.lastTotalProgressIncrease = 0;
      this.lastBytesTransferred = 0.0;
      this.lastTransferProgressUpdate = DateTime.MinValue;
    }
  }
}
