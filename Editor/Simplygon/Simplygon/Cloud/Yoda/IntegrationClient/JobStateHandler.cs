﻿// Decompiled with JetBrains decompiler
// Type: Simplygon.Cloud.Yoda.IntegrationClient.JobStateHandler
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Simplygon.Cloud.Yoda.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Globalization;

namespace Simplygon.Cloud.Yoda.IntegrationClient
{
  internal class JobStateHandler : IJobStateHandler, IJobStateHandlerAccessor, INotifyPropertyChanged
  {
    private Dictionary<Type, JobStateHandler.Transitions> transitions = new Dictionary<Type, JobStateHandler.Transitions>();
    private object stateTransitionLock = new object();
    private string currentStateText = string.Empty;
    private BackgroundWorker backgroundWorker;
    private bool forceAssetUpload;
    private Action<Simplygon.Scene.Scene, string> saveSSFAction;
    private Action<string, string, string, Action<int, string>> zipAction;
    private Action<string, string, string, Action<int, string>> unzipAction;
    private Action<List<string>, Action> importAction;
    private Action<CloudJob, List<string>, Action> importActionWithCloudJob;
    private Action<string> cleanWorkDirectoryAction;
    private bool initiateDownloadAccepted;
    private bool initiateImportAccepted;
    private IJobState currentState;
    private bool isJobDeletionRequested;

    public event EventHandler OnStateChanged;

    public event EventHandler<JobStateChangedEventargs> OnJobInitialized;

    public event EventHandler<JobStateChangedEventargs> OnJobCreated;

    public event EventHandler<JobStateChangedEventargs> OnJobReadyForDownload;

    public event EventHandler<JobStateChangedEventargs> OnJobReadyForImport;

    public event EventHandler<JobStateChangedEventargs> OnJobImported;

    public event EventHandler<JobStateChangedEventargs> OnJobDeleted;

    public event EventHandler<JobFailedEventargs> OnJobFailed;

    public event PropertyChangedEventHandler PropertyChanged;

    public YodaClient23 Client { get; set; }

    public CloudJob Job { get; set; }

    public string CurrentAssetId { get; set; }

    public string CurrentSPLId { get; set; }

    public string CurrentJobDirectory { get; set; }

    public string JobType { get; set; }

    public Action<string> LogAction { get; protected set; }

    public bool ForceAssetUpload
    {
      get
      {
        return this.forceAssetUpload;
      }
    }

    public bool InitiateDownloadAccepted
    {
      get
      {
        return this.initiateDownloadAccepted;
      }
      set
      {
        this.initiateDownloadAccepted = value;
      }
    }

    public bool InitiateImportAccepted
    {
      get
      {
        return this.initiateImportAccepted;
      }
      set
      {
        this.initiateImportAccepted = value;
      }
    }

    public IJobState CurrentState
    {
      get
      {
        return this.currentState;
      }
      protected set
      {
        if (value == this.currentState)
          return;
        this.currentState = value;
        this.OnPropertyChanged(nameof (CurrentState));
        this.CurrentStateText = this.currentState.AsText();
      }
    }

    public string CurrentStateText
    {
      get
      {
        return this.currentStateText;
      }
      protected set
      {
        if (!(value != this.currentStateText))
          return;
        this.currentStateText = value;
        this.OnPropertyChanged(nameof (CurrentStateText));
      }
    }

    public bool IsJobBeingCreated
    {
      get
      {
        return this.CurrentState is JobStateInitialize;
      }
    }

    public bool IsJobBeingUploaded
    {
      get
      {
        return this.CurrentState is JobStateUpload;
      }
    }

    public bool IsJobAwaitingDownloadAccept
    {
      get
      {
        return this.CurrentState is JobStateAwaitingDownloadAccept;
      }
    }

    public bool IsJobBeingDownloaded
    {
      get
      {
        return this.CurrentState is JobStateDownloadAsset;
      }
    }

    public bool IsJobAwaitingImportAccept
    {
      get
      {
        return this.CurrentState is JobStateAwaitingImportAccept;
      }
    }

    public bool IsJobBeingImported
    {
      get
      {
        if (!(this.CurrentState is JobStateDecompress))
          return this.CurrentState is JobStateImport;
        return true;
      }
    }

    public bool IsJobCompleted
    {
      get
      {
        return this.CurrentState is JobStateCompleted;
      }
    }

    public bool IsJobDeletionRequested
    {
      get
      {
        return this.isJobDeletionRequested;
      }
      set
      {
        this.isJobDeletionRequested = value;
      }
    }

    public bool IsJobBeingDeleted
    {
      get
      {
        return this.CurrentState is JobStateDeleteJob;
      }
    }

    public bool IsJobDeleted
    {
      get
      {
        return this.CurrentState is JobStateDeleted;
      }
    }

    public bool IsJobFailed
    {
      get
      {
        return this.CurrentState is JobStateFailed;
      }
    }

    public bool IsJobInFinalState
    {
      get
      {
        JobStateHandler.Transitions transitions;
        if (this.transitions.TryGetValue(this.currentState.GetType(), out transitions))
          return transitions.IsFinalState;
        throw new JobStateException(string.Format(CultureInfo.InvariantCulture, "Unable to find transitions for state '{0}'", (object) this.currentState.GetType().ToString()));
      }
    }

    public JobStateHandler(YodaClient23 client, IJobState initialJobState, Action<string> logAction, Action<Simplygon.Scene.Scene, string> saveSSFAction, Action<string, string, string, Action<int, string>> zipAction, Action<string, string, string, Action<int, string>> unzipAction, Action<List<string>, Action> importAction, Action<string> cleanWorkDirectoryAction, string jobType = "Mjolnir")
      : this(client, initialJobState, logAction, saveSSFAction, zipAction, unzipAction, cleanWorkDirectoryAction, jobType)
    {
      this.importAction = importAction;
    }

    public JobStateHandler(YodaClient23 client, IJobState initialJobState, Action<string> logAction, Action<Simplygon.Scene.Scene, string> saveSSFAction, Action<string, string, string, Action<int, string>> zipAction, Action<string, string, string, Action<int, string>> unzipAction, Action<CloudJob, List<string>, Action> importActionWithCloudJob, Action<string> cleanWorkDirectoryAction, string jobType = "Mjolnir")
      : this(client, initialJobState, logAction, saveSSFAction, zipAction, unzipAction, cleanWorkDirectoryAction, jobType)
    {
      this.importActionWithCloudJob = importActionWithCloudJob;
    }

    public JobStateHandler(YodaClient23 client, IJobState initialJobState, Action<string> logAction, Action<Simplygon.Scene.Scene, string> saveSSFAction, Action<string, string, string, Action<int, string>> zipAction, Action<string, string, string, Action<int, string>> unzipAction, Action<string> cleanWorkDirectoryAction, string jobType = "Mjolnir")
    {
      this.Client = client;
      this.CurrentState = initialJobState;
      this.LogAction = logAction;
      this.saveSSFAction = saveSSFAction;
      this.zipAction = zipAction;
      this.unzipAction = unzipAction;
      this.cleanWorkDirectoryAction = cleanWorkDirectoryAction;
      this.JobType = jobType;
      this.backgroundWorker = new BackgroundWorker();
      this.backgroundWorker.DoWork += new DoWorkEventHandler(this.DoWork);
      this.backgroundWorker.WorkerSupportsCancellation = true;
      this.backgroundWorker.WorkerReportsProgress = false;
      this.BuildTransitions();
    }

    public void Run(bool forceAssetUpload = false)
    {
      if (this.backgroundWorker.IsBusy)
        return;
      this.forceAssetUpload = forceAssetUpload;
      this.CurrentState.Enter((IJobStateHandlerAccessor) this);
      this.backgroundWorker.RunWorkerAsync();
    }

    public void Stop()
    {
      this.backgroundWorker.CancelAsync();
      this.Client.AbortTransfer();
    }

    public void AcceptDownload()
    {
      this.initiateDownloadAccepted = true;
    }

    public void AcceptImport()
    {
      this.initiateImportAccepted = true;
    }

    public void FireJobInitialized()
    {
      // ISSUE: reference to a compiler-generated field
      if (this.OnJobInitialized == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.OnJobInitialized((object) this, new JobStateChangedEventargs(this.Job));
    }

    public void FireJobCreated()
    {
      // ISSUE: reference to a compiler-generated field
      if (this.OnJobCreated == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.OnJobCreated((object) this, new JobStateChangedEventargs(this.Job));
    }

    public void FireJobReadyForDownload()
    {
      // ISSUE: reference to a compiler-generated field
      if (this.OnJobReadyForDownload == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.OnJobReadyForDownload((object) this, new JobStateChangedEventargs(this.Job));
    }

    public void FireJobReadyForImport()
    {
      // ISSUE: reference to a compiler-generated field
      if (this.OnJobReadyForImport == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.OnJobReadyForImport((object) this, new JobStateChangedEventargs(this.Job));
    }

    public void FireJobImported()
    {
      // ISSUE: reference to a compiler-generated field
      if (this.OnJobImported == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.OnJobImported((object) this, new JobStateChangedEventargs(this.Job));
    }

    public void FireJobDeleted()
    {
      // ISSUE: reference to a compiler-generated field
      if (this.OnJobDeleted == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.OnJobDeleted((object) this, new JobStateChangedEventargs(this.Job));
    }

    public void FireJobFailed(string message)
    {
      // ISSUE: reference to a compiler-generated field
      if (this.OnJobFailed == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.OnJobFailed((object) this, new JobFailedEventargs(this.Job, message));
    }

    public void FireSaveSSFAction(Simplygon.Scene.Scene scene, string sceneFilePath)
    {
      if (this.saveSSFAction == null)
        return;
      this.saveSSFAction(scene, sceneFilePath);
    }

    public void FireZipAction(string inputDirectory, string outputDirectory, string fileName, Action<int, string> progressHandler)
    {
      if (this.zipAction == null)
        return;
      this.zipAction(inputDirectory, outputDirectory, fileName, progressHandler);
    }

    public void FireUnzipAction(string inputDirectory, string outputDirectory, string fileName, Action<int, string> progressHandler)
    {
      if (this.unzipAction == null)
        return;
      this.unzipAction(inputDirectory, outputDirectory, fileName, progressHandler);
    }

    public void FireImportAction(List<string> lodFilePaths, Action onCompleted)
    {
      if (this.importAction != null)
      {
        this.importAction(lodFilePaths, onCompleted);
      }
      else
      {
        if (this.importActionWithCloudJob == null)
          return;
        this.importActionWithCloudJob(this.Job, lodFilePaths, onCompleted);
      }
    }

    public void FireCleanWorkDirectoryAction(string jobDirectory)
    {
      if (this.cleanWorkDirectoryAction == null)
        return;
      this.cleanWorkDirectoryAction(jobDirectory);
    }

    protected void OnPropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      if (this.PropertyChanged == null)
        return;
      // ISSUE: reference to a compiler-generated field
      this.PropertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }

    private void DoWork(object sender, DoWorkEventArgs e)
    {
      BackgroundWorker backgroundWorker = sender as BackgroundWorker;
      try
      {
        bool flag = true;
        while (flag)
        {
          if (backgroundWorker.CancellationPending)
          {
            e.Cancel = true;
            flag = false;
          }
          else if (this.isJobDeletionRequested || this.CurrentState.Execute())
            this.MoveToNextState();
          else
            Thread.Sleep(100);
        }
      }
      catch (JobStateException ex)
      {
        this.Log(string.Format(CultureInfo.InvariantCulture, "{0} \n\n{1}", (object) ex.Message, ex.InnerException == null ? (object) string.Empty : (object) ex.InnerException.Message));
        this.MoveToState((IJobState) new JobStateFailed(string.Format(CultureInfo.InvariantCulture, "Failed ({0})", (object) ex.Message)));
      }
    }

    public void RequestJobDeletion()
    {
      if (this.isJobDeletionRequested || this.CurrentState is JobStateDeleteJob || this.CurrentState is JobStateDeleted)
        return;
      this.isJobDeletionRequested = true;
      this.Client.AbortTransfer();
      if (!this.IsJobInFinalState)
        this.Job.SecondaryMessage = "(cancellation pending)";
      if (this.backgroundWorker.IsBusy)
        return;
      this.backgroundWorker.RunWorkerAsync();
    }

    public void Reimport()
    {
      lock (this.stateTransitionLock)
      {
        if (!this.IsJobCompleted)
          return;
        this.MoveToState((IJobState) new JobStateImport());
      }
    }

    public void Log(string message)
    {
      if (this.LogAction == null)
        return;
      this.LogAction(message);
    }

    private void MoveToNextState()
    {
      lock (this.stateTransitionLock)
      {
        JobStateHandler.Transitions transitions;
        if (!this.transitions.TryGetValue(this.CurrentState.GetType(), out transitions))
          throw new JobStateException(string.Format(CultureInfo.InvariantCulture, "Unable to find a suitable transition from job state '{0}'.", (object) this.currentState.GetType().ToString()));
        Type nextState = transitions.GetNextState();
        if (nextState == null || transitions.IsFinalState && !this.isJobDeletionRequested)
        {
          this.backgroundWorker.CancelAsync();
        }
        else
        {
          IJobState instance = Activator.CreateInstance(nextState) as IJobState;
          this.CurrentState.Exit();
          this.CurrentState = instance;
          this.CurrentState.Enter((IJobStateHandlerAccessor) this);
          // ISSUE: reference to a compiler-generated field
          if (this.OnStateChanged != null)
          {
            // ISSUE: reference to a compiler-generated field
            this.OnStateChanged((object) this, EventArgs.Empty);
          }
          if (this.backgroundWorker.IsBusy)
            return;
          this.backgroundWorker.RunWorkerAsync();
        }
      }
    }

    private void MoveToState(IJobState state)
    {
      lock (this.stateTransitionLock)
      {
        this.CurrentState.Exit();
        this.CurrentState = state;
        this.CurrentState.Enter((IJobStateHandlerAccessor) this);
        // ISSUE: reference to a compiler-generated field
        if (this.OnStateChanged != null)
        {
          // ISSUE: reference to a compiler-generated field
          this.OnStateChanged((object) this, EventArgs.Empty);
        }
        if (this.backgroundWorker.IsBusy)
          return;
        this.backgroundWorker.RunWorkerAsync();
      }
    }

    private void BuildTransitions()
    {
      JobStateHandler.Transitions transitions1 = new JobStateHandler.Transitions(false);
      transitions1.AddTargetState(typeof (JobStateDeleteJob), new List<Func<bool>>()
      {
        new Func<bool>(this.IsDeleteRequested)
      });
      transitions1.AddTargetState(typeof (JobStateAwaitingDownloadAccept), new List<Func<bool>>()
      {
        new Func<bool>(this.HasAssetBeenProcessed)
      });
      transitions1.AddTargetState(typeof (JobStateUpload), (List<Func<bool>>) null);
      JobStateHandler.Transitions transitions2 = new JobStateHandler.Transitions(false);
      transitions2.AddTargetState(typeof (JobStateDeleteJob), new List<Func<bool>>()
      {
        new Func<bool>(this.IsDeleteRequested)
      });
      transitions2.AddTargetState(typeof (JobStateProcessJob), (List<Func<bool>>) null);
      JobStateHandler.Transitions transitions3 = new JobStateHandler.Transitions(false);
      transitions3.AddTargetState(typeof (JobStateDeleteJob), new List<Func<bool>>()
      {
        new Func<bool>(this.IsDeleteRequested)
      });
      transitions3.AddTargetState(typeof (JobStateFailed), new List<Func<bool>>()
      {
        new Func<bool>(this.HasProcessedStateNotYetProcessed)
      });
      transitions3.AddTargetState(typeof (JobStateAwaitingDownloadAccept), new List<Func<bool>>());
      JobStateHandler.Transitions transitions4 = new JobStateHandler.Transitions(false);
      transitions4.AddTargetState(typeof (JobStateDeleteJob), new List<Func<bool>>()
      {
        new Func<bool>(this.IsDeleteRequested)
      });
      transitions4.AddTargetState(typeof (JobStateDownloadAsset), (List<Func<bool>>) null);
      JobStateHandler.Transitions transitions5 = new JobStateHandler.Transitions(false);
      transitions5.AddTargetState(typeof (JobStateDeleteJob), new List<Func<bool>>()
      {
        new Func<bool>(this.IsDeleteRequested)
      });
      transitions5.AddTargetState(typeof (JobStateFailed), new List<Func<bool>>()
      {
        new Func<bool>(this.IsDownloadedAssetFileMissing)
      });
      transitions5.AddTargetState(typeof (JobStateDecompress), (List<Func<bool>>) null);
      JobStateHandler.Transitions transitions6 = new JobStateHandler.Transitions(false);
      transitions6.AddTargetState(typeof (JobStateDeleteJob), new List<Func<bool>>()
      {
        new Func<bool>(this.IsDeleteRequested)
      });
      transitions6.AddTargetState(typeof (JobStateAwaitingImportAccept), (List<Func<bool>>) null);
      JobStateHandler.Transitions transitions7 = new JobStateHandler.Transitions(false);
      transitions7.AddTargetState(typeof (JobStateDeleteJob), new List<Func<bool>>()
      {
        new Func<bool>(this.IsDeleteRequested)
      });
      transitions7.AddTargetState(typeof (JobStateImport), (List<Func<bool>>) null);
      JobStateHandler.Transitions transitions8 = new JobStateHandler.Transitions(false);
      transitions8.AddTargetState(typeof (JobStateDeleteJob), new List<Func<bool>>()
      {
        new Func<bool>(this.IsDeleteRequested)
      });
      transitions8.AddTargetState(typeof (JobStateCompleted), (List<Func<bool>>) null);
      JobStateHandler.Transitions transitions9 = new JobStateHandler.Transitions(false);
      transitions9.AddTargetState(typeof (JobStateDeleted), (List<Func<bool>>) null);
      JobStateHandler.Transitions transitions10 = new JobStateHandler.Transitions(true);
      transitions10.AddTargetState(typeof (JobStateDeleteJob), new List<Func<bool>>()
      {
        new Func<bool>(this.IsDeleteRequested)
      });
      JobStateHandler.Transitions transitions11 = new JobStateHandler.Transitions(true);
      JobStateHandler.Transitions transitions12 = new JobStateHandler.Transitions(true);
      transitions12.AddTargetState(typeof (JobStateDeleteJob), new List<Func<bool>>()
      {
        new Func<bool>(this.IsDeleteRequested)
      });
      this.transitions.Add(typeof (JobStateInitialize), transitions1);
      this.transitions.Add(typeof (JobStateUpload), transitions2);
      this.transitions.Add(typeof (JobStateProcessJob), transitions3);
      this.transitions.Add(typeof (JobStateAwaitingDownloadAccept), transitions4);
      this.transitions.Add(typeof (JobStateDownloadAsset), transitions5);
      this.transitions.Add(typeof (JobStateDecompress), transitions6);
      this.transitions.Add(typeof (JobStateAwaitingImportAccept), transitions7);
      this.transitions.Add(typeof (JobStateImport), transitions8);
      this.transitions.Add(typeof (JobStateDeleteJob), transitions9);
      this.transitions.Add(typeof (JobStateCompleted), transitions10);
      this.transitions.Add(typeof (JobStateDeleted), transitions11);
      this.transitions.Add(typeof (JobStateFailed), transitions12);
    }

    private bool HasAssetBeenProcessed()
    {
      bool flag = false;
      if (!this.forceAssetUpload)
      {
        if (string.IsNullOrEmpty(this.CurrentAssetId) || string.IsNullOrEmpty(this.CurrentSPLId))
          throw new Exception("Unable to determine if asset has been processed - asset ID and/or settings ID is null or empty.");
        Asset23 asset = this.Client.GetAsset(this.CurrentAssetId, this.CurrentSPLId);
        if (asset != null)
        {
          this.CurrentAssetId = asset.AssetId;
          flag = true;
        }
      }
      return flag;
    }

    private bool IsDeleteRequested()
    {
      return this.isJobDeletionRequested;
    }

    private bool HasProcessedStateNotYetProcessed()
    {
      return this.Job.TotalProgress < 70;
    }

    private bool IsDownloadedAssetFileMissing()
    {
      int num = !File.Exists(Path.Combine(this.CurrentJobDirectory, JobConstants.OutputPackedFileName)) ? 1 : 0;
      if (num == 0)
        return num != 0;
      this.OnStateChanged += (EventHandler) ((s, e) =>
      {
        this.Job.PrimaryMessage = "Failed - Asset missing.";
        this.Job.SecondaryMessage = string.Empty;
      });
      return num != 0;
    }

    private class Transitions
    {
      private List<KeyValuePair<Type, List<Func<bool>>>> nextStates = new List<KeyValuePair<Type, List<Func<bool>>>>();

      public bool IsFinalState { get; protected set; }

      public Transitions(bool isFinalState)
      {
        this.IsFinalState = isFinalState;
      }

      public void AddTargetState(Type targetStateType, List<Func<bool>> preconditions = null)
      {
        this.nextStates.Add(new KeyValuePair<Type, List<Func<bool>>>(targetStateType, preconditions));
      }

      public Type GetNextState()
      {
        return this.nextStates.FirstOrDefault<KeyValuePair<Type, List<Func<bool>>>>((Func<KeyValuePair<Type, List<Func<bool>>>, bool>) (entry =>
        {
          if (entry.Value != null)
            return entry.Value.All<Func<bool>>((Func<Func<bool>, bool>) (precondition => precondition()));
          return true;
        })).Key;
      }
    }

    public delegate void JobFailedEventHandler(string message);
  }
}
