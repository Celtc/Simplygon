﻿// Decompiled with JetBrains decompiler
// Type: Ionic.ComparisonOperator
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.ComponentModel;

namespace Ionic
{
  internal enum ComparisonOperator
  {
    [Description(">")] GreaterThan,
    [Description(">=")] GreaterThanOrEqualTo,
    [Description("<")] LesserThan,
    [Description("<=")] LesserThanOrEqualTo,
    [Description("=")] EqualTo,
    [Description("!=")] NotEqualTo,
  }
}
