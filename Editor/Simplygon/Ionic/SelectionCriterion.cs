﻿// Decompiled with JetBrains decompiler
// Type: Ionic.SelectionCriterion
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using Ionic.Zip;
using System.Diagnostics;

namespace Ionic
{
  internal abstract class SelectionCriterion
  {
    internal virtual bool Verbose { get; set; }

    internal abstract bool Evaluate(string filename);

    [Conditional("SelectorTrace")]
    protected static void CriterionTrace(string format, params object[] args)
    {
    }

    internal abstract bool Evaluate(ZipEntry entry);
  }
}
