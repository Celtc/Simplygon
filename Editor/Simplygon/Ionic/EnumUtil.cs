﻿// Decompiled with JetBrains decompiler
// Type: Ionic.EnumUtil
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.ComponentModel;

namespace Ionic
{
  internal sealed class EnumUtil
  {
    private EnumUtil()
    {
    }

    internal static string GetDescription(Enum value)
    {
      DescriptionAttribute[] customAttributes = (DescriptionAttribute[]) value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof (DescriptionAttribute), false);
      if (customAttributes.Length > 0)
        return customAttributes[0].Description;
      return value.ToString();
    }

    internal static object Parse(Type enumType, string stringRepresentation)
    {
      return EnumUtil.Parse(enumType, stringRepresentation, false);
    }

    internal static object Parse(Type enumType, string stringRepresentation, bool ignoreCase)
    {
      if (ignoreCase)
        stringRepresentation = stringRepresentation.ToLower();
      foreach (Enum @enum in Enum.GetValues(enumType))
      {
        string str = EnumUtil.GetDescription(@enum);
        if (ignoreCase)
          str = str.ToLower();
        if (str == stringRepresentation)
          return (object) @enum;
      }
      return Enum.Parse(enumType, stringRepresentation, ignoreCase);
    }
  }
}
