﻿// Decompiled with JetBrains decompiler
// Type: Ionic.Zlib.ZlibException
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.Runtime.InteropServices;

namespace Ionic.Zlib
{
  [Guid("ebc25cf6-9120-4283-b972-0e5520d0000E")]
  internal class ZlibException : Exception
  {
    public ZlibException()
    {
    }

    public ZlibException(string s)
      : base(s)
    {
    }
  }
}
