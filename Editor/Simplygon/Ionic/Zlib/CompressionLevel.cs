﻿// Decompiled with JetBrains decompiler
// Type: Ionic.Zlib.CompressionLevel
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Ionic.Zlib
{
  internal enum CompressionLevel
  {
    Level0 = 0,
    None = 0,
    BestSpeed = 1,
    Level1 = 1,
    Level2 = 2,
    Level3 = 3,
    Level4 = 4,
    Level5 = 5,
    Default = 6,
    Level6 = 6,
    Level7 = 7,
    Level8 = 8,
    BestCompression = 9,
    Level9 = 9,
  }
}
