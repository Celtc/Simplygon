﻿// Decompiled with JetBrains decompiler
// Type: Ionic.Zlib.ZlibStreamFlavor
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Ionic.Zlib
{
  internal enum ZlibStreamFlavor
  {
    ZLIB = 1950, // 0x0000079E
    DEFLATE = 1951, // 0x0000079F
    GZIP = 1952, // 0x000007A0
  }
}
