﻿// Decompiled with JetBrains decompiler
// Type: Ionic.LogicalConjunction
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Ionic
{
  internal enum LogicalConjunction
  {
    NONE,
    AND,
    OR,
    XOR,
  }
}
