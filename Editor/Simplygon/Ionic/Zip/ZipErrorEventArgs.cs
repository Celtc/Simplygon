﻿// Decompiled with JetBrains decompiler
// Type: Ionic.Zip.ZipErrorEventArgs
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;

namespace Ionic.Zip
{
  internal class ZipErrorEventArgs : ZipProgressEventArgs
  {
    private Exception _exc;

    private ZipErrorEventArgs()
    {
    }

    internal static ZipErrorEventArgs Saving(string archiveName, ZipEntry entry, Exception exception)
    {
      ZipErrorEventArgs zipErrorEventArgs = new ZipErrorEventArgs();
      zipErrorEventArgs.EventType = ZipProgressEventType.Error_Saving;
      zipErrorEventArgs.ArchiveName = archiveName;
      zipErrorEventArgs.CurrentEntry = entry;
      zipErrorEventArgs._exc = exception;
      return zipErrorEventArgs;
    }

    public Exception Exception
    {
      get
      {
        return this._exc;
      }
    }

    public string FileName
    {
      get
      {
        return this.CurrentEntry.LocalFileName;
      }
    }
  }
}
