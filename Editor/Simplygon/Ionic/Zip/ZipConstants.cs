﻿// Decompiled with JetBrains decompiler
// Type: Ionic.Zip.ZipConstants
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Ionic.Zip
{
  internal static class ZipConstants
  {
    public const uint PackedToRemovableMedia = 808471376;
    public const uint Zip64EndOfCentralDirectoryRecordSignature = 101075792;
    public const uint Zip64EndOfCentralDirectoryLocatorSignature = 117853008;
    public const uint EndOfCentralDirectorySignature = 101010256;
    public const int ZipEntrySignature = 67324752;
    public const int ZipEntryDataDescriptorSignature = 134695760;
    public const int SplitArchiveSignature = 134695760;
    public const int ZipDirEntrySignature = 33639248;
    public const int AesKeySize = 192;
    public const int AesBlockSize = 128;
    public const ushort AesAlgId128 = 26126;
    public const ushort AesAlgId192 = 26127;
    public const ushort AesAlgId256 = 26128;
  }
}
