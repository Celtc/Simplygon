﻿// Decompiled with JetBrains decompiler
// Type: Ionic.Zip.ReadOptions
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.IO;
using System.Text;

namespace Ionic.Zip
{
  internal class ReadOptions
  {
    public EventHandler<ReadProgressEventArgs> ReadProgress { get; set; }

    public TextWriter StatusMessageWriter { get; set; }

    public Encoding Encoding { get; set; }
  }
}
