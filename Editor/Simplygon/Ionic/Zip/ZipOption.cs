﻿// Decompiled with JetBrains decompiler
// Type: Ionic.Zip.ZipOption
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Ionic.Zip
{
  internal enum ZipOption
  {
    Default = 0,
    Never = 0,
    AsNecessary = 1,
    Always = 2,
  }
}
