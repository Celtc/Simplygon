﻿// Decompiled with JetBrains decompiler
// Type: Ionic.Zip.ReadProgressEventArgs
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Ionic.Zip
{
  internal class ReadProgressEventArgs : ZipProgressEventArgs
  {
    internal ReadProgressEventArgs()
    {
    }

    private ReadProgressEventArgs(string archiveName, ZipProgressEventType flavor)
      : base(archiveName, flavor)
    {
    }

    internal static ReadProgressEventArgs Before(string archiveName, int entriesTotal)
    {
      ReadProgressEventArgs progressEventArgs = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_BeforeReadEntry);
      progressEventArgs.EntriesTotal = entriesTotal;
      return progressEventArgs;
    }

    internal static ReadProgressEventArgs After(string archiveName, ZipEntry entry, int entriesTotal)
    {
      ReadProgressEventArgs progressEventArgs = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_AfterReadEntry);
      progressEventArgs.EntriesTotal = entriesTotal;
      progressEventArgs.CurrentEntry = entry;
      return progressEventArgs;
    }

    internal static ReadProgressEventArgs Started(string archiveName)
    {
      return new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_Started);
    }

    internal static ReadProgressEventArgs ByteUpdate(string archiveName, ZipEntry entry, long bytesXferred, long totalBytes)
    {
      ReadProgressEventArgs progressEventArgs = new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_ArchiveBytesRead);
      progressEventArgs.CurrentEntry = entry;
      progressEventArgs.BytesTransferred = bytesXferred;
      progressEventArgs.TotalBytesToTransfer = totalBytes;
      return progressEventArgs;
    }

    internal static ReadProgressEventArgs Completed(string archiveName)
    {
      return new ReadProgressEventArgs(archiveName, ZipProgressEventType.Reading_Completed);
    }
  }
}
