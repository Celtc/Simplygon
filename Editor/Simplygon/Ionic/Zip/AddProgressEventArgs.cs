﻿// Decompiled with JetBrains decompiler
// Type: Ionic.Zip.AddProgressEventArgs
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace Ionic.Zip
{
  internal class AddProgressEventArgs : ZipProgressEventArgs
  {
    internal AddProgressEventArgs()
    {
    }

    private AddProgressEventArgs(string archiveName, ZipProgressEventType flavor)
      : base(archiveName, flavor)
    {
    }

    internal static AddProgressEventArgs AfterEntry(string archiveName, ZipEntry entry, int entriesTotal)
    {
      AddProgressEventArgs progressEventArgs = new AddProgressEventArgs(archiveName, ZipProgressEventType.Adding_AfterAddEntry);
      progressEventArgs.EntriesTotal = entriesTotal;
      progressEventArgs.CurrentEntry = entry;
      return progressEventArgs;
    }

    internal static AddProgressEventArgs Started(string archiveName)
    {
      return new AddProgressEventArgs(archiveName, ZipProgressEventType.Adding_Started);
    }

    internal static AddProgressEventArgs Completed(string archiveName)
    {
      return new AddProgressEventArgs(archiveName, ZipProgressEventType.Adding_Completed);
    }
  }
}
