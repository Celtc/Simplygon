﻿// Decompiled with JetBrains decompiler
// Type: Ionic.Zip.ComHelper
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.Runtime.InteropServices;

namespace Ionic.Zip
{
  [ComVisible(true)]
  [Guid("ebc25cf6-9120-4283-b972-0e5520d0000F")]
  [ClassInterface(ClassInterfaceType.AutoDispatch)]
  internal class ComHelper
  {
    public bool IsZipFile(string filename)
    {
      return ZipFile.IsZipFile(filename);
    }

    public bool IsZipFileWithExtract(string filename)
    {
      return ZipFile.IsZipFile(filename, true);
    }

    public bool CheckZip(string filename)
    {
      return ZipFile.CheckZip(filename);
    }

    public bool CheckZipPassword(string filename, string password)
    {
      return ZipFile.CheckZipPassword(filename, password);
    }

    public void FixZipDirectory(string filename)
    {
      ZipFile.FixZipDirectory(filename);
    }

    public string GetZipLibraryVersion()
    {
      return ZipFile.LibraryVersion.ToString();
    }
  }
}
