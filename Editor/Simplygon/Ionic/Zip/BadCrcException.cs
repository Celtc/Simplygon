﻿// Decompiled with JetBrains decompiler
// Type: Ionic.Zip.BadCrcException
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace Ionic.Zip
{
  [Guid("ebc25cf6-9120-4283-b972-0e5520d00009")]
  [Serializable]
  internal class BadCrcException : ZipException
  {
    public BadCrcException()
    {
    }

    public BadCrcException(string message)
      : base(message)
    {
    }

    protected BadCrcException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }
  }
}
