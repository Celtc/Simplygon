﻿// Decompiled with JetBrains decompiler
// Type: Ionic.BZip2.WorkItem
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.IO;

namespace Ionic.BZip2
{
  internal class WorkItem
  {
    public int index;
    public MemoryStream ms;
    public int ordinal;
    public BitWriter bw;

    public BZip2Compressor Compressor { get; private set; }

    public WorkItem(int ix, int blockSize)
    {
      this.ms = new MemoryStream();
      this.bw = new BitWriter((Stream) this.ms);
      this.Compressor = new BZip2Compressor(this.bw, blockSize);
      this.index = ix;
    }
  }
}
