﻿// Decompiled with JetBrains decompiler
// Type: Ionic.BZip2.BitWriter
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

using System.IO;

namespace Ionic.BZip2
{
  internal class BitWriter
  {
    private uint accumulator;
    private int nAccumulatedBits;
    private Stream output;
    private int totalBytesWrittenOut;

    public BitWriter(Stream s)
    {
      this.output = s;
    }

    public byte RemainingBits
    {
      get
      {
        return (byte) (this.accumulator >> 32 - this.nAccumulatedBits & (uint) byte.MaxValue);
      }
    }

    public int NumRemainingBits
    {
      get
      {
        return this.nAccumulatedBits;
      }
    }

    public int TotalBytesWrittenOut
    {
      get
      {
        return this.totalBytesWrittenOut;
      }
    }

    public void Reset()
    {
      this.accumulator = 0U;
      this.nAccumulatedBits = 0;
      this.totalBytesWrittenOut = 0;
      this.output.Seek(0L, SeekOrigin.Begin);
      this.output.SetLength(0L);
    }

    public void WriteBits(int nbits, uint value)
    {
      int nAccumulatedBits = this.nAccumulatedBits;
      uint accumulator = this.accumulator;
      while (nAccumulatedBits >= 8)
      {
        this.output.WriteByte((byte) (accumulator >> 24 & (uint) byte.MaxValue));
        ++this.totalBytesWrittenOut;
        accumulator <<= 8;
        nAccumulatedBits -= 8;
      }
      this.accumulator = accumulator | value << 32 - nAccumulatedBits - nbits;
      this.nAccumulatedBits = nAccumulatedBits + nbits;
    }

    public void WriteByte(byte b)
    {
      this.WriteBits(8, (uint) b);
    }

    public void WriteInt(uint u)
    {
      this.WriteBits(8, u >> 24 & (uint) byte.MaxValue);
      this.WriteBits(8, u >> 16 & (uint) byte.MaxValue);
      this.WriteBits(8, u >> 8 & (uint) byte.MaxValue);
      this.WriteBits(8, u & (uint) byte.MaxValue);
    }

    public void Flush()
    {
      this.WriteBits(0, 0U);
    }

    public void FinishAndPad()
    {
      this.Flush();
      if (this.NumRemainingBits <= 0)
        return;
      this.output.WriteByte((byte) (this.accumulator >> 24 & (uint) byte.MaxValue));
      ++this.totalBytesWrittenOut;
    }
  }
}
