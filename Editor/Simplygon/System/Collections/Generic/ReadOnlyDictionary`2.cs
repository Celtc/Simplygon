﻿// Decompiled with JetBrains decompiler
// Type: System.Collections.Generic.ReadOnlyDictionary`2
// Assembly: Simplygon, Version=2.5.1.0, Culture=neutral, PublicKeyToken=null
// MVID: 170329F6-7183-49D7-BE1E-D184EE6EBA04
// Assembly location: C:\Programming\Unity\VRForkLift\Assets\Editor\Simplygon.dll

namespace System.Collections.Generic
{
  internal class ReadOnlyDictionary<TKey, TValue> : IDictionary<TKey, TValue>, ICollection<KeyValuePair<TKey, TValue>>, IEnumerable<KeyValuePair<TKey, TValue>>, IEnumerable
  {
    private const string READ_ONLY_ERROR_MESSAGE = "This dictionary is read-only";
    protected IDictionary<TKey, TValue> dictionary;

    public ReadOnlyDictionary()
    {
      this.dictionary = (IDictionary<TKey, TValue>) new Dictionary<TKey, TValue>();
    }

    public ReadOnlyDictionary(IDictionary<TKey, TValue> dictionary)
    {
      this.dictionary = dictionary;
    }

    public bool ContainsKey(TKey key)
    {
      return this.dictionary.ContainsKey(key);
    }

    public ICollection<TKey> Keys
    {
      get
      {
        return this.dictionary.Keys;
      }
    }

    public bool TryGetValue(TKey key, out TValue value)
    {
      return this.dictionary.TryGetValue(key, out value);
    }

    public ICollection<TValue> Values
    {
      get
      {
        return this.dictionary.Values;
      }
    }

    public TValue this[TKey key]
    {
      get
      {
        return this.dictionary[key];
      }
      set
      {
        throw new NotSupportedException("This dictionary is read-only");
      }
    }

    public bool Contains(KeyValuePair<TKey, TValue> item)
    {
      return this.dictionary.Contains(item);
    }

    public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
    {
      this.dictionary.CopyTo(array, arrayIndex);
    }

    public int Count
    {
      get
      {
        return this.dictionary.Count;
      }
    }

    public bool IsReadOnly
    {
      get
      {
        return true;
      }
    }

    public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
    {
      return this.dictionary.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return this.dictionary.GetEnumerator();
    }

    void IDictionary<TKey, TValue>.Add(TKey key, TValue value)
    {
      throw new NotSupportedException("This dictionary is read-only");
    }

    bool IDictionary<TKey, TValue>.Remove(TKey key)
    {
      throw new NotSupportedException("This dictionary is read-only");
    }

    void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item)
    {
      throw new NotSupportedException("This dictionary is read-only");
    }

    void ICollection<KeyValuePair<TKey, TValue>>.Clear()
    {
      throw new NotSupportedException("This dictionary is read-only");
    }

    bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item)
    {
      throw new NotSupportedException("This dictionary is read-only");
    }
  }
}
